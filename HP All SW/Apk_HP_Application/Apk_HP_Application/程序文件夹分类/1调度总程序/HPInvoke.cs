﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.Net;
using System.Xml;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Drawing;
using System.Data;
using System.Data.Sql;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;
using APK_HP.MES.HPSql;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public class HPInvoke
    {
        public static frm_AutomaticSmartLine frm_MainPanel = null;
        public static bool bMachineIsInitialized = false;
        public static bool bMachineIsStarted = false;
        public static bool bMachineIsReseting = false;

        public static string strStartMsg = "";

        public static TcpListener invodeSocketServer = null;
        public static List<Thread> m_listThreadClient = new List<Thread>();
        public static Queue<TcpClient> m_queTcpClient = new Queue<TcpClient>();
        public static HslCommunication.ModBus.ModbusTcpServer InvokeModebusTCPServer = new HslCommunication.ModBus.ModbusTcpServer();
        public static HPSQLService dataBaseSQL = new HPSQLService();

        public static Thread threadAllMonitor = null;

        public static bool bEnableRFID = true;
        public static bool bRFIDisConnected = false;
        public static bool bIsIDReaded = false;

        #region>>>>>>工位对像定义
        public static frm_LoadStation panel_Load = new frm_LoadStation();
        public static frm_FlashStation panel_Flash1 = new frm_FlashStation(HPglobal.strComport_烧录1烧录机, 1); 
        public static frm_FlashStation panel_Flash2 = new frm_FlashStation(HPglobal.strComport_烧录2烧录机, 2);
        public static frm_LaserStation panel_Laser = new frm_LaserStation();
        public static frm_UnloadStation panel_RobotUnload = new frm_UnloadStation();
        public static frm_PackingStation panel_Packing = new frm_PackingStation();

        public static myStationForm LoadStation = new myStationForm("上料工位", panel_Load);
        public static myStationForm Flash1Station = new myStationForm("烧录1工位", panel_Flash1);
        public static myStationForm Flash2Station = new myStationForm("烧录2工位", panel_Flash2);
        public static myStationForm LaserStation = new myStationForm("打码切割工位", panel_Laser);
        public static myStationForm UnloadStation = new myStationForm("下料组盘工位", panel_RobotUnload);
        public static myStationForm PackingStation = new myStationForm("包装工位", panel_Packing);

        public static TcpClient PC_LoadClient = null;
        public static TcpClient PC_FlashClient = null;
        public static TcpClient PC_RobotClient = null;
        public static TcpClient PC_PackingProcess = null;

        public static TcpClient FlashMachineServer1 = null;
        public static TcpClient FlashMachineServer2 = null;

        public static PC_ModeBusNet PCServer_Robot = new PC_ModeBusNet();

        public static HslCommunication.ModBus.ModbusRtu comRFID= new HslCommunication.ModBus.ModbusRtu();

        public static bool IsLoadStationReady = false;
        public static bool IsFlash1StationReady = false;
        public static bool IsFlash2StationReady = false;
        public static bool IsLaserStationReady = false;
        public static bool IsUnLoadStationReady = false;
        public static bool IsPackingStationReady = false;
        public static bool IsRobotProgramReady = false;

        public static Int16[] SafeMonitorArry1 = null;
        public static Int16[] SafeMonitorArry2 = null;
        public static Int16[] SafeMonitorArry3 = null;
        public static Int16[] SafeMonitorArry4 = null;
        public static Int16[] SafeMonitorArry5 = null;
        public static Int16[] SafeMonitorArry6 = null;
        public static Int16[] SafeMonitorArry7 = null;

        public static Int16[] ProductInfoData_Load = new Int16[60];
        public static Int16[] ProductInfoData_Flash1 = new Int16[60];
        public static Int16[] ProductInfoData_Flash2 = new Int16[60];
        public static Int16[] ProductInfoData_LaserCut = new Int16[60];
        public static Int16[] ProductInfoData_Unload = new Int16[60];
        public static Int16[] ProductInfoData_Packing = new Int16[60];

        public static bool bForceOK_PackingSN = true;

        public static bool bRealUpdataWorkSheet = false;
        public static int PackingCount = 0;

        public static bool bEnableLastModeInvode = true;

        //public static int PlanProductCount = 0; //计划产量（个数 PLC 内部，由工单控制), 如果有坏品会自动递增
        public static int ActuralProductedCount = 0;

        public static int PlanTrayCount_AutoCal = 0;  //计划产量（盘数), 如果有坏品会自动递增
        public static int ActureTrayCountOfLoaded = 0; // 当前打码工位已接受到的产量(盘数), 如果与计划产量相等，就停止接受

        public static bool bLastProductMode = false;
        public static string strLastTryaID_ForLastProductMode = "";
        public static int countRemaind_ForLastTray = 0;

        public static int LaserRemaind_ForLaserStation = 0; //整个工单过程中Laser工位还余下多少没有打码和切割的
        public static Int16[] lastModeFlag = new Int16[100];

        public static bool CurrentPlaneProductCompleted = false;

        public static Queue<string> m_queUnloadPCBs = new Queue<string>();

        #endregion

        public static ProcessSaveFile ObjAllParameters = new ProcessSaveFile();

        public struct struct_ConnectInfo
        {
            public string strIPAddress;
            public int nPort;
        };

        public struct strThreadOjb
        {
            public string strResourceName;
            public string strTrayID;
/*            public OmronFinsNet PLC;*/
            public string PLCResultAddr;
            public string PLCFinishedAddr;
            public bool bResult;
            public long dwErrorCode;
            public string strOutputMes;
        };

        public struct BarcodeRequireEvent
        {
            public string StationName;
            public TcpClient BarcodeScanner;
            public PLC_Panasonic DevicePLC;
            public PC_ModeBusNet DevicePC;
            public bool Responed;
            public bool ErrorCode;
            public string ErrorMessage;
        }

        public static bool Machine_Initialize()
        {
            bMachineIsInitialized = true;
            try
            {
                #region>>>>>>启动Socket服务
                /*                invodeSocketServer = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Parse("192.168.1.232"), 5218);*/
                System.Net.IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 5218);
                invodeSocketServer = new System.Net.Sockets.TcpListener(endPoint);

                if(invodeSocketServer != null)
                {
                    invodeSocketServer.Server.LingerState = new LingerOption(true, 10);
                    invodeSocketServer.Server.ReceiveBufferSize = 8192;
                    invodeSocketServer.Start();
                }
                else
                {
                    bMachineIsInitialized = false;
                    HPglobal.RecordeAllMessage($"创建服务器失败: 端口: {5218})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                }


                //接立连接线程
                Thread threadClientConnect = new Thread(new ParameterizedThreadStart(StationClient_OnConnect));
                threadClientConnect.Start(invodeSocketServer);
                Thread.Sleep(300);
                #endregion

                #region>>>>>>>> 初始化Modbus Server
                InvokeModebusTCPServer.Port = 5219;
                InvokeModebusTCPServer.ServerStart(InvokeModebusTCPServer.Port);
                if (!InvokeModebusTCPServer.IsStarted)
                {
                    HPglobal.RecordeAllMessage("初始化Modbus Server失败", true, false, "");
                    bMachineIsInitialized = false;
                }
                else
                {
                    #region>>>>>>>> 启动数据监控
                    HPglobal.RecordeAllMessage("初始化Modbus Server成功", true, false, "");
                    threadAllMonitor = new Thread(new ParameterizedThreadStart(thread_AllMonitor));
                    threadAllMonitor.Start(null);
                    #endregion
                }
                #endregion

                #region>>>>>>连接所有PLC 或 PCModbus
                //============================连接上料PLC==========================
                Task<bool> task0 = Task.Factory.StartNew<bool>(() =>
               {
                   try
                   {
                       return LoadStation.ConnectPLC(HPglobal.strIPAddress_上料PLC, HPglobal.nPortOfAllDevice);
                   }
                   catch (Exception ex)
                   {
                       HPglobal.RecordeAllMessage("连接上料PLC 初始化异常", true, false, "");
                       return false;
                   }

               });

                //============================连接烧录1PLC==========================
                Task<bool> task1 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        return Flash1Station.ConnectPLC(HPglobal.strIPAddress_烧录1PLC, HPglobal.nPortOfAllDevice);
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接烧录1PLC 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接烧录2PLC==========================
                Task<bool> task2 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        return Flash2Station.ConnectPLC(HPglobal.strIPAddress_烧录2PLC, HPglobal.nPortOfAllDevice);
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接烧录2PLC 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接打码切割PC==========================
                Task<bool> task3 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        return LaserStation.ConnectPCModbus(HPglobal.strIPAddress_打码切割主控PC, HPglobal.nPortOfAllDevice, 2);
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接打码切割PC 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接下料PLC==========================
                Task<bool> task4 = Task.Factory.StartNew<bool>(() =>
                 {
                     try
                     {
                         return UnloadStation.ConnectPLC(HPglobal.strIPAddress_下料PLC, HPglobal.nPortOfAllDevice);
                     }
                     catch (Exception ex)
                     {
                         HPglobal.RecordeAllMessage("连接下料PLC 初始化异常", true, false, "");
                         return false;
                     }

                 });

                //============================连接包装PLC==========================
                Task<bool> task5 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        return PackingStation.ConnectPLC(HPglobal.strIPAddress_包装PLC, HPglobal.nPortOfAllDevice);
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接包装PLC 初始化异常", true, false, "");
                        return false;
                    }

                });

                #endregion

                #region>>>>>>连接所有条码枪
                //============================连接上料条码枪==========================
                Task<bool> task6 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        return LoadStation.stationBase.ConnectScanner1(HPglobal.strIPAddress_上料条码枪, HPglobal.nPortOfAllDevice, 2000);
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接上料条码枪 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接烧录1条码枪==========================
                Task<bool> task7 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        return Flash1Station.stationBase.ConnectScanner1(HPglobal.strIPAddress_烧录1条码枪, HPglobal.nPortOfAllDevice, 2000);
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接烧录1条码枪 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接烧录2条码枪==========================
                Task<bool> task8 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        return Flash2Station.stationBase.ConnectScanner1(HPglobal.strIPAddress_烧录2条码枪, HPglobal.nPortOfAllDevice, 2000);
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接烧录2条码枪 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接打码切割条码枪==========================
                Task<bool> task9 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        bool result1 = LaserStation.stationBase.ConnectScanner1(HPglobal.strIPAddress_打码切割条码枪1, HPglobal.nPortOfAllDevice, 2000);

                        bool result2 = LaserStation.stationBase.ConnectScanner2(HPglobal.strIPAddress_打码切割条码枪2, HPglobal.nPortOfAllDevice, 2000);

                        return result1 & result2;
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接打码切割条码枪 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接下料PLC==========================
                Task<bool> task10 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        bool result1 = UnloadStation.stationBase.ConnectScanner1(HPglobal.strIPAddress_下料条码枪1, HPglobal.nPortOfAllDevice, 2000);

                        bool result2 = UnloadStation.stationBase.ConnectScanner2(HPglobal.strIPAddress_下料条码枪2, HPglobal.nPortOfAllDevice, 2000);

                        return result1 & result2;
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接下料PLC 初始化异常", true, false, "");
                        return false;
                    }

                });

                //============================连接包装条码枪==========================
                Task<bool> task11 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        bool result1 = PackingStation.stationBase.ConnectScanner1(HPglobal.strIPAddress_包装条码枪, HPglobal.nPortOfAllDevice, 2000);

                        bool result2 = PackingStation.stationBase.ConnectScanner2(HPglobal.strIPAddress_包装条码枪, HPglobal.nPortOfAllDevice, 2000);

                        return result1 & result2;
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接包装条码枪 初始化异常", true, false, "");
                        return false;
                    }


                });

                #endregion

                #region>>>>>>连接烧录机

                Task<bool> task12 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        FlashMachineServer1 = TimeOutSocket.Connect(new IPEndPoint(IPAddress.Parse(HPglobal.strIPAddress_烧录1烧录机), 5000), 2000);
                        if (FlashMachineServer1 == null)
                        {
                            HPglobal.RecordeAllMessage($"连接烧录机1失败( 连接端口: {HPglobal.strIPAddress_烧录1烧录机}:{5000})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                            return false;
                        }

                        HPglobal.RecordeAllMessage($"连接烧录机1成功", true, true, "");
                        return true;
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接烧录机1 初始化异常", true, false, "");
                        return false;
                    }

                });

                Task<bool> task13 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        FlashMachineServer2 = TimeOutSocket.Connect(new IPEndPoint(IPAddress.Parse(HPglobal.strIPAddress_烧录2烧录机), 5000), 2000);
                        if (FlashMachineServer2 == null)
                        {
                            HPglobal.RecordeAllMessage($"连接烧录机2失败( 连接端口: {HPglobal.strIPAddress_烧录2烧录机}:{5000})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                            return false;
                        }

                        HPglobal.RecordeAllMessage($"连接烧录机2成功", true, true, "");
                        return true;
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接烧录机2 初始化异常", true, false, "");
                        return false;
                    }

                });

                #endregion

                #region>>>>>>连接所有PC
                //============================连接Packing PC==========================
                Task<bool> task14 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        OperateResult result = PCServer_Robot.ConnectModebusTCPServer(HPglobal.strIPAddress_下料PC, HPglobal.nPortOfAllDevice, 2);
                        if (!result.IsSuccess)
                            HPglobal.RecordeAllMessage($"Connect 下料组盘电脑失败 连接端口: {HPglobal.strIPAddress_下料PC}:{HPglobal.nPortOfAllDevice})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                        else
                            HPglobal.RecordeAllMessage($"Connect 下料组盘电脑成功", true, true, "");

                        return result.IsSuccess;
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage($"连接Packing PC 初始化异常:{ex.Message}", true, false, "");
                        return false;
                    }


                });


                #endregion

                #region >>>>>>>>>>>>>连接数据库
                Task<bool> task15 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        dataBaseSQL.IpAddress = ObjAllParameters.dataBaseSQL_IpAddress;// "JONHA-HP\\SQLEXPRESS"; // "192.168.1.232";
                        dataBaseSQL.Port = ObjAllParameters.dataBaseSQL_Port; // 1433;
                        dataBaseSQL.User = ObjAllParameters.dataBaseSQL_User; // "sa";
                        dataBaseSQL.Database = ObjAllParameters.dataBaseSQL_Database; // "HPDatabase";
                        dataBaseSQL.Password = ObjAllParameters.dataBaseSQL_Password; // "123456";
                        if (!dataBaseSQL.Connect())
                        {
                            HPglobal.RecordeAllMessage($"连接MySQL数据库失败: {dataBaseSQL.IpAddress}:{1433})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                            return false;
                        }

                        HPglobal.RecordeAllMessage($"连接MySQL数据库成功", true, true, "");

                        return true;
                    }
                    catch(Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接数据库 初始化异常", true, false, "");
                        return false;
                    }
                });

                #endregion

                #region >>>>>>>>>>>>>连接RFID
                Task<bool> task16 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        //CommunicationZH.HPRFIDRead.gConnReader("com7") != 1
                        HPInvoke.comRFID.SerialPortInni("com7", 19200, 8, StopBits.One, Parity.None);
                        HPInvoke.comRFID.Station = 2;
                        HPInvoke.comRFID.Open();

                        bRFIDisConnected = true;
                        bIsIDReaded = false;

                        Thread idMonitor = new Thread(new ParameterizedThreadStart(thread_IDMonitorV2));
                        idMonitor.Start(null);
                        HPglobal.RecordeAllMessage($"连接RFID成功", true, true, "");
                        return true;
                    }
                    catch(Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接RFID(com7) 初始化异常", true, false, "");
                        return false;
                    }
                });

                #endregion

                #region >>>>>>>>>>>>>连接工艺处理服务程序
                Task<bool> task17 = Task.Factory.StartNew<bool>(() =>
                {
                    try
                    {
                        PC_PackingProcess = TimeOutSocket.Connect(new IPEndPoint(IPAddress.Parse(HPglobal.strIPAddress_包装PC), 5230), 2000);
                        if (PC_PackingProcess == null)
                        {
                            HPglobal.RecordeAllMessage($"连接工艺处理服务程序: {HPglobal.strIPAddress_包装PC}:{5230})", true, true, "");
                            return false;
                        }

                        HPglobal.RecordeAllMessage($"连接工艺处理服务程序成功", true, true, "");
                        return true;
                    }
                    catch (Exception ex)
                    {
                        HPglobal.RecordeAllMessage("连接RFID 初始化异常", true, false, "");
                        return false;
                    }
                });

                #endregion

                #region>>>>>>确认所有结果并初始化每个工位
                Task[] arryTask = { task0, task1, task2, task3, task4, task5, task6, task7, task8, task9, task10, task11, task12, task13, task14, task15, task16, task17};
                Task.WaitAll(arryTask, 10000);
                bool allResult = task0.Result & task1.Result & task2.Result & task3.Result & task4.Result & task5.Result &
                            task6.Result & task7.Result & task8.Result & task9.Result & task10.Result & task11.Result &
                            task12.Result & task13.Result & task14.Result & task15.Result & task16.Result & task17.Result;

                //建立线程
                bool binitResult = LoadStation.Intialize();
                bool binitResult2 = Flash1Station.Intialize();
                bool binitResult3 = Flash2Station.Intialize();
                bool binitResult4 = LaserStation.Intialize();
                bool binitResult5 = UnloadStation.Intialize();
                bool binitResult6 = PackingStation.Intialize();

                allResult = allResult & binitResult & binitResult2 & binitResult3 & binitResult4 & binitResult5 & binitResult6 ;
                #endregion

                //设置初始化标专
                panel_Flash1.PLCServer = Flash1Station.stationBase.PLCServer;
                panel_Flash2.PLCServer = Flash2Station.stationBase.PLCServer;

                Flash1Station.stationBase.bEnbalScanner2 = false;
                Flash2Station.stationBase.bEnbalScanner2 = false;

                bMachineIsInitialized = allResult;

            }
            catch (Exception ex)
            {
                bMachineIsInitialized = false;
                HPglobal.m_strLastException = string.Format("Machine_Initialize 失败\r\n{0}", ex.ToString());
                throw ex;
            }

            return bMachineIsInitialized;
        }

        public static bool Machine_Reset()
        {
            try
            {
                Task<bool> task1 = Task.Factory.StartNew<bool>(() => { return LoadStation.Reset(); });
                Task<bool> task2 = Task.Factory.StartNew<bool>(() => { return Flash1Station.Reset(); });
                Task<bool> task3 = Task.Factory.StartNew<bool>(() => { return Flash2Station.Reset(); });
                Task<bool> task4 = Task.Factory.StartNew<bool>(() => { return LaserStation.Reset(); });
                Task<bool> task5 = Task.Factory.StartNew<bool>(() =>
                {
                    bool result1 = panel_RobotUnload.Reset();
                    bool result2 = UnloadStation.Reset();
                    return (result1 & result2);
                });
                Task<bool> task6 = Task.Factory.StartNew<bool>(() => { return PackingStation.Reset(); });

                Task[] arryTask = {  task1, task2, task3, task4, task5, task6 };
                bool bResult = Task.WaitAll(arryTask, 120000);

                bool allResult = task1.Result & task2.Result & task3.Result & task4.Result & task5.Result & task6.Result;
                if(allResult)
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Idle;
                else
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_NotReset;

                return allResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Machine_Reset 失败\r\n{0}", ex.ToString());
            }

            return false;
        }

        public static bool Machine_IsReady()
        {
            try
            {
                if (LoadStation.stationBase.bEnabled)
                    IsLoadStationReady = LoadStation.IsReady();
                else
                    IsLoadStationReady = true;

                if (Flash1Station.stationBase.bEnabled)
                    IsFlash1StationReady = Flash1Station.IsReady();
                else
                    IsFlash1StationReady = true;

                if (Flash2Station.stationBase.bEnabled)
                    IsFlash2StationReady = Flash2Station.IsReady();
                else
                    IsFlash2StationReady = true;

                if (LaserStation.stationBase.bEnabled)
                    IsLaserStationReady = LaserStation.IsReady();
                else
                    IsLaserStationReady = true;

                if (UnloadStation.stationBase.bEnabled)
                    IsUnLoadStationReady = UnloadStation.IsReady();
                else
                    IsUnLoadStationReady = true;

                if (PackingStation.stationBase.bEnabled)
                    IsPackingStationReady = PackingStation.IsReady();
                else
                    IsPackingStationReady = true;

                IsRobotProgramReady = panel_RobotUnload.IsReady();

                return (IsLoadStationReady  & IsFlash1StationReady & IsFlash2StationReady & IsLaserStationReady & IsUnLoadStationReady & IsPackingStationReady & IsRobotProgramReady);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Machine_Reset 失败\r\n{0}", ex.ToString());
            }

            return false;
        }

        public static bool Machine_Start()
        {
            try
            {
                //确认所有设备是否处于准备状诚
                if(!Machine_IsReady())
                {
                    MessageBox.Show("某个设备没有准备信号，无法启动！\n请检查各设备是否在准备当中！", "确认", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                //建立逻辑调度线程
                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Starting;
                Task<bool> task1 = Task.Factory.StartNew<bool>(() => { return LoadStation.Start(); });
                Task<bool> task2 = Task.Factory.StartNew<bool>(() => { return Flash1Station.Start(); });
                Task<bool> task3 = Task.Factory.StartNew<bool>(() => { return Flash2Station.Start(); });
                Task<bool> task4 = Task.Factory.StartNew<bool>(() => { return LaserStation.Start(); });
                Task<bool> task5 = Task.Factory.StartNew<bool>(() => { return UnloadStation.Start(); });
                Task<bool> task6 = Task.Factory.StartNew<bool>(() => { return PackingStation.Start(); });
                Task<bool> task7 = Task.Factory.StartNew<bool>(() => { return panel_RobotUnload.Start(); });

                Task[] arryTask = { task1, task2, task3, task4, task5, task6, task7 };
                bool bResult = Task.WaitAll(arryTask, 120000);

                bool allResult = task1.Result & task2.Result & task3.Result & task4.Result & task5.Result & task6.Result & task7.Result;
                if (allResult)
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Working;
                else
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_NotReset;

                return allResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Machine_Start 失败\r\n{0}", ex.ToString());
            }

            return false;
        }

        public static bool Machine_Stop()
        {
            try
            {
                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Stopping;

                Task<bool> task1 = Task.Factory.StartNew<bool>(() => { return LoadStation.Stop(); });
                Task<bool> task2 = Task.Factory.StartNew<bool>(() => { return Flash1Station.Stop(); });
                Task<bool> task3 = Task.Factory.StartNew<bool>(() => { return Flash2Station.Stop(); });
                Task<bool> task4 = Task.Factory.StartNew<bool>(() => { return LaserStation.Stop(); });
                Task<bool> task5 = Task.Factory.StartNew<bool>(() => { return UnloadStation.Stop(); });
                Task<bool> task6 = Task.Factory.StartNew<bool>(() => { return PackingStation.Stop(); });
                Task<bool> task7 = Task.Factory.StartNew<bool>(() => { return panel_RobotUnload.Stop(); });

                Task[] arryTask = { task1, task2, task3, task4, task5, task6, task7 };
                bool bResult = Task.WaitAll(arryTask, 120000);

                bool allResult = task1.Result & task2.Result & task3.Result & task4.Result & task5.Result & task6.Result & task7.Result;
                if (allResult)
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Idle;
                else
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_NotReset;

                return allResult;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Machine_Start 失败\r\n{0}", ex.ToString());
            }

            return false;
        }

        private static void StationClient_OnConnect(object wsarg)
        {
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(200);

                try
                {
                    if (invodeSocketServer == null)
                        return;

                    if (!invodeSocketServer.Pending())
                        continue;

                    TcpClient tcpClient = invodeSocketServer.AcceptTcpClient();
                    if (tcpClient != null)
                    {
                        //得到连接Client信息
                        String strIP = tcpClient.Client.LocalEndPoint.ToString();
                        strIP = tcpClient.Client.RemoteEndPoint.ToString();

                        //等待发命工位首个命令过来
                        DateTime time1, time2;
                        time1 = time2 = DateTime.Now;
                        while (true)
                        {
                            Thread.Sleep(200);
                            if (HPglobal.m_bQuitApp)
                                return;

                            time2 = DateTime.Now;
                            if ((time2 - time1).TotalSeconds > 5)
                            {
                                HPglobal.RecordeAllMessage($"客户端{tcpClient.Client.RemoteEndPoint.ToString()}没有在5s以内发送工站信息过来，将自动断开连接", true, false, "数据需要以\r\n (CRLF) 作为结束符");
                                break;
                            }

                            string[] parameters = null;
                            string requirement = HPglobal.Server_GetClient_Requirement(tcpClient, ref parameters);
                            if (requirement != null && requirement.Contains("Station:1"))
                            {
                                m_queTcpClient.Enqueue(tcpClient);
                                panel_Load.SetClient(tcpClient);
                                break;
                            }
                            else
                                continue;
                        }

                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = "Client_OnConnect 异常:\r\n" + ex.Message;
                    return;
                }
            }

            //断开连接
            try
            {
                invodeSocketServer.Server.Close();
                /*                addMessageOutput("Client_OnConnect 服务已经终止，请重新启动", "", "");*/
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = "Client_OnConnect 异常(Close):\r\n" + ex.Message;
                return;
            }
        }

        public static void QuitApplicationAndClear()
        {
            try
            {
                #region>>>>>>>>>停止所有服务及关闭仪器
                if (invodeSocketServer != null)
                    invodeSocketServer.Server.Close();

                if (FlashMachineServer1 != null)
                    FlashMachineServer1.Client.Close();

                if (FlashMachineServer2 != null)
                    FlashMachineServer2.Client.Close();

                #endregion

                #region>>>>>>>>>释放全局变量及对话框
                panel_Load.Dispose();
                panel_Flash1.Dispose();
                panel_Flash2.Dispose();
                panel_Laser.Dispose();
                panel_RobotUnload.Dispose();
                panel_Packing.Dispose();
                LoadStation.Dispose();
                Flash1Station.Dispose();
                Flash2Station.Dispose();
                LaserStation.Dispose();
                UnloadStation.Dispose();
                PackingStation.Dispose();
                #endregion

                CommunicationZH.HPRFIDRead.gDiscReader();


            }
            catch (Exception ex)
            {

            }
        }

        public static bool Function_ReadBarcode(TcpClient clientScanner, ref string strRead)
        {
            strRead = "";
            int ntrayNumber = 3;
            Byte[] trigger = {0x16, 0x54, 0x0D };
            Byte[] stop = { 0x16, 0x55, 0x0D };
            try
            {
                RETRAY_READ:
                for (int nIndex = 0; nIndex < ntrayNumber; nIndex++)
                {
                    byte[] bytesBuff = null;

                    //清除原来的数据；
                    int nLen = clientScanner.Client.Available;
                    if (nLen > 0)
                    {
                        bytesBuff = new byte[nLen];
                        nLen = clientScanner.Client.Receive(bytesBuff, nLen, SocketFlags.None);
                    }

                    //触发条码枪
                    if (clientScanner.Client.Send(trigger) < 0)
                        return false;


                    DateTime time1, time2;
                    time1 = time2 = DateTime.Now;
                    while (!HPglobal.m_bQuitApp)
                    {
                        Thread.Sleep(500);
                        Application.DoEvents();

                        if (clientScanner.Client.Available >= 3)
                        {
                            // clientScanner.Client.Receive(bytesBuff, nLen, SocketFlags.Peek);
                            Thread.Sleep(200);
                            nLen = clientScanner.Client.Available;

                            bytesBuff = null;
                            bytesBuff = new byte[nLen];

                            nLen = clientScanner.Client.Receive(bytesBuff, nLen, SocketFlags.None);
                            string strTemp = "\r\n";
                            strRead = System.Text.Encoding.Default.GetString(bytesBuff).Trim();
                            strRead = strRead.Replace("\0", "");
                            strRead = strRead.Replace("\r\n", "");
                            if (strRead.Contains("ERROR") || strRead.Contains("Error") || strRead.Contains("error"))
                            {
                                strRead = "";
                                HPglobal.RecordeAllMessage($"{ (clientScanner.Client.LocalEndPoint as IPEndPoint).Address.ToString() }读取条码失败!" , true, true, "检查PLC电源是否开启或线路是否正常及IP配制");
                                Thread.Sleep(1500);
                                clientScanner.Client.Send(stop);
                                break;
                            }

                            clientScanner.Client.Send(stop);
                            return true;
                        }

                        time2 = DateTime.Now;
                        TimeSpan timeSpan = time2 - time1;
                        if (timeSpan.TotalSeconds > 2.2)
                        {
                            clientScanner.Client.Send(stop);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strRead = "";
                HPglobal.m_strLastException = string.Format("CognexBarcodeRead_Read() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
            }

            clientScanner.Client.Send(stop);
            return false;
        }

        public static short Function_MESCheckLastResult(string strCurrentStation, int idScanner, string strTrayID, string strPCBSN)
        {
            APK_HP.MES.HPSql.WorkStationNameEnum emStation;

            if (HPInvoke.ObjAllParameters.bForceMesCheckOK)
                return 1;

            try
            {
                if (strCurrentStation == "上料工位")
                    emStation = WorkStationNameEnum.上料工站;

                else if (strCurrentStation == "烧录1工位" || strCurrentStation == "烧录2工位")
                    emStation = WorkStationNameEnum.上料工站;

                else if (strCurrentStation == "打码切割工位" && idScanner == 1)
                    emStation = WorkStationNameEnum.烧录工站;

                else if (strCurrentStation == "打码切割工位" && idScanner == 2)
                    emStation = WorkStationNameEnum.打码工站;

                else if (strCurrentStation == "下料组盘工位" && idScanner == 1)
                    emStation = WorkStationNameEnum.切割工站;

                else if (strCurrentStation == "下料组盘工位" && idScanner == 2)
                    emStation = WorkStationNameEnum.顶部测试工站;

                else if (strCurrentStation == "包装工位")
                    emStation = WorkStationNameEnum.底部测试工站; //底部测试工站

                else
                    return 2;

                bool result = HPInvoke.dataBaseSQL.SQL_GetStationResultStatusFromTray(strTrayID, emStation);

                ResultInformation resultList = null;
                HPInvoke.dataBaseSQL.SQL_GetStationResultFromTray(strTrayID, emStation, out resultList);
                if (resultList == null || resultList.Results[0].Result.Length <= 0)
                    result = false;

                if (strCurrentStation == "烧录2工位")
               {
                    if (result == false)
                        return 2;

                    result = HPInvoke.dataBaseSQL.SQL_GetStationResultStatusFromTray(strTrayID, WorkStationNameEnum.烧录工站);
                    resultList = null;
                    HPInvoke.dataBaseSQL.SQL_GetStationResultFromTray(strTrayID, WorkStationNameEnum.烧录工站, out resultList);
                    if (resultList == null || resultList.Results[0].Result.Length <= 0)
                        result = false;

                    return (result ? (short) 3 : (short) 1);
                }
                else
                    return (result ? (short) 1 : (short) 2);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Function_MESCheckLastResult 失败\r\n{0}", ex.ToString());
            }

            return 2;
        }

        public static void thread_AllMonitor(object wsarg)
        {
            string trigAddress = "D1200";
            string completedAddress = "D1201";
            string resultAddress = "D1202";
            string errorMsg = "";
            emActionHandle actionHandle_result;
            string str1or2 = (string)wsarg;

            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(500);
                try
                {
                    #region>>>>>>读取每个工站的监控数据
                    SafeMonitorArry1 = LoadStation.stationBase.ReadInt16Arry("D1006", 2);
                    SafeMonitorArry2 = Flash1Station.stationBase.ReadInt16Arry("D1006", 2);
                    SafeMonitorArry3 = Flash2Station.stationBase.ReadInt16Arry("D1006", 2);
                    SafeMonitorArry4 = LaserStation.stationBase.ReadInt16Arry("D1006", 2);
                    SafeMonitorArry5 = UnloadStation.stationBase.ReadInt16Arry("D1006", 2);
                    SafeMonitorArry6 = PackingStation.stationBase.ReadInt16Arry("D1006", 2);
                    SafeMonitorArry7 = PCServer_Robot.ReadInt16Arry("1006", 2);

                    #endregion

                    #region>>>>>>将数据写入总调度让监控软件读取并产生相应的监控视频
                    if (SafeMonitorArry1 != null && SafeMonitorArry2 != null && SafeMonitorArry3 != null &&
                         SafeMonitorArry4 != null && SafeMonitorArry5 != null && SafeMonitorArry6 != null && SafeMonitorArry7 != null)
                    {
                        Int16[] values = SafeMonitorArry1.Concat(SafeMonitorArry2).Concat(SafeMonitorArry3).Concat(SafeMonitorArry4).Concat(SafeMonitorArry5).Concat(SafeMonitorArry6).Concat(SafeMonitorArry7).ToArray();
                        InvokeModebusTCPServer.Write("2001", values);
                    }
                    else
                    {
                        if (SafeMonitorArry1 != null)
                            InvokeModebusTCPServer.Write("2001",  SafeMonitorArry1);
                        else
                            HPglobal.RecordeAllMessage($"thread_AllMonitor 读取上料监控失败", true, true, "检查PLC电源是否开启或线路是否正常及IP配制");
                        //==================================================================
                        if ( SafeMonitorArry2 != null)
                            InvokeModebusTCPServer.Write("2003",  SafeMonitorArry2);
                        else
                            HPglobal.RecordeAllMessage($"thread_AllMonitor 读取烧录1监控失败", true, true, "检查PLC电源是否开启或线路是否正常及IP配制");
                        //==================================================================
                        if ( SafeMonitorArry3 != null)
                            InvokeModebusTCPServer.Write("2005",  SafeMonitorArry3);
                        else
                            HPglobal.RecordeAllMessage($"thread_AllMonitor 读取烧录2监控失败", true, true, "检查PLC电源是否开启或线路是否正常及IP配制");
                        //==================================================================
                        if ( SafeMonitorArry4 != null)
                            InvokeModebusTCPServer.Write("2007",  SafeMonitorArry4);
                        else
                            HPglobal.RecordeAllMessage($"thread_AllMonitor 读取打码监控失败", true, true, "检查PLC电源是否开启或线路是否正常及IP配制");
                        //==================================================================
                        if ( SafeMonitorArry5 != null)
                            InvokeModebusTCPServer.Write("2009",  SafeMonitorArry5);
                        else
                            HPglobal.RecordeAllMessage($"thread_AllMonitor 读取下料监控失败", true, true, "检查PLC电源是否开启或线路是否正常及IP配制");
                        //==================================================================
                        if ( SafeMonitorArry6 != null)
                            InvokeModebusTCPServer.Write("2011",  SafeMonitorArry6);
                        else
                            HPglobal.RecordeAllMessage($"thread_AllMonitor 读取包装监控失败", true, true, "检查PLC电源是否开启或线路是否正常及IP配制");
                        //==================================================================
                        if ( SafeMonitorArry7 != null)
                            InvokeModebusTCPServer.Write("2013",  SafeMonitorArry7);
                        else
                            HPglobal.RecordeAllMessage($"thread_AllMonitor 读取机器人监控失败", true, true, "检查PLC电源是否开启或线路是否正常及IP配制");

                    }

                    #endregion

                    #region>>>>>>实时定入工单信息
                    if (frm_MainPanel != null && bRealUpdataWorkSheet)
                        frm_MainPanel.WriteOrderInfoToProductionLine();
                    #endregion

                    #region>>>>>>读取已下料的托盘信息
                    HPInvoke.ObjAllParameters.UnloadPCBs = ReadUnloaedPCBID_FromRobotPLC();
                    #endregion

                    #region>>>>>>实时读取生产信息
                    if (LoadStation.stationBase.IsDeviceConnected)
                        ProductInfoData_Load = LoadStation.stationBase.ReadInt16Arry("D1030", 60);

                    if (Flash1Station.stationBase.IsDeviceConnected)
                        ProductInfoData_Flash1 = Flash1Station.stationBase.ReadInt16Arry("D1030", 60);

                    if (Flash2Station.stationBase.IsDeviceConnected)
                        ProductInfoData_Flash2 = Flash2Station.stationBase.ReadInt16Arry("D1030", 60);

                    if (LaserStation.stationBase.IsDeviceConnected)
                        ProductInfoData_LaserCut = LaserStation.stationBase.ReadInt16Arry("D1030", 60);

                    if (UnloadStation.stationBase.IsDeviceConnected)
                        ProductInfoData_Unload = UnloadStation.stationBase.ReadInt16Arry("D1030", 60);

                    if (PackingStation.stationBase.IsDeviceConnected)
                        ProductInfoData_Packing = PackingStation.stationBase.ReadInt16Arry("D1030", 60);
                    #endregion

                    #region>>>>>>确认是否是尾料模式
                    if (bLastProductMode && ProductInfoData_Unload[25] != 1)
                    {
                        LoadStation.stationBase.WriteInt16("D1055", 1);
                        Flash1Station.stationBase.WriteInt16("D1055", 1);
                        Flash2Station.stationBase.WriteInt16("D1055", 1);
                        LaserStation.stationBase.WriteInt16("1055", 1);
                        UnloadStation.stationBase.WriteInt16("D1055", 1);
                        PackingStation.stationBase.WriteInt16("D1055", 1);
                    }
                    else if (!bLastProductMode && ProductInfoData_Unload[25] != 0)
                    {
                        LoadStation.stationBase.WriteInt16("D1055", 0);
                        Flash1Station.stationBase.WriteInt16("D1055", 0);
                        Flash2Station.stationBase.WriteInt16("D1055", 0);
                        LaserStation.stationBase.WriteInt16("1055", 0);
                        UnloadStation.stationBase.WriteInt16("D1055", 0);
                        PackingStation.stationBase.WriteInt16("D1055", 0);
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_AllMonitor  执行其间发生错误\r\n{0}", ex.ToString());
                }
            }
        }

        public static void thread_IDMonitor(object wsarg)
        {
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(1000);
                try
                {
                    byte[] reads = new byte[20];
                    CommunicationZH.HPRFIDRead.gSetLED((byte)CommunicationZH.HPRFIDRead.LEDDef.LED_OFF);
                    CommunicationZH.HPRFIDRead.gSetLED((byte)CommunicationZH.HPRFIDRead.LEDDef.LED_YELLOW);
                    short result = CommunicationZH.HPRFIDRead.gReadEM4100(reads);
                    if(result == 1)
                    {
                        bRFIDisConnected = true;
                        bIsIDReaded = true;
                        Int32 dwID = (reads[1] << 24) + (reads[2] << 16) + (reads[3] << 8) + reads[4];
                        HPglobal.m_strRFID = dwID.ToString("D010");
                        CommunicationZH.HPRFIDRead.gSetLED((byte)CommunicationZH.HPRFIDRead.LEDDef.LED_GREEN);
                        Thread.Sleep(500);
/*                        CommunicationZH.HPRFIDRead.gSetLED((byte)CommunicationZH.HPRFIDRead.LEDDef.LED_YELLOW);*/
                    }
                    else if (result == 2)
                    {
                        HPglobal.m_strRFID = "";
                        bRFIDisConnected = false;
                        bIsIDReaded = false;
                        CommunicationZH.HPRFIDRead.gSetLED((byte)CommunicationZH.HPRFIDRead.LEDDef.LED_RED);
                        Thread.Sleep(500);
/*                        CommunicationZH.HPRFIDRead.gSetLED((byte)CommunicationZH.HPRFIDRead.LEDDef.LED_YELLOW);*/
                    }
                    else if (result == 3)
                    {
                        HPglobal.m_strRFID = "";
                        bRFIDisConnected = false;
                        bIsIDReaded = false;
                        CommunicationZH.HPRFIDRead.gConnReader("com6");
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_AllMonitor  执行其间发生错误\r\n{0}", ex.ToString());
                }
            }
        }

        public static void thread_IDMonitorV2(object wsarg)
        {
            string strLastRFID = "";
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(1000);
                try
                {
                    OperateResult<UInt32> result = comRFID.ReadUInt32("0");
                    OperateResult<byte[]> reads = comRFID.Read("0", 4);
                    UInt32 dwID = (UInt32)(reads.Content[2] << 24) + (UInt32)(reads.Content[3] << 16) + (UInt32)(reads.Content[0] << 8) + (UInt32)reads.Content[1];
                    if (reads.IsSuccess && dwID > 0)
                    {
                        bRFIDisConnected = true;
                        bIsIDReaded = true;
                        HPglobal.m_strRFID = dwID.ToString("D010");
                        
                        if (HPglobal.m_strRFID.Length > 0 && strLastRFID != HPglobal.m_strRFID)
                        {
                            if(frm_MainPanel != null)
                            {
                                string group = "操作员";
                                dataBaseSQL.SQL_GetUserGroup(HPglobal.m_strRFID, out group);
                                frm_MainPanel.SetPrivilegy(group);
                            }
                        }

                        strLastRFID = HPglobal.m_strRFID;
                    }
                    else
                    {
                        if (frm_MainPanel != null && (HPglobal.m_strRFID.Length > 0 || strLastRFID.Length > 0))
                        {
                            strLastRFID = HPglobal.m_strRFID = "";
                            frm_MainPanel.SetPrivilegy("操作员");
                        }
                        strLastRFID = HPglobal.m_strRFID = "";
                        bRFIDisConnected = false;
                        bIsIDReaded = false;
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_AllMonitor  执行其间发生错误\r\n{0}", ex.ToString());
                }
            }
        }

        public static void ResetFlashAreaData(string sn, bool clearAll = false)
        {
            int strArea1Addr = 7000;
            int strArea2Addr = strArea1Addr + 16 + 100;
            int strArea3Addr = strArea2Addr + 16 + 100;
            int strArea4Addr = strArea3Addr + 16 + 100;
            int strArea5Addr = strArea4Addr + 16 + 100;
            int strArea6Addr = strArea5Addr + 16 + 100;
            int strArea7Addr = strArea6Addr + 16 + 100;
            int strArea8Addr = strArea7Addr + 16 + 100;

            if (sn == null || sn.Length <= 0)
                return;

            int[] nArryAddr = new int[] { strArea1Addr, strArea2Addr, strArea3Addr, strArea4Addr, strArea5Addr, strArea6Addr, strArea7Addr, strArea8Addr };
            try
            {
                for (int nIndex = 0; nIndex < nArryAddr.Length; nIndex++)
                {
                    int nAddr = nArryAddr[nIndex];
                    string strPN = UnloadStation.stationBase.ReadString($"D{nAddr}", 16);
                    if (clearAll ||
                       (!clearAll && (strPN != null && strPN.Length > 0 && strPN.Contains(sn))))
                    {
                        UnloadStation.stationBase.WriteString($"D{nAddr}", "Empty");

                        Int16[] resetData = new Int16[100];

                        for (int n = 0; n < 100; n++)
                            resetData[n] = 0xFF;

                        UnloadStation.stationBase.WriteInt16($"D{nAddr + 16}", resetData);
                    }
                }

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteFalshResultToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                throw new Exception(ex.Message);
            }
        }

        public static void ResetFlashAreaData(int areIndex, bool clearAll = false)
        {
            int strArea1Addr = 7000;
            int strArea2Addr = strArea1Addr + 16 + 100;
            int strArea3Addr = strArea2Addr + 16 + 100;
            int strArea4Addr = strArea3Addr + 16 + 100;
            int strArea5Addr = strArea4Addr + 16 + 100;
            int strArea6Addr = strArea5Addr + 16 + 100;
            int strArea7Addr = strArea6Addr + 16 + 100;
            int strArea8Addr = strArea7Addr + 16 + 100;

            if (areIndex < 0 || areIndex > 6)
                return;
            
            int[] nArryAddr = new int[] { strArea1Addr, strArea2Addr, strArea3Addr, strArea4Addr, strArea5Addr, strArea6Addr, strArea7Addr, strArea8Addr };
            try
            {
                for (int nIndex = 0; nIndex < nArryAddr.Length; nIndex++)
                {
                    int nAddr = nArryAddr[nIndex];
                    if(clearAll ||
                       (!clearAll && nIndex == areIndex))
                    {
                        UnloadStation.stationBase.WriteString($"D{nAddr}", "Empty");

                        Int16[] resetData = new Int16[100];

                        for (int n = 0; n < 100; n++)
                            resetData[n] = 0xFF;

                        UnloadStation.stationBase.WriteInt16($"D{nAddr + 16}", resetData);
                    }
                }

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteFalshResultToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                throw new Exception(ex.Message);
            }
        }

        public static void ResetAllUnloadPCBs()
        {
            int nLenOfAddr = 20;
            int[] arryAreaAddr = new int[20];
            for (int nIndex = 0; nIndex < nLenOfAddr; nIndex++)
            {
                arryAreaAddr[nIndex] = 8000 + nIndex * 16;
            }
            try
            {
                for (int nIndex = 0; nIndex < nLenOfAddr; nIndex++)
                {
                    UnloadStation.stationBase.WriteString($"D{arryAreaAddr[nIndex]}", "Empty");
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("ResetAllUnloadPCBs  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                throw new Exception(ex.Message);
            }
        }

        public static bool WriteFalshResultToRobotPLC(string SN, Int16[] result)
        {
            bool bWriteResult = false;
            int strArea1Addr = 7000;
            int strArea2Addr = strArea1Addr + 16 + 100;
            int strArea3Addr = strArea2Addr + 16 + 100;
            int strArea4Addr = strArea3Addr + 16 + 100;
            int strArea5Addr = strArea4Addr + 16 + 100;
            int strArea6Addr = strArea5Addr + 16 + 100;
            int strArea7Addr = strArea6Addr + 16 + 100;
            int strArea8Addr = strArea7Addr + 16 + 100;

            int[] nArryAddr = new int[]{ strArea1Addr , strArea2Addr, strArea3Addr, strArea4Addr, strArea5Addr, strArea6Addr, strArea7Addr, strArea8Addr };
            try
            {
                for(int nIndex = 0; nIndex < nArryAddr.Length; nIndex++)
                {
                    int nAddr = nArryAddr[nIndex];

                    string strPN = UnloadStation.stationBase.ReadString($"D{nAddr}", 16);
                    if((strPN != null && strPN.Length > 0 && strPN.Contains("Empty")) ||
                        (strPN != null && strPN.Length > 0 && strPN.Contains(SN)))
                    {
                        UnloadStation.stationBase.WriteString($"D{nAddr}", SN);
                        UnloadStation.stationBase.WriteInt16($"D{nAddr + 16}", result);
                        bWriteResult = true;
                        break;
                    }
                }

                if(!bWriteResult)
                {
                    HPglobal.m_strLastException = $"WriteFalshResultToRobotPLC 写入烧录结果到PLC失败! 载板编号: {SN}";
                    HPglobal.GlobalWorkingMessageOutput(HPglobal.m_strLastException, Color.Red, 9, 1);
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }

                return bWriteResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteFalshResultToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public static string[] ReadUnloaedPCBID_FromRobotPLC()
        {
            int nLenOfAddr = 20;
            int[] arryAreaAddr = new int[20];
            string[] arryString = null;

            for (int nIndex = 0; nIndex < nLenOfAddr; nIndex++)
            {
                arryAreaAddr[nIndex] = 8000 + nIndex * 16;
            }
            try
            {
                for (int nIndex = 0; nIndex < nLenOfAddr; nIndex++)
                {
                    string strPN = UnloadStation.stationBase.ReadString($"D{arryAreaAddr[nIndex]}", 16);
                    int n = strPN.IndexOf("\0");
                    strPN = strPN.Substring(0, n < 0 ? strPN.Length : strPN.IndexOf("\0"));
                    strPN = strPN.TrimEnd("\0".ToCharArray());
                    if ((strPN != null && strPN.Length > 0 && !strPN.Contains("Empty")))
                    {
                        if(arryString == null)
                        {
                            arryString = new string[1];
                            arryString[0] = strPN;
                        }
                        else
                        {
                            arryString = arryString.Concat(new string[1] { strPN }).ToArray();
                        }
                    }
                }

                return arryString;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("ReadUnloaedPCBID_FromRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return null;
            }
        }

        public static string GetWorkOrdeInfomation(string order, bool updateWorkOrder = true)
        {
            try
            {
                int nLen = PC_PackingProcess.Client.Available;
                byte[] bytesBuff = new byte[nLen];
                if(nLen > 0)
                    PC_PackingProcess.Client.Receive(bytesBuff, nLen, SocketFlags.None);

                if (PC_PackingProcess == null || PC_PackingProcess.Client == null ||
                    PC_PackingProcess.Client.Send(System.Text.Encoding.ASCII.GetBytes($"GetWorkOrder{order} \r\n")) <= 0)
                {
                    HPglobal.m_strLastException = $"GetWorkOrdeInfomation() 异常, 上料客户端没有连接或发送异常!";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                    return "";
                }

                string strEndPoint = PC_PackingProcess.Client.LocalEndPoint.ToString();
                string responed = "";
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (!HPglobal.m_bQuitApp)
                {
                    Thread.Sleep(500);
                    Application.DoEvents();
                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 15)
                    {
                        HPglobal.RecordeAllMessage($"客户端{strEndPoint}没有在5s以内发送工站信息过来，将自动断开连接", true, false, "数据需要以\r\n (CRLF) 作为结束符");
                        return "";
                    }

                    int nLen2 = PC_PackingProcess.Client.Available;
                    if (nLen2 == 0)
                        continue;

                    Thread.Sleep(500);
                    string[] Parameters = null;
                    nLen2 = PC_PackingProcess.Client.Available;
                    bytesBuff = new byte[nLen2];
                    //responed = HPglobal.Server_GetClient_Requirement(PC_PackingProcess, ref Parameters);
                    nLen = PC_PackingProcess.Client.Receive(bytesBuff, nLen2, SocketFlags.None);
                    responed = System.Text.Encoding.Unicode.GetString(bytesBuff);
                    //string recMsg = Encoding.GetEncoding("GB2312").GetString(responed, 0, responed.Length);
                    if (responed != null && responed.Length > 20 && !responed.Contains("Error"))
                    {
                        responed = responed.Replace("\r\n", "");
                        if(updateWorkOrder)
                        {
                            HPInvoke.ObjAllParameters.APKWorkOrderInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<HpWebService.HpMesParameter>(responed);
                        }
                        return responed;
                    }
                    else
                        continue;

                }

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("GetWorkOrdeInfomation() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");

            }

            return "";
        }

        public static bool PrintWrokOrderLable(string order, int packingCount)
        {
            try
            {
                int nLen = PC_PackingProcess.Client.Available;
                byte[] bytesBuff = new byte[nLen];
                if (nLen > 0)
                    PC_PackingProcess.Client.Receive(bytesBuff, nLen, SocketFlags.None);

                if (PC_PackingProcess == null || PC_PackingProcess.Client == null ||
                    PC_PackingProcess.Client.Send(System.Text.Encoding.ASCII.GetBytes($"Print{order};{packingCount} \r\n")) <= 0)
                {
                    HPglobal.m_strLastException = $"PrintWrokOrderLable() 异常, 上料客户端没有连接或发送异常!";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                    return false;
                }

                string strEndPoint = PC_PackingProcess.Client.LocalEndPoint.ToString();
                string responed = "";
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (!HPglobal.m_bQuitApp)
                {
                    Thread.Sleep(1500);
                    Application.DoEvents();
                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 15)
                    {
                        HPglobal.RecordeAllMessage($"客户端{strEndPoint}没有在5s以内发送工站信息过来，将自动断开连接", true, false, "数据需要以\r\n (CRLF) 作为结束符");
                        return false;
                    }

                    string[] Parameters = null;
                    responed = HPglobal.Server_GetClient_Requirement(PC_PackingProcess, ref Parameters);
                    if (responed != null && responed.Contains("OK"))
                    {
                        return true;
                    }
                    else
                        continue;

                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("PrintWrokOrderLable() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");

            }

            return false;
        }

        public static void SaveFlashDataModeToFile(string PCBName, bool mode)
        {
            string strFileName = $"FlashMode.xml";
            XmlNode root = null;
            try
            {
                XmlDocument xml = new XmlDocument();
                if(File.Exists(strFileName))
                {
                    xml.Load(strFileName);
                    root = xml.SelectSingleNode("PCBs");
                }
                else
                {
                    XmlDeclaration dec = xml.CreateXmlDeclaration("1.0", "utf-8", null);
                    xml.AppendChild(dec);

                    root = xml.CreateNode(XmlNodeType.Element, "PCBs", "");
                    xml.AppendChild(root);

                }

                if(root.SelectSingleNode(PCBName) == null)
                {
                    XmlNode nodeNewItem = xml.CreateNode(XmlNodeType.Element, PCBName, "");
                    XmlNode nodeFlashMode = xml.CreateNode(XmlNodeType.Element, "FlashMode", "");

                    nodeNewItem.AppendChild(nodeFlashMode);
                    root.AppendChild(nodeNewItem);

                }

                root[PCBName].SelectSingleNode("FlashMode").InnerText = mode ? "1" : "0";

                //写入到文件
                xml.Save(strFileName);

            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SaveFlashDataModeToFile() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
            }
        }

        public static bool GetFlashDataModeFromFile(string PCBName, out bool mode)
        {
            string strFileName = $"FlashMode.xml";
            XmlNode root = null;

            mode = false;

            try
            {
                if (!File.Exists(strFileName))
                    return false;

                XmlDocument xml = new XmlDocument();
                xml.Load(strFileName);

                if (xml.SelectSingleNode("PCBs") == null ||
                    xml.SelectSingleNode("PCBs").SelectSingleNode(PCBName) == null ||
                    xml.SelectSingleNode("PCBs").SelectSingleNode(PCBName).SelectSingleNode("FlashMode") == null)
                    return false;
                else
                {
                    string modeValue = (string)xml.SelectSingleNode("PCBs").SelectSingleNode(PCBName).SelectSingleNode("FlashMode").InnerText;
                    mode = (modeValue == "1" ? true : false);
                }

                return true;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("GetFlashDataModeFromFile() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

    }

    public class ProcessSaveFile
    {
        [XmlIgnoreAttribute] 
        public string name { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter)), Category("工单配制"), DefaultValue(""), DisplayName("工单信息")]
        public HpWebService.HpMesParameter APKWorkOrderInfo { get; set; }

        #region>>>>>>>> MES 工艺配制
        [Category("MES 工艺配制"), DefaultValue("192.168.1.100"), DisplayName("服务器IP")]
        public string dataBaseSQL_IpAddress { get; set; }

        [Category("MES 工艺配制"), DefaultValue(1433), DisplayName("端口")]
        public int dataBaseSQL_Port { get; set; }

        [Category("MES 工艺配制"), DefaultValue("sa"), DisplayName("用户名")]
        public string dataBaseSQL_User { get; set; }

        [Category("MES 工艺配制"), DefaultValue("123456"), DisplayName("密码")]
        public string dataBaseSQL_Password { get; set; }

        [Category("MES 工艺配制"), DefaultValue("HPDatabase"), DisplayName("数据库名")]
        public string dataBaseSQL_Database { get; set; }

        [XmlIgnoreAttribute]
        public bool bForceMesCheckOK = false;

        #endregion

        #region>>>>>>>> 调度系统参数(所有工位)

        [Category("调度系统参数"), DefaultValue(0), DisplayName("工单号")]
        public string workOrder { get; set; }

        [Category("调度系统参数"), DefaultValue(0), DisplayName("上料是否旋转")]
        public bool chk_Rotate_Load { get; set; }

        [Category("调度系统参数"), DefaultValue(0), DisplayName("烧录1是否旋转")]
        public bool chk_Rotate_Flash1 { get; set; }

        [Category("调度系统参数"), DefaultValue(0), DisplayName("烧录2是否旋转")]
        public bool chk_Rotate_Flash2 { get; set; }

        [Category("调度系统参数"), DefaultValue(0), DisplayName("打码切割是否旋转")]
        public bool chk_Rotate_Laser { get; set; }

        [Category("调度系统参数"), DefaultValue(0), DisplayName("下料是否旋转")]
        public bool chk_Rotate_Unload { get; set; }

        [Category("调度系统参数"), DefaultValue(0), DisplayName("包装是否旋转")]
        public bool chk_Rotate_Packing { get; set; }

        [Category("调度系统参数"), DefaultValue(0.00), DisplayName("托盘长度")]
        public double TrayWidth { get; set; }

        [Category("调度系统参数"), DefaultValue(0.00), DisplayName("托盘宽度")]
        public double TrayHeight { get; set; }

        [Category("调度系统参数"), DefaultValue(0.00), DisplayName("码垛间距")]
        public double MaduoDst { get; set; }

        [Category("调度系统参数"), DefaultValue(0.00), DisplayName("PCB板号验证")]
        public string PCBAName { get; set; }

        #endregion

        #region>>>>>>>> 烧录
        [Category("烧录工站参数"), DefaultValue(""), DisplayName("烧录菜单文件")]
        public string schemaFilename { get; set; }

        [Category("烧录工站参数"), DefaultValue(""), DisplayName("烧录序号文件")]
        public string FlashCFilename { get; set; }

        [Category("烧录工站参数"), DefaultValue(false), DisplayName("是否切换行例数据")]
        public bool flashDataMode { get; set; }

        [Category("烧录工站参数"), DefaultValue(false), DisplayName("烧录数据读取延时(ms)")]
        public int flashGetDataSleep { get; set; }

        //         [Category("烧录工站参数"), DefaultValue(""), DisplayName("烧录型号")]
        //         public string nameOfFlashType { get; set; }

        #endregion

        #region>>>>>>>> 打码切割

        //         [Category("打码切割工站参数"), DefaultValue(""), DisplayName("打码型号")]
        //         string laserPN { get; set; }

        #endregion

        #region>>>>>>>> 下料组盘

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("包装总量")]
        public short cfg_Total { get; set; }

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("包装盘数")]
        public short cfg_CountTray { get; set; }

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("尾盘料个数")]
        public short cfg_Remaind { get; set; }

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("托盘行数")]
        public short cfg_Row { get; set; }

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("托盘例数")]
        public short cfg_Col { get; set; }

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("当前包装盘数")]
        public short currentCountTray { get; set; }

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("计划生产总产量")]
        public int totalProductCount { get; set; }

        [Category("下料组盘工站参数"), DefaultValue(0), DisplayName("当前生产总产量")]
        public short currentTotalCountTray { get; set; }

        #endregion

        #region>>>>>>>> 包装

        [Category("包装工站参数"), DefaultValue(""), DisplayName("标签条码型号")]
        public string strSNType { get; set; }

        [Category("包装工站参数"), DefaultValue(""), DisplayName("最后标签")]
        public string strBarCodePrint { get; set; }

        [Category("包装工站参数"), DefaultValue(""), DisplayName("最后已下料PCBs")]
        public string[] UnloadPCBs { get; set; }

        #endregion

        public ProcessSaveFile()
        {
            APKWorkOrderInfo = new HpWebService.HpMesParameter();

            name = "";

            dataBaseSQL_IpAddress = "192.168.1.100";
            dataBaseSQL_Port = 1433;
            dataBaseSQL_User = "sa";
            dataBaseSQL_Password = "123456";
            dataBaseSQL_Database = "HPMesSQL";

            PCBAName = "";

            //============================================================
            //调度系统参数(所有工位)
            //============================================================
            /*            workOrder = "未定义工单编号";*/
            chk_Rotate_Load = false;
            chk_Rotate_Flash1 = false;
            chk_Rotate_Flash2 = false;
            chk_Rotate_Laser = false;
            chk_Rotate_Unload = false;
            chk_Rotate_Packing = false;

            TrayWidth = 150.00f;
            TrayHeight = 150.00f;
            MaduoDst = 0.00f;
            //============================================================
            //烧录
            //============================================================
            schemaFilename = System.Environment.CurrentDirectory + @"\XML" + @"\apexmic.xml";
            FlashCFilename = System.Environment.CurrentDirectory  + @"\MenuTree_Canon.c";
            flashDataMode = false;
            flashGetDataSleep = 15000;
            /*            nameOfFlashType = "";*/

            //============================================================
            //打码切割
            //============================================================
            /*            laserPN = "";*/

            //============================================================
            //下料组盘
            //============================================================
            cfg_Total = 80;
            cfg_CountTray = 5;
            cfg_Remaind = 14;
//             cfg_Row = 4;
//             cfg_Col = 4;
            currentCountTray = 0;
            totalProductCount = 94;
            currentTotalCountTray = 0;

            //============================================================
            //包装
            //============================================================
            strSNType = "12345";
            strBarCodePrint = "";

            UnloadPCBs = new string[1] { "" };
        }
    }
}
