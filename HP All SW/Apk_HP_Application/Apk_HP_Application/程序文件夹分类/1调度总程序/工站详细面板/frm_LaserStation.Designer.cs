﻿namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    partial class frm_LaserStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_LaserStation));
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.启用禁用 = new DevExpress.XtraEditors.CheckButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.停止 = new DevExpress.XtraEditors.SimpleButton();
            this.错误清除 = new DevExpress.XtraEditors.SimpleButton();
            this.启动 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl6.AppearanceCaption.Options.UseFont = true;
            this.groupControl6.Controls.Add(this.启用禁用);
            this.groupControl6.Controls.Add(this.simpleButton4);
            this.groupControl6.Controls.Add(this.停止);
            this.groupControl6.Controls.Add(this.错误清除);
            this.groupControl6.Controls.Add(this.启动);
            this.groupControl6.Location = new System.Drawing.Point(0, 1);
            this.groupControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(177, 265);
            this.groupControl6.TabIndex = 123;
            this.groupControl6.Text = "操作(仅限于电脑)";
            // 
            // 启用禁用
            // 
            this.启用禁用.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.启用禁用.Appearance.Options.UseFont = true;
            this.启用禁用.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearanceDisabled.Options.UseFont = true;
            this.启用禁用.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearanceHovered.Options.UseFont = true;
            this.启用禁用.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearancePressed.Options.UseFont = true;
            this.启用禁用.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.启用禁用.Checked = true;
            this.启用禁用.Location = new System.Drawing.Point(7, 33);
            this.启用禁用.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.启用禁用.Name = "启用禁用";
            this.启用禁用.Size = new System.Drawing.Size(76, 51);
            this.启用禁用.TabIndex = 173;
            this.启用禁用.Text = "启用/禁用";
            this.启用禁用.CheckedChanged += new System.EventHandler(this.启用禁用_CheckedChanged);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceHovered.Options.UseFont = true;
            this.simpleButton4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearancePressed.Options.UseFont = true;
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(4, 208);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(167, 49);
            this.simpleButton4.TabIndex = 135;
            this.simpleButton4.Text = "复位";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // 停止
            // 
            this.停止.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.停止.Appearance.Options.UseFont = true;
            this.停止.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearanceDisabled.Options.UseFont = true;
            this.停止.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearanceHovered.Options.UseFont = true;
            this.停止.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearancePressed.Options.UseFont = true;
            this.停止.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("停止.ImageOptions.Image")));
            this.停止.Location = new System.Drawing.Point(89, 30);
            this.停止.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.停止.Name = "停止";
            this.停止.Size = new System.Drawing.Size(84, 55);
            this.停止.TabIndex = 132;
            this.停止.Text = "停止";
            this.停止.Click += new System.EventHandler(this.停止_Click);
            // 
            // 错误清除
            // 
            this.错误清除.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.错误清除.Appearance.Options.UseFont = true;
            this.错误清除.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearanceDisabled.Options.UseFont = true;
            this.错误清除.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearanceHovered.Options.UseFont = true;
            this.错误清除.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearancePressed.Options.UseFont = true;
            this.错误清除.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("错误清除.ImageOptions.Image")));
            this.错误清除.Location = new System.Drawing.Point(4, 146);
            this.错误清除.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.错误清除.Name = "错误清除";
            this.错误清除.Size = new System.Drawing.Size(79, 55);
            this.错误清除.TabIndex = 131;
            this.错误清除.Text = "错误清除";
            this.错误清除.Click += new System.EventHandler(this.错误清除_Click);
            // 
            // 启动
            // 
            this.启动.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.启动.Appearance.Options.UseFont = true;
            this.启动.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearanceDisabled.Options.UseFont = true;
            this.启动.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearanceHovered.Options.UseFont = true;
            this.启动.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearancePressed.Options.UseFont = true;
            this.启动.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("启动.ImageOptions.Image")));
            this.启动.Location = new System.Drawing.Point(7, 87);
            this.启动.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.启动.Name = "启动";
            this.启动.Size = new System.Drawing.Size(76, 55);
            this.启动.TabIndex = 130;
            this.启动.Text = "启动";
            this.启动.Click += new System.EventHandler(this.启动_Click);
            // 
            // frm_LaserStation
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 911);
            this.Controls.Add(this.groupControl6);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_LaserStation";
            this.Text = "frm_LaserStation";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.CheckButton 启用禁用;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton 停止;
        private DevExpress.XtraEditors.SimpleButton 错误清除;
        private DevExpress.XtraEditors.SimpleButton 启动;
    }
}