﻿namespace VisionLib
{
    partial class ImageFunctionSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageFunctionSet));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ucCameraView1 = new VisionLib.ucCameraView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bStartPos = new System.Windows.Forms.Button();
            this.bEpsonMark = new System.Windows.Forms.Button();
            this.bThreshold = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsSearchRoi1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.SearchROIAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SearchROIDel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSearchRoi0 = new System.Windows.Forms.ToolStripButton();
            this.tsFindRoi1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.ModelROIAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.ModelROIEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.ModelROIDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsFindRoi0 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ResultpropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.bOpenImage = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.bTempImage = new System.Windows.Forms.Button();
            this.bCameraPara = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.bExit = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bContinuousShot = new System.Windows.Forms.Button();
            this.bOpenTest = new System.Windows.Forms.Button();
            this.bPhotograph = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.listView2 = new System.Windows.Forms.ListView();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 780F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1757, 972);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ucCameraView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 139);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.tableLayoutPanel1.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(780, 727);
            this.panel3.TabIndex = 2;
            // 
            // ucCameraView1
            // 
            this.ucCameraView1.camID = 0;
            this.ucCameraView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucCameraView1.Location = new System.Drawing.Point(0, 0);
            this.ucCameraView1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.ucCameraView1.Name = "ucCameraView1";
            this.ucCameraView1.Size = new System.Drawing.Size(780, 727);
            this.ucCameraView1.TabIndex = 0;
            this.ucCameraView1.DoubleClick += new System.EventHandler(this.ucCameraView1_DoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(4, 4);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1749, 127);
            this.panel1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox1);
            this.panel4.Controls.Add(this.toolStrip1);
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(981, 267);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 599);
            this.panel4.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bStartPos);
            this.groupBox1.Controls.Add(this.bEpsonMark);
            this.groupBox1.Controls.Add(this.bThreshold);
            this.groupBox1.Location = new System.Drawing.Point(92, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(668, 69);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // bStartPos
            // 
            this.bStartPos.Location = new System.Drawing.Point(283, 12);
            this.bStartPos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bStartPos.Name = "bStartPos";
            this.bStartPos.Size = new System.Drawing.Size(117, 49);
            this.bStartPos.TabIndex = 1;
            this.bStartPos.Text = "机器人拍照点";
            this.bStartPos.UseVisualStyleBackColor = true;
            this.bStartPos.Click += new System.EventHandler(this.bStartPos_Click);
            // 
            // bEpsonMark
            // 
            this.bEpsonMark.Location = new System.Drawing.Point(135, 12);
            this.bEpsonMark.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bEpsonMark.Name = "bEpsonMark";
            this.bEpsonMark.Size = new System.Drawing.Size(117, 49);
            this.bEpsonMark.TabIndex = 1;
            this.bEpsonMark.Text = "机器人标定";
            this.bEpsonMark.UseVisualStyleBackColor = true;
            this.bEpsonMark.Click += new System.EventHandler(this.bEpsonMark_Click);
            // 
            // bThreshold
            // 
            this.bThreshold.Location = new System.Drawing.Point(20, 12);
            this.bThreshold.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bThreshold.Name = "bThreshold";
            this.bThreshold.Size = new System.Drawing.Size(100, 49);
            this.bThreshold.TabIndex = 0;
            this.bThreshold.Text = "二值化";
            this.bThreshold.UseVisualStyleBackColor = true;
            this.bThreshold.Click += new System.EventHandler(this.bThreshold_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(42, 42);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSearchRoi1,
            this.tsSearchRoi0,
            this.tsFindRoi1,
            this.tsFindRoi0,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(88, 599);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsSearchRoi1
            // 
            this.tsSearchRoi1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsSearchRoi1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SearchROIAdd,
            this.editToolStripMenuItem,
            this.SearchROIDel});
            this.tsSearchRoi1.Image = ((System.Drawing.Image)(resources.GetObject("tsSearchRoi1.Image")));
            this.tsSearchRoi1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSearchRoi1.Name = "tsSearchRoi1";
            this.tsSearchRoi1.Size = new System.Drawing.Size(86, 46);
            this.tsSearchRoi1.Text = "SearchROI";
            // 
            // SearchROIAdd
            // 
            this.SearchROIAdd.Image = ((System.Drawing.Image)(resources.GetObject("SearchROIAdd.Image")));
            this.SearchROIAdd.Name = "SearchROIAdd";
            this.SearchROIAdd.Size = new System.Drawing.Size(203, 48);
            this.SearchROIAdd.Text = "Add";
            this.SearchROIAdd.Click += new System.EventHandler(this.SearchROIAdd_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(203, 48);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // SearchROIDel
            // 
            this.SearchROIDel.Image = ((System.Drawing.Image)(resources.GetObject("SearchROIDel.Image")));
            this.SearchROIDel.Name = "SearchROIDel";
            this.SearchROIDel.Size = new System.Drawing.Size(203, 48);
            this.SearchROIDel.Text = "Delete";
            this.SearchROIDel.Click += new System.EventHandler(this.SearchROIDel_Click);
            // 
            // tsSearchRoi0
            // 
            this.tsSearchRoi0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsSearchRoi0.Image = ((System.Drawing.Image)(resources.GetObject("tsSearchRoi0.Image")));
            this.tsSearchRoi0.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSearchRoi0.Name = "tsSearchRoi0";
            this.tsSearchRoi0.Size = new System.Drawing.Size(86, 46);
            this.tsSearchRoi0.Text = "SearchROI0";
            this.tsSearchRoi0.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.tsSearchRoi0.ToolTipText = "搜索区域";
            this.tsSearchRoi0.Click += new System.EventHandler(this.tsSearchRoi_Click);
            // 
            // tsFindRoi1
            // 
            this.tsFindRoi1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsFindRoi1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ModelROIAdd,
            this.ModelROIEdit,
            this.ModelROIDelete});
            this.tsFindRoi1.Image = ((System.Drawing.Image)(resources.GetObject("tsFindRoi1.Image")));
            this.tsFindRoi1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsFindRoi1.Name = "tsFindRoi1";
            this.tsFindRoi1.Size = new System.Drawing.Size(86, 46);
            this.tsFindRoi1.Text = "ModelRoi";
            // 
            // ModelROIAdd
            // 
            this.ModelROIAdd.Image = ((System.Drawing.Image)(resources.GetObject("ModelROIAdd.Image")));
            this.ModelROIAdd.Name = "ModelROIAdd";
            this.ModelROIAdd.Size = new System.Drawing.Size(203, 48);
            this.ModelROIAdd.Text = "Add";
            this.ModelROIAdd.Click += new System.EventHandler(this.ModelROIAdd_Click);
            // 
            // ModelROIEdit
            // 
            this.ModelROIEdit.Name = "ModelROIEdit";
            this.ModelROIEdit.Size = new System.Drawing.Size(203, 48);
            this.ModelROIEdit.Text = "Edit";
            this.ModelROIEdit.Click += new System.EventHandler(this.ModelROIEdit_Click);
            // 
            // ModelROIDelete
            // 
            this.ModelROIDelete.Image = ((System.Drawing.Image)(resources.GetObject("ModelROIDelete.Image")));
            this.ModelROIDelete.Name = "ModelROIDelete";
            this.ModelROIDelete.Size = new System.Drawing.Size(203, 48);
            this.ModelROIDelete.Text = "Delete";
            this.ModelROIDelete.Click += new System.EventHandler(this.ModelROIDelete_Click);
            // 
            // tsFindRoi0
            // 
            this.tsFindRoi0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsFindRoi0.Image = ((System.Drawing.Image)(resources.GetObject("tsFindRoi0.Image")));
            this.tsFindRoi0.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsFindRoi0.Name = "tsFindRoi0";
            this.tsFindRoi0.Size = new System.Drawing.Size(86, 46);
            this.tsFindRoi0.Text = "模板区域";
            this.tsFindRoi0.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.tsFindRoi0.ToolTipText = "模板区域";
            this.tsFindRoi0.Click += new System.EventHandler(this.tsFindRoi_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(86, 46);
            this.toolStripButton1.Text = "tsSelOK";
            this.toolStripButton1.ToolTipText = "区域确认";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(92, 80);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(684, 422);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.propertyGrid1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(676, 393);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "参数";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.LineColor = System.Drawing.SystemColors.ControlDark;
            this.propertyGrid1.Location = new System.Drawing.Point(4, 4);
            this.propertyGrid1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(668, 385);
            this.propertyGrid1.TabIndex = 11;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ResultpropertyGrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Size = new System.Drawing.Size(676, 393);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "结果";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ResultpropertyGrid
            // 
            this.ResultpropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResultpropertyGrid.LineColor = System.Drawing.SystemColors.ControlDark;
            this.ResultpropertyGrid.Location = new System.Drawing.Point(4, 4);
            this.ResultpropertyGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ResultpropertyGrid.Name = "ResultpropertyGrid";
            this.ResultpropertyGrid.Size = new System.Drawing.Size(668, 385);
            this.ResultpropertyGrid.TabIndex = 12;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.propertyGrid2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage3.Size = new System.Drawing.Size(676, 393);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "预处理";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // propertyGrid2
            // 
            this.propertyGrid2.LineColor = System.Drawing.SystemColors.ControlDark;
            this.propertyGrid2.Location = new System.Drawing.Point(5, 9);
            this.propertyGrid2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.propertyGrid2.Name = "propertyGrid2";
            this.propertyGrid2.Size = new System.Drawing.Size(665, 484);
            this.propertyGrid2.TabIndex = 12;
            this.propertyGrid2.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid2_PropertyValueChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.bOpenImage);
            this.panel2.Controls.Add(this.bTempImage);
            this.panel2.Controls.Add(this.bCameraPara);
            this.panel2.Location = new System.Drawing.Point(981, 139);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(772, 120);
            this.panel2.TabIndex = 1;
            // 
            // bOpenImage
            // 
            this.bOpenImage.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bOpenImage.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bOpenImage.ImageIndex = 5;
            this.bOpenImage.ImageList = this.imageList1;
            this.bOpenImage.Location = new System.Drawing.Point(375, 10);
            this.bOpenImage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bOpenImage.Name = "bOpenImage";
            this.bOpenImage.Size = new System.Drawing.Size(151, 106);
            this.bOpenImage.TabIndex = 3;
            this.bOpenImage.Text = "打开图像";
            this.bOpenImage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bOpenImage.UseVisualStyleBackColor = true;
            this.bOpenImage.Click += new System.EventHandler(this.bOpenImage_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Camera_Leica_Active_64px_1186714_easyicon.net.png");
            this.imageList1.Images.SetKeyName(1, "photograph_543px_1193989_easyicon.net.png");
            this.imageList1.Images.SetKeyName(2, "set_system_64px_12353_easyicon.net.png");
            this.imageList1.Images.SetKeyName(3, "save.png");
            this.imageList1.Images.SetKeyName(4, "sign_out_45.12px_1189761_easyicon.net.png");
            this.imageList1.Images.SetKeyName(5, "open_file_64px_1187339_easyicon.net.png");
            this.imageList1.Images.SetKeyName(6, "photograph_543px_1193989_easyicon.net.png");
            this.imageList1.Images.SetKeyName(7, "photograph_941px_1199881_easyicon.net.png");
            this.imageList1.Images.SetKeyName(8, "Start_48px_1186321_easyicon.net.png");
            this.imageList1.Images.SetKeyName(9, "Stop_32px_507733_easyicon.net.png");
            // 
            // bTempImage
            // 
            this.bTempImage.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bTempImage.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bTempImage.ImageIndex = 1;
            this.bTempImage.ImageList = this.imageList1;
            this.bTempImage.Location = new System.Drawing.Point(176, 10);
            this.bTempImage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bTempImage.Name = "bTempImage";
            this.bTempImage.Size = new System.Drawing.Size(151, 106);
            this.bTempImage.TabIndex = 2;
            this.bTempImage.Text = "抓取模板";
            this.bTempImage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bTempImage.UseVisualStyleBackColor = true;
            this.bTempImage.Click += new System.EventHandler(this.bTempImage_Click);
            // 
            // bCameraPara
            // 
            this.bCameraPara.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bCameraPara.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bCameraPara.ImageIndex = 0;
            this.bCameraPara.ImageList = this.imageList1;
            this.bCameraPara.Location = new System.Drawing.Point(15, 10);
            this.bCameraPara.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bCameraPara.Name = "bCameraPara";
            this.bCameraPara.Size = new System.Drawing.Size(141, 106);
            this.bCameraPara.TabIndex = 0;
            this.bCameraPara.Text = "相机参数";
            this.bCameraPara.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bCameraPara.UseVisualStyleBackColor = true;
            this.bCameraPara.Click += new System.EventHandler(this.bCameraPara_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.bExit);
            this.panel6.Controls.Add(this.bSave);
            this.panel6.Location = new System.Drawing.Point(981, 874);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel6.Name = "panel6";
            this.tableLayoutPanel1.SetRowSpan(this.panel6, 2);
            this.panel6.Size = new System.Drawing.Size(772, 94);
            this.panel6.TabIndex = 7;
            // 
            // bExit
            // 
            this.bExit.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bExit.ImageIndex = 4;
            this.bExit.ImageList = this.imageList1;
            this.bExit.Location = new System.Drawing.Point(535, 4);
            this.bExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(185, 88);
            this.bExit.TabIndex = 4;
            this.bExit.Text = "  退出";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.button1_Click);
            // 
            // bSave
            // 
            this.bSave.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bSave.ImageIndex = 3;
            this.bSave.ImageList = this.imageList1;
            this.bSave.Location = new System.Drawing.Point(300, 8);
            this.bSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(185, 88);
            this.bSave.TabIndex = 3;
            this.bSave.Text = "  保存";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // panel7
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel7, 2);
            this.panel7.Controls.Add(this.bContinuousShot);
            this.panel7.Controls.Add(this.bOpenTest);
            this.panel7.Controls.Add(this.bPhotograph);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(4, 874);
            this.panel7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel7.Name = "panel7";
            this.tableLayoutPanel1.SetRowSpan(this.panel7, 2);
            this.panel7.Size = new System.Drawing.Size(969, 94);
            this.panel7.TabIndex = 8;
            // 
            // bContinuousShot
            // 
            this.bContinuousShot.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bContinuousShot.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bContinuousShot.ImageIndex = 8;
            this.bContinuousShot.ImageList = this.imageList1;
            this.bContinuousShot.Location = new System.Drawing.Point(680, 2);
            this.bContinuousShot.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bContinuousShot.Name = "bContinuousShot";
            this.bContinuousShot.Size = new System.Drawing.Size(195, 88);
            this.bContinuousShot.TabIndex = 6;
            this.bContinuousShot.Text = "   连续拍照";
            this.bContinuousShot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bContinuousShot.UseVisualStyleBackColor = true;
            this.bContinuousShot.Click += new System.EventHandler(this.bContinuousShot_Click);
            // 
            // bOpenTest
            // 
            this.bOpenTest.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bOpenTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bOpenTest.ImageIndex = 5;
            this.bOpenTest.ImageList = this.imageList1;
            this.bOpenTest.Location = new System.Drawing.Point(392, 4);
            this.bOpenTest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bOpenTest.Name = "bOpenTest";
            this.bOpenTest.Size = new System.Drawing.Size(195, 88);
            this.bOpenTest.TabIndex = 5;
            this.bOpenTest.Text = "   打开测试";
            this.bOpenTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bOpenTest.UseVisualStyleBackColor = true;
            this.bOpenTest.Click += new System.EventHandler(this.bOpenTest_Click);
            // 
            // bPhotograph
            // 
            this.bPhotograph.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bPhotograph.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bPhotograph.ImageIndex = 6;
            this.bPhotograph.ImageList = this.imageList1;
            this.bPhotograph.Location = new System.Drawing.Point(111, 2);
            this.bPhotograph.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bPhotograph.Name = "bPhotograph";
            this.bPhotograph.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bPhotograph.Size = new System.Drawing.Size(193, 88);
            this.bPhotograph.TabIndex = 4;
            this.bPhotograph.Text = "   拍照测试";
            this.bPhotograph.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bPhotograph.UseVisualStyleBackColor = true;
            this.bPhotograph.Click += new System.EventHandler(this.bPhotograph_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.listView1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(792, 139);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(181, 120);
            this.panel5.TabIndex = 6;
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(181, 120);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.listView2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(792, 267);
            this.panel8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(181, 599);
            this.panel8.TabIndex = 10;
            // 
            // listView2
            // 
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView2.Location = new System.Drawing.Point(0, 0);
            this.listView2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(181, 599);
            this.listView2.TabIndex = 1;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.List;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Select_32px_1072882_easyicon.net.png");
            this.imageList2.Images.SetKeyName(1, "SELECTION_31.764705882353px_1162378_easyicon.net.png");
            this.imageList2.Images.SetKeyName(2, "target-download-icon.png");
            // 
            // ImageFunctionSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1757, 972);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ImageFunctionSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImageFunctionSet";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImageFunctionSet_FormClosing);
            this.Load += new System.EventHandler(this.ImageFunctionSet_Load);
            this.ResizeEnd += new System.EventHandler(this.ImageFunctionSet_ResizeEnd);
            this.Resize += new System.EventHandler(this.ImageFunctionSet_Resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private ucCameraView ucCameraView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button bCameraPara;
        private System.Windows.Forms.Button bTempImage;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.ToolStripButton tsFindRoi0;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.Button bOpenImage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bThreshold;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PropertyGrid propertyGrid2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button bOpenTest;
        private System.Windows.Forms.Button bPhotograph;
        private System.Windows.Forms.PropertyGrid ResultpropertyGrid;
        private System.Windows.Forms.ToolStripDropDownButton tsSearchRoi1;
        private System.Windows.Forms.ToolStripMenuItem SearchROIAdd;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SearchROIDel;
        private System.Windows.Forms.ToolStripButton tsSearchRoi0;
        private System.Windows.Forms.Button bContinuousShot;
        private System.Windows.Forms.ToolStripDropDownButton tsFindRoi1;
        private System.Windows.Forms.ToolStripMenuItem ModelROIAdd;
        private System.Windows.Forms.ToolStripMenuItem ModelROIEdit;
        private System.Windows.Forms.ToolStripMenuItem ModelROIDelete;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.Button bEpsonMark;
        private System.Windows.Forms.Button bStartPos;
    }
}