﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public partial class frm_UnloadStation : DevExpress.XtraEditors.XtraForm
    {
        public bool bDispose = false;
        public static Queue<HPglobal.MessageOut> m_queMessagePK = new Queue<HPglobal.MessageOut>();

        public frm_UnloadStation()
        {
            InitializeComponent();

            Thread threadOfHeart = new Thread(new ParameterizedThreadStart(thread_TestHeartBettwenRobot));
            threadOfHeart.Start(null);

            Thread threadPackingCount = new Thread(new ParameterizedThreadStart(thread_ForRecordeingPacking));
            threadPackingCount.Start(null);
        }

        ~frm_UnloadStation()
        {
            bDispose = true;
        }

        public void thread_ForRecordeingPacking(object wsarg)
        {
            while (!HPglobal.m_bQuitApp || !bDispose)
            {
                try
                {
                    Thread.Sleep(100);
                    if (HPglobal.m_bQuitApp)
                        return;

                    HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"{DateTime.Now.ToLongTimeString()}>>>等待触发包装一叠完成信号 D1060 -> 1", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(200);
                        if (HPglobal.m_bQuitApp)
                            return;

                        short trigger = HPInvoke.UnloadStation.stationBase.ReadInt16("D1060");
                        if (trigger == 1)
                        {
                            #region>>>>>>设定包装数量到包装工位
                            HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"clear", Color.Blue, 9);
                            HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"{DateTime.Now.ToLongTimeString()}>>>受到触发 D1060 ->1，开始转换数据", Color.Blue, 9);

                            short PackCount1 = HPInvoke.UnloadStation.stationBase.ReadInt16("D1061");
                            HPInvoke.PackingStation.stationBase.WriteInt16("D1061", PackCount1);

                            HPInvoke.UnloadStation.stationBase.WriteInt16("D1061", (short) 0);
                            Thread.Sleep(50);
                            HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"{DateTime.Now.ToLongTimeString()}>>>转换数据完成", Color.Blue, 9);

                            HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"{DateTime.Now.ToLongTimeString()}>>>复位D1060 -> 0", Color.Blue, 9);
                            #endregion

                            #region>>>>>>读取所有已下料盘信息并放到包装队列
                            HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"{DateTime.Now.ToLongTimeString()}>>>读取并记录包装条码到数据库", Color.Blue, 9);
                            string[] arryUnloadPCBIDs = HPInvoke.ReadUnloaedPCBID_FromRobotPLC();
                            string strWritePCBIDs = "";
                            if (arryUnloadPCBIDs != null && arryUnloadPCBIDs.Length > 0)
                            {
                                for (int nIndex = 0; nIndex < arryUnloadPCBIDs.Length; nIndex++)
                                {
                                    if (nIndex == arryUnloadPCBIDs.Length - 1)
                                        strWritePCBIDs += arryUnloadPCBIDs[nIndex];
                                    else
                                        strWritePCBIDs += (arryUnloadPCBIDs[nIndex] + ";");
                                }

                                HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"{DateTime.Now.ToLongTimeString()}>>>当前包装信息：{strWritePCBIDs}", Color.Blue, 9);
                                lock(HPInvoke.m_queUnloadPCBs)
                                {
                                    HPInvoke.m_queUnloadPCBs.Enqueue(strWritePCBIDs);
                                }

                            }

                            #endregion

                            #region>>>>>>清除已下料的PCBs
                            HPglobal.WorkingMessageOutput(ref m_queMessagePK, $"{DateTime.Now.ToLongTimeString()}>>>清除已下料的PCBs", Color.Blue, 9);
                            HPInvoke.ResetAllUnloadPCBs();
                            #endregion

                            HPInvoke.UnloadStation.stationBase.WriteInt16("D1060", 0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_ForRecordeingPacking  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        public void thread_TestHeartBettwenRobot(object wsarg)
        {
            string trigAddress = "";
            while (!HPglobal.m_bQuitApp || !bDispose)
            {
                try
                {
                    Thread.Sleep(2000);
                    if (HPglobal.m_bQuitApp)
                        return;

                    OperateResult result = HPInvoke.PCServer_Robot.Write("1000", (short)1);
                    if (result.IsSuccess)
                        HPInvoke.PCServer_Robot.isConnected = true;
                    else
                    {
                        HPglobal.RecordeAllMessage("注意：机器人服务器已断开，开始重连", true, false, "");
                        HPInvoke.PCServer_Robot.isConnected = false;
                        result = HPInvoke.PCServer_Robot.ConnectModebusTCPServer(HPglobal.strIPAddress_下料PC, HPglobal.nPortOfAllDevice, 2);
                        if (result.IsSuccess)
                        {
                            HPInvoke.PCServer_Robot.isConnected = true;
                            HPInvoke.PCServer_Robot.SetPersistentConnection(); //重连
                        }
                        else
                        {
                            HPInvoke.PCServer_Robot.isConnected = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_TestHeartBettwenPLC  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        public bool IsReady()
        {
            try
            {
                return (HPInvoke.PCServer_Robot.ReadInt16("1002").Content == 1 ? true : false);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_UnloadStation IsReady() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;
        }

        public bool Reset()
        {
            try
            {
                //发送复位信号
                HPInvoke.PCServer_Robot.Write("1004", (short)1);
                Thread.Sleep(500);
                HPInvoke.PCServer_Robot.Write("1004", (short)0);

                return true;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_UnloadStation Reset() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
            return false;
        }

        public bool Start()
        {
            try
            {
                HPInvoke.PCServer_Robot.Write("1003", (short)1);
                Thread.Sleep(500);
                HPInvoke.PCServer_Robot.Write("1003", (short)0);

                return true;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_UnloadStation Start() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
            return false;
        }

        public bool Stop()
        {
            try
            {
                HPInvoke.PCServer_Robot.Write("1005", (short)1);
                Thread.Sleep(500);
                HPInvoke.PCServer_Robot.Write("1005", (short)0);

                return true;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_UnloadStation Stop() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
            return false;
        }

        private void 启用禁用_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void 启动_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void 停止_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void 复位_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void 错误清除_Click(object sender, EventArgs e)
        {

        }

        public void UpdateStatus()
        {
            try
            {
                #region>>>>显示好板，坏板统计数据

                #endregion

                #region>>>>显示设备的连接状态
                //===============================
                if (HPInvoke.PCServer_Robot.isConnected)
                    LEDRobot.LanternBackground = (LEDRobot.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                else
                    LEDRobot.LanternBackground = (LEDRobot.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);

                #endregion

                #region>>>>>>取出接受与发出的命
                HPglobal.outMessage(richTextCamera1, m_queMessagePK);
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_UnloadStation UpdateStatus 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                UpdateStatus();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_UnloadStation timer1_Tick 异常\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }
    }
}