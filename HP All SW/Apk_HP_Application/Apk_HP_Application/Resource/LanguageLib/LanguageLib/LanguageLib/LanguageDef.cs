﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageLib
{
    /// <summary>
    /// Adding a new language:
    /// 1. Add the language as the last item into enum definition.
    /// 2. Create a new public variable for the language.
    /// 3. Create a new public method to save the text.
    /// 4. Create a new public method to read the saved text.
    /// 5. Add new language methods to the writing and reading dictionary. 
    /// </summary>
    public class LanguageDef
    {
        public enum lang : int { English, Finnish, Swedish, Chinese, Russian, Danish, French, German, Spanish, Portuguese, Estonian, Polish, Hungarian, Bulgarian, Japanese };

        // Dictionary for writing texts and reading texts.
        protected Dictionary<String, SetLang> langWrite = new Dictionary<String, SetLang>();
        protected Dictionary<String, GetLang> langRead  = new Dictionary<String, GetLang>();

        // Dictionary for writing help texts and reading help texts.
        protected Dictionary<String, SetHelp> langWriteHelp = new Dictionary<String, SetHelp>();
        protected Dictionary<String, GetHelp> langReadHelp = new Dictionary<String, GetHelp>();

        // Static string for the text "Help".
        public static String HELP = "Help";

        // Variables for texts.
        public String English;
        public String Finnish;
        public String Swedish;
        public String Chinese;
        public String Russian;
        public String Danish;
        public String French;
        public String German;
        public String Spanish;
        public String Portuguese;
        public String Estonian;
        public String Polish;
        public String Hungarian;
        public String Bulgarian;
        public String Japanese;

        // Variables for help texts.
        public String EnglishHelp;
        public String FinnishHelp;
        public String SwedishHelp;
        public String ChineseHelp;
        public String RussianHelp;
        public String DanishHelp;
        public String FrenchHelp;
        public String GermanHelp;
        public String SpanishHelp;
        public String PortugueseHelp;
        public String EstonianHelp;
        public String PolishHelp;
        public String HungarianHelp;
        public String BulgarianHelp;
        public String JapaneseHelp;

        /// <summary>
        /// Delegate method to set the text.
        /// </summary>
        /// <param name="text"></param>
        public delegate void SetLang(String text);
        /// <summary>
        /// Sets the english text.
        /// </summary>
        /// <param name="text">english text</param>
        public void SetEnglish(String text)
        {
            English = text;
        }
        /// <summary>
        /// Sets the finnish text.
        /// </summary>
        /// <param name="text">finnish text</param>
        public void SetFinnish(String text)
        {
            Finnish = text;
        }
        /// <summary>
        /// Sets the swedish text.
        /// </summary>
        /// <param name="text">swedish text</param>
        public void SetSwedish(String text)
        {
            Swedish = text;
        }
        /// <summary>
        /// Sets the chinese text.
        /// </summary>
        /// <param name="text">chinese text</param>
        public void SetChinese(String text)
        {
            Chinese = text;
        }
        /// <summary>
        /// Sets the russian text.
        /// </summary>
        /// <param name="text">russian text</param>
        public void SetRussian(String text)
        {
            Russian = text;
        }
        /// <summary>
        /// Sets the danish text.
        /// </summary>
        /// <param name="text">danish text</param>
        public void SetDanish(String text)
        {
            Danish = text;
        }
        /// <summary>
        /// Sets the french text.
        /// </summary>
        /// <param name="text">french text</param>
        public void SetFrench(String text)
        {
            French = text;
        }
        /// <summary>
        /// Sets the german text.
        /// </summary>
        /// <param name="text">german text</param>
        public void SetGerman(String text)
        {
            German = text;
        }
        /// <summary>
        /// Sets the spanish text.
        /// </summary>
        /// <param name="text">spanish text</param>
        public void SetSpanish(String text)
        {
            Spanish = text;
        }
        /// <summary>
        /// Sets the portuguese text.
        /// </summary>
        /// <param name="text">portuguese text</param>
        public void SetPortuguese(String text)
        {
            Portuguese = text;
        }
        /// <summary>
        /// Sets the estonian text.
        /// </summary>
        /// <param name="text">portuguese text</param>
        public void SetEstonian(String text)
        {
            Estonian = text;
        }
        /// <summary>
        /// Sets the polish text.
        /// </summary>
        /// <param name="text">polish text</param>
        public void SetPolish(String text)
        {
            Polish = text;
        }
        public void SetHungarian(String text)
        {
            Hungarian = text;
        }        
        public void SetBulgarian(String text)
        {
            Bulgarian = text;
        }
        public void SetJapanese(String text)
        {
            Japanese = text;
        }

        /// <summary>
        /// Delegate methods to get the text.
        /// </summary>
        /// <returns></returns>
        public delegate String GetLang();
        /// <summary>
        /// Returns the english text.
        /// </summary>
        /// <returns>String:english text</returns>
        public String GetEnglish()
        {
            return English;
        }
        /// <summary>
        /// Returns the finnish text.
        /// </summary>
        /// <returns>String:finnish text</returns>
        public String GetFinnish()
        {
            return Finnish;
        }
        /// <summary>
        /// Returns the swedish text.
        /// </summary>
        /// <returns>String:swedish text</returns>
        public String GetSwedish()
        {
            return Swedish;
        }
        /// <summary>
        /// Returns the chinese text.
        /// </summary>
        /// <returns>String:chinese text</returns>
        public String GetChinese()
        {
            return Chinese;
        }
        /// <summary>
        /// Returns the russian text.
        /// </summary>
        /// <returns>String:russian text</returns>
        public String GetRussian()
        {
            return Russian;
        }
        /// <summary>
        /// Returns the danish text.
        /// </summary>
        /// <returns>String:danish text</returns>
        public String GetDanish()
        {
            return Danish;
        }
        /// <summary>
        /// Returns the french text.
        /// </summary>
        /// <returns>String:french text</returns>
        public String GetFrench()
        {
            return French;
        }
        /// <summary>
        /// Returns the german text.
        /// </summary>
        /// <returns>String:german text</returns>
        public String GetGerman()
        {
            return German;
        }
        /// <summary>
        /// Returns the spanish text.
        /// </summary>
        /// <returns>String:spanish text</returns>
        public String GetSpanish()
        {
            return Spanish;
        }
        /// <summary>
        /// Returns the portuguese text.
        /// </summary>
        /// <returns>String:portuguese text</returns>
        public String GetPortuguese()
        {
            return Portuguese;
        }
        /// <summary>
        /// Returns the portuguese text.
        /// </summary>
        /// <returns>String:portuguese text</returns>
        public String GetEstonian()
        {
            return Estonian;
        }
        /// <summary>
        /// Returns the polish text.
        /// </summary>
        /// <returns>String:polish text</returns>
        public String GetPolish()
        {
            return Polish;
        }
        public String GetHungarian()
        {
            return Hungarian;
        }
        public String GetBulgarian()
        {
            return Bulgarian;
        }
        public String GetJapanese()
        {
            return Japanese;
        }

        /// <summary>
        /// Delegate method to set the help text.
        /// </summary>
        /// <param name="text"></param>
        public delegate void SetHelp(String text);

        /// <summary>
        /// Sets the english help text.
        /// </summary>
        /// <param name="text">english help text</param>
        public void SetEnglishHelp(String text)
        {
            EnglishHelp = text;
        }
        /// <summary>
        /// Sets the finnish help text.
        /// </summary>
        /// <param name="text">finnish help text</param>
        public void SetFinnishHelp(String text)
        {
            FinnishHelp = text;
        }
        /// <summary>
        /// Sets the swedish help text.
        /// </summary>
        /// <param name="text">swedish help text</param>
        public void SetSwedishHelp(String text)
        {
            SwedishHelp = text;
        }
        /// <summary>
        /// Sets the chinese help text.
        /// </summary>
        /// <param name="text">chinese help text</param>
        public void SetChineseHelp(String text)
        {
            ChineseHelp = text;
        }
        /// <summary>
        /// Sets the russian help text.
        /// </summary>
        /// <param name="text">russian help text</param>
        public void SetRussianHelp(String text)
        {
            RussianHelp = text;
        }
        /// <summary>
        /// Sets the danish help text.
        /// </summary>
        /// <param name="text">danish help text</param>
        public void SetDanishHelp(String text)
        {
            DanishHelp = text;
        }
        /// <summary>
        /// Sets the french help text.
        /// </summary>
        /// <param name="text">french help text</param>
        public void SetFrenchHelp(String text)
        {
            FrenchHelp = text;
        }
        /// <summary>
        /// Sets the german help text.
        /// </summary>
        /// <param name="text">german help text</param>
        public void SetGermanHelp(String text)
        {
            GermanHelp = text;
        }
        /// <summary>
        /// Sets the spanish help text.
        /// </summary>
        /// <param name="text">spanish help text</param>
        public void SetSpanishHelp(String text)
        {
            SpanishHelp = text;
        }
        /// <summary>
        /// Sets the portuguese help text.
        /// </summary>
        /// <param name="text">portuguese help text</param>
        public void SetPortugueseHelp(String text)
        {
            PortugueseHelp = text;
        }
        /// <summary>
        /// Sets the estonian help text.
        /// </summary>
        /// <param name="text">portuguese help text</param>
        public void SetEstonianHelp(String text)
        {
            EstonianHelp = text;
        }
        /// <summary>
        /// Sets the polish help text.
        /// </summary>
        /// <param name="text">polish help text</param>
        public void SetPolishHelp(String text)
        {
            PolishHelp = text;
        }
        public void SetHungarianHelp(String text)
        {
            HungarianHelp = text;
        }
        public void SetBulgarianHelp(String text)
        {
            BulgarianHelp = text;
        }
        public void SetJapaneseHelp(String text)
        {
            JapaneseHelp = text;
        }
        /// <summary>
        /// Delegate methods to get the help text.
        /// </summary>
        /// <returns></returns>
        public delegate String GetHelp();
        /// <summary>
        /// Returns the english help text
        /// </summary>
        /// <returns>String:english help text</returns>
        public String GetEnglishHelp()
        {
            return EnglishHelp;
        }
        /// <summary>
        /// Returns the finnish help text
        /// </summary>
        /// <returns>String:finnish help text</returns>
        public String GetFinnishHelp()
        {
            return FinnishHelp;
        }
        /// <summary>
        /// Returns the swedish help text
        /// </summary>
        /// <returns>String:swedish help text</returns>
        public String GetSwedishHelp()
        {
            return SwedishHelp;
        }
        /// <summary>
        /// Returns the chinese help text
        /// </summary>
        /// <returns>String:chinese help text</returns>
        public String GetChineseHelp()
        {
            return ChineseHelp;
        }
        /// <summary>
        /// Returns the russian help text
        /// </summary>
        /// <returns>String:russian help text</returns>
        public String GetRussianHelp()
        {
            return RussianHelp;
        }
        /// <summary>
        /// Returns the danish help text
        /// </summary>
        /// <returns>String:danish help text</returns>
        public String GetDanishHelp()
        {
            return DanishHelp;
        }
        /// <summary>
        /// Returns the french help text
        /// </summary>
        /// <returns>String:french help text</returns>
        public String GetFrenchHelp()
        {
            return FrenchHelp;
        }
        /// <summary>
        /// Returns the german help text
        /// </summary>
        /// <returns>String:german help text</returns>
        public String GetGermanHelp()
        {
            return GermanHelp;
        }
        /// <summary>
        /// Returns the spanish help text
        /// </summary>
        /// <returns>String:spanish help text</returns>
        public String GetSpanishHelp()
        {
            return SpanishHelp;
        }
        /// <summary>
        /// Returns the portuguese help text
        /// </summary>
        /// <returns>String:portuguese help text</returns>
        public String GetPortugueseHelp()
        {
            return PortugueseHelp;
        }
        /// <summary>
        /// Returns the portuguese help text
        /// </summary>
        /// <returns>String:portuguese help text</returns>
        public String GetEstonianHelp()
        {
            return EstonianHelp;
        }
        /// <summary>
        /// Returns the polish help text
        /// </summary>
        /// <returns>String:polish help text</returns>
        public String GetPolishHelp()
        {
            return PolishHelp;
        }
        public String GetHungarianHelp()
        {
            return HungarianHelp;
        }
        public String GetBulgarianHelp()
        {
            return BulgarianHelp;
        }
        public String GetJapaneseHelp()
        {
            return JapaneseHelp;
        }
        /// <summary>
        /// Creates writind and reading dictionary.
        /// </summary>
        public void CreateDictionaries()
        {
            // Add languages to the writing dictionary.
            langWrite.Add(lang.English.ToString(),    new SetLang(SetEnglish));
            langWrite.Add(lang.Finnish.ToString(),    new SetLang(SetFinnish));
            langWrite.Add(lang.Swedish.ToString(),    new SetLang(SetSwedish));
            langWrite.Add(lang.Chinese.ToString(),    new SetLang(SetChinese));
            langWrite.Add(lang.Russian.ToString(),    new SetLang(SetRussian));
            langWrite.Add(lang.Danish.ToString(),     new SetLang(SetDanish)); 
            langWrite.Add(lang.French.ToString(),     new SetLang(SetFrench));
            langWrite.Add(lang.German.ToString(),     new SetLang(SetGerman));
            langWrite.Add(lang.Spanish.ToString(),    new SetLang(SetSpanish));
            langWrite.Add(lang.Portuguese.ToString(), new SetLang(SetPortuguese));
            langWrite.Add(lang.Estonian.ToString(),   new SetLang(SetEstonian));
            langWrite.Add(lang.Polish.ToString(),     new SetLang(SetPolish));
            langWrite.Add(lang.Hungarian.ToString(),  new SetLang(SetHungarian));
            langWrite.Add(lang.Bulgarian.ToString(),  new SetLang(SetBulgarian));
            langWrite.Add(lang.Japanese.ToString(), new SetLang(SetJapanese));

            // Add languages to the reading dictionary.
            langRead.Add(lang.English.ToString(),    new GetLang(GetEnglish));
            langRead.Add(lang.Finnish.ToString(),    new GetLang(GetFinnish));
            langRead.Add(lang.Swedish.ToString(),    new GetLang(GetSwedish));
            langRead.Add(lang.Chinese.ToString(),    new GetLang(GetChinese));
            langRead.Add(lang.Russian.ToString(),    new GetLang(GetRussian));
            langRead.Add(lang.Danish.ToString(),     new GetLang(GetDanish));
            langRead.Add(lang.French.ToString(),     new GetLang(GetFrench));
            langRead.Add(lang.German.ToString(),     new GetLang(GetGerman));
            langRead.Add(lang.Spanish.ToString(),    new GetLang(GetSpanish));
            langRead.Add(lang.Portuguese.ToString(), new GetLang(GetPortuguese));
            langRead.Add(lang.Estonian.ToString(),   new GetLang(GetEstonian));
            langRead.Add(lang.Polish.ToString(),     new GetLang(GetPolish));
            langRead.Add(lang.Hungarian.ToString(),  new GetLang(GetHungarian));
            langRead.Add(lang.Bulgarian.ToString(),  new GetLang(GetBulgarian));
            langRead.Add(lang.Japanese.ToString(), new GetLang(GetJapanese));

            // Add languages to the writing dictionary for help texts.
            langWriteHelp.Add(lang.English.ToString()    + HELP, new SetHelp(SetEnglishHelp));
            langWriteHelp.Add(lang.Finnish.ToString()    + HELP, new SetHelp(SetFinnishHelp));
            langWriteHelp.Add(lang.Swedish.ToString()    + HELP, new SetHelp(SetSwedishHelp));
            langWriteHelp.Add(lang.Chinese.ToString()    + HELP, new SetHelp(SetChineseHelp));
            langWriteHelp.Add(lang.Russian.ToString()    + HELP, new SetHelp(SetRussianHelp));
            langWriteHelp.Add(lang.Danish.ToString()     + HELP, new SetHelp(SetDanishHelp));
            langWriteHelp.Add(lang.French.ToString()     + HELP, new SetHelp(SetFrenchHelp));
            langWriteHelp.Add(lang.German.ToString()     + HELP, new SetHelp(SetGermanHelp));
            langWriteHelp.Add(lang.Spanish.ToString()    + HELP, new SetHelp(SetSpanishHelp));
            langWriteHelp.Add(lang.Portuguese.ToString() + HELP, new SetHelp(SetPortugueseHelp));
            langWriteHelp.Add(lang.Estonian.ToString()   + HELP, new SetHelp(SetEstonianHelp));
            langWriteHelp.Add(lang.Polish.ToString()     + HELP, new SetHelp(SetPolishHelp));
            langWriteHelp.Add(lang.Hungarian.ToString()  + HELP, new SetHelp(SetHungarianHelp));
            langWriteHelp.Add(lang.Bulgarian.ToString()  + HELP, new SetHelp(SetBulgarianHelp));
            langWriteHelp.Add(lang.Japanese.ToString() + HELP, new SetHelp(SetJapaneseHelp));

            // Add languages to the reading dictionary for help texts.
            langReadHelp.Add(lang.English.ToString()    + HELP, new GetHelp(GetEnglishHelp));
            langReadHelp.Add(lang.Finnish.ToString()    + HELP, new GetHelp(GetFinnishHelp));
            langReadHelp.Add(lang.Swedish.ToString()    + HELP, new GetHelp(GetSwedishHelp));
            langReadHelp.Add(lang.Chinese.ToString()    + HELP, new GetHelp(GetChineseHelp));
            langReadHelp.Add(lang.Russian.ToString()    + HELP, new GetHelp(GetRussianHelp));
            langReadHelp.Add(lang.Danish.ToString()     + HELP, new GetHelp(GetDanishHelp));
            langReadHelp.Add(lang.French.ToString()     + HELP, new GetHelp(GetFrenchHelp));
            langReadHelp.Add(lang.German.ToString()     + HELP, new GetHelp(GetGermanHelp));
            langReadHelp.Add(lang.Spanish.ToString()    + HELP, new GetHelp(GetSpanishHelp));
            langReadHelp.Add(lang.Portuguese.ToString() + HELP, new GetHelp(GetPortugueseHelp));
            langReadHelp.Add(lang.Estonian.ToString()   + HELP, new GetHelp(GetEstonianHelp));
            langReadHelp.Add(lang.Polish.ToString()     + HELP, new GetHelp(GetPolishHelp));
            langReadHelp.Add(lang.Hungarian.ToString()  + HELP, new GetHelp(GetHungarianHelp));
            langReadHelp.Add(lang.Bulgarian.ToString()  + HELP, new GetHelp(GetBulgarianHelp));
            langReadHelp.Add(lang.Japanese.ToString() + HELP, new GetHelp(GetJapaneseHelp));
        }
    }
}
