﻿namespace Apk_HP_Application
{
    partial class frm_ActionErrorHandle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btmHanleContinue = new System.Windows.Forms.Button();
            this.btmHanleSkip = new System.Windows.Forms.Button();
            this.btmHanleStop = new System.Windows.Forms.Button();
            this.btmHanleAbort = new System.Windows.Forms.Button();
            this.time_display = new System.Windows.Forms.Timer(this.components);
            this.确定 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(1, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(960, 308);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "请输入信息";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btmHanleContinue
            // 
            this.btmHanleContinue.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmHanleContinue.Location = new System.Drawing.Point(29, 371);
            this.btmHanleContinue.Name = "btmHanleContinue";
            this.btmHanleContinue.Size = new System.Drawing.Size(199, 105);
            this.btmHanleContinue.TabIndex = 1;
            this.btmHanleContinue.Text = "继续(已处理)";
            this.btmHanleContinue.UseVisualStyleBackColor = true;
            this.btmHanleContinue.Click += new System.EventHandler(this.btmHanleContinue_Click);
            // 
            // btmHanleSkip
            // 
            this.btmHanleSkip.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmHanleSkip.Location = new System.Drawing.Point(264, 371);
            this.btmHanleSkip.Name = "btmHanleSkip";
            this.btmHanleSkip.Size = new System.Drawing.Size(197, 105);
            this.btmHanleSkip.TabIndex = 2;
            this.btmHanleSkip.Text = "重来";
            this.btmHanleSkip.UseVisualStyleBackColor = true;
            this.btmHanleSkip.Click += new System.EventHandler(this.btmHanleSkip_Click);
            // 
            // btmHanleStop
            // 
            this.btmHanleStop.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmHanleStop.Location = new System.Drawing.Point(497, 371);
            this.btmHanleStop.Name = "btmHanleStop";
            this.btmHanleStop.Size = new System.Drawing.Size(197, 105);
            this.btmHanleStop.TabIndex = 3;
            this.btmHanleStop.Text = "停止";
            this.btmHanleStop.UseVisualStyleBackColor = true;
            this.btmHanleStop.Click += new System.EventHandler(this.btmHanleStop_Click);
            // 
            // btmHanleAbort
            // 
            this.btmHanleAbort.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmHanleAbort.Location = new System.Drawing.Point(730, 371);
            this.btmHanleAbort.Name = "btmHanleAbort";
            this.btmHanleAbort.Size = new System.Drawing.Size(197, 105);
            this.btmHanleAbort.TabIndex = 4;
            this.btmHanleAbort.Text = "终止运行";
            this.btmHanleAbort.UseVisualStyleBackColor = true;
            this.btmHanleAbort.Click += new System.EventHandler(this.btmHanleAbort_Click);
            // 
            // time_display
            // 
            this.time_display.Enabled = true;
            this.time_display.Interval = 1000;
            this.time_display.Tick += new System.EventHandler(this.time_display_Tick);
            // 
            // 确定
            // 
            this.确定.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.确定.Location = new System.Drawing.Point(264, 371);
            this.确定.Name = "确定";
            this.确定.Size = new System.Drawing.Size(430, 105);
            this.确定.TabIndex = 5;
            this.确定.Text = "确定";
            this.确定.UseVisualStyleBackColor = true;
            this.确定.Click += new System.EventHandler(this.确定_Click);
            // 
            // frm_ActionErrorHandle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 503);
            this.Controls.Add(this.确定);
            this.Controls.Add(this.btmHanleAbort);
            this.Controls.Add(this.btmHanleStop);
            this.Controls.Add(this.btmHanleSkip);
            this.Controls.Add(this.btmHanleContinue);
            this.Controls.Add(this.textBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_ActionErrorHandle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "动作执行，错误处理";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Closing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_Closed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btmHanleContinue;
        private System.Windows.Forms.Button btmHanleSkip;
        private System.Windows.Forms.Button btmHanleStop;
        private System.Windows.Forms.Button btmHanleAbort;
        private System.Windows.Forms.Timer time_display;
        private System.Windows.Forms.Button 确定;
    }
}