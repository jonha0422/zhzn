﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LanguageLib
{
    public partial class GiveControlText : Form
    {
        public bool Accepted { get; set; }

        public GiveControlText()
        {
            InitializeComponent();
        }

        public void SetControlname(string name)
        {
            name = name.TrimStart(' ');
            lControlName.Text = name;
        }

        public string Message {
            get
            {
                return tMessage.Text;
            }
            set
            {
                tMessage.Text = value;
            }
        }

        public string ControlText
        {
            get
            {
                return tControlText.Text;
            }
            set
            {
                tControlText.Text = value;
            }
        }

        public bool EnableControlTextField
        {
            get
            {
                return tControlText.Enabled;
            }
            set
            {
                tControlText.Enabled = value;
            }
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            Accepted = true;
            Close();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            Accepted = false;
            Close();
        }
    }
}
