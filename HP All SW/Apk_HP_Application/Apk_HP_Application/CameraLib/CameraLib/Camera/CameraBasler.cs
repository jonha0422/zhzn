﻿using Basler.Pylon;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CamaeraLib.Camera
{
    public class CameraBasler : CameraBase
    {
        private Basler.Pylon.Camera camera = null;
        private PixelDataConverter converter = new PixelDataConverter();
        private Stopwatch stopWatch = new Stopwatch();
        List<IGrabResult> m_frameList = new List<IGrabResult>();        /* 图像缓存列表 */
        IGrabResult _GrabbedRawData;
        protected Object m_lockObject;                     /* Lock object used for thread synchronization. */

        /* Creates the last error text from message and detailed text. */


        /* Constructor with creation of basic objects. */
        public CameraBasler()
        {
            m_lockObject = new Object();
        }

        /* Indicates that ImageProvider and device are open. */
        public bool IsOpen
        {
            get { return m_open; }
        }
        private void ShowException(Exception exception)
        {
            LastError =  exception.Message;
        }
        /* Open using index. Before ImageProvider can be opened using the index, Pylon.EnumerateDevices() needs to be called. */
        private void setEvent()
        {
            camera.ConnectionLost += OnConnectionLost;
            camera.CameraOpened += OnCameraOpened;
            camera.CameraClosed += OnCameraClosed;
            camera.StreamGrabber.GrabStarted += OnGrabStarted;
            camera.StreamGrabber.ImageGrabbed += OnImageGrabbed;
            camera.StreamGrabber.GrabStopped += OnGrabStopped;
        }
        public override int OpenCamByID(short id)
        {
            try
            {
                List<ICameraInfo> allCameras = CameraFinder.Enumerate();
                if (allCameras.Count <= id)
                {
                    return -1;
                }
                // Create a new camera object.
                camera = new Basler.Pylon.Camera(allCameras[id]);

                camera.CameraOpened += Configuration.AcquireContinuous;

                // Register for the events of the image provider needed for proper operation.

                camID = id;
                // Open the connection to the camera device.
                camera.Open();
                setEvent();
                return 0;


            }
            catch (Exception exception)
            {
                ShowException(exception);
            }
            return -1;
        }
        public override int OpenCameraByName(string Name)
        {
            camName = Name;
            try
            {
                List<ICameraInfo> allCameras = CameraFinder.Enumerate();
                for(int id=0;id< allCameras.Count; id++)
                {
                    if(allCameras[id][CameraInfoKey.UserDefinedName]== Name)
                    {
                        camera = new Basler.Pylon.Camera(allCameras[id]);

                        camera.CameraOpened += Configuration.AcquireContinuous;

                        // Register for the events of the image provider needed for proper operation.

                        camID = id;
                        // Open the connection to the camera device.
                        camera.Open();
                        setEvent();
                        return 0;
                    }
                    
                }
                


            }
            catch (Exception exception)
            {
                ShowException(exception);
            }
            return 1;
        }
        /* Close the device */
        public override int Close()
        {
            try
            {
                if (camera != null)
                {
                    camera.Close();
                    camera.Dispose();
                    camera = null;
                }
            }
            catch (Exception exception)
            {
                ShowException(exception);
            }
            return 0;
        }

        /* Start the grab of one image. */
        public override int OneShot()
        {
            try
            {
                // Starts the grabbing of one image.
                camera.Parameters[PLCamera.AcquisitionMode].SetValue(PLCamera.AcquisitionMode.SingleFrame);
                camera.StreamGrabber.Start(1, GrabStrategy.OneByOne, GrabLoop.ProvidedByStreamGrabber);
            }
            catch (Exception exception)
            {
                ShowException(exception);
                return -1;
            }
            return 0;
        }

        /* Start the grab of images until stopped. */
        public override int ContinuousShot()
        {
            try
            {
                // Start the grabbing of images until grabbing is stopped.
                camera.Parameters[PLCamera.AcquisitionMode].SetValue(PLCamera.AcquisitionMode.Continuous);
                camera.StreamGrabber.Start(GrabStrategy.OneByOne, GrabLoop.ProvidedByStreamGrabber);
                return 0;
            }
            catch (Exception exception)
            {
                ShowException(exception);
            }
            return -1;
        }

        /* Stops the grabbing of images. */
        public override int Stop()
        {
            try
            {
                camera.StreamGrabber.Stop();
                return 0;
            }
            catch (Exception exception)
            {
                ShowException(exception);
            }
            return -1;
        }
        public override int SetTrigger(bool Trigger)
        {
            if (Trigger == true)
            {
                camera.Parameters[PLCamera.TriggerMode].SetValue("On");
            }
            else
            {
                camera.Parameters[PLCamera.TriggerMode].SetValue("Off");
            }
            return 0;
        }
        public override int SetTriggerSource(int TriggerSource)
        {
            if (TriggerSource == 0)
            {
                camera.Parameters[PLCamera.TriggerSource].SetValue("Software");
            }
            else
            {
                camera.Parameters[PLCamera.TriggerSource].SetValue("Line1");
            }
            
            return 0;
        }
        public override int SetExposureTime(int ExposureTime)
        {
            try
            {
                if (camera.IsOpen == false)
                {
                    return -1;
                }
                if (camera.Parameters.Contains(PLCamera.ExposureTimeAbs))
                {
                    camera.Parameters[PLCamera.ExposureTimeAbs].SetValue((double)ExposureTime);
                }
                else
                {
                    camera.Parameters[PLCamera.ExposureTime].SetValue((double)ExposureTime);
                }
            }
            catch (Exception e)
            {
                return -1;
            }
            return 0;
        }
        public override int getExposureTime(out int ExposureTime)
        {
            ExposureTime = 0;
            try
            {
                if (camera.IsOpen == false)
                {
                    return -1;
                }
                if (camera.Parameters.Contains(PLCamera.ExposureTimeAbs))
                {
                    ExposureTime = (int)camera.Parameters[PLCamera.ExposureTimeAbs].GetValue();
                }
                else
                {
                    ExposureTime = (int)camera.Parameters[PLCamera.ExposureTime].GetValue();
                }
            }
            catch (Exception e)
            {
                return -1;
            }
            return 0;
        }
        public override int SetGainVal(int GainVal)
        {
            try
            {
                if (camera.IsOpen == false)
                {
                    return -1;
                }
                if (camera.Parameters.Contains(PLCamera.GainAbs))
                {
                    camera.Parameters[PLCamera.GainAbs].SetValue((double)GainVal);
                }
                else if (camera.Parameters.Contains(PLCamera.GainRaw))
                {
                    camera.Parameters[PLCamera.GainRaw].SetValue((long)GainVal);
                }
                else
                {
                    camera.Parameters[PLCamera.Gain].SetValue((double)GainVal);
                }
            }
            catch(Exception e){
                return -1;
            }
            return 0;
        }
        public override int GetGainVal(out int GainVal)
        {
            GainVal = 0;
            try
            {
                if (camera.IsOpen == false)
                {
                    return -1;
                }
                if (camera.Parameters.Contains(PLCamera.GainAbs))
                {
                    GainVal = (int)camera.Parameters[PLCamera.GainAbs].GetValue();
                }
                else
                {
                    GainVal = (int)camera.Parameters[PLCamera.Gain].GetValue();
                }
            }
            catch (Exception e)
            {
                return -1;
            }
            return 0;
            return 0;
        }
        public override Image GetLatestImage()
        {
            lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
            {
                /* Release all images but the latest. */
                while (m_frameList.Count > 1)
                {
                    m_frameList[0].Dispose();
                    m_frameList.RemoveAt(0);
                }
                if (m_frameList.Count > 0) /* If images available. */
                {
                   // Bitmap bitmap = new Bitmap(m_frameList[0].Width, m_frameList[0].Height, PixelFormat.Format32bppRgb);
                    // Lock the bits of the bitmap.
                   // BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                    // Place the pointer to the buffer of the bitmap.
                    //converter.OutputPixelFormat = PixelType.BGRA8packed;
                    //IntPtr ptrBmp = bmpData.Scan0;
                  //  byte[] buffer1 = m_frameList[0].PixelData as byte[];
                   // converter.Convert<byte>(buffer1, m_frameList[0]); //Exception handling TODO
                   // bitmap.UnlockBits(bmpData);

                    byte[] buffer = m_frameList[0].PixelData as byte[];
                    // m_frameList[0].PixelData
                    Image mImage = new Image(m_frameList[0].Width, m_frameList[0].Height, buffer, false);
                    return mImage;
                }
            }
            return null; /* No image available. */
        }

        public override Bitmap GetLatestBitmap()
        {
            lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
            {
                /* Release all images but the latest. */
                while (m_frameList.Count > 1)
                {
                    m_frameList[0].Dispose();
                    m_frameList.RemoveAt(0);
                }
                if (m_frameList.Count > 0) /* If images available. */
                {
                     Bitmap bitmap = new Bitmap(m_frameList[0].Width, m_frameList[0].Height, PixelFormat.Format32bppRgb);
                    // Lock the bits of the bitmap.
                     BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                    // Place the pointer to the buffer of the bitmap.
                    converter.OutputPixelFormat = PixelType.BGRA8packed;
                    IntPtr ptrBmp = bmpData.Scan0;
                      byte[] buffer1 = m_frameList[0].PixelData as byte[];
                     converter.Convert<byte>(buffer1, m_frameList[0]); //Exception handling TODO
                     bitmap.UnlockBits(bmpData);


                     return bitmap;
                }
            }
            return null; /* No image available. */
        }
        private void OnConnectionLost(Object sender, EventArgs e)
        {
            

     
        }


        // Occurs when the connection to a camera device is opened.
        private void OnCameraOpened(Object sender, EventArgs e)
        {
           
        }


        // Occurs when the connection to a camera device is closed.
        private void OnCameraClosed(Object sender, EventArgs e)
        {
            
        }


        // Occurs when a camera starts grabbing.
        private void OnGrabStarted(Object sender, EventArgs e)
        {


            // Reset the stopwatch used to reduce the amount of displayed images. The camera may acquire images faster than the images can be displayed.
            OnGrabbingStartedEvent();
            stopWatch.Reset();

            
        }


        // Occurs when an image has been acquired and is ready to be processed.
        private void OnImageGrabbed(Object sender, ImageGrabbedEventArgs e)
        {
            

            try
            {
                // Acquire the image from the camera. Only show the latest image. The camera may acquire images faster than the images can be displayed.

                // Get the grab result.
                //e.GrabResult.Dispose();
                IGrabResult grabResult = e.GrabResult;
                // Check if the image can be displayed.
                if (grabResult.IsValid)
                {
                    _GrabbedRawData = grabResult.Clone();
                    lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
                    {
                        m_frameList.Add(_GrabbedRawData);
                        if (m_frameList.Count > 5)
                        {
                            m_frameList[0].Dispose();
                            m_frameList.RemoveAt(0);
                        }
                    }
                    if (!stopWatch.IsRunning || stopWatch.ElapsedMilliseconds > 33)
                    {
                        stopWatch.Restart();
                    }
                    OnImageReadyEvent();
                    // Reduce the number of displayed images to a reasonable amount if the camera is acquiring images very fast.
                    //if (!stopWatch.IsRunning || stopWatch.ElapsedMilliseconds > 33)
                    //{
                    //    stopWatch.Restart();

                        //    Bitmap bitmap = new Bitmap(grabResult.Width, grabResult.Height, PixelFormat.Format32bppRgb);
                        //    // Lock the bits of the bitmap.
                        //    BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                        //    // Place the pointer to the buffer of the bitmap.
                        //    converter.OutputPixelFormat = PixelType.BGRA8packed;
                        //    IntPtr ptrBmp = bmpData.Scan0;
                        //    converter.Convert(ptrBmp, bmpData.Stride * bitmap.Height, grabResult); //Exception handling TODO
                        //    bitmap.UnlockBits(bmpData);
                        //    OnImageReadyEvent();
                        //    byte[] buffer = grabResult.PixelData as byte[];

                        //}
                }
            }
            catch (Exception exception)
            {
                ShowException(exception);
            }
            finally
            {
                // Dispose the grab result if needed for returning it to the grab loop.
                e.DisposeGrabResultIfClone();
            }
        }


        // Occurs when a camera has stopped grabbing.
        private void OnGrabStopped(Object sender, GrabStopEventArgs e)
        {
            

            // Reset the stopwatch.
            stopWatch.Reset();

            // Re-enable the updating of the device list.
           
        }
        
    }
}
