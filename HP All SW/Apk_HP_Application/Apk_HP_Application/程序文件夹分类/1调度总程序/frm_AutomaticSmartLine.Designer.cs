﻿namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    partial class frm_AutomaticSmartLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSplashScreen.SplashScreenManager ScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Apk_HP_Application.程序文件夹分类._1调度总程序.SplashScreen1), true, true);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AutomaticSmartLine));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.NP_主页运行 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.txtLastTrayID = new DevExpress.XtraEditors.TextEdit();
            this.btm进入尾料模式 = new DevExpress.XtraEditors.CheckButton();
            this.LastProductModeLED = new HslCommunication.Controls.UserLantern();
            this.btmChangeWorkOrder = new DevExpress.XtraEditors.SimpleButton();
            this.LED_ProcessServerStatus = new HslCommunication.Controls.UserLantern();
            this.btmInvoke_ClearErrorMachine = new DevExpress.XtraEditors.SimpleButton();
            this.checkButton1 = new DevExpress.XtraEditors.CheckButton();
            this.imageList16 = new System.Windows.Forms.ImageList(this.components);
            this.伺服启用禁用 = new DevExpress.XtraEditors.CheckButton();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btmInvoke_StopMachine = new DevExpress.XtraEditors.SimpleButton();
            this.btmInvoke_StartMachine = new DevExpress.XtraEditors.SimpleButton();
            this.btmInvoke_ResetMachine = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.chkReadyPacking = new DevExpress.XtraEditors.CheckEdit();
            this.chkReadyUnload = new DevExpress.XtraEditors.CheckEdit();
            this.chkReadyLaser = new DevExpress.XtraEditors.CheckEdit();
            this.chkReadyFlash2 = new DevExpress.XtraEditors.CheckEdit();
            this.chkReadyFlash1 = new DevExpress.XtraEditors.CheckEdit();
            this.chkReadyLoad = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.chkWriteWorkSheetToSQL = new System.Windows.Forms.CheckBox();
            this.txtTestPC = new DevExpress.XtraEditors.TextEdit();
            this.btmPrintLable = new DevExpress.XtraEditors.SimpleButton();
            this.获取工单信息 = new DevExpress.XtraEditors.SimpleButton();
            this.txtWorkID = new DevExpress.XtraEditors.TextEdit();
            this.propertyProcess = new System.Windows.Forms.PropertyGrid();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.NP_设置 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.chkFocePackingSNOK = new System.Windows.Forms.CheckBox();
            this.chkEnableLastMode = new System.Windows.Forms.CheckBox();
            this.强制MES验证通过 = new System.Windows.Forms.CheckBox();
            this.txtSNLength = new System.Windows.Forms.TextBox();
            this.不扫描条码 = new System.Windows.Forms.CheckBox();
            this.txtSfcSNPrefix = new System.Windows.Forms.TextBox();
            this.chkSfcLength = new System.Windows.Forms.CheckBox();
            this.chkSfcPrefix = new System.Windows.Forms.CheckBox();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.memoEdit3 = new DevExpress.XtraEditors.MemoEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIPAddr = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.NP_监控 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.cmbPackingPCBs = new System.Windows.Forms.ListBox();
            this.groupControl18 = new DevExpress.XtraEditors.GroupControl();
            this.txt已识别的好品数量 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit6 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.label15 = new System.Windows.Forms.Label();
            this.当前标签包装数量1068 = new DevExpress.XtraEditors.SpinEdit();
            this.brm刷新最后打码标志 = new DevExpress.XtraEditors.SimpleButton();
            this.spinEdit5 = new DevExpress.XtraEditors.SpinEdit();
            this.gridFlagMark = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btmSetLastTrayRemaindCount = new DevExpress.XtraEditors.SimpleButton();
            this.label16 = new System.Windows.Forms.Label();
            this.sped最后托盘剩余个数 = new DevExpress.XtraEditors.SpinEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.spedLaserRemaind = new DevExpress.XtraEditors.SpinEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.spinEdit3 = new DevExpress.XtraEditors.SpinEdit();
            this.复位所有烧录结果数据 = new DevExpress.XtraEditors.SimpleButton();
            this.label12 = new System.Windows.Forms.Label();
            this.sped生产应上盘数 = new DevExpress.XtraEditors.SpinEdit();
            this.spedPackingPackedCount2 = new DevExpress.XtraEditors.SpinEdit();
            this.spedUnloadPackedCount2 = new DevExpress.XtraEditors.SpinEdit();
            this.btmClearFailed = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.sped生产实际总盘数 = new DevExpress.XtraEditors.SpinEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.sped当前坏品总数量 = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.spedUnloadPackedCountSet = new DevExpress.XtraEditors.SpinEdit();
            this.btmSetPackingCount = new DevExpress.XtraEditors.SimpleButton();
            this.spedPackingPackedCountSet = new DevExpress.XtraEditors.SpinEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.spedPackingPackedCount = new DevExpress.XtraEditors.SpinEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.spedUnloadPackedCount = new DevExpress.XtraEditors.SpinEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.TotalLineOutAll = new DevExpress.XtraEditors.SpinEdit();
            this.speCountOfBuffTray = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.Sped生产总量 = new DevExpress.XtraEditors.SpinEdit();
            this.speCountOfLoadTray = new DevExpress.XtraEditors.SpinEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.sped产品行数 = new DevExpress.XtraEditors.SpinEdit();
            this.btmSetCurrentTrayOut = new DevExpress.XtraEditors.SimpleButton();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.TotalTrayOfLineOutPCS = new DevExpress.XtraEditors.SpinEdit();
            this.UnloadCfg_CountTrayBK = new DevExpress.XtraEditors.SpinEdit();
            this.spedSetTrayOuted = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.TotalTrayOfLineOut = new DevExpress.XtraEditors.SpinEdit();
            this.sped生产总盘数 = new DevExpress.XtraEditors.SpinEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.Sped包装总量 = new DevExpress.XtraEditors.SpinEdit();
            this.CurrentCountTray = new DevExpress.XtraEditors.SpinEdit();
            this.生产最后打包总盘数 = new DevExpress.XtraEditors.SpinEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.sped产品例数 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit4 = new DevExpress.XtraEditors.SpinEdit();
            this.btmGetCurrentWorkSheet = new DevExpress.XtraEditors.SimpleButton();
            this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbUnloadPCBs = new System.Windows.Forms.ListBox();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2014 = new DevExpress.XtraEditors.TextEdit();
            this.txtD2001 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2008 = new DevExpress.XtraEditors.TextEdit();
            this.txtD2009 = new DevExpress.XtraEditors.TextEdit();
            this.txtD2011 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2004 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2010 = new DevExpress.XtraEditors.TextEdit();
            this.txtD2003 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2013 = new DevExpress.XtraEditors.TextEdit();
            this.txtD2005 = new DevExpress.XtraEditors.TextEdit();
            this.txtD2012 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2002 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2006 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.txtD2007 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.NP_手动调试 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.手动获取当前选中工单及转工单 = new DevExpress.XtraEditors.SimpleButton();
            this.btmLoad = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.btmSNTriggerPacking = new DevExpress.XtraEditors.SimpleButton();
            this.btmSNTriggerUnload = new DevExpress.XtraEditors.SimpleButton();
            this.btmSNTriggerLaser = new DevExpress.XtraEditors.SimpleButton();
            this.btmSNTriggerFlash2 = new DevExpress.XtraEditors.SimpleButton();
            this.btmSNTriggerFlash1 = new DevExpress.XtraEditors.SimpleButton();
            this.btmSNTriggerLoad = new DevExpress.XtraEditors.SimpleButton();
            this.txtSN_B3 = new DevExpress.XtraEditors.TextEdit();
            this.txtSN_B2 = new DevExpress.XtraEditors.TextEdit();
            this.txtSN_B1 = new DevExpress.XtraEditors.TextEdit();
            this.txtSN_A3 = new DevExpress.XtraEditors.TextEdit();
            this.txtSN_A2 = new DevExpress.XtraEditors.TextEdit();
            this.txtSN_A1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit2 = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.NP_生产数据查询 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.btmReconnectSQL = new DevExpress.XtraEditors.SimpleButton();
            this.btm工单信息刷新 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl14 = new DevExpress.XtraEditors.GroupControl();
            this.txtPCBIDForFind = new DevExpress.XtraEditors.TextEdit();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtTrayIDForFind = new DevExpress.XtraEditors.TextEdit();
            this.btmRefencePacking = new DevExpress.XtraEditors.SimpleButton();
            this.btmRefenceTrayID = new DevExpress.XtraEditors.SimpleButton();
            this.btmReferenceResults = new DevExpress.XtraEditors.SimpleButton();
            this.tabPane2 = new DevExpress.XtraBars.Navigation.TabPane();
            this.Tab_DB_Load = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.groupControl15 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl13 = new DevExpress.XtraEditors.GroupControl();
            this.btm当前工单完成 = new DevExpress.XtraEditors.SimpleButton();
            this.btm得到最后打码标志 = new DevExpress.XtraEditors.SimpleButton();
            this.输入包装信息 = new DevExpress.XtraEditors.SimpleButton();
            this.得到烧录TRYA工位结果 = new DevExpress.XtraEditors.SimpleButton();
            this.得到烧录工位结果 = new DevExpress.XtraEditors.SimpleButton();
            this.设置烧录工位结果 = new DevExpress.XtraEditors.SimpleButton();
            this.得到托盘当前结果 = new DevExpress.XtraEditors.SimpleButton();
            this.得到PCBID当前结果 = new DevExpress.XtraEditors.SimpleButton();
            this.得到PCBID当前状态 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.得到托盘当前状态 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit4 = new DevExpress.XtraEditors.MemoEdit();
            this.设置工位结果数据 = new DevExpress.XtraEditors.SimpleButton();
            this.txtPCB板号 = new DevExpress.XtraEditors.TextEdit();
            this.chkStationResultStatus = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.cmbEmStation = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtStatusDisplay = new DevExpress.XtraEditors.TextEdit();
            this.手动解盘 = new DevExpress.XtraEditors.SimpleButton();
            this.设置工位状态 = new DevExpress.XtraEditors.SimpleButton();
            this.手动绑盘 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.自动生成PCBID = new DevExpress.XtraEditors.SimpleButton();
            this.txtPCB虚拟编号 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.txt托盘编号 = new DevExpress.XtraEditors.TextEdit();
            this.Tab_DB_Flash = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.Tab_DB_Laser = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.Tab_DB_Robot = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.Tab_DB_Tray = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Tab_DB_Result = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Tab_DB_Packing = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.Tab_DB_ORDER = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.NP_工艺参数 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.保存时写入所有信息 = new DevExpress.XtraEditors.CheckEdit();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNG_上料调度 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.groupControl12 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.chkRealUpdateWorkSheet = new DevExpress.XtraEditors.CheckEdit();
            this.spePCBDstCol = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.spePCBDstRow = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.spePCBWidth = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.spePCBLength = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.speDstMaduo = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.speTrayWidth = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.speTrayLength = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.btmWriteUnload = new DevExpress.XtraEditors.SimpleButton();
            this.label6 = new System.Windows.Forms.Label();
            this.UnloadCfg_TotalProduct = new DevExpress.XtraEditors.SpinEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.UnloadCfg_Remaind = new DevExpress.XtraEditors.SpinEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.UnloadCfg_Total = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.txtPCBPNNumber = new DevExpress.XtraEditors.TextEdit();
            this.UnloadCfg_Col = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.UnloadCfg_Row = new DevExpress.XtraEditors.SpinEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.UnloadCfg_CountTray = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.txtWorkOrder2 = new DevExpress.XtraEditors.TextEdit();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.烧录2是否翻转 = new DevExpress.XtraEditors.CheckEdit();
            this.btmAllOn = new DevExpress.XtraEditors.SimpleButton();
            this.上料是否翻转 = new DevExpress.XtraEditors.CheckEdit();
            this.btmAllOff = new DevExpress.XtraEditors.SimpleButton();
            this.烧录是否翻转 = new DevExpress.XtraEditors.CheckEdit();
            this.打码切割是否翻转 = new DevExpress.XtraEditors.CheckEdit();
            this.下料组盘是否翻转 = new DevExpress.XtraEditors.CheckEdit();
            this.包装是否翻转 = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.tabNG_烧录 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.txtFlashNumFile = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.chk切换行例数据 = new DevExpress.XtraEditors.CheckEdit();
            this.txtFlashMenuFile = new System.Windows.Forms.ComboBox();
            this.btmGetFalshPro = new DevExpress.XtraEditors.SimpleButton();
            this.button2 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.tabNG_打码切割 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.spinEdit8 = new DevExpress.XtraEditors.SpinEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.txtMarkText = new DevExpress.XtraEditors.TextEdit();
            this.tabNG_下料组盘 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.chkEnableMarkTest = new DevExpress.XtraEditors.CheckEdit();
            this.chkLaserMarkTest_Down = new DevExpress.XtraEditors.CheckEdit();
            this.chkLaserMarkTest_Up = new DevExpress.XtraEditors.CheckEdit();
            this.tabNG_打标包装 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btmSaveInvokeCfg = new DevExpress.XtraEditors.SimpleButton();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.sped最后托盘剩余个数2 = new DevExpress.XtraEditors.SpinEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.spedLaserRemaind2 = new DevExpress.XtraEditors.SpinEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.sped生产应上盘数2 = new DevExpress.XtraEditors.SpinEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.sped生产实际总盘数2 = new DevExpress.XtraEditors.SpinEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.sped当前坏品总数量2 = new DevExpress.XtraEditors.SpinEdit();
            this.TotalLineOutAll2 = new DevExpress.XtraEditors.SpinEdit();
            this.Sped生产总量2 = new DevExpress.XtraEditors.SpinEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.sped生产总盘数2 = new DevExpress.XtraEditors.SpinEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.label25 = new System.Windows.Forms.Label();
            this.良品率 = new DevExpress.XtraEditors.TextEdit();
            this.TestFunction = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.NP_主页运行.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastTrayID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyPacking.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyUnload.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyLaser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyFlash2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyFlash1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyLoad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTestPC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.NP_设置.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).BeginInit();
            this.NP_监控.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).BeginInit();
            this.groupControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt已识别的好品数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.当前标签包装数量1068.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridFlagMark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped最后托盘剩余个数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLaserRemaind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产应上盘数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedPackingPackedCount2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedUnloadPackedCount2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产实际总盘数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped当前坏品总数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedUnloadPackedCountSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedPackingPackedCountSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedPackingPackedCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedUnloadPackedCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLineOutAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfBuffTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sped生产总量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfLoadTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped产品行数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOutPCS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTrayBK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedSetTrayOuted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产总盘数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sped包装总量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentCountTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.生产最后打包总盘数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped产品例数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2014.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2001.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2008.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2009.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2011.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2004.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2010.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2003.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2013.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2005.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2012.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2002.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2006.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2007.Properties)).BeginInit();
            this.NP_手动调试.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_B3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_B2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_B1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_A3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_A2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_A1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            this.NP_生产数据查询.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).BeginInit();
            this.groupControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBIDForFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayIDForFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane2)).BeginInit();
            this.tabPane2.SuspendLayout();
            this.Tab_DB_Load.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).BeginInit();
            this.groupControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCB板号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStationResultStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEmStation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusDisplay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCB虚拟编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt托盘编号.Properties)).BeginInit();
            this.Tab_DB_Tray.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.Tab_DB_Result.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.Tab_DB_Packing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.Tab_DB_ORDER.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.NP_工艺参数.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.保存时写入所有信息.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNG_上料调度.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).BeginInit();
            this.groupControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRealUpdateWorkSheet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBDstCol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBDstRow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speDstMaduo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTrayWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTrayLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_TotalProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Remaind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Total.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBPNNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Col.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Row.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkOrder2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.烧录2是否翻转.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.上料是否翻转.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.烧录是否翻转.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.打码切割是否翻转.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.下料组盘是否翻转.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.包装是否翻转.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.tabNG_烧录.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk切换行例数据.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.tabNG_打码切割.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarkText.Properties)).BeginInit();
            this.tabNG_下料组盘.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableMarkTest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Down.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Up.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped最后托盘剩余个数2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLaserRemaind2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产应上盘数2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产实际总盘数2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped当前坏品总数量2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLineOutAll2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sped生产总量2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产总盘数2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.良品率.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ScreenManager
            // 
            ScreenManager.ClosingDelay = 500;
            // 
            // navigationPane1
            // 
            this.navigationPane1.AllowHtmlDraw = true;
            this.navigationPane1.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.Appearance.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Hovered.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.AppearanceButton.Hovered.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Normal.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.AppearanceButton.Normal.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Pressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.navigationPane1.AppearanceButton.Pressed.Options.UseFont = true;
            this.navigationPane1.Controls.Add(this.NP_主页运行);
            this.navigationPane1.Controls.Add(this.NP_设置);
            this.navigationPane1.Controls.Add(this.NP_监控);
            this.navigationPane1.Controls.Add(this.NP_手动调试);
            this.navigationPane1.Controls.Add(this.NP_生产数据查询);
            this.navigationPane1.Controls.Add(this.NP_工艺参数);
            this.navigationPane1.Location = new System.Drawing.Point(0, 0);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AllowBorderColorBlending = true;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.NP_主页运行,
            this.NP_设置,
            this.NP_工艺参数,
            this.NP_手动调试,
            this.NP_监控,
            this.NP_生产数据查询});
            this.navigationPane1.RegularSize = new System.Drawing.Size(1904, 706);
            this.navigationPane1.RibbonAndBarsMergeStyle = DevExpress.XtraBars.Docking2010.Views.RibbonAndBarsMergeStyle.Always;
            this.navigationPane1.SelectedPage = this.NP_主页运行;
            this.navigationPane1.ShowToolTips = DevExpress.Utils.DefaultBoolean.True;
            this.navigationPane1.Size = new System.Drawing.Size(1904, 706);
            this.navigationPane1.TabIndex = 0;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // NP_主页运行
            // 
            this.NP_主页运行.Caption = " 主页运行";
            this.NP_主页运行.Controls.Add(this.txtLastTrayID);
            this.NP_主页运行.Controls.Add(this.btm进入尾料模式);
            this.NP_主页运行.Controls.Add(this.LastProductModeLED);
            this.NP_主页运行.Controls.Add(this.btmChangeWorkOrder);
            this.NP_主页运行.Controls.Add(this.LED_ProcessServerStatus);
            this.NP_主页运行.Controls.Add(this.btmInvoke_ClearErrorMachine);
            this.NP_主页运行.Controls.Add(this.checkButton1);
            this.NP_主页运行.Controls.Add(this.伺服启用禁用);
            this.NP_主页运行.Controls.Add(this.labelControl16);
            this.NP_主页运行.Controls.Add(this.labelControl15);
            this.NP_主页运行.Controls.Add(this.labelControl14);
            this.NP_主页运行.Controls.Add(this.labelControl13);
            this.NP_主页运行.Controls.Add(this.labelControl6);
            this.NP_主页运行.Controls.Add(this.labelControl5);
            this.NP_主页运行.Controls.Add(this.btmInvoke_StopMachine);
            this.NP_主页运行.Controls.Add(this.btmInvoke_StartMachine);
            this.NP_主页运行.Controls.Add(this.btmInvoke_ResetMachine);
            this.NP_主页运行.Controls.Add(this.panelControl6);
            this.NP_主页运行.Controls.Add(this.panelControl5);
            this.NP_主页运行.Controls.Add(this.panelControl4);
            this.NP_主页运行.Controls.Add(this.panelControl3);
            this.NP_主页运行.Controls.Add(this.panelControl2);
            this.NP_主页运行.Controls.Add(this.panelControl1);
            this.NP_主页运行.Controls.Add(this.checkEdit7);
            this.NP_主页运行.Controls.Add(this.chkReadyPacking);
            this.NP_主页运行.Controls.Add(this.chkReadyUnload);
            this.NP_主页运行.Controls.Add(this.chkReadyLaser);
            this.NP_主页运行.Controls.Add(this.chkReadyFlash2);
            this.NP_主页运行.Controls.Add(this.chkReadyFlash1);
            this.NP_主页运行.Controls.Add(this.chkReadyLoad);
            this.NP_主页运行.Controls.Add(this.popupContainerControl1);
            this.NP_主页运行.Controls.Add(this.popupContainerEdit1);
            this.NP_主页运行.Controls.Add(this.pictureBox1);
            this.NP_主页运行.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.NP_主页运行.Image = ((System.Drawing.Image)(resources.GetObject("NP_主页运行.Image")));
            this.NP_主页运行.Name = "NP_主页运行";
            this.NP_主页运行.Size = new System.Drawing.Size(1706, 646);
            // 
            // txtLastTrayID
            // 
            this.txtLastTrayID.EditValue = "";
            this.txtLastTrayID.Location = new System.Drawing.Point(600, 181);
            this.txtLastTrayID.Name = "txtLastTrayID";
            this.txtLastTrayID.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastTrayID.Properties.Appearance.Options.UseFont = true;
            this.txtLastTrayID.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtLastTrayID.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtLastTrayID.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtLastTrayID.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtLastTrayID.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtLastTrayID.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtLastTrayID.Properties.ReadOnly = true;
            this.txtLastTrayID.Size = new System.Drawing.Size(265, 24);
            this.txtLastTrayID.TabIndex = 221;
            // 
            // btm进入尾料模式
            // 
            this.btm进入尾料模式.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btm进入尾料模式.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btm进入尾料模式.Appearance.Options.UseFont = true;
            this.btm进入尾料模式.Appearance.Options.UseForeColor = true;
            this.btm进入尾料模式.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm进入尾料模式.AppearanceDisabled.ForeColor = System.Drawing.Color.Blue;
            this.btm进入尾料模式.AppearanceDisabled.Options.UseFont = true;
            this.btm进入尾料模式.AppearanceDisabled.Options.UseForeColor = true;
            this.btm进入尾料模式.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm进入尾料模式.AppearanceHovered.ForeColor = System.Drawing.Color.Blue;
            this.btm进入尾料模式.AppearanceHovered.Options.UseFont = true;
            this.btm进入尾料模式.AppearanceHovered.Options.UseForeColor = true;
            this.btm进入尾料模式.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm进入尾料模式.AppearancePressed.ForeColor = System.Drawing.Color.Fuchsia;
            this.btm进入尾料模式.AppearancePressed.Options.UseFont = true;
            this.btm进入尾料模式.AppearancePressed.Options.UseForeColor = true;
            this.btm进入尾料模式.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btm进入尾料模式.Location = new System.Drawing.Point(600, 105);
            this.btm进入尾料模式.Name = "btm进入尾料模式";
            this.btm进入尾料模式.Size = new System.Drawing.Size(265, 70);
            this.btm进入尾料模式.TabIndex = 220;
            this.btm进入尾料模式.Text = "手动进入尾料模式";
            this.btm进入尾料模式.CheckedChanged += new System.EventHandler(this.btm进入尾料模式_CheckedChanged);
            // 
            // LastProductModeLED
            // 
            this.LastProductModeLED.BackColor = System.Drawing.Color.Transparent;
            this.LastProductModeLED.LanternBackground = System.Drawing.Color.Gray;
            this.LastProductModeLED.Location = new System.Drawing.Point(867, 105);
            this.LastProductModeLED.Margin = new System.Windows.Forms.Padding(15, 21, 15, 21);
            this.LastProductModeLED.Name = "LastProductModeLED";
            this.LastProductModeLED.Size = new System.Drawing.Size(72, 72);
            this.LastProductModeLED.TabIndex = 177;
            // 
            // btmChangeWorkOrder
            // 
            this.btmChangeWorkOrder.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmChangeWorkOrder.Appearance.Options.UseFont = true;
            this.btmChangeWorkOrder.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmChangeWorkOrder.AppearanceDisabled.Options.UseFont = true;
            this.btmChangeWorkOrder.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmChangeWorkOrder.AppearanceHovered.Options.UseFont = true;
            this.btmChangeWorkOrder.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmChangeWorkOrder.AppearancePressed.Options.UseFont = true;
            this.btmChangeWorkOrder.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmChangeWorkOrder.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmChangeWorkOrder.ImageOptions.Image")));
            this.btmChangeWorkOrder.Location = new System.Drawing.Point(0, 43);
            this.btmChangeWorkOrder.LookAndFeel.SkinName = "Office 2007 Blue";
            this.btmChangeWorkOrder.Name = "btmChangeWorkOrder";
            this.btmChangeWorkOrder.Size = new System.Drawing.Size(365, 83);
            this.btmChangeWorkOrder.TabIndex = 176;
            this.btmChangeWorkOrder.Text = "转工单  (准备好工单号并放在指示牌下)";
            this.btmChangeWorkOrder.Click += new System.EventHandler(this.btmChangeWorkOrder_Click);
            // 
            // LED_ProcessServerStatus
            // 
            this.LED_ProcessServerStatus.BackColor = System.Drawing.Color.Transparent;
            this.LED_ProcessServerStatus.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ProcessServerStatus.Location = new System.Drawing.Point(368, 7);
            this.LED_ProcessServerStatus.Margin = new System.Windows.Forms.Padding(7, 9, 7, 9);
            this.LED_ProcessServerStatus.Name = "LED_ProcessServerStatus";
            this.LED_ProcessServerStatus.Size = new System.Drawing.Size(24, 24);
            this.LED_ProcessServerStatus.TabIndex = 175;
            // 
            // btmInvoke_ClearErrorMachine
            // 
            this.btmInvoke_ClearErrorMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmInvoke_ClearErrorMachine.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ClearErrorMachine.Appearance.Image")));
            this.btmInvoke_ClearErrorMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_ClearErrorMachine.Appearance.Options.UseImage = true;
            this.btmInvoke_ClearErrorMachine.Appearance.Options.UseTextOptions = true;
            this.btmInvoke_ClearErrorMachine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btmInvoke_ClearErrorMachine.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btmInvoke_ClearErrorMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ClearErrorMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_ClearErrorMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ClearErrorMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_ClearErrorMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ClearErrorMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_ClearErrorMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_ClearErrorMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ClearErrorMachine.ImageOptions.Image")));
            this.btmInvoke_ClearErrorMachine.Location = new System.Drawing.Point(1128, 29);
            this.btmInvoke_ClearErrorMachine.Name = "btmInvoke_ClearErrorMachine";
            this.btmInvoke_ClearErrorMachine.Size = new System.Drawing.Size(184, 70);
            this.btmInvoke_ClearErrorMachine.TabIndex = 174;
            this.btmInvoke_ClearErrorMachine.Text = "清除所有报警";
            this.btmInvoke_ClearErrorMachine.Click += new System.EventHandler(this.btmInvoke_ClearErrorMachine_Click);
            // 
            // checkButton1
            // 
            this.checkButton1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkButton1.Appearance.Options.UseFont = true;
            this.checkButton1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearanceDisabled.Options.UseFont = true;
            this.checkButton1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearanceHovered.Options.UseFont = true;
            this.checkButton1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearancePressed.Options.UseFont = true;
            this.checkButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.checkButton1.ImageOptions.ImageIndex = 0;
            this.checkButton1.ImageOptions.ImageList = this.imageList16;
            this.checkButton1.Location = new System.Drawing.Point(996, 29);
            this.checkButton1.Name = "checkButton1";
            this.checkButton1.Size = new System.Drawing.Size(133, 70);
            this.checkButton1.TabIndex = 173;
            this.checkButton1.Text = "急停关闭/打开";
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16.Images.SetKeyName(0, "emblem-nowrite.png");
            this.imageList16.Images.SetKeyName(1, "emblem-ok.png");
            this.imageList16.Images.SetKeyName(2, "stock_lock-ok.png");
            this.imageList16.Images.SetKeyName(3, "stock_lock-open.png");
            this.imageList16.Images.SetKeyName(4, "stock_music-library.png");
            this.imageList16.Images.SetKeyName(5, "stock_new-drawing.png");
            this.imageList16.Images.SetKeyName(6, "stock_new-formula.png");
            this.imageList16.Images.SetKeyName(7, "stock_new-html.png");
            this.imageList16.Images.SetKeyName(8, "stock_new-master-document.png");
            this.imageList16.Images.SetKeyName(9, "stock_new-presentation.png");
            // 
            // 伺服启用禁用
            // 
            this.伺服启用禁用.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.伺服启用禁用.Appearance.Options.UseFont = true;
            this.伺服启用禁用.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.伺服启用禁用.AppearanceDisabled.Options.UseFont = true;
            this.伺服启用禁用.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.伺服启用禁用.AppearanceHovered.Options.UseFont = true;
            this.伺服启用禁用.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.伺服启用禁用.AppearancePressed.Options.UseFont = true;
            this.伺服启用禁用.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.伺服启用禁用.ImageOptions.ImageIndex = 1;
            this.伺服启用禁用.ImageOptions.ImageList = this.imageList16;
            this.伺服启用禁用.Location = new System.Drawing.Point(864, 29);
            this.伺服启用禁用.Name = "伺服启用禁用";
            this.伺服启用禁用.Size = new System.Drawing.Size(133, 70);
            this.伺服启用禁用.TabIndex = 172;
            this.伺服启用禁用.Text = "安全门关闭/打开";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Gold;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl16.AppearanceDisabled.Options.UseFont = true;
            this.labelControl16.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl16.AppearanceHovered.Options.UseFont = true;
            this.labelControl16.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl16.AppearancePressed.Options.UseFont = true;
            this.labelControl16.Location = new System.Drawing.Point(1444, 600);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(112, 21);
            this.labelControl16.TabIndex = 107;
            this.labelControl16.Text = "全自动包装工位";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Gold;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl15.AppearanceDisabled.Options.UseFont = true;
            this.labelControl15.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl15.AppearanceHovered.Options.UseFont = true;
            this.labelControl15.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl15.AppearancePressed.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(1178, 600);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(96, 21);
            this.labelControl15.TabIndex = 109;
            this.labelControl15.Text = "下料组盘工位";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Gold;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl14.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Appearance.Options.UseForeColor = true;
            this.labelControl14.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl14.AppearanceDisabled.Options.UseFont = true;
            this.labelControl14.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl14.AppearanceHovered.Options.UseFont = true;
            this.labelControl14.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl14.AppearancePressed.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(205, 600);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(128, 21);
            this.labelControl14.TabIndex = 108;
            this.labelControl14.Text = "人工半自动上料位";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Gold;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Appearance.Options.UseForeColor = true;
            this.labelControl13.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl13.AppearanceDisabled.Options.UseFont = true;
            this.labelControl13.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl13.AppearanceHovered.Options.UseFont = true;
            this.labelControl13.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl13.AppearancePressed.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(472, 600);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(73, 21);
            this.labelControl13.TabIndex = 107;
            this.labelControl13.Text = "烧录工位1";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Gold;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl6.AppearanceDisabled.Options.UseFont = true;
            this.labelControl6.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl6.AppearanceHovered.Options.UseFont = true;
            this.labelControl6.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl6.AppearancePressed.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(717, 600);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(73, 21);
            this.labelControl6.TabIndex = 106;
            this.labelControl6.Text = "烧录工位2";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Gold;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            this.labelControl5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl5.AppearanceDisabled.Options.UseFont = true;
            this.labelControl5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl5.AppearanceHovered.Options.UseFont = true;
            this.labelControl5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl5.AppearancePressed.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(955, 600);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(80, 21);
            this.labelControl5.TabIndex = 105;
            this.labelControl5.Text = "打码切割位";
            // 
            // btmInvoke_StopMachine
            // 
            this.btmInvoke_StopMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_StopMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_StopMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_StopMachine.ImageOptions.Image")));
            this.btmInvoke_StopMachine.Location = new System.Drawing.Point(732, 29);
            this.btmInvoke_StopMachine.Name = "btmInvoke_StopMachine";
            this.btmInvoke_StopMachine.Size = new System.Drawing.Size(133, 70);
            this.btmInvoke_StopMachine.TabIndex = 104;
            this.btmInvoke_StopMachine.Text = "停止";
            this.btmInvoke_StopMachine.Click += new System.EventHandler(this.btmInvoke_StopMachine_Click);
            // 
            // btmInvoke_StartMachine
            // 
            this.btmInvoke_StartMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_StartMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_StartMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_StartMachine.ImageOptions.Image")));
            this.btmInvoke_StartMachine.Location = new System.Drawing.Point(600, 29);
            this.btmInvoke_StartMachine.Name = "btmInvoke_StartMachine";
            this.btmInvoke_StartMachine.Size = new System.Drawing.Size(133, 70);
            this.btmInvoke_StartMachine.TabIndex = 103;
            this.btmInvoke_StartMachine.Text = "启动";
            this.btmInvoke_StartMachine.Click += new System.EventHandler(this.btmInvoke_StartMachine_Click);
            // 
            // btmInvoke_ResetMachine
            // 
            this.btmInvoke_ResetMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmInvoke_ResetMachine.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ResetMachine.Appearance.Image")));
            this.btmInvoke_ResetMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_ResetMachine.Appearance.Options.UseImage = true;
            this.btmInvoke_ResetMachine.Appearance.Options.UseTextOptions = true;
            this.btmInvoke_ResetMachine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btmInvoke_ResetMachine.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btmInvoke_ResetMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_ResetMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_ResetMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_ResetMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_ResetMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ResetMachine.ImageOptions.Image")));
            this.btmInvoke_ResetMachine.Location = new System.Drawing.Point(468, 29);
            this.btmInvoke_ResetMachine.Name = "btmInvoke_ResetMachine";
            this.btmInvoke_ResetMachine.Size = new System.Drawing.Size(133, 70);
            this.btmInvoke_ResetMachine.TabIndex = 102;
            this.btmInvoke_ResetMachine.Text = "复位";
            this.btmInvoke_ResetMachine.Click += new System.EventHandler(this.btmInvoke_ResetMachine_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.panelControl6.Appearance.Options.UseFont = true;
            this.panelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl6.Location = new System.Drawing.Point(162, 474);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(226, 108);
            this.panelControl6.TabIndex = 100;
            // 
            // panelControl5
            // 
            this.panelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.panelControl5.Appearance.Options.UseFont = true;
            this.panelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl5.Location = new System.Drawing.Point(400, 474);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(226, 108);
            this.panelControl5.TabIndex = 99;
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.panelControl4.Appearance.Options.UseFont = true;
            this.panelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl4.Location = new System.Drawing.Point(640, 474);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(226, 108);
            this.panelControl4.TabIndex = 98;
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelControl3.Appearance.Options.UseFont = true;
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Location = new System.Drawing.Point(882, 474);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(226, 108);
            this.panelControl3.TabIndex = 97;
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.panelControl2.Appearance.Options.UseFont = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Location = new System.Drawing.Point(1121, 474);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(226, 108);
            this.panelControl2.TabIndex = 96;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.panelControl1.Appearance.Options.UseFont = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Location = new System.Drawing.Point(1380, 474);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(226, 108);
            this.panelControl1.TabIndex = 95;
            // 
            // checkEdit7
            // 
            this.checkEdit7.EditValue = true;
            this.checkEdit7.Location = new System.Drawing.Point(1639, -2);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.AutoWidth = true;
            this.checkEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.checkEdit7.Properties.Caption = "";
            this.checkEdit7.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.checkEdit7.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("checkEdit7.Properties.PictureChecked")));
            this.checkEdit7.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("checkEdit7.Properties.PictureUnchecked")));
            this.checkEdit7.Properties.ReadOnly = true;
            this.checkEdit7.Size = new System.Drawing.Size(76, 76);
            this.checkEdit7.TabIndex = 90;
            // 
            // chkReadyPacking
            // 
            this.chkReadyPacking.Location = new System.Drawing.Point(1579, 187);
            this.chkReadyPacking.Name = "chkReadyPacking";
            this.chkReadyPacking.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReadyPacking.Properties.Appearance.Options.UseFont = true;
            this.chkReadyPacking.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyPacking.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkReadyPacking.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyPacking.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkReadyPacking.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyPacking.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkReadyPacking.Properties.Caption = "";
            this.chkReadyPacking.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkReadyPacking.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("chkReadyPacking.Properties.PictureChecked")));
            this.chkReadyPacking.Properties.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("chkReadyPacking.Properties.PictureGrayed")));
            this.chkReadyPacking.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("chkReadyPacking.Properties.PictureUnchecked")));
            this.chkReadyPacking.Size = new System.Drawing.Size(16, 20);
            this.chkReadyPacking.TabIndex = 87;
            // 
            // chkReadyUnload
            // 
            this.chkReadyUnload.Location = new System.Drawing.Point(1327, 169);
            this.chkReadyUnload.Name = "chkReadyUnload";
            this.chkReadyUnload.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReadyUnload.Properties.Appearance.Options.UseFont = true;
            this.chkReadyUnload.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyUnload.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkReadyUnload.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyUnload.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkReadyUnload.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyUnload.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkReadyUnload.Properties.Caption = "";
            this.chkReadyUnload.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkReadyUnload.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("chkReadyUnload.Properties.PictureChecked")));
            this.chkReadyUnload.Properties.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("chkReadyUnload.Properties.PictureGrayed")));
            this.chkReadyUnload.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("chkReadyUnload.Properties.PictureUnchecked")));
            this.chkReadyUnload.Size = new System.Drawing.Size(16, 20);
            this.chkReadyUnload.TabIndex = 86;
            // 
            // chkReadyLaser
            // 
            this.chkReadyLaser.Location = new System.Drawing.Point(1088, 169);
            this.chkReadyLaser.Name = "chkReadyLaser";
            this.chkReadyLaser.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReadyLaser.Properties.Appearance.Options.UseFont = true;
            this.chkReadyLaser.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyLaser.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkReadyLaser.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyLaser.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkReadyLaser.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyLaser.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkReadyLaser.Properties.Caption = "";
            this.chkReadyLaser.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkReadyLaser.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("chkReadyLaser.Properties.PictureChecked")));
            this.chkReadyLaser.Properties.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("chkReadyLaser.Properties.PictureGrayed")));
            this.chkReadyLaser.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("chkReadyLaser.Properties.PictureUnchecked")));
            this.chkReadyLaser.Size = new System.Drawing.Size(16, 20);
            this.chkReadyLaser.TabIndex = 85;
            // 
            // chkReadyFlash2
            // 
            this.chkReadyFlash2.Location = new System.Drawing.Point(738, 270);
            this.chkReadyFlash2.Name = "chkReadyFlash2";
            this.chkReadyFlash2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReadyFlash2.Properties.Appearance.Options.UseFont = true;
            this.chkReadyFlash2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyFlash2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkReadyFlash2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyFlash2.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkReadyFlash2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyFlash2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkReadyFlash2.Properties.Caption = "";
            this.chkReadyFlash2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkReadyFlash2.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("chkReadyFlash2.Properties.PictureChecked")));
            this.chkReadyFlash2.Properties.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("chkReadyFlash2.Properties.PictureGrayed")));
            this.chkReadyFlash2.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("chkReadyFlash2.Properties.PictureUnchecked")));
            this.chkReadyFlash2.Size = new System.Drawing.Size(16, 20);
            this.chkReadyFlash2.TabIndex = 84;
            // 
            // chkReadyFlash1
            // 
            this.chkReadyFlash1.Location = new System.Drawing.Point(504, 271);
            this.chkReadyFlash1.Name = "chkReadyFlash1";
            this.chkReadyFlash1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReadyFlash1.Properties.Appearance.Options.UseFont = true;
            this.chkReadyFlash1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyFlash1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkReadyFlash1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyFlash1.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkReadyFlash1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyFlash1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkReadyFlash1.Properties.Caption = "";
            this.chkReadyFlash1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkReadyFlash1.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("chkReadyFlash1.Properties.PictureChecked")));
            this.chkReadyFlash1.Properties.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("chkReadyFlash1.Properties.PictureGrayed")));
            this.chkReadyFlash1.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("chkReadyFlash1.Properties.PictureUnchecked")));
            this.chkReadyFlash1.Size = new System.Drawing.Size(16, 20);
            this.chkReadyFlash1.TabIndex = 83;
            // 
            // chkReadyLoad
            // 
            this.chkReadyLoad.Location = new System.Drawing.Point(371, 169);
            this.chkReadyLoad.Name = "chkReadyLoad";
            this.chkReadyLoad.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkReadyLoad.Properties.Appearance.Options.UseFont = true;
            this.chkReadyLoad.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyLoad.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkReadyLoad.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyLoad.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkReadyLoad.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkReadyLoad.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkReadyLoad.Properties.Caption = "";
            this.chkReadyLoad.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkReadyLoad.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("chkReadyLoad.Properties.PictureChecked")));
            this.chkReadyLoad.Properties.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("chkReadyLoad.Properties.PictureGrayed")));
            this.chkReadyLoad.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("chkReadyLoad.Properties.PictureUnchecked")));
            this.chkReadyLoad.Size = new System.Drawing.Size(16, 20);
            this.chkReadyLoad.TabIndex = 19;
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.chkWriteWorkSheetToSQL);
            this.popupContainerControl1.Controls.Add(this.txtTestPC);
            this.popupContainerControl1.Controls.Add(this.btmPrintLable);
            this.popupContainerControl1.Controls.Add(this.获取工单信息);
            this.popupContainerControl1.Controls.Add(this.txtWorkID);
            this.popupContainerControl1.Controls.Add(this.propertyProcess);
            this.popupContainerControl1.Location = new System.Drawing.Point(0, 43);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(365, 419);
            this.popupContainerControl1.TabIndex = 3;
            // 
            // chkWriteWorkSheetToSQL
            // 
            this.chkWriteWorkSheetToSQL.AutoSize = true;
            this.chkWriteWorkSheetToSQL.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkWriteWorkSheetToSQL.Location = new System.Drawing.Point(222, 384);
            this.chkWriteWorkSheetToSQL.Name = "chkWriteWorkSheetToSQL";
            this.chkWriteWorkSheetToSQL.Size = new System.Drawing.Size(135, 21);
            this.chkWriteWorkSheetToSQL.TabIndex = 186;
            this.chkWriteWorkSheetToSQL.Text = "获取时写入工单信息";
            this.chkWriteWorkSheetToSQL.UseVisualStyleBackColor = true;
            // 
            // txtTestPC
            // 
            this.txtTestPC.EditValue = "20";
            this.txtTestPC.Location = new System.Drawing.Point(222, 340);
            this.txtTestPC.Name = "txtTestPC";
            this.txtTestPC.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTestPC.Properties.Appearance.Options.UseFont = true;
            this.txtTestPC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTestPC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTestPC.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTestPC.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTestPC.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTestPC.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTestPC.Size = new System.Drawing.Size(76, 24);
            this.txtTestPC.TabIndex = 185;
            // 
            // btmPrintLable
            // 
            this.btmPrintLable.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmPrintLable.Appearance.Options.UseFont = true;
            this.btmPrintLable.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmPrintLable.AppearanceDisabled.Options.UseFont = true;
            this.btmPrintLable.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmPrintLable.AppearanceHovered.Options.UseFont = true;
            this.btmPrintLable.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmPrintLable.AppearancePressed.Options.UseFont = true;
            this.btmPrintLable.Location = new System.Drawing.Point(137, 370);
            this.btmPrintLable.Name = "btmPrintLable";
            this.btmPrintLable.Size = new System.Drawing.Size(79, 46);
            this.btmPrintLable.TabIndex = 184;
            this.btmPrintLable.Text = "打印条码";
            this.btmPrintLable.Click += new System.EventHandler(this.btmPrintLable_Click);
            // 
            // 获取工单信息
            // 
            this.获取工单信息.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.获取工单信息.Appearance.Options.UseFont = true;
            this.获取工单信息.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.获取工单信息.AppearanceDisabled.Options.UseFont = true;
            this.获取工单信息.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.获取工单信息.AppearanceHovered.Options.UseFont = true;
            this.获取工单信息.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.获取工单信息.AppearancePressed.Options.UseFont = true;
            this.获取工单信息.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.获取工单信息.Location = new System.Drawing.Point(4, 370);
            this.获取工单信息.Name = "获取工单信息";
            this.获取工单信息.Size = new System.Drawing.Size(127, 46);
            this.获取工单信息.TabIndex = 1;
            this.获取工单信息.Text = "手动获取工单及转工单";
            this.获取工单信息.Click += new System.EventHandler(this.获取工单信息_Click);
            // 
            // txtWorkID
            // 
            this.txtWorkID.EditValue = "83863614";
            this.txtWorkID.Location = new System.Drawing.Point(4, 340);
            this.txtWorkID.Name = "txtWorkID";
            this.txtWorkID.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWorkID.Properties.Appearance.Options.UseFont = true;
            this.txtWorkID.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkID.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtWorkID.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkID.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtWorkID.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkID.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtWorkID.Size = new System.Drawing.Size(212, 24);
            this.txtWorkID.TabIndex = 0;
            // 
            // propertyProcess
            // 
            this.propertyProcess.Location = new System.Drawing.Point(0, 4);
            this.propertyProcess.Name = "propertyProcess";
            this.propertyProcess.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.propertyProcess.Size = new System.Drawing.Size(362, 322);
            this.propertyProcess.TabIndex = 183;
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "当前工单:";
            this.popupContainerEdit1.Location = new System.Drawing.Point(-1, 3);
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.popupContainerEdit1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.popupContainerEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.popupContainerEdit1.Properties.Appearance.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.popupContainerEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.popupContainerEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.popupContainerEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.popupContainerEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControl1;
            this.popupContainerEdit1.Size = new System.Drawing.Size(366, 34);
            this.popupContainerEdit1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1712, 830);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // NP_设置
            // 
            this.NP_设置.Caption = " 系统设置";
            this.NP_设置.Controls.Add(this.groupControl3);
            this.NP_设置.Controls.Add(this.groupControl2);
            this.NP_设置.Controls.Add(this.groupControl1);
            this.NP_设置.Image = ((System.Drawing.Image)(resources.GetObject("NP_设置.Image")));
            this.NP_设置.Name = "NP_设置";
            this.NP_设置.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.NP_设置.Size = new System.Drawing.Size(1706, 646);
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Location = new System.Drawing.Point(357, 432);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(318, 153);
            this.groupControl3.TabIndex = 96;
            this.groupControl3.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.chkFocePackingSNOK);
            this.groupControl2.Controls.Add(this.chkEnableLastMode);
            this.groupControl2.Controls.Add(this.强制MES验证通过);
            this.groupControl2.Controls.Add(this.txtSNLength);
            this.groupControl2.Controls.Add(this.不扫描条码);
            this.groupControl2.Controls.Add(this.txtSfcSNPrefix);
            this.groupControl2.Controls.Add(this.chkSfcLength);
            this.groupControl2.Controls.Add(this.chkSfcPrefix);
            this.groupControl2.Location = new System.Drawing.Point(357, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(318, 422);
            this.groupControl2.TabIndex = 95;
            this.groupControl2.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // chkFocePackingSNOK
            // 
            this.chkFocePackingSNOK.AutoSize = true;
            this.chkFocePackingSNOK.Checked = true;
            this.chkFocePackingSNOK.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFocePackingSNOK.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chkFocePackingSNOK.Location = new System.Drawing.Point(9, 165);
            this.chkFocePackingSNOK.Name = "chkFocePackingSNOK";
            this.chkFocePackingSNOK.Size = new System.Drawing.Size(260, 35);
            this.chkFocePackingSNOK.TabIndex = 97;
            this.chkFocePackingSNOK.Text = "包装工位强制扫码OK";
            this.chkFocePackingSNOK.UseVisualStyleBackColor = true;
            this.chkFocePackingSNOK.CheckedChanged += new System.EventHandler(this.chkFocePackingSNOK_CheckedChanged);
            // 
            // chkEnableLastMode
            // 
            this.chkEnableLastMode.AutoSize = true;
            this.chkEnableLastMode.Checked = true;
            this.chkEnableLastMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnableLastMode.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chkEnableLastMode.Location = new System.Drawing.Point(9, 206);
            this.chkEnableLastMode.Name = "chkEnableLastMode";
            this.chkEnableLastMode.Size = new System.Drawing.Size(177, 35);
            this.chkEnableLastMode.TabIndex = 96;
            this.chkEnableLastMode.Text = "启用尾料模式";
            this.chkEnableLastMode.UseVisualStyleBackColor = true;
            this.chkEnableLastMode.CheckedChanged += new System.EventHandler(this.chkEnableLastMode_CheckedChanged);
            // 
            // 强制MES验证通过
            // 
            this.强制MES验证通过.AutoSize = true;
            this.强制MES验证通过.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.强制MES验证通过.Location = new System.Drawing.Point(9, 126);
            this.强制MES验证通过.Name = "强制MES验证通过";
            this.强制MES验证通过.Size = new System.Drawing.Size(125, 21);
            this.强制MES验证通过.TabIndex = 95;
            this.强制MES验证通过.Text = "强制MES验证通过";
            this.强制MES验证通过.UseVisualStyleBackColor = true;
            this.强制MES验证通过.CheckedChanged += new System.EventHandler(this.强制MES验证通过_CheckedChanged);
            // 
            // txtSNLength
            // 
            this.txtSNLength.Location = new System.Drawing.Point(140, 63);
            this.txtSNLength.Name = "txtSNLength";
            this.txtSNLength.Size = new System.Drawing.Size(81, 23);
            this.txtSNLength.TabIndex = 85;
            // 
            // 不扫描条码
            // 
            this.不扫描条码.AutoSize = true;
            this.不扫描条码.Checked = true;
            this.不扫描条码.CheckState = System.Windows.Forms.CheckState.Checked;
            this.不扫描条码.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.不扫描条码.Location = new System.Drawing.Point(9, 99);
            this.不扫描条码.Name = "不扫描条码";
            this.不扫描条码.Size = new System.Drawing.Size(87, 21);
            this.不扫描条码.TabIndex = 94;
            this.不扫描条码.Text = "不扫描条码";
            this.不扫描条码.UseVisualStyleBackColor = true;
            // 
            // txtSfcSNPrefix
            // 
            this.txtSfcSNPrefix.Location = new System.Drawing.Point(140, 34);
            this.txtSfcSNPrefix.MaxLength = 3;
            this.txtSfcSNPrefix.Name = "txtSfcSNPrefix";
            this.txtSfcSNPrefix.Size = new System.Drawing.Size(81, 23);
            this.txtSfcSNPrefix.TabIndex = 84;
            // 
            // chkSfcLength
            // 
            this.chkSfcLength.AutoSize = true;
            this.chkSfcLength.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSfcLength.Location = new System.Drawing.Point(9, 65);
            this.chkSfcLength.Name = "chkSfcLength";
            this.chkSfcLength.Size = new System.Drawing.Size(116, 21);
            this.chkSfcLength.TabIndex = 83;
            this.chkSfcLength.Text = "Sfc条码长度校验";
            this.chkSfcLength.UseVisualStyleBackColor = true;
            // 
            // chkSfcPrefix
            // 
            this.chkSfcPrefix.AutoSize = true;
            this.chkSfcPrefix.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSfcPrefix.Location = new System.Drawing.Point(9, 39);
            this.chkSfcPrefix.Name = "chkSfcPrefix";
            this.chkSfcPrefix.Size = new System.Drawing.Size(128, 21);
            this.chkSfcPrefix.TabIndex = 82;
            this.chkSfcPrefix.Text = "Sfc条码前三位校验";
            this.chkSfcPrefix.UseVisualStyleBackColor = true;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.memoEdit3);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.txtIPAddr);
            this.groupControl1.Controls.Add(this.txtPort);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(348, 618);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // memoEdit3
            // 
            this.memoEdit3.EditValue = resources.GetString("memoEdit3.EditValue");
            this.memoEdit3.Location = new System.Drawing.Point(6, 52);
            this.memoEdit3.Name = "memoEdit3";
            this.memoEdit3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEdit3.Properties.Appearance.Options.UseFont = true;
            this.memoEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.memoEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.memoEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoEdit3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.memoEdit3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoEdit3.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.memoEdit3.Properties.WordWrap = false;
            this.memoEdit3.Size = new System.Drawing.Size(337, 561);
            this.memoEdit3.TabIndex = 82;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 78;
            this.label1.Text = "PLC_上料地址";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(222, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 79;
            this.label2.Text = "端口";
            // 
            // txtIPAddr
            // 
            this.txtIPAddr.Location = new System.Drawing.Point(117, 25);
            this.txtIPAddr.Name = "txtIPAddr";
            this.txtIPAddr.Size = new System.Drawing.Size(97, 23);
            this.txtIPAddr.TabIndex = 80;
            this.txtIPAddr.Text = "192.168.1.2";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(258, 25);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(45, 23);
            this.txtPort.TabIndex = 81;
            this.txtPort.Text = "4050";
            // 
            // NP_监控
            // 
            this.NP_监控.Caption = "   监控";
            this.NP_监控.Controls.Add(this.cmbPackingPCBs);
            this.NP_监控.Controls.Add(this.groupControl18);
            this.NP_监控.Controls.Add(this.btmGetCurrentWorkSheet);
            this.NP_监控.Controls.Add(this.propertyGrid2);
            this.NP_监控.Controls.Add(this.labelControl61);
            this.NP_监控.Controls.Add(this.button1);
            this.NP_监控.Controls.Add(this.cmbUnloadPCBs);
            this.NP_监控.Controls.Add(this.groupControl10);
            this.NP_监控.Image = ((System.Drawing.Image)(resources.GetObject("NP_监控.Image")));
            this.NP_监控.Name = "NP_监控";
            this.NP_监控.Size = new System.Drawing.Size(1702, 647);
            // 
            // cmbPackingPCBs
            // 
            this.cmbPackingPCBs.FormattingEnabled = true;
            this.cmbPackingPCBs.HorizontalScrollbar = true;
            this.cmbPackingPCBs.ItemHeight = 17;
            this.cmbPackingPCBs.Location = new System.Drawing.Point(966, 304);
            this.cmbPackingPCBs.Name = "cmbPackingPCBs";
            this.cmbPackingPCBs.ScrollAlwaysVisible = true;
            this.cmbPackingPCBs.Size = new System.Drawing.Size(272, 344);
            this.cmbPackingPCBs.TabIndex = 194;
            // 
            // groupControl18
            // 
            this.groupControl18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl18.Appearance.Options.UseFont = true;
            this.groupControl18.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl18.AppearanceCaption.Options.UseFont = true;
            this.groupControl18.Controls.Add(this.txt已识别的好品数量);
            this.groupControl18.Controls.Add(this.labelControl67);
            this.groupControl18.Controls.Add(this.spinEdit6);
            this.groupControl18.Controls.Add(this.simpleButton3);
            this.groupControl18.Controls.Add(this.label15);
            this.groupControl18.Controls.Add(this.当前标签包装数量1068);
            this.groupControl18.Controls.Add(this.brm刷新最后打码标志);
            this.groupControl18.Controls.Add(this.spinEdit5);
            this.groupControl18.Controls.Add(this.gridFlagMark);
            this.groupControl18.Controls.Add(this.btmSetLastTrayRemaindCount);
            this.groupControl18.Controls.Add(this.label16);
            this.groupControl18.Controls.Add(this.sped最后托盘剩余个数);
            this.groupControl18.Controls.Add(this.label14);
            this.groupControl18.Controls.Add(this.spedLaserRemaind);
            this.groupControl18.Controls.Add(this.label13);
            this.groupControl18.Controls.Add(this.spinEdit3);
            this.groupControl18.Controls.Add(this.复位所有烧录结果数据);
            this.groupControl18.Controls.Add(this.label12);
            this.groupControl18.Controls.Add(this.sped生产应上盘数);
            this.groupControl18.Controls.Add(this.spedPackingPackedCount2);
            this.groupControl18.Controls.Add(this.spedUnloadPackedCount2);
            this.groupControl18.Controls.Add(this.btmClearFailed);
            this.groupControl18.Controls.Add(this.label11);
            this.groupControl18.Controls.Add(this.sped生产实际总盘数);
            this.groupControl18.Controls.Add(this.label10);
            this.groupControl18.Controls.Add(this.sped当前坏品总数量);
            this.groupControl18.Controls.Add(this.simpleButton1);
            this.groupControl18.Controls.Add(this.spedUnloadPackedCountSet);
            this.groupControl18.Controls.Add(this.btmSetPackingCount);
            this.groupControl18.Controls.Add(this.spedPackingPackedCountSet);
            this.groupControl18.Controls.Add(this.label9);
            this.groupControl18.Controls.Add(this.spedPackingPackedCount);
            this.groupControl18.Controls.Add(this.label8);
            this.groupControl18.Controls.Add(this.spedUnloadPackedCount);
            this.groupControl18.Controls.Add(this.label62);
            this.groupControl18.Controls.Add(this.TotalLineOutAll);
            this.groupControl18.Controls.Add(this.speCountOfBuffTray);
            this.groupControl18.Controls.Add(this.spinEdit2);
            this.groupControl18.Controls.Add(this.label65);
            this.groupControl18.Controls.Add(this.Sped生产总量);
            this.groupControl18.Controls.Add(this.speCountOfLoadTray);
            this.groupControl18.Controls.Add(this.label54);
            this.groupControl18.Controls.Add(this.label64);
            this.groupControl18.Controls.Add(this.label57);
            this.groupControl18.Controls.Add(this.label63);
            this.groupControl18.Controls.Add(this.sped产品行数);
            this.groupControl18.Controls.Add(this.btmSetCurrentTrayOut);
            this.groupControl18.Controls.Add(this.label61);
            this.groupControl18.Controls.Add(this.label60);
            this.groupControl18.Controls.Add(this.label53);
            this.groupControl18.Controls.Add(this.TotalTrayOfLineOutPCS);
            this.groupControl18.Controls.Add(this.UnloadCfg_CountTrayBK);
            this.groupControl18.Controls.Add(this.spedSetTrayOuted);
            this.groupControl18.Controls.Add(this.labelControl63);
            this.groupControl18.Controls.Add(this.TotalTrayOfLineOut);
            this.groupControl18.Controls.Add(this.sped生产总盘数);
            this.groupControl18.Controls.Add(this.label59);
            this.groupControl18.Controls.Add(this.Sped包装总量);
            this.groupControl18.Controls.Add(this.CurrentCountTray);
            this.groupControl18.Controls.Add(this.生产最后打包总盘数);
            this.groupControl18.Controls.Add(this.label56);
            this.groupControl18.Controls.Add(this.sped产品例数);
            this.groupControl18.Controls.Add(this.spinEdit4);
            this.groupControl18.Location = new System.Drawing.Point(275, 3);
            this.groupControl18.Name = "groupControl18";
            this.groupControl18.Size = new System.Drawing.Size(685, 640);
            this.groupControl18.TabIndex = 193;
            this.groupControl18.Text = "总调度工艺参数";
            // 
            // txt已识别的好品数量
            // 
            this.txt已识别的好品数量.Location = new System.Drawing.Point(414, 167);
            this.txt已识别的好品数量.Name = "txt已识别的好品数量";
            this.txt已识别的好品数量.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt已识别的好品数量.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.Appearance.Options.UseBackColor = true;
            this.txt已识别的好品数量.Properties.Appearance.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.AppearanceFocused.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.ReadOnly = true;
            this.txt已识别的好品数量.Size = new System.Drawing.Size(104, 24);
            this.txt已识别的好品数量.TabIndex = 244;
            // 
            // labelControl67
            // 
            this.labelControl67.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl67.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl67.Appearance.Options.UseBackColor = true;
            this.labelControl67.Appearance.Options.UseFont = true;
            this.labelControl67.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl67.AppearanceDisabled.Options.UseFont = true;
            this.labelControl67.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl67.AppearanceHovered.Options.UseFont = true;
            this.labelControl67.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl67.AppearancePressed.Options.UseFont = true;
            this.labelControl67.Location = new System.Drawing.Point(312, 171);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(96, 17);
            this.labelControl67.TabIndex = 243;
            this.labelControl67.Text = "已识别的好品数量";
            // 
            // spinEdit6
            // 
            this.spinEdit6.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit6.Location = new System.Drawing.Point(524, 105);
            this.spinEdit6.Name = "spinEdit6";
            this.spinEdit6.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit6.Properties.Appearance.Options.UseFont = true;
            this.spinEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinEdit6.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit6.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinEdit6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit6.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit6.Size = new System.Drawing.Size(58, 24);
            this.spinEdit6.TabIndex = 242;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton3.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton3.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton3.AppearanceHovered.Options.UseFont = true;
            this.simpleButton3.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton3.AppearancePressed.Options.UseFont = true;
            this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(588, 101);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(92, 32);
            this.simpleButton3.TabIndex = 241;
            this.simpleButton3.Text = "设定";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(304, 107);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 17);
            this.label15.TabIndex = 240;
            this.label15.Text = "当前标签包装数量";
            // 
            // 当前标签包装数量1068
            // 
            this.当前标签包装数量1068.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.当前标签包装数量1068.Location = new System.Drawing.Point(414, 104);
            this.当前标签包装数量1068.Name = "当前标签包装数量1068";
            this.当前标签包装数量1068.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.当前标签包装数量1068.Properties.Appearance.Options.UseFont = true;
            this.当前标签包装数量1068.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.当前标签包装数量1068.Properties.AppearanceDisabled.Options.UseFont = true;
            this.当前标签包装数量1068.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.当前标签包装数量1068.Properties.AppearanceFocused.Options.UseFont = true;
            this.当前标签包装数量1068.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.当前标签包装数量1068.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.当前标签包装数量1068.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.当前标签包装数量1068.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.当前标签包装数量1068.Properties.ReadOnly = true;
            this.当前标签包装数量1068.Size = new System.Drawing.Size(105, 24);
            this.当前标签包装数量1068.TabIndex = 239;
            // 
            // brm刷新最后打码标志
            // 
            this.brm刷新最后打码标志.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brm刷新最后打码标志.Appearance.Options.UseFont = true;
            this.brm刷新最后打码标志.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.brm刷新最后打码标志.AppearanceDisabled.Options.UseFont = true;
            this.brm刷新最后打码标志.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.brm刷新最后打码标志.AppearanceHovered.Options.UseFont = true;
            this.brm刷新最后打码标志.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.brm刷新最后打码标志.AppearancePressed.Options.UseFont = true;
            this.brm刷新最后打码标志.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.brm刷新最后打码标志.Location = new System.Drawing.Point(408, 595);
            this.brm刷新最后打码标志.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.brm刷新最后打码标志.Name = "brm刷新最后打码标志";
            this.brm刷新最后打码标志.Size = new System.Drawing.Size(272, 32);
            this.brm刷新最后打码标志.TabIndex = 238;
            this.brm刷新最后打码标志.Text = "刷新 (最后托盘打码/切割/抓取标志)";
            this.brm刷新最后打码标志.Click += new System.EventHandler(this.brm刷新最后打码标志_Click);
            // 
            // spinEdit5
            // 
            this.spinEdit5.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit5.Location = new System.Drawing.Point(524, 275);
            this.spinEdit5.Name = "spinEdit5";
            this.spinEdit5.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit5.Properties.Appearance.Options.UseFont = true;
            this.spinEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinEdit5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit5.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit5.Size = new System.Drawing.Size(58, 24);
            this.spinEdit5.TabIndex = 238;
            // 
            // gridFlagMark
            // 
            this.gridFlagMark.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFlagMark.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridFlagMark.ColumnHeadersHeight = 24;
            this.gridFlagMark.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridFlagMark.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.gridFlagMark.Location = new System.Drawing.Point(408, 332);
            this.gridFlagMark.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridFlagMark.Name = "gridFlagMark";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFlagMark.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridFlagMark.RowTemplate.Height = 23;
            this.gridFlagMark.Size = new System.Drawing.Size(272, 295);
            this.gridFlagMark.TabIndex = 194;
            // 
            // Column1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "1";
            this.Column1.Name = "Column1";
            this.Column1.Width = 20;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "2";
            this.Column2.Name = "Column2";
            this.Column2.Width = 20;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "3";
            this.Column3.Name = "Column3";
            this.Column3.Width = 20;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "4";
            this.Column4.Name = "Column4";
            this.Column4.Width = 20;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "5";
            this.Column5.Name = "Column5";
            this.Column5.Width = 20;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "6";
            this.Column6.Name = "Column6";
            this.Column6.Width = 20;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "7";
            this.Column7.Name = "Column7";
            this.Column7.Width = 20;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "8";
            this.Column8.Name = "Column8";
            this.Column8.Width = 20;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "9";
            this.Column9.Name = "Column9";
            this.Column9.Width = 20;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "10";
            this.Column10.Name = "Column10";
            this.Column10.Width = 20;
            // 
            // btmSetLastTrayRemaindCount
            // 
            this.btmSetLastTrayRemaindCount.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetLastTrayRemaindCount.Appearance.Options.UseFont = true;
            this.btmSetLastTrayRemaindCount.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetLastTrayRemaindCount.AppearanceDisabled.Options.UseFont = true;
            this.btmSetLastTrayRemaindCount.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetLastTrayRemaindCount.AppearanceHovered.Options.UseFont = true;
            this.btmSetLastTrayRemaindCount.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetLastTrayRemaindCount.AppearancePressed.Options.UseFont = true;
            this.btmSetLastTrayRemaindCount.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetLastTrayRemaindCount.ImageOptions.Image")));
            this.btmSetLastTrayRemaindCount.Location = new System.Drawing.Point(588, 272);
            this.btmSetLastTrayRemaindCount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetLastTrayRemaindCount.Name = "btmSetLastTrayRemaindCount";
            this.btmSetLastTrayRemaindCount.Size = new System.Drawing.Size(92, 32);
            this.btmSetLastTrayRemaindCount.TabIndex = 237;
            this.btmSetLastTrayRemaindCount.Text = "设定";
            this.btmSetLastTrayRemaindCount.Click += new System.EventHandler(this.btmSetLastTrayRemaindCount_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(304, 276);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 17);
            this.label16.TabIndex = 236;
            this.label16.Text = "最后托盘剩余个数";
            // 
            // sped最后托盘剩余个数
            // 
            this.sped最后托盘剩余个数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped最后托盘剩余个数.Location = new System.Drawing.Point(414, 274);
            this.sped最后托盘剩余个数.Name = "sped最后托盘剩余个数";
            this.sped最后托盘剩余个数.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.sped最后托盘剩余个数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped最后托盘剩余个数.Properties.Appearance.Options.UseBackColor = true;
            this.sped最后托盘剩余个数.Properties.Appearance.Options.UseFont = true;
            this.sped最后托盘剩余个数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped最后托盘剩余个数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped最后托盘剩余个数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped最后托盘剩余个数.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped最后托盘剩余个数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped最后托盘剩余个数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped最后托盘剩余个数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped最后托盘剩余个数.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped最后托盘剩余个数.Properties.ReadOnly = true;
            this.sped最后托盘剩余个数.Size = new System.Drawing.Size(105, 24);
            this.sped最后托盘剩余个数.TabIndex = 235;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(299, 249);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 17);
            this.label14.TabIndex = 234;
            this.label14.Text = "打码/切割最后数量";
            // 
            // spedLaserRemaind
            // 
            this.spedLaserRemaind.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedLaserRemaind.Location = new System.Drawing.Point(414, 246);
            this.spedLaserRemaind.Name = "spedLaserRemaind";
            this.spedLaserRemaind.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.spedLaserRemaind.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedLaserRemaind.Properties.Appearance.Options.UseBackColor = true;
            this.spedLaserRemaind.Properties.Appearance.Options.UseFont = true;
            this.spedLaserRemaind.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedLaserRemaind.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedLaserRemaind.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedLaserRemaind.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedLaserRemaind.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedLaserRemaind.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedLaserRemaind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedLaserRemaind.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedLaserRemaind.Properties.ReadOnly = true;
            this.spedLaserRemaind.Size = new System.Drawing.Size(105, 24);
            this.spedLaserRemaind.TabIndex = 233;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(304, 219);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 17);
            this.label13.TabIndex = 232;
            this.label13.Text = "当前已出好品个数";
            // 
            // spinEdit3
            // 
            this.spinEdit3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit3.Location = new System.Drawing.Point(524, 138);
            this.spinEdit3.Name = "spinEdit3";
            this.spinEdit3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit3.Properties.Appearance.Options.UseFont = true;
            this.spinEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinEdit3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit3.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit3.Size = new System.Drawing.Size(58, 24);
            this.spinEdit3.TabIndex = 231;
            // 
            // 复位所有烧录结果数据
            // 
            this.复位所有烧录结果数据.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.复位所有烧录结果数据.Appearance.Options.UseFont = true;
            this.复位所有烧录结果数据.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.复位所有烧录结果数据.AppearanceDisabled.Options.UseFont = true;
            this.复位所有烧录结果数据.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.复位所有烧录结果数据.AppearanceHovered.Options.UseFont = true;
            this.复位所有烧录结果数据.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.复位所有烧录结果数据.AppearancePressed.Options.UseFont = true;
            this.复位所有烧录结果数据.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("复位所有烧录结果数据.ImageOptions.Image")));
            this.复位所有烧录结果数据.Location = new System.Drawing.Point(10, 544);
            this.复位所有烧录结果数据.Name = "复位所有烧录结果数据";
            this.复位所有烧录结果数据.Size = new System.Drawing.Size(355, 91);
            this.复位所有烧录结果数据.TabIndex = 182;
            this.复位所有烧录结果数据.Text = "复位数据(会再次重新工单数据)";
            this.复位所有烧录结果数据.Click += new System.EventHandler(this.复位所有烧录结果数据_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(74, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 17);
            this.label12.TabIndex = 230;
            this.label12.Text = "生产应上盘数";
            // 
            // sped生产应上盘数
            // 
            this.sped生产应上盘数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped生产应上盘数.Location = new System.Drawing.Point(160, 94);
            this.sped生产应上盘数.Name = "sped生产应上盘数";
            this.sped生产应上盘数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped生产应上盘数.Properties.Appearance.Options.UseFont = true;
            this.sped生产应上盘数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产应上盘数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped生产应上盘数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产应上盘数.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped生产应上盘数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产应上盘数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped生产应上盘数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped生产应上盘数.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped生产应上盘数.Properties.ReadOnly = true;
            this.sped生产应上盘数.Size = new System.Drawing.Size(92, 24);
            this.sped生产应上盘数.TabIndex = 229;
            // 
            // spedPackingPackedCount2
            // 
            this.spedPackingPackedCount2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedPackingPackedCount2.Location = new System.Drawing.Point(467, 70);
            this.spedPackingPackedCount2.Name = "spedPackingPackedCount2";
            this.spedPackingPackedCount2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedPackingPackedCount2.Properties.Appearance.Options.UseFont = true;
            this.spedPackingPackedCount2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCount2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedPackingPackedCount2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCount2.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedPackingPackedCount2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCount2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedPackingPackedCount2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedPackingPackedCount2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedPackingPackedCount2.Properties.ReadOnly = true;
            this.spedPackingPackedCount2.Size = new System.Drawing.Size(52, 24);
            this.spedPackingPackedCount2.TabIndex = 228;
            // 
            // spedUnloadPackedCount2
            // 
            this.spedUnloadPackedCount2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedUnloadPackedCount2.Location = new System.Drawing.Point(467, 32);
            this.spedUnloadPackedCount2.Name = "spedUnloadPackedCount2";
            this.spedUnloadPackedCount2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedUnloadPackedCount2.Properties.Appearance.Options.UseFont = true;
            this.spedUnloadPackedCount2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCount2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedUnloadPackedCount2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCount2.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedUnloadPackedCount2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCount2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedUnloadPackedCount2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedUnloadPackedCount2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedUnloadPackedCount2.Properties.ReadOnly = true;
            this.spedUnloadPackedCount2.Size = new System.Drawing.Size(52, 24);
            this.spedUnloadPackedCount2.TabIndex = 227;
            // 
            // btmClearFailed
            // 
            this.btmClearFailed.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmClearFailed.Appearance.Options.UseFont = true;
            this.btmClearFailed.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmClearFailed.AppearanceDisabled.Options.UseFont = true;
            this.btmClearFailed.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmClearFailed.AppearanceHovered.Options.UseFont = true;
            this.btmClearFailed.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmClearFailed.AppearancePressed.Options.UseFont = true;
            this.btmClearFailed.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmClearFailed.ImageOptions.Image")));
            this.btmClearFailed.Location = new System.Drawing.Point(588, 134);
            this.btmClearFailed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmClearFailed.Name = "btmClearFailed";
            this.btmClearFailed.Size = new System.Drawing.Size(92, 32);
            this.btmClearFailed.TabIndex = 226;
            this.btmClearFailed.Text = "设定";
            this.btmClearFailed.Click += new System.EventHandler(this.btmClearFailed_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(62, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 17);
            this.label11.TabIndex = 225;
            this.label11.Text = "生产实际总盘数";
            // 
            // sped生产实际总盘数
            // 
            this.sped生产实际总盘数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped生产实际总盘数.Location = new System.Drawing.Point(160, 124);
            this.sped生产实际总盘数.Name = "sped生产实际总盘数";
            this.sped生产实际总盘数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped生产实际总盘数.Properties.Appearance.Options.UseFont = true;
            this.sped生产实际总盘数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产实际总盘数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped生产实际总盘数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产实际总盘数.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped生产实际总盘数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产实际总盘数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped生产实际总盘数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped生产实际总盘数.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped生产实际总盘数.Properties.ReadOnly = true;
            this.sped生产实际总盘数.Size = new System.Drawing.Size(92, 24);
            this.sped生产实际总盘数.TabIndex = 224;
            this.sped生产实际总盘数.EditValueChanged += new System.EventHandler(this.sped生产实际总盘数_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(316, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 17);
            this.label10.TabIndex = 223;
            this.label10.Text = "当前坏品总数量";
            // 
            // sped当前坏品总数量
            // 
            this.sped当前坏品总数量.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped当前坏品总数量.Location = new System.Drawing.Point(414, 137);
            this.sped当前坏品总数量.Name = "sped当前坏品总数量";
            this.sped当前坏品总数量.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped当前坏品总数量.Properties.Appearance.Options.UseFont = true;
            this.sped当前坏品总数量.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped当前坏品总数量.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped当前坏品总数量.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped当前坏品总数量.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped当前坏品总数量.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped当前坏品总数量.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped当前坏品总数量.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped当前坏品总数量.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped当前坏品总数量.Properties.ReadOnly = true;
            this.sped当前坏品总数量.Size = new System.Drawing.Size(105, 24);
            this.sped当前坏品总数量.TabIndex = 222;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton1.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton1.AppearanceHovered.Options.UseFont = true;
            this.simpleButton1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton1.AppearancePressed.Options.UseFont = true;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(589, 27);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(92, 33);
            this.simpleButton1.TabIndex = 221;
            this.simpleButton1.Text = "设定";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // spedUnloadPackedCountSet
            // 
            this.spedUnloadPackedCountSet.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedUnloadPackedCountSet.Location = new System.Drawing.Point(525, 32);
            this.spedUnloadPackedCountSet.Name = "spedUnloadPackedCountSet";
            this.spedUnloadPackedCountSet.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedUnloadPackedCountSet.Properties.Appearance.Options.UseFont = true;
            this.spedUnloadPackedCountSet.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCountSet.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedUnloadPackedCountSet.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCountSet.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedUnloadPackedCountSet.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCountSet.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedUnloadPackedCountSet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedUnloadPackedCountSet.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedUnloadPackedCountSet.Size = new System.Drawing.Size(57, 24);
            this.spedUnloadPackedCountSet.TabIndex = 220;
            // 
            // btmSetPackingCount
            // 
            this.btmSetPackingCount.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetPackingCount.Appearance.Options.UseFont = true;
            this.btmSetPackingCount.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetPackingCount.AppearanceDisabled.Options.UseFont = true;
            this.btmSetPackingCount.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetPackingCount.AppearanceHovered.Options.UseFont = true;
            this.btmSetPackingCount.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetPackingCount.AppearancePressed.Options.UseFont = true;
            this.btmSetPackingCount.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetPackingCount.ImageOptions.Image")));
            this.btmSetPackingCount.Location = new System.Drawing.Point(589, 68);
            this.btmSetPackingCount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetPackingCount.Name = "btmSetPackingCount";
            this.btmSetPackingCount.Size = new System.Drawing.Size(92, 32);
            this.btmSetPackingCount.TabIndex = 219;
            this.btmSetPackingCount.Text = "设定";
            this.btmSetPackingCount.Click += new System.EventHandler(this.btmSetPackingCount_Click);
            // 
            // spedPackingPackedCountSet
            // 
            this.spedPackingPackedCountSet.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedPackingPackedCountSet.Location = new System.Drawing.Point(525, 70);
            this.spedPackingPackedCountSet.Name = "spedPackingPackedCountSet";
            this.spedPackingPackedCountSet.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedPackingPackedCountSet.Properties.Appearance.Options.UseFont = true;
            this.spedPackingPackedCountSet.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCountSet.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedPackingPackedCountSet.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCountSet.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedPackingPackedCountSet.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCountSet.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedPackingPackedCountSet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedPackingPackedCountSet.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedPackingPackedCountSet.Size = new System.Drawing.Size(58, 24);
            this.spedPackingPackedCountSet.TabIndex = 218;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(316, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 17);
            this.label9.TabIndex = 217;
            this.label9.Text = "包装应打包数量";
            // 
            // spedPackingPackedCount
            // 
            this.spedPackingPackedCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedPackingPackedCount.Location = new System.Drawing.Point(414, 70);
            this.spedPackingPackedCount.Name = "spedPackingPackedCount";
            this.spedPackingPackedCount.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedPackingPackedCount.Properties.Appearance.Options.UseFont = true;
            this.spedPackingPackedCount.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCount.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedPackingPackedCount.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCount.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedPackingPackedCount.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedPackingPackedCount.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedPackingPackedCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedPackingPackedCount.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedPackingPackedCount.Properties.ReadOnly = true;
            this.spedPackingPackedCount.Size = new System.Drawing.Size(52, 24);
            this.spedPackingPackedCount.TabIndex = 216;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(316, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 17);
            this.label8.TabIndex = 215;
            this.label8.Text = "分拣已下好品数";
            // 
            // spedUnloadPackedCount
            // 
            this.spedUnloadPackedCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedUnloadPackedCount.Location = new System.Drawing.Point(414, 32);
            this.spedUnloadPackedCount.Name = "spedUnloadPackedCount";
            this.spedUnloadPackedCount.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedUnloadPackedCount.Properties.Appearance.Options.UseFont = true;
            this.spedUnloadPackedCount.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCount.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedUnloadPackedCount.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCount.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedUnloadPackedCount.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedUnloadPackedCount.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedUnloadPackedCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedUnloadPackedCount.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedUnloadPackedCount.Properties.ReadOnly = true;
            this.spedUnloadPackedCount.Size = new System.Drawing.Size(52, 24);
            this.spedUnloadPackedCount.TabIndex = 214;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label62.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(7, 158);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(145, 17);
            this.label62.TabIndex = 212;
            this.label62.Text = "最后打包总盘数/尾盘余量";
            // 
            // TotalLineOutAll
            // 
            this.TotalLineOutAll.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalLineOutAll.Location = new System.Drawing.Point(414, 217);
            this.TotalLineOutAll.Name = "TotalLineOutAll";
            this.TotalLineOutAll.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotalLineOutAll.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalLineOutAll.Properties.Appearance.Options.UseBackColor = true;
            this.TotalLineOutAll.Properties.Appearance.Options.UseFont = true;
            this.TotalLineOutAll.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TotalLineOutAll.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll.Properties.AppearanceFocused.Options.UseFont = true;
            this.TotalLineOutAll.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TotalLineOutAll.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalLineOutAll.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TotalLineOutAll.Properties.ReadOnly = true;
            this.TotalLineOutAll.Size = new System.Drawing.Size(105, 24);
            this.TotalLineOutAll.TabIndex = 198;
            // 
            // speCountOfBuffTray
            // 
            this.speCountOfBuffTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speCountOfBuffTray.Location = new System.Drawing.Point(160, 383);
            this.speCountOfBuffTray.Name = "speCountOfBuffTray";
            this.speCountOfBuffTray.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.speCountOfBuffTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speCountOfBuffTray.Properties.Appearance.Options.UseBackColor = true;
            this.speCountOfBuffTray.Properties.Appearance.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfBuffTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfBuffTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfBuffTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speCountOfBuffTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speCountOfBuffTray.Properties.ReadOnly = true;
            this.speCountOfBuffTray.Size = new System.Drawing.Size(80, 24);
            this.speCountOfBuffTray.TabIndex = 196;
            // 
            // spinEdit2
            // 
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(220, 155);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit2.Properties.Appearance.Options.UseFont = true;
            this.spinEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinEdit2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit2.Properties.ReadOnly = true;
            this.spinEdit2.Size = new System.Drawing.Size(59, 24);
            this.spinEdit2.TabIndex = 206;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(41, 386);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(116, 17);
            this.label65.TabIndex = 197;
            this.label65.Text = "当前缓存盘好品个数";
            // 
            // Sped生产总量
            // 
            this.Sped生产总量.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Sped生产总量.Location = new System.Drawing.Point(160, 35);
            this.Sped生产总量.Name = "Sped生产总量";
            this.Sped生产总量.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sped生产总量.Properties.Appearance.Options.UseFont = true;
            this.Sped生产总量.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped生产总量.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Sped生产总量.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped生产总量.Properties.AppearanceFocused.Options.UseFont = true;
            this.Sped生产总量.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped生产总量.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.Sped生产总量.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Sped生产总量.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Sped生产总量.Properties.ReadOnly = true;
            this.Sped生产总量.Size = new System.Drawing.Size(119, 24);
            this.Sped生产总量.TabIndex = 207;
            // 
            // speCountOfLoadTray
            // 
            this.speCountOfLoadTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speCountOfLoadTray.Location = new System.Drawing.Point(160, 357);
            this.speCountOfLoadTray.Name = "speCountOfLoadTray";
            this.speCountOfLoadTray.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.speCountOfLoadTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speCountOfLoadTray.Properties.Appearance.Options.UseBackColor = true;
            this.speCountOfLoadTray.Properties.Appearance.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfLoadTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfLoadTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfLoadTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speCountOfLoadTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speCountOfLoadTray.Properties.ReadOnly = true;
            this.speCountOfLoadTray.Size = new System.Drawing.Size(80, 24);
            this.speCountOfLoadTray.TabIndex = 194;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label54.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(19, 248);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(133, 17);
            this.label54.TabIndex = 200;
            this.label54.Text = "正常包装/尾盘包装总盘";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(41, 360);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(116, 17);
            this.label64.TabIndex = 195;
            this.label64.Text = "当前下料盘好品个数";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(42, 38);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(112, 17);
            this.label57.TabIndex = 208;
            this.label57.Text = "生产总量(产品个数)";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label63.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(44, 307);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(113, 17);
            this.label63.TabIndex = 193;
            this.label63.Text = "PLC界面上手动更改";
            // 
            // sped产品行数
            // 
            this.sped产品行数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped产品行数.Location = new System.Drawing.Point(160, 215);
            this.sped产品行数.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sped产品行数.Name = "sped产品行数";
            this.sped产品行数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped产品行数.Properties.Appearance.Options.UseFont = true;
            this.sped产品行数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped产品行数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped产品行数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped产品行数.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped产品行数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped产品行数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped产品行数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped产品行数.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.sped产品行数.Properties.ReadOnly = true;
            this.sped产品行数.Size = new System.Drawing.Size(59, 24);
            this.sped产品行数.TabIndex = 201;
            // 
            // btmSetCurrentTrayOut
            // 
            this.btmSetCurrentTrayOut.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetCurrentTrayOut.Appearance.Options.UseFont = true;
            this.btmSetCurrentTrayOut.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTrayOut.AppearanceDisabled.Options.UseFont = true;
            this.btmSetCurrentTrayOut.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTrayOut.AppearanceHovered.Options.UseFont = true;
            this.btmSetCurrentTrayOut.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTrayOut.AppearancePressed.Options.UseFont = true;
            this.btmSetCurrentTrayOut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetCurrentTrayOut.ImageOptions.Image")));
            this.btmSetCurrentTrayOut.Location = new System.Drawing.Point(246, 364);
            this.btmSetCurrentTrayOut.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetCurrentTrayOut.Name = "btmSetCurrentTrayOut";
            this.btmSetCurrentTrayOut.Size = new System.Drawing.Size(156, 43);
            this.btmSetCurrentTrayOut.TabIndex = 191;
            this.btmSetCurrentTrayOut.Text = "设定当前已包装总盘数";
            this.btmSetCurrentTrayOut.Click += new System.EventHandler(this.btmSetCurrentTrayOut_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label61.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(62, 68);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(92, 17);
            this.label61.TabIndex = 210;
            this.label61.Text = "生产计划总盘数";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(33, 414);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(121, 17);
            this.label60.TabIndex = 97;
            this.label60.Text = "已生产总量/包装总量";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(96, 189);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(56, 17);
            this.label53.TabIndex = 205;
            this.label53.Text = "包装总量";
            // 
            // TotalTrayOfLineOutPCS
            // 
            this.TotalTrayOfLineOutPCS.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalTrayOfLineOutPCS.Location = new System.Drawing.Point(160, 413);
            this.TotalTrayOfLineOutPCS.Name = "TotalTrayOfLineOutPCS";
            this.TotalTrayOfLineOutPCS.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotalTrayOfLineOutPCS.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalTrayOfLineOutPCS.Properties.Appearance.Options.UseBackColor = true;
            this.TotalTrayOfLineOutPCS.Properties.Appearance.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOutPCS.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOutPCS.Properties.AppearanceFocused.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOutPCS.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalTrayOfLineOutPCS.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TotalTrayOfLineOutPCS.Properties.ReadOnly = true;
            this.TotalTrayOfLineOutPCS.Size = new System.Drawing.Size(80, 24);
            this.TotalTrayOfLineOutPCS.TabIndex = 96;
            this.TotalTrayOfLineOutPCS.EditValueChanged += new System.EventHandler(this.TotalTrayOfLineOutPCS_EditValueChanged);
            // 
            // UnloadCfg_CountTrayBK
            // 
            this.UnloadCfg_CountTrayBK.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_CountTrayBK.Location = new System.Drawing.Point(160, 245);
            this.UnloadCfg_CountTrayBK.Name = "UnloadCfg_CountTrayBK";
            this.UnloadCfg_CountTrayBK.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_CountTrayBK.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTrayBK.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTrayBK.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTrayBK.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_CountTrayBK.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.UnloadCfg_CountTrayBK.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_CountTrayBK.Properties.ReadOnly = true;
            this.UnloadCfg_CountTrayBK.Size = new System.Drawing.Size(59, 24);
            this.UnloadCfg_CountTrayBK.TabIndex = 213;
            // 
            // spedSetTrayOuted
            // 
            this.spedSetTrayOuted.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedSetTrayOuted.Location = new System.Drawing.Point(246, 329);
            this.spedSetTrayOuted.Name = "spedSetTrayOuted";
            this.spedSetTrayOuted.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.spedSetTrayOuted.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedSetTrayOuted.Properties.Appearance.Options.UseBackColor = true;
            this.spedSetTrayOuted.Properties.Appearance.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetTrayOuted.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetTrayOuted.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetTrayOuted.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedSetTrayOuted.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedSetTrayOuted.Size = new System.Drawing.Size(71, 24);
            this.spedSetTrayOuted.TabIndex = 95;
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl63.Appearance.Options.UseFont = true;
            this.labelControl63.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl63.AppearanceDisabled.Options.UseFont = true;
            this.labelControl63.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl63.AppearanceHovered.Options.UseFont = true;
            this.labelControl63.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl63.AppearancePressed.Options.UseFont = true;
            this.labelControl63.Location = new System.Drawing.Point(87, 220);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(65, 17);
            this.labelControl63.TabIndex = 202;
            this.labelControl63.Text = "产品行/例数";
            // 
            // TotalTrayOfLineOut
            // 
            this.TotalTrayOfLineOut.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalTrayOfLineOut.Location = new System.Drawing.Point(160, 329);
            this.TotalTrayOfLineOut.Name = "TotalTrayOfLineOut";
            this.TotalTrayOfLineOut.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotalTrayOfLineOut.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalTrayOfLineOut.Properties.Appearance.Options.UseBackColor = true;
            this.TotalTrayOfLineOut.Properties.Appearance.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOut.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOut.Properties.AppearanceFocused.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOut.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalTrayOfLineOut.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TotalTrayOfLineOut.Properties.ReadOnly = true;
            this.TotalTrayOfLineOut.Size = new System.Drawing.Size(80, 24);
            this.TotalTrayOfLineOut.TabIndex = 93;
            // 
            // sped生产总盘数
            // 
            this.sped生产总盘数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped生产总盘数.Location = new System.Drawing.Point(160, 64);
            this.sped生产总盘数.Name = "sped生产总盘数";
            this.sped生产总盘数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped生产总盘数.Properties.Appearance.Options.UseFont = true;
            this.sped生产总盘数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped生产总盘数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped生产总盘数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped生产总盘数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped生产总盘数.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped生产总盘数.Properties.ReadOnly = true;
            this.sped生产总盘数.Size = new System.Drawing.Size(92, 24);
            this.sped生产总盘数.TabIndex = 209;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(33, 332);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(124, 17);
            this.label59.TabIndex = 94;
            this.label59.Text = "当前已包装盘数(所有)";
            // 
            // Sped包装总量
            // 
            this.Sped包装总量.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Sped包装总量.Location = new System.Drawing.Point(160, 185);
            this.Sped包装总量.Name = "Sped包装总量";
            this.Sped包装总量.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sped包装总量.Properties.Appearance.Options.UseFont = true;
            this.Sped包装总量.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped包装总量.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Sped包装总量.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped包装总量.Properties.AppearanceFocused.Options.UseFont = true;
            this.Sped包装总量.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped包装总量.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.Sped包装总量.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Sped包装总量.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Sped包装总量.Properties.ReadOnly = true;
            this.Sped包装总量.Size = new System.Drawing.Size(119, 24);
            this.Sped包装总量.TabIndex = 204;
            // 
            // CurrentCountTray
            // 
            this.CurrentCountTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CurrentCountTray.Location = new System.Drawing.Point(160, 294);
            this.CurrentCountTray.Name = "CurrentCountTray";
            this.CurrentCountTray.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.CurrentCountTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentCountTray.Properties.Appearance.Options.UseBackColor = true;
            this.CurrentCountTray.Properties.Appearance.Options.UseFont = true;
            this.CurrentCountTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CurrentCountTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.CurrentCountTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CurrentCountTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.CurrentCountTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CurrentCountTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.CurrentCountTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CurrentCountTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.CurrentCountTray.Properties.ReadOnly = true;
            this.CurrentCountTray.Size = new System.Drawing.Size(80, 24);
            this.CurrentCountTray.TabIndex = 89;
            // 
            // 生产最后打包总盘数
            // 
            this.生产最后打包总盘数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.生产最后打包总盘数.Location = new System.Drawing.Point(160, 155);
            this.生产最后打包总盘数.Name = "生产最后打包总盘数";
            this.生产最后打包总盘数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.生产最后打包总盘数.Properties.Appearance.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.生产最后打包总盘数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.生产最后打包总盘数.Properties.AppearanceFocused.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.生产最后打包总盘数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.生产最后打包总盘数.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.生产最后打包总盘数.Properties.ReadOnly = true;
            this.生产最后打包总盘数.Size = new System.Drawing.Size(59, 24);
            this.生产最后打包总盘数.TabIndex = 211;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(33, 288);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(124, 17);
            this.label56.TabIndex = 90;
            this.label56.Text = "实时包装盘数(包装机)";
            // 
            // sped产品例数
            // 
            this.sped产品例数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped产品例数.Location = new System.Drawing.Point(220, 214);
            this.sped产品例数.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sped产品例数.Name = "sped产品例数";
            this.sped产品例数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped产品例数.Properties.Appearance.Options.UseFont = true;
            this.sped产品例数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped产品例数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped产品例数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped产品例数.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped产品例数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped产品例数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped产品例数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped产品例数.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.sped产品例数.Properties.ReadOnly = true;
            this.sped产品例数.Size = new System.Drawing.Size(59, 24);
            this.sped产品例数.TabIndex = 203;
            // 
            // spinEdit4
            // 
            this.spinEdit4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit4.Location = new System.Drawing.Point(220, 245);
            this.spinEdit4.Name = "spinEdit4";
            this.spinEdit4.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit4.Properties.Appearance.Options.UseFont = true;
            this.spinEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinEdit4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit4.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit4.Properties.ReadOnly = true;
            this.spinEdit4.Size = new System.Drawing.Size(59, 24);
            this.spinEdit4.TabIndex = 199;
            // 
            // btmGetCurrentWorkSheet
            // 
            this.btmGetCurrentWorkSheet.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmGetCurrentWorkSheet.Appearance.Options.UseFont = true;
            this.btmGetCurrentWorkSheet.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmGetCurrentWorkSheet.AppearanceDisabled.Options.UseFont = true;
            this.btmGetCurrentWorkSheet.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmGetCurrentWorkSheet.AppearanceHovered.Options.UseFont = true;
            this.btmGetCurrentWorkSheet.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmGetCurrentWorkSheet.AppearancePressed.Options.UseFont = true;
            this.btmGetCurrentWorkSheet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmGetCurrentWorkSheet.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmGetCurrentWorkSheet.ImageOptions.Image")));
            this.btmGetCurrentWorkSheet.Location = new System.Drawing.Point(1244, 9);
            this.btmGetCurrentWorkSheet.LookAndFeel.SkinName = "Office 2007 Blue";
            this.btmGetCurrentWorkSheet.Name = "btmGetCurrentWorkSheet";
            this.btmGetCurrentWorkSheet.Size = new System.Drawing.Size(442, 54);
            this.btmGetCurrentWorkSheet.TabIndex = 187;
            this.btmGetCurrentWorkSheet.Text = "得到当前工单信息(激活测试)";
            this.btmGetCurrentWorkSheet.Click += new System.EventHandler(this.btmGetCurrentWorkSheet_Click);
            // 
            // propertyGrid2
            // 
            this.propertyGrid2.Location = new System.Drawing.Point(1244, 65);
            this.propertyGrid2.Name = "propertyGrid2";
            this.propertyGrid2.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.propertyGrid2.Size = new System.Drawing.Size(442, 565);
            this.propertyGrid2.TabIndex = 186;
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl61.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Appearance.Options.UseBackColor = true;
            this.labelControl61.Appearance.Options.UseFont = true;
            this.labelControl61.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl61.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl61.Location = new System.Drawing.Point(966, 31);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(136, 17);
            this.labelControl61.TabIndex = 185;
            this.labelControl61.Text = "已包装的虚拟PCBID 板号";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button1.Location = new System.Drawing.Point(1101, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 39);
            this.button1.TabIndex = 184;
            this.button1.Text = "清除所有已下料盘信息";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbUnloadPCBs
            // 
            this.cmbUnloadPCBs.FormattingEnabled = true;
            this.cmbUnloadPCBs.HorizontalScrollbar = true;
            this.cmbUnloadPCBs.ItemHeight = 17;
            this.cmbUnloadPCBs.Location = new System.Drawing.Point(966, 56);
            this.cmbUnloadPCBs.Name = "cmbUnloadPCBs";
            this.cmbUnloadPCBs.ScrollAlwaysVisible = true;
            this.cmbUnloadPCBs.Size = new System.Drawing.Size(272, 242);
            this.cmbUnloadPCBs.TabIndex = 183;
            // 
            // groupControl10
            // 
            this.groupControl10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl10.Appearance.Options.UseFont = true;
            this.groupControl10.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl10.AppearanceCaption.Options.UseFont = true;
            this.groupControl10.Controls.Add(this.labelControl34);
            this.groupControl10.Controls.Add(this.labelControl33);
            this.groupControl10.Controls.Add(this.labelControl32);
            this.groupControl10.Controls.Add(this.labelControl31);
            this.groupControl10.Controls.Add(this.labelControl30);
            this.groupControl10.Controls.Add(this.labelControl29);
            this.groupControl10.Controls.Add(this.labelControl28);
            this.groupControl10.Controls.Add(this.txtD2014);
            this.groupControl10.Controls.Add(this.txtD2001);
            this.groupControl10.Controls.Add(this.labelControl18);
            this.groupControl10.Controls.Add(this.labelControl2);
            this.groupControl10.Controls.Add(this.txtD2008);
            this.groupControl10.Controls.Add(this.txtD2009);
            this.groupControl10.Controls.Add(this.txtD2011);
            this.groupControl10.Controls.Add(this.labelControl19);
            this.groupControl10.Controls.Add(this.labelControl3);
            this.groupControl10.Controls.Add(this.labelControl4);
            this.groupControl10.Controls.Add(this.txtD2004);
            this.groupControl10.Controls.Add(this.labelControl17);
            this.groupControl10.Controls.Add(this.txtD2010);
            this.groupControl10.Controls.Add(this.txtD2003);
            this.groupControl10.Controls.Add(this.labelControl20);
            this.groupControl10.Controls.Add(this.labelControl21);
            this.groupControl10.Controls.Add(this.txtD2013);
            this.groupControl10.Controls.Add(this.txtD2005);
            this.groupControl10.Controls.Add(this.txtD2012);
            this.groupControl10.Controls.Add(this.labelControl22);
            this.groupControl10.Controls.Add(this.txtD2002);
            this.groupControl10.Controls.Add(this.labelControl23);
            this.groupControl10.Controls.Add(this.labelControl24);
            this.groupControl10.Controls.Add(this.labelControl25);
            this.groupControl10.Controls.Add(this.txtD2006);
            this.groupControl10.Controls.Add(this.labelControl26);
            this.groupControl10.Controls.Add(this.txtD2007);
            this.groupControl10.Controls.Add(this.labelControl27);
            this.groupControl10.Location = new System.Drawing.Point(3, 3);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(266, 481);
            this.groupControl10.TabIndex = 181;
            this.groupControl10.Text = "报警I/O监控";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl34.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Appearance.Options.UseBackColor = true;
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl34.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl34.Location = new System.Drawing.Point(182, 429);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(60, 17);
            this.labelControl34.TabIndex = 163;
            this.labelControl34.Text = "机器人工位";
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl33.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Appearance.Options.UseBackColor = true;
            this.labelControl33.Appearance.Options.UseFont = true;
            this.labelControl33.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl33.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl33.Location = new System.Drawing.Point(182, 366);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(48, 17);
            this.labelControl33.TabIndex = 162;
            this.labelControl33.Text = "包装工位";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl32.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Appearance.Options.UseBackColor = true;
            this.labelControl32.Appearance.Options.UseFont = true;
            this.labelControl32.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl32.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl32.Location = new System.Drawing.Point(182, 301);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(48, 17);
            this.labelControl32.TabIndex = 161;
            this.labelControl32.Text = "下料工位";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl31.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Appearance.Options.UseBackColor = true;
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl31.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl31.Location = new System.Drawing.Point(182, 176);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(55, 17);
            this.labelControl31.TabIndex = 160;
            this.labelControl31.Text = "烧录2工位";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl30.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.Options.UseBackColor = true;
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl30.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl30.Location = new System.Drawing.Point(182, 238);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(48, 17);
            this.labelControl30.TabIndex = 159;
            this.labelControl30.Text = "打码工位";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl29.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.Options.UseBackColor = true;
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl29.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl29.Location = new System.Drawing.Point(182, 112);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(55, 17);
            this.labelControl29.TabIndex = 158;
            this.labelControl29.Text = "烧录1工位";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl28.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.Options.UseBackColor = true;
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl28.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl28.Location = new System.Drawing.Point(182, 48);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(48, 17);
            this.labelControl28.TabIndex = 157;
            this.labelControl28.Text = "上料工位";
            // 
            // txtD2014
            // 
            this.txtD2014.Location = new System.Drawing.Point(139, 434);
            this.txtD2014.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2014.Name = "txtD2014";
            this.txtD2014.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2014.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2014.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2014.Properties.Appearance.Options.UseFont = true;
            this.txtD2014.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2014.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2014.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2014.Properties.ReadOnly = true;
            this.txtD2014.Size = new System.Drawing.Size(37, 22);
            this.txtD2014.TabIndex = 156;
            // 
            // txtD2001
            // 
            this.txtD2001.EditValue = "";
            this.txtD2001.Location = new System.Drawing.Point(139, 32);
            this.txtD2001.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2001.Name = "txtD2001";
            this.txtD2001.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2001.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2001.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2001.Properties.Appearance.Options.UseFont = true;
            this.txtD2001.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2001.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2001.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2001.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtD2001.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2001.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2001.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtD2001.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2001.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2001.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtD2001.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2001.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2001.Properties.ReadOnly = true;
            this.txtD2001.Size = new System.Drawing.Size(37, 22);
            this.txtD2001.TabIndex = 129;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.Options.UseBackColor = true;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl18.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl18.Location = new System.Drawing.Point(44, 37);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(89, 17);
            this.labelControl18.TabIndex = 154;
            this.labelControl18.Text = "D2001 工作状态";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseBackColor = true;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl2.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl2.Location = new System.Drawing.Point(20, 313);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(113, 17);
            this.labelControl2.TabIndex = 146;
            this.labelControl2.Text = "D2010 报警错误代码";
            // 
            // txtD2008
            // 
            this.txtD2008.Location = new System.Drawing.Point(139, 245);
            this.txtD2008.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2008.Name = "txtD2008";
            this.txtD2008.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2008.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2008.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2008.Properties.Appearance.Options.UseFont = true;
            this.txtD2008.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2008.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2008.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2008.Properties.ReadOnly = true;
            this.txtD2008.Size = new System.Drawing.Size(37, 22);
            this.txtD2008.TabIndex = 136;
            // 
            // txtD2009
            // 
            this.txtD2009.Location = new System.Drawing.Point(139, 285);
            this.txtD2009.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2009.Name = "txtD2009";
            this.txtD2009.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2009.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2009.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2009.Properties.Appearance.Options.UseFont = true;
            this.txtD2009.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2009.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2009.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2009.Properties.ReadOnly = true;
            this.txtD2009.Size = new System.Drawing.Size(37, 22);
            this.txtD2009.TabIndex = 137;
            // 
            // txtD2011
            // 
            this.txtD2011.Location = new System.Drawing.Point(139, 348);
            this.txtD2011.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2011.Name = "txtD2011";
            this.txtD2011.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2011.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2011.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2011.Properties.Appearance.Options.UseFont = true;
            this.txtD2011.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2011.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2011.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2011.Properties.ReadOnly = true;
            this.txtD2011.Size = new System.Drawing.Size(37, 22);
            this.txtD2011.TabIndex = 151;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl19.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl19.Location = new System.Drawing.Point(20, 438);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(113, 17);
            this.labelControl19.TabIndex = 153;
            this.labelControl19.Text = "D2014 报警错误代码";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl3.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl3.Location = new System.Drawing.Point(44, 352);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(89, 17);
            this.labelControl3.TabIndex = 147;
            this.labelControl3.Text = "D2011 工作状态";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl4.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl4.Location = new System.Drawing.Point(44, 163);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(89, 17);
            this.labelControl4.TabIndex = 141;
            this.labelControl4.Text = "D2005 工作状态";
            // 
            // txtD2004
            // 
            this.txtD2004.Location = new System.Drawing.Point(139, 118);
            this.txtD2004.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2004.Name = "txtD2004";
            this.txtD2004.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2004.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2004.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2004.Properties.Appearance.Options.UseFont = true;
            this.txtD2004.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2004.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2004.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2004.Properties.ReadOnly = true;
            this.txtD2004.Size = new System.Drawing.Size(37, 22);
            this.txtD2004.TabIndex = 132;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.Options.UseBackColor = true;
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl17.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl17.Location = new System.Drawing.Point(20, 59);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(113, 17);
            this.labelControl17.TabIndex = 138;
            this.labelControl17.Text = "D2002 报警错误代码";
            // 
            // txtD2010
            // 
            this.txtD2010.Location = new System.Drawing.Point(139, 308);
            this.txtD2010.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2010.Name = "txtD2010";
            this.txtD2010.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtD2010.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2010.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2010.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2010.Properties.Appearance.Options.UseFont = true;
            this.txtD2010.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2010.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2010.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2010.Properties.ReadOnly = true;
            this.txtD2010.Size = new System.Drawing.Size(37, 22);
            this.txtD2010.TabIndex = 150;
            // 
            // txtD2003
            // 
            this.txtD2003.Location = new System.Drawing.Point(139, 95);
            this.txtD2003.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2003.Name = "txtD2003";
            this.txtD2003.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2003.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2003.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2003.Properties.Appearance.Options.UseFont = true;
            this.txtD2003.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2003.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2003.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2003.Properties.ReadOnly = true;
            this.txtD2003.Size = new System.Drawing.Size(37, 22);
            this.txtD2003.TabIndex = 131;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl20.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl20.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl20.Location = new System.Drawing.Point(20, 187);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(113, 17);
            this.labelControl20.TabIndex = 142;
            this.labelControl20.Text = "D2006 报警错误代码";
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseBackColor = true;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl21.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl21.Location = new System.Drawing.Point(20, 375);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(113, 17);
            this.labelControl21.TabIndex = 148;
            this.labelControl21.Text = "D2012 报警错误代码";
            // 
            // txtD2013
            // 
            this.txtD2013.Location = new System.Drawing.Point(139, 411);
            this.txtD2013.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2013.Name = "txtD2013";
            this.txtD2013.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2013.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2013.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2013.Properties.Appearance.Options.UseFont = true;
            this.txtD2013.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2013.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2013.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2013.Properties.ReadOnly = true;
            this.txtD2013.Size = new System.Drawing.Size(37, 22);
            this.txtD2013.TabIndex = 155;
            // 
            // txtD2005
            // 
            this.txtD2005.Location = new System.Drawing.Point(139, 159);
            this.txtD2005.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2005.Name = "txtD2005";
            this.txtD2005.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2005.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2005.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2005.Properties.Appearance.Options.UseFont = true;
            this.txtD2005.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2005.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2005.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2005.Properties.ReadOnly = true;
            this.txtD2005.Size = new System.Drawing.Size(37, 22);
            this.txtD2005.TabIndex = 133;
            // 
            // txtD2012
            // 
            this.txtD2012.Location = new System.Drawing.Point(139, 371);
            this.txtD2012.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2012.Name = "txtD2012";
            this.txtD2012.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2012.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2012.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2012.Properties.Appearance.Options.UseFont = true;
            this.txtD2012.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2012.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2012.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2012.Properties.ReadOnly = true;
            this.txtD2012.Size = new System.Drawing.Size(37, 22);
            this.txtD2012.TabIndex = 152;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl22.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.Options.UseBackColor = true;
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl22.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl22.Location = new System.Drawing.Point(44, 416);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(89, 17);
            this.labelControl22.TabIndex = 149;
            this.labelControl22.Text = "D2013 工作状态";
            // 
            // txtD2002
            // 
            this.txtD2002.Location = new System.Drawing.Point(139, 55);
            this.txtD2002.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2002.Name = "txtD2002";
            this.txtD2002.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2002.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2002.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2002.Properties.Appearance.Options.UseFont = true;
            this.txtD2002.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2002.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2002.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2002.Properties.ReadOnly = true;
            this.txtD2002.Size = new System.Drawing.Size(37, 22);
            this.txtD2002.TabIndex = 130;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl23.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.Options.UseBackColor = true;
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl23.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl23.Location = new System.Drawing.Point(44, 226);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(89, 17);
            this.labelControl23.TabIndex = 143;
            this.labelControl23.Text = "D2007 工作状态";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl24.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.Options.UseBackColor = true;
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl24.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl24.Location = new System.Drawing.Point(44, 289);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(89, 17);
            this.labelControl24.TabIndex = 145;
            this.labelControl24.Text = "D2009 工作状态";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.Options.UseBackColor = true;
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl25.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl25.Location = new System.Drawing.Point(44, 100);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(89, 17);
            this.labelControl25.TabIndex = 139;
            this.labelControl25.Text = "D2003 工作状态";
            // 
            // txtD2006
            // 
            this.txtD2006.Location = new System.Drawing.Point(139, 182);
            this.txtD2006.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2006.Name = "txtD2006";
            this.txtD2006.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2006.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2006.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2006.Properties.Appearance.Options.UseFont = true;
            this.txtD2006.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2006.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2006.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2006.Properties.ReadOnly = true;
            this.txtD2006.Size = new System.Drawing.Size(37, 22);
            this.txtD2006.TabIndex = 134;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl26.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl26.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl26.Location = new System.Drawing.Point(20, 122);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(113, 17);
            this.labelControl26.TabIndex = 140;
            this.labelControl26.Text = "D2004 报警错误代码";
            // 
            // txtD2007
            // 
            this.txtD2007.Location = new System.Drawing.Point(139, 222);
            this.txtD2007.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD2007.Name = "txtD2007";
            this.txtD2007.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD2007.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD2007.Properties.Appearance.Options.UseBackColor = true;
            this.txtD2007.Properties.Appearance.Options.UseFont = true;
            this.txtD2007.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD2007.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD2007.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD2007.Properties.ReadOnly = true;
            this.txtD2007.Size = new System.Drawing.Size(37, 22);
            this.txtD2007.TabIndex = 135;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl27.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Appearance.Options.UseBackColor = true;
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl27.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl27.Location = new System.Drawing.Point(20, 250);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(113, 17);
            this.labelControl27.TabIndex = 144;
            this.labelControl27.Text = "D2008 报警错误代码";
            // 
            // NP_手动调试
            // 
            this.NP_手动调试.Caption = " 手动调试";
            this.NP_手动调试.Controls.Add(this.xtraTabControl1);
            this.NP_手动调试.Controls.Add(this.groupControl6);
            this.NP_手动调试.Controls.Add(this.groupControl4);
            this.NP_手动调试.Image = ((System.Drawing.Image)(resources.GetObject("NP_手动调试.Image")));
            this.NP_手动调试.Name = "NP_手动调试";
            this.NP_手动调试.Size = new System.Drawing.Size(1702, 647);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.Appearance.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.PageClient.Options.UseFont = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(441, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1267, 652);
            this.xtraTabControl1.TabIndex = 100;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderDisabled.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabPage1.Appearance.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.PageClient.Options.UseFont = true;
            this.xtraTabPage1.Controls.Add(this.panelControl8);
            this.xtraTabPage1.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.Image")));
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1261, 605);
            this.xtraTabPage1.Text = "测试工单/虚拟工单";
            // 
            // panelControl8
            // 
            this.panelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl8.Controls.Add(this.labelControl66);
            this.panelControl8.Controls.Add(this.textEdit1);
            this.panelControl8.Controls.Add(this.手动获取当前选中工单及转工单);
            this.panelControl8.Controls.Add(this.btmLoad);
            this.panelControl8.Controls.Add(this.listView1);
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1261, 610);
            this.panelControl8.TabIndex = 0;
            // 
            // labelControl66
            // 
            this.labelControl66.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl66.Appearance.Options.UseFont = true;
            this.labelControl66.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl66.AppearanceDisabled.Options.UseFont = true;
            this.labelControl66.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl66.AppearanceHovered.Options.UseFont = true;
            this.labelControl66.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl66.AppearancePressed.Options.UseFont = true;
            this.labelControl66.Location = new System.Drawing.Point(437, 551);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(560, 28);
            this.labelControl66.TabIndex = 30;
            this.labelControl66.Text = "当前选中及更改的工单号(请确认工单号是否有进行其它更改)";
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(6, 580);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit1.Size = new System.Drawing.Size(1250, 24);
            this.textEdit1.TabIndex = 29;
            // 
            // 手动获取当前选中工单及转工单
            // 
            this.手动获取当前选中工单及转工单.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.手动获取当前选中工单及转工单.Appearance.Options.UseFont = true;
            this.手动获取当前选中工单及转工单.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动获取当前选中工单及转工单.AppearanceDisabled.Options.UseFont = true;
            this.手动获取当前选中工单及转工单.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动获取当前选中工单及转工单.AppearanceHovered.Options.UseFont = true;
            this.手动获取当前选中工单及转工单.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动获取当前选中工单及转工单.AppearancePressed.Options.UseFont = true;
            this.手动获取当前选中工单及转工单.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.手动获取当前选中工单及转工单.Location = new System.Drawing.Point(176, 510);
            this.手动获取当前选中工单及转工单.Name = "手动获取当前选中工单及转工单";
            this.手动获取当前选中工单及转工单.Size = new System.Drawing.Size(229, 65);
            this.手动获取当前选中工单及转工单.TabIndex = 28;
            this.手动获取当前选中工单及转工单.Text = "手动获取当前选中工单及转工单";
            this.手动获取当前选中工单及转工单.Click += new System.EventHandler(this.手动获取当前选中工单及转工单_Click);
            // 
            // btmLoad
            // 
            this.btmLoad.BackColor = System.Drawing.Color.Moccasin;
            this.btmLoad.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmLoad.Location = new System.Drawing.Point(6, 507);
            this.btmLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmLoad.Name = "btmLoad";
            this.btmLoad.Size = new System.Drawing.Size(164, 69);
            this.btmLoad.TabIndex = 27;
            this.btmLoad.Text = "加载虚拟工单文件(&O)";
            this.btmLoad.UseVisualStyleBackColor = false;
            this.btmLoad.Click += new System.EventHandler(this.btmLoad_Click);
            // 
            // listView1
            // 
            this.listView1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(6, 8);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1250, 494);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1261, 605);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl6.AppearanceCaption.Options.UseFont = true;
            this.groupControl6.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("groupControl6.CaptionImageOptions.Image")));
            this.groupControl6.Controls.Add(this.groupControl7);
            this.groupControl6.Controls.Add(this.labelControl11);
            this.groupControl6.Controls.Add(this.comboBoxEdit1);
            this.groupControl6.Controls.Add(this.labelControl10);
            this.groupControl6.Controls.Add(this.memoEdit2);
            this.groupControl6.Controls.Add(this.simpleButton9);
            this.groupControl6.Controls.Add(this.simpleButton8);
            this.groupControl6.Controls.Add(this.textEdit4);
            this.groupControl6.Location = new System.Drawing.Point(0, 321);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(431, 326);
            this.groupControl6.TabIndex = 99;
            this.groupControl6.Text = "TCP/IP Client 读/写";
            // 
            // groupControl7
            // 
            this.groupControl7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl7.Appearance.Options.UseFont = true;
            this.groupControl7.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl7.AppearanceCaption.Options.UseFont = true;
            this.groupControl7.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("groupControl7.CaptionImageOptions.Image")));
            this.groupControl7.Controls.Add(this.checkEdit8);
            this.groupControl7.Controls.Add(this.btmSNTriggerPacking);
            this.groupControl7.Controls.Add(this.btmSNTriggerUnload);
            this.groupControl7.Controls.Add(this.btmSNTriggerLaser);
            this.groupControl7.Controls.Add(this.btmSNTriggerFlash2);
            this.groupControl7.Controls.Add(this.btmSNTriggerFlash1);
            this.groupControl7.Controls.Add(this.btmSNTriggerLoad);
            this.groupControl7.Controls.Add(this.txtSN_B3);
            this.groupControl7.Controls.Add(this.txtSN_B2);
            this.groupControl7.Controls.Add(this.txtSN_B1);
            this.groupControl7.Controls.Add(this.txtSN_A3);
            this.groupControl7.Controls.Add(this.txtSN_A2);
            this.groupControl7.Controls.Add(this.txtSN_A1);
            this.groupControl7.Location = new System.Drawing.Point(288, 49);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(127, 40);
            this.groupControl7.TabIndex = 100;
            this.groupControl7.Text = "条码枪手动控制";
            this.groupControl7.Visible = false;
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(6, 322);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit8.Properties.Appearance.Options.UseFont = true;
            this.checkEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.checkEdit8.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkEdit8.Properties.AppearanceFocused.Options.UseFont = true;
            this.checkEdit8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkEdit8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.checkEdit8.Properties.Caption = "MES校验(读取时对工艺进行验证)";
            this.checkEdit8.Size = new System.Drawing.Size(233, 21);
            this.checkEdit8.TabIndex = 97;
            // 
            // btmSNTriggerPacking
            // 
            this.btmSNTriggerPacking.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSNTriggerPacking.Appearance.Options.UseFont = true;
            this.btmSNTriggerPacking.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerPacking.AppearanceDisabled.Options.UseFont = true;
            this.btmSNTriggerPacking.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerPacking.AppearanceHovered.Options.UseFont = true;
            this.btmSNTriggerPacking.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerPacking.AppearancePressed.Options.UseFont = true;
            this.btmSNTriggerPacking.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSNTriggerPacking.ImageOptions.Image")));
            this.btmSNTriggerPacking.Location = new System.Drawing.Point(5, 267);
            this.btmSNTriggerPacking.Name = "btmSNTriggerPacking";
            this.btmSNTriggerPacking.Size = new System.Drawing.Size(122, 32);
            this.btmSNTriggerPacking.TabIndex = 96;
            this.btmSNTriggerPacking.Text = "包装 读取";
            // 
            // btmSNTriggerUnload
            // 
            this.btmSNTriggerUnload.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSNTriggerUnload.Appearance.Options.UseFont = true;
            this.btmSNTriggerUnload.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerUnload.AppearanceDisabled.Options.UseFont = true;
            this.btmSNTriggerUnload.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerUnload.AppearanceHovered.Options.UseFont = true;
            this.btmSNTriggerUnload.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerUnload.AppearancePressed.Options.UseFont = true;
            this.btmSNTriggerUnload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSNTriggerUnload.ImageOptions.Image")));
            this.btmSNTriggerUnload.Location = new System.Drawing.Point(5, 222);
            this.btmSNTriggerUnload.Name = "btmSNTriggerUnload";
            this.btmSNTriggerUnload.Size = new System.Drawing.Size(122, 32);
            this.btmSNTriggerUnload.TabIndex = 95;
            this.btmSNTriggerUnload.Text = "下料装盘 读取";
            // 
            // btmSNTriggerLaser
            // 
            this.btmSNTriggerLaser.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSNTriggerLaser.Appearance.Options.UseFont = true;
            this.btmSNTriggerLaser.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerLaser.AppearanceDisabled.Options.UseFont = true;
            this.btmSNTriggerLaser.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerLaser.AppearanceHovered.Options.UseFont = true;
            this.btmSNTriggerLaser.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerLaser.AppearancePressed.Options.UseFont = true;
            this.btmSNTriggerLaser.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSNTriggerLaser.ImageOptions.Image")));
            this.btmSNTriggerLaser.Location = new System.Drawing.Point(5, 177);
            this.btmSNTriggerLaser.Name = "btmSNTriggerLaser";
            this.btmSNTriggerLaser.Size = new System.Drawing.Size(122, 32);
            this.btmSNTriggerLaser.TabIndex = 94;
            this.btmSNTriggerLaser.Text = "打码切割 读取";
            // 
            // btmSNTriggerFlash2
            // 
            this.btmSNTriggerFlash2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSNTriggerFlash2.Appearance.Options.UseFont = true;
            this.btmSNTriggerFlash2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerFlash2.AppearanceDisabled.Options.UseFont = true;
            this.btmSNTriggerFlash2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerFlash2.AppearanceHovered.Options.UseFont = true;
            this.btmSNTriggerFlash2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerFlash2.AppearancePressed.Options.UseFont = true;
            this.btmSNTriggerFlash2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSNTriggerFlash2.ImageOptions.Image")));
            this.btmSNTriggerFlash2.Location = new System.Drawing.Point(5, 132);
            this.btmSNTriggerFlash2.Name = "btmSNTriggerFlash2";
            this.btmSNTriggerFlash2.Size = new System.Drawing.Size(122, 32);
            this.btmSNTriggerFlash2.TabIndex = 93;
            this.btmSNTriggerFlash2.Text = "烧录2 读取";
            // 
            // btmSNTriggerFlash1
            // 
            this.btmSNTriggerFlash1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSNTriggerFlash1.Appearance.Options.UseFont = true;
            this.btmSNTriggerFlash1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerFlash1.AppearanceDisabled.Options.UseFont = true;
            this.btmSNTriggerFlash1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerFlash1.AppearanceHovered.Options.UseFont = true;
            this.btmSNTriggerFlash1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerFlash1.AppearancePressed.Options.UseFont = true;
            this.btmSNTriggerFlash1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSNTriggerFlash1.ImageOptions.Image")));
            this.btmSNTriggerFlash1.Location = new System.Drawing.Point(5, 87);
            this.btmSNTriggerFlash1.Name = "btmSNTriggerFlash1";
            this.btmSNTriggerFlash1.Size = new System.Drawing.Size(122, 32);
            this.btmSNTriggerFlash1.TabIndex = 92;
            this.btmSNTriggerFlash1.Text = "烧录1 读取";
            // 
            // btmSNTriggerLoad
            // 
            this.btmSNTriggerLoad.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSNTriggerLoad.Appearance.Options.UseFont = true;
            this.btmSNTriggerLoad.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerLoad.AppearanceDisabled.Options.UseFont = true;
            this.btmSNTriggerLoad.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerLoad.AppearanceHovered.Options.UseFont = true;
            this.btmSNTriggerLoad.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSNTriggerLoad.AppearancePressed.Options.UseFont = true;
            this.btmSNTriggerLoad.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSNTriggerLoad.ImageOptions.Image")));
            this.btmSNTriggerLoad.Location = new System.Drawing.Point(5, 42);
            this.btmSNTriggerLoad.Name = "btmSNTriggerLoad";
            this.btmSNTriggerLoad.Size = new System.Drawing.Size(122, 32);
            this.btmSNTriggerLoad.TabIndex = 91;
            this.btmSNTriggerLoad.Text = "上料 读取";
            // 
            // txtSN_B3
            // 
            this.txtSN_B3.Location = new System.Drawing.Point(129, 270);
            this.txtSN_B3.Name = "txtSN_B3";
            this.txtSN_B3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN_B3.Properties.Appearance.Options.UseFont = true;
            this.txtSN_B3.Size = new System.Drawing.Size(167, 26);
            this.txtSN_B3.TabIndex = 90;
            // 
            // txtSN_B2
            // 
            this.txtSN_B2.Location = new System.Drawing.Point(129, 225);
            this.txtSN_B2.Name = "txtSN_B2";
            this.txtSN_B2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN_B2.Properties.Appearance.Options.UseFont = true;
            this.txtSN_B2.Size = new System.Drawing.Size(167, 26);
            this.txtSN_B2.TabIndex = 89;
            // 
            // txtSN_B1
            // 
            this.txtSN_B1.Location = new System.Drawing.Point(129, 180);
            this.txtSN_B1.Name = "txtSN_B1";
            this.txtSN_B1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN_B1.Properties.Appearance.Options.UseFont = true;
            this.txtSN_B1.Size = new System.Drawing.Size(167, 26);
            this.txtSN_B1.TabIndex = 88;
            // 
            // txtSN_A3
            // 
            this.txtSN_A3.Location = new System.Drawing.Point(129, 136);
            this.txtSN_A3.Name = "txtSN_A3";
            this.txtSN_A3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN_A3.Properties.Appearance.Options.UseFont = true;
            this.txtSN_A3.Size = new System.Drawing.Size(167, 26);
            this.txtSN_A3.TabIndex = 87;
            // 
            // txtSN_A2
            // 
            this.txtSN_A2.Location = new System.Drawing.Point(129, 90);
            this.txtSN_A2.Name = "txtSN_A2";
            this.txtSN_A2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN_A2.Properties.Appearance.Options.UseFont = true;
            this.txtSN_A2.Size = new System.Drawing.Size(167, 26);
            this.txtSN_A2.TabIndex = 86;
            // 
            // txtSN_A1
            // 
            this.txtSN_A1.Location = new System.Drawing.Point(129, 45);
            this.txtSN_A1.Name = "txtSN_A1";
            this.txtSN_A1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN_A1.Properties.Appearance.Options.UseFont = true;
            this.txtSN_A1.Size = new System.Drawing.Size(167, 26);
            this.txtSN_A1.TabIndex = 85;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl11.AppearanceDisabled.Options.UseFont = true;
            this.labelControl11.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl11.AppearanceHovered.Options.UseFont = true;
            this.labelControl11.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl11.AppearancePressed.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(13, 44);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(39, 17);
            this.labelControl11.TabIndex = 10;
            this.labelControl11.Text = "客户端:";
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(10, 65);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit1.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.comboBoxEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.comboBoxEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.comboBoxEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.Items.AddRange(new object[] {
            "上料 PC",
            "打码切割 PC",
            "包装 PC",
            "条码枪_上料",
            "条码枪_烧录1",
            "条码枪_烧录2",
            "条码枪_打码切割",
            "条码枪_下料",
            "条码枪_包装"});
            this.comboBoxEdit1.Size = new System.Drawing.Size(154, 24);
            this.comboBoxEdit1.TabIndex = 9;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl10.AppearanceDisabled.Options.UseFont = true;
            this.labelControl10.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl10.AppearanceHovered.Options.UseFont = true;
            this.labelControl10.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl10.AppearancePressed.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(13, 96);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(27, 17);
            this.labelControl10.TabIndex = 8;
            this.labelControl10.Text = "数据:";
            // 
            // memoEdit2
            // 
            this.memoEdit2.Location = new System.Drawing.Point(5, 197);
            this.memoEdit2.Name = "memoEdit2";
            this.memoEdit2.Size = new System.Drawing.Size(410, 118);
            this.memoEdit2.TabIndex = 7;
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton9.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearanceHovered.Options.UseFont = true;
            this.simpleButton9.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearancePressed.Options.UseFont = true;
            this.simpleButton9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton9.ImageOptions.Image")));
            this.simpleButton9.Location = new System.Drawing.Point(301, 112);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(114, 32);
            this.simpleButton9.TabIndex = 6;
            this.simpleButton9.Text = "发送";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton8.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearanceHovered.Options.UseFont = true;
            this.simpleButton8.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearancePressed.Options.UseFont = true;
            this.simpleButton8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton8.ImageOptions.Image")));
            this.simpleButton8.Location = new System.Drawing.Point(301, 164);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(114, 29);
            this.simpleButton8.TabIndex = 7;
            this.simpleButton8.Text = "读取(Bytes)";
            // 
            // textEdit4
            // 
            this.textEdit4.EditValue = "D1000";
            this.textEdit4.Location = new System.Drawing.Point(10, 117);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit4.Size = new System.Drawing.Size(285, 24);
            this.textEdit4.TabIndex = 1;
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl4.AppearanceCaption.Options.UseBorderColor = true;
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("groupControl4.CaptionImageOptions.Image")));
            this.groupControl4.Controls.Add(this.simpleButton10);
            this.groupControl4.Controls.Add(this.labelControl12);
            this.groupControl4.Controls.Add(this.comboBoxEdit2);
            this.groupControl4.Controls.Add(this.spinEdit1);
            this.groupControl4.Controls.Add(this.labelControl9);
            this.groupControl4.Controls.Add(this.memoEdit1);
            this.groupControl4.Controls.Add(this.simpleButton7);
            this.groupControl4.Controls.Add(this.simpleButton6);
            this.groupControl4.Controls.Add(this.labelControl8);
            this.groupControl4.Controls.Add(this.textEdit3);
            this.groupControl4.Controls.Add(this.labelControl7);
            this.groupControl4.Controls.Add(this.textEdit2);
            this.groupControl4.Location = new System.Drawing.Point(3, 3);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(431, 315);
            this.groupControl4.TabIndex = 97;
            this.groupControl4.Text = "PLC && PC 读取";
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton10.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton10.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton10.AppearanceHovered.Options.UseFont = true;
            this.simpleButton10.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton10.AppearancePressed.Options.UseFont = true;
            this.simpleButton10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton10.ImageOptions.Image")));
            this.simpleButton10.Location = new System.Drawing.Point(304, 164);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(112, 31);
            this.simpleButton10.TabIndex = 11;
            this.simpleButton10.Text = "读取(Bytes)";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearanceDisabled.Options.UseFont = true;
            this.labelControl12.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearanceHovered.Options.UseFont = true;
            this.labelControl12.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearancePressed.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(13, 47);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(75, 17);
            this.labelControl12.TabIndex = 11;
            this.labelControl12.Text = "PLC或PC选择:";
            // 
            // comboBoxEdit2
            // 
            this.comboBoxEdit2.Location = new System.Drawing.Point(10, 65);
            this.comboBoxEdit2.Name = "comboBoxEdit2";
            this.comboBoxEdit2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEdit2.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.comboBoxEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.comboBoxEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.comboBoxEdit2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboBoxEdit2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.comboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit2.Properties.Items.AddRange(new object[] {
            "PLC 上料",
            "PLC 烧录1",
            "PLC 烧录2",
            "PC 打码切割",
            "PLC 下料",
            "PLC 包装",
            "PLC 上料",
            "PLC 上料"});
            this.comboBoxEdit2.Size = new System.Drawing.Size(152, 24);
            this.comboBoxEdit2.TabIndex = 10;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(10, 168);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit1.Properties.Appearance.Options.UseFont = true;
            this.spinEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(71, 24);
            this.spinEdit1.TabIndex = 9;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl9.AppearanceDisabled.Options.UseFont = true;
            this.labelControl9.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl9.AppearanceHovered.Options.UseFont = true;
            this.labelControl9.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl9.AppearancePressed.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(12, 150);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(111, 17);
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "读取字节(Bytes)长度";
            this.labelControl9.Click += new System.EventHandler(this.labelControl9_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(6, 197);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(410, 111);
            this.memoEdit1.TabIndex = 6;
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton7.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton7.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton7.AppearanceHovered.Options.UseFont = true;
            this.simpleButton7.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton7.AppearancePressed.Options.UseFont = true;
            this.simpleButton7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton7.ImageOptions.Image")));
            this.simpleButton7.Location = new System.Drawing.Point(300, 60);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(116, 32);
            this.simpleButton7.TabIndex = 5;
            this.simpleButton7.Text = "写入(bool)";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton6.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearanceHovered.Options.UseFont = true;
            this.simpleButton6.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearancePressed.Options.UseFont = true;
            this.simpleButton6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.ImageOptions.Image")));
            this.simpleButton6.Location = new System.Drawing.Point(300, 98);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(116, 32);
            this.simpleButton6.TabIndex = 4;
            this.simpleButton6.Text = "写入(short)";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceDisabled.Options.UseFont = true;
            this.labelControl8.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceHovered.Options.UseFont = true;
            this.labelControl8.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearancePressed.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(204, 47);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(59, 17);
            this.labelControl8.TabIndex = 3;
            this.labelControl8.Text = "数值(写入):";
            // 
            // textEdit3
            // 
            this.textEdit3.EditValue = "D1000";
            this.textEdit3.Location = new System.Drawing.Point(201, 65);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit3.Size = new System.Drawing.Size(93, 24);
            this.textEdit3.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearanceDisabled.Options.UseFont = true;
            this.labelControl7.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearanceHovered.Options.UseFont = true;
            this.labelControl7.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearancePressed.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(12, 96);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(48, 17);
            this.labelControl7.TabIndex = 1;
            this.labelControl7.Text = "PLC地址:";
            // 
            // textEdit2
            // 
            this.textEdit2.EditValue = "D1000";
            this.textEdit2.Location = new System.Drawing.Point(10, 114);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit2.Size = new System.Drawing.Size(136, 24);
            this.textEdit2.TabIndex = 0;
            // 
            // NP_生产数据查询
            // 
            this.NP_生产数据查询.Caption = " 数据管理";
            this.NP_生产数据查询.Controls.Add(this.btmReconnectSQL);
            this.NP_生产数据查询.Controls.Add(this.btm工单信息刷新);
            this.NP_生产数据查询.Controls.Add(this.groupControl14);
            this.NP_生产数据查询.Controls.Add(this.btmRefencePacking);
            this.NP_生产数据查询.Controls.Add(this.btmRefenceTrayID);
            this.NP_生产数据查询.Controls.Add(this.btmReferenceResults);
            this.NP_生产数据查询.Controls.Add(this.tabPane2);
            this.NP_生产数据查询.Image = ((System.Drawing.Image)(resources.GetObject("NP_生产数据查询.Image")));
            this.NP_生产数据查询.Name = "NP_生产数据查询";
            this.NP_生产数据查询.Size = new System.Drawing.Size(1702, 647);
            // 
            // btmReconnectSQL
            // 
            this.btmReconnectSQL.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmReconnectSQL.Appearance.Options.UseFont = true;
            this.btmReconnectSQL.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReconnectSQL.AppearanceDisabled.Options.UseFont = true;
            this.btmReconnectSQL.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReconnectSQL.AppearanceHovered.Options.UseFont = true;
            this.btmReconnectSQL.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReconnectSQL.AppearancePressed.Options.UseFont = true;
            this.btmReconnectSQL.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmReconnectSQL.Location = new System.Drawing.Point(15, 539);
            this.btmReconnectSQL.Name = "btmReconnectSQL";
            this.btmReconnectSQL.Size = new System.Drawing.Size(307, 92);
            this.btmReconnectSQL.TabIndex = 248;
            this.btmReconnectSQL.Text = "关闭再次打开数据库";
            this.btmReconnectSQL.Click += new System.EventHandler(this.btmReconnectSQL_Click);
            // 
            // btm工单信息刷新
            // 
            this.btm工单信息刷新.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btm工单信息刷新.Appearance.Options.UseFont = true;
            this.btm工单信息刷新.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm工单信息刷新.AppearanceDisabled.Options.UseFont = true;
            this.btm工单信息刷新.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm工单信息刷新.AppearanceHovered.Options.UseFont = true;
            this.btm工单信息刷新.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm工单信息刷新.AppearancePressed.Options.UseFont = true;
            this.btm工单信息刷新.Location = new System.Drawing.Point(15, 240);
            this.btm工单信息刷新.Name = "btm工单信息刷新";
            this.btm工单信息刷新.Size = new System.Drawing.Size(162, 62);
            this.btm工单信息刷新.TabIndex = 247;
            this.btm工单信息刷新.Text = "工单信息刷新";
            this.btm工单信息刷新.Click += new System.EventHandler(this.btm工单信息刷新_Click);
            // 
            // groupControl14
            // 
            this.groupControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl14.Appearance.Options.UseFont = true;
            this.groupControl14.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl14.AppearanceCaption.Options.UseFont = true;
            this.groupControl14.Controls.Add(this.txtPCBIDForFind);
            this.groupControl14.Controls.Add(this.labelControl57);
            this.groupControl14.Controls.Add(this.labelControl56);
            this.groupControl14.Controls.Add(this.labelControl55);
            this.groupControl14.Controls.Add(this.dateTimePicker2);
            this.groupControl14.Controls.Add(this.labelControl58);
            this.groupControl14.Controls.Add(this.dateTimePicker1);
            this.groupControl14.Controls.Add(this.txtTrayIDForFind);
            this.groupControl14.Location = new System.Drawing.Point(3, 3);
            this.groupControl14.Name = "groupControl14";
            this.groupControl14.Size = new System.Drawing.Size(319, 191);
            this.groupControl14.TabIndex = 241;
            this.groupControl14.Text = "数据查询条件";
            // 
            // txtPCBIDForFind
            // 
            this.txtPCBIDForFind.EditValue = "";
            this.txtPCBIDForFind.Location = new System.Drawing.Point(89, 146);
            this.txtPCBIDForFind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBIDForFind.Name = "txtPCBIDForFind";
            this.txtPCBIDForFind.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBIDForFind.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPCBIDForFind.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBIDForFind.Properties.Appearance.Options.UseFont = true;
            this.txtPCBIDForFind.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBIDForFind.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBIDForFind.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBIDForFind.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBIDForFind.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBIDForFind.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBIDForFind.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBIDForFind.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBIDForFind.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBIDForFind.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBIDForFind.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBIDForFind.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBIDForFind.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBIDForFind.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBIDForFind.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBIDForFind.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBIDForFind.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBIDForFind.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBIDForFind.Size = new System.Drawing.Size(200, 24);
            this.txtPCBIDForFind.TabIndex = 251;
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl57.Appearance.Options.UseFont = true;
            this.labelControl57.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl57.AppearanceDisabled.Options.UseFont = true;
            this.labelControl57.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl57.AppearanceHovered.Options.UseFont = true;
            this.labelControl57.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl57.AppearancePressed.Options.UseFont = true;
            this.labelControl57.Location = new System.Drawing.Point(12, 151);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(71, 17);
            this.labelControl57.TabIndex = 250;
            this.labelControl57.Text = "PCB虚拟编号";
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl56.AppearanceDisabled.Options.UseFont = true;
            this.labelControl56.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl56.AppearanceHovered.Options.UseFont = true;
            this.labelControl56.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl56.AppearancePressed.Options.UseFont = true;
            this.labelControl56.Location = new System.Drawing.Point(35, 76);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(48, 17);
            this.labelControl56.TabIndex = 247;
            this.labelControl56.Text = "结束时间";
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl55.AppearanceDisabled.Options.UseFont = true;
            this.labelControl55.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl55.AppearanceHovered.Options.UseFont = true;
            this.labelControl55.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl55.AppearancePressed.Options.UseFont = true;
            this.labelControl55.Location = new System.Drawing.Point(35, 39);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(48, 17);
            this.labelControl55.TabIndex = 246;
            this.labelControl55.Text = "开始时间";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(89, 71);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker2.TabIndex = 245;
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.Appearance.Options.UseFont = true;
            this.labelControl58.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.AppearanceDisabled.Options.UseFont = true;
            this.labelControl58.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.AppearanceHovered.Options.UseFont = true;
            this.labelControl58.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.AppearancePressed.Options.UseFont = true;
            this.labelControl58.Location = new System.Drawing.Point(35, 114);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(48, 17);
            this.labelControl58.TabIndex = 249;
            this.labelControl58.Text = "托盘编号";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(89, 34);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker1.TabIndex = 244;
            this.dateTimePicker1.Value = new System.DateTime(2019, 10, 1, 0, 0, 0, 0);
            // 
            // txtTrayIDForFind
            // 
            this.txtTrayIDForFind.EditValue = "";
            this.txtTrayIDForFind.Location = new System.Drawing.Point(89, 108);
            this.txtTrayIDForFind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTrayIDForFind.Name = "txtTrayIDForFind";
            this.txtTrayIDForFind.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTrayIDForFind.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTrayIDForFind.Properties.Appearance.Options.UseBackColor = true;
            this.txtTrayIDForFind.Properties.Appearance.Options.UseFont = true;
            this.txtTrayIDForFind.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTrayIDForFind.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayIDForFind.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayIDForFind.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayIDForFind.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrayIDForFind.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTrayIDForFind.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayIDForFind.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayIDForFind.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayIDForFind.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTrayIDForFind.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTrayIDForFind.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayIDForFind.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayIDForFind.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayIDForFind.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTrayIDForFind.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTrayIDForFind.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayIDForFind.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayIDForFind.Size = new System.Drawing.Size(200, 24);
            this.txtTrayIDForFind.TabIndex = 248;
            // 
            // btmRefencePacking
            // 
            this.btmRefencePacking.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRefencePacking.Appearance.Options.UseFont = true;
            this.btmRefencePacking.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRefencePacking.AppearanceDisabled.Options.UseFont = true;
            this.btmRefencePacking.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRefencePacking.AppearanceHovered.Options.UseFont = true;
            this.btmRefencePacking.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRefencePacking.AppearancePressed.Options.UseFont = true;
            this.btmRefencePacking.Location = new System.Drawing.Point(15, 471);
            this.btmRefencePacking.Name = "btmRefencePacking";
            this.btmRefencePacking.Size = new System.Drawing.Size(162, 62);
            this.btmRefencePacking.TabIndex = 246;
            this.btmRefencePacking.Text = "包装数据刷新";
            this.btmRefencePacking.Click += new System.EventHandler(this.btmRefencePacking_Click);
            // 
            // btmRefenceTrayID
            // 
            this.btmRefenceTrayID.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRefenceTrayID.Appearance.Options.UseFont = true;
            this.btmRefenceTrayID.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRefenceTrayID.AppearanceDisabled.Options.UseFont = true;
            this.btmRefenceTrayID.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRefenceTrayID.AppearanceHovered.Options.UseFont = true;
            this.btmRefenceTrayID.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRefenceTrayID.AppearancePressed.Options.UseFont = true;
            this.btmRefenceTrayID.Location = new System.Drawing.Point(15, 317);
            this.btmRefenceTrayID.Name = "btmRefenceTrayID";
            this.btmRefenceTrayID.Size = new System.Drawing.Size(162, 62);
            this.btmRefenceTrayID.TabIndex = 242;
            this.btmRefenceTrayID.Text = "托盘数据刷新";
            this.btmRefenceTrayID.Click += new System.EventHandler(this.btmRefenceTrayID_Click);
            // 
            // btmReferenceResults
            // 
            this.btmReferenceResults.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmReferenceResults.Appearance.Options.UseFont = true;
            this.btmReferenceResults.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReferenceResults.AppearanceDisabled.Options.UseFont = true;
            this.btmReferenceResults.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReferenceResults.AppearanceHovered.Options.UseFont = true;
            this.btmReferenceResults.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReferenceResults.AppearancePressed.Options.UseFont = true;
            this.btmReferenceResults.Location = new System.Drawing.Point(15, 394);
            this.btmReferenceResults.Name = "btmReferenceResults";
            this.btmReferenceResults.Size = new System.Drawing.Size(162, 62);
            this.btmReferenceResults.TabIndex = 243;
            this.btmReferenceResults.Text = "结果数据刷新";
            this.btmReferenceResults.Click += new System.EventHandler(this.btmReferenceResults_Click);
            // 
            // tabPane2
            // 
            this.tabPane2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPane2.Appearance.Options.UseFont = true;
            this.tabPane2.AppearanceButton.Hovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane2.AppearanceButton.Hovered.Options.UseFont = true;
            this.tabPane2.AppearanceButton.Normal.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane2.AppearanceButton.Normal.Options.UseFont = true;
            this.tabPane2.AppearanceButton.Pressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane2.AppearanceButton.Pressed.Options.UseFont = true;
            this.tabPane2.Controls.Add(this.Tab_DB_Load);
            this.tabPane2.Controls.Add(this.Tab_DB_Flash);
            this.tabPane2.Controls.Add(this.Tab_DB_Laser);
            this.tabPane2.Controls.Add(this.Tab_DB_Robot);
            this.tabPane2.Controls.Add(this.Tab_DB_Tray);
            this.tabPane2.Controls.Add(this.Tab_DB_Result);
            this.tabPane2.Controls.Add(this.Tab_DB_Packing);
            this.tabPane2.Controls.Add(this.Tab_DB_ORDER);
            this.tabPane2.Location = new System.Drawing.Point(333, -2);
            this.tabPane2.Name = "tabPane2";
            this.tabPane2.PageProperties.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane2.PageProperties.AppearanceCaption.Options.UseFont = true;
            this.tabPane2.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.Tab_DB_Load,
            this.Tab_DB_Flash,
            this.Tab_DB_Laser,
            this.Tab_DB_Robot,
            this.Tab_DB_ORDER,
            this.Tab_DB_Tray,
            this.Tab_DB_Result,
            this.Tab_DB_Packing});
            this.tabPane2.RegularSize = new System.Drawing.Size(1375, 655);
            this.tabPane2.SelectedPage = this.Tab_DB_Load;
            this.tabPane2.Size = new System.Drawing.Size(1375, 655);
            this.tabPane2.TabIndex = 0;
            this.tabPane2.Text = "tabPane2";
            // 
            // Tab_DB_Load
            // 
            this.Tab_DB_Load.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Tab_DB_Load.Appearance.Options.UseFont = true;
            this.Tab_DB_Load.Caption = "上料邦盘";
            this.Tab_DB_Load.Controls.Add(this.groupControl15);
            this.Tab_DB_Load.Controls.Add(this.groupControl13);
            this.Tab_DB_Load.Name = "Tab_DB_Load";
            this.Tab_DB_Load.Properties.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Tab_DB_Load.Properties.AppearanceCaption.Options.UseFont = true;
            this.Tab_DB_Load.Size = new System.Drawing.Size(1357, 606);
            // 
            // groupControl15
            // 
            this.groupControl15.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl15.Appearance.Options.UseFont = true;
            this.groupControl15.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl15.AppearanceCaption.Options.UseFont = true;
            this.groupControl15.Location = new System.Drawing.Point(911, 3);
            this.groupControl15.Name = "groupControl15";
            this.groupControl15.Size = new System.Drawing.Size(359, 250);
            this.groupControl15.TabIndex = 242;
            this.groupControl15.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // groupControl13
            // 
            this.groupControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl13.Appearance.Options.UseFont = true;
            this.groupControl13.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl13.AppearanceCaption.Options.UseFont = true;
            this.groupControl13.Controls.Add(this.btm当前工单完成);
            this.groupControl13.Controls.Add(this.btm得到最后打码标志);
            this.groupControl13.Controls.Add(this.输入包装信息);
            this.groupControl13.Controls.Add(this.得到烧录TRYA工位结果);
            this.groupControl13.Controls.Add(this.得到烧录工位结果);
            this.groupControl13.Controls.Add(this.设置烧录工位结果);
            this.groupControl13.Controls.Add(this.得到托盘当前结果);
            this.groupControl13.Controls.Add(this.得到PCBID当前结果);
            this.groupControl13.Controls.Add(this.得到PCBID当前状态);
            this.groupControl13.Controls.Add(this.labelControl54);
            this.groupControl13.Controls.Add(this.得到托盘当前状态);
            this.groupControl13.Controls.Add(this.memoEdit4);
            this.groupControl13.Controls.Add(this.设置工位结果数据);
            this.groupControl13.Controls.Add(this.txtPCB板号);
            this.groupControl13.Controls.Add(this.chkStationResultStatus);
            this.groupControl13.Controls.Add(this.labelControl52);
            this.groupControl13.Controls.Add(this.cmbEmStation);
            this.groupControl13.Controls.Add(this.txtStatusDisplay);
            this.groupControl13.Controls.Add(this.手动解盘);
            this.groupControl13.Controls.Add(this.设置工位状态);
            this.groupControl13.Controls.Add(this.手动绑盘);
            this.groupControl13.Controls.Add(this.labelControl53);
            this.groupControl13.Controls.Add(this.自动生成PCBID);
            this.groupControl13.Controls.Add(this.txtPCB虚拟编号);
            this.groupControl13.Controls.Add(this.labelControl50);
            this.groupControl13.Controls.Add(this.labelControl51);
            this.groupControl13.Controls.Add(this.txt托盘编号);
            this.groupControl13.Location = new System.Drawing.Point(3, 3);
            this.groupControl13.Name = "groupControl13";
            this.groupControl13.Size = new System.Drawing.Size(902, 592);
            this.groupControl13.TabIndex = 98;
            this.groupControl13.Text = "全局变量/上料绑盘/解绑";
            // 
            // btm当前工单完成
            // 
            this.btm当前工单完成.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btm当前工单完成.Appearance.Options.UseFont = true;
            this.btm当前工单完成.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm当前工单完成.AppearanceDisabled.Options.UseFont = true;
            this.btm当前工单完成.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm当前工单完成.AppearanceHovered.Options.UseFont = true;
            this.btm当前工单完成.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm当前工单完成.AppearancePressed.Options.UseFont = true;
            this.btm当前工单完成.Location = new System.Drawing.Point(722, 525);
            this.btm当前工单完成.Name = "btm当前工单完成";
            this.btm当前工单完成.Size = new System.Drawing.Size(166, 62);
            this.btm当前工单完成.TabIndex = 252;
            this.btm当前工单完成.Text = "当前工单完成";
            this.btm当前工单完成.Click += new System.EventHandler(this.btm当前工单完成_Click);
            // 
            // btm得到最后打码标志
            // 
            this.btm得到最后打码标志.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btm得到最后打码标志.Appearance.Options.UseFont = true;
            this.btm得到最后打码标志.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm得到最后打码标志.AppearanceDisabled.Options.UseFont = true;
            this.btm得到最后打码标志.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm得到最后打码标志.AppearanceHovered.Options.UseFont = true;
            this.btm得到最后打码标志.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btm得到最后打码标志.AppearancePressed.Options.UseFont = true;
            this.btm得到最后打码标志.Location = new System.Drawing.Point(420, 527);
            this.btm得到最后打码标志.Name = "btm得到最后打码标志";
            this.btm得到最后打码标志.Size = new System.Drawing.Size(166, 62);
            this.btm得到最后打码标志.TabIndex = 251;
            this.btm得到最后打码标志.Text = "得到最后打码标志";
            this.btm得到最后打码标志.Click += new System.EventHandler(this.btm得到最后打码标志_Click);
            // 
            // 输入包装信息
            // 
            this.输入包装信息.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.输入包装信息.Appearance.Options.UseFont = true;
            this.输入包装信息.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.输入包装信息.AppearanceDisabled.Options.UseFont = true;
            this.输入包装信息.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.输入包装信息.AppearanceHovered.Options.UseFont = true;
            this.输入包装信息.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.输入包装信息.AppearancePressed.Options.UseFont = true;
            this.输入包装信息.Location = new System.Drawing.Point(223, 525);
            this.输入包装信息.Name = "输入包装信息";
            this.输入包装信息.Size = new System.Drawing.Size(166, 62);
            this.输入包装信息.TabIndex = 251;
            this.输入包装信息.Text = "输入包装信息";
            this.输入包装信息.Click += new System.EventHandler(this.输入包装信息_Click);
            // 
            // 得到烧录TRYA工位结果
            // 
            this.得到烧录TRYA工位结果.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.得到烧录TRYA工位结果.Appearance.Options.UseFont = true;
            this.得到烧录TRYA工位结果.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到烧录TRYA工位结果.AppearanceDisabled.Options.UseFont = true;
            this.得到烧录TRYA工位结果.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到烧录TRYA工位结果.AppearanceHovered.Options.UseFont = true;
            this.得到烧录TRYA工位结果.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到烧录TRYA工位结果.AppearancePressed.Options.UseFont = true;
            this.得到烧录TRYA工位结果.Location = new System.Drawing.Point(26, 525);
            this.得到烧录TRYA工位结果.Name = "得到烧录TRYA工位结果";
            this.得到烧录TRYA工位结果.Size = new System.Drawing.Size(166, 62);
            this.得到烧录TRYA工位结果.TabIndex = 250;
            this.得到烧录TRYA工位结果.Text = "得到烧录工位结果";
            this.得到烧录TRYA工位结果.Click += new System.EventHandler(this.得到烧录TRYA工位结果_Click);
            // 
            // 得到烧录工位结果
            // 
            this.得到烧录工位结果.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.得到烧录工位结果.Appearance.Options.UseFont = true;
            this.得到烧录工位结果.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到烧录工位结果.AppearanceDisabled.Options.UseFont = true;
            this.得到烧录工位结果.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到烧录工位结果.AppearanceHovered.Options.UseFont = true;
            this.得到烧录工位结果.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到烧录工位结果.AppearancePressed.Options.UseFont = true;
            this.得到烧录工位结果.Location = new System.Drawing.Point(223, 442);
            this.得到烧录工位结果.Name = "得到烧录工位结果";
            this.得到烧录工位结果.Size = new System.Drawing.Size(166, 62);
            this.得到烧录工位结果.TabIndex = 249;
            this.得到烧录工位结果.Text = "得到烧录PCBID工位结果";
            this.得到烧录工位结果.Click += new System.EventHandler(this.得到烧录工位结果_Click);
            // 
            // 设置烧录工位结果
            // 
            this.设置烧录工位结果.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.设置烧录工位结果.Appearance.Options.UseFont = true;
            this.设置烧录工位结果.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置烧录工位结果.AppearanceDisabled.Options.UseFont = true;
            this.设置烧录工位结果.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置烧录工位结果.AppearanceHovered.Options.UseFont = true;
            this.设置烧录工位结果.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置烧录工位结果.AppearancePressed.Options.UseFont = true;
            this.设置烧录工位结果.Location = new System.Drawing.Point(26, 442);
            this.设置烧录工位结果.Name = "设置烧录工位结果";
            this.设置烧录工位结果.Size = new System.Drawing.Size(166, 62);
            this.设置烧录工位结果.TabIndex = 248;
            this.设置烧录工位结果.Text = "设置烧录工位结果";
            this.设置烧录工位结果.Click += new System.EventHandler(this.设置烧录工位结果_Click);
            // 
            // 得到托盘当前结果
            // 
            this.得到托盘当前结果.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.得到托盘当前结果.Appearance.Options.UseFont = true;
            this.得到托盘当前结果.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到托盘当前结果.AppearanceDisabled.Options.UseFont = true;
            this.得到托盘当前结果.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到托盘当前结果.AppearanceHovered.Options.UseFont = true;
            this.得到托盘当前结果.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到托盘当前结果.AppearancePressed.Options.UseFont = true;
            this.得到托盘当前结果.Location = new System.Drawing.Point(227, 352);
            this.得到托盘当前结果.Name = "得到托盘当前结果";
            this.得到托盘当前结果.Size = new System.Drawing.Size(162, 62);
            this.得到托盘当前结果.TabIndex = 247;
            this.得到托盘当前结果.Text = "得到托盘当前结果";
            this.得到托盘当前结果.Click += new System.EventHandler(this.得到托盘当前结果_Click);
            // 
            // 得到PCBID当前结果
            // 
            this.得到PCBID当前结果.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.得到PCBID当前结果.Appearance.Options.UseFont = true;
            this.得到PCBID当前结果.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到PCBID当前结果.AppearanceDisabled.Options.UseFont = true;
            this.得到PCBID当前结果.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到PCBID当前结果.AppearanceHovered.Options.UseFont = true;
            this.得到PCBID当前结果.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到PCBID当前结果.AppearancePressed.Options.UseFont = true;
            this.得到PCBID当前结果.Location = new System.Drawing.Point(26, 352);
            this.得到PCBID当前结果.Name = "得到PCBID当前结果";
            this.得到PCBID当前结果.Size = new System.Drawing.Size(162, 62);
            this.得到PCBID当前结果.TabIndex = 246;
            this.得到PCBID当前结果.Text = "得到PCBID当前结果";
            this.得到PCBID当前结果.Click += new System.EventHandler(this.得到PCBID当前结果_Click);
            // 
            // 得到PCBID当前状态
            // 
            this.得到PCBID当前状态.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.得到PCBID当前状态.Appearance.Options.UseFont = true;
            this.得到PCBID当前状态.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到PCBID当前状态.AppearanceDisabled.Options.UseFont = true;
            this.得到PCBID当前状态.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到PCBID当前状态.AppearanceHovered.Options.UseFont = true;
            this.得到PCBID当前状态.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到PCBID当前状态.AppearancePressed.Options.UseFont = true;
            this.得到PCBID当前状态.Location = new System.Drawing.Point(227, 264);
            this.得到PCBID当前状态.Name = "得到PCBID当前状态";
            this.得到PCBID当前状态.Size = new System.Drawing.Size(162, 62);
            this.得到PCBID当前状态.TabIndex = 245;
            this.得到PCBID当前状态.Text = "得到PCBID当前状态";
            this.得到PCBID当前状态.Click += new System.EventHandler(this.得到PCBID当前状态_Click);
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl54.AppearanceDisabled.Options.UseFont = true;
            this.labelControl54.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl54.AppearanceHovered.Options.UseFont = true;
            this.labelControl54.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl54.AppearancePressed.Options.UseFont = true;
            this.labelControl54.Location = new System.Drawing.Point(424, 160);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(130, 17);
            this.labelControl54.TabIndex = 243;
            this.labelControl54.Text = "工艺/工站结果数据/信息";
            // 
            // 得到托盘当前状态
            // 
            this.得到托盘当前状态.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.得到托盘当前状态.Appearance.Options.UseFont = true;
            this.得到托盘当前状态.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到托盘当前状态.AppearanceDisabled.Options.UseFont = true;
            this.得到托盘当前状态.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到托盘当前状态.AppearanceHovered.Options.UseFont = true;
            this.得到托盘当前状态.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到托盘当前状态.AppearancePressed.Options.UseFont = true;
            this.得到托盘当前状态.Location = new System.Drawing.Point(26, 264);
            this.得到托盘当前状态.Name = "得到托盘当前状态";
            this.得到托盘当前状态.Size = new System.Drawing.Size(162, 62);
            this.得到托盘当前状态.TabIndex = 244;
            this.得到托盘当前状态.Text = "得到托盘当前状态";
            this.得到托盘当前状态.Click += new System.EventHandler(this.得到托盘当前状态_Click);
            // 
            // memoEdit4
            // 
            this.memoEdit4.Location = new System.Drawing.Point(420, 180);
            this.memoEdit4.Name = "memoEdit4";
            this.memoEdit4.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.memoEdit4.Properties.WordWrap = false;
            this.memoEdit4.Size = new System.Drawing.Size(296, 343);
            this.memoEdit4.TabIndex = 242;
            // 
            // 设置工位结果数据
            // 
            this.设置工位结果数据.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.设置工位结果数据.Appearance.Options.UseFont = true;
            this.设置工位结果数据.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置工位结果数据.AppearanceDisabled.Options.UseFont = true;
            this.设置工位结果数据.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置工位结果数据.AppearanceHovered.Options.UseFont = true;
            this.设置工位结果数据.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置工位结果数据.AppearancePressed.Options.UseFont = true;
            this.设置工位结果数据.Location = new System.Drawing.Point(722, 181);
            this.设置工位结果数据.Name = "设置工位结果数据";
            this.设置工位结果数据.Size = new System.Drawing.Size(166, 62);
            this.设置工位结果数据.TabIndex = 241;
            this.设置工位结果数据.Text = "设置工位结果";
            this.设置工位结果数据.Click += new System.EventHandler(this.设置工位结果数据_Click);
            // 
            // txtPCB板号
            // 
            this.txtPCB板号.EditValue = "P4BHN2342";
            this.txtPCB板号.Location = new System.Drawing.Point(91, 85);
            this.txtPCB板号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCB板号.Name = "txtPCB板号";
            this.txtPCB板号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCB板号.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPCB板号.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCB板号.Properties.Appearance.Options.UseFont = true;
            this.txtPCB板号.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCB板号.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB板号.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB板号.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCB板号.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCB板号.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCB板号.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB板号.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB板号.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCB板号.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCB板号.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCB板号.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB板号.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB板号.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCB板号.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCB板号.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCB板号.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB板号.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB板号.Size = new System.Drawing.Size(191, 24);
            this.txtPCB板号.TabIndex = 243;
            // 
            // chkStationResultStatus
            // 
            this.chkStationResultStatus.Location = new System.Drawing.Point(420, 89);
            this.chkStationResultStatus.Name = "chkStationResultStatus";
            this.chkStationResultStatus.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStationResultStatus.Properties.Appearance.Options.UseFont = true;
            this.chkStationResultStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkStationResultStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkStationResultStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkStationResultStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkStationResultStatus.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkStationResultStatus.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkStationResultStatus.Properties.Caption = "工艺/工站结果";
            this.chkStationResultStatus.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.chkStationResultStatus.Properties.PictureChecked = ((System.Drawing.Image)(resources.GetObject("chkStationResultStatus.Properties.PictureChecked")));
            this.chkStationResultStatus.Properties.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("chkStationResultStatus.Properties.PictureGrayed")));
            this.chkStationResultStatus.Properties.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("chkStationResultStatus.Properties.PictureUnchecked")));
            this.chkStationResultStatus.Size = new System.Drawing.Size(97, 21);
            this.chkStationResultStatus.TabIndex = 239;
            this.chkStationResultStatus.CheckedChanged += new System.EventHandler(this.chkStationResultStatus_CheckedChanged);
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl52.AppearanceDisabled.Options.UseFont = true;
            this.labelControl52.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl52.AppearanceHovered.Options.UseFont = true;
            this.labelControl52.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl52.AppearancePressed.Options.UseFont = true;
            this.labelControl52.Location = new System.Drawing.Point(38, 88);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(47, 17);
            this.labelControl52.TabIndex = 242;
            this.labelControl52.Text = "PCB板号";
            // 
            // cmbEmStation
            // 
            this.cmbEmStation.Location = new System.Drawing.Point(420, 56);
            this.cmbEmStation.Name = "cmbEmStation";
            this.cmbEmStation.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmStation.Properties.Appearance.Options.UseFont = true;
            this.cmbEmStation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbEmStation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cmbEmStation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbEmStation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbEmStation.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbEmStation.Properties.AppearanceFocused.Options.UseFont = true;
            this.cmbEmStation.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbEmStation.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cmbEmStation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbEmStation.Properties.Items.AddRange(new object[] {
            "上料工站 = 0",
            "烧录工站 = 1",
            "打码工站 = 2",
            "切割工站 = 3",
            "顶部测试工站 = 4",
            "底部测试工站 = 5",
            "包装工站 = 6"});
            this.cmbEmStation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbEmStation.Size = new System.Drawing.Size(296, 24);
            this.cmbEmStation.TabIndex = 99;
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.EditValue = "Failed";
            this.txtStatusDisplay.Location = new System.Drawing.Point(523, 88);
            this.txtStatusDisplay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtStatusDisplay.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtStatusDisplay.Properties.Appearance.Options.UseBackColor = true;
            this.txtStatusDisplay.Properties.Appearance.Options.UseFont = true;
            this.txtStatusDisplay.Properties.Appearance.Options.UseTextOptions = true;
            this.txtStatusDisplay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtStatusDisplay.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtStatusDisplay.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatusDisplay.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtStatusDisplay.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtStatusDisplay.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtStatusDisplay.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtStatusDisplay.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatusDisplay.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtStatusDisplay.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtStatusDisplay.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtStatusDisplay.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtStatusDisplay.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatusDisplay.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtStatusDisplay.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtStatusDisplay.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtStatusDisplay.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtStatusDisplay.Size = new System.Drawing.Size(193, 24);
            this.txtStatusDisplay.TabIndex = 240;
            // 
            // 手动解盘
            // 
            this.手动解盘.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.手动解盘.Appearance.Options.UseFont = true;
            this.手动解盘.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动解盘.AppearanceDisabled.Options.UseFont = true;
            this.手动解盘.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动解盘.AppearanceHovered.Options.UseFont = true;
            this.手动解盘.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动解盘.AppearancePressed.Options.UseFont = true;
            this.手动解盘.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("手动解盘.ImageOptions.Image")));
            this.手动解盘.Location = new System.Drawing.Point(227, 181);
            this.手动解盘.Name = "手动解盘";
            this.手动解盘.Size = new System.Drawing.Size(162, 62);
            this.手动解盘.TabIndex = 241;
            this.手动解盘.Text = "手动解盘";
            this.手动解盘.Click += new System.EventHandler(this.手动解盘_Click);
            // 
            // 设置工位状态
            // 
            this.设置工位状态.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.设置工位状态.Appearance.Options.UseFont = true;
            this.设置工位状态.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置工位状态.AppearanceDisabled.Options.UseFont = true;
            this.设置工位状态.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置工位状态.AppearanceHovered.Options.UseFont = true;
            this.设置工位状态.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设置工位状态.AppearancePressed.Options.UseFont = true;
            this.设置工位状态.Location = new System.Drawing.Point(722, 55);
            this.设置工位状态.Name = "设置工位状态";
            this.设置工位状态.Size = new System.Drawing.Size(162, 62);
            this.设置工位状态.TabIndex = 100;
            this.设置工位状态.Text = "设置工位状态";
            this.设置工位状态.Click += new System.EventHandler(this.设置工位结果_Click);
            // 
            // 手动绑盘
            // 
            this.手动绑盘.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.手动绑盘.Appearance.Options.UseFont = true;
            this.手动绑盘.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动绑盘.AppearanceDisabled.Options.UseFont = true;
            this.手动绑盘.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动绑盘.AppearanceHovered.Options.UseFont = true;
            this.手动绑盘.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.手动绑盘.AppearancePressed.Options.UseFont = true;
            this.手动绑盘.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("手动绑盘.ImageOptions.Image")));
            this.手动绑盘.Location = new System.Drawing.Point(26, 181);
            this.手动绑盘.Name = "手动绑盘";
            this.手动绑盘.Size = new System.Drawing.Size(162, 62);
            this.手动绑盘.TabIndex = 0;
            this.手动绑盘.Text = "手动绑盘";
            this.手动绑盘.Click += new System.EventHandler(this.手动绑盘_Click);
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl53.AppearanceDisabled.Options.UseFont = true;
            this.labelControl53.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl53.AppearanceHovered.Options.UseFont = true;
            this.labelControl53.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl53.AppearancePressed.Options.UseFont = true;
            this.labelControl53.Location = new System.Drawing.Point(420, 34);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(77, 17);
            this.labelControl53.TabIndex = 238;
            this.labelControl53.Text = "工艺/工站选定";
            // 
            // 自动生成PCBID
            // 
            this.自动生成PCBID.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.自动生成PCBID.Appearance.Options.UseFont = true;
            this.自动生成PCBID.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.自动生成PCBID.AppearanceDisabled.Options.UseFont = true;
            this.自动生成PCBID.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.自动生成PCBID.AppearanceHovered.Options.UseFont = true;
            this.自动生成PCBID.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.自动生成PCBID.AppearancePressed.Options.UseFont = true;
            this.自动生成PCBID.Location = new System.Drawing.Point(288, 112);
            this.自动生成PCBID.Name = "自动生成PCBID";
            this.自动生成PCBID.Size = new System.Drawing.Size(101, 39);
            this.自动生成PCBID.TabIndex = 240;
            this.自动生成PCBID.Text = "自动生成PCBID";
            this.自动生成PCBID.Click += new System.EventHandler(this.自动生成PCBID_Click);
            // 
            // txtPCB虚拟编号
            // 
            this.txtPCB虚拟编号.EditValue = "";
            this.txtPCB虚拟编号.Location = new System.Drawing.Point(91, 121);
            this.txtPCB虚拟编号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCB虚拟编号.Name = "txtPCB虚拟编号";
            this.txtPCB虚拟编号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCB虚拟编号.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPCB虚拟编号.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCB虚拟编号.Properties.Appearance.Options.UseFont = true;
            this.txtPCB虚拟编号.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCB虚拟编号.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB虚拟编号.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB虚拟编号.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCB虚拟编号.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCB虚拟编号.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCB虚拟编号.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB虚拟编号.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB虚拟编号.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCB虚拟编号.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCB虚拟编号.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCB虚拟编号.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB虚拟编号.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB虚拟编号.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCB虚拟编号.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCB虚拟编号.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCB虚拟编号.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCB虚拟编号.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCB虚拟编号.Size = new System.Drawing.Size(191, 24);
            this.txtPCB虚拟编号.TabIndex = 239;
            this.txtPCB虚拟编号.EditValueChanged += new System.EventHandler(this.txtPCB虚拟编号_EditValueChanged);
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl50.AppearanceDisabled.Options.UseFont = true;
            this.labelControl50.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl50.AppearanceHovered.Options.UseFont = true;
            this.labelControl50.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl50.AppearancePressed.Options.UseFont = true;
            this.labelControl50.Location = new System.Drawing.Point(14, 124);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(71, 17);
            this.labelControl50.TabIndex = 238;
            this.labelControl50.Text = "PCB虚拟编号";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl51.AppearanceDisabled.Options.UseFont = true;
            this.labelControl51.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl51.AppearanceHovered.Options.UseFont = true;
            this.labelControl51.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl51.AppearancePressed.Options.UseFont = true;
            this.labelControl51.Location = new System.Drawing.Point(37, 49);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(48, 17);
            this.labelControl51.TabIndex = 237;
            this.labelControl51.Text = "托盘编号";
            // 
            // txt托盘编号
            // 
            this.txt托盘编号.EditValue = "HPTRAYID01";
            this.txt托盘编号.Location = new System.Drawing.Point(91, 44);
            this.txt托盘编号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt托盘编号.Name = "txt托盘编号";
            this.txt托盘编号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt托盘编号.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt托盘编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt托盘编号.Properties.Appearance.Options.UseFont = true;
            this.txt托盘编号.Properties.Appearance.Options.UseTextOptions = true;
            this.txt托盘编号.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt托盘编号.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt托盘编号.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt托盘编号.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txt托盘编号.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txt托盘编号.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt托盘编号.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt托盘编号.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt托盘编号.Properties.AppearanceFocused.Options.UseFont = true;
            this.txt托盘编号.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txt托盘编号.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt托盘编号.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt托盘编号.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt托盘编号.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txt托盘编号.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txt托盘编号.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt托盘编号.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt托盘编号.Size = new System.Drawing.Size(191, 24);
            this.txt托盘编号.TabIndex = 236;
            // 
            // Tab_DB_Flash
            // 
            this.Tab_DB_Flash.Caption = "烧录";
            this.Tab_DB_Flash.Name = "Tab_DB_Flash";
            this.Tab_DB_Flash.Size = new System.Drawing.Size(1375, 655);
            // 
            // Tab_DB_Laser
            // 
            this.Tab_DB_Laser.Caption = "打码切割";
            this.Tab_DB_Laser.Name = "Tab_DB_Laser";
            this.Tab_DB_Laser.Size = new System.Drawing.Size(1375, 655);
            // 
            // Tab_DB_Robot
            // 
            this.Tab_DB_Robot.Caption = "下料组盘测试";
            this.Tab_DB_Robot.Name = "Tab_DB_Robot";
            this.Tab_DB_Robot.Size = new System.Drawing.Size(1375, 655);
            // 
            // Tab_DB_Tray
            // 
            this.Tab_DB_Tray.Caption = "托盘信息";
            this.Tab_DB_Tray.Controls.Add(this.dataGridView1);
            this.Tab_DB_Tray.Name = "Tab_DB_Tray";
            this.Tab_DB_Tray.Size = new System.Drawing.Size(1357, 606);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1279, 605);
            this.dataGridView1.TabIndex = 0;
            // 
            // Tab_DB_Result
            // 
            this.Tab_DB_Result.Caption = "数据结果查询";
            this.Tab_DB_Result.Controls.Add(this.dataGridView2);
            this.Tab_DB_Result.Name = "Tab_DB_Result";
            this.Tab_DB_Result.Size = new System.Drawing.Size(1357, 606);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.Size = new System.Drawing.Size(1279, 605);
            this.dataGridView2.TabIndex = 1;
            // 
            // Tab_DB_Packing
            // 
            this.Tab_DB_Packing.Caption = "包装结果查询";
            this.Tab_DB_Packing.Controls.Add(this.dataGridView3);
            this.Tab_DB_Packing.Name = "Tab_DB_Packing";
            this.Tab_DB_Packing.Size = new System.Drawing.Size(1357, 606);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowTemplate.Height = 23;
            this.dataGridView3.Size = new System.Drawing.Size(1279, 605);
            this.dataGridView3.TabIndex = 2;
            // 
            // Tab_DB_ORDER
            // 
            this.Tab_DB_ORDER.Caption = "工单信息";
            this.Tab_DB_ORDER.Controls.Add(this.dataGridView4);
            this.Tab_DB_ORDER.Name = "Tab_DB_ORDER";
            this.Tab_DB_ORDER.Size = new System.Drawing.Size(1357, 606);
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(0, 0);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowTemplate.Height = 23;
            this.dataGridView4.Size = new System.Drawing.Size(1279, 605);
            this.dataGridView4.TabIndex = 1;
            // 
            // NP_工艺参数
            // 
            this.NP_工艺参数.Caption = " 工艺参数";
            this.NP_工艺参数.Controls.Add(this.保存时写入所有信息);
            this.NP_工艺参数.Controls.Add(this.tabPane1);
            this.NP_工艺参数.Controls.Add(this.simpleButton2);
            this.NP_工艺参数.Controls.Add(this.btmSaveInvokeCfg);
            this.NP_工艺参数.Controls.Add(this.propertyGrid1);
            this.NP_工艺参数.Image = ((System.Drawing.Image)(resources.GetObject("NP_工艺参数.Image")));
            this.NP_工艺参数.Name = "NP_工艺参数";
            this.NP_工艺参数.Size = new System.Drawing.Size(1706, 646);
            // 
            // 保存时写入所有信息
            // 
            this.保存时写入所有信息.EditValue = true;
            this.保存时写入所有信息.Location = new System.Drawing.Point(342, 575);
            this.保存时写入所有信息.Name = "保存时写入所有信息";
            this.保存时写入所有信息.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.保存时写入所有信息.Properties.Appearance.Options.UseFont = true;
            this.保存时写入所有信息.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.保存时写入所有信息.Properties.AppearanceDisabled.Options.UseFont = true;
            this.保存时写入所有信息.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.保存时写入所有信息.Properties.AppearanceFocused.Options.UseFont = true;
            this.保存时写入所有信息.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.保存时写入所有信息.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.保存时写入所有信息.Properties.Caption = "保存时写入所有信息";
            this.保存时写入所有信息.Size = new System.Drawing.Size(180, 21);
            this.保存时写入所有信息.TabIndex = 183;
            // 
            // tabPane1
            // 
            this.tabPane1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPane1.Appearance.Options.UseFont = true;
            this.tabPane1.AppearanceButton.Hovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.AppearanceButton.Hovered.Options.UseFont = true;
            this.tabPane1.AppearanceButton.Normal.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.AppearanceButton.Normal.Options.UseFont = true;
            this.tabPane1.AppearanceButton.Pressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.AppearanceButton.Pressed.Options.UseFont = true;
            this.tabPane1.Controls.Add(this.tabNG_上料调度);
            this.tabPane1.Controls.Add(this.tabNG_烧录);
            this.tabPane1.Controls.Add(this.tabNG_打码切割);
            this.tabPane1.Controls.Add(this.tabNG_下料组盘);
            this.tabPane1.Controls.Add(this.tabNG_打标包装);
            this.tabPane1.Location = new System.Drawing.Point(12, 15);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.PageProperties.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.PageProperties.AppearanceCaption.Options.UseFont = true;
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNG_上料调度,
            this.tabNG_烧录,
            this.tabNG_打码切割,
            this.tabNG_下料组盘,
            this.tabNG_打标包装});
            this.tabPane1.RegularSize = new System.Drawing.Size(1002, 542);
            this.tabPane1.SelectedPage = this.tabNG_上料调度;
            this.tabPane1.Size = new System.Drawing.Size(1002, 542);
            this.tabPane1.TabIndex = 0;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabNG_上料调度
            // 
            this.tabNG_上料调度.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabNG_上料调度.Appearance.Options.UseFont = true;
            this.tabNG_上料调度.Caption = "上料调度";
            this.tabNG_上料调度.Controls.Add(this.groupControl12);
            this.tabNG_上料调度.Controls.Add(this.groupControl8);
            this.tabNG_上料调度.Controls.Add(this.groupControl11);
            this.tabNG_上料调度.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_上料调度.Image")));
            this.tabNG_上料调度.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_上料调度.Name = "tabNG_上料调度";
            this.tabNG_上料调度.Properties.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabNG_上料调度.Properties.AppearanceCaption.Options.UseFont = true;
            this.tabNG_上料调度.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_上料调度.Size = new System.Drawing.Size(984, 478);
            // 
            // groupControl12
            // 
            this.groupControl12.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl12.Appearance.Options.UseFont = true;
            this.groupControl12.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl12.AppearanceCaption.Options.UseFont = true;
            this.groupControl12.Controls.Add(this.labelControl65);
            this.groupControl12.Controls.Add(this.labelControl64);
            this.groupControl12.Controls.Add(this.chkRealUpdateWorkSheet);
            this.groupControl12.Controls.Add(this.spePCBDstCol);
            this.groupControl12.Controls.Add(this.labelControl60);
            this.groupControl12.Controls.Add(this.spePCBDstRow);
            this.groupControl12.Controls.Add(this.labelControl59);
            this.groupControl12.Controls.Add(this.spePCBWidth);
            this.groupControl12.Controls.Add(this.labelControl45);
            this.groupControl12.Controls.Add(this.spePCBLength);
            this.groupControl12.Controls.Add(this.labelControl44);
            this.groupControl12.Controls.Add(this.speDstMaduo);
            this.groupControl12.Controls.Add(this.labelControl43);
            this.groupControl12.Controls.Add(this.speTrayWidth);
            this.groupControl12.Controls.Add(this.labelControl38);
            this.groupControl12.Controls.Add(this.speTrayLength);
            this.groupControl12.Controls.Add(this.labelControl37);
            this.groupControl12.Controls.Add(this.btmWriteUnload);
            this.groupControl12.Controls.Add(this.label6);
            this.groupControl12.Controls.Add(this.UnloadCfg_TotalProduct);
            this.groupControl12.Controls.Add(this.label5);
            this.groupControl12.Controls.Add(this.UnloadCfg_Remaind);
            this.groupControl12.Controls.Add(this.textEdit11);
            this.groupControl12.Controls.Add(this.textEdit12);
            this.groupControl12.Controls.Add(this.label4);
            this.groupControl12.Controls.Add(this.textEdit10);
            this.groupControl12.Controls.Add(this.UnloadCfg_Total);
            this.groupControl12.Controls.Add(this.labelControl46);
            this.groupControl12.Controls.Add(this.labelControl49);
            this.groupControl12.Controls.Add(this.txtPCBPNNumber);
            this.groupControl12.Controls.Add(this.UnloadCfg_Col);
            this.groupControl12.Controls.Add(this.labelControl47);
            this.groupControl12.Controls.Add(this.UnloadCfg_Row);
            this.groupControl12.Controls.Add(this.label3);
            this.groupControl12.Controls.Add(this.labelControl42);
            this.groupControl12.Controls.Add(this.UnloadCfg_CountTray);
            this.groupControl12.Controls.Add(this.labelControl39);
            this.groupControl12.Controls.Add(this.labelControl41);
            this.groupControl12.Controls.Add(this.labelControl36);
            this.groupControl12.Controls.Add(this.labelControl40);
            this.groupControl12.Controls.Add(this.labelControl35);
            this.groupControl12.Controls.Add(this.labelControl48);
            this.groupControl12.Controls.Add(this.txtWorkOrder2);
            this.groupControl12.Location = new System.Drawing.Point(3, 3);
            this.groupControl12.Name = "groupControl12";
            this.groupControl12.Size = new System.Drawing.Size(516, 468);
            this.groupControl12.TabIndex = 181;
            this.groupControl12.Text = "当前工单主要信息";
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.Appearance.Options.UseFont = true;
            this.labelControl65.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.AppearanceDisabled.Options.UseFont = true;
            this.labelControl65.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.AppearanceHovered.Options.UseFont = true;
            this.labelControl65.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.AppearancePressed.Options.UseFont = true;
            this.labelControl65.Location = new System.Drawing.Point(183, 282);
            this.labelControl65.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(12, 17);
            this.labelControl65.TabIndex = 267;
            this.labelControl65.Text = "个";
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl64.Appearance.Options.UseFont = true;
            this.labelControl64.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl64.AppearanceDisabled.Options.UseFont = true;
            this.labelControl64.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl64.AppearanceHovered.Options.UseFont = true;
            this.labelControl64.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl64.AppearancePressed.Options.UseFont = true;
            this.labelControl64.Location = new System.Drawing.Point(183, 248);
            this.labelControl64.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(12, 17);
            this.labelControl64.TabIndex = 266;
            this.labelControl64.Text = "盘";
            // 
            // chkRealUpdateWorkSheet
            // 
            this.chkRealUpdateWorkSheet.Location = new System.Drawing.Point(224, 435);
            this.chkRealUpdateWorkSheet.Name = "chkRealUpdateWorkSheet";
            this.chkRealUpdateWorkSheet.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRealUpdateWorkSheet.Properties.Appearance.Options.UseFont = true;
            this.chkRealUpdateWorkSheet.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkRealUpdateWorkSheet.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkRealUpdateWorkSheet.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkRealUpdateWorkSheet.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkRealUpdateWorkSheet.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkRealUpdateWorkSheet.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkRealUpdateWorkSheet.Properties.Caption = "实时更新工艺数据(防止重启下位机及数据修改)";
            this.chkRealUpdateWorkSheet.Size = new System.Drawing.Size(274, 21);
            this.chkRealUpdateWorkSheet.TabIndex = 265;
            this.chkRealUpdateWorkSheet.CheckedChanged += new System.EventHandler(this.chkRealUpdateWorkSheet_CheckedChanged);
            // 
            // spePCBDstCol
            // 
            this.spePCBDstCol.EditValue = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.spePCBDstCol.Location = new System.Drawing.Point(418, 225);
            this.spePCBDstCol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spePCBDstCol.Name = "spePCBDstCol";
            this.spePCBDstCol.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstCol.Properties.Appearance.Options.UseFont = true;
            this.spePCBDstCol.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstCol.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spePCBDstCol.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstCol.Properties.AppearanceFocused.Options.UseFont = true;
            this.spePCBDstCol.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstCol.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spePCBDstCol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spePCBDstCol.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spePCBDstCol.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spePCBDstCol.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.spePCBDstCol.Size = new System.Drawing.Size(80, 24);
            this.spePCBDstCol.TabIndex = 263;
            // 
            // labelControl60
            // 
            this.labelControl60.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl60.Appearance.Options.UseFont = true;
            this.labelControl60.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl60.AppearanceDisabled.Options.UseFont = true;
            this.labelControl60.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl60.AppearanceHovered.Options.UseFont = true;
            this.labelControl60.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl60.AppearancePressed.Options.UseFont = true;
            this.labelControl60.Location = new System.Drawing.Point(318, 233);
            this.labelControl60.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(95, 17);
            this.labelControl60.TabIndex = 264;
            this.labelControl60.Text = "PCB 拼板间距(例)";
            // 
            // spePCBDstRow
            // 
            this.spePCBDstRow.EditValue = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.spePCBDstRow.Location = new System.Drawing.Point(418, 192);
            this.spePCBDstRow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spePCBDstRow.Name = "spePCBDstRow";
            this.spePCBDstRow.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstRow.Properties.Appearance.Options.UseFont = true;
            this.spePCBDstRow.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstRow.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spePCBDstRow.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstRow.Properties.AppearanceFocused.Options.UseFont = true;
            this.spePCBDstRow.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBDstRow.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spePCBDstRow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spePCBDstRow.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spePCBDstRow.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spePCBDstRow.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.spePCBDstRow.Size = new System.Drawing.Size(80, 24);
            this.spePCBDstRow.TabIndex = 261;
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl59.Appearance.Options.UseFont = true;
            this.labelControl59.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl59.AppearanceDisabled.Options.UseFont = true;
            this.labelControl59.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl59.AppearanceHovered.Options.UseFont = true;
            this.labelControl59.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl59.AppearancePressed.Options.UseFont = true;
            this.labelControl59.Location = new System.Drawing.Point(322, 200);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(91, 17);
            this.labelControl59.TabIndex = 262;
            this.labelControl59.Text = "PCB拼板间距(行)";
            // 
            // spePCBWidth
            // 
            this.spePCBWidth.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spePCBWidth.Location = new System.Drawing.Point(418, 159);
            this.spePCBWidth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spePCBWidth.Name = "spePCBWidth";
            this.spePCBWidth.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBWidth.Properties.Appearance.Options.UseFont = true;
            this.spePCBWidth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBWidth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spePCBWidth.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBWidth.Properties.AppearanceFocused.Options.UseFont = true;
            this.spePCBWidth.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBWidth.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spePCBWidth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spePCBWidth.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spePCBWidth.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spePCBWidth.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.spePCBWidth.Size = new System.Drawing.Size(80, 24);
            this.spePCBWidth.TabIndex = 259;
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl45.AppearanceDisabled.Options.UseFont = true;
            this.labelControl45.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl45.AppearanceHovered.Options.UseFont = true;
            this.labelControl45.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl45.AppearancePressed.Options.UseFont = true;
            this.labelControl45.Location = new System.Drawing.Point(336, 167);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(77, 17);
            this.labelControl45.TabIndex = 260;
            this.labelControl45.Text = "PCB宽度(mm)";
            // 
            // spePCBLength
            // 
            this.spePCBLength.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spePCBLength.Location = new System.Drawing.Point(418, 126);
            this.spePCBLength.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spePCBLength.Name = "spePCBLength";
            this.spePCBLength.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBLength.Properties.Appearance.Options.UseFont = true;
            this.spePCBLength.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBLength.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spePCBLength.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBLength.Properties.AppearanceFocused.Options.UseFont = true;
            this.spePCBLength.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spePCBLength.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spePCBLength.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spePCBLength.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spePCBLength.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spePCBLength.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.spePCBLength.Size = new System.Drawing.Size(80, 24);
            this.spePCBLength.TabIndex = 257;
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl44.AppearanceDisabled.Options.UseFont = true;
            this.labelControl44.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl44.AppearanceHovered.Options.UseFont = true;
            this.labelControl44.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl44.AppearancePressed.Options.UseFont = true;
            this.labelControl44.Location = new System.Drawing.Point(336, 134);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(77, 17);
            this.labelControl44.TabIndex = 258;
            this.labelControl44.Text = "PCB长度(mm)";
            // 
            // speDstMaduo
            // 
            this.speDstMaduo.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.speDstMaduo.Location = new System.Drawing.Point(418, 93);
            this.speDstMaduo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speDstMaduo.Name = "speDstMaduo";
            this.speDstMaduo.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speDstMaduo.Properties.Appearance.Options.UseFont = true;
            this.speDstMaduo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speDstMaduo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speDstMaduo.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speDstMaduo.Properties.AppearanceFocused.Options.UseFont = true;
            this.speDstMaduo.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speDstMaduo.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speDstMaduo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speDstMaduo.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.speDstMaduo.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speDstMaduo.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.speDstMaduo.Size = new System.Drawing.Size(80, 24);
            this.speDstMaduo.TabIndex = 255;
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.AppearanceDisabled.Options.UseFont = true;
            this.labelControl43.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.AppearanceHovered.Options.UseFont = true;
            this.labelControl43.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.AppearancePressed.Options.UseFont = true;
            this.labelControl43.Location = new System.Drawing.Point(311, 101);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(102, 17);
            this.labelControl43.TabIndex = 256;
            this.labelControl43.Text = "料盘码垛间距(mm)";
            // 
            // speTrayWidth
            // 
            this.speTrayWidth.EditValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.speTrayWidth.Location = new System.Drawing.Point(418, 60);
            this.speTrayWidth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speTrayWidth.Name = "speTrayWidth";
            this.speTrayWidth.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayWidth.Properties.Appearance.Options.UseFont = true;
            this.speTrayWidth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayWidth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speTrayWidth.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayWidth.Properties.AppearanceFocused.Options.UseFont = true;
            this.speTrayWidth.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayWidth.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speTrayWidth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speTrayWidth.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.speTrayWidth.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speTrayWidth.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.speTrayWidth.Size = new System.Drawing.Size(80, 24);
            this.speTrayWidth.TabIndex = 253;
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl38.AppearanceDisabled.Options.UseFont = true;
            this.labelControl38.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl38.AppearanceHovered.Options.UseFont = true;
            this.labelControl38.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl38.AppearancePressed.Options.UseFont = true;
            this.labelControl38.Location = new System.Drawing.Point(335, 68);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(78, 17);
            this.labelControl38.TabIndex = 254;
            this.labelControl38.Text = "料盘宽度(mm)";
            // 
            // speTrayLength
            // 
            this.speTrayLength.EditValue = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.speTrayLength.Location = new System.Drawing.Point(418, 27);
            this.speTrayLength.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speTrayLength.Name = "speTrayLength";
            this.speTrayLength.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayLength.Properties.Appearance.Options.UseFont = true;
            this.speTrayLength.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayLength.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speTrayLength.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayLength.Properties.AppearanceFocused.Options.UseFont = true;
            this.speTrayLength.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speTrayLength.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speTrayLength.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speTrayLength.Properties.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.speTrayLength.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speTrayLength.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.speTrayLength.Size = new System.Drawing.Size(80, 24);
            this.speTrayLength.TabIndex = 251;
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl37.AppearanceDisabled.Options.UseFont = true;
            this.labelControl37.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl37.AppearanceHovered.Options.UseFont = true;
            this.labelControl37.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl37.AppearancePressed.Options.UseFont = true;
            this.labelControl37.Location = new System.Drawing.Point(335, 35);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(78, 17);
            this.labelControl37.TabIndex = 252;
            this.labelControl37.Text = "料盘长度(mm)";
            // 
            // btmWriteUnload
            // 
            this.btmWriteUnload.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmWriteUnload.Appearance.Options.UseFont = true;
            this.btmWriteUnload.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmWriteUnload.AppearanceDisabled.Options.UseFont = true;
            this.btmWriteUnload.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmWriteUnload.AppearanceHovered.Options.UseFont = true;
            this.btmWriteUnload.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmWriteUnload.AppearancePressed.Options.UseFont = true;
            this.btmWriteUnload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmWriteUnload.ImageOptions.Image")));
            this.btmWriteUnload.Location = new System.Drawing.Point(17, 429);
            this.btmWriteUnload.Name = "btmWriteUnload";
            this.btmWriteUnload.Size = new System.Drawing.Size(116, 32);
            this.btmWriteUnload.TabIndex = 86;
            this.btmWriteUnload.Text = "写入(short)";
            this.btmWriteUnload.Click += new System.EventHandler(this.btmWriteUnload_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 90;
            this.label6.Text = "生产总量";
            // 
            // UnloadCfg_TotalProduct
            // 
            this.UnloadCfg_TotalProduct.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_TotalProduct.Location = new System.Drawing.Point(97, 100);
            this.UnloadCfg_TotalProduct.Name = "UnloadCfg_TotalProduct";
            this.UnloadCfg_TotalProduct.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_TotalProduct.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_TotalProduct.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_TotalProduct.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_TotalProduct.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_TotalProduct.Properties.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.UnloadCfg_TotalProduct.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_TotalProduct.Size = new System.Drawing.Size(113, 24);
            this.UnloadCfg_TotalProduct.TabIndex = 89;
            this.UnloadCfg_TotalProduct.EditValueChanged += new System.EventHandler(this.UnloadCfg_TotalProduct_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 88;
            this.label5.Text = "尾盘余量";
            // 
            // UnloadCfg_Remaind
            // 
            this.UnloadCfg_Remaind.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Remaind.Location = new System.Drawing.Point(97, 275);
            this.UnloadCfg_Remaind.Name = "UnloadCfg_Remaind";
            this.UnloadCfg_Remaind.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_Remaind.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Remaind.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Remaind.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Remaind.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Remaind.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.UnloadCfg_Remaind.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_Remaind.Properties.ReadOnly = true;
            this.UnloadCfg_Remaind.Size = new System.Drawing.Size(80, 24);
            this.UnloadCfg_Remaind.TabIndex = 87;
            this.UnloadCfg_Remaind.EditValueChanged += new System.EventHandler(this.UnloadCfg_Remaind_EditValueChanged);
            // 
            // textEdit11
            // 
            this.textEdit11.EditValue = "";
            this.textEdit11.Location = new System.Drawing.Point(97, 380);
            this.textEdit11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit11.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit11.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit11.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit11.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit11.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEdit11.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit11.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit11.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit11.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEdit11.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit11.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit11.Size = new System.Drawing.Size(80, 24);
            this.textEdit11.TabIndex = 250;
            // 
            // textEdit12
            // 
            this.textEdit12.EditValue = "";
            this.textEdit12.Location = new System.Drawing.Point(97, 345);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit12.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit12.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit12.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit12.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit12.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEdit12.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit12.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit12.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit12.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEdit12.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit12.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit12.Size = new System.Drawing.Size(80, 24);
            this.textEdit12.TabIndex = 249;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 85;
            this.label4.Text = "包装总量";
            // 
            // textEdit10
            // 
            this.textEdit10.EditValue = "";
            this.textEdit10.Location = new System.Drawing.Point(97, 310);
            this.textEdit10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit10.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit10.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit10.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit10.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit10.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEdit10.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit10.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEdit10.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit10.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit10.Size = new System.Drawing.Size(80, 24);
            this.textEdit10.TabIndex = 248;
            // 
            // UnloadCfg_Total
            // 
            this.UnloadCfg_Total.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Total.Location = new System.Drawing.Point(97, 135);
            this.UnloadCfg_Total.Name = "UnloadCfg_Total";
            this.UnloadCfg_Total.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_Total.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Total.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Total.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Total.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Total.Properties.MaxValue = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.UnloadCfg_Total.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_Total.Properties.ReadOnly = true;
            this.UnloadCfg_Total.Size = new System.Drawing.Size(113, 24);
            this.UnloadCfg_Total.TabIndex = 84;
            this.UnloadCfg_Total.EditValueChanged += new System.EventHandler(this.UnloadCfg_Total_EditValueChanged);
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.AppearanceDisabled.Options.UseFont = true;
            this.labelControl46.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.AppearanceHovered.Options.UseFont = true;
            this.labelControl46.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.AppearancePressed.Options.UseFont = true;
            this.labelControl46.Location = new System.Drawing.Point(183, 321);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(12, 17);
            this.labelControl46.TabIndex = 244;
            this.labelControl46.Text = "盘";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.AppearanceDisabled.Options.UseFont = true;
            this.labelControl49.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.AppearanceHovered.Options.UseFont = true;
            this.labelControl49.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.AppearancePressed.Options.UseFont = true;
            this.labelControl49.Location = new System.Drawing.Point(183, 175);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(12, 17);
            this.labelControl49.TabIndex = 246;
            this.labelControl49.Text = "个";
            // 
            // txtPCBPNNumber
            // 
            this.txtPCBPNNumber.EditValue = "";
            this.txtPCBPNNumber.Location = new System.Drawing.Point(97, 65);
            this.txtPCBPNNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBPNNumber.Name = "txtPCBPNNumber";
            this.txtPCBPNNumber.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBPNNumber.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPCBPNNumber.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBPNNumber.Properties.Appearance.Options.UseFont = true;
            this.txtPCBPNNumber.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBPNNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBPNNumber.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBPNNumber.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBPNNumber.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBPNNumber.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBPNNumber.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBPNNumber.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBPNNumber.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBPNNumber.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBPNNumber.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBPNNumber.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBPNNumber.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBPNNumber.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBPNNumber.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBPNNumber.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBPNNumber.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBPNNumber.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBPNNumber.Size = new System.Drawing.Size(191, 24);
            this.txtPCBPNNumber.TabIndex = 235;
            // 
            // UnloadCfg_Col
            // 
            this.UnloadCfg_Col.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Col.Location = new System.Drawing.Point(97, 205);
            this.UnloadCfg_Col.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UnloadCfg_Col.Name = "UnloadCfg_Col";
            this.UnloadCfg_Col.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Col.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.UnloadCfg_Col.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_Col.Size = new System.Drawing.Size(80, 24);
            this.UnloadCfg_Col.TabIndex = 82;
            this.UnloadCfg_Col.EditValueChanged += new System.EventHandler(this.UnloadCfg_Col_EditValueChanged);
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.AppearanceDisabled.Options.UseFont = true;
            this.labelControl47.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.AppearanceHovered.Options.UseFont = true;
            this.labelControl47.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.AppearancePressed.Options.UseFont = true;
            this.labelControl47.Location = new System.Drawing.Point(183, 214);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(12, 17);
            this.labelControl47.TabIndex = 245;
            this.labelControl47.Text = "个";
            // 
            // UnloadCfg_Row
            // 
            this.UnloadCfg_Row.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Row.Location = new System.Drawing.Point(97, 170);
            this.UnloadCfg_Row.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UnloadCfg_Row.Name = "UnloadCfg_Row";
            this.UnloadCfg_Row.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_Row.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Row.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Row.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Row.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Row.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.UnloadCfg_Row.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_Row.Size = new System.Drawing.Size(80, 24);
            this.UnloadCfg_Row.TabIndex = 80;
            this.UnloadCfg_Row.EditValueChanged += new System.EventHandler(this.UnloadCfg_Row_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 79;
            this.label3.Text = "包装总盘";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.AppearanceDisabled.Options.UseFont = true;
            this.labelControl42.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.AppearanceHovered.Options.UseFont = true;
            this.labelControl42.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.AppearancePressed.Options.UseFont = true;
            this.labelControl42.Location = new System.Drawing.Point(20, 388);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(72, 17);
            this.labelControl42.TabIndex = 228;
            this.labelControl42.Text = "产品虚拟条码";
            // 
            // UnloadCfg_CountTray
            // 
            this.UnloadCfg_CountTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_CountTray.Location = new System.Drawing.Point(97, 240);
            this.UnloadCfg_CountTray.Name = "UnloadCfg_CountTray";
            this.UnloadCfg_CountTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_CountTray.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_CountTray.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.UnloadCfg_CountTray.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_CountTray.Size = new System.Drawing.Size(80, 24);
            this.UnloadCfg_CountTray.TabIndex = 10;
            this.UnloadCfg_CountTray.EditValueChanged += new System.EventHandler(this.UnloadCfg_CountTray_EditValueChanged);
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.AppearanceDisabled.Options.UseFont = true;
            this.labelControl39.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.AppearanceHovered.Options.UseFont = true;
            this.labelControl39.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.AppearancePressed.Options.UseFont = true;
            this.labelControl39.Location = new System.Drawing.Point(17, 213);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(75, 17);
            this.labelControl39.TabIndex = 222;
            this.labelControl39.Text = "产品例数(Col)";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.AppearanceDisabled.Options.UseFont = true;
            this.labelControl41.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.AppearanceHovered.Options.UseFont = true;
            this.labelControl41.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.AppearancePressed.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(20, 353);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(72, 17);
            this.labelControl41.TabIndex = 226;
            this.labelControl41.Text = "生产备用余量";
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl36.AppearanceDisabled.Options.UseFont = true;
            this.labelControl36.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl36.AppearanceHovered.Options.UseFont = true;
            this.labelControl36.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl36.AppearancePressed.Options.UseFont = true;
            this.labelControl36.Location = new System.Drawing.Point(11, 178);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(81, 17);
            this.labelControl36.TabIndex = 220;
            this.labelControl36.Text = "产品行数(Row)";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.AppearanceDisabled.Options.UseFont = true;
            this.labelControl40.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.AppearanceHovered.Options.UseFont = true;
            this.labelControl40.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.AppearancePressed.Options.UseFont = true;
            this.labelControl40.Location = new System.Drawing.Point(20, 318);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(72, 17);
            this.labelControl40.TabIndex = 224;
            this.labelControl40.Text = "生产实际总量";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl35.AppearanceDisabled.Options.UseFont = true;
            this.labelControl35.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl35.AppearanceHovered.Options.UseFont = true;
            this.labelControl35.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl35.AppearancePressed.Options.UseFont = true;
            this.labelControl35.Location = new System.Drawing.Point(41, 73);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(51, 17);
            this.labelControl35.TabIndex = 218;
            this.labelControl35.Text = "PCB 板号";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.AppearanceDisabled.Options.UseFont = true;
            this.labelControl48.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.AppearanceHovered.Options.UseFont = true;
            this.labelControl48.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.AppearancePressed.Options.UseFont = true;
            this.labelControl48.Location = new System.Drawing.Point(56, 38);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(36, 17);
            this.labelControl48.TabIndex = 216;
            this.labelControl48.Text = "工单号";
            // 
            // txtWorkOrder2
            // 
            this.txtWorkOrder2.EditValue = "";
            this.txtWorkOrder2.Location = new System.Drawing.Point(97, 30);
            this.txtWorkOrder2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtWorkOrder2.Name = "txtWorkOrder2";
            this.txtWorkOrder2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtWorkOrder2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtWorkOrder2.Properties.Appearance.Options.UseBackColor = true;
            this.txtWorkOrder2.Properties.Appearance.Options.UseFont = true;
            this.txtWorkOrder2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtWorkOrder2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtWorkOrder2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtWorkOrder2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkOrder2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtWorkOrder2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtWorkOrder2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtWorkOrder2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtWorkOrder2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkOrder2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtWorkOrder2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtWorkOrder2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtWorkOrder2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtWorkOrder2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkOrder2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtWorkOrder2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtWorkOrder2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtWorkOrder2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtWorkOrder2.Size = new System.Drawing.Size(191, 24);
            this.txtWorkOrder2.TabIndex = 215;
            // 
            // groupControl8
            // 
            this.groupControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl8.Appearance.Options.UseFont = true;
            this.groupControl8.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl8.AppearanceCaption.Options.UseFont = true;
            this.groupControl8.Controls.Add(this.烧录2是否翻转);
            this.groupControl8.Controls.Add(this.btmAllOn);
            this.groupControl8.Controls.Add(this.上料是否翻转);
            this.groupControl8.Controls.Add(this.btmAllOff);
            this.groupControl8.Controls.Add(this.烧录是否翻转);
            this.groupControl8.Controls.Add(this.打码切割是否翻转);
            this.groupControl8.Controls.Add(this.下料组盘是否翻转);
            this.groupControl8.Controls.Add(this.包装是否翻转);
            this.groupControl8.Location = new System.Drawing.Point(525, 3);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(262, 222);
            this.groupControl8.TabIndex = 179;
            this.groupControl8.Text = "工位是否翻转设定";
            // 
            // 烧录2是否翻转
            // 
            this.烧录2是否翻转.Location = new System.Drawing.Point(15, 79);
            this.烧录2是否翻转.Name = "烧录2是否翻转";
            this.烧录2是否翻转.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.烧录2是否翻转.Properties.Appearance.Options.UseFont = true;
            this.烧录2是否翻转.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录2是否翻转.Properties.AppearanceDisabled.Options.UseFont = true;
            this.烧录2是否翻转.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录2是否翻转.Properties.AppearanceFocused.Options.UseFont = true;
            this.烧录2是否翻转.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录2是否翻转.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.烧录2是否翻转.Properties.Caption = "烧录2是否翻转";
            this.烧录2是否翻转.Size = new System.Drawing.Size(118, 21);
            this.烧录2是否翻转.TabIndex = 179;
            this.烧录2是否翻转.CheckedChanged += new System.EventHandler(this.烧录2是否翻转_CheckedChanged);
            // 
            // btmAllOn
            // 
            this.btmAllOn.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAllOn.Appearance.Options.UseFont = true;
            this.btmAllOn.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmAllOn.AppearanceDisabled.Options.UseFont = true;
            this.btmAllOn.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmAllOn.AppearanceHovered.Options.UseFont = true;
            this.btmAllOn.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmAllOn.AppearancePressed.Options.UseFont = true;
            this.btmAllOn.Location = new System.Drawing.Point(15, 178);
            this.btmAllOn.Name = "btmAllOn";
            this.btmAllOn.Size = new System.Drawing.Size(101, 39);
            this.btmAllOn.TabIndex = 177;
            this.btmAllOn.Text = "All On(翻转)";
            this.btmAllOn.Click += new System.EventHandler(this.btmAllOn_Click);
            // 
            // 上料是否翻转
            // 
            this.上料是否翻转.Location = new System.Drawing.Point(15, 31);
            this.上料是否翻转.Name = "上料是否翻转";
            this.上料是否翻转.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.上料是否翻转.Properties.Appearance.Options.UseFont = true;
            this.上料是否翻转.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.上料是否翻转.Properties.AppearanceDisabled.Options.UseFont = true;
            this.上料是否翻转.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.上料是否翻转.Properties.AppearanceFocused.Options.UseFont = true;
            this.上料是否翻转.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.上料是否翻转.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.上料是否翻转.Properties.Caption = "上料是否翻转";
            this.上料是否翻转.Size = new System.Drawing.Size(118, 21);
            this.上料是否翻转.TabIndex = 98;
            this.上料是否翻转.CheckedChanged += new System.EventHandler(this.上料是否翻转_CheckedChanged);
            // 
            // btmAllOff
            // 
            this.btmAllOff.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmAllOff.Appearance.Options.UseFont = true;
            this.btmAllOff.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmAllOff.AppearanceDisabled.Options.UseFont = true;
            this.btmAllOff.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmAllOff.AppearanceHovered.Options.UseFont = true;
            this.btmAllOff.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmAllOff.AppearancePressed.Options.UseFont = true;
            this.btmAllOff.Location = new System.Drawing.Point(122, 178);
            this.btmAllOff.Name = "btmAllOff";
            this.btmAllOff.Size = new System.Drawing.Size(101, 39);
            this.btmAllOff.TabIndex = 178;
            this.btmAllOff.Text = "All Off(翻转)";
            this.btmAllOff.Click += new System.EventHandler(this.btmAllOff_Click);
            // 
            // 烧录是否翻转
            // 
            this.烧录是否翻转.Location = new System.Drawing.Point(15, 55);
            this.烧录是否翻转.Name = "烧录是否翻转";
            this.烧录是否翻转.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.烧录是否翻转.Properties.Appearance.Options.UseFont = true;
            this.烧录是否翻转.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录是否翻转.Properties.AppearanceDisabled.Options.UseFont = true;
            this.烧录是否翻转.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录是否翻转.Properties.AppearanceFocused.Options.UseFont = true;
            this.烧录是否翻转.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录是否翻转.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.烧录是否翻转.Properties.Caption = "烧录1是否翻转";
            this.烧录是否翻转.Size = new System.Drawing.Size(118, 21);
            this.烧录是否翻转.TabIndex = 99;
            this.烧录是否翻转.CheckedChanged += new System.EventHandler(this.烧录是否翻转_CheckedChanged);
            // 
            // 打码切割是否翻转
            // 
            this.打码切割是否翻转.Location = new System.Drawing.Point(15, 103);
            this.打码切割是否翻转.Name = "打码切割是否翻转";
            this.打码切割是否翻转.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.打码切割是否翻转.Properties.Appearance.Options.UseFont = true;
            this.打码切割是否翻转.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码切割是否翻转.Properties.AppearanceDisabled.Options.UseFont = true;
            this.打码切割是否翻转.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码切割是否翻转.Properties.AppearanceFocused.Options.UseFont = true;
            this.打码切割是否翻转.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码切割是否翻转.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.打码切割是否翻转.Properties.Caption = "打码切割是否翻转";
            this.打码切割是否翻转.Size = new System.Drawing.Size(118, 21);
            this.打码切割是否翻转.TabIndex = 100;
            this.打码切割是否翻转.CheckedChanged += new System.EventHandler(this.打码切割是否翻转_CheckedChanged);
            // 
            // 下料组盘是否翻转
            // 
            this.下料组盘是否翻转.Location = new System.Drawing.Point(15, 127);
            this.下料组盘是否翻转.Name = "下料组盘是否翻转";
            this.下料组盘是否翻转.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.下料组盘是否翻转.Properties.Appearance.Options.UseFont = true;
            this.下料组盘是否翻转.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料组盘是否翻转.Properties.AppearanceDisabled.Options.UseFont = true;
            this.下料组盘是否翻转.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料组盘是否翻转.Properties.AppearanceFocused.Options.UseFont = true;
            this.下料组盘是否翻转.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料组盘是否翻转.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.下料组盘是否翻转.Properties.Caption = "下料组盘是否翻转";
            this.下料组盘是否翻转.Size = new System.Drawing.Size(118, 21);
            this.下料组盘是否翻转.TabIndex = 101;
            this.下料组盘是否翻转.CheckedChanged += new System.EventHandler(this.下料组盘是否翻转_CheckedChanged);
            // 
            // 包装是否翻转
            // 
            this.包装是否翻转.Location = new System.Drawing.Point(15, 151);
            this.包装是否翻转.Name = "包装是否翻转";
            this.包装是否翻转.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.包装是否翻转.Properties.Appearance.Options.UseFont = true;
            this.包装是否翻转.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.包装是否翻转.Properties.AppearanceDisabled.Options.UseFont = true;
            this.包装是否翻转.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.包装是否翻转.Properties.AppearanceFocused.Options.UseFont = true;
            this.包装是否翻转.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.包装是否翻转.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.包装是否翻转.Properties.Caption = "包装是否翻转";
            this.包装是否翻转.Size = new System.Drawing.Size(118, 21);
            this.包装是否翻转.TabIndex = 102;
            this.包装是否翻转.CheckedChanged += new System.EventHandler(this.包装是否翻转_CheckedChanged);
            // 
            // groupControl11
            // 
            this.groupControl11.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl11.Appearance.Options.UseFont = true;
            this.groupControl11.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl11.AppearanceCaption.Options.UseFont = true;
            this.groupControl11.Location = new System.Drawing.Point(525, 232);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(262, 239);
            this.groupControl11.TabIndex = 181;
            this.groupControl11.Text = "下料包装总数";
            // 
            // tabNG_烧录
            // 
            this.tabNG_烧录.Caption = "烧录";
            this.tabNG_烧录.Controls.Add(this.TestFunction);
            this.tabNG_烧录.Controls.Add(this.txtFlashNumFile);
            this.tabNG_烧录.Controls.Add(this.button3);
            this.tabNG_烧录.Controls.Add(this.label26);
            this.tabNG_烧录.Controls.Add(this.chk切换行例数据);
            this.tabNG_烧录.Controls.Add(this.txtFlashMenuFile);
            this.tabNG_烧录.Controls.Add(this.btmGetFalshPro);
            this.tabNG_烧录.Controls.Add(this.button2);
            this.tabNG_烧录.Controls.Add(this.label28);
            this.tabNG_烧录.Controls.Add(this.panelControl7);
            this.tabNG_烧录.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_烧录.Image")));
            this.tabNG_烧录.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_烧录.Name = "tabNG_烧录";
            this.tabNG_烧录.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_烧录.Size = new System.Drawing.Size(984, 478);
            // 
            // txtFlashNumFile
            // 
            this.txtFlashNumFile.FormattingEnabled = true;
            this.txtFlashNumFile.Location = new System.Drawing.Point(87, 39);
            this.txtFlashNumFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFlashNumFile.Name = "txtFlashNumFile";
            this.txtFlashNumFile.Size = new System.Drawing.Size(786, 25);
            this.txtFlashNumFile.TabIndex = 219;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Moccasin;
            this.button3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button3.Location = new System.Drawing.Point(879, 37);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(74, 29);
            this.button3.TabIndex = 218;
            this.button3.Text = "打开(&O)";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(1, 42);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 17);
            this.label26.TabIndex = 217;
            this.label26.Text = "烧录序号文件";
            // 
            // chk切换行例数据
            // 
            this.chk切换行例数据.Location = new System.Drawing.Point(517, 163);
            this.chk切换行例数据.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chk切换行例数据.Name = "chk切换行例数据";
            this.chk切换行例数据.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.chk切换行例数据.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chk切换行例数据.Properties.Appearance.Options.UseBackColor = true;
            this.chk切换行例数据.Properties.Appearance.Options.UseFont = true;
            this.chk切换行例数据.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.chk切换行例数据.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chk切换行例数据.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.chk切换行例数据.Properties.AppearanceFocused.Options.UseFont = true;
            this.chk切换行例数据.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.chk切换行例数据.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chk切换行例数据.Properties.Caption = "切换行例数据";
            this.chk切换行例数据.Size = new System.Drawing.Size(290, 25);
            this.chk切换行例数据.TabIndex = 216;
            this.chk切换行例数据.CheckedChanged += new System.EventHandler(this.chk切换行例数据_CheckedChanged);
            // 
            // txtFlashMenuFile
            // 
            this.txtFlashMenuFile.FormattingEnabled = true;
            this.txtFlashMenuFile.Location = new System.Drawing.Point(87, 6);
            this.txtFlashMenuFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFlashMenuFile.Name = "txtFlashMenuFile";
            this.txtFlashMenuFile.Size = new System.Drawing.Size(786, 25);
            this.txtFlashMenuFile.TabIndex = 215;
            // 
            // btmGetFalshPro
            // 
            this.btmGetFalshPro.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmGetFalshPro.Appearance.Options.UseFont = true;
            this.btmGetFalshPro.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmGetFalshPro.AppearanceDisabled.Options.UseFont = true;
            this.btmGetFalshPro.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmGetFalshPro.AppearanceHovered.Options.UseFont = true;
            this.btmGetFalshPro.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmGetFalshPro.AppearancePressed.Options.UseFont = true;
            this.btmGetFalshPro.Location = new System.Drawing.Point(517, 68);
            this.btmGetFalshPro.Name = "btmGetFalshPro";
            this.btmGetFalshPro.Size = new System.Drawing.Size(163, 69);
            this.btmGetFalshPro.TabIndex = 1;
            this.btmGetFalshPro.Text = "获取烧录字段";
            this.btmGetFalshPro.Click += new System.EventHandler(this.btmGetFalshPro_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Moccasin;
            this.button2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.Location = new System.Drawing.Point(879, 4);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 29);
            this.button2.TabIndex = 214;
            this.button2.Text = "打开(&O)";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(1, 9);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(80, 17);
            this.label28.TabIndex = 213;
            this.label28.Text = "烧录菜单文件";
            // 
            // panelControl7
            // 
            this.panelControl7.Location = new System.Drawing.Point(1, 69);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(500, 417);
            this.panelControl7.TabIndex = 0;
            // 
            // tabNG_打码切割
            // 
            this.tabNG_打码切割.Caption = "打码切割";
            this.tabNG_打码切割.Controls.Add(this.spinEdit8);
            this.tabNG_打码切割.Controls.Add(this.label7);
            this.tabNG_打码切割.Controls.Add(this.labelControl62);
            this.tabNG_打码切割.Controls.Add(this.txtMarkText);
            this.tabNG_打码切割.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_打码切割.Image")));
            this.tabNG_打码切割.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打码切割.Name = "tabNG_打码切割";
            this.tabNG_打码切割.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打码切割.Size = new System.Drawing.Size(984, 478);
            // 
            // spinEdit8
            // 
            this.spinEdit8.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit8.Location = new System.Drawing.Point(146, 81);
            this.spinEdit8.Name = "spinEdit8";
            this.spinEdit8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.spinEdit8.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spinEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.spinEdit8.Properties.Appearance.Options.UseFont = true;
            this.spinEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinEdit8.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit8.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinEdit8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinEdit8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit8.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEdit8.Properties.ReadOnly = true;
            this.spinEdit8.Size = new System.Drawing.Size(80, 24);
            this.spinEdit8.TabIndex = 219;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 17);
            this.label7.TabIndex = 220;
            this.label7.Text = "当前已包装盘数(所有)";
            // 
            // labelControl62
            // 
            this.labelControl62.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl62.Appearance.Options.UseFont = true;
            this.labelControl62.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl62.AppearanceDisabled.Options.UseFont = true;
            this.labelControl62.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl62.AppearanceHovered.Options.UseFont = true;
            this.labelControl62.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl62.AppearancePressed.Options.UseFont = true;
            this.labelControl62.Location = new System.Drawing.Point(23, 23);
            this.labelControl62.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(48, 17);
            this.labelControl62.TabIndex = 218;
            this.labelControl62.Text = "打码字符";
            // 
            // txtMarkText
            // 
            this.txtMarkText.EditValue = "";
            this.txtMarkText.Location = new System.Drawing.Point(75, 19);
            this.txtMarkText.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMarkText.Name = "txtMarkText";
            this.txtMarkText.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtMarkText.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtMarkText.Properties.Appearance.Options.UseBackColor = true;
            this.txtMarkText.Properties.Appearance.Options.UseFont = true;
            this.txtMarkText.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMarkText.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMarkText.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMarkText.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMarkText.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtMarkText.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtMarkText.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMarkText.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMarkText.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMarkText.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtMarkText.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtMarkText.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMarkText.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMarkText.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMarkText.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtMarkText.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtMarkText.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMarkText.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMarkText.Size = new System.Drawing.Size(191, 24);
            this.txtMarkText.TabIndex = 217;
            // 
            // tabNG_下料组盘
            // 
            this.tabNG_下料组盘.Caption = "下料组盘";
            this.tabNG_下料组盘.Controls.Add(this.chkEnableMarkTest);
            this.tabNG_下料组盘.Controls.Add(this.chkLaserMarkTest_Down);
            this.tabNG_下料组盘.Controls.Add(this.chkLaserMarkTest_Up);
            this.tabNG_下料组盘.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_下料组盘.Image")));
            this.tabNG_下料组盘.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_下料组盘.Name = "tabNG_下料组盘";
            this.tabNG_下料组盘.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_下料组盘.Size = new System.Drawing.Size(984, 478);
            // 
            // chkEnableMarkTest
            // 
            this.chkEnableMarkTest.EditValue = true;
            this.chkEnableMarkTest.Location = new System.Drawing.Point(20, 16);
            this.chkEnableMarkTest.Name = "chkEnableMarkTest";
            this.chkEnableMarkTest.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEnableMarkTest.Properties.Appearance.Options.UseFont = true;
            this.chkEnableMarkTest.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableMarkTest.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkEnableMarkTest.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableMarkTest.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkEnableMarkTest.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableMarkTest.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkEnableMarkTest.Properties.Caption = "启用打码检测";
            this.chkEnableMarkTest.Size = new System.Drawing.Size(110, 21);
            this.chkEnableMarkTest.TabIndex = 217;
            this.chkEnableMarkTest.CheckedChanged += new System.EventHandler(this.chkEnableMarkTest_CheckedChanged);
            // 
            // chkLaserMarkTest_Down
            // 
            this.chkLaserMarkTest_Down.Location = new System.Drawing.Point(31, 70);
            this.chkLaserMarkTest_Down.Name = "chkLaserMarkTest_Down";
            this.chkLaserMarkTest_Down.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLaserMarkTest_Down.Properties.Appearance.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Down.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Down.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Down.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.Caption = "启用底部相机(2)打码检测";
            this.chkLaserMarkTest_Down.Size = new System.Drawing.Size(174, 21);
            this.chkLaserMarkTest_Down.TabIndex = 216;
            // 
            // chkLaserMarkTest_Up
            // 
            this.chkLaserMarkTest_Up.Location = new System.Drawing.Point(31, 43);
            this.chkLaserMarkTest_Up.Name = "chkLaserMarkTest_Up";
            this.chkLaserMarkTest_Up.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLaserMarkTest_Up.Properties.Appearance.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Up.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Up.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Up.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.Caption = "启用顶部相机(1)打码检测";
            this.chkLaserMarkTest_Up.Size = new System.Drawing.Size(203, 21);
            this.chkLaserMarkTest_Up.TabIndex = 215;
            // 
            // tabNG_打标包装
            // 
            this.tabNG_打标包装.Caption = "打标包装";
            this.tabNG_打标包装.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_打标包装.Image")));
            this.tabNG_打标包装.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打标包装.Name = "tabNG_打标包装";
            this.tabNG_打标包装.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打标包装.Size = new System.Drawing.Size(984, 478);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.simpleButton2.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.simpleButton2.AppearanceHovered.Options.UseFont = true;
            this.simpleButton2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.simpleButton2.AppearancePressed.Options.UseFont = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(1061, 446);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(154, 55);
            this.simpleButton2.TabIndex = 182;
            this.simpleButton2.Text = "保存所有";
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.btmSaveInvokeCfg_Click);
            // 
            // btmSaveInvokeCfg
            // 
            this.btmSaveInvokeCfg.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmSaveInvokeCfg.Appearance.Options.UseFont = true;
            this.btmSaveInvokeCfg.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmSaveInvokeCfg.AppearanceDisabled.Options.UseFont = true;
            this.btmSaveInvokeCfg.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmSaveInvokeCfg.AppearanceHovered.Options.UseFont = true;
            this.btmSaveInvokeCfg.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btmSaveInvokeCfg.AppearancePressed.Options.UseFont = true;
            this.btmSaveInvokeCfg.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmSaveInvokeCfg.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSaveInvokeCfg.ImageOptions.Image")));
            this.btmSaveInvokeCfg.Location = new System.Drawing.Point(12, 564);
            this.btmSaveInvokeCfg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSaveInvokeCfg.Name = "btmSaveInvokeCfg";
            this.btmSaveInvokeCfg.Size = new System.Drawing.Size(312, 78);
            this.btmSaveInvokeCfg.TabIndex = 176;
            this.btmSaveInvokeCfg.Text = "保存所有";
            this.btmSaveInvokeCfg.Click += new System.EventHandler(this.btmSaveInvokeCfg_Click);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Location = new System.Drawing.Point(1020, 3);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(554, 616);
            this.propertyGrid1.TabIndex = 1;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            this.propertyGrid1.Click += new System.EventHandler(this.propertyGrid1_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton4.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton4.Location = new System.Drawing.Point(94, 101);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(74, 50);
            this.simpleButton4.TabIndex = 24;
            // 
            // simpleButton5
            // 
            this.simpleButton5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.simpleButton5.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.simpleButton5.Location = new System.Drawing.Point(3, 101);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(74, 50);
            this.simpleButton5.TabIndex = 25;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(3, 8);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.textEdit8.Size = new System.Drawing.Size(100, 20);
            this.textEdit8.TabIndex = 0;
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(109, 7);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(75, 23);
            this.simpleButton11.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearanceDisabled.Options.UseFont = true;
            this.labelControl1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearanceHovered.Options.UseFont = true;
            this.labelControl1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearancePressed.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(198, 106);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(0, 17);
            this.labelControl1.TabIndex = 88;
            // 
            // textEdit9
            // 
            this.textEdit9.EditValue = "NG:25468";
            this.textEdit9.Location = new System.Drawing.Point(195, 127);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.textEdit9.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseBorderColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit9.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit9.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.textEdit9.Size = new System.Drawing.Size(86, 24);
            this.textEdit9.TabIndex = 89;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 209);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 17);
            this.label17.TabIndex = 252;
            this.label17.Text = "最后托盘剩余个数";
            // 
            // sped最后托盘剩余个数2
            // 
            this.sped最后托盘剩余个数2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped最后托盘剩余个数2.Location = new System.Drawing.Point(128, 206);
            this.sped最后托盘剩余个数2.Name = "sped最后托盘剩余个数2";
            this.sped最后托盘剩余个数2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.sped最后托盘剩余个数2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped最后托盘剩余个数2.Properties.Appearance.Options.UseBackColor = true;
            this.sped最后托盘剩余个数2.Properties.Appearance.Options.UseFont = true;
            this.sped最后托盘剩余个数2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped最后托盘剩余个数2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped最后托盘剩余个数2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped最后托盘剩余个数2.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped最后托盘剩余个数2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped最后托盘剩余个数2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped最后托盘剩余个数2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped最后托盘剩余个数2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped最后托盘剩余个数2.Properties.ReadOnly = true;
            this.sped最后托盘剩余个数2.Size = new System.Drawing.Size(92, 24);
            this.sped最后托盘剩余个数2.TabIndex = 251;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(9, 183);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 17);
            this.label18.TabIndex = 250;
            this.label18.Text = "打码/切割最后数量";
            // 
            // spedLaserRemaind2
            // 
            this.spedLaserRemaind2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedLaserRemaind2.Location = new System.Drawing.Point(128, 180);
            this.spedLaserRemaind2.Name = "spedLaserRemaind2";
            this.spedLaserRemaind2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.spedLaserRemaind2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedLaserRemaind2.Properties.Appearance.Options.UseBackColor = true;
            this.spedLaserRemaind2.Properties.Appearance.Options.UseFont = true;
            this.spedLaserRemaind2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedLaserRemaind2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedLaserRemaind2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedLaserRemaind2.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedLaserRemaind2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedLaserRemaind2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedLaserRemaind2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedLaserRemaind2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedLaserRemaind2.Properties.ReadOnly = true;
            this.spedLaserRemaind2.Size = new System.Drawing.Size(92, 24);
            this.spedLaserRemaind2.TabIndex = 249;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(14, 157);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(104, 17);
            this.label19.TabIndex = 248;
            this.label19.Text = "当前已出好品个数";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(42, 79);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 17);
            this.label20.TabIndex = 247;
            this.label20.Text = "生产应上盘数";
            // 
            // sped生产应上盘数2
            // 
            this.sped生产应上盘数2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped生产应上盘数2.Location = new System.Drawing.Point(128, 76);
            this.sped生产应上盘数2.Name = "sped生产应上盘数2";
            this.sped生产应上盘数2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped生产应上盘数2.Properties.Appearance.Options.UseFont = true;
            this.sped生产应上盘数2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产应上盘数2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped生产应上盘数2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产应上盘数2.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped生产应上盘数2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产应上盘数2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped生产应上盘数2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped生产应上盘数2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped生产应上盘数2.Properties.ReadOnly = true;
            this.sped生产应上盘数2.Size = new System.Drawing.Size(92, 24);
            this.sped生产应上盘数2.TabIndex = 246;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(30, 105);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 17);
            this.label21.TabIndex = 245;
            this.label21.Text = "生产实际总盘数";
            // 
            // sped生产实际总盘数2
            // 
            this.sped生产实际总盘数2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped生产实际总盘数2.Location = new System.Drawing.Point(128, 102);
            this.sped生产实际总盘数2.Name = "sped生产实际总盘数2";
            this.sped生产实际总盘数2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped生产实际总盘数2.Properties.Appearance.Options.UseFont = true;
            this.sped生产实际总盘数2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产实际总盘数2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped生产实际总盘数2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产实际总盘数2.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped生产实际总盘数2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产实际总盘数2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped生产实际总盘数2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped生产实际总盘数2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped生产实际总盘数2.Properties.ReadOnly = true;
            this.sped生产实际总盘数2.Size = new System.Drawing.Size(92, 24);
            this.sped生产实际总盘数2.TabIndex = 244;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(26, 131);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 17);
            this.label22.TabIndex = 243;
            this.label22.Text = "当前坏品总数量";
            // 
            // sped当前坏品总数量2
            // 
            this.sped当前坏品总数量2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped当前坏品总数量2.Location = new System.Drawing.Point(128, 128);
            this.sped当前坏品总数量2.Name = "sped当前坏品总数量2";
            this.sped当前坏品总数量2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped当前坏品总数量2.Properties.Appearance.Options.UseFont = true;
            this.sped当前坏品总数量2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped当前坏品总数量2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped当前坏品总数量2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped当前坏品总数量2.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped当前坏品总数量2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped当前坏品总数量2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped当前坏品总数量2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped当前坏品总数量2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped当前坏品总数量2.Properties.ReadOnly = true;
            this.sped当前坏品总数量2.Size = new System.Drawing.Size(92, 24);
            this.sped当前坏品总数量2.TabIndex = 242;
            // 
            // TotalLineOutAll2
            // 
            this.TotalLineOutAll2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalLineOutAll2.Location = new System.Drawing.Point(128, 154);
            this.TotalLineOutAll2.Name = "TotalLineOutAll2";
            this.TotalLineOutAll2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotalLineOutAll2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalLineOutAll2.Properties.Appearance.Options.UseBackColor = true;
            this.TotalLineOutAll2.Properties.Appearance.Options.UseFont = true;
            this.TotalLineOutAll2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TotalLineOutAll2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll2.Properties.AppearanceFocused.Options.UseFont = true;
            this.TotalLineOutAll2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TotalLineOutAll2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalLineOutAll2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TotalLineOutAll2.Properties.ReadOnly = true;
            this.TotalLineOutAll2.Size = new System.Drawing.Size(92, 24);
            this.TotalLineOutAll2.TabIndex = 237;
            // 
            // Sped生产总量2
            // 
            this.Sped生产总量2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Sped生产总量2.Location = new System.Drawing.Point(128, 24);
            this.Sped生产总量2.Name = "Sped生产总量2";
            this.Sped生产总量2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sped生产总量2.Properties.Appearance.Options.UseFont = true;
            this.Sped生产总量2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped生产总量2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Sped生产总量2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped生产总量2.Properties.AppearanceFocused.Options.UseFont = true;
            this.Sped生产总量2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Sped生产总量2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.Sped生产总量2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Sped生产总量2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Sped生产总量2.Properties.ReadOnly = true;
            this.Sped生产总量2.Size = new System.Drawing.Size(92, 24);
            this.Sped生产总量2.TabIndex = 238;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(10, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(112, 17);
            this.label23.TabIndex = 239;
            this.label23.Text = "生产总量(产品个数)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label24.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(30, 53);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 17);
            this.label24.TabIndex = 241;
            this.label24.Text = "生产计划总盘数";
            // 
            // sped生产总盘数2
            // 
            this.sped生产总盘数2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped生产总盘数2.Location = new System.Drawing.Point(128, 50);
            this.sped生产总盘数2.Name = "sped生产总盘数2";
            this.sped生产总盘数2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped生产总盘数2.Properties.Appearance.Options.UseFont = true;
            this.sped生产总盘数2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped生产总盘数2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数2.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped生产总盘数2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped生产总盘数2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped生产总盘数2.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped生产总盘数2.Properties.ReadOnly = true;
            this.sped生产总盘数2.Size = new System.Drawing.Size(92, 24);
            this.sped生产总盘数2.TabIndex = 240;
            // 
            // groupControl5
            // 
            this.groupControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.Appearance.Options.UseFont = true;
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.Controls.Add(this.label25);
            this.groupControl5.Controls.Add(this.良品率);
            this.groupControl5.Controls.Add(this.sped生产应上盘数2);
            this.groupControl5.Controls.Add(this.label17);
            this.groupControl5.Controls.Add(this.sped生产总盘数2);
            this.groupControl5.Controls.Add(this.sped最后托盘剩余个数2);
            this.groupControl5.Controls.Add(this.label18);
            this.groupControl5.Controls.Add(this.label24);
            this.groupControl5.Controls.Add(this.spedLaserRemaind2);
            this.groupControl5.Controls.Add(this.label23);
            this.groupControl5.Controls.Add(this.label19);
            this.groupControl5.Controls.Add(this.Sped生产总量2);
            this.groupControl5.Controls.Add(this.label22);
            this.groupControl5.Controls.Add(this.sped生产实际总盘数2);
            this.groupControl5.Controls.Add(this.sped当前坏品总数量2);
            this.groupControl5.Controls.Add(this.label20);
            this.groupControl5.Controls.Add(this.TotalLineOutAll2);
            this.groupControl5.Controls.Add(this.label21);
            this.groupControl5.Location = new System.Drawing.Point(1565, 699);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(339, 239);
            this.groupControl5.TabIndex = 253;
            this.groupControl5.Text = "数据统计(详细信息请看监控)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label25.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(226, 131);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 17);
            this.label25.TabIndex = 254;
            this.label25.Text = "良品率(%)";
            // 
            // 良品率
            // 
            this.良品率.EditValue = "";
            this.良品率.Location = new System.Drawing.Point(226, 154);
            this.良品率.Name = "良品率";
            this.良品率.Properties.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.良品率.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.良品率.Properties.Appearance.Options.UseBackColor = true;
            this.良品率.Properties.Appearance.Options.UseFont = true;
            this.良品率.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.良品率.Properties.AppearanceDisabled.Options.UseFont = true;
            this.良品率.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.良品率.Properties.AppearanceFocused.Options.UseFont = true;
            this.良品率.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.良品率.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.良品率.Properties.ReadOnly = true;
            this.良品率.Size = new System.Drawing.Size(85, 24);
            this.良品率.TabIndex = 253;
            // 
            // TestFunction
            // 
            this.TestFunction.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TestFunction.Appearance.Options.UseFont = true;
            this.TestFunction.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TestFunction.AppearanceDisabled.Options.UseFont = true;
            this.TestFunction.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TestFunction.AppearanceHovered.Options.UseFont = true;
            this.TestFunction.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TestFunction.AppearancePressed.Options.UseFont = true;
            this.TestFunction.Location = new System.Drawing.Point(517, 242);
            this.TestFunction.Name = "TestFunction";
            this.TestFunction.Size = new System.Drawing.Size(163, 27);
            this.TestFunction.TabIndex = 220;
            this.TestFunction.Text = "Test.....";
            this.TestFunction.Click += new System.EventHandler(this.TestFunction_Click);
            // 
            // frm_AutomaticSmartLine
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 940);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.navigationPane1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_AutomaticSmartLine";
            this.Text = "XtraForm1";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.frm_ASL_HelpBtmClick);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_ASL_Closing);
            this.Load += new System.EventHandler(this.frm_ASL_Load);
            this.SizeChanged += new System.EventHandler(this.frm_ASL_SizeChanged);
            this.Click += new System.EventHandler(this.frm_ASL_ControlClick);
            this.DoubleClick += new System.EventHandler(this.frm_ASL_ControlDoubleClick);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_ASL_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frm_ASL_KeyPress);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_ASL_MouseDoubleClick);
            this.MouseHover += new System.EventHandler(this.frm_ASL_ContrlMouseHover);
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.NP_主页运行.ResumeLayout(false);
            this.NP_主页运行.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastTrayID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyPacking.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyUnload.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyLaser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyFlash2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyFlash1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReadyLoad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            this.popupContainerControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTestPC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.NP_设置.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit3.Properties)).EndInit();
            this.NP_监控.ResumeLayout(false);
            this.NP_监控.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).EndInit();
            this.groupControl18.ResumeLayout(false);
            this.groupControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt已识别的好品数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.当前标签包装数量1068.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridFlagMark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped最后托盘剩余个数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLaserRemaind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产应上盘数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedPackingPackedCount2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedUnloadPackedCount2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产实际总盘数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped当前坏品总数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedUnloadPackedCountSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedPackingPackedCountSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedPackingPackedCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedUnloadPackedCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLineOutAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfBuffTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sped生产总量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfLoadTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped产品行数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOutPCS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTrayBK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedSetTrayOuted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产总盘数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sped包装总量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentCountTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.生产最后打包总盘数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped产品例数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2014.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2001.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2008.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2009.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2011.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2004.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2010.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2003.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2013.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2005.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2012.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2002.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2006.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD2007.Properties)).EndInit();
            this.NP_手动调试.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_B3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_B2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_B1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_A3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_A2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSN_A1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            this.NP_生产数据查询.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).EndInit();
            this.groupControl14.ResumeLayout(false);
            this.groupControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBIDForFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayIDForFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane2)).EndInit();
            this.tabPane2.ResumeLayout(false);
            this.Tab_DB_Load.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).EndInit();
            this.groupControl13.ResumeLayout(false);
            this.groupControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCB板号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStationResultStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEmStation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusDisplay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCB虚拟编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt托盘编号.Properties)).EndInit();
            this.Tab_DB_Tray.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.Tab_DB_Result.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.Tab_DB_Packing.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.Tab_DB_ORDER.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.NP_工艺参数.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.保存时写入所有信息.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNG_上料调度.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).EndInit();
            this.groupControl12.ResumeLayout(false);
            this.groupControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRealUpdateWorkSheet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBDstCol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBDstRow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spePCBLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speDstMaduo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTrayWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speTrayLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_TotalProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Remaind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Total.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBPNNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Col.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Row.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkOrder2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.烧录2是否翻转.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.上料是否翻转.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.烧录是否翻转.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.打码切割是否翻转.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.下料组盘是否翻转.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.包装是否翻转.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.tabNG_烧录.ResumeLayout(false);
            this.tabNG_烧录.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk切换行例数据.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.tabNG_打码切割.ResumeLayout(false);
            this.tabNG_打码切割.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMarkText.Properties)).EndInit();
            this.tabNG_下料组盘.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableMarkTest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Down.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Up.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped最后托盘剩余个数2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLaserRemaind2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产应上盘数2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产实际总盘数2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped当前坏品总数量2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLineOutAll2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sped生产总量2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产总盘数2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.良品率.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_主页运行;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_设置;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_手动调试;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_监控;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIPAddr;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtSNLength;
        private System.Windows.Forms.CheckBox chkSfcLength;
        private System.Windows.Forms.CheckBox chkSfcPrefix;
        private System.Windows.Forms.TextBox txtSfcSNPrefix;
        private System.Windows.Forms.CheckBox 不扫描条码;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_生产数据查询;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_工艺参数;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_上料调度;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_烧录;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_打码切割;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_下料组盘;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_打标包装;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraEditors.SimpleButton 获取工单信息;
        private DevExpress.XtraEditors.TextEdit txtWorkID;
        private DevExpress.XtraEditors.CheckEdit chkReadyLoad;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.MemoEdit memoEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit2;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.CheckEdit chkReadyPacking;
        private DevExpress.XtraEditors.CheckEdit chkReadyUnload;
        private DevExpress.XtraEditors.CheckEdit chkReadyLaser;
        private DevExpress.XtraEditors.CheckEdit chkReadyFlash2;
        private DevExpress.XtraEditors.CheckEdit chkReadyFlash1;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.SimpleButton btmSNTriggerPacking;
        private DevExpress.XtraEditors.SimpleButton btmSNTriggerUnload;
        private DevExpress.XtraEditors.SimpleButton btmSNTriggerLaser;
        private DevExpress.XtraEditors.SimpleButton btmSNTriggerFlash2;
        private DevExpress.XtraEditors.SimpleButton btmSNTriggerFlash1;
        private DevExpress.XtraEditors.SimpleButton btmSNTriggerLoad;
        private DevExpress.XtraEditors.TextEdit txtSN_B3;
        private DevExpress.XtraEditors.TextEdit txtSN_B2;
        private DevExpress.XtraEditors.TextEdit txtSN_B1;
        private DevExpress.XtraEditors.TextEdit txtSN_A3;
        private DevExpress.XtraEditors.TextEdit txtSN_A2;
        private DevExpress.XtraEditors.TextEdit txtSN_A1;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_ResetMachine;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_StopMachine;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_StartMachine;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.MemoEdit memoEdit3;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.CheckButton checkButton1;
        private DevExpress.XtraEditors.CheckButton 伺服启用禁用;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_ClearErrorMachine;
        private System.Windows.Forms.ImageList imageList16;
        private DevExpress.XtraEditors.CheckEdit 包装是否翻转;
        private DevExpress.XtraEditors.CheckEdit 下料组盘是否翻转;
        private DevExpress.XtraEditors.CheckEdit 打码切割是否翻转;
        private DevExpress.XtraEditors.CheckEdit 烧录是否翻转;
        private DevExpress.XtraEditors.CheckEdit 上料是否翻转;
        private DevExpress.XtraEditors.SimpleButton btmSaveInvokeCfg;
        private DevExpress.XtraEditors.SimpleButton btmAllOff;
        private DevExpress.XtraEditors.SimpleButton btmAllOn;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.TextEdit txtD2014;
        private DevExpress.XtraEditors.TextEdit txtD2001;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtD2008;
        private DevExpress.XtraEditors.TextEdit txtD2009;
        private DevExpress.XtraEditors.TextEdit txtD2011;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtD2004;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtD2010;
        private DevExpress.XtraEditors.TextEdit txtD2003;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtD2013;
        private DevExpress.XtraEditors.TextEdit txtD2005;
        private DevExpress.XtraEditors.TextEdit txtD2012;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit txtD2002;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtD2006;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit txtD2007;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.SimpleButton 复位所有烧录结果数据;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_CountTray;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Col;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Row;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Total;
        private DevExpress.XtraEditors.SimpleButton btmWriteUnload;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Remaind;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.CheckEdit 烧录2是否翻转;
        private DevExpress.XtraEditors.GroupControl groupControl12;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_TotalProduct;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private DevExpress.XtraBars.Navigation.TabPane tabPane2;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_Load;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_Flash;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.TextEdit txtWorkOrder2;
        private DevExpress.XtraEditors.TextEdit txtPCBPNNumber;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_Laser;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_Robot;
        private DevExpress.XtraEditors.GroupControl groupControl13;
        private DevExpress.XtraEditors.SimpleButton 手动绑盘;
        private DevExpress.XtraEditors.TextEdit txtPCB虚拟编号;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.TextEdit txt托盘编号;
        private DevExpress.XtraEditors.SimpleButton 自动生成PCBID;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.SimpleButton 手动解盘;
        private DevExpress.XtraEditors.TextEdit txtPCB板号;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.ComboBoxEdit cmbEmStation;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.SimpleButton 设置工位状态;
        private DevExpress.XtraEditors.CheckEdit chkStationResultStatus;
        private DevExpress.XtraEditors.TextEdit txtStatusDisplay;
        private DevExpress.XtraEditors.GroupControl groupControl15;
        private DevExpress.XtraEditors.GroupControl groupControl14;
        private DevExpress.XtraEditors.SimpleButton 设置工位结果数据;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.MemoEdit memoEdit4;
        private DevExpress.XtraEditors.SimpleButton 得到托盘当前状态;
        private DevExpress.XtraEditors.SimpleButton 得到PCBID当前状态;
        private DevExpress.XtraEditors.SimpleButton 得到PCBID当前结果;
        private DevExpress.XtraEditors.SimpleButton 得到托盘当前结果;
        private DevExpress.XtraEditors.SimpleButton 设置烧录工位结果;
        private DevExpress.XtraEditors.SimpleButton 得到烧录工位结果;
        private DevExpress.XtraEditors.SimpleButton 得到烧录TRYA工位结果;
        private System.Windows.Forms.CheckBox 强制MES验证通过;
        private HslCommunication.Controls.UserLantern LED_ProcessServerStatus;
        private System.Windows.Forms.PropertyGrid propertyProcess;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_Tray;
        private System.Windows.Forms.DataGridView dataGridView1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_Result;
        private System.Windows.Forms.DataGridView dataGridView2;
        private DevExpress.XtraEditors.SimpleButton btmRefenceTrayID;
        private DevExpress.XtraEditors.SimpleButton btmReferenceResults;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private DevExpress.XtraEditors.SimpleButton btmPrintLable;
        private DevExpress.XtraEditors.SimpleButton 输入包装信息;
        private DevExpress.XtraEditors.SimpleButton btmRefencePacking;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_Packing;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.ListBox cmbUnloadPCBs;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraEditors.SimpleButton btmChangeWorkOrder;
        private DevExpress.XtraEditors.TextEdit txtPCBIDForFind;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.TextEdit txtTrayIDForFind;
        private DevExpress.XtraEditors.SpinEdit spePCBDstCol;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.SpinEdit spePCBDstRow;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.SpinEdit spePCBWidth;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.SpinEdit spePCBLength;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.SpinEdit speDstMaduo;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.SpinEdit speTrayWidth;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.SpinEdit speTrayLength;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton btmGetFalshPro;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.CheckEdit 保存时写入所有信息;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.TextEdit txtMarkText;
        private DevExpress.XtraEditors.CheckEdit chkRealUpdateWorkSheet;
        private DevExpress.XtraEditors.SimpleButton btmGetCurrentWorkSheet;
        private System.Windows.Forms.PropertyGrid propertyGrid2;
        private DevExpress.XtraEditors.CheckEdit chkLaserMarkTest_Down;
        private DevExpress.XtraEditors.CheckEdit chkLaserMarkTest_Up;
        private DevExpress.XtraEditors.CheckEdit chkEnableMarkTest;
        private DevExpress.XtraEditors.TextEdit txtTestPC;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.SpinEdit spinEdit8;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.GroupControl groupControl18;
        private System.Windows.Forms.Label label62;
        private DevExpress.XtraEditors.SpinEdit TotalLineOutAll;
        private DevExpress.XtraEditors.SpinEdit speCountOfBuffTray;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private System.Windows.Forms.Label label65;
        private DevExpress.XtraEditors.SpinEdit Sped生产总量;
        private DevExpress.XtraEditors.SpinEdit speCountOfLoadTray;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.SpinEdit sped产品行数;
        private DevExpress.XtraEditors.SimpleButton btmSetCurrentTrayOut;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label53;
        private DevExpress.XtraEditors.SpinEdit TotalTrayOfLineOutPCS;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_CountTrayBK;
        private DevExpress.XtraEditors.SpinEdit spedSetTrayOuted;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.SpinEdit TotalTrayOfLineOut;
        private DevExpress.XtraEditors.SpinEdit sped生产总盘数;
        private System.Windows.Forms.Label label59;
        private DevExpress.XtraEditors.SpinEdit Sped包装总量;
        private DevExpress.XtraEditors.SpinEdit CurrentCountTray;
        private DevExpress.XtraEditors.SpinEdit 生产最后打包总盘数;
        private System.Windows.Forms.Label label56;
        private DevExpress.XtraEditors.SpinEdit sped产品例数;
        private DevExpress.XtraEditors.SpinEdit spinEdit4;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.SpinEdit spedPackingPackedCount;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.SpinEdit spedUnloadPackedCount;
        private DevExpress.XtraEditors.SimpleButton btmSetPackingCount;
        private DevExpress.XtraEditors.SpinEdit spedPackingPackedCountSet;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SpinEdit spedUnloadPackedCountSet;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.SpinEdit sped生产实际总盘数;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.SpinEdit sped当前坏品总数量;
        private DevExpress.XtraEditors.SimpleButton btmClearFailed;
        private System.Windows.Forms.CheckBox chkWriteWorkSheetToSQL;
        private DevExpress.XtraEditors.SpinEdit spedPackingPackedCount2;
        private DevExpress.XtraEditors.SpinEdit spedUnloadPackedCount2;
        private HslCommunication.Controls.UserLantern LastProductModeLED;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.SpinEdit sped生产应上盘数;
        private DevExpress.XtraEditors.SpinEdit spinEdit3;
        private System.Windows.Forms.CheckBox chkEnableLastMode;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.SpinEdit spedLaserRemaind;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.CheckButton btm进入尾料模式;
        private DevExpress.XtraEditors.SimpleButton btm得到最后打码标志;
        private System.Windows.Forms.CheckBox chkFocePackingSNOK;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.SpinEdit sped最后托盘剩余个数;
        private DevExpress.XtraEditors.SimpleButton btmSetLastTrayRemaindCount;
        private DevExpress.XtraEditors.SpinEdit spinEdit5;
        private System.Windows.Forms.DataGridView gridFlagMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private DevExpress.XtraEditors.SimpleButton brm刷新最后打码标志;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btmLoad;
        private DevExpress.XtraEditors.SimpleButton 手动获取当前选中工单及转工单;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.ListBox cmbPackingPCBs;
        private DevExpress.XtraEditors.SimpleButton btm当前工单完成;
        private DevExpress.XtraEditors.SimpleButton btm工单信息刷新;
        private DevExpress.XtraBars.Navigation.TabNavigationPage Tab_DB_ORDER;
        private System.Windows.Forms.DataGridView dataGridView4;
        private DevExpress.XtraEditors.SpinEdit spinEdit6;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.SpinEdit 当前标签包装数量1068;
        private DevExpress.XtraEditors.TextEdit txtLastTrayID;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.SpinEdit sped最后托盘剩余个数2;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.SpinEdit spedLaserRemaind2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.SpinEdit sped生产应上盘数2;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.SpinEdit sped生产实际总盘数2;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.SpinEdit sped当前坏品总数量2;
        private DevExpress.XtraEditors.SpinEdit TotalLineOutAll2;
        private DevExpress.XtraEditors.SpinEdit Sped生产总量2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.SpinEdit sped生产总盘数2;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.TextEdit 良品率;
        private System.Windows.Forms.ComboBox txtFlashMenuFile;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.CheckEdit chk切换行例数据;
        private System.Windows.Forms.ComboBox txtFlashNumFile;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.TextEdit txt已识别的好品数量;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.SimpleButton btmReconnectSQL;
        private DevExpress.XtraEditors.SimpleButton TestFunction;
    }
}