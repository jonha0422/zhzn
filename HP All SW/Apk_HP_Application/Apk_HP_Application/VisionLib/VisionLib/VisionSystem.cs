﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace VisionLib
{
    public class WordSpace
    {
        public String wsName;
        [XmlIgnoreAttribute]
        public int WordSpaceID;
        [XmlIgnoreAttribute]
        public List<ImageProcessFunction> CamImageFunList;
        public WordSpace(){
            CamImageFunList = new List<ImageProcessFunction>();
            wsName = "default";
        }
        public int init()
        {
            CamImageFunList.Clear();
            for (int i = 0; i < CamaeraLib.CameraSystem.Instance.CameraList.Count; i++)
            {
                ImageProcessFunction aCamFun = new ImageProcessFunction();
                aCamFun.CamID = i;
                aCamFun.WordSpaceID=WordSpaceID;
                aCamFun.Read(wsName);
                aCamFun.init();
                CamImageFunList.Add(aCamFun);
            }
            return 0;
        }
        public int Write()
        {
            for (int i = 0; i < CamImageFunList.Count; i++)
            {
                CamImageFunList[i].Writer(wsName);
            }
            return 0;
        }

        public CamaeraLib.Image ImageCaptureaOne(int CamID)
        {
            int suc=CamaeraLib.CameraSystem.Instance.CameraList[CamID].OneShot();
            return CamaeraLib.CameraSystem.Instance.CameraList[CamID].GetLatestImage();
        }

        public int ImageProcess(int CamID, bool CaptureNewImage = true) 
        {
            int suc = 0;
            if (CaptureNewImage)
                ImageCaptureaOne(CamID);

            suc += CamImageFunList[CamID].ImageProcess();
            return suc;
        }

        public List<VisionLib.ImageProcess.Result> ImageGetProcessResults(int CamID, int nIndexFunction)
        {
            return CamImageFunList[CamID].ImageProcessList[nIndexFunction].mResult;
        }

    }

    public class VisionSystem
    {
        private static VisionSystem instance;
        private static object theLock = new object();
        public string ProductName { get; set; }
        public List<string> ProductList;
        public List<WordSpace> WordSpaceList;
        //public String mWordSpace;
        //[XmlIgnoreAttribute]
        //public List<ImageProcessFunction> CamImageFunList;
        [XmlIgnoreAttribute]
        public bool AutoRun;
        public delegate void ImageProcessedAfterEventHandler(int WordSpace, int CamID);
        public event ImageProcessedAfterEventHandler ImageProcessAfter;//图像处理完成
        public delegate void ImageProcessBeforeEventHandler(int WordSpace, int CamID);
        public event ImageProcessBeforeEventHandler ImageProcessBefore;//图像处理前
        public static VisionSystem Instance
        {
            get
            {
                lock (theLock)
                {
                    if (instance == null)
                    {
                        instance = new VisionSystem();
                        instance.Read();
                        instance.init();
                    }
                    return instance;
                }
            }
        }
        public VisionSystem()
        {
            WordSpaceList = new List<WordSpace>();
            WordSpace aWordSpace = new WordSpace();
            ProductName = "3.7KW";
            ProductList = new List<string>();
           // ProductList.Add("3.7KW");
            //ProductList.Add("5.5KW");
            //WordSpaceList.Add(aWordSpace);
           // CamImageFunList = new List<ImageProcessFunction>();
           // mWordSpace = "default";
           // WordSpaceList.Add("default");
        }

        public bool Read()
        {

            using (TextReader reader1 = new StreamReader(Application.StartupPath + "\\Config\\VisionSystem.xml"))
            //using (TextReader reader1 = new StreamReader("MotionCardConfig.xml"))
            {
                XmlSerializer serializer1 = new XmlSerializer(typeof(VisionSystem));
                instance = (VisionSystem)serializer1.Deserialize(reader1);
                reader1.Close();
            }
            
            
            return true;
        }
        public bool Writer()
        {
            StreamWriter writer = new StreamWriter(Application.StartupPath + "\\Config\\VisionSystem.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(VisionSystem));
            serializer.Serialize(writer, instance);
            writer.Close();
            for (int j = 0; j < WordSpaceList.Count; j++)
            {
                WordSpaceList[j].Write();
            }
            return true;
        }
        public int init()
        {
            for (int j = 0; j < WordSpaceList.Count; j++)
            {
                WordSpaceList[j].WordSpaceID = j;
                WordSpaceList[j].init();
            }
            return 0;
        }
        public int ShowCamFuncSet(int CamID = 0)
        {
           
            ImageFunctionSet aImageFunFrm = new ImageFunctionSet();
            aImageFunFrm.curCamID = CamID;
            aImageFunFrm.ShowDialog();
            
            return 0;
        }
        public int ImageProcess(int Camid)
        {
            WordSpaceList[0].ImageProcess(Camid);
            return 0;
        }

        public CamaeraLib.Image ImageCaptureaOne(int spID, int CamID)
        {
            return WordSpaceList[spID].ImageCaptureaOne(CamID);
        }

        public int ImageProcess(int spID,int Camid, bool CaptureNewImage = true)
        {
            int suc=WordSpaceList[spID].ImageProcess(Camid, CaptureNewImage);
            return suc;
        }

        public bool getImageProcessResult(int spID, int Camid)
        {
            bool suc = WordSpaceList[spID].CamImageFunList[Camid].ResultSta;
            return suc;
        }

        public List<ImageProcess.Result> getLastImageProcessResultList(int spID, int Camid)
        {
            int id=WordSpaceList[spID].CamImageFunList[Camid].ImageProcessList.Count;
            if (id > 0)
            {
                return WordSpaceList[spID].CamImageFunList[Camid].ImageProcessList[id - 1].mResult;
            }
            return null;
        }
        public List<ImageProcess.Result> getImageProcessResultListById(int spID, int Camid,int Id)
        {
            int cou = WordSpaceList[spID].CamImageFunList[Camid].ImageProcessList.Count;
            if (cou > Id)
            {
                return WordSpaceList[spID].CamImageFunList[Camid].ImageProcessList[Id].mResult;
            }
            return null;
        }
        public int ImageProcessBeforeEvent(int spID, int Camid)
        {
            if (ImageProcessBefore != null)
            {
                ImageProcessBefore(spID, Camid);
            }
            return 0;
        }
        public int ImageProcessAfterEvent(int spID,int Camid)
        {
            if (ImageProcessAfter != null)
            {
                ImageProcessAfter(spID, Camid);
            }
            return 0;
        }
        public int setImageProcessViewPort(HWindowControl ViewPort, int spID, int Camid)
        {
            for (int i = 0; i < WordSpaceList[spID].CamImageFunList[Camid].ImageProcessList.Count; i++)
            {
                WordSpaceList[spID].CamImageFunList[Camid].ImageProcessList[i].ViewPort = ViewPort;
            }
            return 0;
        }
    }
}
