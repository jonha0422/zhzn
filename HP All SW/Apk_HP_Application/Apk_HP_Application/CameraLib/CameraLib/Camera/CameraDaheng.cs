﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ThridLibray;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using GxIAPINET;

namespace CamaeraLib.Camera
{
    
    public class CameraDaheng:CameraBase
    {
        List<IFrameData> m_frameList = new List<IFrameData>();        /* 图像缓存列表 */
        IFrameData _IFrameData;
        Thread renderThread = null;         /* 显示线程  */
        bool m_bShowLoop = true;            /* 线程控制变量 */

        public int CamId;
        public string CamName;
        private string m_TriggerSource;
        protected Object m_lockObject;                     /* Lock object used for thread synchronization. */
        private bool bColor;

        IGXFactory m_objIGXFactory = null;                   ///<Factory对像
        IGXDevice m_objIGXDevice = null;                   ///<设备对像
        IGXStream m_objIGXStream = null;                   ///<流对像
        IGXFeatureControl m_objIGXFeatureControl = null;                   ///<远端设备属性控制器对像
         bool m_bIsSnap = false;                          ///< 相机开始采集标识
        public string  PHJ { get; set; }
        private bool isOneShot = false;
        public CameraDaheng()
        {
            m_lockObject = new Object();
            m_objIGXFactory = IGXFactory.GetInstance();
            m_objIGXFactory.Init();
        }
        
        public override int OpenCamByID(short id)
        {
            CamId = id;
            try
            {
                List<IGXDeviceInfo> listGXDeviceInfo = new List<IGXDeviceInfo>();

                //关闭流
                __CloseStream();
                // 如果设备已经打开则关闭，保证相机在初始化出错情况下能再次打开
                __CloseDevice();

                m_objIGXFactory.UpdateDeviceList(200, listGXDeviceInfo);

                // 判断当前连接设备个数
                if (listGXDeviceInfo.Count <= 0)
                {
                    //MessageBox.Show("未发现设备!");
                    return -1;
                }

                if (listGXDeviceInfo.Count() <= id)
                {
                    return -1;
                }

                //打开列表第一个设备

                m_objIGXDevice = m_objIGXFactory.OpenDeviceBySN(listGXDeviceInfo[id].GetSN(), GX_ACCESS_MODE.GX_ACCESS_EXCLUSIVE);
                m_objIGXFeatureControl = m_objIGXDevice.GetRemoteFeatureControl();


                //打开流
                if (null != m_objIGXDevice)
                {
                    m_objIGXStream = m_objIGXDevice.OpenStream(0);
                    m_objIGXStream.RegisterCaptureCallback(this, __OnFrameCallbackFun);
                }

                

               // m_objGxBitmap = new GxBitmap(m_objIGXDevice, m_pic_ShowImage);

                // 更新设备打开标识
                m_open = true;

                __InitDevice();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return 0;
        }
        /// <summary>
        /// 关闭流
        /// </summary>
        private void __CloseStream()
        {
            try
            {
                //关闭流
                if (null != m_objIGXStream)
                {
                    m_objIGXStream.Close();
                    m_objIGXStream = null;
                }
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// 关闭设备
        /// </summary>
        private void __CloseDevice()
        {
            try
            {
                //关闭设备
                if (null != m_objIGXDevice)
                {
                    m_objIGXDevice.Close();
                    m_objIGXDevice = null;
                }
            }
            catch (Exception)
            {
            }
        }
        /// <summary>
        /// 相机初始化
        /// </summary>
        private void __InitDevice()
        {
            if (null != m_objIGXFeatureControl)
            {
                //设置采集模式连续采集
                m_objIGXFeatureControl.GetEnumFeature("AcquisitionMode").SetValue("Continuous");

                //设置触发模式为开
                // m_objIGXFeatureControl.GetEnumFeature("TriggerMode").SetValue("On");

                //选择触发源为软触发
                // m_objIGXFeatureControl.GetEnumFeature("TriggerSource").SetValue("Software");
                SetExposureTime((int)_CameraConfig.ExposureTime);
            }
        }
        
        public override int OpenCameraByName(string Name)
        {
            try
            {
                List<IGXDeviceInfo> listGXDeviceInfo = new List<IGXDeviceInfo>();

                //关闭流
                __CloseStream();
                // 如果设备已经打开则关闭，保证相机在初始化出错情况下能再次打开
                __CloseDevice();

                m_objIGXFactory.UpdateDeviceList(200, listGXDeviceInfo);

                // 判断当前连接设备个数
                if (listGXDeviceInfo.Count <= 0)
                {
                    //MessageBox.Show("未发现设备!");
                    return -1;
                }

                if (listGXDeviceInfo.Count() <= 0)
                {
                    return -1;
                }

                //打开设备

                m_objIGXDevice = m_objIGXFactory.OpenDeviceByUserID(Name, GX_ACCESS_MODE.GX_ACCESS_EXCLUSIVE);
                m_objIGXFeatureControl = m_objIGXDevice.GetRemoteFeatureControl();


                //打开流
                if (null != m_objIGXDevice)
                {
                    m_objIGXStream = m_objIGXDevice.OpenStream(0);
                    m_objIGXStream.RegisterCaptureCallback(this, __OnFrameCallbackFun);
                }

                // m_objGxBitmap = new GxBitmap(m_objIGXDevice, m_pic_ShowImage);

                // 更新设备打开标识
                m_open = true;

                __InitDevice();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }
        /// <summary>
        /// 停止采集关闭设备、关闭流
        /// </summary>
        private void __CloseAll()
        {
            try
            {
                //如果未停采则先停止采集
                if (m_bIsSnap)
                {
                    if (null != m_objIGXFeatureControl)
                    {
                        m_objIGXFeatureControl.GetCommandFeature("AcquisitionStop").Execute();
                        m_objIGXFeatureControl = null;
                    }
                }
            }
            catch (Exception)
            {

            }

            m_bIsSnap = false;

            try
            {
                //停止流通道、注销采集回调和关闭流
                if (null != m_objIGXStream)
                {
                    m_objIGXStream.StopGrab();
                    //注销采集回调函数
                    m_objIGXStream.UnregisterCaptureCallback();
                    m_objIGXStream.Close();
                    m_objIGXStream = null;
                }

            }
            catch (Exception)
            {

            }

            //关闭设备
            __CloseDevice();
            m_open = false;
        }
        public override int Close()
        {
            try
            {
                // 停止采集关闭设备、关闭流
                __CloseAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }
        public override int OneShot()
        {
            try
            {
                IImageData objIImageData = null;
                double dElapsedtime = 0;
                uint nTimeout = 500;

                //每次发送触发命令之前清空采集输出队列
                //防止库内部缓存帧，造成本次GXGetImage得到的图像是上次发送触发得到的图
                if (null != m_objIGXStream)
                {
                    m_objIGXStream.FlushQueue();
                }
                 m_objIGXFeatureControl.GetEnumFeature("TriggerMode").SetValue("On");

                //选择触发源为软触发
                 m_objIGXFeatureControl.GetEnumFeature("TriggerSource").SetValue("Software");

                 ContinuousShot();
                Thread.Sleep(50);
                //发送软触发命令
                if (null != m_objIGXFeatureControl)
                {
                    isOneShot = true;
                    Thread.Sleep(50);
                    m_objIGXFeatureControl.GetCommandFeature("TriggerSoftware").Execute();

                }
                
                int chkTime = 0;
                while (isOneShot == true && chkTime<50)
                {
                    chkTime++;
                    Thread.Sleep(10);
                }
                //获取图像
                //if (null != m_objIGXStream)
                //{
                    
                //    objIImageData = m_objIGXStream.GetImage(nTimeout);
                //}
                Stop();
                m_objIGXFeatureControl.GetEnumFeature("TriggerMode").SetValue("Off");
              //  m_objGxBitmap.Show(objIImageData);

                if (null != objIImageData)
                {
                    //用完之后释放资源
                    objIImageData.Destroy();
                }
                if (isOneShot == true)
                {
                    return -1;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(CamName+":"+ ex.Message+",请检查相机硬件！");
                return -2;
            }
            return 0;
        }
        public override int ContinuousShot()
        {
            try
            {
                //开启采集流通道
                if (null != m_objIGXStream)
                {
                    //RegisterCaptureCallback第一个参数属于用户自定参数(类型必须为引用
                    //类型)，若用户想用这个参数可以在委托函数中进行使用
                   // m_objIGXStream.RegisterCaptureCallback(this, __OnFrameCallbackFun);
                    m_objIGXStream.StartGrab();
                }

                //发送开采命令
                if (null != m_objIGXFeatureControl)
                {
                    m_objIGXFeatureControl.GetCommandFeature("AcquisitionStart").Execute();
                }
                m_bIsSnap = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }
        public override int Stop()
        {
            try
            {
                //发送停采命令
                if (null != m_objIGXFeatureControl)
                {
                    m_objIGXFeatureControl.GetCommandFeature("AcquisitionStop").Execute();
                }

                //关闭采集流通道
                if (null != m_objIGXStream)
                {
                    m_objIGXStream.StopGrab();
                    //注销采集回调函数
                   // m_objIGXStream.UnregisterCaptureCallback();
                }

                m_bIsSnap = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return 0;
        }

        public override int SetTrigger(bool Trigger)
        {
            if (m_open == false)
            {
                return -1;
            }
            if (Trigger == true)
            {
                m_objIGXFeatureControl.GetEnumFeature("TriggerMode").SetValue("On");
            }
            else
            {
                m_objIGXFeatureControl.GetEnumFeature("TriggerMode").SetValue("Off");
            }
           
            return 0;
        }
        public override int SetTriggerSource(int TriggerSource)
        {
            if (m_open == false)
            {
                return -1;
            }
            if (TriggerSource == 0)
            {
                return -1;
            }
            m_objIGXFeatureControl.GetEnumFeature("TriggerSource").SetValue(TriggerSource.ToString());
            
            return 0;
        }

        public override int SetExposureTime(int ExposureTime)
        {
            if (m_open == false || ExposureTime==0)
            {
                return -1;
            }
            m_objIGXFeatureControl.GetFloatFeature("ExposureTime").SetValue(ExposureTime);
            return 0;
        }
        public override int getExposureTime(out int ExposureTime)
        {
            if (m_open == false)
            {
                ExposureTime = 0;
                return -1;
            }
            ExposureTime = (int)m_objIGXFeatureControl.GetFloatFeature("ExposureTime").GetValue();
            return 0;
        }
        public override int SetGainVal(int GainVal)
        {
            if (m_open == false)
            {
                return -1;
            }
            m_objIGXFeatureControl.GetFloatFeature("Gain").SetValue(GainVal);
            return 0;
        }
        public override int GetGainVal(out int GainVal)
        {
            if (m_open == false)
            {
                GainVal = 0;
                return -1;
            }
            
            GainVal = (int)m_objIGXFeatureControl.GetFloatFeature("Gain").GetValue();
            
            return 0;
        }
       
        
        /* 相机打开回调 */
        private void OnCameraOpen(object sender, EventArgs e)
        {
            base.OnDeviceOpenedEvent();

        }

        /* 相机关闭回调 */
        private void OnCameraClose(object sender, EventArgs e)
        {
            base.OnDeviceClosedEvent();

        }

        /* 相机丢失回调 */
        private void OnConnectLoss(object sender, EventArgs e)
        {

            base.OnDeviceClosedEvent();

        }
        private void OnGrabStarted(Object sender, EventArgs e)
        {
            base.OnGrabbingStartedEvent();

        }

        /// <summary>
        /// 采集事件的委托函数
        /// </summary>
        /// <param name="objUserParam">用户私有参数</param>
        /// <param name="objIFrameData">图像信息对象</param>
        private void __OnFrameCallbackFun(object objUserParam, IFrameData objIFrameData)
        {
            
            try
            {
                m_mutex.WaitOne();
                _IFrameData = objIFrameData;
                lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
                {
                    m_frameList.Add(objIFrameData);
                    if (m_frameList.Count > 5)
                    {
                        m_frameList.RemoveAt(0);
                    }
                }
                m_mutex.ReleaseMutex();
                base.OnImageReadyEvent();
                isOneShot = false;
            }
            catch (Exception)
            {

            }
            
        }
        private GX_VALID_BIT_LIST __GetBestValudBit(GX_PIXEL_FORMAT_ENTRY emPixelFormatEntry)
        {
            GX_VALID_BIT_LIST emValidBits = GX_VALID_BIT_LIST.GX_BIT_0_7;
            switch (emPixelFormatEntry)
            {
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_MONO8:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GR8:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_RG8:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GB8:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_BG8:
                    {
                        emValidBits = GX_VALID_BIT_LIST.GX_BIT_0_7;
                        break;
                    }
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_MONO10:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GR10:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_RG10:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GB10:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_BG10:
                    {
                        emValidBits = GX_VALID_BIT_LIST.GX_BIT_2_9;
                        break;
                    }
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_MONO12:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GR12:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_RG12:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GB12:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_BG12:
                    {
                        emValidBits = GX_VALID_BIT_LIST.GX_BIT_4_11;
                        break;
                    }
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_MONO14:
                    {
                        //暂时没有这样的数据格式待升级
                        break;
                    }
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_MONO16:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GR16:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_RG16:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_GB16:
                case GX_PIXEL_FORMAT_ENTRY.GX_PIXEL_FORMAT_BAYER_BG16:
                    {
                        //暂时没有这样的数据格式待升级
                        break;
                    }
                default:
                    break;
            }
            return emValidBits;
        }
        /// <summary>
        /// 计算宽度所占的字节数
        /// </summary>
        /// <param name="nWidth">图像宽度</param>
        /// <param name="bIsColor">是否是彩色相机</param>
        /// <returns>图像一行所占的字节数</returns>
        private int __GetStride(int nWidth, bool bIsColor)
        {
            return bIsColor ? nWidth * 3 : nWidth;
        }
        /// <summary>
        /// 判断PixelFormat是否为8位
        /// </summary>
        /// <param name="emPixelFormatEntry">图像数据格式</param>
        /// <returns>true为8为数据，false为非8位数据</returns>
        const uint PIXEL_FORMATE_BIT = 0x00FF0000;          ///<用于与当前的数据格式进行与运算得到当前的数据位数
        const uint        GX_PIXEL_8BIT                = 0x00080000;          ///<8位数据图像格式
        private bool __IsPixelFormat8(GX_PIXEL_FORMAT_ENTRY emPixelFormatEntry)
        {
            bool bIsPixelFormat8 = false;
            uint uiPixelFormatEntry = (uint)emPixelFormatEntry;
            if ((uiPixelFormatEntry & PIXEL_FORMATE_BIT) == GX_PIXEL_8BIT)
            {
                bIsPixelFormat8 = true;
            }
            return bIsPixelFormat8;
        }
        public override Image GetLatestImage()
        {
            
            lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
            {
                /* Release all images but the latest. */
                while (m_frameList.Count > 1)
                {
                    m_frameList.RemoveAt(0);
                }
                GX_VALID_BIT_LIST emValidBits = GX_VALID_BIT_LIST.GX_BIT_0_7;
                if (m_frameList.Count > 0) /* If images available. */
                {
                    //获取是否为彩色相机
                    if (m_objIGXDevice.GetRemoteFeatureControl().IsImplemented("PixelColorFilter"))
                    {
                        string strValue = m_objIGXDevice.GetRemoteFeatureControl().GetEnumFeature("PixelColorFilter").GetValue();

                        if ("None" != strValue)
                        {
                            bColor = true;
                        }
                    }
                    int m_nWidth = (int)m_frameList[0].GetWidth();
                    int m_nHeigh = (int)m_frameList[0].GetHeight();
                    if (bColor == true)
                    {
                        
                        emValidBits = __GetBestValudBit(m_frameList[0].GetPixelFormat());
                        IntPtr pBufferColor = m_frameList[0].ConvertToRGB24(emValidBits, GX_BAYER_CONVERT_TYPE_LIST.GX_RAW2RGB_NEIGHBOUR, false);
                        byte[] m_byColorBuffer = new byte[__GetStride(m_nWidth, bColor) * m_nHeigh];
                        Marshal.Copy(pBufferColor, m_byColorBuffer, 0, __GetStride(m_nWidth, true) * m_nHeigh);


                        Image mImage = new Image(m_nWidth, m_nHeigh, m_byColorBuffer, true);
                        //Marshal.Release(pData);
                       // Marshal.FreeHGlobal(pBufferColor);
                        /* 主动调用回收垃圾 */
                        GC.Collect();
                        return mImage;
                    }
                    else
                    {
                        IntPtr pBufferMono = IntPtr.Zero;
                        if (__IsPixelFormat8(m_frameList[0].GetPixelFormat()))
                        {
                            pBufferMono = m_frameList[0].GetBuffer();
                        }
                        else
                        {
                            pBufferMono = m_frameList[0].ConvertToRaw8(emValidBits);
                        }
                        byte[] m_byMonoBuffer = new byte[__GetStride(m_nWidth, bColor) * m_nHeigh];
                        Marshal.Copy(pBufferMono, m_byMonoBuffer, 0, __GetStride(m_nWidth, bColor) * m_nHeigh);
                        Image mImage = new Image(m_nWidth, m_nHeigh, m_byMonoBuffer, false);
                        //Marshal.Release(pData);
                        //Marshal.FreeHGlobal(pBufferMono);
                        /* 主动调用回收垃圾 */
                        GC.Collect();
                        return mImage;
                    }
                    
                }
            }
            return null; /* No image available. */
        }
        public override Bitmap GetLatestBitmap()
        {

            lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
            {
                /* Release all images but the latest. */
                while (m_frameList.Count > 1)
                {
                    m_frameList.RemoveAt(0);
                }
                if (m_frameList.Count > 0) /* If images available. */
                {
                   
                    //return m_frameList[0].ToBitmap(true);
                }
            }
            return null; /* No image available. */
        }

        

    }
}
