﻿#define Err_Hand

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using HalconDotNet;
using System.IO;
using System.Xml.Serialization;
using Apk_HP_Application.程序文件夹分类._0全局全量;
using Apk_HP_Application.程序文件夹分类._4装盘及机器人控制;
using Apk_HP_Application.通讯及接口;
using ZHToolBox.HalconVision;
using static ZHToolBox.HalconVision.HDVisionTypes;
using APK_HP.MES.HPSql;

namespace Apk_HP_Application.程序文件夹分类._4装盘及机器人控制
{
    public partial class frm_RobotControl : DevExpress.XtraEditors.XtraForm
    {
        [XmlIgnoreAttribute]
        SynchronizationContext m_SyncContext = null;

        [XmlIgnoreAttribute]
        Thread safeMonitor = null;

       [XmlIgnoreAttribute]
        object ObjMove = new object();

        [XmlIgnoreAttribute]
        bool bStopFlag = false;

        [XmlIgnoreAttribute]
        Thread threadOfRobot = null;

        [XmlIgnoreAttribute]
        Thread threadCamera = null;

        [XmlIgnoreAttribute]
        double[] cust_row = new double[10];

        [XmlIgnoreAttribute]
        object ojectResupply = new object();

        [XmlIgnoreAttribute]
        object ojectLoadCell = new object();

        [XmlIgnoreAttribute]
        object ojectUnloadAction = new object();

        [XmlIgnoreAttribute]
        public string strVistionTest2 = "";

        public class ParameterSaveFile
        {
            public DeformableInspector VisionTest1_1 { get; set; }

            public ShapeModelInspector VisionTest1_2 { get; set; }

            public AreaInspector VisionTest1_3 { get; set; }

            public ShapeModelInspector VisionTest1_4 { get; set; }

            public ShapeModelInspector VisionTest1_5 { get; set; }

            public DeformableInspector VisionTest2_1 { get; set; }

            public ShapeModelInspector VisionTest2_2 { get; set; }

            public DeformableInspector VisionTest2_3 { get; set; }

            public AreaInspector VisionTest2_4 { get; set; }

            public ShapeModelInspector VisionTest2_5 { get; set; }

            public short Cfg_Total { get; set; }
            public short Cfg_CountTray { get; set; }
            public short Cfg_CountTrayBK { get; set; }
            public short Cfg_Remaind { get; set; }
            public short Cfg_Row { get; set; }
            public short Cfg_Col { get; set; }
            public short CurrentCountTray { get; set; }
            public int TotalProductCount { get; set; }
            public short TotalProductCountTray { get; set; }
            public short TotalProductCountTrayLast { get; set; }
            public short CurrentTotalCountTray { get; set; }

            public short Total_FailCount { get; set; }
            public int Total_CheckedPassed { get; set; }
            public int Total_PassedAndPackedCount { get; set; }
            public short CurrentCountOfLoadTray { get; set; }
            public short CurrentCountOfBuffTray { get; set; }

            public short MarkFace { get; set; }

            public int Camera1_Exposure { get; set; }
            public int Camera1_Contrast { get; set; }
            public int Camera2_Exposure { get; set; }
            public int Camera2_Contrast { get; set; }

            public int lightValue_Channel1 { get; set; }
            public int lightValue_Channel2 { get; set; }
            public int lightValue_Channel3 { get; set; }
            public int lightValue_Channel4 { get; set; }

            public double DstPerPulse { get; set; }

            public string MarkText { get; set; }
            public string MarkTextLast { get; set; }
            public ParameterSaveFile()
            {
                VisionTest1_1 = new DeformableInspector();
                VisionTest1_2 = new ShapeModelInspector();
                VisionTest1_3 = new AreaInspector();
                VisionTest1_4 = new ShapeModelInspector();
                VisionTest1_5 = new ShapeModelInspector();
                VisionTest1_3.Enabled = false;
                VisionTest1_4.Enabled = false;
                VisionTest1_5.Enabled = false;

                VisionTest2_1 = new DeformableInspector();
                VisionTest2_2 = new ShapeModelInspector();
                VisionTest2_3 = new DeformableInspector();
                VisionTest2_4 = new AreaInspector();
                VisionTest2_5 = new ShapeModelInspector();
                VisionTest2_3.Enabled = false;
                VisionTest2_4.Enabled = false;
                VisionTest2_5.Enabled = false;

                Cfg_Total = 0;
                Cfg_CountTray = 0;
                Cfg_Remaind = 0;
                Cfg_Row = 0;
                Cfg_Col = 0;

                Total_CheckedPassed = 0;

                Total_FailCount = 0;
                Total_PassedAndPackedCount = 0;

                Camera1_Exposure = 25;
                Camera1_Contrast = 50;

                Camera2_Exposure = 57;
                Camera2_Contrast = 60;

                MarkFace = 1;
                DstPerPulse = 8.7 / 30000.00;

                MarkText = "";
                MarkTextLast = "";

                lightValue_Channel1 = 128;
                lightValue_Channel2 = 128;
                lightValue_Channel3 = 128;
                lightValue_Channel4 = 128;
            }

            public void Dispose()
            {
                try
                {
                    if (VisionTest1_1 != null)
                        VisionTest1_1.Dispose();

                    if (VisionTest1_2 != null)
                        VisionTest1_2.Dispose();

                    if (VisionTest1_3 != null)
                        VisionTest1_3.Dispose();

                    if (VisionTest1_4 != null)
                        VisionTest1_4.Dispose();

                    if (VisionTest1_5 != null)
                        VisionTest1_5.Dispose();

                    if (VisionTest2_1 != null)
                        VisionTest2_1.Dispose();

                    if (VisionTest2_2 != null)
                        VisionTest2_2.Dispose();

                    if (VisionTest2_3 != null)
                        VisionTest2_3.Dispose();

                    if (VisionTest2_4 != null)
                        VisionTest2_4.Dispose();
                    
                    if (VisionTest2_5 != null)
                        VisionTest2_5.Dispose();

                    VisionTest1_1 = null;
                    VisionTest1_2 = null;
                    VisionTest1_3 = null;
                    VisionTest1_4 = null;
                    VisionTest1_5 = null;

                    VisionTest2_1 = null;
                    VisionTest2_2 = null;
                    VisionTest2_3 = null;
                    VisionTest2_4 = null;
                    VisionTest2_5 = null;

                }
                catch(Exception ex)
                {

                }
            }

        }

        public ParameterSaveFile ParameterAllObj = new ParameterSaveFile();
        bool bSystemStarted = false;

        public frm_RobotControl()
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw |
                               ControlStyles.OptimizedDoubleBuffer |
                               ControlStyles.AllPaintingInWmPaint, true);
            this.UpdateStyles();

            m_SyncContext = SynchronizationContext.Current;
            HPRobotCtrl.frm_MainPanel = this;

            gridRobotWayList.TopLeftHeaderCell.Value = "运行路径";
            viewFlashResult.RowHeadersWidth = 60;
            for (int nIndex = 0; nIndex < 10; nIndex++)
            {
                int nRow = viewFlashResult.Rows.Add();
                viewFlashResult.Rows[nRow].HeaderCell.Value = $"{(nIndex + 1).ToString()}";
                viewFlashResult.Rows[nRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            try
            {
                //初始化硬件
                if (!HPRobotCtrl.InitializeHardware())
                {
                    MessageBox.Show("初始化机器人工位失败", "错误", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                ParameterAllObj = (ParameterSaveFile)ZHToolBox.General.ZHToolBoxGeneral.DeserializeObjFromFile<ParameterSaveFile>($"ConfigFile\\{HPglobal.m_strModelName}\\HPRobotCtrObjParameters.xml");
                propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_1;
                propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_1;

                #region>>>>>>>>更新光源参数
                trackLightSource_1.Value = ParameterAllObj.lightValue_Channel1;
                trackLightSource_2.Value = ParameterAllObj.lightValue_Channel2;
                trackLightSource_3.Value = ParameterAllObj.lightValue_Channel3;
                trackLightSource_4.Value = ParameterAllObj.lightValue_Channel4;

                HPRobotCtrl.lightSource.LightSource_SetValue(1, (short)spedLS_Value_1.Value);
                HPRobotCtrl.lightSource.LightSource_SetValue(2, (short)spedLS_Value_2.Value);
                HPRobotCtrl.lightSource.LightSource_SetValue(3, (short)spedLS_Value_3.Value);
                HPRobotCtrl.lightSource.LightSource_SetValue(4, (short)spedLS_Value_4.Value);
                #endregion

                #region>>>>>>>>初始化相机面板
                Size size = panelControl1.Size;
                HPRobotCtrl.Camera1.TopLevel = false;
                HPRobotCtrl.Camera1.FormBorderStyle = FormBorderStyle.None;
                panelControl1.Controls.Add(HPRobotCtrl.Camera1);
                HPRobotCtrl.Camera1.Size = size;
                HPRobotCtrl.Camera1.Show();
                HPRobotCtrl.Camera1.SendToBack();
                HPRobotCtrl.Camera1.BringToFront();
                HPRobotCtrl.Camera1.hAcqDriverName = "HuaRay"; // "HuaRay", "GigEVision";
                HPRobotCtrl.Camera1.cameraName = "cam0";
                HPRobotCtrl.Camera1.nPort = 0;

                size = panelControl2.Size;
                HPRobotCtrl.Camera2.TopLevel = false;
                HPRobotCtrl.Camera2.FormBorderStyle = FormBorderStyle.None;
                panelControl2.Controls.Add(HPRobotCtrl.Camera2);
                HPRobotCtrl.Camera2.Size = size;
                HPRobotCtrl.Camera2.Show();
                HPRobotCtrl.Camera2.SendToBack();
                HPRobotCtrl.Camera2.BringToFront();
                HPRobotCtrl.Camera2.hAcqDriverName = "HuaRay"; // "HuaRay", "GigEVision";
                HPRobotCtrl.Camera2.cameraName = "cam1";
                HPRobotCtrl.Camera2.nPort = 1;

                HPRobotCtrl.Camera1.OpenCamera();
                HPRobotCtrl.Camera2.OpenCamera();

                #endregion

                #region>>>>>>>>初始化相机面板（3， 4)
                size = panelControl3.Size;
                HPRobotCtrl.Camera3.TopLevel = false;
                HPRobotCtrl.Camera3.FormBorderStyle = FormBorderStyle.None;
                panelControl3.Controls.Add(HPRobotCtrl.Camera3);
                HPRobotCtrl.Camera3.Size = size;
                HPRobotCtrl.Camera3.Show();
                HPRobotCtrl.Camera3.SendToBack();
                HPRobotCtrl.Camera3.BringToFront();

                size = panelControl4.Size;
                HPRobotCtrl.Camera4.TopLevel = false;
                HPRobotCtrl.Camera4.FormBorderStyle = FormBorderStyle.None;
                panelControl4.Controls.Add(HPRobotCtrl.Camera4);
                HPRobotCtrl.Camera4.Size = size;
                HPRobotCtrl.Camera4.Show();
                HPRobotCtrl.Camera4.SendToBack();
                HPRobotCtrl.Camera4.BringToFront();
                #endregion

                #region>>>>>>>> 启动IO线程 
                HPRobotCtrl.bQuitHPRobot = false;
                if (HPRobotCtrl.threadPCRequire == null)
                {
                    HPRobotCtrl.threadPCRequire = new Thread(new ParameterizedThreadStart(thread_ForInvokeContrlRequirement));
                    HPRobotCtrl.threadPCRequire.Start(null);
                }
                #endregion

                #region>>>>>>>> 启动监控线程 
                if (safeMonitor == null)
                {
                    safeMonitor = new Thread(new ParameterizedThreadStart(thread_ForSafeIOMonitor));
                    safeMonitor.Start("1");
                }
                #endregion

                ChangeType("");

                #region>>>>>>>> 启动工单线程 (一定要在预先转换型号 ChangeType() 之后)
                if (HPRobotCtrl.threadOrderRequire == null)
                {
                    HPRobotCtrl.threadOrderRequire = new Thread(new ParameterizedThreadStart(thread_ForAutoChangeOrder));
                    HPRobotCtrl.threadOrderRequire.Start(null);
                }
                #endregion

                bSystemStarted = true;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_RobotControl() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void thread_ForSafeIOMonitor(object wsarg)
        {
            string MonitorAddress = "D1007";
            bool bThreadSuspend = false;
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(200);

                try
                {
                    REDO:
                    OperateResult<short> nTrigger = HPRobotCtrl.PLC_Unload.ReadInt16(MonitorAddress);
                    if (nTrigger.IsSuccess && nTrigger.Content != 0)
                    {
                        #region>>>>>>>>初始化相机面板
                        if (threadOfRobot != null)
                        {
                            threadOfRobot.Suspend();
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>线程已暂停", Color.Red, 9);
                        }

                        if (threadCamera != null)
                        {
                            threadCamera.Suspend();
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>线程已暂停", Color.Red, 9);
                        }

                        bThreadSuspend = true;

                        Application.DoEvents();
                        #endregion

                        #region>>>>>>>>初始化相机面板
                        string errorMsg = $"检测到设备出现错误或异常(D1007 非0)！\r\n\r\n设备已暂停，请在修复之后点击  继续 \r\n\r\n";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        {
                            if (threadOfRobot != null)
                            {
                                threadOfRobot.Resume();
                                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>线程已启动", Color.Blue, 9);
                            }

                            if (threadCamera != null)
                            {
                                threadCamera.Resume();
                                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>线程已启动", Color.Blue, 9);
                            }

                            bThreadSuspend = false;                          
                        }
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        {
                           
                        }

                        #endregion
                    }
                    else if (nTrigger.IsSuccess && nTrigger.Content  == 0 && bThreadSuspend == true)
                    {
                        if (threadOfRobot != null)
                        {
                            threadOfRobot.Resume();
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>线程已启动", Color.Blue, 9);
                        }

                        if (threadCamera != null)
                        {
                            threadCamera.Resume();
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>线程已启动", Color.Blue, 9);
                        }

                        bThreadSuspend = false;
                    }
                }
                catch(Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_ForSafeIOMonitor 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        public void UpateCustRowsDst()
        {
            try
            {
                cust_row[0] = 0.00;
                cust_row[1] = (double)spn_Cust_Row1.Value;
                cust_row[2] = (double)spn_Cust_Row2.Value;
                cust_row[3] = (double)spn_Cust_Row3.Value;
                cust_row[4] = (double)spn_Cust_Row4.Value;
                cust_row[5] = (double)spn_Cust_Row5.Value;
                cust_row[6] = (double)spn_Cust_Row6.Value;
                cust_row[7] = (double)spn_Cust_Row7.Value;
                cust_row[8] = (double)spn_Cust_Row8.Value;
                cust_row[9] = (double)spn_Cust_Row9.Value;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("timer_Display_ExeStatus_Tick() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

        }

        private void timer_Display_ExeStatus_Tick(object sender, EventArgs e)
        {
            try
            {
                #region>>>>>>实时更新生产数据
                txtPCBSN.Text = HPRobotCtrl.strVirtrualPCBSN;
                txtTrayID.Text = HPRobotCtrl.strTrayIDName;

                txtPCBSN2.Text = HPRobotCtrl.strVirtrualPCBSN2;
                txtTrayID2.Text = HPRobotCtrl.strTrayIDName2;

                UnloadCfg_Total.Value = ParameterAllObj.Cfg_Total;
                UnloadCfg_CountTray.Value = ParameterAllObj.Cfg_CountTray;
                UnloadCfg_CountTrayBK.Value = ParameterAllObj.Cfg_CountTrayBK;
                UnloadCfg_Remaind.Value = ParameterAllObj.Cfg_Remaind;
                UnloadCfg_Row.Value = ParameterAllObj.Cfg_Row;
                UnloadCfg_Col.Value = ParameterAllObj.Cfg_Col;
                CurrentCountTray.Value = ParameterAllObj.CurrentCountTray;
                UnloadCfg_TotalProduct.Value = ParameterAllObj.TotalProductCount;
                TotalTrayOfLineOut.Value = ParameterAllObj.CurrentTotalCountTray;
                TotalTrayOfLineOutPCS.Value = TotalTrayOfLineOut.Value * UnloadCfg_Row.Value * UnloadCfg_Col.Value;
                spedTotalFailedCount.Value = ParameterAllObj.Total_FailCount;

                sped生产总盘数.Value = ParameterAllObj.TotalProductCountTray;
                生产最后打包总盘数.Value = ParameterAllObj.TotalProductCountTrayLast;

                Point[] allPoint = GetInPosArryOfTray(true);
                ParameterAllObj.CurrentCountOfLoadTray = (short)(allPoint == null ? 0 : allPoint.Length);

                allPoint = GetInPosArryOfTray(false);
                ParameterAllObj.CurrentCountOfBuffTray = (short)(allPoint == null ? 0 : allPoint.Length);

                speCountOfLoadTray.Value = ParameterAllObj.CurrentCountOfLoadTray;
                speCountOfBuffTray.Value = ParameterAllObj.CurrentCountOfBuffTray;

                TotalLineOutAll.Value = ParameterAllObj.Total_PassedAndPackedCount;

                int PlanTotalCount = ParameterAllObj.TotalProductCount;
                int ActuralProductedTotalCount = ParameterAllObj.Total_PassedAndPackedCount;
                int TrayCount = ParameterAllObj.Cfg_Row * ParameterAllObj.Cfg_Col;
                if (PlanTotalCount > 0 &&
                    ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 1 &&
                    ((Math.Abs(PlanTotalCount - ActuralProductedTotalCount) <= TrayCount) || PlanTotalCount <= TrayCount)) //如果果当前为尾料的最后一盘, 等所有盘做完了再放盘
                {
                    btm进入尾料模式.Checked = true;
                    HPRobotCtrl.PLC_Unload.Write("D1056", (short) 1 );
                }
                else if(PlanTotalCount <= 0 && btm进入尾料模式.Checked  == true)
                {
                    btm进入尾料模式.Checked = false;
                    HPRobotCtrl.PLC_Unload.Write("D1056", (short) 0 );
                }


                if (ParameterAllObj.MarkFace >= 5 &&
                    (chkLaserMarkTest_Up.Checked || chkLaserMarkTest_Down.Checked))
                {
                    chkLaserMarkTest_Up.Checked = false;
                    chkLaserMarkTest_Down.Checked = false;
                }
                else if (ParameterAllObj.MarkFace == 0 && !chkLaserMarkTest_Up.Checked)
                {
                    chkLaserMarkTest_Up.Checked = true;
                    chkLaserMarkTest_Down.Checked = false;
                }
                else if (ParameterAllObj.MarkFace == 1 && !chkLaserMarkTest_Down.Checked)
                {
                    chkLaserMarkTest_Down.Checked = true;
                    chkLaserMarkTest_Up.Checked = false;
                }
                #endregion

                #region>>>>>>设置统计数据
                txt已识别的好品数量.Text = ParameterAllObj.Total_CheckedPassed.ToString();

                txtPassedCamera1.Text = HPRobotCtrl.PassedCamera1.ToString();
                txtFailedCamera1.Text = HPRobotCtrl.FailedCamera1.ToString();
                if (HPRobotCtrl.PassedCamera1 + HPRobotCtrl.FailedCamera1 > 0)
                {
                    double uph = (double)HPRobotCtrl.PassedCamera1 / (HPRobotCtrl.PassedCamera1 + HPRobotCtrl.FailedCamera1);
                    txtUPHCamera1.Text = (uph * 100).ToString("F2") + "%";
                }
                else
                    txtUPHCamera1.Text = "0.00%";

                txtPassedCamera2.Text = HPRobotCtrl.PassedCamera2.ToString();
                txtFailedCamera2.Text = HPRobotCtrl.FailedCamera2.ToString();
                if (HPRobotCtrl.PassedCamera2 + HPRobotCtrl.FailedCamera2 > 0)
                {
                    double uph = (double)HPRobotCtrl.PassedCamera2 / (HPRobotCtrl.PassedCamera2 + HPRobotCtrl.FailedCamera2);
                    txtUPHCamera2.Text = (uph * 100).ToString("F2") + "%";
                }
                else
                    txtUPHCamera2.Text = "0.00%";

                txtTotalPassed.Text = HPRobotCtrl.PassedTotal.ToString();
                txtTotalFailed.Text = HPRobotCtrl.FailedTotal.ToString();
                if (HPRobotCtrl.PassedTotal + HPRobotCtrl.FailedTotal > 0)
                {
                    double uph = (double)HPRobotCtrl.PassedTotal / (HPRobotCtrl.PassedTotal + HPRobotCtrl.FailedTotal);
                    txtTotalUPH.Text = (uph * 100).ToString("F2") + "%";
                }
                else
                    txtTotalUPH.Text = "0.00%";

                #endregion
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("timer_Display_ExeStatus_Tick() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void DisplayObjData()
        {
            try
            {
                #region>>>>>>Unload PLC
                if (!HPRobotCtrl.bIsPLCUnloadConnected && LEDUnloadPLCState.LanternBackground != Color.Gray)
                    LEDUnloadPLCState.LanternBackground = Color.Gray;
                else
                    LEDUnloadPLCState.LanternBackground = (LEDUnloadPLCState.LanternBackground == Color.Green ? Color.Yellow : Color.Green);
                #endregion

                #region>>>>>>Robot 机器人连接状态
                if (!HPRobotCtrl.ObjRobot.m_bIsRobotConnected)
                    LEDRobot.LanternBackground = Color.Gray;
                else
                    LEDRobot.LanternBackground = (LEDRobot.LanternBackground == Color.Green ? Color.Yellow : Color.Green);
                #endregion

                #region>>>>>>ModbusTCP Server
                if (HPRobotCtrl.modebusTCPServer.OnlineCount <= 0)
                    LEDModbusServer_Robot.LanternBackground = Color.Gray;
                else
                    LEDModbusServer_Robot.LanternBackground = (LEDModbusServer_Robot.LanternBackground == Color.Green ? Color.Yellow : Color.Green);
                #endregion

                #region>>>>>>机器人I/O输入状态
                if (HPRobotCtrl.ObjRobot == null || !HPRobotCtrl.ObjRobot.m_bIsRobotConnected || !HPRobotCtrl.ObjRobot.bMonitorIOStatus)
                {
                    userLanternIO.LanternBackground = Color.Gray;
                }
                else
                {
                    userLanternIO.LanternBackground = (userLanternIO.LanternBackground == Color.Green ? Color.Yellow : Color.Green);

                    #region>>>>>>IO 输入的状态

                    HslCommunication.Controls.UserLantern[] arryLEDs = { LED_In0, LED_In1, LED_In2, LED_In3, LED_In4, LED_In5, LED_In6, LED_In7,
                                                                                                                 LED_In8, LED_In9, LED_In10, LED_In11, LED_In12, LED_In13, LED_In14, LED_In15,
                                                                                                                LED_In16, LED_In17, LED_In18, LED_In19, LED_In20, LED_In21, LED_In22, LED_In23,
                                                                                                                LED_In24, LED_In25, LED_In26, LED_In27, LED_In28, LED_In29, LED_In30, LED_In31};

                    UInt32 dwShift = 0x01;
                    for (int nIndex = 0; nIndex < 32; nIndex++)
                    {
                        UInt32 nValue = HPRobotCtrl.ObjRobot.IOInputState & (dwShift << nIndex);
                        arryLEDs[nIndex].LanternBackground = (nValue != 0 ? Color.Red : Color.Green);
                    }

                    #endregion
                }
                #endregion

                #region>>>>>>IO 输入的状态
                if (HPRobotCtrl.D1300Values != null && HPRobotCtrl.D1300Values.IsSuccess && HPRobotCtrl.D1300Values.Content.Length > 10)
                {
                    short[] newValues = new short[HPRobotCtrl.D1300Values.Content.Length];
                    HPRobotCtrl.D1300Values.Content.CopyTo(newValues, 0);
                    txtD1000.Text = newValues[0].ToString();
                    txtD1001.Text = newValues[1].ToString();
                    txtD1002.Text = newValues[2].ToString();
                    txtD1003.Text = newValues[3].ToString();
                    txtD1004.Text = newValues[4].ToString();
                    txtD1005.Text = newValues[5].ToString();
                    txtD1006.Text = newValues[6].ToString();
                    txtD1007.Text = newValues[7].ToString();
                    txtD1008.Text = newValues[8].ToString();
                    txtD1009.Text = newValues[9].ToString();
                    txtD1010.Text = newValues[10].ToString();
                    txtD1011.Text = newValues[11].ToString();
                    txtD1012.Text = newValues[12].ToString();
                    txtD1013.Text = newValues[13].ToString();
                }
                else
                {
                    txtD1000.Text = "Error";
                    txtD1001.Text = "Error";
                    txtD1002.Text = "Error";
                    txtD1003.Text = "Error";
                    txtD1004.Text = "Error";
                    txtD1005.Text = "Error";
                    txtD1006.Text = "Error";
                    txtD1007.Text = "Error";
                    txtD1008.Text = "Error";
                    txtD1009.Text = "Error";
                    txtD1010.Text = "Error";
                    txtD1011.Text = "Error";
                    txtD1012.Text = "Error";
                    txtD1013.Text = "Error";
                }

                #endregion

                #region>>>>>>取出接受与发出的命
                lock (HPRobotCtrl.m_queRobotLoadUnload)
                {
                    HPglobal.outMessage(richTextRobot, HPRobotCtrl.m_queRobotLoadUnload);
                }

                lock (HPRobotCtrl.m_queCamera1)
                {
                    HPglobal.outMessage(richTextCamera1, HPRobotCtrl.m_queCamera1);
                }

                lock (HPRobotCtrl.m_queCamera2)
                {
                    HPglobal.outMessage(richTextCamera2, HPRobotCtrl.m_queCamera2);
                }

                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format(" DisplayObjData() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void timer_IO_Tick(object sender, EventArgs e)
        {
            try
            {
                DisplayObjData();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("GTSBox_ProcessReq_LockOK 异常\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void form_Closing(object sender, FormClosingEventArgs e)
        {
            try
            {
                HPRobotCtrl.ObjRobot.DisConnect();
                timer_IO.Enabled = false;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("form_Closing 异常\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

        }

        public bool[] SetCamera1Result()
        {
            bool[] bNewResult = new bool[ParameterAllObj.VisionTest1_1.CheckResult.Length];

            try
            {
                short nCloCount = 0;
                short nRowCount = 0;

                this.Invoke(new Action(() =>
                {
                    nCloCount = (short) speCols.Value; 
                    nRowCount = (short) speRows.Value;
                }));

                if (ParameterAllObj.VisionTest1_1.CheckResult.Length < nCloCount * nRowCount ||
                   nRowCount > 10 || nCloCount > 10 ||
                   nRowCount <= 0 || nCloCount <= 0)
                {
                    this.Invoke(new Action(() => MessageBox.Show(this, "SetCamera1Result 错误: \r\n当前相机判断区域的个数与托盘区域的个数不相等，或都已超过10行10例", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error)));
                    return bNewResult;
                }

                #region>>>>>>>>>>更新测试值
                for (int nIndexRow = 0; nIndexRow < 10; nIndexRow++)
                {
                    for (int nIndexCol = 0; nIndexCol < 10; nIndexCol++)
                    {
                        HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol] = false;

                        if (nIndexRow < nRowCount && nIndexCol < nCloCount)
                        {
                            int currentIndex = (int)(nIndexRow * nCloCount + nIndexCol);
                            if(强制camera1结果.Checked)
                            {
                                bNewResult[currentIndex] = HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol] = true;
                            }
                            else
                            {
                                bNewResult[currentIndex] = HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol] = ParameterAllObj.VisionTest1_1.CheckResult[currentIndex].ResultStatus;

                                #region>>>>>>>>>>合并2的结果
                                if (ParameterAllObj.VisionTest1_2.Enabled)
                                {
                                    HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol] &= ParameterAllObj.VisionTest1_2.CheckResult[currentIndex].ResultStatus;
                                    bNewResult[currentIndex] = HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol];
                                }
                                #endregion

                                #region>>>>>>>>>>合并3的结果
                                if (ParameterAllObj.VisionTest1_3.Enabled)
                                {
                                    HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol] &= ParameterAllObj.VisionTest1_3.CheckResult[currentIndex].ResultStatus;
                                    bNewResult[currentIndex] = HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol];
                                }
                                #endregion

                                #region>>>>>>>>>>合并4的结果
                                if (ParameterAllObj.VisionTest1_4.Enabled)
                                {
                                    HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol] &= ParameterAllObj.VisionTest1_4.CheckResult[currentIndex].ResultStatus;
                                    bNewResult[currentIndex] = HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol];
                                }
                                #endregion

                                #region>>>>>>>>>>合并5的结果
                                if (ParameterAllObj.VisionTest1_5.Enabled)
                                {
                                    HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol] &= ParameterAllObj.VisionTest1_5.CheckResult[currentIndex].ResultStatus;
                                    bNewResult[currentIndex] = HPRobotCtrl.bArryCamera1_Result[nIndexRow, nIndexCol];
                                }
                                #endregion
                            }
                        }
                    }
                }
                #endregion

                return bNewResult;  
               
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SetCamera1Result() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return bNewResult;
            }
        }

        public void PushCamera1ProdcutInPosInfo(string trayID)
        {
            try
            {
                short nCloCount = 0;
                short nRowCount = 0;

                this.Invoke(new Action(() =>
                {
                    nCloCount = (short)speCols.Value;
                    nRowCount = (short)speRows.Value;
                }));

                lock (HPRobotCtrl.m_queVistion1ProductInPos)
                {
                    //如果有相同的就删除原来的
                    List<HPRobotCtrl.Visition1IsProductInPos> resultList = HPRobotCtrl.m_queVistion1ProductInPos.ToList<HPRobotCtrl.Visition1IsProductInPos>();
                    for (int nIndex = 0; nIndex < resultList.Count; nIndex++)
                    {
                        if (trayID == resultList[nIndex].trayID)
                        {
                            resultList.RemoveAt(nIndex);

                            //重新设定数据队例
                            HPRobotCtrl.m_queVistion1ProductInPos.Clear();
                            HPRobotCtrl.m_queVistion1ProductInPos = new Queue<HPRobotCtrl.Visition1IsProductInPos>(resultList);
                        }
                    }

                    //增加
                    HPRobotCtrl.Visition1IsProductInPos flagInPos = new HPRobotCtrl.Visition1IsProductInPos();
                    flagInPos.trayID = trayID;
                    flagInPos.VisitionTestResults = new bool[10, 10];

                    for (int nRow = 0; nRow < 10; nRow++)
                    {
                        for (int nCol = 0; nCol < 10; nCol++)
                        {
                            if (ParameterAllObj.VisionTest1_5.Enabled == false ||
                                ParameterAllObj.VisionTest1_5.CheckResult == null ||
                                ParameterAllObj.VisionTest1_5.CheckResult.Length < nCloCount * nRowCount)
                                flagInPos.VisitionTestResults[nRow, nCol] = true;
                            else
                            {
                                if (nRow < nRowCount && nCol < nCloCount)
                                {
                                    int currentIndex = (int)(nRow * nCloCount + nCol);
                                    flagInPos.VisitionTestResults[nRow, nCol] = ParameterAllObj.VisionTest1_5.CheckResult[currentIndex].ResultStatus;
                                }
                            }
                        }
                    }

                    HPRobotCtrl.m_queVistion1ProductInPos.Enqueue(flagInPos);
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Copy1Cam1Result_To_Cam2Result  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }


        public bool[] SetCamera2Result(bool[] camera1ResultRow, short[] flashResultRow, ref Int32 ResultFlag)
        {
            try
            {
                bool[] bNewResult = new bool[10];

                short nCountOfCatch = 0;
                short nCloCount = 0;
                short nRowCount = 0;

                this.Invoke(new Action(() =>
                {
                    nCountOfCatch = (short) speCoutOfCatchUsed.Value;

                    nCloCount = (short) speCols.Value;
                    nRowCount = (short) speRows.Value;
                }));

                if (ParameterAllObj.VisionTest2_1.CheckResult.Length < nCountOfCatch ||
                   nCountOfCatch <= 0 || 
                   nCountOfCatch > 10)
                {
                    HPglobal.RecordeAllMessage("SetCamera2Result 错误: \r\n当前相机判断区域的个数与托盘区域的个数不相等，或都已超过10行10例", true, false, "");
                    return null;
                }

                for (int nIndexRow = 0; nIndexRow < 10; nIndexRow++)
                {
                    bNewResult[nIndexRow] = false;

                    //得到结果
                    if (nIndexRow < nCountOfCatch)
                    {
                        if (强制camera2结果.Checked)
                        {
                            bool bResult = false;
                            bResult = bNewResult[nIndexRow] = true;

                            ResultFlag |= (bResult ? ((int)0x01 << nIndexRow) : ((int)0x00 << nIndexRow));

                            //设置结果string
                            strVistionTest2 += (bResult ? "1;" : "0;");
                        }
                        else
                        {
                            bool bResult = false;

                            //合并相机1测试1结果
                            if (camera1ResultRow != null && camera1ResultRow.Length >= nCountOfCatch)
                                bResult = bNewResult[nIndexRow] = ParameterAllObj.VisionTest2_1.CheckResult[nIndexRow].ResultStatus & camera1ResultRow[nIndexRow];
                            else
                                bResult = bNewResult[nIndexRow] = ParameterAllObj.VisionTest2_1.CheckResult[nIndexRow].ResultStatus;

                            //合并相机2测试2结果
                            if (chkLaserMarkTest_Down.Checked && ParameterAllObj.VisionTest2_2.Enabled && ParameterAllObj.VisionTest2_2.CheckResult.Length >= nCountOfCatch)
                            {
                                bResult = bNewResult[nIndexRow] = (bResult & ParameterAllObj.VisionTest2_2.CheckResult[nIndexRow].ResultStatus);
                            }

                            //合并相机2测试3结果
                            if (chkLaserMarkTest_Down.Checked && ParameterAllObj.VisionTest2_3.Enabled && ParameterAllObj.VisionTest2_3.CheckResult.Length >= nCountOfCatch)
                            {
                                bResult = bNewResult[nIndexRow] = (bResult & ParameterAllObj.VisionTest2_3.CheckResult[nIndexRow].ResultStatus);
                            }

                            //合并相机2测试4结果
                            if (chkLaserMarkTest_Down.Checked && ParameterAllObj.VisionTest2_4.Enabled && ParameterAllObj.VisionTest2_4.CheckResult.Length >= nCountOfCatch)
                            {
                                bResult = bNewResult[nIndexRow] = (bResult & ParameterAllObj.VisionTest2_4.CheckResult[nIndexRow].ResultStatus);
                            }

                            //合并相机2测试5结果
                            if (chkLaserMarkTest_Down.Checked && ParameterAllObj.VisionTest2_5.Enabled && ParameterAllObj.VisionTest2_5.CheckResult.Length >= nCountOfCatch)
                            {
                                bResult = bNewResult[nIndexRow] = (bResult & ParameterAllObj.VisionTest2_5.CheckResult[nIndexRow].ResultStatus);
                            }

                            //设置结果string
                            strVistionTest2 += (bResult ? "1;" : "0;");

                            //合并烧录结果
                            if (chkCareFlashResult.Checked && flashResultRow != null && flashResultRow.Length >= nCountOfCatch)
                                bResult = bNewResult[nIndexRow] = (bResult & (flashResultRow[nIndexRow] == 0x00 ? true : false));

                            ResultFlag |= (bResult ? ((int)0x01 << nIndexRow) : ((int)0x00 << nIndexRow));
                        }
                    }
                }

                return bNewResult;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SetCamera2Result  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return null;
            }
         
        }

        public bool VisionTest1_Execute(HalconDotNet.HImage[] image)
        {
            try
            {
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>VisionTest1_Execute 开始进行VistionTest1.....", Color.Magenta, 9);

                #region>>>>>>>>初始化每个相机的结果
                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest1_1.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest1_1.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest1_2.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest1_2.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest1_3.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest1_3.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest1_4.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest1_4.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest1_5.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest1_5.CheckResult[nIndexRow].ResultStatus = false;
                }
                #endregion

                #region>>>>>>>>执行方法1
                if (!ParameterAllObj.VisionTest1_1.Init("", chkNewTemplate.Checked))
                    return false;

                if (!ParameterAllObj.VisionTest1_1.ImageProcess(image[0]))
                    return false;

                //设置相机1的测试结果
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>设置相机1_1的测试结果", Color.Magenta, 9);

                #endregion

                #region>>>>>>>>执行方法2
                if (chkLaserMarkTest_Up.Checked)
                {
                    if (!ParameterAllObj.VisionTest1_2.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest1_2.ImageProcess(image[1]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机1_2的测试结果", Color.Magenta, 9);
                }
                #endregion

                #region>>>>>>>>执行方法3
                if(ParameterAllObj.VisionTest1_3.Enabled)
                {
                    if (!ParameterAllObj.VisionTest1_3.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest1_3.ImageProcess(image[2]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机1_3的测试结果", Color.Magenta, 9);
                }

                #endregion

                #region>>>>>>>>执行方法4
                if (ParameterAllObj.VisionTest1_4.Enabled)
                {
                    if (!ParameterAllObj.VisionTest1_4.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest1_4.ImageProcess(image[3]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机1_4的测试结果", Color.Magenta, 9);
                }

                #endregion

                #region>>>>>>>>执行方法5
                if (ParameterAllObj.VisionTest1_5.Enabled)
                {
                    if (!ParameterAllObj.VisionTest1_5.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest1_5.ImageProcess(image[4]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机1_5的测试结果", Color.Magenta, 9);
                }

                #endregion

                HPRobotCtrl.Camera1.ShowImage(image[0]);
                if (ParameterAllObj.VisionTest1_5.Enabled)
                    HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_5.CheckResult, 0.00, 0.00, false);
                //this.Invoke(new Action(() => HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_5.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest1_4.Enabled)
                    HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_4.CheckResult, 0.00, 0.00, false);
                    //this.Invoke(new Action(() => HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_4.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest1_3.Enabled)
                    HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_3.CheckResult, 0.00, 0.00, false);
                //this.Invoke(new Action(() => HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_3.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest1_2.Enabled)
                    HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_2.CheckResult, 0.00, 0.00, false);
                //this.Invoke(new Action(() => HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_2.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest1_1.Enabled)
                    HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_1.CheckResult, 0.00, 0.00, false);
                    //this.Invoke(new Action(() => HPRobotCtrl.Camera1.ShowResult(ParameterAllObj.VisionTest1_1.CheckResult, 0.00, 0.00, false)));

                return true;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"VisionTest1_Execute() 异常:  {ex.ToString()}\r\n";
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>{HPglobal.m_strLastException}", Color.Red, 9);
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }

        }

        public void UpdateTotalPassedFailed(short passedCurr, short failedCurr)
        {
            try
            {
                lock (HPRobotCtrl.objSetPassedFiled)
                {
                    short currentPassed = (short)(HPRobotCtrl.PLC_Unload.ReadInt16("D1038").Content + passedCurr);
                    short currentFailed = (short)(HPRobotCtrl.PLC_Unload.ReadInt16("D1039").Content + failedCurr);

                    HPRobotCtrl.PLC_Unload.Write("D1038", currentPassed);
                    HPRobotCtrl.PLC_Unload.Write("D1039", currentFailed);
                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = $"UpdateTotalPassedFailed() 异常:  {ex.ToString()}\r\n";
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>{HPglobal.m_strLastException}", Color.Red, 9);
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private object objImageProcess = new object();

        public void thread_ForCamera1(object wsarg)
        {
            string trigAddress = "D1200";
            string completedAddress = "D1201";
            string resultAddress = "D1202";
            string errorMsg = "";
            emActionHandle actionHandle_result;
            string str1or2 = (string)wsarg;
            bool bVistionTestResult = false;
            Task<bool> taskImageProcess = null;
            short passedCurr = 0;
            short failedCurr = 0;
            while (!HPglobal.m_bQuitApp && !bStopFlag)
            {
                Thread.Sleep(200);
                if (HPglobal.m_bQuitApp)
                    return;

                try
                {
                    #region>>>>>初始化数据
                    passedCurr = 0;
                    failedCurr = 0;
                    #endregion

                    TRIGSTART:
                    #region>>>>>>等待烧录事件触发
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>等待PLC触发拍照D1200", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(200);
                        if (HPglobal.m_bQuitApp || bStopFlag)
                            return;

                        #region>>>>>>定义的生产总量是否已经达到
                        if (ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 0)
                        {
                            errorMsg = $"注意: RobotUnload_End 当前定义的生产总量已经达到！\r\n\r\n如果想继续下料，请将总出料盘设为0或重新定义出货总量 之后点击  继续(不一定下料成功) 否则请点击停止 \r\n\r\n";
                            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);
                            return;
                        }
                        #endregion

                        OperateResult<short> nTrigger = HPRobotCtrl.PLC_Unload.ReadInt16(trigAddress);
                        if (nTrigger.IsSuccess && nTrigger.Content == 1)
                            break;
                    }

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"clear", Color.Blue, 9);
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>受到PLC触发拍照D1200->1", Color.Blue, 9);
                    #endregion

                    DateTime StartTime, EndTime;
                    StartTime = EndTime = DateTime.Now;

                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return;

                    #region>>>>>>复位触发信号
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>复位触发信号", Color.Blue, 9);
                    HPRobotCtrl.PLC_Unload.Write(trigAddress, (short)0);
                    HPRobotCtrl.PLC_Unload.Write(completedAddress, (short)0);
                    HPRobotCtrl.PLC_Unload.Write(resultAddress, (short)0);
                    #endregion

                    Thread.Sleep(500);
                    RE_DO_CHECK:

                    #region>>>>>>开始拍照
                    bVistionTestResult = false;                   
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>开始拍照", Color.Blue, 9);

                    HalconDotNet.HImage[] image = new HImage[5];
                    MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest1_1, ParameterAllObj.VisionTest1_2, ParameterAllObj.VisionTest1_3, ParameterAllObj.VisionTest1_4, ParameterAllObj.VisionTest1_5 };

                    for (int nIndex = 0; nIndex < 5; nIndex++)
                    {
                        if(methods[nIndex].Enabled)
                        {
                            HPRobotCtrl.Camera1.SetCameraExposure(methods[nIndex].Exposure * 1000.00);
                            Thread.Sleep((int)spedDelayCameraSet.Value);
                            for (int nCount = 0; nCount < 5; nCount++)
                            {
                                HalconForm.visionResult resultCapture = new HalconForm.visionResult();
                                if ((resultCapture = HPRobotCtrl.Camera1.SnapImage(out image[nIndex])).IsSuccess == false || !HalconGeneral.IsImageObj(image[nIndex]))
                                {
                                    HPglobal.RecordeAllMessage($"相机1采集图像失败, 重新采集!: {resultCapture.ErrorMsg}", true, false, "");
                                    Thread.Sleep(300);
                                }
                                else
                                    break;
                            }

                            Thread.Sleep(20);
                        }
                    }
                    #endregion

                    #region>>>>>>开始进行视觉处理
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>thread_ForCamera1 异步进行图像处理", Color.Blue, 9);
                    bVistionTestResult = false;

                    #region>>>>>>异步进行图像处理
                    bool[] bNewResults = null;
                    taskImageProcess = Task.Factory.StartNew<bool>(() =>
                    {
                        try
                        {
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>异步进行图像处理", Color.Magenta, 9);
                            lock(objImageProcess)
                            {
                                bVistionTestResult = VisionTest1_Execute(image);
                            }

                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>设置测试结果", Color.Magenta, 9);
                            bNewResults = SetCamera1Result();

                            //将第5个产品作为产品有无的结果告诉第二个相机，第二个相机根据结果抓取
                            PushCamera1ProdcutInPosInfo(HPRobotCtrl.strTrayIDName);

                            //设置Pass Fail数据结果
                            for(int nIndex = 0; nIndex < bNewResults.Length; nIndex++)
                            {
                                if (bNewResults[nIndex])
                                    passedCurr++;
                                else
                                    failedCurr++;
                            }

                            HPRobotCtrl.PassedCamera1 += passedCurr;
                            HPRobotCtrl.FailedCamera1 += failedCurr;
                            UpdateTotalPassedFailed(passedCurr, failedCurr);

                            //更新总共有多少好板数
                            if (ParameterAllObj.VisionTest1_5.Enabled)
                            {
                                int nNumberOfInPos = 0;
                                for (int nIndexAOI = 0; nIndexAOI < ParameterAllObj.VisionTest1_5.CheckResult.Length; nIndexAOI++)
                                {
                                    if (ParameterAllObj.VisionTest1_5.CheckResult[nIndexAOI].ResultStatus)
                                        nNumberOfInPos++;
                                }
                                int checkedPassed = (int)(HPRobotCtrl.PLC_Unload.ReadUInt32("D1069").Content + nNumberOfInPos);
                                HPRobotCtrl.PLC_Unload.Write("D1069", checkedPassed);
                            }

                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>异步图像处理完成，结果为 {bVistionTestResult}", Color.Magenta, 9);

                            return bVistionTestResult;
                        }
                        catch(Exception ex)
                        {
                            bVistionTestResult = false;
                            HPglobal.m_strLastException = string.Format("thread_ForCamera1  taskImageProcess 执行其间发生错误\r\n{0}", ex.ToString());
                            HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                            return bVistionTestResult;
                        }
                    });

                    #endregion

                    #endregion

                    ENDPROCESS:
                    #region>>>>>>等待图像处理结果完成
                    Thread.Sleep(200);
                    while (taskImageProcess.Status == TaskStatus.WaitingToRun || taskImageProcess.Status == TaskStatus.Running)
                    {
                        if (HPglobal.m_bQuitApp || bStopFlag)
                            return;

                        Application.DoEvents();
                    }

                    taskImageProcess.Wait();

                    #endregion

                    #region>>>>>>结果全部失败提示
                    bool CheckAllFailed = false;
                    for (int nIndex = 0; nIndex < bNewResults.Length; nIndex++)
                    {
                        CheckAllFailed |= bNewResults[nIndex];
                    }

                    if(CheckAllFailed == false)
                    {
                        errorMsg = $"\r\n\r\n结果全部失败NG, 是否有异常?????\r\n\r\n";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo)
                        {
                            HPRobotCtrl.PLC_Unload.Write("D1008", (short)1);
                            Thread.Sleep(2000);
                            goto RE_DO_CHECK;
                        }
                    }
                    #endregion

                    #region>>>>>>设置数据状态
                    string strVistion1TestResult = "";
                    for (int nRow = 0; nRow < 10; nRow++)
                    {
                        for (int nCol = 0; nCol < 10; nCol++)
                        {
                            strVistion1TestResult += HPRobotCtrl.bArryCamera1_Result[nRow, nCol] ? "1;" : "0;";
                        }
                    }
                    strVistion1TestResult = strVistion1TestResult.TrimEnd(";".ToCharArray());
                    ResultInformation result = new ResultInformation(HPRobotCtrl.dataBaseSQL.SQL_GetResultFiledFromID(APK_HP.MES.HPSql.WorkStationNameEnum.顶部测试工站));
                    result.Results[0].SetResult($"Vision1TestOK({HPRobotCtrl.strTrayIDName};{HPRobotCtrl.strVirtrualPCBSN};[{strVistion1TestResult}])");
                    result.ResultStatus = true;
                    if (!HPRobotCtrl.dataBaseSQL.SQL_SetStationResult(HPRobotCtrl.strVirtrualPCBSN, APK_HP.MES.HPSql.WorkStationNameEnum.顶部测试工站, result))
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 失败{HPRobotCtrl.strVirtrualPCBSN}({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9);
                    else
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 成功{HPRobotCtrl.strVirtrualPCBSN}", Color.Blue, 9);

                    #endregion

                    #region>>>>>>写入PLC完成信息
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>写入PLC完成信息, 结果{(bVistionTestResult ? "1" : "2")}", Color.Blue, 9);
                    HPRobotCtrl.PLC_Unload.Write(completedAddress, (short)1);
                    HPRobotCtrl.PLC_Unload.Write(resultAddress, bVistionTestResult ? (short)1 : (short) 2);
                    #endregion

                    #region>>>>>>保存本地记录
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>保存本地记录(N/A){(bVistionTestResult ? "1" : "2")}", Color.Blue, 9);

                    #endregion

                    EndTime = DateTime.Now;
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>处理用时: {(EndTime - StartTime).TotalSeconds.ToString()}(s)", Color.Blue, 9);
                }
                catch (Exception ex)
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>thread_ForCamera1 异常", Color.Red, 9);
                    HPglobal.m_strLastException = string.Format("thread_ForCamera1 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        public void thread_ForRobotUnloadEvent(object wsarg)
        {
            string trigAddress = "D1303";
            string completedAddress = "D1304";
            string resultAddress = "D1305";
            string errorMsg = "";
            emActionHandle actionHandle_result;
            string str1or2 = (string)wsarg;
            bool bCheckLoadTrayStart = false;

            while (!HPglobal.m_bQuitApp && !bStopFlag)
            {
                Thread.Sleep(200);
                try
                {
                    TRIGSTART:

                    #region>>>>>>等待下料事件触发
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>等待PLC触发下料D1303", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(200);
                        if (HPglobal.m_bQuitApp || bStopFlag)
                            return;

                        #region>>>>>>如果生产总量已经达到，提示
                        if (ParameterAllObj.TotalProductCountTray > 0 &&
                            (ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 0))
                        {
                            errorMsg = $"注意: thread_ForRobotUnloadEvent 当前定义的生产总量已经达到！\r\n\r\n请将总出料盘设为0或重新定义出货总量 之后点击  继续 \r\n\r\n";
                            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                            this.Invoke(new Action(() => btm进入尾料模式.Checked = false));
                            HPRobotCtrl.PLC_Unload.Write("D1056", (short)0);
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);
                            Task.Factory.StartNew(new Action(() => _StopMachine()));
                            return;
                        }
                        #endregion

                        OperateResult<short> nTrigger = HPRobotCtrl.PLC_Unload.ReadInt16(trigAddress);
                        if (nTrigger.IsSuccess  && nTrigger.Content == 1)
                            break;

                        if (HPRobotCtrl.PulseAllRemaindProduct)
                            break;
                    }
                    
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"clear", Color.Blue, 9);
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>受到PLC触发下料或手动触发信号", Color.Blue, 9);
                    #endregion

                    DateTime StartTime, EndTime;
                    StartTime = EndTime = DateTime.Now;

                    #region>>>>>>复位触发信号
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>复位触发信号", Color.Blue, 9);
                    HPRobotCtrl.PLC_Unload.Write(trigAddress, (short)0);
                    HPRobotCtrl.PLC_Unload.Write(completedAddress, (short)0);
                    HPRobotCtrl.PLC_Unload.Write(resultAddress, (short)0);
                    #endregion

                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return;

                    #region>>>>>>确认机器人安全信号 设备安全位
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>确认机器人安全信号D1310 -> 1", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(50);
                        if (HPglobal.m_bQuitApp || bStopFlag)
                            return;

                        OperateResult<short> robotSafeMove = HPRobotCtrl.PLC_Unload.ReadInt16("D1310");
                        if (robotSafeMove.Content == 1)
                            break;
                    }
                    #endregion

                    #region>>>>>>复位安全信号
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>复位安全信号", Color.Blue, 9);
                    HPRobotCtrl.PLC_Unload.Write("D1309", (short) 0);
                    HPRobotCtrl.PLC_Unload.Write("D1311", (short) 0);

                    if (HPRobotCtrl.PulseAllRemaindProduct)
                        goto CLEAR_PRODUCT_MODEL;

                    string strCurrentTrayIDName = $"{HPRobotCtrl.strTrayIDName2}";
                    string strCurrentPCBVirtureSN = $"{HPRobotCtrl.strVirtrualPCBSN2}";
                    #endregion

                    Thread.Sleep(500);

                    #region>>>>>>复制Camera1的临时数据
                    RE_GET_VISION1_DATA:
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>复制Camera1的临时数据", Color.Blue, 9);
                    strVistionTest2 = "";

                    for (int nRow = 0; nRow < 10; nRow++)
                        for (int nCol = 0; nCol < 10; nCol++)
                            HPRobotCtrl.bArryCamera2_RT2_Temp[nRow, nCol] = false;

                    HPRobotCtrl.bArryCamera2_RT2_Temp = HandGetVision1TestResult(txtTrayID2.Text.Trim());

                    #endregion

                    #region>>>>>>Camera1的数据数据失败失败, 提示操作员
                    if (HPRobotCtrl.bArryCamera2_RT2_Temp == null)
                    {
                        errorMsg = $"读取视觉1数据失败\r\n托盘号: {txtTrayID2.Text.Trim()}\r\n点击 重复 可继续重试，\r\n点击其它按纽程序继续执行";
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo)
                            goto RE_GET_VISION1_DATA;
                        else
                        {
                        }
                    }

                    #endregion

                    #region>>>>>>读取烧录数据
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>读取烧录数据", Color.Blue, 9);
                    RE_GET_FLASH_DATA:
                    bool bReadFalshData = false;
                    if (chkCareFlashResult.Checked)
                    {
                        this.Invoke(new Action(()=>
                        {
                            bReadFalshData = HandGetFlashData(txtTrayID2.Text.Trim(), false);
                        }));
                    }
                    else
                        bReadFalshData = true;

                    if (!bReadFalshData)
                    {
                        errorMsg = $"读取烧录数据失败\r\n托盘号: {txtTrayID2.Text.Trim()}\r\n请将此盘手动取出，点击 继续 运行将全部作坏料处理\r\n点击 停止将终止当前的下料(需要人工取盘及复位PLC请求";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);

                        #region>>>>>>烧录数据失败失败, 提示操作员
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Continue)
                        {  }
                        else if (actionHandle_result == emActionHandle.ActionHandle_Redo)
                        {
                            HPRobotCtrl.PLC_Unload.Write("D1011", (short)1);
                            Thread.Sleep(2000);
                            goto RE_GET_FLASH_DATA;
                        }
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        {
                            int nRowCount = (int)speRows.Value;
                            int nColCount = (int)speCols.Value;

                            int nCountFailed = nRowCount * nColCount;

                            //设置统计结果
                            UpdateTotalPassedFailed(0, (short) nCountFailed);

                            OperateResult<Int16> countOfPLC2 = HPRobotCtrl.PLC_Unload.ReadInt16("D1063");
                            HPRobotCtrl.PLC_Unload.Write("D1063", (short)countOfPLC2.Content + nCountFailed);

                            goto TRIGSTART;
                        }
                        #endregion
                    }
                    else
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>读取烧录数据{txtTrayID2.Text.Trim()}成功!", Color.LightGreen, 9);


                    #endregion

                    #region>>>>>>开始取放料
                    bool bUnloadResult = false;
                    if ((bUnloadResult = RobotUnload()))
                    {
                        #region>>>>>>设置数据状态
                        strVistionTest2 = strVistionTest2.TrimEnd(";".ToCharArray());
                        ResultInformation result = new ResultInformation(HPRobotCtrl.dataBaseSQL.SQL_GetResultFiledFromID(APK_HP.MES.HPSql.WorkStationNameEnum.底部测试工站));
                        result.Results[0].SetResult($"Vision2TestOK({strCurrentTrayIDName};{strCurrentPCBVirtureSN};[{strVistionTest2}])");
                        result.ResultStatus = true;
                        if (!HPRobotCtrl.dataBaseSQL.SQL_SetStationResult(strCurrentPCBVirtureSN, APK_HP.MES.HPSql.WorkStationNameEnum.底部测试工站, result))
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 失败{strCurrentPCBVirtureSN}({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9);
                        else
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 成功{strCurrentPCBVirtureSN}", Color.Blue, 9);

                        #endregion

                        //写入PCBID
                        HPRobotCtrl.RecordUnloaedPCBID_ToRobotPLC(strCurrentPCBVirtureSN);
                    }
                    else
                    {
                        HPglobal.RecordeAllMessage("注意: 下料失败, 料没有补齐，等待下一次上料", true, false, "");
                    }

                    #endregion

                    #region>>>>>>托盘解绑及释放数据
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>托盘解绑", Color.Magenta, 9);

                    GetVistion1ProductInPos(strCurrentTrayIDName, true);

                    bool bResult = HPRobotCtrl.dataBaseSQL.SQL_TrayUnBind(strCurrentTrayIDName);
                    if (!bResult)
                    {
                        errorMsg = $"{DateTime.Now.ToLongTimeString()}>>>托盘({strCurrentTrayIDName})解绑 失败!{APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast}\r\n";
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, errorMsg, Color.Red, 9);
                    }
                    else
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>托盘解绑 !成功!", Color.Magenta, 9);
                    #endregion

                    #region>>>>>>保存本地记录

                    #endregion

                    #region>>>>>>如果进入尾料模式，再次开始补料， 如果一个都没有就不需要补料，等待下一盘
                    CLEAR_PRODUCT_MODEL:
                    if (btm进入尾料模式.Checked)
                    {
                        int nRowCount = (int)speRows.Value;
                        int nColCount = (int)speCols.Value;
                        int nCurrenRow = ParameterAllObj.Cfg_Remaind / nColCount;
                        int nCurrenCol = ParameterAllObj.Cfg_Remaind % nColCount;
                        Point EndLoadPoint = new Point(nCurrenRow, nCurrenCol);

                        #region>>>>>>尾料模式，再次开始补料
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>进入尾料模式，再次开始补料并清料(最后补料位置: {nCurrenRow}行, {nCurrenCol}例)", Color.Magenta, 9);
                        if (ResupplyCellForBuff(EndLoadPoint))
                        {
                            #region>>>>>>等待上好料盘
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>上料已经满，等待上好料盘", Color.Blue, 9);
                            if (!WaitLoadA_LoadTray(true, true))
                            {
                                errorMsg = $"人工手动终止上料盘的动作\r\n由于没有盘，下料动作将补终止 ！\r\n";
                                HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                            }
                            #endregion
                        }
                        else
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>注意: 料没有补齐，等待下一次上料！", Color.Blue, 9);

                        #endregion
                    }
                    #endregion

                    //置位安全信号
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>置位安全信号", Color.Blue, 9);
                    HPRobotCtrl.PLC_Unload.Write("D1309", 1);

                    EndTime = DateTime.Now;
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>处理用时: {(EndTime - StartTime).TotalSeconds.ToString()}(s)", Color.Blue, 9);

                    #region>>>>>>如果生产总量已经达到，提示并退出
                    if (ParameterAllObj.TotalProductCountTray > 0 &&
                        (ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 0))
                    {
                        errorMsg = $"注意: thread_ForRobotUnloadEvent 当前定义的生产总量已经达到！\r\n\r\n请将总出料盘设为0或重新定义出货总量 之后点击  继续 \r\n\r\n";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        this.Invoke(new Action(() => btm进入尾料模式.Checked = false));
                        HPRobotCtrl.PLC_Unload.Write("D1056", (short)0);
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);
                        Task.Factory.StartNew(new Action(() => _StopMachine()));
                        return;
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_ForRobotUnloadEvent() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        private void btmTestRobot_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("确定执行回原点动?", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                if (HPRobotCtrl.ObjRobot.Home() == 0)
                {
                    MessageBox.Show("机器人回原点成功, 请启动设备!", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("机器人回原点失败!", "确认", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmTestRobot_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void btmXIncre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_X, dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmXIncre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return ;
            }
        }

        private void btmXDecre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_X, -dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmXDecre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void btmYIncre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_Y, dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmYIncre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void btmYDecre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_Y, -dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmYDecre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void btmZIncre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_Z, dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmZIncre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void btmZDecre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_Z, -dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmZDecre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void btmRIncre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_RU, dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRIncre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void btmRDecre_Click(object sender, EventArgs e)
        {
            try
            {
                double dst = double.Parse(cobMoveDst.Text.Trim());
                if(HPRobotCtrl.ObjRobot.MoveRelative(RobotBase.emRobotAxisIndex.RobotAxis_RU, -dst) != 0)
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRDecre_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void 伺服启用禁用_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int nResult;

                HPRobotCtrl.ObjRobot.Reset();
                Thread.Sleep(200);

                nResult = HPRobotCtrl.ObjRobot.ServoOn(伺服启用禁用.Checked);
                if (HPRobotCtrl.ObjRobot.IsReady())
                    伺服启用禁用.Checked = true;
                else
                    伺服启用禁用.Checked = false;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("伺服启用禁用_CheckedChanged() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        
        }

        private void 机器人报警清除_Click(object sender, EventArgs e)
        {
            try
            {
                HPRobotCtrl.ObjRobot.Reset();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("机器人报警清除_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
           
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int nResult = 0;
            nResult = HPRobotCtrl.ObjRobot.GetIOLine(0);
            nResult = HPRobotCtrl.ObjRobot.GetIOPort8(0);
            nResult = HPRobotCtrl.ObjRobot.GetIOPort16(0);

            nResult = HPRobotCtrl.ObjRobot.SetIOLine(9, true);
            nResult = HPRobotCtrl.ObjRobot.SetIOPort8(0, 0xff);
            nResult = HPRobotCtrl.ObjRobot.SetIOPort16(0, 0xffff);

            Thread.Sleep(100);
            nResult = HPRobotCtrl.ObjRobot.SetIOLine(0, false);
            nResult = HPRobotCtrl.ObjRobot.SetIOPort8(0, 0);
            nResult = HPRobotCtrl.ObjRobot.SetIOPort16(0, 0);
        }

        private void btmSpeedClick(object sender, EventArgs e)
        {
            try
            {
                if(rbtmSeep_Low.Checked)
                   HPRobotCtrl.ObjRobot.SetSpeed(20);
                else if (rbtmSeep_Middel.Checked)
                    HPRobotCtrl.ObjRobot.SetSpeed(50);
                else if (rbtmSeep_High.Checked)
                   HPRobotCtrl.ObjRobot.SetSpeed(100);
                else if (rbtmSeep_SupperHigh.Checked)
                    HPRobotCtrl.ObjRobot.SetSpeed(120);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmSpeedClick() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        private void Output_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.CheckBox[] arryLEDs = { LED_Out0, LED_Out1, LED_Out2, LED_Out3, LED_Out4, LED_Out5, LED_Out6, LED_Out7,
                                                                                           LED_Out8, LED_Out9, LED_Out10, LED_Out11, LED_Out12, LED_Out13, LED_Out14, LED_Out15,
                                                                                           LED_Out16, LED_Out17, LED_Out18, LED_Out19, LED_Out20, LED_Out21, LED_Out22, LED_Out23,
                                                                                           LED_Out24, LED_Out25, LED_Out26, LED_Out27, LED_Out28, LED_Out29, LED_Out30, LED_Out31};

                for (int nIndex = 0; nIndex < 32; nIndex++)
                {
                    if (sender == arryLEDs[nIndex])
                    {
                        HPRobotCtrl.ObjRobot.SetIOLine(nIndex, arryLEDs[nIndex].Checked);
                        break;
                    }
                }

                //             for (int nIndex = 0; nIndex < 32; nIndex++)
                //             {
                //                 UInt32 value = 0;
                //                 zmcaux.ZAux_Direct_GetOp(HPLaserFun.hHandleECI0064, nIndex, ref value);
                //                 arryLEDs[nIndex].Checked = (value == 0 ? false : true);
                //             }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Robot Output_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }

        }

        private void X回原点_Click(object sender, EventArgs e)
        {

        }

        private void Y回原点_Click(object sender, EventArgs e)
        {

        }

        private void Z回原点_Click(object sender, EventArgs e)
        {

        }

        private void R回原点_Click(object sender, EventArgs e)
        {

        }

        private void ButtonLearnRobotPos(DevExpress.XtraEditors.TextEdit text)
        {
            try
            {
                if (MessageBox.Show("确定要获取并更新机器人位置？", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                text.Text = "";
                Application.DoEvents();
                string strPos = HPRobotCtrl.ObjRobot.GetRobotPosToString(chkZero.Checked);
                if (strPos != null && strPos.Length > 5)
                    text.Text = strPos;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Robot btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmRobotLearnOrg_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotOrg);
        }

        private void btmRobotLearnCatch_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotCatch);
         
        }

        private void btmRobotLearnUnload_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotUnload);
     
        }

        private void btmRobotLearnCamera_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotCamera);
   
        }

        private void btmRobotLearnOrg_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotOrg.Text.Trim());
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnOrg_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotLearnCatch_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotCatch.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnCatch_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void UpdataWayList()
        {
            try
            {
                int nRowCount = gridRobotWayList.Rows.Count;
                comboxRunFrom.Properties.Items.Clear();
                comboxRunTo.Properties.Items.Clear();
                for (int nIndex = 0; nIndex < nRowCount; nIndex++)
                {
                    gridRobotWayList.Rows[nIndex].HeaderCell.Value = (nIndex + 1).ToString();
                    comboxRunFrom.Properties.Items.Add((nIndex + 1).ToString());
                    comboxRunTo.Properties.Items.Add((nIndex + 1).ToString());
                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("UpdataWayList btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

        }

        private void btmRobotWayAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridRobotWayList.CurrentCell == null)
                    return;
                else if (gridRobotWayList.CurrentCell.RowIndex == gridRobotWayList.RowCount - 1)
                    gridRobotWayList.Rows.Insert(gridRobotWayList.CurrentCell.RowIndex + 1, 1);
                else
                    gridRobotWayList.Rows.Insert(gridRobotWayList.CurrentCell.RowIndex + 1, 1);
                UpdataWayList();
                gridRobotWayList.Invalidate();
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotWayAdd_Click btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

        }

        private void btmRobotWayDel_Click(object sender, EventArgs e)
        {
            try
            {
                gridRobotWayList.Rows.RemoveAt(gridRobotWayList.CurrentCell.RowIndex);
                UpdataWayList();
                gridRobotWayList.Invalidate();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotWayDel_Click btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotWayClear_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("确定清除所有位置？\n此操作将不可恢复！", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                gridRobotWayList.Rows.Clear();
                UpdataWayList();
                gridRobotWayList.Invalidate();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotWayClear_Click btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotWayLearn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("确定重新学习位置？\n此操作修改你选定的坐标位置！", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                if (gridRobotWayList.CurrentCell.RowIndex < 0)
                {
                    MessageBox.Show("请选中需要重新学的行！", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                double x = 999.99;
                double y = 999.99;
                double z = 999.99;
                double r = 999.99;
                RobotBase.emRobotHandType handType = RobotBase.emRobotHandType.HandType_Auto;
                HPRobotCtrl.ObjRobot.GetCurrentPosition(ref x, ref y, ref z, ref r, ref handType);
                if (r != 999.99)
                {
                    int nRow = gridRobotWayList.CurrentCell.RowIndex;

                    gridRobotWayList.Rows[nRow].Cells[0].Value = x.ToString("F3");
                    gridRobotWayList.Rows[nRow].Cells[1].Value = y.ToString("F3");
                    gridRobotWayList.Rows[nRow].Cells[2].Value = z.ToString("F3");
                    gridRobotWayList.Rows[nRow].Cells[3].Value = r.ToString("F3");
                    if (handType == RobotBase.emRobotHandType.HandType_Left)
                        gridRobotWayList.Rows[nRow].Cells[4].Value = "/L";
                    else if (handType == RobotBase.emRobotHandType.HandType_Right)
                        gridRobotWayList.Rows[nRow].Cells[4].Value = "/R";
                    else
                        gridRobotWayList.Rows[nRow].Cells[4].Value = "Auto";

                    gridRobotWayList.Rows[nRow].Cells[5].Value = "Jump";
                    if(nRow == gridRobotWayList.Rows.Count - 1)
                        gridRobotWayList.Rows[nRow].Cells[6].Value = "";
                    else
                        gridRobotWayList.Rows[nRow].Cells[6].Value = "CP";

                    gridRobotWayList.Rows[nRow].Cells[7].Value = "0.00";
                    gridRobotWayList.Rows[nRow].Cells[8].Value = "true";
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotWayLearn_Click btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

               
        }

        private void btmRobotWayTrayRun_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("确定要移动机器人到目标位置？\n请一定要确保安全及准备急停！", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                HPRobotCtrl.ObjRobot.SetSpeed(5);
                for(int nRow = 0; nRow < gridRobotWayList.Rows.Count; nRow++)
                {
                    if(gridRobotWayList.Rows[nRow].Cells[8].Value.ToString().Contains("true"))
                    {
                        string movePosition = GetRobotRunWayFromTableRow(nRow);
                        if (HPRobotCtrl.ObjRobot.MoveFromPosString(movePosition) != 0)
                        {
                            MessageBox.Show($"移动机器人失败(当前行: {(nRow + 1).ToString()})！\r\n移动将终止! \r\n请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotWayTrayRun_Click btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotWayTrayRunCurrent_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("确定要移动机器人到目标位置？\n请一定要确保安全及准备急停！", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                if (gridRobotWayList.CurrentCell.RowIndex < 0)
                {
                    MessageBox.Show("请选中需要重新学的行！", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                HPRobotCtrl.ObjRobot.SetSpeed(5);
                string movePosition = GetRobotRunWayFromTableRow(gridRobotWayList.CurrentCell.RowIndex);
                if (HPRobotCtrl.ObjRobot.MoveFromPosString(movePosition) != 0)
                {
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnCatch_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        public bool[,] GetVistion1ProductInPos(string trayID, bool bRemove = false)
        {
            bool[,] results = new bool[10, 10];
            for (int nRow = 0; nRow < 10; nRow++)
                for (int nCol = 0; nCol < 10; nCol++)
                    results[nRow, nCol] = true;

            if(HPRobotCtrl.m_queVistion1ProductInPos.Count <= 0)
                return results;

            try
            {
                lock (HPRobotCtrl.m_queVistion1ProductInPos)
                {
                    List<HPRobotCtrl.Visition1IsProductInPos> resultList = HPRobotCtrl.m_queVistion1ProductInPos.ToList<HPRobotCtrl.Visition1IsProductInPos>();
                    for (int nIndex = 0; nIndex < resultList.Count; nIndex++)
                    {
                        if (trayID == resultList[nIndex].trayID)
                        {
                            //设定数值
                            for (int nRow = 0; nRow < 10; nRow++)
                                for (int nCol = 0; nCol < 10; nCol++)
                                    results[nRow, nCol] = resultList[nIndex].VisitionTestResults[nRow, nCol];

                            //删除当前队例
                            if(bRemove)
                            {
                                resultList.RemoveAt(nIndex);

                                //重新设定数据队例
                                HPRobotCtrl.m_queVistion1ProductInPos.Clear();
                                HPRobotCtrl.m_queVistion1ProductInPos = new Queue<HPRobotCtrl.Visition1IsProductInPos>(resultList);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("GetVistion1ProductInPos  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return results;

        }


        public string GetRobotRunWayFromTableRow(int row)
        {
            try
            {
                string x = gridRobotWayList.Rows[row].Cells[0].Value.ToString();
                string y = gridRobotWayList.Rows[row].Cells[1].Value.ToString();
                string z= gridRobotWayList.Rows[row].Cells[2].Value.ToString();
                string r = gridRobotWayList.Rows[row].Cells[3].Value.ToString();
                string Handtype = gridRobotWayList.Rows[row].Cells[4].Value.ToString();
                string Movetype = gridRobotWayList.Rows[row].Cells[5].Value.ToString();
                string cp = (gridRobotWayList.Rows[row].Cells[6].Value.ToString().Contains("CP") ? "CP" : "");
                string zlimit = gridRobotWayList.Rows[row].Cells[7].Value.ToString();
                string enable = gridRobotWayList.Rows[row].Cells[8].Value.ToString();

                string Retun = $"{ x}, {y}, {z}, {r}, {Handtype}, {Movetype}, {cp}, {zlimit}";
                return Retun;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("GetRobotRunWayFromTableRow btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

                return null;
            }
        }

        public bool LoadXmlConfig()
        {
            try
            {
                //初始化全部变量
                XmlNode rootNode = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation");
                if (rootNode == null)
                    return false;

                #region>>>>>>>>加载所有机器人坐标
                XmlNode nodePosDef = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation").SelectSingleNode("RobotStation").SelectSingleNode("PosDef");
                if (nodePosDef != null)
                {
                    #region>>>>>>>>加载机器人坐标
                    txtRobotOrg.Text = (nodePosDef["RobotOrg"] != null ? nodePosDef["RobotOrg"].InnerText : "");
                    txtRobotCatch.Text = (nodePosDef["RobotCatch"] != null ? nodePosDef["RobotCatch"].InnerText : "");
                    txtRobotUnload.Text = (nodePosDef["RobotCamera"] != null ? nodePosDef["RobotCamera"].InnerText : "");
                    txtRobotCamera.Text = (nodePosDef["RobotUnload"] != null ? nodePosDef["RobotUnload"].InnerText : "");

                    txtRobotTransit_LToF.Text = (nodePosDef["RobotPosLtoF"] != null ? nodePosDef["RobotPosLtoF"].InnerText : "");
                    txtRobotTransit_ULToF.Text = (nodePosDef["RobotPosULtoF"] != null ? nodePosDef["RobotPosULtoF"].InnerText : "");
                    txtRobotTransit_VToQA.Text = (nodePosDef["RobotPosVtoF"] != null ? nodePosDef["RobotPosVtoF"].InnerText : "");

                    txtRobotQA.Text = (nodePosDef["RobotPosQA"] != null ? nodePosDef["RobotPosQA"].InnerText : "");
                    txtRobotFailed.Text = (nodePosDef["RobotPosFailed"] != null ? nodePosDef["RobotPosFailed"].InnerText : "");

                    txtRobotPos_BuffFirst.Text = (nodePosDef["RobotPosBuffFirst"] != null ? nodePosDef["RobotPosBuffFirst"].InnerText : "");
                    txtRobotPos_BuffFirst2.Text = (nodePosDef["RobotPosBuffFirst2"] != null ? nodePosDef["RobotPosBuffFirst2"].InnerText : "");
                    if (nodePosDef["RobotPosBuffFirst2"] == null)
                        txtRobotPos_BuffFirst2.Text = txtRobotPos_BuffFirst.Text;

                    txtRobotPos_LoadFirst.Text = (nodePosDef["RobotPosUnloadFirst"] != null ? nodePosDef["RobotPosUnloadFirst"].InnerText : "");
                    txtRobotPos_LoadFirst2.Text = (nodePosDef["RobotPosUnloadFirst2"] != null ? nodePosDef["RobotPosUnloadFirst2"].InnerText : "");
                    if (nodePosDef["RobotPosUnloadFirst2"] == null)
                        txtRobotPos_LoadFirst2.Text = txtRobotPos_LoadFirst.Text;

                    speCathDst.Text = (nodePosDef["speCathDst"] != null ? nodePosDef["speCathDst"].InnerText : "");
                    spedOrgStepDst.Text = (nodePosDef["spedOrgStepDst"] != null ? nodePosDef["spedOrgStepDst"].InnerText : "6.0");

                    #endregion              

                    #region>>>>>>>>运行坐标
                    XmlNode nodeWayList = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation").SelectSingleNode("RobotStation").SelectSingleNode("WayList");
                    int nCount = nodeWayList.ChildNodes.Count;
                    gridRobotWayList.Rows.Clear();
                    UpdataWayList();
                    gridRobotWayList.Invalidate();
                    for (int nIndex = 0; nIndex < nCount; nIndex++)
                    {
                        string pos = nodeWayList.ChildNodes[nIndex].InnerText;
                        int nRow = gridRobotWayList.Rows.Add();
                        pos = pos.Replace(" ", "");
                        string[] values = pos.Split(",".ToCharArray());

                        gridRobotWayList.Rows[nRow].HeaderCell.Value = (nRow + 1).ToString();
                        gridRobotWayList.Rows[nRow].Cells[0].Value = values[0];
                        gridRobotWayList.Rows[nRow].Cells[1].Value = values[1];
                        gridRobotWayList.Rows[nRow].Cells[2].Value = values[2];
                        gridRobotWayList.Rows[nRow].Cells[3].Value = values[3];
                        gridRobotWayList.Rows[nRow].Cells[4].Value = values[4];
                        gridRobotWayList.Rows[nRow].Cells[5].Value = values[5];
                        gridRobotWayList.Rows[nRow].Cells[6].Value = values[6];

                        gridRobotWayList.Rows[nRow].Cells[7].Value = values[7];

                        gridRobotWayList.Rows[nRow].Cells[8].Value = (nodeWayList.ChildNodes[nIndex].Attributes[0].InnerText.Contains("true") ? "true" : "false");
                    }
                    #endregion
                }
                #endregion

                #region>>>>>>>>加载所有工艺参数
                XmlNode nodeParameters = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation").SelectSingleNode("RobotStation").SelectSingleNode("Parameters");
                if(nodeParameters != null)
                {
                    speRows.Text = (nodeParameters["speRows"] != null ? nodeParameters["speRows"].InnerText : "");
                    speCols.Text = (nodeParameters["speCols"] != null ? nodeParameters["speCols"].InnerText : "");
                    speRowDst.Text = (nodeParameters["speRowDst"] != null ? nodeParameters["speRowDst"].InnerText : "");
                    speColDst.Text = (nodeParameters["speColDst"] != null ? nodeParameters["speColDst"].InnerText : "");
                    speCoutOfCatchUsed.Text = (nodeParameters["speCoutOfCatchUsed"] != null ? nodeParameters["speCoutOfCatchUsed"].InnerText : "");

                    speRowOfSingle.Text = (nodeParameters["speRowOfSingle"] != null ? nodeParameters["speRowOfSingle"].InnerText : "");
                    speColOfSingle.Text = (nodeParameters["speColOfSingle"] != null ? nodeParameters["speColOfSingle"].InnerText : "");
                    speRowOfSingleDst.Text = (nodeParameters["speRowOfSingleDst"] != null ? nodeParameters["speRowOfSingleDst"].InnerText : "");
                    speColOfSingleDst.Text = (nodeParameters["speColOfSingleDst"] != null ? nodeParameters["speColOfSingleDst"].InnerText : "");

                    spn_Cust_Row1.Text = (nodeParameters["spn_Cust_Row1"] != null ? nodeParameters["spn_Cust_Row1"].InnerText : "");
                    spn_Cust_Row2.Text = (nodeParameters["spn_Cust_Row2"] != null ? nodeParameters["spn_Cust_Row2"].InnerText : "");
                    spn_Cust_Row3.Text = (nodeParameters["spn_Cust_Row3"] != null ? nodeParameters["spn_Cust_Row3"].InnerText : "");
                    spn_Cust_Row4.Text = (nodeParameters["spn_Cust_Row4"] != null ? nodeParameters["spn_Cust_Row4"].InnerText : "");
                    spn_Cust_Row5.Text = (nodeParameters["spn_Cust_Row5"] != null ? nodeParameters["spn_Cust_Row5"].InnerText : "");
                    spn_Cust_Row6.Text = (nodeParameters["spn_Cust_Row6"] != null ? nodeParameters["spn_Cust_Row6"].InnerText : "");
                    spn_Cust_Row7.Text = (nodeParameters["spn_Cust_Row7"] != null ? nodeParameters["spn_Cust_Row7"].InnerText : "");
                    spn_Cust_Row8.Text = (nodeParameters["spn_Cust_Row8"] != null ? nodeParameters["spn_Cust_Row8"].InnerText : "");
                    spn_Cust_Row9.Text = (nodeParameters["spn_Cust_Row9"] != null ? nodeParameters["spn_Cust_Row9"].InnerText : "");
                }

                #endregion

                #region>>>>>>>> 设置IO信息
                //                 cobx_In_EMG.SelectedIndex = int.Parse(settingNode["IN_EMG_DES"].InnerText);
                //                 cobx_In_AUTO.SelectedIndex = int.Parse(settingNode["IN_AUTO_DES"].InnerText);
                //                 cobx_In_SAFEHANDLE.SelectedIndex = int.Parse(settingNode["IN_SAFEHANDLE_DES"].InnerText);
                //                 cobx_In_TRAY_INPOS.SelectedIndex = int.Parse(settingNode["IN_TRAYINPOS_DES"].InnerText);
                //                 cobx_In_UUT_INPOS.SelectedIndex = int.Parse(settingNode["IN_UUTINPOS_DES"].InnerText);
                //                 cobx_In_LOCK_UP.SelectedIndex = int.Parse(settingNode["IN_LOCKUP_DES"].InnerText);
                //                 cobx_In_LOCK_DOWN.SelectedIndex = int.Parse(settingNode["IN_LOCKDOWN_DES"].InnerText);
                #endregion

                return true;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("LoadXmlConfig 出现异常，程序可能运行错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public void SaveAllConfig(bool bWriteToXmlFile = true)
        {
            try
            {
                #region >>>>>>>>更新所有参数到当前程序（不保存到文件)

                #endregion

                if (bWriteToXmlFile)
                {
                    #region >>>>>>> 将所有参数写入到Xml配方文件
                    XmlNode rootNode = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation");
                    if (rootNode != null)
                    {
                        #region >>>>>>>>保存机器人运动坐标
                        XmlNode nodePosDef = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation").SelectSingleNode("RobotStation").SelectSingleNode("PosDef");
                        if (nodePosDef != null)
                        {
                            //坐标
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotOrg", txtRobotOrg.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotCatch", txtRobotCatch.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotCamera", txtRobotUnload.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotUnload", txtRobotCamera.Text.Trim());

                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosLtoF", txtRobotTransit_LToF.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosULtoF", txtRobotTransit_ULToF.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosVtoF", txtRobotTransit_VToQA.Text.Trim());

                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosQA", txtRobotQA.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosFailed", txtRobotFailed.Text.Trim());

                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosBuffFirst", txtRobotPos_BuffFirst.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosBuffFirst2", txtRobotPos_BuffFirst2.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosUnloadFirst", txtRobotPos_LoadFirst.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "RobotPosUnloadFirst2", txtRobotPos_LoadFirst2.Text.Trim());

                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "speCathDst", speCathDst.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodePosDef, "spedOrgStepDst", spedOrgStepDst.Text.Trim());

                            //===================================================================

                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotOrg"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotCatch"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotCamera"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotUnload"], "Enable", "true");

                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosLtoF"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosULtoF"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosVtoF"], "Enable", "true");

                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosQA"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosFailed"], "Enable", "true");

                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosBuffFirst"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosBuffFirst2"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosUnloadFirst"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["RobotPosUnloadFirst2"], "Enable", "true");

                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["speCathDst"], "Enable", "true");
                            HPglobal.Xml_UpdateNodeAttribute(nodePosDef["spedOrgStepDst"], "Enable", "true");


                            //运行坐标
                            XmlNode nodeWayList= HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation").SelectSingleNode("RobotStation").SelectSingleNode("WayList");
                            nodeWayList.RemoveAll();
                            for (int nIndex = 0; nIndex < gridRobotWayList.RowCount; nIndex++)
                            {
                                if(gridRobotWayList.Rows[nIndex].Cells[0].Value != null && gridRobotWayList.Rows[nIndex].Cells[0].Value.ToString().Length > 0)
                                {
                                    string posString = GetRobotRunWayFromTableRow(nIndex);
                                    if (posString == null || posString.Length < 5)
                                        continue;

                                    XmlNode nodeNewItem = HPglobal.m_xmlSystemConfig.CreateNode(XmlNodeType.Element, "Item", "");
                                    nodeNewItem.InnerText = posString;

                                    string enable = gridRobotWayList.Rows[nIndex].Cells[8].Value.ToString();
                                    XmlAttribute nodeAttr = HPglobal.m_xmlSystemConfig.CreateAttribute("Enable");
                                    nodeAttr.InnerText = enable;
                                    nodeNewItem.Attributes.Append(nodeAttr);

                                    nodeWayList.AppendChild(nodeNewItem);
                                }
                            }
                        }
                        #endregion

                        XmlNode nodeParameters = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation").SelectSingleNode("RobotStation").SelectSingleNode("Parameters");
                        if (nodeParameters == null)
                        {
                            XmlNode nodeNewItem = HPglobal.m_xmlSystemConfig.CreateNode(XmlNodeType.Element, "Parameters", "");
                            nodeParameters = HPglobal.m_xmlSystemConfig.SelectSingleNode("STFInformation").SelectSingleNode("RobotStation").AppendChild(nodeNewItem);
                        }
                        if (nodeParameters != null)
                        {
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speRows", speRows.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speCols", speCols.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speRowDst", speRowDst.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speColDst", speColDst.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speCoutOfCatchUsed", speCoutOfCatchUsed.Text.Trim());

                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speRowOfSingle", speRowOfSingle.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speColOfSingle", speColOfSingle.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speRowOfSingleDst", speRowOfSingleDst.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "speColOfSingleDst", speColOfSingleDst.Text.Trim());

                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row1", spn_Cust_Row1.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row2", spn_Cust_Row2.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row3", spn_Cust_Row3.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row4", spn_Cust_Row4.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row5", spn_Cust_Row5.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row6", spn_Cust_Row6.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row7", spn_Cust_Row7.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row8", spn_Cust_Row8.Text.Trim());
                            HPglobal.Xml_UpdateNodeValue(nodeParameters, "spn_Cust_Row9", spn_Cust_Row9.Text.Trim());

                        }

                        //写入到文件
                        HPglobal.m_xmlSystemConfig.Save(HPglobal.m_strXmlFilename);

                        MessageBox.Show("!!!恭喜恭喜，保存成功!!!", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("UpdateAllConfig btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmSaveRobotConfig_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAllConfig(true);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmSaveRobotConfig_Click btmRobotLearnOrg_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
          
        }

        private void btmRobotLearnUnload_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotUnload.Text.Trim());
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnUnload_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

        }

        private void btmRobotLearnCamera_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotCamera.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnCamera_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        public void MoveRobotToPos(string movePosition)
        {
            RobotBase.emRobotHandType handType = RobotBase.emRobotHandType.HandType_Auto;
            if (chkHandelTypeLeft.Checked)
                handType = RobotBase.emRobotHandType.HandType_Left;
            else if (chkHandelTypeRight.Checked)
                handType = RobotBase.emRobotHandType.HandType_Right;
            else
                handType = RobotBase.emRobotHandType.HandType_Auto;

            try
            {
                if (MessageBox.Show("确定要移动机器人到目标位置？\n请一定要确保安全及准备急停！", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                if (HPRobotCtrl.ObjRobot.MoveFromPosString(movePosition) != 0)
                {
                    MessageBox.Show("移动机器人失败！请确认位是否正确或机器人是否连机", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void chkEnableRobotIO_CheckedChanged(object sender, EventArgs e)
        {
            HPRobotCtrl.ObjRobot.bMonitorIOStatus = chkEnableRobotIO.Checked;
        }

        public void thread_ForInvokeContrlRequirement(object wsarg)
        {
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(200);
                try
                {
                    OperateResult<short[]> D1000Values = HPRobotCtrl.modebusTCPServer.ReadInt16("1000", 20);
                    if (!D1000Values.IsSuccess)
                    {
                        continue;
                    }

                    if(D1000Values.Content[3] == 1) //启动
                    {
                        HPRobotCtrl.modebusTCPServer.Write("1003", (short)0);
                        btmInvoke_StartMachine_Click(null, null);
                    }
                    else if (D1000Values.Content[4] == 1) //复位
                    {
                        HPRobotCtrl.modebusTCPServer.Write("1004", (short)0);
                        btmInvoke_ResetMachine_Click(null, null);
                    }
                    else if (D1000Values.Content[5] == 1) //停止
                    {
                        HPRobotCtrl.modebusTCPServer.Write("1005", (short)0);
                        btmInvoke_StopMachine_Click(null, null);
                    }                  
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_ForInvokeContrlRequirement  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

                }
            }
        }

        public void thread_ForAutoChangeOrder(object wsarg)
        {
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(2000);

                try
                {
                    //是否转工单
                    OperateResult<string> tr2 = HPRobotCtrl.PLC_Unload.ReadString("D2250", 16);
                    if (tr2.IsSuccess)
                    {
                        ParameterAllObj.MarkText = tr2.Content.TrimEnd("\0".ToCharArray());
                        if (ParameterAllObj.MarkText.Length > 0 &&
                            ParameterAllObj.MarkText != ParameterAllObj.MarkTextLast)
                        {
                            if (HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable ||
                                HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Working)
                            {
                                string errorMsg = $"错误: 设备已经在运行中或设备被禁用, 无法转工单\r\n";
                                HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                                HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                                emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                                continue;
                            }
                            else
                            {
                                //转工单
                                ChangeType(ParameterAllObj.MarkText);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_ForInvokeContrlRequirement  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

                }
            }
        }
        public void _ResetMachine()
        {
            try
            {
                if(HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Working)
                {
                    MessageBox.Show("请先停止设备", "确认", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                bStopFlag = false;
                if (HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                    return;

                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Reseting;

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"clear", Color.Blue, 9);
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"设备开始复位", Color.Blue, 9);

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"Write 1039 ---> 0", Color.Blue, 9);
                HPRobotCtrl.PLC_Unload.Write("D1309", (short)0);

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"Write 1007 ---> 0, 1002 ----> 0", Color.Blue, 9);
                HPRobotCtrl.modebusTCPServer.Write("1007", (short)0);
                HPRobotCtrl.modebusTCPServer.Write("1002", (short)0);

                Handle_CyliderControl(false, false, false, false, false, false, false);
                Task<bool> stepTask = Task.Factory.StartNew<bool>(() =>
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"变距遇机开始复位(异步).....", Color.Blue, 9);
                    bool resultCatch = HPRobotCtrl.CatchStep_Home();
                    //HPRobotCtrl.CatchStep_StepMove(Int32.Parse(speCathDst.Text.Trim()), false);

                    OperateResult<int> valueCol = HPRobotCtrl.PLC_Unload.ReadInt32("D2212");
                    if (valueCol.IsSuccess)
                    {
                        double mmDst = (double )valueCol.Content / 1000.000;
                        this.Invoke(new Action(() =>  spinDstRow.Value = (decimal)mmDst));
                        Int32 nCountOfPulse = 0;
                        this.Invoke(new Action(() => nCountOfPulse = (Int32)((mmDst  - (double)spedOrgStepDst.Value )/ ParameterAllObj.DstPerPulse)));
                        HPRobotCtrl.CatchStep_StepMove(nCountOfPulse);
                    }
                    else
                        return false;

                    return resultCatch;
                });

                Task<bool> RobotTask = Task.Factory.StartNew<bool>(() =>
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"机器人开始复位(异步).....", Color.Blue, 9);
                    HPRobotCtrl.ObjRobot.ServoOn(true);
                    HPRobotCtrl.ObjRobot.SetSpeed(5);
                    if (HPRobotCtrl.ObjRobot.Home() != 0)
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"Write 1309 ---> 1", Color.Blue, 9);
                    OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1309", (short)1);

                    return true;
                });

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"等待机器人和电机复位完成.....", Color.Magenta, 9);
                while (stepTask.Status == TaskStatus.WaitingToRun || stepTask.Status == TaskStatus.Running ||
                            RobotTask.Status == TaskStatus.WaitingToRun || RobotTask.Status == TaskStatus.Running)
                {
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return;

                    Application.DoEvents();
                }

                stepTask.Wait();
                RobotTask.Wait();

                btmUpdateRow_Click(null, null);
                设定吸嘴间距_Click(null, null);
                SetTrayFullOrEmpty(false, false);
                SetTrayFullOrEmpty(true, false);

                if (RobotTask.Result && stepTask.Result)
                {
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Idle;
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"复位成功", Color.Blue, 9);
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"Write 1007 ---> 0, 1002 ----> 1", Color.Blue, 9);
                    HPRobotCtrl.modebusTCPServer.Write("1007", (short)0);
                    HPRobotCtrl.modebusTCPServer.Write("1002", (short)1);
                    HPRobotCtrl.PLC_Unload.Write("D1309", (short)1);
                }
                else
                {
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"复位出现报警", Color.Red, 9);
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"Write 1007 ---> 99, 1002 ---->99", Color.Blue, 9);
                    HPRobotCtrl.modebusTCPServer.Write("1007", (short)99);
                    HPRobotCtrl.modebusTCPServer.Write("1002", (short)99);
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPRobotCtrl _ResetMachine  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmInvoke_ResetMachine_Click(object sender, EventArgs e)
        {
            _ResetMachine();
        }

        public void _StartMachine()
        {
            try
            {
                if (HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable ||
                    HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Working)
                {
                    HPglobal.RecordeAllMessage("注意: 设备已经在运行中或设备被禁用, 启动无效！", true, false, "");
                    return;
                }

                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Starting;
                Application.DoEvents();

                #region>>>>>>移动到过渡过位
                if (!Handle_MoveRobot(txtRobotTransit_LToF.Text.Trim()))
                {
                    HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                    return;
                }
                #endregion

                HPRobotCtrl.modebusTCPServer.Write("1006", (short)0);

                //安全信号复位
                HPRobotCtrl.PLC_Unload.Write("D1309", (short)0);

                //启动工作线程

                bStopFlag = false;
                #region>>>>>>>>>>> 开启机器人取料线程
                if(threadOfRobot == null)
                {
                    threadOfRobot = new Thread(new ParameterizedThreadStart(thread_ForRobotUnloadEvent));
                    threadOfRobot.Start("1");
                }

                #endregion

                #region>>>>>>>>>>> 开启Camera1拍照线程
                if (threadCamera == null)
                {
                    threadCamera = new Thread(new ParameterizedThreadStart(thread_ForCamera1));
                    threadCamera.Start("1");
                }
                #endregion

                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Working;
                HPRobotCtrl.modebusTCPServer.Write("1006", (short)3);

                //安全信号复位
                HPRobotCtrl.PLC_Unload.Write("D1309", (short)1);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPRobotCtrl _StartMachine  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmInvoke_StartMachine_Click(object sender, EventArgs e)
        {
            _StartMachine();
        }

        public void _StopMachine()
        {
            try
            {
                bStopFlag = true;
                Thread.Sleep(300);

                if (HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable ||
                    HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Idle)
                    return;

                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Stopping;
                Application.DoEvents();

                if (threadOfRobot != null)
                    threadOfRobot.Join(5000);

                if (threadCamera != null)
                    threadCamera.Join(5000);

                threadOfRobot = null;
                threadCamera = null;

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera1, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>线程 已退出，请重新启动", Color.Red, 9);

                Application.DoEvents();
                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Idle;
                HPRobotCtrl.modebusTCPServer.Write("1006", (short)0);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPRobotCtrl _StopMachine  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmInvoke_StopMachine_Click(object sender, EventArgs e)
        {
            _StopMachine();
        }

        private void btmRobotLearnLF_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotTransit_LToF);

        }

        private void btmRobotLearnUNF_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotTransit_ULToF);
        }

        private void btmRobotLearnVF_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotTransit_VToQA);
        }

        private void btmRobotLearnQA_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotQA);
        }

        private void btmRobotLearnFailed_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotFailed);
        }

        private void btmRobotLearnQA_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotQA.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnQA_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotLearnFailed_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotFailed.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnFailed_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotLearnLF_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotTransit_LToF.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnLF_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotLearnUNF_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotTransit_ULToF.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnUNF_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotLearnVF_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotTransit_VToQA.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnVF_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        public bool Handle_CyliderControl1(bool state)
        {
            try
            {
                if (HPRobotCtrl.ObjRobot.SetIOLine(8, state) != 0)
                {
                    string errorMsg = $"Handle_CyliderControl1 吸取产品汽缸控制失败，确认机器人是连机或传感器是否有感应！\r\n";
                    HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                    HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                    emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                    {
                        return false;
                    }
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Handle_CyliderControl1() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

            return false;
        }

        public bool Handle_CyliderControl(bool bit0 = true, bool bit1 = true, bool bit2 = true, bool bit3 = true, bool bit4 = true, bool bit5 = true, bool bit6 = true, int nDontCareResult = 0, bool[] productInPos = null)
        {
            int value0 = (0x01 << 0);
            int value1 = (0x01 << 1);
            int value2 = (0x01 << 2);
            int value3 = (0x01 << 3);
            int value4 = (0x01 << 4);
            int value5 = (0x01 << 5);
            int value6 = (0x01 << 6);

            try
            {
                if(nDontCareResult == 0)
                {
                    value0 = (bit0 ? (HPRobotCtrl.arryCacthIsEnable[0] ? (0x01 << 0) : 0x00) : 0x00);
                    value1 = (bit1 ? (HPRobotCtrl.arryCacthIsEnable[1] ? (0x01 << 1) : 0x00) : 0x00);
                    value2 = (bit2 ? (HPRobotCtrl.arryCacthIsEnable[2] ? (0x01 << 2) : 0x00) : 0x00);
                    value3 = (bit3 ? (HPRobotCtrl.arryCacthIsEnable[3] ? (0x01 << 3) : 0x00) : 0x00);
                    value4 = (bit4 ? (HPRobotCtrl.arryCacthIsEnable[4] ? (0x01 << 4) : 0x00) : 0x00);
                    value5 = (bit5 ? (HPRobotCtrl.arryCacthIsEnable[5] ? (0x01 << 5) : 0x00) : 0x00);
                    value6 = (bit6 ? (HPRobotCtrl.arryCacthIsEnable[6] ? (0x01 << 6) : 0x00) : 0x00);
                }
                else
                {
                    value0 = HPRobotCtrl.arryCacthIsEnable[0] ? (0x01 << 0) : 0x00;
                    value1 = HPRobotCtrl.arryCacthIsEnable[1] ? (0x01 << 1) : 0x00;
                    value2 = HPRobotCtrl.arryCacthIsEnable[2] ? (0x01 << 2) : 0x00;
                    value3 = HPRobotCtrl.arryCacthIsEnable[3] ? (0x01 << 3) : 0x00;
                    value4 = HPRobotCtrl.arryCacthIsEnable[4] ? (0x01 << 4) : 0x00;
                    value5 = HPRobotCtrl.arryCacthIsEnable[5] ? (0x01 << 5) : 0x00;
                    value6 = HPRobotCtrl.arryCacthIsEnable[6] ? (0x01 << 6) : 0x00;
                }

                if(productInPos != null && productInPos.Length >= 10) //是否有产品
                {
                    value0 = productInPos[0] ? value0 : 0x00;
                    value1=  productInPos[1] ? value1 : 0x00;
                    value2 = productInPos[2] ? value2 : 0x00;
                    value3 = productInPos[3] ? value3 : 0x00;
                    value4 = productInPos[4] ? value4 : 0x00;
                    value5 = productInPos[5] ? value5 : 0x00;
                    value6 = productInPos[6] ? value6 : 0x00;
                }


                int valueAll = value0 | value1 | value2 | value3 | value4 | value5 | value6;

                if (HPRobotCtrl.ObjRobot.SetIOPort8(1, (byte)valueAll) != 0)
                {
                    string errorMsg = $"吸取产品汽缸控制失败，确认机器人是连机或传感器是否有感应！\r\n";
                    HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                    HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                    emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                    {
                        return false;
                    }
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Handle_VacuuControl() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

            return false;
        }

        public bool Handle_WaitVacuuSensor(bool bit0, bool bit1, bool bit2, bool bit3, bool bit4, bool bit5, bool bit6, int SensorWaitTimeOut = 2000)
        {
            try
            {
                int value0 = (bit0 ? (HPRobotCtrl.arryCacthIsEnable[0] ? (0x01 << 0) : 0x00) : 0x00);
                int value1 = (bit1 ? (HPRobotCtrl.arryCacthIsEnable[1] ? (0x01 << 1) : 0x00) : 0x00);
                int value2 = (bit2 ? (HPRobotCtrl.arryCacthIsEnable[2] ? (0x01 << 2) : 0x00) : 0x00);
                int value3 = (bit3 ? (HPRobotCtrl.arryCacthIsEnable[3] ? (0x01 << 3) : 0x00) : 0x00);
                int value4 = (bit4 ? (HPRobotCtrl.arryCacthIsEnable[4] ? (0x01 << 4) : 0x00) : 0x00);
                int value5 = (bit5 ? (HPRobotCtrl.arryCacthIsEnable[5] ? (0x01 << 5) : 0x00) : 0x00);
                int value6 = (bit6 ? (HPRobotCtrl.arryCacthIsEnable[6] ? (0x01 << 6) : 0x00) : 0x00);

                int valueAll = value0 | value1 | value2 | value3 | value4 | value5 | value6;

                REDO:
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(20);
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    if (HPRobotCtrl.ObjRobot.GetIOPort8(0) == valueAll)
                        return true;

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalMilliseconds >= SensorWaitTimeOut)
                    {
                        string errorMsg = $"真空控制失败，确认机器人是连机或真空传感器是否有感应！\r\n如果选择终止将停止机器人运行及取料流程\r\n！！请确保手动操作正确！！！";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        {
                            goto REDO;
                        }
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        {
                            return false;
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Handle_WaitVacuuSensor() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;

        }

        public bool Handle_VacuuControl1(bool state, bool bWaitSensor = false, int SensorWaitTimeOut = 2000)
        {
            int nState = (state ? 1 : 0);
            try
            {
                REDO:
                int nResult = HPRobotCtrl.ObjRobot.SetIOLine(0, state);
                if (nResult != 0)
                    goto ERROR_Handle;

                if (!bWaitSensor)
                    return true;

                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(20);
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    if (HPRobotCtrl.ObjRobot.GetIOLine(8) == nState)
                        return true;

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalMilliseconds >= SensorWaitTimeOut)
                        goto ERROR_Handle;

                }

                ERROR_Handle:
                string errorMsg = $"Handle_VacuuControl1 真空控制失败(Out: {0}, In: {8})，确认机器人是连机或真空传感器是否有感应！\r\n如果选择终止将停止机器人运行及取料流程\r\n！！请确保手动操作正确！！！";
                HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                {
                    goto REDO;
                }
                else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Handle_VacuuControl1() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

            return false;
        }

        public bool Handle_VacuuControl(bool bit0, bool bit1, bool bit2, bool bit3, bool bit4, bool bit5, bool bit6, bool bWaitSensor = false, int SensorWaitTimeOut = 2000,  int nDontCareResult = 0, bool[] productInPos = null)
        {
            int value0 = (0x01 << 0);
            int value1 = (0x01 << 1);
            int value2 = (0x01 << 2);
            int value3 = (0x01 << 3);
            int value4 = (0x01 << 4);
            int value5 = (0x01 << 5);
            int value6 = (0x01 << 6);

            try
            {
                if (nDontCareResult == 0)
                {
                    value0 = (bit0 ? (HPRobotCtrl.arryCacthIsEnable[0] ? (0x01 << 0) : 0x00) : 0x00);
                    value1 = (bit1 ? (HPRobotCtrl.arryCacthIsEnable[1] ? (0x01 << 1) : 0x00) : 0x00);
                    value2 = (bit2 ? (HPRobotCtrl.arryCacthIsEnable[2] ? (0x01 << 2) : 0x00) : 0x00);
                    value3 = (bit3 ? (HPRobotCtrl.arryCacthIsEnable[3] ? (0x01 << 3) : 0x00) : 0x00);
                    value4 = (bit4 ? (HPRobotCtrl.arryCacthIsEnable[4] ? (0x01 << 4) : 0x00) : 0x00);
                    value5 = (bit5 ? (HPRobotCtrl.arryCacthIsEnable[5] ? (0x01 << 5) : 0x00) : 0x00);
                    value6 = (bit6 ? (HPRobotCtrl.arryCacthIsEnable[6] ? (0x01 << 6) : 0x00) : 0x00);
                }
                else
                {
                    value0 = HPRobotCtrl.arryCacthIsEnable[0] ? (0x01 << 0) : 0x00;
                    value1 = HPRobotCtrl.arryCacthIsEnable[1] ? (0x01 << 1) : 0x00;
                    value2 = HPRobotCtrl.arryCacthIsEnable[2] ? (0x01 << 2) : 0x00;
                    value3 = HPRobotCtrl.arryCacthIsEnable[3] ? (0x01 << 3) : 0x00;
                    value4 = HPRobotCtrl.arryCacthIsEnable[4] ? (0x01 << 4) : 0x00;
                    value5 = HPRobotCtrl.arryCacthIsEnable[5] ? (0x01 << 5) : 0x00;
                    value6 = HPRobotCtrl.arryCacthIsEnable[6] ? (0x01 << 6) : 0x00;
                }

                if (productInPos != null && productInPos.Length >= 10) //是否有产品
                {
                    value0 = productInPos[0] ? value0 : 0x00;
                    value1 = productInPos[1] ? value1 : 0x00;
                    value2 = productInPos[2] ? value2 : 0x00;
                    value3 = productInPos[3] ? value3 : 0x00;
                    value4 = productInPos[4] ? value4 : 0x00;
                    value5 = productInPos[5] ? value5 : 0x00;
                    value6 = productInPos[6] ? value6 : 0x00;
                }

                int valueAll = value0 | value1 | value2 | value3 | value4 | value5 | value6;

                REDO:
                int nResult = HPRobotCtrl.ObjRobot.SetIOPort8(0, (byte)valueAll);
                if (nResult != 0)
                    goto ERROR_Handle;

                if (!bWaitSensor)
                    return true;

                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(20);
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    if (HPRobotCtrl.ObjRobot.GetIOPort8(1) == valueAll)
                        return true;

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalMilliseconds >= SensorWaitTimeOut)
                        goto ERROR_Handle;

                }

                ERROR_Handle:
                string errorMsg = $"真空控制失败，确认机器人是连机或真空传感器是否有感应！\r\n\r\n状态: 1: {bit0}, 2: {bit1}, 3: {bit2}, 4: {bit3}, 5: {bit4}, 6: {bit5}, 7: {bit6}\r\n\r\n";
                errorMsg += $"!!!!请对应传感器状态!!!!\r\n继续 将根据实际吸取情况执行\r\n如果选择终止将停止机器人运行及取料流程(产品可能会留在托盘上面)\r\n！！请确保手动操作正确！！！";
                HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                if (actionHandle_result == emActionHandle.ActionHandle_Continue)
                {
                    return true;
                }
                if (actionHandle_result == emActionHandle.ActionHandle_Redo)
                {
                    goto REDO;
                }
                else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Handle_VacuuControl() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

            return false;
        }

        public bool Handle_MoveRobot(string movePos, string PassPos = "")
        {
            try
            {
                REDO:
                if (HPRobotCtrl.ObjRobot.MoveFromPosString(movePos, PassPos) != 0)
                {
                    string errorMsg = $"Handle_MoveRobot！\r\n机器人移动失败！\r\n请手动清除机器人错误，并重新启用机器人。再重新这个动作！\r\n如果选择终止将停止机器人运行及取料流程\r\n！！请确保手动操作正确！！！";
                    HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                    HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                    emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                    {
                        goto REDO;
                    }
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        throw new Exception("Handle_MoveRobot 异常或操作员强行终止");
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Handle_MoveRobot() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                throw new Exception(HPglobal.m_strLastException);
            }
        }

        public struct SafeUpdataUIData
        {
            public DataGridView gridView;
            public int nCellRow;
            public int nCellCol;
            public int nStatusFlag;
            public int nCountOfVacuu;
            public bool bReset;
        }

        private void SafeUpdataLoadTrayStatus(object state)
        {
            try
            {
                SafeUpdataUIData Values = (SafeUpdataUIData)state;
                UpdataLoadTrayStatus(Values.gridView, Values.nCellRow, Values.nCellCol, Values.nStatusFlag, Values.nCountOfVacuu, Values.bReset);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SafeUpdataUIFromArry() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        public void UpdataLoadTrayStatus(DataGridView gridView, int nCellRow, int nCellCol, int nStatusFlag, int nCountOfVacuu, bool bReset = false)
        {
            try
            {
                this.Invoke(new Action(() =>
                {
                    int nCoutOfVacuu = (int)speCoutOfCatchUsed.Value;

                    #region>>>>>>> 把数据复位
                    if (bReset)
                    {
                        for (int nIndex = 0; nIndex < speRows.Value; nIndex++)
                        {
                           // Type typeOfCell = gridView.Columns[0].CellType;

                            if (nCoutOfVacuu >= 1)
                            {
                                gridView[0, nIndex].Style.BackColor = Color.White;
                                gridView[0, nIndex].Value = false;
                            }

                            if (nCoutOfVacuu >= 2)
                            {
                                gridView[1, nIndex].Style.BackColor = Color.White;
                                gridView[1, nIndex].Value = false;
                            }

                            if (nCoutOfVacuu >= 3)
                            {
                                gridView[2, nIndex].Style.BackColor = Color.White;
                                gridView[2, nIndex].Value = false;
                            }

                            if (nCoutOfVacuu >= 4)
                            {
                                gridView[3, nIndex].Style.BackColor = Color.White;
                                gridView[3, nIndex].Value = false;
                            }

                            if (nCoutOfVacuu >= 5)
                            {
                                gridView[4, nIndex].Style.BackColor = Color.White;
                                gridView[4, nIndex].Value = false;
                            }

                            if (nCoutOfVacuu >= 6)
                            {
                                gridView[5, nIndex].Style.BackColor = Color.White;
                                gridView[5, nIndex].Value = false;
                            }

                            if (nCoutOfVacuu >= 7)
                            {
                                gridView[6, nIndex].Style.BackColor = Color.White;
                                gridView[6, nIndex].Value = false;
                            }
                        }
                        return;
                    }
                    #endregion

                    bool bResult_1 = ((nStatusFlag & (0x01 << 0)) == 0 ? false : true); //1号产品结果
                    bool bResult_2 = ((nStatusFlag & (0x01 << 1)) == 0 ? false : true); //2号产品结果
                    bool bResult_3 = ((nStatusFlag & (0x01 << 2)) == 0 ? false : true); //3号产品结果
                    bool bResult_4 = ((nStatusFlag & (0x01 << 3)) == 0 ? false : true); //4号产品结果
                    bool bResult_5 = ((nStatusFlag & (0x01 << 4)) == 0 ? false : true); //5号产品结果
                    bool bResult_6 = ((nStatusFlag & (0x01 << 5)) == 0 ? false : true); //6号产品结果
                    bool bResult_7 = ((nStatusFlag & (0x01 << 6)) == 0 ? false : true); //8号产品结果

                    if (nCountOfVacuu >= 1)
                    {
                        gridView[nCellCol * nCountOfVacuu + 0, nCellRow].Value = bResult_1;
                        if (!bResult_1)
                            gridView[nCellCol * nCountOfVacuu + 6, nCellRow].Style.BackColor = Color.Red;
                    }

                    if (nCountOfVacuu >= 2)
                    {
                        gridView[nCellCol * nCountOfVacuu + 1, nCellRow].Value = bResult_2;
                        if (!bResult_2)
                            gridView[nCellCol * nCountOfVacuu + 1, nCellRow].Style.BackColor = Color.Red;
                    }

                    if (nCountOfVacuu >= 3)
                    {
                        gridView[nCellCol * nCountOfVacuu + 2, nCellRow].Value = bResult_3;
                        if (!bResult_3)
                            gridView[nCellCol * nCountOfVacuu + 3, nCellRow].Style.BackColor = Color.Red;
                    }

                    if (nCountOfVacuu >= 4)
                    {
                        gridView[nCellCol * nCountOfVacuu + 3, nCellRow].Value = bResult_4;
                        if (!bResult_4)
                            gridView[nCellCol * nCountOfVacuu + 3, nCellRow].Style.BackColor = Color.Red;
                    }

                    if (nCountOfVacuu >= 5)
                    {
                        gridView[nCellCol * nCountOfVacuu + 4, nCellRow].Value = bResult_5;
                        if (!bResult_5)
                            gridView[nCellCol * nCountOfVacuu + 4, nCellRow].Style.BackColor = Color.Red;
                    }

                    if (nCountOfVacuu >= 6)
                    {
                        gridView[nCellCol * nCountOfVacuu + 5, nCellRow].Value = bResult_6;
                        if (!bResult_6)
                            gridView[nCellCol * nCountOfVacuu + 5, nCellRow].Style.BackColor = Color.Red;
                    }

                    if (nCountOfVacuu >= 7)
                    {
                        gridView[nCellCol * nCountOfVacuu + 6, nCellRow].Value = bResult_7;
                        if (!bResult_7)
                            gridView[nCellCol * nCountOfVacuu + 6, nCellRow].Style.BackColor = Color.Red;
                    }
                }));
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("UpdataLoadTrayStatus() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void SetTrayCellFlag(DataGridView gridView, int nRow, int Col , bool bFlag)
        {
            try
            {
                this.Invoke(new Action(()=>gridView[Col, nRow].Value = bFlag));
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SetTrayCellFlag() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private bool ResupplyCellForBuff(Point EndLoadPoint, bool bForceCleare = false)
        {
            bool bResupplyResult = true;
            UpateCustRowsDst();
            Color disPlayColor = Color.CadetBlue;
            int nFontSize = 8;

            try
            {
                #region>>>>>>确认机器人安全信号 设备安全位
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>ResupplyCellForBuff 确认机器人安全信号D1310 -> 1", Color.Blue, 9);
                while (true)
                {
                    Thread.Sleep(50);
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return true;

                    OperateResult<short> robotSafeMove = HPRobotCtrl.PLC_Unload.ReadInt16("D1310");
                    if (robotSafeMove.Content == 1)
                        break;
                }
                #endregion

                #region>>>>>>确认料盘是否存在及等待上好料盘
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>{"ResupplyCellForBuff 确认料盘是否存在及等待上好料盘......"}", Color.Blue, 9);
                while (true)
                {
                    Thread.Sleep(100);
                    OperateResult<Int16> status1 = HPRobotCtrl.PLC_Unload.ReadInt16("D1307");
                    OperateResult<Int16> status2 = HPRobotCtrl.PLC_Unload.ReadInt16("D1306");
                    if (status1.IsSuccess && status1.Content == 0 && status2.IsSuccess && status2.Content == 1)
                        break;

                    if (HPglobal.m_bQuitApp)
                        return true;
                }

                #endregion

                //得到上料盘空料信息
                Point[] LoadEmptyPos = GetEmptyPosArryOfTray(true);

                #region>>>>>>已补料到最后一个，直接返回true
                if (EndLoadPoint != null &&
                    EndLoadPoint.X >= 0 &&
                    EndLoadPoint.Y >= 0 &&
                    LoadEmptyPos[0].X >= EndLoadPoint.X &&
                    LoadEmptyPos[0].Y >= EndLoadPoint.Y)
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>补料成功，当前盘已是最后一个", disPlayColor, nFontSize);
                    return true;
                }
                #endregion

                if (bForceCleare == false && LoadEmptyPos.Length <= 0)
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>强制清料或当前上料料盘已满", disPlayColor, nFontSize);
                    return true;
                }

                //得到缓存盘有料信息
                Point[] InPosOfBuffTray = GetInPosArryOfTray(false);
                if (InPosOfBuffTray.Length <= 0)
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>缓存盘没有料，无法补料", Color.Red, nFontSize);
                    return false;
                }

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>开始补料", Color.Blue, 9);

                //安全信号复位
                HPRobotCtrl.PLC_Unload.Write("D1309", 0);

                #region>>>>>>移动到过渡过位
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>移动到过渡过位", disPlayColor, nFontSize);
                Handle_MoveRobot(txtRobotTransit_LToF.Text.Trim());
                #endregion

                #region>>>>>>移动到上料盘补料第一个位置
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>移动到上料盘补料第一个位置", disPlayColor, nFontSize);
                RobotBase.RobotPosition strcutPos = RobotBase.RobotPosition.Parse(txtRobotPos_LoadFirst2.Text.Trim());
                strcutPos.z = 0.00;
                string newPos = strcutPos.ToString();
                Handle_MoveRobot(newPos);
                #endregion

                #region>>>>>>开始循环补料
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>开始循环补料", Color.Magenta, nFontSize);
                for (int nIndexPos = 0; nIndexPos < LoadEmptyPos.Length; nIndexPos++)
                {
                    STARTREPLAY:

                    #region>>>>>>已补料到最后一个
                    if (EndLoadPoint != null &&
                        EndLoadPoint.X >= 0 &&
                        EndLoadPoint.Y >= 0 &&
                        LoadEmptyPos[nIndexPos].X  >=EndLoadPoint.X  &&
                        LoadEmptyPos[nIndexPos].Y  >=EndLoadPoint.Y )
                    {
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>已补料到最后一个", disPlayColor, nFontSize);
                        break;
                    }
                    #endregion

                    #region>>>>>>移动缓存盘盘补料第一个有料位置
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>移动缓存盘盘补料第一个有料位置", disPlayColor, nFontSize);
                    Point []pBuff = GetInPosArryOfTray(false);
                    if (pBuff == null || pBuff.Length <= 0 || pBuff[0].X <= -1 || pBuff[0].Y <= -1)
                    {
                        string errorMsg = $"补料失败！没有足够的缓存料\r\n!!!缓存盘已经没有物料或当前位置不存在有料的情况!!!\r\n！！请修正或放入新的缓存盘\r\n\r\n！！！";
                        errorMsg += $"继续，或重做 表示人工已放入一个满料的缓存的盘\r\n\r\n";
                        errorMsg += $"停止，或终止 手工停止当前的补料动作\r\n\r\n";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9);
                        bResupplyResult = false;
                        goto ENDREPLAY;

                        #region>>>>>>1111111111111

                        //                         emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        //                         if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        //                         {
                        //                             SetTrayFullOrEmpty(false, true);
                        //                             goto STARTREPLAY;
                        //                         }
                        //                         else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        //                         {
                        //                             HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>补料失败，操作员没有放补料盘并终止", Color.Blue, 9);
                        //                             bResupplyResult = false;
                        //                             goto ENDREPLAY;
                        //                         }
                        #endregion
                    }

                    strcutPos = RobotBase.RobotPosition.Parse(txtRobotPos_BuffFirst2.Text.Trim());
                    strcutPos.x -= ((pBuff[0].X * (double)speRowOfSingleDst.Value) + cust_row[pBuff[0].X]); 
                    strcutPos.y -= (pBuff[0].Y * (double)speColOfSingleDst.Value);
                    newPos = strcutPos.ToString();
                    Handle_MoveRobot(newPos);
                    #endregion

                    #region>>>>>>降下汽缸
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>降下汽缸", disPlayColor, nFontSize);
                    Handle_CyliderControl1( true);
                    #endregion

                    #region>>>>>>打开真空
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>打开真空", disPlayColor, nFontSize);
                    Handle_VacuuControl1(true, true, 2000); //开启真空
                    #endregion

                    #region>>>>>>升起汽缸
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>升起汽缸", disPlayColor, nFontSize);
                    Handle_CyliderControl(false, false, false, false, false, false, false);
                    #endregion

                    #region>>>>>>移动到上料盘需要补料的位置
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>移动到上料盘需要补料的位置", disPlayColor, nFontSize);
                    strcutPos = RobotBase.RobotPosition.Parse(txtRobotPos_LoadFirst2.Text.Trim());
                    strcutPos.x -= ((LoadEmptyPos[nIndexPos].X * (double)speRowOfSingleDst.Value) + cust_row[LoadEmptyPos[nIndexPos].X]); 
                    strcutPos.y -= (LoadEmptyPos[nIndexPos].Y * (double)speColOfSingleDst.Value);
                    newPos = strcutPos.ToString();
                    Handle_MoveRobot(newPos);
                    #endregion

                    #region>>>>>>检查料是否在吸嘴上
                    if (HPRobotCtrl.ObjRobot.GetIOLine(8) != 1)
                    {
                        string errorMsg = $"ResupplyCellForBuff 补料失败，补料时发现吸嘴上没有料，可能料没吸上或料丢失\r\n如果人工补料完成(请手动清除缓存盘有料标志)请点击 继续\r\n";
                        errorMsg += $"如果无法人工补料，请将物料重新放在缓存盘位置点击其它键(重做/停止/终止)";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Continue)
                        {
                          
                        }
                        else
                        {
                            goto STARTREPLAY;
                        }
                    }

                    #endregion

                    #region>>>>>>降下汽缸并关闭真空
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>降下汽缸并关闭真空", disPlayColor, nFontSize);
                    Handle_CyliderControl1( true);
                    Handle_VacuuControl1(false);
                    Thread.Sleep(150);
                    #endregion

                    #region>>>>>>升起汽缸
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>升起汽缸", disPlayColor, nFontSize);
                    Handle_CyliderControl(false, false, false, false, false, false, false);
                    #endregion

                    #region>>>>>>设置料是否已放好或料是否已取走标志
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>设置料是否已放好或料是否已取走标志", disPlayColor, nFontSize);
                    SetTrayCellFlag(gridLoadTrayResult, LoadEmptyPos[nIndexPos].X, LoadEmptyPos[nIndexPos].Y, true);
                    SetTrayCellFlag(gridBuffTrayResult, pBuff[0].X, pBuff[0].Y, false);
                    Application.DoEvents();
                    #endregion

                    #region>>>>>>更新已下料及包装数据
                    short nCurrentCount = HPRobotCtrl.PLC_Unload.ReadInt16("D1061").Content;
                    HPRobotCtrl.CurrentUnitUnloadedToPackingStation = nCurrentCount + 1;
                    Thread.Sleep(50);
                    HPRobotCtrl.PLC_Unload.Write("D1061", (short)HPRobotCtrl.CurrentUnitUnloadedToPackingStation);
                    this.Invoke(new Action(() => { spedCurrentUnitUnloadedToPackingStation.Value = (short)HPRobotCtrl.CurrentUnitUnloadedToPackingStation; }));

                    #endregion

                }
                #endregion

                ENDREPLAY:
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>关闭所有真空", disPlayColor, nFontSize);
                Handle_VacuuControl(false, false, false, false, false, false, false, chkVacuuSensor.Checked, 5000, 0);

                #region>>>>>>升起汽缸
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>升起汽缸", disPlayColor, nFontSize);
                Handle_CyliderControl(false, false, false, false, false, false, false);
                Thread.Sleep(150);
                #endregion

                #region>>>>>>移动到上料盘补料第一个位置
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>移动到上料盘补料第一个位置", disPlayColor, nFontSize);
                strcutPos = RobotBase.RobotPosition.Parse(txtRobotPos_LoadFirst2.Text.Trim());
                strcutPos.z = 0.00;
                newPos = strcutPos.ToString();
                Handle_MoveRobot(newPos);
                #endregion

                #region>>>>>>移动到过渡过位
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>移动到过渡过位", disPlayColor, nFontSize);
                Handle_MoveRobot(txtRobotTransit_LToF.Text.Trim());
                #endregion

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>补料成功完成.........", Color.Blue, 9);

                return bResupplyResult;
            }
            catch(Exception ex)
            {
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>补料异常.........", Color.Red, 9);

                #region>>>>>>移动到上料盘补料第一个位置
                RobotBase.RobotPosition strcutPos = RobotBase.RobotPosition.Parse(txtRobotPos_LoadFirst2.Text.Trim());
                strcutPos.z = 0.00;
                string newPos = strcutPos.ToString();
                Handle_MoveRobot(newPos);
                #endregion

                #region>>>>>>移动到过渡过位
                Handle_MoveRobot(txtRobotTransit_LToF.Text.Trim());
                #endregion

                bResupplyResult = false;
                HPglobal.m_strLastException = string.Format("ResupplyCellForBuff() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;
        }

        private bool[] vistion2TestResults = new bool[10];
        public bool VisionTest2_Execute(HalconDotNet.HImage[] image)
        {
            try
            {
                for (int nIndex = 0; nIndex < 10; nIndex++)
                    vistion2TestResults[nIndex] = false;

                #region>>>>>>>>初始化每个方法的结果
                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest2_1.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest2_1.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest2_2.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest2_2.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest2_3.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest2_3.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest2_4.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest2_4.CheckResult[nIndexRow].ResultStatus = false;
                }

                for (int nIndexRow = 0; nIndexRow < ParameterAllObj.VisionTest2_5.CheckResult.Length; nIndexRow++)
                {
                    ParameterAllObj.VisionTest2_5.CheckResult[nIndexRow].ResultStatus = false;
                }
                #endregion

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>VisionTest2_Execute 开始进行VistionTest2.....", Color.Magenta, 9);

                #region>>>>>>>>执行方法1
                if (!ParameterAllObj.VisionTest2_1.Init("", chkNewTemplate.Checked))
                    return false;

                if (!ParameterAllObj.VisionTest2_1.ImageProcess(image[0]))
                    return false;

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机2_1的测试结果", Color.Magenta, 9);
                

                for (int nIndex = 0; nIndex < ParameterAllObj.VisionTest2_1.CheckResult.Length; nIndex++)
                    vistion2TestResults[nIndex] = ParameterAllObj.VisionTest2_1.CheckResult[nIndex].ResultStatus;

                #endregion

                #region>>>>>>>>执行方法2
                if (chkLaserMarkTest_Down.Checked)
                {
                    if (!ParameterAllObj.VisionTest2_2.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest2_2.ImageProcess(image[1]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机2_2的测试结果", Color.Magenta, 9);
                   
                    for (int nIndex = 0; nIndex < ParameterAllObj.VisionTest2_2.CheckResult.Length; nIndex++)
                        vistion2TestResults[nIndex] &= ParameterAllObj.VisionTest2_2.CheckResult[nIndex].ResultStatus;
                }
                #endregion

                #region>>>>>>>>执行方法3
                if (ParameterAllObj.VisionTest2_3.Enabled)
                {
                    if (!ParameterAllObj.VisionTest2_3.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest2_3.ImageProcess(image[2]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机2_3的测试结果", Color.Magenta, 9);
                   
                    for (int nIndex = 0; nIndex < ParameterAllObj.VisionTest2_3.CheckResult.Length; nIndex++)
                        vistion2TestResults[nIndex] &= ParameterAllObj.VisionTest2_3.CheckResult[nIndex].ResultStatus;
                }
                #endregion

                #region>>>>>>>>执行方法4
                if (ParameterAllObj.VisionTest2_4.Enabled)
                {
                    if (!ParameterAllObj.VisionTest2_4.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest2_4.ImageProcess(image[3]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机2_4的测试结果", Color.Magenta, 9);
                   
                    for (int nIndex = 0; nIndex < ParameterAllObj.VisionTest2_4.CheckResult.Length; nIndex++)
                        vistion2TestResults[nIndex] &= ParameterAllObj.VisionTest2_4.CheckResult[nIndex].ResultStatus;
                }
                #endregion

                #region>>>>>>>>执行方法5
                if (ParameterAllObj.VisionTest2_5.Enabled)
                {
                    if (!ParameterAllObj.VisionTest2_5.Init("", chkNewTemplate.Checked))
                        return false;

                    if (!ParameterAllObj.VisionTest2_5.ImageProcess(image[4]))
                        return false;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>设置相机2_5的测试结果", Color.Magenta, 9);
                   
                    for (int nIndex = 0; nIndex < ParameterAllObj.VisionTest2_5.CheckResult.Length; nIndex++)
                        vistion2TestResults[nIndex] &= ParameterAllObj.VisionTest2_5.CheckResult[nIndex].ResultStatus;
                }
                #endregion

                HPRobotCtrl.Camera2.ShowImage(image[0]);
                if (ParameterAllObj.VisionTest2_5.Enabled)
                    HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_5.CheckResult, 0.00, 0.00, false);
                //this.Invoke(new Action(() => HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_5.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest2_4.Enabled)
                    HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_4.CheckResult, 0.00, 0.00, false);
                //this.Invoke(new Action(() => HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_4.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest2_3.Enabled)
                    HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_3.CheckResult, 0.00, 0.00, false);
                //this.Invoke(new Action(() => HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_3.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest2_2.Enabled)
                    HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_2.CheckResult, 0.00, 0.00, false);
                //this.Invoke(new Action(() => HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_2.CheckResult, 0.00, 0.00, false)));

                if (ParameterAllObj.VisionTest2_1.Enabled)
                    HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_1.CheckResult, 0.00, 0.00, false);
                    //this.Invoke(new Action(() => HPRobotCtrl.Camera2.ShowResult(ParameterAllObj.VisionTest2_1.CheckResult, 0.00, 0.00, false)));

                return true;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"VisionTest2_Execute() 异常: {ex.ToString()}\r\n";
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>{HPglobal.m_strLastException}", Color.Red, 9);
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }

        }

        public bool UnloadCell(bool WhereTrayDefaulUnload,
                                              int nFromCellRow, int nFromCellCol,
                                              int nToCellRow, int nToCellCol, 
                                              int nResultsCamera1Flag, 
                                              bool bNeedVisionCheck, bool bQACheck = false, 
                                              bool bMoveToTrasferPos = true /*是否每一次首先先都移到过渡位*/)
        {
            short passedCurr = 0;
            short failedCurr = 0;
            short currentFailedCount = 0;
            lock(ojectLoadCell)
            {
                try
                {
                    RECHECK:
                    if (ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 0)
                    {
                        string errorMsg = $"UnloadCell 注意: 当前定义的生产总量已经达到！\r\n\r\n请将总出料盘设为0或重新定义出货总量 之后点击  继续 \r\n\r\n";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        return true;
                    }

                    bool bVistionTestResult = false;
                    bool isOnQAPos = false;
                    Color disPlayColor = Color.CadetBlue;
                    int nFontSize = 8;

                    short ncoutOfNumCatcher = (short)speCoutOfCatchUsed.Value;
                    UpateCustRowsDst();

                    //得到产品有无的信息

                    bool[,] InposFlag = GetVistion1ProductInPos(HPRobotCtrl.strTrayIDName2);
                    bool[] CatchInPosFlag = new bool[10];
                   for(int nIndex = 0; nIndex < 10; nIndex++)
                    {
                        if (InposFlag == null)
                            CatchInPosFlag[nIndex] = true;
                        else
                            CatchInPosFlag[nIndex] = InposFlag[nFromCellRow, nIndex];
                    }

                    #region>>>>>> 设定抓取结果
                    Int16[,] FlagResult = null;
                    bool bLastFlag_1 = true;
                    bool bLastFlag_2 = true;
                    bool bLastFlag_3 = true;
                    bool bLastFlag_4 = true;
                    bool bLastFlag_5 = true;
                    bool bLastFlag_6 = true;
                    bool bLastFlag_7 = true;
                    if (btm进入尾料模式.Checked)
                    {
                        HPRobotCtrl.dataBaseSQL.SQL_GetLastModeFlagAllProcessFromTray(txtTrayID2.Text.Trim(), out FlagResult);
                        if(FlagResult != null && FlagResult.Length > 10)
                        {
                            bLastFlag_1 = FlagResult[nFromCellRow, 0] == 0x01 ? true : false;
                            bLastFlag_2 = FlagResult[nFromCellRow, 1] == 0x01 ? true : false;
                            bLastFlag_3 = FlagResult[nFromCellRow, 2] == 0x01 ? true : false;
                            bLastFlag_4 = FlagResult[nFromCellRow, 3] == 0x01 ? true : false;
                            bLastFlag_5 = FlagResult[nFromCellRow, 4] == 0x01 ? true : false;
                            bLastFlag_6 = FlagResult[nFromCellRow, 5] == 0x01 ? true : false;
                            bLastFlag_7 = FlagResult[nFromCellRow, 6] == 0x01 ? true : false;
                        }
                        else
                        {
                            string errorMsg = $"UnloadCell 失败:\r\nSQL_GetLastModeFlagAllProcessFromTray 没能读取最后打码数量的标志, 请检查";
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9);
                            HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                            emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                            if (actionHandle_result == emActionHandle.ActionHandle_Redo)
                                goto RECHECK;
                            else
                            {
                            }
                        }
                    }

                    bool bResult_1 = ((nResultsCamera1Flag & (0x01 << 0)) == 0 ? false : true) & bLastFlag_1; //1号产品结果
                    bool bResult_2 = ((nResultsCamera1Flag & (0x01 << 1)) == 0 ? false : true) & bLastFlag_2; //2号产品结果
                    bool bResult_3 = ((nResultsCamera1Flag & (0x01 << 2)) == 0 ? false : true) & bLastFlag_3; //3号产品结果
                    bool bResult_4 = ((nResultsCamera1Flag & (0x01 << 3)) == 0 ? false : true) & bLastFlag_4; //4号产品结果
                    bool bResult_5 = ((nResultsCamera1Flag & (0x01 << 4)) == 0 ? false : true) & bLastFlag_5; //5号产品结果
                    bool bResult_6 = ((nResultsCamera1Flag & (0x01 << 5)) == 0 ? false : true) & bLastFlag_6; //6号产品结果
                    bool bResult_7 = ((nResultsCamera1Flag & (0x01 << 6)) == 0 ? false : true) & bLastFlag_7; //7号产品结果

                    #endregion

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 取第 {nFromCellRow} 行第 {nFromCellCol} 例产品", Color.Blue, 9);

                    string strVistionResult = $"1: {bResult_1.ToString()}; 2: {bResult_2.ToString()}; 3: {bResult_3.ToString()}; 4: {bResult_4.ToString()}; 5: {bResult_5.ToString()}; 6: {bResult_6.ToString()}; 7: {bResult_7.ToString()}";
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 当前相机1测试结果：({strVistionResult})", disPlayColor, nFontSize);

                    Handle_CyliderControl(false, false, false, false, false, false, false); //升起汽缸

                    #region>>>>>>移动到过渡过位
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 移动到过渡过位", disPlayColor, nFontSize);
                    if (bMoveToTrasferPos)
                        Handle_MoveRobot(txtRobotTransit_LToF.Text.Trim());
                    #endregion

                    #region>>>>>>机器人走到第指定例吸料
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 机器人走到第指定例吸料", disPlayColor, nFontSize);
                    RobotBase.RobotPosition strcutPos = RobotBase.RobotPosition.Parse(txtRobotCatch.Text.Trim());
                    strcutPos.x -= ((nFromCellRow * (double)speRowDst.Value) + cust_row[nFromCellRow]);
                    strcutPos.y -= (nFromCellCol * (double)speColDst.Value);
                    string newPos = strcutPos.ToString();

                    //开启真空
                    Task taskNew = Task.Factory.StartNew(() => Handle_VacuuControl(bResult_1, bResult_2, bResult_3, bResult_4, bResult_5, bResult_6, bResult_7, false, 10000, btm进入尾料模式.Checked ? 0 : 1, CatchInPosFlag));

                    //移动到位置
                    Handle_MoveRobot(newPos);

                    //降下汽缸
                    Handle_CyliderControl(bResult_1, bResult_2, bResult_3, bResult_4, bResult_5, bResult_6, bResult_7, btm进入尾料模式.Checked  ? 0 : 1, CatchInPosFlag); 
                    Thread.Sleep(150);
                    taskNew.Wait();
                    taskNew.Dispose();

                    //吸料
                    if (!Handle_VacuuControl(bResult_1, bResult_2, bResult_3, bResult_4, bResult_5, bResult_6, bResult_7, chkVacuuSensor.Checked, 2000, btm进入尾料模式.Checked ? 0 : 1, CatchInPosFlag))
                    {
                        Handle_CyliderControl(false, false, false, false, false, false, false);
                        goto END_MOVE;
                    }

                    //升起汽缸
                    Handle_CyliderControl(false, false, false, false, false, false, false);
                    #endregion

                    #region>>>>>>如果所有产品都是坏品，直接扔坏品位
                    if (强制camera2结果.Checked == false && nResultsCamera1Flag == 0x00) //如果所有产品都是坏品，直接扔坏品位
                    {
                        //设置统计结果
                        UpdateTotalPassedFailed(0, ncoutOfNumCatcher);

                        OperateResult<Int16> countOfPLC2 = HPRobotCtrl.PLC_Unload.ReadInt16("D1063");
                        HPRobotCtrl.PLC_Unload.Write("D1063", (short)countOfPLC2.Content + ncoutOfNumCatcher);

                        //移动到坏品位
                        Handle_MoveRobot(txtRobotFailed.Text.Trim());

                        //断开真空
                        Handle_VacuuControl(false, false, false, false, false, false, false, chkVacuuSensor.Checked, 2000, 0);
                        Thread.Sleep(150);

                        goto END_MOVE;
                    }
                    #endregion

                    #region>>>>>>移动到拍照位置
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 移动到拍照位置", disPlayColor, nFontSize);
                    Handle_MoveRobot(txtRobotCamera.Text.Trim());
                    #endregion

                    HPRobotCtrl.PLC_Unload.Write("D1309", 1);

                    #region>>>>>>开始进行拍照
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 开始进行拍照", disPlayColor, nFontSize);
                    Thread.Sleep(50);
                    HalconDotNet.HImage[] image = new HImage[5];
                    MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest2_1, ParameterAllObj.VisionTest2_2, ParameterAllObj.VisionTest2_3, ParameterAllObj.VisionTest2_4, ParameterAllObj.VisionTest2_5 };
                    
                    for (int nIndex = 0; nIndex < 5; nIndex++)
                    {
                        if (methods[nIndex].Enabled)
                        {
                            HPRobotCtrl.Camera2.SetCameraExposure(methods[nIndex].Exposure * 1000.00);
                            Thread.Sleep((int)spedDelayCameraSet.Value);
                            for (int nCount = 0; nCount < 5; nCount++)
                            {
                                HalconForm.visionResult result = new HalconForm.visionResult();
                                if ((result = HPRobotCtrl.Camera2.SnapImage(out image[nIndex])).IsSuccess == false || !HalconGeneral.IsImageObj(image[nIndex]))
                                {
                                    HPglobal.RecordeAllMessage($"相机2采集图像失败, 重新采集!: {result.ErrorMsg}", true, false, "");
                                    Thread.Sleep(300);
                                }
                                else
                                    break;
                            }

                            Thread.Sleep(20);
                        }
                    }
                    #endregion

                    #region>>>>>>开始进行视觉处理并处理结果数据
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 开始进行视觉处理", disPlayColor, nFontSize);
                    bVistionTestResult = false;

                    //异步进行图像处理
                    Task<bool> taskImageProcess = Task.Factory.StartNew<bool>(() =>
                    {
                        try
                        {
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell: 异步图像处理， 第{nFromCellRow}行第{nFromCellCol}例", Color.Magenta, nFontSize);

                            lock (objImageProcess)
                            {
                                bVistionTestResult = VisionTest2_Execute(image);
                            }

                            //设置统计结果
                            for (int nIndex = 0; nIndex < ncoutOfNumCatcher; nIndex++)
                            {
                                if (vistion2TestResults[nIndex])
                                    passedCurr++;
                                else
                                    failedCurr++;
                            }

                            HPRobotCtrl.PassedCamera2 += passedCurr;
                            HPRobotCtrl.FailedCamera2 += failedCurr;
                            UpdateTotalPassedFailed(passedCurr, failedCurr);

                            //合并Camera1的拍照结果
                            bool[] arryResult1 = new bool[10];
                            for (int nIndex = 0; nIndex < 10; nIndex++)
                                arryResult1[nIndex] = ((nResultsCamera1Flag & (((int)0x01) << nIndex)) == 0x00 ? false : true);

                            //合并烧录的拍照结果
                            short[] arryResultFlash = null;
                            if (chkCareFlashResult.Checked)
                                arryResultFlash = HPRobotCtrl.GetFlashAreaDataRow(nFromCellRow, txtTrayID2.Text.Trim(), false);

                            //将新的结果再显示出来
                            Int32 newResultFlag = 0x00;
                            HPRobotCtrl.bArryCamera2_Result = SetCamera2Result(arryResult1, arryResultFlash, ref newResultFlag);
                            if (HPRobotCtrl.bArryCamera2_Result == null)
                            {
                                bVistionTestResult = false;
                                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell :SetCamera2Result 设置相机2结果失败", Color.Red, nFontSize);
                            }
                            else
                            {
                                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queCamera2, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell: 图像处理完成{nFromCellRow}行第{nFromCellCol}例，结果为 {bVistionTestResult}", Color.Magenta, nFontSize);
                            }
                        }
                        catch(Exception ex)
                        {
                            HPglobal.m_strLastException = string.Format("UnloadCell  taskImageProcess 执行其间发生错误\r\n{0}", ex.ToString());
                            HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                        }

                        return bVistionTestResult;
                    });
                    #endregion

                    #region>>>>>>确认料盘是否存在及等待上好料盘
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell 确认料盘是否存在及等待上好料盘......", Color.Blue, nFontSize);
                    while (true)
                    {
                        Thread.Sleep(100);
                        OperateResult<Int16> status1 = HPRobotCtrl.PLC_Unload.ReadInt16("D1307");
                        OperateResult<Int16> status2 = HPRobotCtrl.PLC_Unload.ReadInt16("D1306");
                        if (status1.IsSuccess && status1.Content == 0 && status2.IsSuccess && status2.Content == 1)
                            break;

                        if (HPglobal.m_bQuitApp)
                            return true;
                    }

                    #endregion

                    //安全信号复位
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell 安全信号复位......", Color.Blue, nFontSize);
                    HPRobotCtrl.PLC_Unload.Write("D1309", 0);

                    #region>>>>>>移到下料位置，如果有QA抽检就把产品放在QA位, 否则就移动到下料位
                    short needQA = HPRobotCtrl.PLC_Unload.ReadInt16("D1301").Content;
                    isOnQAPos = false;
                    if (needQA == 1)
                    {
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 下料位到QA抽位", disPlayColor, nFontSize);
                       
                        //移动到QA过渡过位
                        Handle_MoveRobot(txtRobotTransit_VToQA.Text.Trim());

                        //移动到抽检位
                        Handle_MoveRobot(txtRobotQA.Text.Trim());
                        isOnQAPos = true;
                    }
                    else
                    {
                        #region>>>>>>开始移动到好品/缓存下料位置
                        RobotBase.RobotPosition strcutPos2;
                        if (WhereTrayDefaulUnload)
                        {
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 下料位到 好品位", disPlayColor, nFontSize);

                            strcutPos2 = RobotBase.RobotPosition.Parse(txtRobotUnload.Text.Trim());
                        }
                        else
                        {
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 下料位到 缓存位", disPlayColor, nFontSize);

                            #region>>>>>>移动到过渡过位
                            Handle_MoveRobot("-251.96, 146.38, 0, 136.51, /R");

                            strcutPos2 = RobotBase.RobotPosition.Parse(txtRobotPos_BuffFirst.Text.Trim());

                            #endregion
                        }

                        strcutPos2.x -= ((nToCellRow * (double)speRowDst.Value) + +cust_row[nToCellRow]);
                        strcutPos2.y -= (nToCellCol * (double)speColDst.Value);
                        string newPos2 = strcutPos2.ToString();
                        Handle_MoveRobot(newPos2);
                        #endregion
                    }
                    #endregion

                    #region>>>>>>等待图像处理完成
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 等待图像处理完成", disPlayColor, nFontSize);
                    Thread.Sleep(100);
                    while (taskImageProcess.Status == TaskStatus.WaitingToRun || taskImageProcess.Status == TaskStatus.Running)
                    {
                        if (HPglobal.m_bQuitApp || bStopFlag)
                            return false;

                        Application.DoEvents();
                    }

                    taskImageProcess.Wait();
                    taskImageProcess.Dispose();
                    #endregion

                    #region>>>>>>重新更新图像测试结果(!!!!! 非常重要  !!!!!!!!)
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 重新更新图像测试结果(!!!!! 非常重要  !!!!!!!!)", disPlayColor, nFontSize);
                    bResult_1 = (HPRobotCtrl.bArryCamera2_Result == null ? false : HPRobotCtrl.bArryCamera2_Result[0]); //1号产品结果
                    bResult_2 = (HPRobotCtrl.bArryCamera2_Result == null ? false : HPRobotCtrl.bArryCamera2_Result[1]); //2号产品结果
                    bResult_3 = (HPRobotCtrl.bArryCamera2_Result == null ? false : HPRobotCtrl.bArryCamera2_Result[2]); //3号产品结果
                    bResult_4 = (HPRobotCtrl.bArryCamera2_Result == null ? false : HPRobotCtrl.bArryCamera2_Result[3]); //4号产品结果
                    bResult_5 = (HPRobotCtrl.bArryCamera2_Result == null ? false : HPRobotCtrl.bArryCamera2_Result[4]); //5号产品结果
                    bResult_6 = (HPRobotCtrl.bArryCamera2_Result == null ? false : HPRobotCtrl.bArryCamera2_Result[5]); //6号产品结果
                    bResult_7 = (HPRobotCtrl.bArryCamera2_Result == null ? false : HPRobotCtrl.bArryCamera2_Result[6]); //7号产品结果

                    this.Invoke(new Action(() =>
                    {
                        if (needQA == 1)
                            currentFailedCount = (short)speCoutOfCatchUsed.Value;
                        else
                        {
                            if (!bResult_1 && speCoutOfCatchUsed.Value >= 1)
                                currentFailedCount++;

                            if (!bResult_2 && speCoutOfCatchUsed.Value >= 2)
                                currentFailedCount++;

                            if (!bResult_3 && speCoutOfCatchUsed.Value >= 3)
                                currentFailedCount++;

                            if (!bResult_4 && speCoutOfCatchUsed.Value >= 4)
                                currentFailedCount++;

                            if (!bResult_5 && speCoutOfCatchUsed.Value >= 5)
                                currentFailedCount++;

                            if (!bResult_6 && speCoutOfCatchUsed.Value >= 6)
                                currentFailedCount++;

                            if (!bResult_7 && speCoutOfCatchUsed.Value >= 7)
                                currentFailedCount++;
                        }
                    }));

                    OperateResult<Int16> countOfPLC = HPRobotCtrl.PLC_Unload.ReadInt16("D1063");
                    currentFailedCount += countOfPLC.Content;
                    HPRobotCtrl.PLC_Unload.Write("D1063", (short)currentFailedCount);

                    #endregion

                    #region>>>>>>开始放料并设放料结果(根据吸嘴的个数), 放下汽缸，断开真空下料
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 开始放料并设放料结果(根据吸嘴的个数), 放下汽缸，断开真空下料", disPlayColor, nFontSize);

                    //检查一下所有的真空
                    int nValusOfCatched = HPRobotCtrl.ObjRobot.GetIOPort8(1);

                    //放下汽缸
                    Handle_CyliderControl(bResult_1, bResult_2, bResult_3, bResult_4, bResult_5, bResult_6, bResult_7);

                    //断开真空
                    Handle_VacuuControl(!bResult_1, !bResult_2, !bResult_3, !bResult_4, !bResult_5, !bResult_6, !bResult_7, false, 2000, 0);
                    Thread.Sleep(100);

                    #endregion

                    #region>>>>>>检查是否有可能坏品会掉下去
                    int nValus = HPRobotCtrl.ObjRobot.GetIOPort8(1);

                    int value0 = (!bResult_1 ? ((HPRobotCtrl.arryCacthIsEnable[0] && bLastFlag_1 && CatchInPosFlag[0]) ? (0x01 << 0) : 0x00) : 0x00);
                    int value1 = (!bResult_2 ? ((HPRobotCtrl.arryCacthIsEnable[1] && bLastFlag_2 && CatchInPosFlag[1]) ? (0x01 << 1) : 0x00) : 0x00);
                    int value2 = (!bResult_3 ? ((HPRobotCtrl.arryCacthIsEnable[2] && bLastFlag_3 && CatchInPosFlag[2]) ? (0x01 << 2) : 0x00) : 0x00);
                    int value3 = (!bResult_4 ? ((HPRobotCtrl.arryCacthIsEnable[3] && bLastFlag_4 && CatchInPosFlag[3]) ? (0x01 << 3) : 0x00) : 0x00);
                    int value4 = (!bResult_5 ? ((HPRobotCtrl.arryCacthIsEnable[4] && bLastFlag_5 && CatchInPosFlag[4]) ? (0x01 << 4) : 0x00) : 0x00);
                    int value5 = (!bResult_6 ? ((HPRobotCtrl.arryCacthIsEnable[5] && bLastFlag_6 && CatchInPosFlag[5]) ? (0x01 << 5) : 0x00) : 0x00);
                    int value6 = (!bResult_7 ? ((HPRobotCtrl.arryCacthIsEnable[6] && bLastFlag_7 && CatchInPosFlag[6]) ? (0x01 << 6) : 0x00) : 0x00);
                    int valueAll = value0 | value1 | value2 | value3 | value4 | value5 | value6;

                    if(nValus != valueAll)
                    {
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 坏品可能掉入包装盒，请人工清理", disPlayColor, nFontSize);
                        string errorMsg = $"!!!!!!!!严重错误!!!!!!!!\r\n坏品可能掉入包装盒，请人工清理";
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    }

                    #endregion

                    #region>>>>>>更新下料表格数据
                    int nCountOfPassed = 0;
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 更新下料表格数据", disPlayColor, nFontSize);
                    if (needQA == 1)
                    {
                        bResult_1 = bResult_2 = bResult_3 = bResult_4 = bResult_5 = bResult_6 = bResult_7 = false;
                    }

                    int nCoutOfVacuu = (int)speCoutOfCatchUsed.Value;
                    value1 = (bResult_1 ? 0x01 << 0 : 0x00);
                    value2 = (bResult_2 ? 0x01 << 1 : 0x00);
                    value3 = (bResult_3 ? 0x01 << 2 : 0x00);
                    value4 = (bResult_4 ? 0x01 << 3 : 0x00);
                    value5 = (bResult_5 ? 0x01 << 4 : 0x00);
                    value6 = (bResult_6 ? 0x01 << 5 : 0x00);
                    int value7 = (bResult_7 ? 0x01 << 6 : 0x00);

                    if (bResult_1) nCountOfPassed++;
                    if (bResult_2) nCountOfPassed++;
                    if (bResult_3) nCountOfPassed++;
                    if (bResult_4) nCountOfPassed++;
                    if (bResult_5) nCountOfPassed++;
                    if (bResult_6) nCountOfPassed++;
                    if (bResult_7) nCountOfPassed++;

                    int nStatusFlag = value1 | value2 | value3 | value4 | value5 | value6 | value7;

                    SafeUpdataUIData values = new SafeUpdataUIData();
                    values.nCellRow = nToCellRow;
                    values.nCellCol = nToCellCol;
                    values.nStatusFlag = nStatusFlag;
                    values.nCountOfVacuu = nCoutOfVacuu;
                    values.bReset = false;

                    if (WhereTrayDefaulUnload)
                        values.gridView = gridLoadTrayResult;
                    else
                        values.gridView = gridBuffTrayResult;

                    m_SyncContext.Send((SendOrPostCallback)SafeUpdataLoadTrayStatus, values);

                    #endregion

                    #region>>>>>>放料完成，升起汽缸
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 放料完成，升起汽缸", disPlayColor, 9);
                    Handle_CyliderControl(false, false, false, false, false, false, false); //升起汽缸
                    Thread.Sleep(50);
                    #endregion

                    #region>>>>>>更新已下料数据
                    if(WhereTrayDefaulUnload)
                    {
                        short nCurrentCount = HPRobotCtrl.PLC_Unload.ReadInt16("D1061").Content;
                        HPRobotCtrl.CurrentUnitUnloadedToPackingStation = nCurrentCount + nCountOfPassed;
                        Thread.Sleep(50);
                        HPRobotCtrl.PLC_Unload.Write("D1061", (short)HPRobotCtrl.CurrentUnitUnloadedToPackingStation);
                        this.Invoke(new Action(() => { spedCurrentUnitUnloadedToPackingStation.Value = (short)HPRobotCtrl.CurrentUnitUnloadedToPackingStation; }));

                    }
                    #endregion

                    REPLACE_BAD:
                    #region>>>>>>开始移动到坏品下料位置（如果有坏品)
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 开始移动到坏品下料位置（如果有坏品)", disPlayColor, 9);
                    if ((!bResult_1 && HPRobotCtrl.arryCacthIsEnable[0]) ||
                         (!bResult_2 && HPRobotCtrl.arryCacthIsEnable[1]) ||
                         (!bResult_3 && HPRobotCtrl.arryCacthIsEnable[2]) ||
                         (!bResult_4 && HPRobotCtrl.arryCacthIsEnable[3]) ||
                         (!bResult_5 && HPRobotCtrl.arryCacthIsEnable[4]) ||
                         (!bResult_6 && HPRobotCtrl.arryCacthIsEnable[5]) ||
                         (!bResult_7 && HPRobotCtrl.arryCacthIsEnable[6]))
                    {
                        if (isOnQAPos) //当前位置在抽检位
                        {
                            Handle_MoveRobot(txtRobotTransit_VToQA.Text.Trim()); //移动到QA过渡位
                        }
                        else //当前位置在下料位 / 缓存盘位
                        {
                            if (WhereTrayDefaulUnload) //当前位置在下料位
                                Handle_MoveRobot(txtRobotTransit_ULToF.Text.Trim());
                            else //当前位置在缓存盘位
                            {
                                Handle_MoveRobot("-251.96, 146.38, 0, 136.51, /R");

                                Handle_MoveRobot(txtRobotTransit_ULToF.Text.Trim());
                            }
                        }

                        //移动到坏品位
                        Handle_MoveRobot(txtRobotFailed.Text.Trim()); 

                        //断开真空
                        Handle_VacuuControl(false, false, false, false, false, false, false, chkVacuuSensor.Checked, 2000, 0);
                        Thread.Sleep(100);
                    }
                    else //没有坏品
                    {
                        if (isOnQAPos) //当前位置在抽检位
                        {
                            Handle_MoveRobot(txtRobotTransit_VToQA.Text.Trim()); //移动到QA过渡位
                        }

                        if (!WhereTrayDefaulUnload) //如果是上料到缓存盘，先移回过渡位置
                            Handle_MoveRobot("-251.96, 146.38, 0, 136.51, /R");
                    }

                    #endregion

                    END_MOVE:
                    #region>>>>>>移动到过渡过位(回到原始位置)
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell : 移动到过渡过位", disPlayColor, 9);
                    Handle_MoveRobot(txtRobotTransit_LToF.Text.Trim());
                    #endregion

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadCell: From: {nFromCellRow}, {nFromCellCol} To:  {nToCellRow}, {nToCellCol}例取料完成", Color.Blue, 9);

                    return true;
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("UnloadCell  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }

                return false;
            }
        }

        public bool IsLoadTrayInPos()
        {
            try
            {
                OperateResult<Int16> status1 = HPRobotCtrl.PLC_Unload.ReadInt16("D1307");
                OperateResult<Int16> status2 = HPRobotCtrl.PLC_Unload.ReadInt16("D1306");
                if (status1.IsSuccess && status2.IsSuccess && status1.Content == 0 && status2.Content == 1)
                    return true;

                return false;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("IsLoadTrayInPos() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        Task taskWaitLoadTray = null;

        public bool WaitLoadA_LoadTray(bool bForceLoadANewTray = false, bool waitNewTrayLoaded = false)
        {
            int nRowCount = (int)speRows.Value;
            int nColCount = (int)speCols.Value;

            int nRowDst = (int)speRowDst.Value;
            int nColDst = (int)speColDst.Value;

            int nCoutOfVacuu = (int)speCoutOfCatchUsed.Value;

            try
            {
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>上料盘", Color.Blue, 9);

                LOADTRAY:
                //移动到过渡过位
                Handle_MoveRobot(txtRobotTransit_LToF.Text.Trim());

                //安全信号复位
                HPRobotCtrl.PLC_Unload.Write("D1309", 1);

                if(bForceLoadANewTray)
                {
                    //设置满料信号
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>强制满料信号D1307->1", Color.Blue, 9);
                    HPRobotCtrl.PLC_Unload.Write("D1307", 1);
                    Thread.Sleep(100);
                    UpdataLoadTrayStatus(gridLoadTrayResult, -1, -1, 0x00, nCoutOfVacuu, true);
                    SetCurrentPackedValue(0, true);
                }

                if (!waitNewTrayLoaded)
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>不等待料盘上好信号", Color.Blue, 9);
                    return true;
                }

                //等待上好料盘
                WAITLOADTRAY:
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>等待上好料盘D1307为0，D1306为1", Color.Blue, 9);
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(100);
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    //如果上料完成
                    if (IsLoadTrayInPos())
                    {
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>料盘已上好，继续进行上下料", Color.Blue, 9);
                        return true;
                    }

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 120)
                    {
                        string errorMsg = $"等待上好料盘超时失败！\r\n正常PLC结果：D1307为0，D1306为1 \r\n请手动控制PLC上好料盘或解决问题之后再点击 继续";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                        HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                        emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        {
                            goto WAITLOADTRAY;
                        }
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        {
                            return false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WaitLoadA_LoadTray() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                throw new Exception(HPglobal.m_strLastException);
            }
        }

        public bool RobotUnload()
        {
            lock (ojectUnloadAction)
            {
                RECHECK:
                if (ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 0)
                {
                    string errorMsg = $"注意: RobotUnload 当前定义的生产总量已经达到！\r\n\r\n请将总出料盘设为0或重新定义出货总量 之后点击  继续 \r\n\r\n";
                    HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                    return true;
                }

                bool bUnloadTrayResult = true;
                try
                {
                    Point[] ProductPoint = GetInPosArryOfTray(false);
                    if (ProductPoint == null || ProductPoint.Length <= 0)
                    {
                        return RobotUnload_ToBufferTray();
                    }
                    else
                    {
                        return RobotUnload_End();
                    }

                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("RobotUnload  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }

                return bUnloadTrayResult;
            }

        }

        public bool RobotUnload_ToBufferTray()
        {
            bool bUnloadTrayResult = true;
            HPRobotCtrl.bIsEndUnloadFlag = false;
            Point EndLoadPoint = new Point(-1, -1);

            try
            {
                int nRowCount = (int)speRows.Value;
                int nColCount = (int)speCols.Value;

                int nRowDst = (int)speRowDst.Value;
                int nColDst = (int)speColDst.Value;

                UpateCustRowsDst();

                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>RobotUnload_ToBufferTray", Color.Blue, 9);

                //复位安全信号
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>复位安全信号", Color.Blue, 9);
                HPRobotCtrl.PLC_Unload.Write("D1309", 0);

                #region>>>>>>确认料盘是否存在及等待上缓存盘
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>确认料盘是否存在及等待上缓存盘", Color.Blue, 9);
                while (true)
                {
                    Thread.Sleep(100);
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    OperateResult<Int16> status1 = HPRobotCtrl.PLC_Unload.ReadInt16("D1302");
                    if (status1 != null && status1.Content == 1)
                        break;
                }

                #endregion

                //得到切割打码标及抓取标志
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>得到切割打码标及抓取标志", Color.Blue, 9);
                Int16[,] FlagResult = null;
                HPRobotCtrl.dataBaseSQL.SQL_GetLastModeFlagAllProcessFromTray(txtTrayID2.Text.Trim(), out FlagResult);

                #region>>>>>>将整个托盘放满（按一行一行放)
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>将整个托盘放满（按一行一行放)", Color.Blue, 9);
                int nCoutOfVacuu = (int)speCoutOfCatchUsed.Value;
                UpdataLoadTrayStatus(gridBuffTrayResult, -1, -1, 0x00, nCoutOfVacuu, true);
                Application.DoEvents();

                for (int nIndexRow = 0; nIndexRow < nRowCount; nIndexRow++)
                {
                    #region>>>>>>先放完一例
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    #region>>>>>>如果已经进入工单生产的尾料模式，请取抓取标志，对不应该抓的产品不抓
                    if (btm进入尾料模式.Checked && FlagResult != null &&
                        FlagResult[nIndexRow, 0] == 0x00 &&
                        FlagResult[nIndexRow, 1] == 0x00 &&
                        FlagResult[nIndexRow, 2] == 0x00 &&
                        FlagResult[nIndexRow, 3] == 0x00 &&
                        FlagResult[nIndexRow, 4] == 0x00 &&
                        FlagResult[nIndexRow, 5] == 0x00 &&
                        FlagResult[nIndexRow, 6] == 0x00 &&
                        FlagResult[nIndexRow, 7] == 0x00 &&
                        FlagResult[nIndexRow, 8] == 0x00 &&
                        FlagResult[nIndexRow, 9] == 0x00)
                    {
                        continue;
                    }
                    #endregion

                    //得到结果
                    int ResultFlag = 0x00;
                    for (int nIndex = 0; nIndex < 10; nIndex++)
                        ResultFlag |= (HPRobotCtrl.bArryCamera2_RT2_Temp[nIndexRow, nIndex] ? ((int)0x01 << nIndex) : ((int)0x00 << nIndex));

                    UnloadCell(false, nIndexRow, 0, nIndexRow, 0, ResultFlag, true, false, nIndexRow == 0 ? true : false);
                    #endregion

                }

                //清除PLC内部Flash数据
                HPRobotCtrl.ResetFlashAreaData(txtTrayID2.Text.Trim(), false);
                #endregion

                #region>>>>>>写入取盘PLC完成信息
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>写入取盘PLC完成信息及安全信号", Color.Blue, 9);

                if (HPRobotCtrl.PLC_Unload.ReadInt16("D1066").Content == 0 || btm进入尾料模式.Checked == false)
                    HPRobotCtrl.PLC_Unload.Write("D1312", (short)0);
                else
                    HPRobotCtrl.PLC_Unload.Write("D1312", (short)1);

                HPRobotCtrl.PLC_Unload.Write("D1304", (short)1);
                HPRobotCtrl.PLC_Unload.Write("D1305", (short)1);
                HPRobotCtrl.PLC_Unload.Write("D1311", (short)1);
                #endregion

                #region>>>>>>开始补料(如果上料盘一个料都没有， 就等下一个料盘，如果有就开始补料)
                Point[] ProductPoint = GetInPosArryOfTray(true);
                if (ProductPoint != null && ProductPoint.Length > 0)
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>开始补料", Color.Blue, 9);
                    if (!ResupplyCellForBuff(EndLoadPoint))
                    {
                        bUnloadTrayResult = false;
                    }
                    else
                    {
                        #region>>>>>>等待上好料盘
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>上料已经满，等待上好料盘", Color.Blue, 9);
                        if (!WaitLoadA_LoadTray(true))
                        {
                            string errorMsg = $"人工手动终止上料盘的动作\r\n由于没有盘，下料动作将补终止 ！\r\n";
                            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                            bUnloadTrayResult = false;
                        }
                        #endregion
                    }
                }
          
                #endregion

                return bUnloadTrayResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("RobotUnload_ToBufferTray  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return bUnloadTrayResult;
        }

        public bool RobotUnload_End() //下尾料
        {
            bool bUnloadTrayResult = true;
            HPRobotCtrl.bIsEndUnloadFlag = true;
            Point EndLoadPoint = new Point(-1, -1);
            bool UnloadToBufferTrayFlag = false;
            string errorMsg = "";
            bool completedAllOrderPlan = false; //是否已完成当前工单
            bool checkLoadTrayStart = false;

            try
            {
                #region>>>>>>更新所有下料数据
                int nRowCount = (int)speRows.Value;
                int nColCount = (int)speCols.Value;

                int nRowDst = (int)speRowDst.Value;
                int nColDst = (int)speColDst.Value;

                int nNumOfCatch = (int)speCoutOfCatchUsed.Value;

                HPRobotCtrl.CurrentUnitInTray = 0; //将当前托盘已下料总数设为0

                UpateCustRowsDst();
                #endregion

                //开始下料整个托盘
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadTray 开始下料整个托盘", Color.Blue, 9);

                //得到切割打码标及抓取标志
                Int16[,] FlagResult = null;
                HPRobotCtrl.dataBaseSQL.SQL_GetLastModeFlagAllProcessFromTray(txtTrayID2.Text.Trim(), out FlagResult);

                #region>>>>>>将整个托盘放满（按一行一行放)
                int nCoutOfVacuu = (int)speCoutOfCatchUsed.Value;
                int nIndexRow = 0;
                for (nIndexRow = 0; nIndexRow < nRowCount; nIndexRow++)
                {
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    RECHECK2:
                    #region>>>>>>定义的生产总量是否已经达到
                    if (ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 0)
                    {
                        completedAllOrderPlan = true;
                        errorMsg = $"注意: RobotUnload_End 当前定义的生产总量已经达到！\r\n\r\n如果想继续下料，请将总出料盘设为0或重新定义出货总量 之后点击  继续(不一定下料成功) 否则请点击停止 \r\n\r\n";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        EndLoadPoint = new Point(ParameterAllObj.Cfg_Remaind / nColCount, ParameterAllObj.Cfg_Remaind % nColCount);
                        break;
                    }
                    #endregion

                    #region>>>>>>确认是否是尾盘下料模式
                    if (ParameterAllObj.TotalProductCountTray - ParameterAllObj.CurrentTotalCountTray <= 1)
                    {
                        HPRobotCtrl.bIsEndUnloadFlag = true;
                        EndLoadPoint = new Point(ParameterAllObj.Cfg_Remaind / nColCount, ParameterAllObj.Cfg_Remaind % nColCount);
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadTray(UnloadCell) 开始下料({nIndexRow}, {nIndexRow})(尾盘下料模式)", Color.Blue, 9);
                    }
                    else
                    {
                        HPRobotCtrl.bIsEndUnloadFlag = false;
                        EndLoadPoint = new Point(-1, -1);
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>UnloadTray(UnloadCell) 开始下料({nIndexRow}, {nIndexRow})(正常下料模式)", Color.Blue, 9);
                    }
                    #endregion

                    #region>>>>>>如果已经进入工单生产的尾料模式，请取抓取标志，对不应该抓的产品不抓
                    if (btm进入尾料模式.Checked && FlagResult != null &&
                        FlagResult[nIndexRow, 0] == 0x00 &&
                        FlagResult[nIndexRow, 1] == 0x00 &&
                        FlagResult[nIndexRow, 2] == 0x00 &&
                        FlagResult[nIndexRow, 3] == 0x00 &&
                        FlagResult[nIndexRow, 4] == 0x00 &&
                        FlagResult[nIndexRow, 5] == 0x00 &&
                        FlagResult[nIndexRow, 6] == 0x00 &&
                        FlagResult[nIndexRow, 7] == 0x00 &&
                        FlagResult[nIndexRow, 8] == 0x00 &&
                        FlagResult[nIndexRow, 9] == 0x00)
                    {
                        continue;
                    }
                    #endregion

                    #region>>>>>>得到结果
                    int ResultFlag = 0x00;
                    for (int nIndex = 0; nIndex < 10; nIndex++)
                        ResultFlag |= (HPRobotCtrl.bArryCamera2_RT2_Temp[nIndexRow, nIndex] ? ((int)0x01 << nIndex) : ((int)0x00 << nIndex));

                    #endregion

                    #region>>>>>>确定当前行是否有产品， 上料没完成，说明已经满料
                    bool CompletedUnload = false;
                    int nLoadRowNotHasPCB = 0;
                    int nLoadColNotHasPCB = 0;
                    bool bIsRowNeedUnload = false;
                    for (nLoadRowNotHasPCB = 0; nLoadRowNotHasPCB < nRowCount; nLoadRowNotHasPCB++)
                    {
                        for (nLoadColNotHasPCB = 0; nLoadColNotHasPCB < nColCount; nLoadColNotHasPCB++)
                        {
                            if (IsRowEmpty(true, nLoadRowNotHasPCB, nLoadColNotHasPCB) == false)
                            {
                                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>下料盘当前行 {nLoadRowNotHasPCB}, {nLoadColNotHasPCB} 有料，继续取下一行", Color.Blue, 9);
                                continue;
                            }
                            else
                            {
                                bIsRowNeedUnload = true;
                                goto ROWUNLOAD;
                            }
                        }
                    }
                    #endregion

                    ROWUNLOAD:
                    if ((UnloadToBufferTrayFlag == false && nLoadRowNotHasPCB < nRowCount && nLoadColNotHasPCB < nColCount) ||
                        UnloadToBufferTrayFlag == true)
                    {
                        #region>>>>>>确定当前行是不是尾料，如果是尾料也不能上料
                        Point[] UnitInPos = GetInPosArryOfTray(true); //得到当前tray 已有料的个数
                        Point[] UnitInPosOfBuffTray = GetInPosArryOfTray(false); //得到当前tray 已有料的个数
                        int nCurrentPoint = (nNumOfCatch * nColCount) * nLoadRowNotHasPCB + nLoadColNotHasPCB * nNumOfCatch;

                        if (UnloadToBufferTrayFlag == false &&
                            HPRobotCtrl.bIsEndUnloadFlag &&
                            UnitInPosOfBuffTray.Length >= ParameterAllObj.Cfg_Remaind) //在尾料阶段如果当前缓存盘的料大于所有剩余的料，就直接补料
                        {
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>当前行是尾料, 不能上料(缓存盘可以直接补尾盘的料)", Color.Blue, 9);
                        }
                        else if (UnloadToBufferTrayFlag == false &&
                                    HPRobotCtrl.bIsEndUnloadFlag &&
                                    (nCurrentPoint >= (ParameterAllObj.Cfg_Remaind - nNumOfCatch)))   //如果当前行再放一排就放满的情况就直接补料
                        {
                            HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>当前行是尾料, 不能上料(需要补料)", Color.Blue, 9);
                        }
                        else
                        {
                            if (UnloadToBufferTrayFlag == false)
                            {
                                UnloadCell(true, nIndexRow, 0, nLoadRowNotHasPCB, nLoadColNotHasPCB, ResultFlag, true, false, nIndexRow == 0 ? true : false);
                            }
                            else
                            {
                                UnloadCell(false, nIndexRow, 0, nIndexRow, 0, ResultFlag, true, false, nIndexRow == 0 ? true : false);
                            }

                            CompletedUnload = true;
                        }
                        #endregion
                    }

                    if (CompletedUnload) //如果上料已经完成，继续下一行上料
                        continue;

                    #region>>>>>>如果上料没完成, 当产品每一行已满料或者已经进行到尾盘收料阶段,  开始补料   //上料没完成，说明已经满料
                    nIndexRow--;

                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>开始补料", Color.Blue, 9);
                    bool ResuplyResult = ResupplyCellForBuff(EndLoadPoint);

                    #endregion

                    #region>>>>>>如果补料没有完成，说明补料盘没有料，将放入补料标志设为真，机器人将料往补料盘上送
                    if (!ResuplyResult)
                    {
                        UnloadToBufferTrayFlag = true;
                        continue;
                    }
                    #endregion

                    #region>>>>>>补料成功，等待上好料盘，如果成功，继续下一步(循环)
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>上料已经满，等待上好料盘", Color.Blue, 9);
                    bool bWaitLoadTray = WaitLoadA_LoadTray(true);
                    #endregion

                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return false;

                    if (!bWaitLoadTray)
                        goto ERROR_LOAD;

                    #region>>>>>>如果是尾盘，一定等待当前总包装盘数为0，作好防呆
                    if (HPRobotCtrl.bIsEndUnloadFlag)
                    {
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>尾盘，等待当前总包装盘数为0，作好防呆", Color.Blue, 9);
                        Thread.Sleep(1000);
                        while (ParameterAllObj.CurrentCountTray != 0)
                        {
                            Thread.Sleep(100);
                            if (HPglobal.m_bQuitApp || bStopFlag)
                                return false;

                            errorMsg = $"失败! \r\n\r\n尾盘，等待当前总包装盘数为0，作好防呆！当前的包装盘没有变成0，请确认包装机正常工作或手动清0 \r\n\r\n";
                            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                            HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                            emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        }
                    }


                    #endregion

                    //!!!!!!!!!!!!!!!!!!!!!!!!!!! 继续下一次循环 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    continue;

                    ERROR_LOAD:
                    #region>>>>>>等待上好料盘不成功，返回false, 停止上料动作，人工处理
                    errorMsg = $"人工手动终止上料盘的动作\r\n由于没有盘，下料动作将补终止 ！\r\n";
                    HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                    bUnloadTrayResult = false;
                    return bUnloadTrayResult;
                    #endregion

                }
                #endregion

                #region>>>>>>写入取盘PLC完成信息
                END_Unload:
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>写入取盘PLC完成信息及安全信号", Color.Blue, 9);

                if (HPRobotCtrl.PLC_Unload.ReadInt16("D1066").Content == 0 || btm进入尾料模式.Checked == false)
                    HPRobotCtrl.PLC_Unload.Write("D1312", (short)0);
                else
                    HPRobotCtrl.PLC_Unload.Write("D1312", (short)1);

                HPRobotCtrl.PLC_Unload.Write("D1304", (short)1);
                HPRobotCtrl.PLC_Unload.Write("D1305", (short)1);
                HPRobotCtrl.PLC_Unload.Write("D1311", (short)1);

                //如果已完当前工单生产总量，并且尾料标志设为true, 则要进行复位
                if (completedAllOrderPlan && btm进入尾料模式.Checked)
                {
                    btm进入尾料模式.Checked = false;
                    HPRobotCtrl.PLC_Unload.Write("D1056", (short)0);
                }

                #endregion

                #region>>>>>>清除PLC内部Flash数据
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>{"UnloadTray 清除PLC内部Flash数据"}", Color.Blue, 9);
                HPRobotCtrl.ResetFlashAreaData(txtTrayID2.Text.Trim(), false);
                #endregion

                #region>>>>>>开始补料， 如果一个都没有就不需要补料，等待下一盘
                Point[] UnitInPos2 = GetInPosArryOfTray(true); //得到当前tray 已有料的个数, 如果一个都没有就不需要补料，等待下一盘
                if (UnitInPos2.Length > 0)
                {
                    HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>开始补料", Color.Blue, 9);
                    if (!ResupplyCellForBuff(EndLoadPoint))
                    {
                        bUnloadTrayResult = false;
                    }
                    else
                    {
                        #region>>>>>>等待上好料盘
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>上料已经满，等待上好料盘", Color.Blue, 9);
                        if (!WaitLoadA_LoadTray(true))
                        {
                            errorMsg = $"人工手动终止上料盘的动作\r\n由于没有盘，下料动作将补终止 ！\r\n";
                            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                            bUnloadTrayResult = false;
                        }
                        #endregion
                    }
                }
                #endregion

                return bUnloadTrayResult;
            }
            catch (Exception ex)
            {
                bUnloadTrayResult = false;
                HPglobal.m_strLastException = string.Format("RobotUnload_End  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return bUnloadTrayResult;
        }

        private void 触发1303_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
        }

        private void 下料盘已满_CheckedChanged(object sender, EventArgs e)
        {
            OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1307", 下料盘已满.Checked ? (short)1 : (short) 0);
        }

        private void 下料组盘触发_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1303", (short)1);
                Thread.Sleep(500);
                result = HPRobotCtrl.PLC_Unload.Write("D1303", (short)0);
            });
        }

        private void 相机1拍照触发_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1200", (short)1);
                Thread.Sleep(500);
                result = HPRobotCtrl.PLC_Unload.Write("D1200", (short)0);
            });
        }

        private void PLC设备安信号_CheckedChanged(object sender, EventArgs e)
        {
            OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1309", PLC设备安信号.Checked ? (short)1 : (short)0);
        }

        private void 设定吸嘴间距_Click(object sender, EventArgs e)
        {
            OperateResult<Int32> status = HPRobotCtrl.StepSevo.ReadInt32(0x000A.ToString());
            if (status.IsSuccess)
                speCathDst.Value = status.Content;
        }

        private void 间距电机回原点_Click(object sender, EventArgs e)
        {
            if(HPRobotCtrl.CatchStep_Home())
            {
                MessageBox.Show("间距调整电机因原点成功", "恭喜", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("间距调整电机因原点失败", "错误", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void 电机间距Dcre_Click(object sender, EventArgs e)
        {
            HPRobotCtrl.CatchStep_StepMove(-Int32.Parse(txtStepDst.Text.Trim()));
        }

        private void 电机间距Incre_Click(object sender, EventArgs e)
        {
            HPRobotCtrl.CatchStep_StepMove(Int32.Parse(txtStepDst.Text.Trim()));
        }

        public void SetTrayFullOrEmpty(bool whereTrayLoadOrBuff, bool fullOrEmpty = true)
        {
            System.Windows.Forms.DataGridView obj = (whereTrayLoadOrBuff ? gridLoadTrayResult : gridBuffTrayResult);

            try
            {
                this.Invoke(new Action(() =>
                {
                    for (int nIndex = 0; nIndex < speRowOfSingle.Value; nIndex++)
                    {
                        for (int nCol = 0; nCol < speColOfSingle.Value; nCol++)
                        {
                            obj.Rows[nIndex].Cells[nCol].Style.BackColor = Color.White;
                            obj[nCol, nIndex].Value = fullOrEmpty;
                        }
                    }
                }));

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SetBuffTrayFull() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void SetBuffTrayEmpty()
        {
            try
            {
                this.Invoke(new Action(() =>
                {
                    for (int nIndex = 0; nIndex < speRowOfSingle.Value; nIndex++)
                    {
                        for (int nCol = 0; nCol < speColOfSingle.Value; nCol++)
                        {
                            gridBuffTrayResult.Rows[nIndex].Cells[nCol].Style.BackColor = Color.Red;
                            gridBuffTrayResult[nCol, nIndex].Value = false;
                        }
                    }
                }));
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SetBuffTrayEmpty() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmUpdateRow_Click(object sender, EventArgs e)
        {
            try
            {
                this.Invoke(new Action(() =>
                {
                    gridLoadTrayResult.Rows.Clear();
                    gridBuffTrayResult.Rows.Clear();
                    for (int nIndex = 0; nIndex < speRows.Value; nIndex++)
                    {
                        int nRow = gridLoadTrayResult.Rows.Add();
                        gridLoadTrayResult.Rows[nRow].HeaderCell.Value = (nRow + 1).ToString();

                        nRow = gridBuffTrayResult.Rows.Add();
                        gridBuffTrayResult.Rows[nRow].HeaderCell.Value = (nRow + 1).ToString();
                    }

                    decimal nPcsOfCol = speCoutOfCatchUsed.Value;
                    for (int nIndex = 0; nIndex < speRows.Value; nIndex++)
                    {
                        for (int nCol = 0; nCol < 20; nCol++)
                        {
                            if (nCol >= nPcsOfCol)
                            {
                                gridLoadTrayResult.Rows[nIndex].Cells[nCol].Style.BackColor = Color.Gray;
                                gridBuffTrayResult.Rows[nIndex].Cells[nCol].Style.BackColor = Color.Gray;
                            }
                        }
                    }

                    HPRobotCtrl.arryCacthIsEnable[0] = (speCoutOfCatchUsed.Value >= 1 ? true : false);
                    HPRobotCtrl.arryCacthIsEnable[1] = (speCoutOfCatchUsed.Value >= 2 ? true : false);
                    HPRobotCtrl.arryCacthIsEnable[2] = (speCoutOfCatchUsed.Value >= 3 ? true : false);
                    HPRobotCtrl.arryCacthIsEnable[3] = (speCoutOfCatchUsed.Value >= 4 ? true : false);
                    HPRobotCtrl.arryCacthIsEnable[4] = (speCoutOfCatchUsed.Value >= 5 ? true : false);
                    HPRobotCtrl.arryCacthIsEnable[5] = (speCoutOfCatchUsed.Value >= 6 ? true : false);
                    HPRobotCtrl.arryCacthIsEnable[6] = (speCoutOfCatchUsed.Value >= 7 ? true : false);
                }));
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmUpdateRow_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
         
        }

        private void simpleButton29_Click(object sender, EventArgs e)
        {
            HPRobotCtrl.PassedCamera1 = 0;
            HPRobotCtrl.FailedCamera1 = 0;

            HPRobotCtrl.PassedCamera2 = 0;
            HPRobotCtrl.FailedCamera2 = 0;

            HPRobotCtrl.PLC_Unload.Write("D1069", (int) 0);
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            HPRobotCtrl.PassedTotal = 0;
            HPRobotCtrl.FailedTotal = 0;
        }

        private void speCols_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btmRobotLearnBuffFirst_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotPos_BuffFirst);
        }

        private void btmRobotLearnLoadFirst_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotPos_LoadFirst);
        }

        private void btmRobotLearnBuffFirst_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotPos_BuffFirst.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnBuffFirst_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotLearnLoadFirst_Go_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotPos_LoadFirst.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnLoadFirst_Go_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void frm_RobotControl_Load(object sender, EventArgs e)
        {
            try
            {
                ChangeType(HPglobal.m_strModelName.Replace("Model_", ""));
                btmUpdateRow_Click(null, null);
                设定吸嘴间距_Click(null, null);
                SetTrayFullOrEmpty(false, false);
                SetTrayFullOrEmpty(true, false);

                if (HPglobal.m_strUserApp == "Administrator")
                    SetPrivilegy("管理员");
                else
                    SetPrivilegy("操作员");
            }
            catch(Exception ex)
            {

            }
        }

        private void btmSetBuffTrayFull_Click(object sender, EventArgs e)
        {
            try
            {
                SetTrayFullOrEmpty(false, true);
            }
            catch(Exception ex)
            {
                HPglobal.RecordeAllMessage($"btmSetBuffTrayFull_Click 异常:{ex.Message}", true, false, "");
            }
        }

        public Point[] GetEmptyPosArryOfTray(bool whereTrayLoadOrBuff)
        {
            List<Point> posList = new List<Point>();
            System.Windows.Forms.DataGridView obj = (whereTrayLoadOrBuff ? gridLoadTrayResult : gridBuffTrayResult);
            try
            {
                for (int nRow = 0; nRow < speRowOfSingle.Value; nRow++)
                {
                    for (int nCol = 0; nCol < speColOfSingle.Value; nCol++)
                    {
                        if (obj[nCol, nRow].Value != null &&
                            obj[nCol, nRow].Value.ToString() == "False")
                        {
                            posList.Add(new Point(nRow, nCol));
                        }
                    }
                }

                return posList.ToArray();
            }
            catch (Exception ex)
            {
                HPglobal.RecordeAllMessage($"GetEmptyPosArryOfTray 异常:{ex.Message}", true, false, "");
                return null;
            }            
        }

        public Point[] GetInPosArryOfTray(bool whereTrayLoadOrBuff)
        {
            List<Point> posList = new List<Point>();
            System.Windows.Forms.DataGridView obj = (whereTrayLoadOrBuff ? gridLoadTrayResult : gridBuffTrayResult);
            try
            {
                for (int nRow = 0; nRow < obj.RowCount; nRow++)
                {
                    for (int nCol = 0; nCol < obj.ColumnCount; nCol++)
                    {
                        if (obj[nCol, nRow].Value != null &&
                            obj[nCol, nRow].Value.ToString() == "True")
                        {
                            posList.Add(new Point(nRow, nCol));
                        }
                    }
                }

                return posList.ToArray();
            }
            catch (Exception ex)
            {
                HPglobal.RecordeAllMessage($"GetInPosArryOfTray 异常:{ex.Message}", true, false, "");
                return null;
            }
        }

        public bool IsRowEmpty(bool whereTrayLoadOrBuff, int Row, int Col)
        {
            bool bResult = true;
            System.Windows.Forms.DataGridView  obj = (whereTrayLoadOrBuff ? gridLoadTrayResult : gridBuffTrayResult);
            try
            {
                for (int nCol = 0; nCol < speColOfSingle.Value; nCol++)
                {
                    if (obj[nCol, Row].Value != null && obj[nCol, Row].Value.ToString() == "True")
                    {
                        bResult = false;
                    }
                }

                return bResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("IsRowEmpty() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        private void btmSetBuffStartPos_Click(object sender, EventArgs e)
        {
            try
            {
                for (int nIndex = 0; nIndex < speRowOfSingle.Value; nIndex++)
                {
                    for (int nCol = 0; nCol < speColOfSingle.Value; nCol++)
                    {
                        gridBuffTrayResult.Rows[nIndex].Cells[nCol].Style.BackColor = Color.White;

                        if ((nIndex +1) < speStartBuffRow.Value)
                            gridBuffTrayResult[nCol, nIndex].Value = false;
                        else
                        {
                            if ((nIndex + 1) > speStartBuffRow.Value || (nCol + 1) >= speStartBuffCol.Value)
                                gridBuffTrayResult[nCol, nIndex].Value = true;
                            else
                                gridBuffTrayResult[nCol, nIndex].Value = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SetBuffTrayFull() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void simpleButton30_Click(object sender, EventArgs e)
        {
            try
            {
                RobotUnload_End();
            }
            catch(Exception ex)
            {

            }
        }

        private void simpleButton31_Click(object sender, EventArgs e)
        {
            HPRobotCtrl.CatchStep_StepMove(Int32.Parse(speCathDst.Text.Trim()), false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                HalconDotNet.HImage[] image = new HImage[5];
                MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest1_1, ParameterAllObj.VisionTest1_2, ParameterAllObj.VisionTest1_3, ParameterAllObj.VisionTest1_4, ParameterAllObj.VisionTest1_5 };

                for (int nIndex = 0; nIndex < 5; nIndex++)
                {
                    if(methods[nIndex].Enabled)
                    {
                        HPRobotCtrl.Camera1.SetCameraExposure(methods[nIndex].Exposure * 1000.00);
                        Thread.Sleep((int)spedDelayCameraSet.Value);
                        for (int nCount = 0; nCount < 5; nCount++)
                        {
                            HalconForm.visionResult result = new HalconForm.visionResult();
                            if ((result = HPRobotCtrl.Camera1.SnapImage(out image[nIndex])).IsSuccess == false || !HalconGeneral.IsImageObj(image[nIndex]))
                            {
                                HPglobal.RecordeAllMessage($"相机1采集图像失败, 重新采集!: {result.ErrorMsg}", true, false, "");
                                Thread.Sleep(300);
                            }
                            else
                                break;
                        }

                        Thread.Sleep(20);

                    }

                }

                HPRobotCtrl.Camera1.Show();
                VisionTest1_Execute(image);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                HalconDotNet.HImage[] image = new HImage[5];
                MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest2_1, ParameterAllObj.VisionTest2_2, ParameterAllObj.VisionTest2_3, ParameterAllObj.VisionTest2_4, ParameterAllObj.VisionTest2_5 };

                for (int nIndex = 0; nIndex < 5; nIndex++)
                {
                    if (methods[nIndex].Enabled)
                    {
                        HPRobotCtrl.Camera2.SetCameraExposure(methods[nIndex].Exposure * 1000.00);
                        Thread.Sleep((int)spedDelayCameraSet.Value);
                        for(int nCount = 0; nCount < 5; nCount++)
                        {
                            HalconForm.visionResult result = new HalconForm.visionResult();
                            if ((result = HPRobotCtrl.Camera2.SnapImage(out image[nIndex])).IsSuccess == false || !HalconGeneral.IsImageObj(image[nIndex]))
                            {
                                HPglobal.RecordeAllMessage($"相机2采集图像失败, 重新采集!: {result.ErrorMsg}", true, false, "");
                                Thread.Sleep(300);
                            }
                            else
                                break;
                        }

                        Thread.Sleep(20);
                    }
                }

                HPRobotCtrl.Camera2.Show();
                VisionTest2_Execute(image);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void simpleButton32_Click(object sender, EventArgs e)
        {
            Task taskImageProcess = taskImageProcess = Task.Factory.StartNew(() =>
            {
                try
                {
                    HImage image;
                    HObject objImage;
                    MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest1_1, ParameterAllObj.VisionTest1_2, ParameterAllObj.VisionTest1_3, ParameterAllObj.VisionTest1_4, ParameterAllObj.VisionTest1_5 };
                    if (chk采集新的图片.Checked)
                    {
                        HPRobotCtrl.Camera1.SetCameraExposure(methods[listBoxControl1.SelectedIndex].Exposure * 1000.00);
                        Thread.Sleep((int)spedDelayCameraSet.Value);
                        HPRobotCtrl.Camera1.SnapImage(out image);
                    }
                    else
                    {
                        string file = "";
                        this.Invoke(new Action(() => file = txtImageProcessFile.Text.Trim()));
                        HalconDotNet.HOperatorSet.ReadImage(out objImage, file);
                        image = new HImage(objImage);
                    }

                    if (listBoxControl1.SelectedIndex == 0)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest1_1.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest1_1.ImageProcess(image);
                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera1.Show();
                            HPRobotCtrl.Camera1.ShowImage(image);
                            HPRobotCtrl.Camera3.Show();
                            HPRobotCtrl.Camera3.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowResult(ParameterAllObj.VisionTest1_1.CheckResult, 0.00, 0.00, false);
                            propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_1;
                        }));
                    }
                    else if (listBoxControl1.SelectedIndex == 1)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest1_2.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest1_2.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera1.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowResult(ParameterAllObj.VisionTest1_2.CheckResult, 0.00, 0.00, false);
                            propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_2;
                        }));
                    }
                    else if (listBoxControl1.SelectedIndex == 2)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest1_3.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest1_3.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera1.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowResult(ParameterAllObj.VisionTest1_3.CheckResult, 0.00, 0.00, false);
                            propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_3;
                        }));
                    }
                    else if (listBoxControl1.SelectedIndex == 3)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest1_4.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest1_4.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera1.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowResult(ParameterAllObj.VisionTest1_4.CheckResult, 0.00, 0.00, false);
                            propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_4;
                        }));
                    }
                    else if (listBoxControl1.SelectedIndex == 4)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest1_5.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest1_5.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera1.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowImage(image);
                            HPRobotCtrl.Camera3.ShowResult(ParameterAllObj.VisionTest1_5.CheckResult, 0.00, 0.00, false);
                            propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_5;
                        }));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });
        }

        private void propertyGrid1_Click(object sender, EventArgs e)
        {

        }

        private void propertyGrid1_BindingContextChanged(object sender, EventArgs e)
        {

        }

        private void btmSaveVisionPara_Click(object sender, EventArgs e)
        {
            try
            {
                ParameterAllObj.Camera1_Exposure = (int)spinEdit1.Value;
                ParameterAllObj.Camera1_Contrast = (int)spinEdit2.Value;

                ParameterAllObj.Camera2_Exposure = (int)spinEdit4.Value;
                ParameterAllObj.Camera2_Contrast = (int)spinEdit3.Value;

                ParameterAllObj.lightValue_Channel1 = (int) trackLightSource_1.Value;
                ParameterAllObj.lightValue_Channel2 = (int)trackLightSource_2.Value;
                ParameterAllObj.lightValue_Channel3 = (int)trackLightSource_3.Value;
                ParameterAllObj.lightValue_Channel4 = (int)trackLightSource_4.Value;

                ZHToolBox.General.ZHToolBoxGeneral.SerializeObjToFile<ParameterSaveFile>(ParameterAllObj, $"ConfigFile\\{HPglobal.m_strModelName}\\HPRobotCtrObjParameters.xml");
            }
            catch(Exception ex)
            {
                return;
            }
        }

        private void 测试增加1_Click(object sender, EventArgs e)
        {

        }

        private void 测试删除1_Click(object sender, EventArgs e)
        {

        }

        private void 运行测试2_Click(object sender, EventArgs e)
        {
            Task taskImageProcess = taskImageProcess = Task.Factory.StartNew(() =>
            {
                try
                {
                    HImage image;
                    HObject objImage;
                    MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest2_1, ParameterAllObj.VisionTest2_2, ParameterAllObj.VisionTest2_3, ParameterAllObj.VisionTest2_4, ParameterAllObj.VisionTest2_5 };
                    if (chk采集新的图片.Checked)
                    {
                        HPRobotCtrl.Camera2.SetCameraExposure(methods[listBoxControl2.SelectedIndex].Exposure * 1000.00);
                        Thread.Sleep((int)spedDelayCameraSet.Value);
                        HPRobotCtrl.Camera2.SnapImage(out image);
                    }
                    else
                    {
                        string file = "";
                        this.Invoke(new Action(() => file = txtImageProcessFile.Text.Trim()));
                        HalconDotNet.HOperatorSet.ReadImage(out objImage, file);
                        image = new HImage(objImage);
                    }

                    if (listBoxControl2.SelectedIndex == 0)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest2_1.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest2_1.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera2.Show();
                            HPRobotCtrl.Camera2.ShowImage(image);
                            HPRobotCtrl.Camera4.Show();
                            HPRobotCtrl.Camera4.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowResult(ParameterAllObj.VisionTest2_1.CheckResult, 0.00, 0.00, false);
                            propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_1;
                        }));
                    }
                    else if (listBoxControl2.SelectedIndex == 1)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest2_2.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest2_2.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera2.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowResult(ParameterAllObj.VisionTest2_2.CheckResult, 0.00, 0.00, false);
                            propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_2;
                        }));
                    }
                    else if (listBoxControl2.SelectedIndex == 2)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest2_3.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest2_3.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera2.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowResult(ParameterAllObj.VisionTest2_3.CheckResult, 0.00, 0.00, false);
                            propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_3;
                        }));
                    }
                    else if (listBoxControl2.SelectedIndex == 3)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest2_4.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest2_4.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera2.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowResult(ParameterAllObj.VisionTest2_4.CheckResult, 0.00, 0.00, false);
                            propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_4;
                        }));
                    }
                    else if (listBoxControl2.SelectedIndex == 4)
                    {
                        bool bVistionTestResult = ParameterAllObj.VisionTest2_5.Init("", chkNewTemplate.Checked);
                        if (!bVistionTestResult)
                            return;

                        bVistionTestResult = ParameterAllObj.VisionTest2_5.ImageProcess(image);

                        this.Invoke(new Action(() =>
                        {
                            HPRobotCtrl.Camera2.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowImage(image);
                            HPRobotCtrl.Camera4.ShowResult(ParameterAllObj.VisionTest2_5.CheckResult, 0.00, 0.00, false);
                            propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_5;
                        }));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            });
          
        }

        private void 测试增加2_Click(object sender, EventArgs e)
        {

        }

        private void 测试删除2_Click(object sender, EventArgs e)
        {

        }

        private void btmLoad_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog file = new OpenFileDialog();
                file.Title = "请选择图片";
                file.Filter = "(*.bmp)|*.bmp|(*.jpg)|*.jpg|(*.png)|*.png|(*.*)|*.*";
                //file.InitialDirectory = @"E:\计算机资料\01 C#视频\基础实训4\1110C#基础\资料\img";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    string strNames = file.FileName;
                    txtImageProcessFile.Text = strNames;
                }
            }
            catch (Exception ex)
            {
                HPglobal.RecordeAllMessage($"btmLoad_Click 异常:{ex.Message}", true, false, "");
            }
        }

        private void timerFlashData_Tick(object sender, EventArgs e)
        {
            try
            {
//                 txtNameArea1.Text = HPRobotCtrl.FlashDataPN[0];
//                 txtNameArea2.Text = HPRobotCtrl.FlashDataPN[1];
//                 txtNameArea3.Text = HPRobotCtrl.FlashDataPN[2];
//                 txtNameArea4.Text = HPRobotCtrl.FlashDataPN[3];
//                 txtNameArea5.Text = HPRobotCtrl.FlashDataPN[4];
//                 txtNameArea6.Text = HPRobotCtrl.FlashDataPN[5];
//                 txtNameArea7.Text = HPRobotCtrl.FlashDataPN[6];
//                 txtNameArea8.Text = HPRobotCtrl.FlashDataPN[7];
// 
//                 dataGridArea1.DataSource = HPRobotCtrl.dtFlashDT[0];
//                 dataGridArea2.DataSource = HPRobotCtrl.dtFlashDT[1];
//                 dataGridArea3.DataSource = HPRobotCtrl.dtFlashDT[2];
//                 dataGridArea4.DataSource = HPRobotCtrl.dtFlashDT[3];
//                 dataGridArea5.DataSource = HPRobotCtrl.dtFlashDT[4];
//                 dataGridArea6.DataSource = HPRobotCtrl.dtFlashDT[5];
//                 dataGridArea7.DataSource = HPRobotCtrl.dtFlashDT[6];
//                 dataGridArea8.DataSource = HPRobotCtrl.dtFlashDT[7];

            }
            catch(Exception ex)
            {
                HPglobal.RecordeAllMessage($"timerFlashData_Tick 异常:{ex.Message}", true, false, "");
            }
        }

        private void 缓存盘设置为空盘_Click(object sender, EventArgs e)
        {
            try
            {
                SetTrayFullOrEmpty(false, false);
            }
            catch (Exception ex)
            {
                HPglobal.RecordeAllMessage($"缓存盘设置为空盘_Click 异常:{ex.Message}", true, false, "");
            }
        }

        private void speRowOfSingleDst_EditValueChanged(object sender, EventArgs e)
        {

        }

        public bool isClearingLastProduct = false;
        private void btm清料_Click(object sender, EventArgs e)
        {
            try
            {
                if(强行清料并包装模式.Checked)
                {
                    if (isClearingLastProduct)
                        return;

                    _StopMachine();

                    Task.Factory.StartNew(() =>
                    {
                        isClearingLastProduct = true;

                        ResupplyCellForBuff(new Point(-1, -1), true);

                        WaitLoadA_LoadTray(true, true);

                        isClearingLastProduct = false;
                    });
                }
                else
                {
                    Task.Factory.StartNew(() =>
                    {
                        HPRobotCtrl.PulseAllRemaindProduct = true;
                        Thread.Sleep(2000);
                        HPRobotCtrl.PulseAllRemaindProduct = false;
                    });
                }
            }
            catch(Exception ex)
            {
                isClearingLastProduct = false;
                HPglobal.RecordeAllMessage($"btm清料_Click 异常:{ex.Message}", true, false, "");
            }
            
        }

        public bool HandGetFlashData(string sn, bool clear = false)
        {
            bool bResultOfGetFlashData = true;
            try
            {
/*                short[,] FlashResult = HPRobotCtrl.GetFlashAreaData2(sn, clear);*/

                Int16[,] FlashResult = null;
                HPRobotCtrl.dataBaseSQL.SQL_GetFlashResultFromTray(sn, out FlashResult); //"P4HA594013"
                if (FlashResult == null || FlashResult.Length < 100)
                {
                    bResultOfGetFlashData = false;
                    HPglobal.m_strLastException = $"HandGetFlashData  错误/异常：得不到烧录数据结果, 托盘号: {sn}\r\n";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }

                for (int nRow = 0; nRow < 10; nRow++)
                {
                    for (int nCol = 0; nCol < 10; nCol++)
                    {
                        if (FlashResult != null)
                        {
                            viewFlashResult.Rows[nRow].Cells[nCol].Value = FlashResult[nRow, nCol].ToString("X02");
                            viewFlashResult.Rows[nRow].Cells[nCol].Style.BackColor = (FlashResult[nRow, nCol] == 0x00 ? Color.Green : Color.Red);
                        }
                        else
                        {
                            bResultOfGetFlashData = false;
                            viewFlashResult.Rows[nRow].Cells[nCol].Value = "Err";
                            viewFlashResult.Rows[nRow].Cells[nCol].Style.BackColor = Color.Red;
                        }
                    }
                }

            }
            catch(Exception ex)
            {
                bResultOfGetFlashData = false;
                HPglobal.m_strLastException = string.Format("btmGetFlashData_Click  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return bResultOfGetFlashData;
        }

        public bool[,] HandGetVision1TestResult(string trayID)
        {
            bool[] bStatus1D = new bool[100];
            bool[,] bStatus2D = new bool[10, 10];

            try
            {
                ResultInformation resultList = null;
                bool result = HPRobotCtrl.dataBaseSQL.SQL_GetStationResultFromTray(trayID, WorkStationNameEnum.顶部测试工站, out resultList);
               if(result)
                {
                    int nIndexOfC = resultList.Results[0].Result.IndexOf("[");
                    string dataResult = resultList.Results[0].Result.Substring(nIndexOfC + 1);
                    dataResult = dataResult.Trim("]".ToCharArray());
                    dataResult = dataResult.Trim(")".ToCharArray());

                    string[] arryValues = dataResult.Split(";".ToCharArray());
                    if (arryValues.Length < 100)
                        return null;

                    for (int nIndex = 0; nIndex < 100; nIndex++)
                    {
                        bStatus1D[nIndex] = (arryValues[nIndex] == "1" ? true : false);
                    }

                    int CountData = 0;
                    for (int nRow = 0; nRow < 10; nRow++)
                    {
                        for (int nCol = 0; nCol < 10; nCol++)
                        {
                            bStatus2D[nRow, nCol] = bStatus1D[CountData++];
                        }
                    }
                }

                return bStatus2D;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmGetFlashData_Click  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return null; 
        }

        private void btmGetFlashData_Click(object sender, EventArgs e)
        {
            try
            {
                HandGetFlashData(txtTrayID2.Text.Trim(), false);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmGetFlashData_Click  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        public bool SetCurrentPackedValue(short setValue , bool bIncress = true)
        {
            try
            {
                if(bIncress)
                {
                    OperateResult<short> values = HPRobotCtrl.PLC_Unload.ReadInt16("D1046");
                    if (values.IsSuccess == false)
                        return false;

                    OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1046", (short)(values.Content + 1));
                    return result.IsSuccess;
                }
                else
                {
                    OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1046", setValue);
                    return result.IsSuccess;
                }
                
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("SetCurrentPackedValue  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        private void btmSetCurrentTrayOut_Click(object sender, EventArgs e)
        {
            SetCurrentPackedValue((short)spedSetTrayOuted.Value, false);
        }

        private void listBoxControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBoxControl2.SelectedIndex == 0)
                propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_1;

            else if (listBoxControl2.SelectedIndex == 1)
                propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_2;

            else if (listBoxControl2.SelectedIndex == 2)
                propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_3;

            else if (listBoxControl2.SelectedIndex == 3)
                propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_4;

            else if (listBoxControl2.SelectedIndex == 4)
                propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_5;
        }

        private void listBoxControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxControl1.SelectedIndex == 0)
                propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_1;

            else if (listBoxControl1.SelectedIndex == 1)
                propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_2;

            else if (listBoxControl1.SelectedIndex == 2)
                propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_3;

            else if (listBoxControl1.SelectedIndex == 3)
                propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_4;

            else if (listBoxControl1.SelectedIndex == 4)
                propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_5;
        }

        private void btmSetCurrentTray_Click(object sender, EventArgs e)
        {
            try
            {
                OperateResult result = HPRobotCtrl.PLC_Unload.Write("D1040", (short)spedSetCurrentTray.Value);
                return ;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmSetCurrentTray_Click  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        private void popupContainerEdit1_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btmUnbindTray_Click(object sender, EventArgs e)
        {
            if(txtTrayID2.Text.Trim().Length > 0)
            {
                if(!HPRobotCtrl.dataBaseSQL.SQL_TrayUnBind(txtTrayID2.Text.Trim()))
                {
                    HPglobal.m_strLastException = $"btmUnbindTray_Click  解绑({txtTrayID2.Text.Trim()}) 失败\r\n";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
            
        }

        private void bmtUnbindTray2_Click(object sender, EventArgs e)
        {
            if (textEdit9.Text.Trim().Length > 0)
            {
                if (!HPRobotCtrl.dataBaseSQL.SQL_TrayUnBind(textEdit9.Text.Trim()))
                {
                    HPglobal.m_strLastException = $"btmUnbindTray_Click  解绑({textEdit9.Text.Trim()}) 失败\r\n";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        private void trackExposure_1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spinEdit1.Value = trackExposure_1.Value;
                HPRobotCtrl.Camera1.SetCameraExposure((double)spinEdit1.Value * 1000);
            }
            catch(Exception ex)
            {

            }

        }

        private void trackContrast_1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spinEdit2.Value = trackContrast_1.Value;
                HPRobotCtrl.Camera1.SetCameraContrast((long)spinEdit2.Value);
            }
            catch (Exception ex)
            {

            }
           
        }

        private void trackExposure_2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spinEdit4.Value = trackExposure_2.Value;
                HPRobotCtrl.Camera2.SetCameraExposure((double)spinEdit4.Value * 1000);
            }
            catch (Exception ex)
            {

            }
          
        }

        private void trackContrast_2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spinEdit3.Value = trackContrast_2.Value;
                HPRobotCtrl.Camera2.SetCameraContrast((long)spinEdit3.Value);
            }
            catch (Exception ex)
            {

            }
           
        }

        private void btmSetDst_Click(object sender, EventArgs e)
        {
            //35电机 导程2MM 细分1000
            double mmDst = (double)spinDstRow.Value - (double) spedOrgStepDst.Value;

            Int32 nCountOfPulse = (Int32) (mmDst / ParameterAllObj.DstPerPulse);
            HPRobotCtrl.CatchStep_StepMove(nCountOfPulse, false);
        }

        private void 下料数据交互触发_Click(object sender, EventArgs e)
        {
            try
            {
                //如果当前盘被清0了，说明当前的盘数已达到，将已下料的总数写到包装的1061
                //调度接受到分拣PLC触发信号后会将1061里的数据写入到包装PLC1061的地址 
                HPRobotCtrl.PLC_Unload.Write("D1060", (short)1); //触发总调度进行包装数据交互
                HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>等待包装信息写入触发完成 D1060 -> 0", Color.Blue, 9);
                DateTime time3, time4;
                time3 = time4 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(200);
                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return;

                    OperateResult<short> values = HPRobotCtrl.PLC_Unload.ReadInt16("D1060");
                    if (values.IsSuccess && values.Content == 0)
                        break;

                    time4 = DateTime.Now;
                    if ((time4 - time3).TotalSeconds > 120)
                    {
                        string errorMsg = $"失败: 等待包装信息写入触发完成超时 D1060 -> 0";
                        HPglobal.WorkingMessageOutput(ref HPRobotCtrl.m_queRobotLoadUnload, $"{DateTime.Now.ToLongTimeString()}>>>失败: 等待包装信息写入触发完成超时 D1060 -> 0", Color.Red, 9);
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                        break;
                    }
                }

                HPRobotCtrl.CurrentUnitUnloadedToPackingStation = 0;

                if (ParameterAllObj.CurrentCountTray == 0)
                {
                 
                }
            }
            catch(Exception ex)
            {

            }
        }







        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btm进入尾料模式_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (btm进入尾料模式.Checked)
                    HPRobotCtrl.PLC_Unload.Write("D1056", (short)1);
                else
                    HPRobotCtrl.PLC_Unload.Write("D1056", (short)0);
            }
            catch (Exception ex)
            {

            }

        }

        private void btm进入尾料模式_Click(object sender, EventArgs e)
        {
            try
             {
//                 if (btm进入尾料模式.Checked)
//                 {
//                     if (MessageBox.Show(this, "你确定要 取消 尾料模式？？请确认在之前所有的料盘都已做完", "!!!! 确认 !!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
//                     {
//                         HPRobotCtrl.PLC_Unload.Write("D1056", (short)0);
//                     }
//                     else
//                         btm进入尾料模式.Checked = true;
//                 }
//                 else
//                 {
//                     if (MessageBox.Show(this, "你确定要 启用 尾料模？？请确认在之前所有的料盘都已做完", "!!!! 确认 !!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
//                     {
//                         HPRobotCtrl.PLC_Unload.Write("D1056", (short) 1);
//                     }
//                     else
//                         btm进入尾料模式.Checked = false;
//                 }

            }
            catch (Exception ex)
            {

            }
        }

        private object objChangingOrder = new object();
        public bool ChangeType(string typeName)
        {
            bool Result = true;
            try
            {
                lock(objChangingOrder)
                {
                    if (typeName != null && typeName.Length > 0)
                    {
                        string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                        string folderPath = System.IO.Path.GetDirectoryName(exeFileName) + "\\ConfigFile";

                        HPglobal.m_strModelName = $"{"Model_"}{typeName}";
                        HPglobal.m_strXmlFilename = $"{folderPath}\\{HPglobal.m_strModelName}\\HP_Debug.xml";
                        if (!File.Exists(HPglobal.m_strXmlFilename))
                        {
                            string errorMsg = $"!!!!转工单失败!!!!当前工单或文件不存在!!!!\r\n型号/打码名字: {HPglobal.m_strModelName}\r\n";
                            errorMsg += $"打码内容: {typeName }\r\n";
                            errorMsg += $"当前文件不存在，请重新转工单或预先做好模板文件/文件夹....\r\n";
                            errorMsg += $"xml文件: {HPglobal.m_strXmlFilename}\r\n";
                            errorMsg += $"文件夹: ConfigFile\\{HPglobal.m_strModelName}";
                            HPglobal.RecordeAllMessage(errorMsg, true, false, "");

                            HPRobotCtrl.PLC_Unload.Write("D1314", (short)1);
                            emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);

                            btmInvoke_StopMachine_Click(null, null);
                            HPRobotCtrl.modebusTCPServer.Write("1007", (short)99);
                            HPRobotCtrl.modebusTCPServer.Write("1002", (short)99);

                            Result = false;
                        }
                        else
                        {
                            //打开XML文件
                            HPglobal.m_MesInterfaceNode = null;
                            HPglobal.m_xmlSystemConfig.Load(HPglobal.m_strXmlFilename);
                        }
                    }

                    if (bSystemStarted)
                    {
                        this.Invoke(new Action(() =>
                        {

                            #region>>>>>>>>打开Xml文件
                            LoadXmlConfig();

                            if (ParameterAllObj != null)
                                ParameterAllObj.Dispose();

                            ParameterAllObj = (ParameterSaveFile)ZHToolBox.General.ZHToolBoxGeneral.DeserializeObjFromFile<ParameterSaveFile>($"ConfigFile\\{HPglobal.m_strModelName}\\HPRobotCtrObjParameters.xml");
                            propertyGrid1.SelectedObject = ParameterAllObj.VisionTest1_1;
                            propertyGrid2.SelectedObject = ParameterAllObj.VisionTest2_1; 
                            #endregion

                            if (ParameterAllObj == null)
                                HPglobal.RecordeAllMessage($"自动转工单失败({HPglobal.m_strModelName})", true, false, "");

                            #region>>>>>>>>更新视觉方法

                            listBoxControl1.Items.Clear();
                            int nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_1.Name);
                            nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_2.Name);
                            nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_3.Name);
                            nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_4.Name);
                            nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_5.Name);

                            listBoxControl2.Items.Clear();
                            nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_1.Name);
                            nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_2.Name);
                            nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_3.Name);
                            nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_4.Name);
                            nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_5.Name);
                            #endregion

                            #region>>>>>>>>更新相机参数
                            HPRobotCtrl.Camera1.Exposure = ParameterAllObj.Camera1_Exposure * 1000.00;
                            HPRobotCtrl.Camera1.Contrast = ParameterAllObj.Camera1_Contrast;
                            spinEdit1.Value = trackExposure_1.Value = ParameterAllObj.Camera1_Exposure;
                            spinEdit2.Value = trackContrast_1.Value = ParameterAllObj.Camera1_Contrast;

                            HPRobotCtrl.Camera2.Exposure = ParameterAllObj.Camera2_Exposure * 1000.00;
                            HPRobotCtrl.Camera2.Contrast = ParameterAllObj.Camera2_Contrast;
                            spinEdit4.Value = trackExposure_2.Value = ParameterAllObj.Camera2_Exposure;
                            spinEdit3.Value = trackContrast_2.Value = ParameterAllObj.Camera2_Contrast;

                            #endregion

                            #region>>>>>>>>更新光源参数
                            trackLightSource_1.Value = ParameterAllObj.lightValue_Channel1;
                            trackLightSource_2.Value = ParameterAllObj.lightValue_Channel2;
                            trackLightSource_3.Value = ParameterAllObj.lightValue_Channel3;
                            trackLightSource_4.Value = ParameterAllObj.lightValue_Channel4;

                            HPRobotCtrl.lightSource.LightSource_SetValue(1, (short)spedLS_Value_1.Value);
                            HPRobotCtrl.lightSource.LightSource_SetValue(2, (short)spedLS_Value_2.Value);
                            HPRobotCtrl.lightSource.LightSource_SetValue(3, (short)spedLS_Value_3.Value);
                            HPRobotCtrl.lightSource.LightSource_SetValue(4, (short)spedLS_Value_4.Value);
                            #endregion

                            ParameterAllObj.MarkTextLast = typeName;

                            #region>>>>>>>>更新工艺参数
                            OperateResult<short[]> ProcessData1030 = HPRobotCtrl.PLC_Unload.ReadInt16("D1030", 60);
                            OperateResult<short[]> ProcessData2200 = HPRobotCtrl.PLC_Unload.ReadInt16("D2200", 60);
                            if(ProcessData1030.IsSuccess && ProcessData2200.IsSuccess)
                            {
                                speRowOfSingle.Value = speRows.Value = ProcessData1030.Content[1];
                                speColOfSingle.Value = speCols.Value = ProcessData1030.Content[2];

                                speRowOfSingleDst.Value = speRowDst.Value =(decimal)(((double)HPRobotCtrl.PLC_Unload.ReadInt32("D2210").Content) / 1000.00);
                                speColOfSingleDst.Value = speColDst.Value = (decimal)(((double)HPRobotCtrl.PLC_Unload.ReadInt32("D2212").Content) / 1000.00);

                                speCoutOfCatchUsed.Value = ProcessData1030.Content[2];
                                spinDstRow.Value = (decimal)(((double)HPRobotCtrl.PLC_Unload.ReadInt32("D2212").Content) / 1000.00);
                            }
                            #endregion

                            btmUpdateRow_Click(null, null);
                            设定吸嘴间距_Click(null, null);
                            SetTrayFullOrEmpty(false, false);
                            SetTrayFullOrEmpty(true, false);

                            if (Program.AppForm != null)
                                Program.AppForm.Text = Program.AppNameOrg + " -- " + HPglobal.m_strModelName;

                            HPglobal.RecordeAllMessage($"自动转工单成功({HPglobal.m_strModelName})", true, false, "");

                        }));
                    }

                    return Result;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show($"ChangeType 异常{ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                throw ex;
            }
        }

        private void btm手动切换型号_Click(object sender, EventArgs e)
        {
            try
            {
                ParameterAllObj.MarkText = ParameterAllObj.MarkTextLast = txtWorkOrder.Text.Trim();
                ChangeType(ParameterAllObj.MarkText);
            }
            catch(Exception ex)
            {
                return;
            }
        }

        private void panelControl2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rbtmSeep_High_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void propertyGrid2_BindingContextChanged(object sender, EventArgs e)
        {

        }

        private void propertyGrid2_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if(e.ChangedItem.Label == "名称")
            {
                listBoxControl2.Items.Clear();
                int nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_1.Name);
                nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_2.Name);
                nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_3.Name);
                nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_4.Name);
                nitem = listBoxControl2.Items.Add(ParameterAllObj.VisionTest2_5.Name);
            }
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (e.ChangedItem.Label == "名称")
            {
                listBoxControl1.Items.Clear();
                int nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_1.Name);
                nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_2.Name);
                nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_3.Name);
                nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_4.Name);
                nitem = listBoxControl1.Items.Add(ParameterAllObj.VisionTest1_5.Name);
            }
        }

        private void rbtmSeep_SupperHigh_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbtmSeep_Low_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btmRobotLearnBuffFirst2_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotPos_BuffFirst2);
        }

        private void btmRobotLearnBuffFirst_Go2_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotPos_BuffFirst2.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnBuffFirst_Go2_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void btmRobotLearnLoadFirst2_Click(object sender, EventArgs e)
        {
            ButtonLearnRobotPos(txtRobotPos_LoadFirst2);
        }

        private void btmRobotLearnLoadFirst_Go2_Click(object sender, EventArgs e)
        {
            try
            {
                MoveRobotToPos(txtRobotPos_LoadFirst2.Text.Trim());
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRobotLearnLoadFirst_Go2_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void timer_GC_Tick(object sender, EventArgs e)
        {
            try
            {
                GC.Collect();
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("timer_GC_Tick() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        private void 得到顶部测试结果_Click(object sender, EventArgs e)
        {
            try
            {
                HandGetVision1TestResult(txtTrayID.Text.Trim()); //"P4HA594013"
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("得到顶部测试结果_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }

        }

        bool isImageTesting = false;
        private void 图像循环测试_Click(object sender, EventArgs e)
        {
            object imageProLock = new object();

            if(!isImageTesting)
            {
                isImageTesting = true;
                图像循环测试.Text = "图像循环测试中....";
                图像循环测试.BackColor = Color.LightGreen;
                try
                {
                    #region >>>>>图像1异步测试
                    HImage[] arryImage = null;
                    Task.Factory.StartNew(() =>
                    {
                        for (int nIndex = 0; nIndex < 1000000 && isImageTesting; nIndex++)
                        {
                            if (HPglobal.m_bQuitApp || bStopFlag)
                                return;

                            HImage image;
                            HObject objImage;
                            if (chk采集新的图片.Checked)
                            {
                                MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest2_1, ParameterAllObj.VisionTest2_2, ParameterAllObj.VisionTest2_3, ParameterAllObj.VisionTest2_4, ParameterAllObj.VisionTest2_5 };
                                arryImage = new HImage[5];
                                for (int nIndexImage = 0; nIndexImage < 5; nIndexImage++)
                                {
                                    if (methods[nIndexImage].Enabled)
                                    {
                                        HPRobotCtrl.Camera2.SetCameraExposure(methods[nIndexImage].Exposure * 1000.00);
                                        Thread.Sleep((int)spedDelayCameraSet.Value);
                                        HalconForm.visionResult result = HPRobotCtrl.Camera2.SnapImage(out arryImage[nIndexImage]);
                                    }
                                }
                            }
                            else
                            {
                                string file = "";
                                this.Invoke(new Action(() => file = txtImageProcessFile.Text.Trim()));
                                HalconDotNet.HOperatorSet.ReadImage(out objImage, file);
                                image = new HImage(objImage);

                                arryImage = new HImage[5] { image, image, image, image, image };
                            }

                            //HPRobotCtrl.Camera2.Show();
                            lock (imageProLock)
                            {
                                VisionTest2_Execute(arryImage);
                            }

                            Thread.Sleep(200);
                        }
                    });
                    #endregion

                    #region >>>>>图像2异步测试
                    Task.Factory.StartNew(() =>
                    {
                        for (int nIndex = 0; nIndex < 1000000 && isImageTesting; nIndex++)
                        {
                            if (HPglobal.m_bQuitApp || bStopFlag)
                                return;

                            HImage image;
                            HObject objImage;
                            if (chk采集新的图片.Checked)
                            {
                                MethodBase[] methods = new MethodBase[5] { ParameterAllObj.VisionTest1_1, ParameterAllObj.VisionTest1_2, ParameterAllObj.VisionTest1_3, ParameterAllObj.VisionTest1_4, ParameterAllObj.VisionTest1_5 };
                                arryImage = new HImage[5];
                                for (int nIndexImage = 0; nIndexImage < 5; nIndexImage++)
                                {
                                    if (methods[nIndexImage].Enabled)
                                    {
                                        HPRobotCtrl.Camera1.SetCameraExposure(methods[nIndexImage].Exposure * 1000.00);
                                        Thread.Sleep((int)spedDelayCameraSet.Value);
                                        HalconForm.visionResult result = HPRobotCtrl.Camera1.SnapImage(out arryImage[nIndexImage]);
                                    }
                                }
                            }
                            else
                            {
                                string file = "";
                                this.Invoke(new Action(() => file = txtImageProcessFile.Text.Trim()));
                                HalconDotNet.HOperatorSet.ReadImage(out objImage, file);
                                image = new HImage(objImage);

                                arryImage = new HImage[5] { image, image, image, image, image };
                            }

                            //HPRobotCtrl.Camera2.Show();
                            lock (imageProLock)
                            {
                                VisionTest1_Execute(arryImage);
                            }

                            Thread.Sleep(200);
                        }
                    });
                    #endregion
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                isImageTesting = false;
                图像循环测试.Text = "图像循环测试";
                图像循环测试.BackColor = Color.Yellow;
            }

         
        }

        private void 更新下料总数_Click(object sender, EventArgs e)
        {
            try
            {
                short nCurrentCount = (short) spedCurrentUnitUnloadedToPackingStationNew.Value;
                HPRobotCtrl.CurrentUnitUnloadedToPackingStation = nCurrentCount;
                Thread.Sleep(50);
                HPRobotCtrl.PLC_Unload.Write("D1061", (short)HPRobotCtrl.CurrentUnitUnloadedToPackingStation);
                this.Invoke(new Action(() => { spedCurrentUnitUnloadedToPackingStation.Value = (short)HPRobotCtrl.CurrentUnitUnloadedToPackingStation; }));
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("更新下料总数_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void trackLightSource_1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spedLS_Value_1.Value = trackLightSource_1.Value;
                HPRobotCtrl.lightSource.LightSource_SetValue(1, (short) spedLS_Value_1.Value);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("trackLightSource_1_EditValueChanged() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void trackLightSource_2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spedLS_Value_2.Value = trackLightSource_2.Value;
                HPRobotCtrl.lightSource.LightSource_SetValue(2, (short)spedLS_Value_2.Value);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("trackLightSource_2_EditValueChanged() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void trackLightSource_3_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spedLS_Value_3.Value = trackLightSource_3.Value;
                HPRobotCtrl.lightSource.LightSource_SetValue(3, (short)spedLS_Value_3.Value);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("trackLightSource_3_EditValueChanged() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void trackLightSource_4_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                spedLS_Value_4.Value = trackLightSource_4.Value;
                HPRobotCtrl.lightSource.LightSource_SetValue(4, (short)spedLS_Value_4.Value);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("trackLightSource_4_EditValueChanged() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void SetPrivilegy(string group)
        {
            try
            {
                this.Invoke(new Action(() =>
                {
                    if (group == "管理员")
                    {
                        foreach (Control objControl in NP_设置.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_工艺参数.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_视觉参数.Controls)
                            objControl.Enabled = true;
                    }
                    else if (group == "工程师")
                    {
                        foreach (Control objControl in NP_设置.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_工艺参数.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_视觉参数.Controls)
                            objControl.Enabled = true;
                    }
                    else //操作员
                    {
                        foreach (Control objControl in NP_设置.Controls)
                            objControl.Enabled = false;

                        foreach (Control objControl in NP_工艺参数.Controls)
                            objControl.Enabled = false;

                        foreach (Control objControl in NP_视觉参数.Controls)
                            objControl.Enabled = false;
                    }

                    HPglobal.RecordeAllMessage($"注意: 当前用户{HPglobal.m_strRFID}, 切换权限({group})成功!", true, false, "");
                }));
            }
            catch (Exception ex)
            {

            }
        }
    }
}