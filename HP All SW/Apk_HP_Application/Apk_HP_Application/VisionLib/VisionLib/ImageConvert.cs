﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib
{
    public class ImageConvert
    {
        public static int ImageTohImage(CamaeraLib.Image aImage,out HImage hImage)
        {
            hImage = null;
            try
            {
                /*Byte[] 类型 转化成 IntPtr类型 */
                GCHandle HObject = GCHandle.Alloc(aImage.Buffer, GCHandleType.Pinned);
                IntPtr pObject = HObject.AddrOfPinnedObject();
                if (HObject.IsAllocated)
                    HObject.Free();

                if (aImage.Color == true)
                {
                    hImage = new HImage();
                    hImage.GenImageInterleaved(pObject, "bgr", aImage.Width, aImage.Height, -1, "byte", 0, 0, 0, 0, -1, 0);
                }
                else
                {
                    hImage = new HImage("byte", aImage.Width, aImage.Height, pObject);
                }
            }
            catch (Exception e)
            {
                return -1;
            }
           
            return 0;
        }
    }
}
