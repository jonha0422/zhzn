﻿namespace Apk_HP_Application.程序文件夹分类._4装盘及机器人控制
{
    partial class frm_RobotControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_RobotControl));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.NP_主页运行 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.btm进入尾料模式 = new DevExpress.XtraEditors.CheckButton();
            this.LEDRobot = new HslCommunication.Controls.UserLantern();
            this.LEDModbusServer_Robot = new HslCommunication.Controls.UserLantern();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.LEDUnloadPLCState = new HslCommunication.Controls.UserLantern();
            this.txtTrayID = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.得到顶部测试结果 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl17 = new DevExpress.XtraEditors.GroupControl();
            this.txt已识别的好品数量 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.txtUPHCamera2 = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalUPH = new DevExpress.XtraEditors.TextEdit();
            this.txtFailedCamera2 = new DevExpress.XtraEditors.TextEdit();
            this.txtPassedCamera2 = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalFailed = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton29 = new DevExpress.XtraEditors.SimpleButton();
            this.txtUPHCamera1 = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalPassed = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.txtFailedCamera1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.txtPassedCamera1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPCBSN = new DevExpress.XtraEditors.TextEdit();
            this.txtTrayID2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.PopupInfor2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl4 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.btmUnbindTray = new DevExpress.XtraEditors.SimpleButton();
            this.viewFlashResult = new System.Windows.Forms.DataGridView();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btmGetFlashData = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.txtPCBSN2 = new DevExpress.XtraEditors.TextEdit();
            this.button2 = new System.Windows.Forms.Button();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label23 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.LEDModbusServer = new HslCommunication.Controls.UserLantern();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.richTextCamera2 = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.richTextCamera1 = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.richTextRobot = new System.Windows.Forms.RichTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.checkButton1 = new DevExpress.XtraEditors.CheckButton();
            this.imageList16 = new System.Windows.Forms.ImageList(this.components);
            this.checkButton2 = new DevExpress.XtraEditors.CheckButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.userLantern2 = new HslCommunication.Controls.UserLantern();
            this.userLantern1 = new HslCommunication.Controls.UserLantern();
            this.btmInvoke_StopMachine = new DevExpress.XtraEditors.SimpleButton();
            this.btmInvoke_StartMachine = new DevExpress.XtraEditors.SimpleButton();
            this.btmInvoke_ResetMachine = new DevExpress.XtraEditors.SimpleButton();
            this.NP_设置 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btmRobotLearnFailed_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnFailed = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRobotFailed = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.btmRobotLearnQA_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnQA = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotQA = new DevExpress.XtraEditors.TextEdit();
            this.btmRobotLearnVF_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnVF = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnUNF_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnUNF = new DevExpress.XtraEditors.SimpleButton();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRobotTransit_VToQA = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRobotTransit_ULToF = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btmRobotLearnLF_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnLF = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotTransit_LToF = new DevExpress.XtraEditors.TextEdit();
            this.btmRobotLearnCamera_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnCamera = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotCamera = new DevExpress.XtraEditors.TextEdit();
            this.btmRobotLearnUnload_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnUnload = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotUnload = new DevExpress.XtraEditors.TextEdit();
            this.btmRobotLearnCatch_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnCatch = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotCatch = new DevExpress.XtraEditors.TextEdit();
            this.btmRobotLearnOrg_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnOrg = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotOrg = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl15 = new DevExpress.XtraEditors.GroupControl();
            this.label73 = new System.Windows.Forms.Label();
            this.btmRobotLearnLoadFirst_Go2 = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnLoadFirst2 = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotPos_LoadFirst2 = new DevExpress.XtraEditors.TextEdit();
            this.btmRobotLearnBuffFirst_Go2 = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnBuffFirst2 = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotPos_BuffFirst2 = new DevExpress.XtraEditors.TextEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.spn_Cust_Row9 = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Cust_Row8 = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Cust_Row7 = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Cust_Row6 = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Cust_Row5 = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Cust_Row4 = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Cust_Row3 = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Cust_Row2 = new DevExpress.XtraEditors.SpinEdit();
            this.chkCustomizeRowDst = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.spn_Cust_Row1 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.speColOfSingleDst = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.speRowOfSingleDst = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.speColOfSingle = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.speRowOfSingle = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.label55 = new System.Windows.Forms.Label();
            this.btmRobotLearnLoadFirst_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnLoadFirst = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotPos_LoadFirst = new DevExpress.XtraEditors.TextEdit();
            this.btmRobotLearnBuffFirst_Go = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotLearnBuffFirst = new DevExpress.XtraEditors.SimpleButton();
            this.txtRobotPos_BuffFirst = new DevExpress.XtraEditors.TextEdit();
            this.label58 = new System.Windows.Forms.Label();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.btmRobotWayTrayRunCurrent = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.label22 = new System.Windows.Forms.Label();
            this.comboxRunTo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.comboxRunFrom = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btmRobotWayTrayRun = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotWayClear = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotWayDel = new DevExpress.XtraEditors.SimpleButton();
            this.btmRobotWayLearn = new DevExpress.XtraEditors.SimpleButton();
            this.gridRobotWayList = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btmRobotWayAdd = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl13 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.spedOrgStepDst = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.btmSetDst = new DevExpress.XtraEditors.SimpleButton();
            this.spinDstRow = new DevExpress.XtraEditors.SpinEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.simpleButton31 = new DevExpress.XtraEditors.SimpleButton();
            this.设定吸嘴间距 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.speCathDst = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton21 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton22 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.btmSaveRobotConfig = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl12 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.chkZero = new System.Windows.Forms.CheckBox();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.chkHandelTypeRight = new System.Windows.Forms.RadioButton();
            this.chkHandelTypeLeft = new System.Windows.Forms.RadioButton();
            this.chkHandelTypeAuto = new System.Windows.Forms.RadioButton();
            this.NP_手动调试 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.groupControl19 = new DevExpress.XtraEditors.GroupControl();
            this.强制camera2结果 = new DevExpress.XtraEditors.CheckEdit();
            this.强制camera1结果 = new DevExpress.XtraEditors.CheckEdit();
            this.chkEnableRobotIO = new DevExpress.XtraEditors.CheckEdit();
            this.chkCareFlashResult = new DevExpress.XtraEditors.CheckEdit();
            this.chkVacuuSensor = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.bmtUnbindTray2 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.LED_Out0 = new System.Windows.Forms.CheckBox();
            this.LED_Out31 = new System.Windows.Forms.CheckBox();
            this.LED_Out1 = new System.Windows.Forms.CheckBox();
            this.X回原点 = new DevExpress.XtraEditors.SimpleButton();
            this.Z回原点 = new DevExpress.XtraEditors.SimpleButton();
            this.LED_Out30 = new System.Windows.Forms.CheckBox();
            this.Y回原点 = new DevExpress.XtraEditors.SimpleButton();
            this.LED_Out2 = new System.Windows.Forms.CheckBox();
            this.R回原点 = new DevExpress.XtraEditors.SimpleButton();
            this.LED_Out29 = new System.Windows.Forms.CheckBox();
            this.LED_Out3 = new System.Windows.Forms.CheckBox();
            this.LED_Out28 = new System.Windows.Forms.CheckBox();
            this.LED_Out4 = new System.Windows.Forms.CheckBox();
            this.LED_Out27 = new System.Windows.Forms.CheckBox();
            this.LED_Out5 = new System.Windows.Forms.CheckBox();
            this.LED_Out26 = new System.Windows.Forms.CheckBox();
            this.LED_Out6 = new System.Windows.Forms.CheckBox();
            this.LED_Out25 = new System.Windows.Forms.CheckBox();
            this.LED_Out7 = new System.Windows.Forms.CheckBox();
            this.LED_Out24 = new System.Windows.Forms.CheckBox();
            this.LED_Out8 = new System.Windows.Forms.CheckBox();
            this.LED_Out23 = new System.Windows.Forms.CheckBox();
            this.LED_Out9 = new System.Windows.Forms.CheckBox();
            this.LED_Out22 = new System.Windows.Forms.CheckBox();
            this.LED_Out10 = new System.Windows.Forms.CheckBox();
            this.LED_Out21 = new System.Windows.Forms.CheckBox();
            this.LED_Out11 = new System.Windows.Forms.CheckBox();
            this.LED_Out20 = new System.Windows.Forms.CheckBox();
            this.LED_Out12 = new System.Windows.Forms.CheckBox();
            this.LED_Out19 = new System.Windows.Forms.CheckBox();
            this.LED_Out13 = new System.Windows.Forms.CheckBox();
            this.LED_Out18 = new System.Windows.Forms.CheckBox();
            this.LED_Out14 = new System.Windows.Forms.CheckBox();
            this.LED_Out17 = new System.Windows.Forms.CheckBox();
            this.LED_Out15 = new System.Windows.Forms.CheckBox();
            this.LED_Out16 = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.LED_In0 = new HslCommunication.Controls.UserLantern();
            this.label45 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.LED_In1 = new HslCommunication.Controls.UserLantern();
            this.label38 = new System.Windows.Forms.Label();
            this.LED_In2 = new HslCommunication.Controls.UserLantern();
            this.label39 = new System.Windows.Forms.Label();
            this.LED_In3 = new HslCommunication.Controls.UserLantern();
            this.label40 = new System.Windows.Forms.Label();
            this.LED_In4 = new HslCommunication.Controls.UserLantern();
            this.label41 = new System.Windows.Forms.Label();
            this.LED_In5 = new HslCommunication.Controls.UserLantern();
            this.label42 = new System.Windows.Forms.Label();
            this.LED_In6 = new HslCommunication.Controls.UserLantern();
            this.label43 = new System.Windows.Forms.Label();
            this.LED_In7 = new HslCommunication.Controls.UserLantern();
            this.label44 = new System.Windows.Forms.Label();
            this.LED_In9 = new HslCommunication.Controls.UserLantern();
            this.label26 = new System.Windows.Forms.Label();
            this.LED_In10 = new HslCommunication.Controls.UserLantern();
            this.label30 = new System.Windows.Forms.Label();
            this.LED_In11 = new HslCommunication.Controls.UserLantern();
            this.label31 = new System.Windows.Forms.Label();
            this.LED_In12 = new HslCommunication.Controls.UserLantern();
            this.label32 = new System.Windows.Forms.Label();
            this.LED_In13 = new HslCommunication.Controls.UserLantern();
            this.label33 = new System.Windows.Forms.Label();
            this.LED_In14 = new HslCommunication.Controls.UserLantern();
            this.label34 = new System.Windows.Forms.Label();
            this.LED_In15 = new HslCommunication.Controls.UserLantern();
            this.label35 = new System.Windows.Forms.Label();
            this.LED_In8 = new HslCommunication.Controls.UserLantern();
            this.label36 = new System.Windows.Forms.Label();
            this.LED_In17 = new HslCommunication.Controls.UserLantern();
            this.LED_In24 = new HslCommunication.Controls.UserLantern();
            this.LED_In18 = new HslCommunication.Controls.UserLantern();
            this.LED_In31 = new HslCommunication.Controls.UserLantern();
            this.LED_In19 = new HslCommunication.Controls.UserLantern();
            this.LED_In30 = new HslCommunication.Controls.UserLantern();
            this.LED_In20 = new HslCommunication.Controls.UserLantern();
            this.LED_In29 = new HslCommunication.Controls.UserLantern();
            this.LED_In21 = new HslCommunication.Controls.UserLantern();
            this.LED_In28 = new HslCommunication.Controls.UserLantern();
            this.LED_In22 = new HslCommunication.Controls.UserLantern();
            this.LED_In27 = new HslCommunication.Controls.UserLantern();
            this.LED_In23 = new HslCommunication.Controls.UserLantern();
            this.LED_In26 = new HslCommunication.Controls.UserLantern();
            this.LED_In16 = new HslCommunication.Controls.UserLantern();
            this.LED_In25 = new HslCommunication.Controls.UserLantern();
            this.userLanternIO = new HslCommunication.Controls.UserLantern();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.强行清料并包装模式 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.userLantern3 = new HslCommunication.Controls.UserLantern();
            this.LEDLasetInvoke = new HslCommunication.Controls.UserLantern();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.txtWorkOrder = new DevExpress.XtraEditors.TextEdit();
            this.btm手动切换型号 = new DevExpress.XtraEditors.SimpleButton();
            this.下料数据交互触发 = new DevExpress.XtraEditors.SimpleButton();
            this.btm清料 = new DevExpress.XtraEditors.SimpleButton();
            this.相机1拍照触发 = new DevExpress.XtraEditors.SimpleButton();
            this.下料组盘触发 = new DevExpress.XtraEditors.SimpleButton();
            this.PLC设备安信号 = new DevExpress.XtraEditors.CheckButton();
            this.下料盘已满 = new DevExpress.XtraEditors.CheckButton();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.触发1303 = new DevExpress.XtraEditors.SimpleButton();
            this.txtD1013 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1000 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1007 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1008 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1010 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1003 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1009 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1002 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1012 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1004 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1011 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1001 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1005 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1006 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.rbtmSeep_SupperHigh = new System.Windows.Forms.RadioButton();
            this.rbtmSeep_High = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.cobMoveDst = new System.Windows.Forms.ComboBox();
            this.rbtmSeep_Middel = new System.Windows.Forms.RadioButton();
            this.rbtmSeep_Low = new System.Windows.Forms.RadioButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btmRDecre = new DevExpress.XtraEditors.SimpleButton();
            this.btmXIncre = new DevExpress.XtraEditors.SimpleButton();
            this.btmXDecre = new DevExpress.XtraEditors.SimpleButton();
            this.机器人报警清除 = new DevExpress.XtraEditors.SimpleButton();
            this.btmYDecre = new DevExpress.XtraEditors.SimpleButton();
            this.btmYIncre = new DevExpress.XtraEditors.SimpleButton();
            this.机器人回原点 = new DevExpress.XtraEditors.SimpleButton();
            this.伺服启用禁用 = new DevExpress.XtraEditors.CheckButton();
            this.btmZDecre = new DevExpress.XtraEditors.SimpleButton();
            this.btmZIncre = new DevExpress.XtraEditors.SimpleButton();
            this.btmRIncre = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl14 = new DevExpress.XtraEditors.GroupControl();
            this.txtStepDst = new DevExpress.XtraEditors.TextEdit();
            this.电机间距Dcre = new DevExpress.XtraEditors.SimpleButton();
            this.电机间距Incre = new DevExpress.XtraEditors.SimpleButton();
            this.间距电机回原点 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton25 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton26 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton27 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton28 = new DevExpress.XtraEditors.SimpleButton();
            this.NP_生产数据查询 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl3 = new DevExpress.XtraTab.XtraTabControl();
            this.烧录区域1数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea1 = new System.Windows.Forms.TextBox();
            this.dataGridArea1 = new System.Windows.Forms.DataGridView();
            this.烧录区域2数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea2 = new System.Windows.Forms.TextBox();
            this.dataGridArea2 = new System.Windows.Forms.DataGridView();
            this.烧录区域3数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea3 = new System.Windows.Forms.TextBox();
            this.dataGridArea3 = new System.Windows.Forms.DataGridView();
            this.烧录区域4数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea4 = new System.Windows.Forms.TextBox();
            this.dataGridArea4 = new System.Windows.Forms.DataGridView();
            this.烧录区域5数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea5 = new System.Windows.Forms.TextBox();
            this.dataGridArea5 = new System.Windows.Forms.DataGridView();
            this.烧录区域6数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea6 = new System.Windows.Forms.TextBox();
            this.dataGridArea6 = new System.Windows.Forms.DataGridView();
            this.烧录区域7数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea7 = new System.Windows.Forms.TextBox();
            this.dataGridArea7 = new System.Windows.Forms.DataGridView();
            this.烧录区域8数据 = new DevExpress.XtraTab.XtraTabPage();
            this.txtNameArea8 = new System.Windows.Forms.TextBox();
            this.dataGridArea8 = new System.Windows.Forms.DataGridView();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.NP_工艺参数 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.spedSetCurrentTray = new DevExpress.XtraEditors.SpinEdit();
            this.btmSetCurrentTray = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton30 = new DevExpress.XtraEditors.SimpleButton();
            this.btmSaveConfig2 = new DevExpress.XtraEditors.SimpleButton();
            this.gridBuffTrayResult = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn14 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn17 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn19 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn20 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gridLoadTrayResult = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.缓存盘设置为空盘 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.btmUpdateRow = new DevExpress.XtraEditors.SimpleButton();
            this.speStartBuffCol = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.speCoutOfCatchUsed = new DevExpress.XtraEditors.SpinEdit();
            this.speStartBuffRow = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btmSetBuffStartPos = new DevExpress.XtraEditors.SimpleButton();
            this.speColDst = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.speRowDst = new DevExpress.XtraEditors.SpinEdit();
            this.btmSetBuffTrayFull = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.speCols = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.speRows = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.NP_视觉参数 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.图像循环测试 = new System.Windows.Forms.Button();
            this.chk采集新的图片 = new DevExpress.XtraEditors.CheckEdit();
            this.spedDelayCameraSet = new DevExpress.XtraEditors.SpinEdit();
            this.label74 = new System.Windows.Forms.Label();
            this.chkLaserMarkTest_Down = new DevExpress.XtraEditors.CheckEdit();
            this.chkLaserMarkTest_Up = new DevExpress.XtraEditors.CheckEdit();
            this.txtImageProcessFile = new System.Windows.Forms.ComboBox();
            this.btmLoad = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.chkNewTemplate = new DevExpress.XtraEditors.CheckEdit();
            this.btmSaveVisionPara = new DevExpress.XtraEditors.SimpleButton();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.trackContrast_1 = new DevExpress.XtraEditors.TrackBarControl();
            this.trackExposure_1 = new DevExpress.XtraEditors.TrackBarControl();
            this.测试删除1 = new DevExpress.XtraEditors.SimpleButton();
            this.测试增加1 = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
            this.运行测试1 = new DevExpress.XtraEditors.SimpleButton();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit3 = new DevExpress.XtraEditors.SpinEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.spinEdit4 = new DevExpress.XtraEditors.SpinEdit();
            this.测试删除2 = new DevExpress.XtraEditors.SimpleButton();
            this.测试增加2 = new DevExpress.XtraEditors.SimpleButton();
            this.trackContrast_2 = new DevExpress.XtraEditors.TrackBarControl();
            this.运行测试2 = new DevExpress.XtraEditors.SimpleButton();
            this.trackExposure_2 = new DevExpress.XtraEditors.TrackBarControl();
            this.listBoxControl2 = new DevExpress.XtraEditors.ListBoxControl();
            this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.tabNavigationPage3 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.label77 = new System.Windows.Forms.Label();
            this.spedLS_Value_4 = new DevExpress.XtraEditors.SpinEdit();
            this.trackLightSource_4 = new DevExpress.XtraEditors.TrackBarControl();
            this.label78 = new System.Windows.Forms.Label();
            this.spedLS_Value_3 = new DevExpress.XtraEditors.SpinEdit();
            this.trackLightSource_3 = new DevExpress.XtraEditors.TrackBarControl();
            this.label76 = new System.Windows.Forms.Label();
            this.spedLS_Value_2 = new DevExpress.XtraEditors.SpinEdit();
            this.trackLightSource_2 = new DevExpress.XtraEditors.TrackBarControl();
            this.label75 = new System.Windows.Forms.Label();
            this.spedLS_Value_1 = new DevExpress.XtraEditors.SpinEdit();
            this.trackLightSource_1 = new DevExpress.XtraEditors.TrackBarControl();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.label62 = new System.Windows.Forms.Label();
            this.UnloadCfg_Remaind = new DevExpress.XtraEditors.SpinEdit();
            this.UnloadCfg_TotalProduct = new DevExpress.XtraEditors.SpinEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.UnloadCfg_CountTrayBK = new DevExpress.XtraEditors.SpinEdit();
            this.sped生产总盘数 = new DevExpress.XtraEditors.SpinEdit();
            this.生产最后打包总盘数 = new DevExpress.XtraEditors.SpinEdit();
            this.UnloadCfg_CountTray = new DevExpress.XtraEditors.SpinEdit();
            this.UnloadCfg_Col = new DevExpress.XtraEditors.SpinEdit();
            this.UnloadCfg_Total = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.label53 = new System.Windows.Forms.Label();
            this.UnloadCfg_Row = new DevExpress.XtraEditors.SpinEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.groupControl18 = new DevExpress.XtraEditors.GroupControl();
            this.更新下料总数 = new DevExpress.XtraEditors.SimpleButton();
            this.spedCurrentUnitUnloadedToPackingStationNew = new DevExpress.XtraEditors.SpinEdit();
            this.spedTotalFailedCount = new DevExpress.XtraEditors.SpinEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.spedCurrentUnitUnloadedToPackingStation = new DevExpress.XtraEditors.SpinEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.TotalLineOutAll = new DevExpress.XtraEditors.SpinEdit();
            this.speCountOfBuffTray = new DevExpress.XtraEditors.SpinEdit();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.speCountOfLoadTray = new DevExpress.XtraEditors.SpinEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.btmSetCurrentTrayOut = new DevExpress.XtraEditors.SimpleButton();
            this.label60 = new System.Windows.Forms.Label();
            this.TotalTrayOfLineOutPCS = new DevExpress.XtraEditors.SpinEdit();
            this.spedSetTrayOuted = new DevExpress.XtraEditors.SpinEdit();
            this.TotalTrayOfLineOut = new DevExpress.XtraEditors.SpinEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.CurrentCountTray = new DevExpress.XtraEditors.SpinEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.timer_IO = new System.Windows.Forms.Timer(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.timerFlashData = new System.Windows.Forms.Timer(this.components);
            this.timer_GC = new System.Windows.Forms.Timer(this.components);
            this.timer_Display_ExeStatus = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.NP_主页运行.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).BeginInit();
            this.groupControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt已识别的好品数量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPHCamera2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUPH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailedCamera2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassedCamera2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFailed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPHCamera1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPassed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailedCamera1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassedCamera1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupInfor2)).BeginInit();
            this.PopupInfor2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl4)).BeginInit();
            this.xtraTabControl4.SuspendLayout();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewFlashResult)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.NP_设置.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotFailed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotQA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotTransit_VToQA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotTransit_ULToF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotTransit_LToF.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotCamera.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotUnload.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotCatch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotOrg.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).BeginInit();
            this.groupControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_LoadFirst2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_BuffFirst2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomizeRowDst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speColOfSingleDst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRowOfSingleDst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speColOfSingle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRowOfSingle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_LoadFirst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_BuffFirst.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboxRunTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboxRunFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRobotWayList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).BeginInit();
            this.groupControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spedOrgStepDst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDstRow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCathDst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).BeginInit();
            this.groupControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            this.NP_手动调试.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl19)).BeginInit();
            this.groupControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.强制camera2结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.强制camera1结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableRobotIO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCareFlashResult.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVacuuSensor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.强行清料并包装模式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1013.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1000.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1007.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1008.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1010.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1003.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1009.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1002.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1012.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1004.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1011.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1001.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1005.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1006.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).BeginInit();
            this.groupControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStepDst.Properties)).BeginInit();
            this.NP_生产数据查询.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).BeginInit();
            this.xtraTabControl3.SuspendLayout();
            this.烧录区域1数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea1)).BeginInit();
            this.烧录区域2数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea2)).BeginInit();
            this.烧录区域3数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea3)).BeginInit();
            this.烧录区域4数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea4)).BeginInit();
            this.烧录区域5数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea5)).BeginInit();
            this.烧录区域6数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea6)).BeginInit();
            this.烧录区域7数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea7)).BeginInit();
            this.烧录区域8数据.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea8)).BeginInit();
            this.NP_工艺参数.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spedSetCurrentTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBuffTrayResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLoadTrayResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speStartBuffCol.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCoutOfCatchUsed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speStartBuffRow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speColDst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRowDst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCols.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRows.Properties)).BeginInit();
            this.NP_视觉参数.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk采集新的图片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedDelayCameraSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Down.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Up.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewTemplate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.tabNavigationPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Remaind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_TotalProduct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTrayBK.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产总盘数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.生产最后打包总盘数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Col.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Total.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Row.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).BeginInit();
            this.groupControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spedCurrentUnitUnloadedToPackingStationNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedTotalFailedCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedCurrentUnitUnloadedToPackingStation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLineOutAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfBuffTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfLoadTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOutPCS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedSetTrayOuted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentCountTray.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // navigationPane1
            // 
            this.navigationPane1.AllowHtmlDraw = true;
            this.navigationPane1.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.Appearance.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Hovered.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.AppearanceButton.Hovered.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Normal.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.AppearanceButton.Normal.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Pressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.navigationPane1.AppearanceButton.Pressed.Options.UseFont = true;
            this.navigationPane1.Controls.Add(this.NP_主页运行);
            this.navigationPane1.Controls.Add(this.NP_设置);
            this.navigationPane1.Controls.Add(this.NP_手动调试);
            this.navigationPane1.Controls.Add(this.NP_生产数据查询);
            this.navigationPane1.Controls.Add(this.NP_工艺参数);
            this.navigationPane1.Controls.Add(this.NP_视觉参数);
            this.navigationPane1.Location = new System.Drawing.Point(0, 0);
            this.navigationPane1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AllowBorderColorBlending = true;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.NP_主页运行,
            this.NP_设置,
            this.NP_视觉参数,
            this.NP_工艺参数,
            this.NP_手动调试,
            this.NP_生产数据查询});
            this.navigationPane1.RegularSize = new System.Drawing.Size(1904, 706);
            this.navigationPane1.RibbonAndBarsMergeStyle = DevExpress.XtraBars.Docking2010.Views.RibbonAndBarsMergeStyle.Always;
            this.navigationPane1.SelectedPage = this.NP_主页运行;
            this.navigationPane1.ShowToolTips = DevExpress.Utils.DefaultBoolean.True;
            this.navigationPane1.Size = new System.Drawing.Size(1904, 706);
            this.navigationPane1.TabIndex = 1;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // NP_主页运行
            // 
            this.NP_主页运行.Caption = " 主页运行";
            this.NP_主页运行.Controls.Add(this.btm进入尾料模式);
            this.NP_主页运行.Controls.Add(this.LEDRobot);
            this.NP_主页运行.Controls.Add(this.LEDModbusServer_Robot);
            this.NP_主页运行.Controls.Add(this.labelControl46);
            this.NP_主页运行.Controls.Add(this.LEDUnloadPLCState);
            this.NP_主页运行.Controls.Add(this.txtTrayID);
            this.NP_主页运行.Controls.Add(this.txtTrayID2);
            this.NP_主页运行.Controls.Add(this.labelControl49);
            this.NP_主页运行.Controls.Add(this.panelControl2);
            this.NP_主页运行.Controls.Add(this.panelControl1);
            this.NP_主页运行.Controls.Add(this.label23);
            this.NP_主页运行.Controls.Add(this.label12);
            this.NP_主页运行.Controls.Add(this.LEDModbusServer);
            this.NP_主页运行.Controls.Add(this.labelControl24);
            this.NP_主页运行.Controls.Add(this.richTextCamera2);
            this.NP_主页运行.Controls.Add(this.label11);
            this.NP_主页运行.Controls.Add(this.richTextCamera1);
            this.NP_主页运行.Controls.Add(this.label10);
            this.NP_主页运行.Controls.Add(this.richTextRobot);
            this.NP_主页运行.Controls.Add(this.label25);
            this.NP_主页运行.Controls.Add(this.checkButton1);
            this.NP_主页运行.Controls.Add(this.checkButton2);
            this.NP_主页运行.Controls.Add(this.labelControl7);
            this.NP_主页运行.Controls.Add(this.labelControl8);
            this.NP_主页运行.Controls.Add(this.labelControl6);
            this.NP_主页运行.Controls.Add(this.labelControl5);
            this.NP_主页运行.Controls.Add(this.userLantern2);
            this.NP_主页运行.Controls.Add(this.userLantern1);
            this.NP_主页运行.Controls.Add(this.btmInvoke_StopMachine);
            this.NP_主页运行.Controls.Add(this.btmInvoke_StartMachine);
            this.NP_主页运行.Controls.Add(this.btmInvoke_ResetMachine);
            this.NP_主页运行.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.NP_主页运行.Image = ((System.Drawing.Image)(resources.GetObject("NP_主页运行.Image")));
            this.NP_主页运行.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_主页运行.Name = "NP_主页运行";
            this.NP_主页运行.Size = new System.Drawing.Size(1684, 647);
            // 
            // btm进入尾料模式
            // 
            this.btm进入尾料模式.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btm进入尾料模式.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.btm进入尾料模式.Appearance.Options.UseFont = true;
            this.btm进入尾料模式.Appearance.Options.UseForeColor = true;
            this.btm进入尾料模式.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm进入尾料模式.AppearanceDisabled.ForeColor = System.Drawing.Color.Blue;
            this.btm进入尾料模式.AppearanceDisabled.Options.UseFont = true;
            this.btm进入尾料模式.AppearanceDisabled.Options.UseForeColor = true;
            this.btm进入尾料模式.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm进入尾料模式.AppearanceHovered.ForeColor = System.Drawing.Color.Blue;
            this.btm进入尾料模式.AppearanceHovered.Options.UseFont = true;
            this.btm进入尾料模式.AppearanceHovered.Options.UseForeColor = true;
            this.btm进入尾料模式.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm进入尾料模式.AppearancePressed.ForeColor = System.Drawing.Color.Fuchsia;
            this.btm进入尾料模式.AppearancePressed.Options.UseFont = true;
            this.btm进入尾料模式.AppearancePressed.Options.UseForeColor = true;
            this.btm进入尾料模式.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btm进入尾料模式.Location = new System.Drawing.Point(473, 11);
            this.btm进入尾料模式.Name = "btm进入尾料模式";
            this.btm进入尾料模式.Size = new System.Drawing.Size(212, 73);
            this.btm进入尾料模式.TabIndex = 219;
            this.btm进入尾料模式.Text = "进入尾料模式";
            this.btm进入尾料模式.CheckedChanged += new System.EventHandler(this.btm进入尾料模式_CheckedChanged);
            this.btm进入尾料模式.Click += new System.EventHandler(this.btm进入尾料模式_Click);
            // 
            // LEDRobot
            // 
            this.LEDRobot.BackColor = System.Drawing.Color.Transparent;
            this.LEDRobot.LanternBackground = System.Drawing.Color.Gray;
            this.LEDRobot.Location = new System.Drawing.Point(1002, 22);
            this.LEDRobot.Margin = new System.Windows.Forms.Padding(15, 33, 15, 33);
            this.LEDRobot.Name = "LEDRobot";
            this.LEDRobot.Size = new System.Drawing.Size(32, 32);
            this.LEDRobot.TabIndex = 218;
            // 
            // LEDModbusServer_Robot
            // 
            this.LEDModbusServer_Robot.BackColor = System.Drawing.Color.Transparent;
            this.LEDModbusServer_Robot.LanternBackground = System.Drawing.Color.Gray;
            this.LEDModbusServer_Robot.Location = new System.Drawing.Point(1244, 22);
            this.LEDModbusServer_Robot.Margin = new System.Windows.Forms.Padding(7, 14, 7, 14);
            this.LEDModbusServer_Robot.Name = "LEDModbusServer_Robot";
            this.LEDModbusServer_Robot.Size = new System.Drawing.Size(32, 32);
            this.LEDModbusServer_Robot.TabIndex = 217;
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.AppearanceDisabled.Options.UseFont = true;
            this.labelControl46.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.AppearanceHovered.Options.UseFont = true;
            this.labelControl46.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl46.AppearancePressed.Options.UseFont = true;
            this.labelControl46.Location = new System.Drawing.Point(82, 100);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(212, 17);
            this.labelControl46.TabIndex = 209;
            this.labelControl46.Text = "产品信息/载板/条码/测试结果/测量统计";
            // 
            // LEDUnloadPLCState
            // 
            this.LEDUnloadPLCState.BackColor = System.Drawing.Color.Transparent;
            this.LEDUnloadPLCState.LanternBackground = System.Drawing.Color.Gray;
            this.LEDUnloadPLCState.Location = new System.Drawing.Point(1111, 22);
            this.LEDUnloadPLCState.Margin = new System.Windows.Forms.Padding(7, 14, 7, 14);
            this.LEDUnloadPLCState.Name = "LEDUnloadPLCState";
            this.LEDUnloadPLCState.Size = new System.Drawing.Size(32, 32);
            this.LEDUnloadPLCState.TabIndex = 216;
            // 
            // txtTrayID
            // 
            this.txtTrayID.EditValue = "";
            this.txtTrayID.Location = new System.Drawing.Point(301, 97);
            this.txtTrayID.Name = "txtTrayID";
            this.txtTrayID.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTrayID.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTrayID.Properties.Appearance.Options.UseBackColor = true;
            this.txtTrayID.Properties.Appearance.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTrayID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTrayID.Properties.PopupControl = this.popupContainerControl1;
            this.txtTrayID.Size = new System.Drawing.Size(366, 24);
            this.txtTrayID.TabIndex = 6;
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.popupContainerControl1.Appearance.Options.UseFont = true;
            this.popupContainerControl1.Controls.Add(this.得到顶部测试结果);
            this.popupContainerControl1.Controls.Add(this.groupControl17);
            this.popupContainerControl1.Controls.Add(this.labelControl48);
            this.popupContainerControl1.Controls.Add(this.button1);
            this.popupContainerControl1.Controls.Add(this.txtPCBSN);
            this.popupContainerControl1.Location = new System.Drawing.Point(208, 2);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(456, 369);
            this.popupContainerControl1.TabIndex = 7;
            // 
            // 得到顶部测试结果
            // 
            this.得到顶部测试结果.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.得到顶部测试结果.Appearance.Options.UseFont = true;
            this.得到顶部测试结果.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到顶部测试结果.AppearanceDisabled.Options.UseFont = true;
            this.得到顶部测试结果.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到顶部测试结果.AppearanceHovered.Options.UseFont = true;
            this.得到顶部测试结果.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.得到顶部测试结果.AppearancePressed.Options.UseFont = true;
            this.得到顶部测试结果.Location = new System.Drawing.Point(320, 48);
            this.得到顶部测试结果.Name = "得到顶部测试结果";
            this.得到顶部测试结果.Size = new System.Drawing.Size(131, 48);
            this.得到顶部测试结果.TabIndex = 215;
            this.得到顶部测试结果.Text = "得到顶部测试结果";
            this.得到顶部测试结果.Click += new System.EventHandler(this.得到顶部测试结果_Click);
            // 
            // groupControl17
            // 
            this.groupControl17.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl17.Appearance.Options.UseFont = true;
            this.groupControl17.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl17.AppearanceCaption.Options.UseFont = true;
            this.groupControl17.Controls.Add(this.txt已识别的好品数量);
            this.groupControl17.Controls.Add(this.labelControl65);
            this.groupControl17.Controls.Add(this.label71);
            this.groupControl17.Controls.Add(this.label70);
            this.groupControl17.Controls.Add(this.label69);
            this.groupControl17.Controls.Add(this.txtUPHCamera2);
            this.groupControl17.Controls.Add(this.txtTotalUPH);
            this.groupControl17.Controls.Add(this.txtFailedCamera2);
            this.groupControl17.Controls.Add(this.txtPassedCamera2);
            this.groupControl17.Controls.Add(this.txtTotalFailed);
            this.groupControl17.Controls.Add(this.simpleButton29);
            this.groupControl17.Controls.Add(this.txtUPHCamera1);
            this.groupControl17.Controls.Add(this.txtTotalPassed);
            this.groupControl17.Controls.Add(this.labelControl41);
            this.groupControl17.Controls.Add(this.txtFailedCamera1);
            this.groupControl17.Controls.Add(this.labelControl42);
            this.groupControl17.Controls.Add(this.txtPassedCamera1);
            this.groupControl17.Controls.Add(this.labelControl43);
            this.groupControl17.Location = new System.Drawing.Point(116, 108);
            this.groupControl17.Name = "groupControl17";
            this.groupControl17.Size = new System.Drawing.Size(335, 256);
            this.groupControl17.TabIndex = 205;
            this.groupControl17.Text = "外观拍照统计结果(相机1, 相机2)";
            // 
            // txt已识别的好品数量
            // 
            this.txt已识别的好品数量.Location = new System.Drawing.Point(118, 177);
            this.txt已识别的好品数量.Name = "txt已识别的好品数量";
            this.txt已识别的好品数量.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt已识别的好品数量.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.Appearance.Options.UseBackColor = true;
            this.txt已识别的好品数量.Properties.Appearance.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.AppearanceFocused.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt已识别的好品数量.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txt已识别的好品数量.Properties.ReadOnly = true;
            this.txt已识别的好品数量.Size = new System.Drawing.Size(104, 24);
            this.txt已识别的好品数量.TabIndex = 206;
            // 
            // labelControl65
            // 
            this.labelControl65.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl65.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.Appearance.Options.UseBackColor = true;
            this.labelControl65.Appearance.Options.UseFont = true;
            this.labelControl65.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.AppearanceDisabled.Options.UseFont = true;
            this.labelControl65.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.AppearanceHovered.Options.UseFont = true;
            this.labelControl65.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl65.AppearancePressed.Options.UseFont = true;
            this.labelControl65.Location = new System.Drawing.Point(16, 181);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(96, 17);
            this.labelControl65.TabIndex = 205;
            this.labelControl65.Text = "已识别的好品数量";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.BackColor = System.Drawing.Color.LightSalmon;
            this.label71.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label71.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label71.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label71.Location = new System.Drawing.Point(262, 41);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(42, 19);
            this.label71.TabIndex = 204;
            this.label71.Text = "所有()";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.Color.LightSalmon;
            this.label70.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label70.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label70.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label70.Location = new System.Drawing.Point(160, 41);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(73, 19);
            this.label70.TabIndex = 203;
            this.label70.Text = "相机2(当前)";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.Color.LightSalmon;
            this.label69.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label69.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label69.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label69.Location = new System.Drawing.Point(73, 41);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(73, 19);
            this.label69.TabIndex = 202;
            this.label69.Text = "相机1(当前)";
            // 
            // txtUPHCamera2
            // 
            this.txtUPHCamera2.Location = new System.Drawing.Point(168, 135);
            this.txtUPHCamera2.Name = "txtUPHCamera2";
            this.txtUPHCamera2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtUPHCamera2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera2.Properties.Appearance.Options.UseBackColor = true;
            this.txtUPHCamera2.Properties.Appearance.Options.UseFont = true;
            this.txtUPHCamera2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtUPHCamera2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtUPHCamera2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtUPHCamera2.Properties.ReadOnly = true;
            this.txtUPHCamera2.Size = new System.Drawing.Size(54, 24);
            this.txtUPHCamera2.TabIndex = 11;
            // 
            // txtTotalUPH
            // 
            this.txtTotalUPH.Location = new System.Drawing.Point(244, 135);
            this.txtTotalUPH.Name = "txtTotalUPH";
            this.txtTotalUPH.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalUPH.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalUPH.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalUPH.Properties.Appearance.Options.UseFont = true;
            this.txtTotalUPH.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalUPH.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTotalUPH.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalUPH.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTotalUPH.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalUPH.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTotalUPH.Properties.ReadOnly = true;
            this.txtTotalUPH.Size = new System.Drawing.Size(76, 24);
            this.txtTotalUPH.TabIndex = 5;
            // 
            // txtFailedCamera2
            // 
            this.txtFailedCamera2.Location = new System.Drawing.Point(168, 104);
            this.txtFailedCamera2.Name = "txtFailedCamera2";
            this.txtFailedCamera2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtFailedCamera2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera2.Properties.Appearance.Options.UseBackColor = true;
            this.txtFailedCamera2.Properties.Appearance.Options.UseFont = true;
            this.txtFailedCamera2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtFailedCamera2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtFailedCamera2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtFailedCamera2.Properties.ReadOnly = true;
            this.txtFailedCamera2.Size = new System.Drawing.Size(54, 24);
            this.txtFailedCamera2.TabIndex = 10;
            // 
            // txtPassedCamera2
            // 
            this.txtPassedCamera2.Location = new System.Drawing.Point(168, 74);
            this.txtPassedCamera2.Name = "txtPassedCamera2";
            this.txtPassedCamera2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPassedCamera2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera2.Properties.Appearance.Options.UseBackColor = true;
            this.txtPassedCamera2.Properties.Appearance.Options.UseFont = true;
            this.txtPassedCamera2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPassedCamera2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPassedCamera2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPassedCamera2.Properties.ReadOnly = true;
            this.txtPassedCamera2.Size = new System.Drawing.Size(54, 24);
            this.txtPassedCamera2.TabIndex = 9;
            // 
            // txtTotalFailed
            // 
            this.txtTotalFailed.Location = new System.Drawing.Point(244, 105);
            this.txtTotalFailed.Name = "txtTotalFailed";
            this.txtTotalFailed.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalFailed.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalFailed.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalFailed.Properties.Appearance.Options.UseFont = true;
            this.txtTotalFailed.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalFailed.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTotalFailed.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalFailed.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTotalFailed.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalFailed.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTotalFailed.Properties.ReadOnly = true;
            this.txtTotalFailed.Size = new System.Drawing.Size(76, 24);
            this.txtTotalFailed.TabIndex = 3;
            // 
            // simpleButton29
            // 
            this.simpleButton29.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton29.Appearance.Options.UseFont = true;
            this.simpleButton29.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton29.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton29.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton29.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton29.AppearanceHovered.Image")));
            this.simpleButton29.AppearanceHovered.Options.UseFont = true;
            this.simpleButton29.AppearanceHovered.Options.UseImage = true;
            this.simpleButton29.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton29.AppearancePressed.Options.UseFont = true;
            this.simpleButton29.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton29.ImageOptions.Image")));
            this.simpleButton29.Location = new System.Drawing.Point(228, 213);
            this.simpleButton29.Name = "simpleButton29";
            this.simpleButton29.Size = new System.Drawing.Size(102, 38);
            this.simpleButton29.TabIndex = 8;
            this.simpleButton29.Text = "清零";
            this.simpleButton29.Click += new System.EventHandler(this.simpleButton29_Click);
            // 
            // txtUPHCamera1
            // 
            this.txtUPHCamera1.Location = new System.Drawing.Point(78, 134);
            this.txtUPHCamera1.Name = "txtUPHCamera1";
            this.txtUPHCamera1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtUPHCamera1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera1.Properties.Appearance.Options.UseBackColor = true;
            this.txtUPHCamera1.Properties.Appearance.Options.UseFont = true;
            this.txtUPHCamera1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtUPHCamera1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera1.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtUPHCamera1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPHCamera1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtUPHCamera1.Properties.ReadOnly = true;
            this.txtUPHCamera1.Size = new System.Drawing.Size(54, 24);
            this.txtUPHCamera1.TabIndex = 5;
            // 
            // txtTotalPassed
            // 
            this.txtTotalPassed.Location = new System.Drawing.Point(244, 74);
            this.txtTotalPassed.Name = "txtTotalPassed";
            this.txtTotalPassed.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalPassed.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalPassed.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalPassed.Properties.Appearance.Options.UseFont = true;
            this.txtTotalPassed.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalPassed.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTotalPassed.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalPassed.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTotalPassed.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTotalPassed.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTotalPassed.Properties.ReadOnly = true;
            this.txtTotalPassed.Size = new System.Drawing.Size(76, 24);
            this.txtTotalPassed.TabIndex = 1;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl41.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.Appearance.Options.UseBackColor = true;
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.AppearanceDisabled.Options.UseFont = true;
            this.labelControl41.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.AppearanceHovered.Options.UseFont = true;
            this.labelControl41.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl41.AppearancePressed.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(39, 138);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(25, 17);
            this.labelControl41.TabIndex = 4;
            this.labelControl41.Text = "UPH";
            // 
            // txtFailedCamera1
            // 
            this.txtFailedCamera1.Location = new System.Drawing.Point(78, 104);
            this.txtFailedCamera1.Name = "txtFailedCamera1";
            this.txtFailedCamera1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtFailedCamera1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera1.Properties.Appearance.Options.UseBackColor = true;
            this.txtFailedCamera1.Properties.Appearance.Options.UseFont = true;
            this.txtFailedCamera1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtFailedCamera1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera1.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtFailedCamera1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedCamera1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtFailedCamera1.Properties.ReadOnly = true;
            this.txtFailedCamera1.Size = new System.Drawing.Size(54, 24);
            this.txtFailedCamera1.TabIndex = 3;
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl42.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.Appearance.Options.UseBackColor = true;
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.AppearanceDisabled.Options.UseFont = true;
            this.labelControl42.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.AppearanceHovered.Options.UseFont = true;
            this.labelControl42.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl42.AppearancePressed.Options.UseFont = true;
            this.labelControl42.Location = new System.Drawing.Point(16, 108);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(48, 17);
            this.labelControl42.TabIndex = 2;
            this.labelControl42.Text = "坏品数量";
            // 
            // txtPassedCamera1
            // 
            this.txtPassedCamera1.Location = new System.Drawing.Point(78, 74);
            this.txtPassedCamera1.Name = "txtPassedCamera1";
            this.txtPassedCamera1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtPassedCamera1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera1.Properties.Appearance.Options.UseBackColor = true;
            this.txtPassedCamera1.Properties.Appearance.Options.UseFont = true;
            this.txtPassedCamera1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPassedCamera1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera1.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPassedCamera1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedCamera1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPassedCamera1.Properties.ReadOnly = true;
            this.txtPassedCamera1.Size = new System.Drawing.Size(54, 24);
            this.txtPassedCamera1.TabIndex = 1;
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl43.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.Appearance.Options.UseBackColor = true;
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.AppearanceDisabled.Options.UseFont = true;
            this.labelControl43.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.AppearanceHovered.Options.UseFont = true;
            this.labelControl43.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl43.AppearancePressed.Options.UseFont = true;
            this.labelControl43.Location = new System.Drawing.Point(16, 78);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(48, 17);
            this.labelControl43.TabIndex = 0;
            this.labelControl43.Text = "好品数量";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.AppearanceDisabled.Options.UseFont = true;
            this.labelControl48.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.AppearanceHovered.Options.UseFont = true;
            this.labelControl48.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl48.AppearancePressed.Options.UseFont = true;
            this.labelControl48.Location = new System.Drawing.Point(6, 13);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(72, 17);
            this.labelControl48.TabIndex = 214;
            this.labelControl48.Text = "产品虚拟条码";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button1.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.button1.Location = new System.Drawing.Point(348, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 40);
            this.button1.TabIndex = 204;
            this.button1.Text = "手动拍照测试";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtPCBSN
            // 
            this.txtPCBSN.EditValue = "";
            this.txtPCBSN.Location = new System.Drawing.Point(81, 8);
            this.txtPCBSN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBSN.Name = "txtPCBSN";
            this.txtPCBSN.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBSN.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBSN.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBSN.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseFont = true;
            this.txtPCBSN.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Size = new System.Drawing.Size(202, 28);
            this.txtPCBSN.TabIndex = 207;
            // 
            // txtTrayID2
            // 
            this.txtTrayID2.EditValue = "";
            this.txtTrayID2.Location = new System.Drawing.Point(971, 97);
            this.txtTrayID2.Name = "txtTrayID2";
            this.txtTrayID2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTrayID2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTrayID2.Properties.Appearance.Options.UseBackColor = true;
            this.txtTrayID2.Properties.Appearance.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTrayID2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTrayID2.Properties.PopupControl = this.PopupInfor2;
            this.txtTrayID2.Size = new System.Drawing.Size(366, 24);
            this.txtTrayID2.TabIndex = 4;
            // 
            // PopupInfor2
            // 
            this.PopupInfor2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PopupInfor2.Appearance.Options.UseFont = true;
            this.PopupInfor2.Controls.Add(this.labelControl47);
            this.PopupInfor2.Controls.Add(this.xtraTabControl4);
            this.PopupInfor2.Controls.Add(this.txtPCBSN2);
            this.PopupInfor2.Controls.Add(this.button2);
            this.PopupInfor2.Location = new System.Drawing.Point(204, 2);
            this.PopupInfor2.Name = "PopupInfor2";
            this.PopupInfor2.Size = new System.Drawing.Size(460, 446);
            this.PopupInfor2.TabIndex = 5;
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.AppearanceDisabled.Options.UseFont = true;
            this.labelControl47.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.AppearanceHovered.Options.UseFont = true;
            this.labelControl47.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl47.AppearancePressed.Options.UseFont = true;
            this.labelControl47.Location = new System.Drawing.Point(6, 14);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(72, 17);
            this.labelControl47.TabIndex = 213;
            this.labelControl47.Text = "产品虚拟条码";
            // 
            // xtraTabControl4
            // 
            this.xtraTabControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl4.Appearance.Options.UseFont = true;
            this.xtraTabControl4.AppearancePage.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl4.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl4.AppearancePage.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl4.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl4.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl4.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.xtraTabControl4.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl4.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabControl4.AppearancePage.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl4.AppearancePage.PageClient.Options.UseFont = true;
            this.xtraTabControl4.Location = new System.Drawing.Point(5, 49);
            this.xtraTabControl4.Name = "xtraTabControl4";
            this.xtraTabControl4.SelectedTabPage = this.xtraTabPage6;
            this.xtraTabControl4.Size = new System.Drawing.Size(411, 394);
            this.xtraTabControl4.TabIndex = 212;
            this.xtraTabControl4.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage6,
            this.xtraTabPage7});
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Appearance.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage6.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage6.Appearance.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage6.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage6.Appearance.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage6.Appearance.HeaderDisabled.Options.UseFont = true;
            this.xtraTabPage6.Appearance.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage6.Appearance.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabPage6.Appearance.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage6.Appearance.PageClient.Options.UseFont = true;
            this.xtraTabPage6.Controls.Add(this.panelControl5);
            this.xtraTabPage6.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage6.Image")));
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(405, 362);
            this.xtraTabPage6.Text = "烧录数据";
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.panelControl5.Appearance.Options.UseFont = true;
            this.panelControl5.Controls.Add(this.btmUnbindTray);
            this.panelControl5.Controls.Add(this.viewFlashResult);
            this.panelControl5.Controls.Add(this.btmGetFlashData);
            this.panelControl5.Location = new System.Drawing.Point(0, 0);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(410, 362);
            this.panelControl5.TabIndex = 2;
            // 
            // btmUnbindTray
            // 
            this.btmUnbindTray.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmUnbindTray.Appearance.Options.UseFont = true;
            this.btmUnbindTray.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmUnbindTray.AppearanceDisabled.Options.UseFont = true;
            this.btmUnbindTray.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmUnbindTray.AppearanceHovered.Options.UseFont = true;
            this.btmUnbindTray.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmUnbindTray.AppearancePressed.Options.UseFont = true;
            this.btmUnbindTray.Location = new System.Drawing.Point(147, 302);
            this.btmUnbindTray.Name = "btmUnbindTray";
            this.btmUnbindTray.Size = new System.Drawing.Size(131, 48);
            this.btmUnbindTray.TabIndex = 2;
            this.btmUnbindTray.Text = "托盘解绑(完成下料)";
            this.btmUnbindTray.Click += new System.EventHandler(this.btmUnbindTray_Click);
            // 
            // viewFlashResult
            // 
            this.viewFlashResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.viewFlashResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30});
            this.viewFlashResult.Location = new System.Drawing.Point(0, 0);
            this.viewFlashResult.Name = "viewFlashResult";
            this.viewFlashResult.RowHeadersWidth = 60;
            this.viewFlashResult.RowTemplate.Height = 23;
            this.viewFlashResult.Size = new System.Drawing.Size(364, 296);
            this.viewFlashResult.TabIndex = 0;
            // 
            // Column21
            // 
            this.Column21.HeaderText = "1";
            this.Column21.Name = "Column21";
            this.Column21.Width = 30;
            // 
            // Column22
            // 
            this.Column22.HeaderText = "2";
            this.Column22.Name = "Column22";
            this.Column22.Width = 30;
            // 
            // Column23
            // 
            this.Column23.HeaderText = "3";
            this.Column23.Name = "Column23";
            this.Column23.Width = 30;
            // 
            // Column24
            // 
            this.Column24.HeaderText = "4";
            this.Column24.Name = "Column24";
            this.Column24.Width = 30;
            // 
            // Column25
            // 
            this.Column25.HeaderText = "5";
            this.Column25.Name = "Column25";
            this.Column25.Width = 30;
            // 
            // Column26
            // 
            this.Column26.HeaderText = "6";
            this.Column26.Name = "Column26";
            this.Column26.Width = 30;
            // 
            // Column27
            // 
            this.Column27.HeaderText = "7";
            this.Column27.Name = "Column27";
            this.Column27.Width = 30;
            // 
            // Column28
            // 
            this.Column28.HeaderText = "8";
            this.Column28.Name = "Column28";
            this.Column28.Width = 30;
            // 
            // Column29
            // 
            this.Column29.HeaderText = "9";
            this.Column29.Name = "Column29";
            this.Column29.Width = 30;
            // 
            // Column30
            // 
            this.Column30.HeaderText = "10";
            this.Column30.Name = "Column30";
            this.Column30.Width = 30;
            // 
            // btmGetFlashData
            // 
            this.btmGetFlashData.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmGetFlashData.Appearance.Options.UseFont = true;
            this.btmGetFlashData.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmGetFlashData.AppearanceDisabled.Options.UseFont = true;
            this.btmGetFlashData.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmGetFlashData.AppearanceHovered.Options.UseFont = true;
            this.btmGetFlashData.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmGetFlashData.AppearancePressed.Options.UseFont = true;
            this.btmGetFlashData.Location = new System.Drawing.Point(5, 302);
            this.btmGetFlashData.Name = "btmGetFlashData";
            this.btmGetFlashData.Size = new System.Drawing.Size(131, 48);
            this.btmGetFlashData.TabIndex = 1;
            this.btmGetFlashData.Text = "得到烧录数据";
            this.btmGetFlashData.Click += new System.EventHandler(this.btmGetFlashData_Click);
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.panelControl6);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(405, 362);
            this.xtraTabPage7.Text = "其它操作";
            // 
            // panelControl6
            // 
            this.panelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.panelControl6.Appearance.Options.UseFont = true;
            this.panelControl6.Location = new System.Drawing.Point(0, 0);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(410, 362);
            this.panelControl6.TabIndex = 3;
            // 
            // txtPCBSN2
            // 
            this.txtPCBSN2.EditValue = "";
            this.txtPCBSN2.Location = new System.Drawing.Point(84, 8);
            this.txtPCBSN2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBSN2.Name = "txtPCBSN2";
            this.txtPCBSN2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBSN2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBSN2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBSN2.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseFont = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Size = new System.Drawing.Size(200, 28);
            this.txtPCBSN2.TabIndex = 211;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button2.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.Location = new System.Drawing.Point(357, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 40);
            this.button2.TabIndex = 204;
            this.button2.Text = "手动拍照测试";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.AppearanceDisabled.Options.UseFont = true;
            this.labelControl49.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.AppearanceHovered.Options.UseFont = true;
            this.labelControl49.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl49.AppearancePressed.Options.UseFont = true;
            this.labelControl49.Location = new System.Drawing.Point(754, 100);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(212, 17);
            this.labelControl49.TabIndex = 208;
            this.labelControl49.Text = "产品信息/载板/条码/测试结果/测量统计";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.PopupInfor2);
            this.panelControl2.Location = new System.Drawing.Point(673, 125);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(664, 519);
            this.panelControl2.TabIndex = 203;
            this.panelControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl2_Paint);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.popupContainerControl1);
            this.panelControl1.Location = new System.Drawing.Point(3, 125);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(664, 519);
            this.panelControl1.TabIndex = 202;
            this.panelControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl1_Paint);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.LightSalmon;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(673, 103);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 19);
            this.label23.TabIndex = 201;
            this.label23.Text = "相机2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.LightSalmon;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(3, 103);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 19);
            this.label12.TabIndex = 200;
            this.label12.Text = "相机1";
            // 
            // LEDModbusServer
            // 
            this.LEDModbusServer.BackColor = System.Drawing.Color.Transparent;
            this.LEDModbusServer.LanternBackground = System.Drawing.Color.Gray;
            this.LEDModbusServer.Location = new System.Drawing.Point(2121, 303);
            this.LEDModbusServer.Margin = new System.Windows.Forms.Padding(338, 6480, 338, 6480);
            this.LEDModbusServer.Name = "LEDModbusServer";
            this.LEDModbusServer.Size = new System.Drawing.Size(69, 91);
            this.LEDModbusServer.TabIndex = 196;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearanceDisabled.Options.UseFont = true;
            this.labelControl24.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearanceHovered.Options.UseFont = true;
            this.labelControl24.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearancePressed.Options.UseFont = true;
            this.labelControl24.Location = new System.Drawing.Point(1201, 63);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(132, 17);
            this.labelControl24.TabIndex = 195;
            this.labelControl24.Text = "Modbus TCP/IP Server";
            // 
            // richTextCamera2
            // 
            this.richTextCamera2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextCamera2.Location = new System.Drawing.Point(1349, 235);
            this.richTextCamera2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextCamera2.Name = "richTextCamera2";
            this.richTextCamera2.Size = new System.Drawing.Size(331, 176);
            this.richTextCamera2.TabIndex = 193;
            this.richTextCamera2.Text = "";
            this.richTextCamera2.WordWrap = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.LightSalmon;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Location = new System.Drawing.Point(1435, 213);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 19);
            this.label11.TabIndex = 194;
            this.label11.Text = "相机2运行信息输出";
            // 
            // richTextCamera1
            // 
            this.richTextCamera1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextCamera1.Location = new System.Drawing.Point(1349, 28);
            this.richTextCamera1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextCamera1.Name = "richTextCamera1";
            this.richTextCamera1.Size = new System.Drawing.Size(331, 180);
            this.richTextCamera1.TabIndex = 191;
            this.richTextCamera1.Text = "";
            this.richTextCamera1.WordWrap = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.LightSalmon;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(1435, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 19);
            this.label10.TabIndex = 192;
            this.label10.Text = "相机1运行信息输出";
            // 
            // richTextRobot
            // 
            this.richTextRobot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextRobot.Location = new System.Drawing.Point(1349, 438);
            this.richTextRobot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextRobot.Name = "richTextRobot";
            this.richTextRobot.Size = new System.Drawing.Size(331, 207);
            this.richTextRobot.TabIndex = 168;
            this.richTextRobot.Text = "";
            this.richTextRobot.WordWrap = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.LightSalmon;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(1435, 415);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(118, 19);
            this.label25.TabIndex = 169;
            this.label25.Text = "机器人运行信息输出";
            // 
            // checkButton1
            // 
            this.checkButton1.Appearance.BackColor = System.Drawing.Color.Red;
            this.checkButton1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkButton1.Appearance.Options.UseBackColor = true;
            this.checkButton1.Appearance.Options.UseFont = true;
            this.checkButton1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearanceDisabled.Options.UseFont = true;
            this.checkButton1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearanceHovered.Options.UseFont = true;
            this.checkButton1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearancePressed.Options.UseFont = true;
            this.checkButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.checkButton1.ImageOptions.ImageIndex = 0;
            this.checkButton1.ImageOptions.ImageList = this.imageList16;
            this.checkButton1.Location = new System.Drawing.Point(816, 11);
            this.checkButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkButton1.Name = "checkButton1";
            this.checkButton1.Size = new System.Drawing.Size(133, 73);
            this.checkButton1.TabIndex = 189;
            this.checkButton1.Text = "急停关闭/打开";
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16.Images.SetKeyName(0, "emblem-nowrite.png");
            this.imageList16.Images.SetKeyName(1, "emblem-ok.png");
            this.imageList16.Images.SetKeyName(2, "stock_lock-ok.png");
            this.imageList16.Images.SetKeyName(3, "stock_lock-open.png");
            this.imageList16.Images.SetKeyName(4, "stock_music-library.png");
            this.imageList16.Images.SetKeyName(5, "stock_new-drawing.png");
            this.imageList16.Images.SetKeyName(6, "stock_new-formula.png");
            this.imageList16.Images.SetKeyName(7, "stock_new-html.png");
            this.imageList16.Images.SetKeyName(8, "stock_new-master-document.png");
            this.imageList16.Images.SetKeyName(9, "stock_new-presentation.png");
            // 
            // checkButton2
            // 
            this.checkButton2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.checkButton2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkButton2.Appearance.Options.UseBackColor = true;
            this.checkButton2.Appearance.Options.UseFont = true;
            this.checkButton2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton2.AppearanceDisabled.Options.UseFont = true;
            this.checkButton2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton2.AppearanceHovered.Options.UseFont = true;
            this.checkButton2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton2.AppearancePressed.Options.UseFont = true;
            this.checkButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.checkButton2.ImageOptions.ImageIndex = 1;
            this.checkButton2.ImageOptions.ImageList = this.imageList16;
            this.checkButton2.Location = new System.Drawing.Point(684, 11);
            this.checkButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkButton2.Name = "checkButton2";
            this.checkButton2.Size = new System.Drawing.Size(133, 73);
            this.checkButton2.TabIndex = 188;
            this.checkButton2.Text = "安全门关闭/打开";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearanceDisabled.Options.UseFont = true;
            this.labelControl7.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearanceHovered.Options.UseFont = true;
            this.labelControl7.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearancePressed.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(-491, -33);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(93, 17);
            this.labelControl7.TabIndex = 177;
            this.labelControl7.Text = "下料PLC连接状态";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceDisabled.Options.UseFont = true;
            this.labelControl8.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceHovered.Options.UseFont = true;
            this.labelControl8.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearancePressed.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(-491, -94);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(132, 17);
            this.labelControl8.TabIndex = 176;
            this.labelControl8.Text = "Modbus TCP/IP Server";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl6.AppearanceDisabled.Options.UseFont = true;
            this.labelControl6.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl6.AppearanceHovered.Options.UseFont = true;
            this.labelControl6.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl6.AppearancePressed.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(1085, 63);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(93, 17);
            this.labelControl6.TabIndex = 172;
            this.labelControl6.Text = "下料PLC连接状态";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceDisabled.Options.UseFont = true;
            this.labelControl5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceHovered.Options.UseFont = true;
            this.labelControl5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearancePressed.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(974, 63);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(84, 17);
            this.labelControl5.TabIndex = 171;
            this.labelControl5.Text = "机器人连接状态";
            // 
            // userLantern2
            // 
            this.userLantern2.BackColor = System.Drawing.Color.Transparent;
            this.userLantern2.LanternBackground = System.Drawing.Color.Gray;
            this.userLantern2.Location = new System.Drawing.Point(3218, 546);
            this.userLantern2.Margin = new System.Windows.Forms.Padding(33, 511, 33, 511);
            this.userLantern2.Name = "userLantern2";
            this.userLantern2.Size = new System.Drawing.Size(69, 91);
            this.userLantern2.TabIndex = 167;
            // 
            // userLantern1
            // 
            this.userLantern1.BackColor = System.Drawing.Color.Transparent;
            this.userLantern1.LanternBackground = System.Drawing.Color.Gray;
            this.userLantern1.Location = new System.Drawing.Point(3218, 401);
            this.userLantern1.Margin = new System.Windows.Forms.Padding(15, 219, 15, 219);
            this.userLantern1.Name = "userLantern1";
            this.userLantern1.Size = new System.Drawing.Size(69, 91);
            this.userLantern1.TabIndex = 166;
            // 
            // btmInvoke_StopMachine
            // 
            this.btmInvoke_StopMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_StopMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_StopMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_StopMachine.ImageOptions.Image")));
            this.btmInvoke_StopMachine.Location = new System.Drawing.Point(340, 11);
            this.btmInvoke_StopMachine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmInvoke_StopMachine.Name = "btmInvoke_StopMachine";
            this.btmInvoke_StopMachine.Size = new System.Drawing.Size(133, 73);
            this.btmInvoke_StopMachine.TabIndex = 104;
            this.btmInvoke_StopMachine.Text = "停止";
            this.btmInvoke_StopMachine.Click += new System.EventHandler(this.btmInvoke_StopMachine_Click);
            // 
            // btmInvoke_StartMachine
            // 
            this.btmInvoke_StartMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_StartMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_StartMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_StartMachine.ImageOptions.Image")));
            this.btmInvoke_StartMachine.Location = new System.Drawing.Point(208, 11);
            this.btmInvoke_StartMachine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmInvoke_StartMachine.Name = "btmInvoke_StartMachine";
            this.btmInvoke_StartMachine.Size = new System.Drawing.Size(133, 73);
            this.btmInvoke_StartMachine.TabIndex = 103;
            this.btmInvoke_StartMachine.Text = "启动";
            this.btmInvoke_StartMachine.Click += new System.EventHandler(this.btmInvoke_StartMachine_Click);
            // 
            // btmInvoke_ResetMachine
            // 
            this.btmInvoke_ResetMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmInvoke_ResetMachine.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ResetMachine.Appearance.Image")));
            this.btmInvoke_ResetMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_ResetMachine.Appearance.Options.UseImage = true;
            this.btmInvoke_ResetMachine.Appearance.Options.UseTextOptions = true;
            this.btmInvoke_ResetMachine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btmInvoke_ResetMachine.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btmInvoke_ResetMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_ResetMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_ResetMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_ResetMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_ResetMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ResetMachine.ImageOptions.Image")));
            this.btmInvoke_ResetMachine.Location = new System.Drawing.Point(76, 11);
            this.btmInvoke_ResetMachine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmInvoke_ResetMachine.Name = "btmInvoke_ResetMachine";
            this.btmInvoke_ResetMachine.Size = new System.Drawing.Size(133, 73);
            this.btmInvoke_ResetMachine.TabIndex = 102;
            this.btmInvoke_ResetMachine.Text = "复位";
            this.btmInvoke_ResetMachine.Click += new System.EventHandler(this.btmInvoke_ResetMachine_Click);
            // 
            // NP_设置
            // 
            this.NP_设置.Caption = " 系统设置";
            this.NP_设置.Controls.Add(this.xtraTabControl1);
            this.NP_设置.Controls.Add(this.groupControl13);
            this.NP_设置.Controls.Add(this.btmSaveRobotConfig);
            this.NP_设置.Controls.Add(this.groupControl12);
            this.NP_设置.Image = ((System.Drawing.Image)(resources.GetObject("NP_设置.Image")));
            this.NP_设置.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_设置.Name = "NP_设置";
            this.NP_设置.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.NP_设置.Size = new System.Drawing.Size(1684, 647);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.Appearance.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.PageClient.Options.UseFont = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(232, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1458, 645);
            this.xtraTabControl1.TabIndex = 178;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage5});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderDisabled.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabPage1.Appearance.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.PageClient.Options.UseFont = true;
            this.xtraTabPage1.Controls.Add(this.groupControl3);
            this.xtraTabPage1.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.Image")));
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1452, 598);
            this.xtraTabPage1.Text = "基本坐标设定";
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.btmRobotLearnFailed_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnFailed);
            this.groupControl3.Controls.Add(this.label7);
            this.groupControl3.Controls.Add(this.txtRobotFailed);
            this.groupControl3.Controls.Add(this.label9);
            this.groupControl3.Controls.Add(this.btmRobotLearnQA_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnQA);
            this.groupControl3.Controls.Add(this.txtRobotQA);
            this.groupControl3.Controls.Add(this.btmRobotLearnVF_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnVF);
            this.groupControl3.Controls.Add(this.btmRobotLearnUNF_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnUNF);
            this.groupControl3.Controls.Add(this.label8);
            this.groupControl3.Controls.Add(this.txtRobotTransit_VToQA);
            this.groupControl3.Controls.Add(this.label6);
            this.groupControl3.Controls.Add(this.txtRobotTransit_ULToF);
            this.groupControl3.Controls.Add(this.label5);
            this.groupControl3.Controls.Add(this.label4);
            this.groupControl3.Controls.Add(this.label3);
            this.groupControl3.Controls.Add(this.label2);
            this.groupControl3.Controls.Add(this.btmRobotLearnLF_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnLF);
            this.groupControl3.Controls.Add(this.txtRobotTransit_LToF);
            this.groupControl3.Controls.Add(this.btmRobotLearnCamera_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnCamera);
            this.groupControl3.Controls.Add(this.txtRobotCamera);
            this.groupControl3.Controls.Add(this.btmRobotLearnUnload_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnUnload);
            this.groupControl3.Controls.Add(this.txtRobotUnload);
            this.groupControl3.Controls.Add(this.btmRobotLearnCatch_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnCatch);
            this.groupControl3.Controls.Add(this.txtRobotCatch);
            this.groupControl3.Controls.Add(this.btmRobotLearnOrg_Go);
            this.groupControl3.Controls.Add(this.btmRobotLearnOrg);
            this.groupControl3.Controls.Add(this.txtRobotOrg);
            this.groupControl3.Controls.Add(this.label1);
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1451, 628);
            this.groupControl3.TabIndex = 96;
            this.groupControl3.Text = "机器人位置学习及定义";
            // 
            // btmRobotLearnFailed_Go
            // 
            this.btmRobotLearnFailed_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnFailed_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnFailed_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnFailed_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnFailed_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnFailed_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnFailed_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnFailed_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnFailed_Go.Location = new System.Drawing.Point(478, 358);
            this.btmRobotLearnFailed_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnFailed_Go.Name = "btmRobotLearnFailed_Go";
            this.btmRobotLearnFailed_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnFailed_Go.TabIndex = 168;
            this.btmRobotLearnFailed_Go.Text = "Go当前位置";
            this.btmRobotLearnFailed_Go.Click += new System.EventHandler(this.btmRobotLearnFailed_Go_Click);
            // 
            // btmRobotLearnFailed
            // 
            this.btmRobotLearnFailed.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnFailed.Appearance.Options.UseFont = true;
            this.btmRobotLearnFailed.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnFailed.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnFailed.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnFailed.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnFailed.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnFailed.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnFailed.Location = new System.Drawing.Point(321, 358);
            this.btmRobotLearnFailed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnFailed.Name = "btmRobotLearnFailed";
            this.btmRobotLearnFailed.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnFailed.TabIndex = 167;
            this.btmRobotLearnFailed.Text = "学习/得到当前位置";
            this.btmRobotLearnFailed.Click += new System.EventHandler(this.btmRobotLearnFailed_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label7.Location = new System.Drawing.Point(10, 352);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 166;
            this.label7.Text = "坏品位置";
            // 
            // txtRobotFailed
            // 
            this.txtRobotFailed.Location = new System.Drawing.Point(8, 373);
            this.txtRobotFailed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotFailed.Name = "txtRobotFailed";
            this.txtRobotFailed.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotFailed.Properties.Appearance.Options.UseFont = true;
            this.txtRobotFailed.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotFailed.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotFailed.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotFailed.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotFailed.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotFailed.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotFailed.Size = new System.Drawing.Size(307, 24);
            this.txtRobotFailed.TabIndex = 165;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label9.Location = new System.Drawing.Point(10, 287);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 164;
            this.label9.Text = "抽检位置";
            // 
            // btmRobotLearnQA_Go
            // 
            this.btmRobotLearnQA_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnQA_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnQA_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnQA_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnQA_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnQA_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnQA_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnQA_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnQA_Go.Location = new System.Drawing.Point(478, 293);
            this.btmRobotLearnQA_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnQA_Go.Name = "btmRobotLearnQA_Go";
            this.btmRobotLearnQA_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnQA_Go.TabIndex = 163;
            this.btmRobotLearnQA_Go.Text = "Go当前位置";
            this.btmRobotLearnQA_Go.Click += new System.EventHandler(this.btmRobotLearnQA_Go_Click);
            // 
            // btmRobotLearnQA
            // 
            this.btmRobotLearnQA.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnQA.Appearance.Options.UseFont = true;
            this.btmRobotLearnQA.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnQA.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnQA.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnQA.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnQA.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnQA.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnQA.Location = new System.Drawing.Point(321, 293);
            this.btmRobotLearnQA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnQA.Name = "btmRobotLearnQA";
            this.btmRobotLearnQA.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnQA.TabIndex = 162;
            this.btmRobotLearnQA.Text = "学习/得到当前位置";
            this.btmRobotLearnQA.Click += new System.EventHandler(this.btmRobotLearnQA_Click);
            // 
            // txtRobotQA
            // 
            this.txtRobotQA.Location = new System.Drawing.Point(8, 308);
            this.txtRobotQA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotQA.Name = "txtRobotQA";
            this.txtRobotQA.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotQA.Properties.Appearance.Options.UseFont = true;
            this.txtRobotQA.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotQA.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotQA.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotQA.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotQA.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotQA.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotQA.Size = new System.Drawing.Size(307, 24);
            this.txtRobotQA.TabIndex = 161;
            // 
            // btmRobotLearnVF_Go
            // 
            this.btmRobotLearnVF_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnVF_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnVF_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnVF_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnVF_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnVF_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnVF_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnVF_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnVF_Go.Location = new System.Drawing.Point(1118, 163);
            this.btmRobotLearnVF_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnVF_Go.Name = "btmRobotLearnVF_Go";
            this.btmRobotLearnVF_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnVF_Go.TabIndex = 160;
            this.btmRobotLearnVF_Go.Text = "Go当前位置";
            this.btmRobotLearnVF_Go.Click += new System.EventHandler(this.btmRobotLearnVF_Go_Click);
            // 
            // btmRobotLearnVF
            // 
            this.btmRobotLearnVF.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnVF.Appearance.Options.UseFont = true;
            this.btmRobotLearnVF.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnVF.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnVF.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnVF.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnVF.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnVF.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnVF.Location = new System.Drawing.Point(961, 163);
            this.btmRobotLearnVF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnVF.Name = "btmRobotLearnVF";
            this.btmRobotLearnVF.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnVF.TabIndex = 159;
            this.btmRobotLearnVF.Text = "学习/得到当前位置";
            this.btmRobotLearnVF.Click += new System.EventHandler(this.btmRobotLearnVF_Click);
            // 
            // btmRobotLearnUNF_Go
            // 
            this.btmRobotLearnUNF_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnUNF_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnUNF_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUNF_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnUNF_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUNF_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnUNF_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUNF_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnUNF_Go.Location = new System.Drawing.Point(1118, 98);
            this.btmRobotLearnUNF_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnUNF_Go.Name = "btmRobotLearnUNF_Go";
            this.btmRobotLearnUNF_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnUNF_Go.TabIndex = 158;
            this.btmRobotLearnUNF_Go.Text = "Go当前位置";
            this.btmRobotLearnUNF_Go.Click += new System.EventHandler(this.btmRobotLearnUNF_Go_Click);
            // 
            // btmRobotLearnUNF
            // 
            this.btmRobotLearnUNF.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnUNF.Appearance.Options.UseFont = true;
            this.btmRobotLearnUNF.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUNF.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnUNF.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUNF.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnUNF.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUNF.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnUNF.Location = new System.Drawing.Point(961, 98);
            this.btmRobotLearnUNF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnUNF.Name = "btmRobotLearnUNF";
            this.btmRobotLearnUNF.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnUNF.TabIndex = 157;
            this.btmRobotLearnUNF.Text = "学习/得到当前位置";
            this.btmRobotLearnUNF.Click += new System.EventHandler(this.btmRobotLearnUNF_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label8.Location = new System.Drawing.Point(650, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 17);
            this.label8.TabIndex = 156;
            this.label8.Text = "过渡位 (拍照<->抽检)";
            // 
            // txtRobotTransit_VToQA
            // 
            this.txtRobotTransit_VToQA.Location = new System.Drawing.Point(648, 178);
            this.txtRobotTransit_VToQA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotTransit_VToQA.Name = "txtRobotTransit_VToQA";
            this.txtRobotTransit_VToQA.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotTransit_VToQA.Properties.Appearance.Options.UseFont = true;
            this.txtRobotTransit_VToQA.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_VToQA.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotTransit_VToQA.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_VToQA.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotTransit_VToQA.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_VToQA.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotTransit_VToQA.Size = new System.Drawing.Size(307, 24);
            this.txtRobotTransit_VToQA.TabIndex = 155;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label6.Location = new System.Drawing.Point(650, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 17);
            this.label6.TabIndex = 154;
            this.label6.Text = "过渡位 (放料<->坏品)";
            // 
            // txtRobotTransit_ULToF
            // 
            this.txtRobotTransit_ULToF.Location = new System.Drawing.Point(648, 113);
            this.txtRobotTransit_ULToF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotTransit_ULToF.Name = "txtRobotTransit_ULToF";
            this.txtRobotTransit_ULToF.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotTransit_ULToF.Properties.Appearance.Options.UseFont = true;
            this.txtRobotTransit_ULToF.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_ULToF.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotTransit_ULToF.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_ULToF.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotTransit_ULToF.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_ULToF.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotTransit_ULToF.Size = new System.Drawing.Size(307, 24);
            this.txtRobotTransit_ULToF.TabIndex = 153;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label5.Location = new System.Drawing.Point(650, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 152;
            this.label5.Text = "过渡位 (取料)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label4.Location = new System.Drawing.Point(10, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 17);
            this.label4.TabIndex = 149;
            this.label4.Text = "第1行抓料位置";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label3.Location = new System.Drawing.Point(10, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 17);
            this.label3.TabIndex = 148;
            this.label3.Text = "第1行放料位置";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(10, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 147;
            this.label2.Text = "视觉拍照位置";
            // 
            // btmRobotLearnLF_Go
            // 
            this.btmRobotLearnLF_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnLF_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnLF_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLF_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnLF_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLF_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnLF_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLF_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnLF_Go.Location = new System.Drawing.Point(1118, 33);
            this.btmRobotLearnLF_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnLF_Go.Name = "btmRobotLearnLF_Go";
            this.btmRobotLearnLF_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnLF_Go.TabIndex = 146;
            this.btmRobotLearnLF_Go.Text = "Go当前位置";
            this.btmRobotLearnLF_Go.Click += new System.EventHandler(this.btmRobotLearnLF_Go_Click);
            // 
            // btmRobotLearnLF
            // 
            this.btmRobotLearnLF.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnLF.Appearance.Options.UseFont = true;
            this.btmRobotLearnLF.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLF.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnLF.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLF.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnLF.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLF.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnLF.Location = new System.Drawing.Point(961, 33);
            this.btmRobotLearnLF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnLF.Name = "btmRobotLearnLF";
            this.btmRobotLearnLF.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnLF.TabIndex = 145;
            this.btmRobotLearnLF.Text = "学习/得到当前位置";
            this.btmRobotLearnLF.Click += new System.EventHandler(this.btmRobotLearnLF_Click);
            // 
            // txtRobotTransit_LToF
            // 
            this.txtRobotTransit_LToF.Location = new System.Drawing.Point(648, 48);
            this.txtRobotTransit_LToF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotTransit_LToF.Name = "txtRobotTransit_LToF";
            this.txtRobotTransit_LToF.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotTransit_LToF.Properties.Appearance.Options.UseFont = true;
            this.txtRobotTransit_LToF.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_LToF.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotTransit_LToF.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_LToF.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotTransit_LToF.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotTransit_LToF.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotTransit_LToF.Size = new System.Drawing.Size(307, 24);
            this.txtRobotTransit_LToF.TabIndex = 144;
            // 
            // btmRobotLearnCamera_Go
            // 
            this.btmRobotLearnCamera_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnCamera_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnCamera_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCamera_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnCamera_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCamera_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnCamera_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCamera_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnCamera_Go.Location = new System.Drawing.Point(478, 228);
            this.btmRobotLearnCamera_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnCamera_Go.Name = "btmRobotLearnCamera_Go";
            this.btmRobotLearnCamera_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnCamera_Go.TabIndex = 143;
            this.btmRobotLearnCamera_Go.Text = "Go当前位置";
            this.btmRobotLearnCamera_Go.Click += new System.EventHandler(this.btmRobotLearnCamera_Go_Click);
            // 
            // btmRobotLearnCamera
            // 
            this.btmRobotLearnCamera.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnCamera.Appearance.Options.UseFont = true;
            this.btmRobotLearnCamera.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCamera.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnCamera.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCamera.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnCamera.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCamera.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnCamera.Location = new System.Drawing.Point(321, 228);
            this.btmRobotLearnCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnCamera.Name = "btmRobotLearnCamera";
            this.btmRobotLearnCamera.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnCamera.TabIndex = 142;
            this.btmRobotLearnCamera.Text = "学习/得到当前位置";
            this.btmRobotLearnCamera.Click += new System.EventHandler(this.btmRobotLearnCamera_Click);
            // 
            // txtRobotCamera
            // 
            this.txtRobotCamera.Location = new System.Drawing.Point(8, 243);
            this.txtRobotCamera.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotCamera.Name = "txtRobotCamera";
            this.txtRobotCamera.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotCamera.Properties.Appearance.Options.UseFont = true;
            this.txtRobotCamera.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotCamera.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotCamera.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotCamera.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotCamera.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotCamera.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotCamera.Size = new System.Drawing.Size(307, 24);
            this.txtRobotCamera.TabIndex = 141;
            // 
            // btmRobotLearnUnload_Go
            // 
            this.btmRobotLearnUnload_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnUnload_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnUnload_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUnload_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnUnload_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUnload_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnUnload_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUnload_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnUnload_Go.Location = new System.Drawing.Point(478, 163);
            this.btmRobotLearnUnload_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnUnload_Go.Name = "btmRobotLearnUnload_Go";
            this.btmRobotLearnUnload_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnUnload_Go.TabIndex = 140;
            this.btmRobotLearnUnload_Go.Text = "Go当前位置";
            this.btmRobotLearnUnload_Go.Click += new System.EventHandler(this.btmRobotLearnUnload_Go_Click);
            // 
            // btmRobotLearnUnload
            // 
            this.btmRobotLearnUnload.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnUnload.Appearance.Options.UseFont = true;
            this.btmRobotLearnUnload.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUnload.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnUnload.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUnload.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnUnload.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnUnload.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnUnload.Location = new System.Drawing.Point(321, 163);
            this.btmRobotLearnUnload.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnUnload.Name = "btmRobotLearnUnload";
            this.btmRobotLearnUnload.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnUnload.TabIndex = 139;
            this.btmRobotLearnUnload.Text = "学习/得到当前位置";
            this.btmRobotLearnUnload.Click += new System.EventHandler(this.btmRobotLearnUnload_Click);
            // 
            // txtRobotUnload
            // 
            this.txtRobotUnload.Location = new System.Drawing.Point(8, 178);
            this.txtRobotUnload.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotUnload.Name = "txtRobotUnload";
            this.txtRobotUnload.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotUnload.Properties.Appearance.Options.UseFont = true;
            this.txtRobotUnload.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotUnload.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotUnload.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotUnload.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotUnload.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotUnload.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotUnload.Size = new System.Drawing.Size(307, 24);
            this.txtRobotUnload.TabIndex = 138;
            // 
            // btmRobotLearnCatch_Go
            // 
            this.btmRobotLearnCatch_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnCatch_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnCatch_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCatch_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnCatch_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCatch_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnCatch_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCatch_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnCatch_Go.Location = new System.Drawing.Point(478, 98);
            this.btmRobotLearnCatch_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnCatch_Go.Name = "btmRobotLearnCatch_Go";
            this.btmRobotLearnCatch_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnCatch_Go.TabIndex = 137;
            this.btmRobotLearnCatch_Go.Text = "Go当前位置";
            this.btmRobotLearnCatch_Go.Click += new System.EventHandler(this.btmRobotLearnCatch_Go_Click);
            // 
            // btmRobotLearnCatch
            // 
            this.btmRobotLearnCatch.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnCatch.Appearance.Options.UseFont = true;
            this.btmRobotLearnCatch.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCatch.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnCatch.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCatch.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnCatch.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnCatch.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnCatch.Location = new System.Drawing.Point(321, 98);
            this.btmRobotLearnCatch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnCatch.Name = "btmRobotLearnCatch";
            this.btmRobotLearnCatch.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnCatch.TabIndex = 136;
            this.btmRobotLearnCatch.Text = "学习/得到当前位置";
            this.btmRobotLearnCatch.Click += new System.EventHandler(this.btmRobotLearnCatch_Click);
            // 
            // txtRobotCatch
            // 
            this.txtRobotCatch.Location = new System.Drawing.Point(8, 113);
            this.txtRobotCatch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotCatch.Name = "txtRobotCatch";
            this.txtRobotCatch.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotCatch.Properties.Appearance.Options.UseFont = true;
            this.txtRobotCatch.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotCatch.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotCatch.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotCatch.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotCatch.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotCatch.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotCatch.Size = new System.Drawing.Size(307, 24);
            this.txtRobotCatch.TabIndex = 135;
            // 
            // btmRobotLearnOrg_Go
            // 
            this.btmRobotLearnOrg_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnOrg_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnOrg_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnOrg_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnOrg_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnOrg_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnOrg_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnOrg_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnOrg_Go.Location = new System.Drawing.Point(478, 33);
            this.btmRobotLearnOrg_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnOrg_Go.Name = "btmRobotLearnOrg_Go";
            this.btmRobotLearnOrg_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnOrg_Go.TabIndex = 134;
            this.btmRobotLearnOrg_Go.Text = "Go当前位置";
            this.btmRobotLearnOrg_Go.Click += new System.EventHandler(this.btmRobotLearnOrg_Go_Click);
            // 
            // btmRobotLearnOrg
            // 
            this.btmRobotLearnOrg.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnOrg.Appearance.Options.UseFont = true;
            this.btmRobotLearnOrg.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnOrg.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnOrg.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnOrg.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnOrg.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnOrg.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnOrg.Location = new System.Drawing.Point(321, 33);
            this.btmRobotLearnOrg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnOrg.Name = "btmRobotLearnOrg";
            this.btmRobotLearnOrg.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnOrg.TabIndex = 133;
            this.btmRobotLearnOrg.Text = "学习/得到当前位置";
            this.btmRobotLearnOrg.Click += new System.EventHandler(this.btmRobotLearnOrg_Click);
            // 
            // txtRobotOrg
            // 
            this.txtRobotOrg.Location = new System.Drawing.Point(8, 48);
            this.txtRobotOrg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotOrg.Name = "txtRobotOrg";
            this.txtRobotOrg.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotOrg.Properties.Appearance.Options.UseFont = true;
            this.txtRobotOrg.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotOrg.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotOrg.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotOrg.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotOrg.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotOrg.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotOrg.Size = new System.Drawing.Size(307, 24);
            this.txtRobotOrg.TabIndex = 132;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(10, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 131;
            this.label1.Text = "机器人原点位置";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.xtraTabPage2.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage2.Controls.Add(this.groupControl15);
            this.xtraTabPage2.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage2.Image")));
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1452, 598);
            this.xtraTabPage2.Text = "补料坐标设定";
            // 
            // groupControl15
            // 
            this.groupControl15.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl15.Appearance.Options.UseFont = true;
            this.groupControl15.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl15.AppearanceCaption.Options.UseFont = true;
            this.groupControl15.Controls.Add(this.label73);
            this.groupControl15.Controls.Add(this.btmRobotLearnLoadFirst_Go2);
            this.groupControl15.Controls.Add(this.btmRobotLearnLoadFirst2);
            this.groupControl15.Controls.Add(this.txtRobotPos_LoadFirst2);
            this.groupControl15.Controls.Add(this.btmRobotLearnBuffFirst_Go2);
            this.groupControl15.Controls.Add(this.btmRobotLearnBuffFirst2);
            this.groupControl15.Controls.Add(this.txtRobotPos_BuffFirst2);
            this.groupControl15.Controls.Add(this.label72);
            this.groupControl15.Controls.Add(this.groupControl11);
            this.groupControl15.Controls.Add(this.label55);
            this.groupControl15.Controls.Add(this.btmRobotLearnLoadFirst_Go);
            this.groupControl15.Controls.Add(this.btmRobotLearnLoadFirst);
            this.groupControl15.Controls.Add(this.txtRobotPos_LoadFirst);
            this.groupControl15.Controls.Add(this.btmRobotLearnBuffFirst_Go);
            this.groupControl15.Controls.Add(this.btmRobotLearnBuffFirst);
            this.groupControl15.Controls.Add(this.txtRobotPos_BuffFirst);
            this.groupControl15.Controls.Add(this.label58);
            this.groupControl15.Location = new System.Drawing.Point(0, 0);
            this.groupControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl15.Name = "groupControl15";
            this.groupControl15.Size = new System.Drawing.Size(1452, 628);
            this.groupControl15.TabIndex = 97;
            this.groupControl15.Text = "机器人位置学习及定义";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label73.Location = new System.Drawing.Point(11, 218);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(334, 17);
            this.label73.TabIndex = 192;
            this.label73.Text = "补料时 下料盘放料的第一个位置(机器人1号吸嘴对第1个产品)";
            // 
            // btmRobotLearnLoadFirst_Go2
            // 
            this.btmRobotLearnLoadFirst_Go2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnLoadFirst_Go2.Appearance.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst_Go2.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst_Go2.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst_Go2.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go2.Location = new System.Drawing.Point(520, 225);
            this.btmRobotLearnLoadFirst_Go2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnLoadFirst_Go2.Name = "btmRobotLearnLoadFirst_Go2";
            this.btmRobotLearnLoadFirst_Go2.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnLoadFirst_Go2.TabIndex = 191;
            this.btmRobotLearnLoadFirst_Go2.Text = "Go当前位置";
            this.btmRobotLearnLoadFirst_Go2.Click += new System.EventHandler(this.btmRobotLearnLoadFirst_Go2_Click);
            // 
            // btmRobotLearnLoadFirst2
            // 
            this.btmRobotLearnLoadFirst2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnLoadFirst2.Appearance.Options.UseFont = true;
            this.btmRobotLearnLoadFirst2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst2.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnLoadFirst2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst2.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnLoadFirst2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst2.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnLoadFirst2.Location = new System.Drawing.Point(363, 225);
            this.btmRobotLearnLoadFirst2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnLoadFirst2.Name = "btmRobotLearnLoadFirst2";
            this.btmRobotLearnLoadFirst2.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnLoadFirst2.TabIndex = 190;
            this.btmRobotLearnLoadFirst2.Text = "学习/得到当前位置";
            this.btmRobotLearnLoadFirst2.Click += new System.EventHandler(this.btmRobotLearnLoadFirst2_Click);
            // 
            // txtRobotPos_LoadFirst2
            // 
            this.txtRobotPos_LoadFirst2.Location = new System.Drawing.Point(9, 240);
            this.txtRobotPos_LoadFirst2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotPos_LoadFirst2.Name = "txtRobotPos_LoadFirst2";
            this.txtRobotPos_LoadFirst2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotPos_LoadFirst2.Properties.Appearance.Options.UseFont = true;
            this.txtRobotPos_LoadFirst2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_LoadFirst2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotPos_LoadFirst2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_LoadFirst2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotPos_LoadFirst2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_LoadFirst2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotPos_LoadFirst2.Size = new System.Drawing.Size(307, 24);
            this.txtRobotPos_LoadFirst2.TabIndex = 189;
            // 
            // btmRobotLearnBuffFirst_Go2
            // 
            this.btmRobotLearnBuffFirst_Go2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnBuffFirst_Go2.Appearance.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst_Go2.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst_Go2.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst_Go2.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go2.Location = new System.Drawing.Point(519, 159);
            this.btmRobotLearnBuffFirst_Go2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnBuffFirst_Go2.Name = "btmRobotLearnBuffFirst_Go2";
            this.btmRobotLearnBuffFirst_Go2.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnBuffFirst_Go2.TabIndex = 188;
            this.btmRobotLearnBuffFirst_Go2.Text = "Go当前位置";
            this.btmRobotLearnBuffFirst_Go2.Click += new System.EventHandler(this.btmRobotLearnBuffFirst_Go2_Click);
            // 
            // btmRobotLearnBuffFirst2
            // 
            this.btmRobotLearnBuffFirst2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnBuffFirst2.Appearance.Options.UseFont = true;
            this.btmRobotLearnBuffFirst2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst2.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnBuffFirst2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst2.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnBuffFirst2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst2.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnBuffFirst2.Location = new System.Drawing.Point(362, 159);
            this.btmRobotLearnBuffFirst2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnBuffFirst2.Name = "btmRobotLearnBuffFirst2";
            this.btmRobotLearnBuffFirst2.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnBuffFirst2.TabIndex = 187;
            this.btmRobotLearnBuffFirst2.Text = "学习/得到当前位置";
            this.btmRobotLearnBuffFirst2.Click += new System.EventHandler(this.btmRobotLearnBuffFirst2_Click);
            // 
            // txtRobotPos_BuffFirst2
            // 
            this.txtRobotPos_BuffFirst2.Location = new System.Drawing.Point(8, 174);
            this.txtRobotPos_BuffFirst2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotPos_BuffFirst2.Name = "txtRobotPos_BuffFirst2";
            this.txtRobotPos_BuffFirst2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotPos_BuffFirst2.Properties.Appearance.Options.UseFont = true;
            this.txtRobotPos_BuffFirst2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_BuffFirst2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotPos_BuffFirst2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_BuffFirst2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotPos_BuffFirst2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_BuffFirst2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotPos_BuffFirst2.Size = new System.Drawing.Size(307, 24);
            this.txtRobotPos_BuffFirst2.TabIndex = 186;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label72.Location = new System.Drawing.Point(10, 152);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(334, 17);
            this.label72.TabIndex = 185;
            this.label72.Text = "补料时 缓存盘取料的第一个位置(机器人1号吸嘴对第1个产品)";
            // 
            // groupControl11
            // 
            this.groupControl11.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl11.Appearance.Options.UseFont = true;
            this.groupControl11.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl11.AppearanceCaption.Options.UseFont = true;
            this.groupControl11.Controls.Add(this.labelControl61);
            this.groupControl11.Controls.Add(this.labelControl54);
            this.groupControl11.Controls.Add(this.labelControl55);
            this.groupControl11.Controls.Add(this.labelControl56);
            this.groupControl11.Controls.Add(this.labelControl57);
            this.groupControl11.Controls.Add(this.labelControl53);
            this.groupControl11.Controls.Add(this.labelControl52);
            this.groupControl11.Controls.Add(this.labelControl51);
            this.groupControl11.Controls.Add(this.spn_Cust_Row9);
            this.groupControl11.Controls.Add(this.spn_Cust_Row8);
            this.groupControl11.Controls.Add(this.spn_Cust_Row7);
            this.groupControl11.Controls.Add(this.spn_Cust_Row6);
            this.groupControl11.Controls.Add(this.spn_Cust_Row5);
            this.groupControl11.Controls.Add(this.spn_Cust_Row4);
            this.groupControl11.Controls.Add(this.spn_Cust_Row3);
            this.groupControl11.Controls.Add(this.spn_Cust_Row2);
            this.groupControl11.Controls.Add(this.chkCustomizeRowDst);
            this.groupControl11.Controls.Add(this.labelControl50);
            this.groupControl11.Controls.Add(this.spn_Cust_Row1);
            this.groupControl11.Controls.Add(this.labelControl35);
            this.groupControl11.Controls.Add(this.speColOfSingleDst);
            this.groupControl11.Controls.Add(this.labelControl36);
            this.groupControl11.Controls.Add(this.speRowOfSingleDst);
            this.groupControl11.Controls.Add(this.labelControl37);
            this.groupControl11.Controls.Add(this.speColOfSingle);
            this.groupControl11.Controls.Add(this.labelControl38);
            this.groupControl11.Controls.Add(this.speRowOfSingle);
            this.groupControl11.Controls.Add(this.simpleButton8);
            this.groupControl11.Controls.Add(this.simpleButton9);
            this.groupControl11.Controls.Add(this.simpleButton10);
            this.groupControl11.Controls.Add(this.simpleButton24);
            this.groupControl11.Location = new System.Drawing.Point(8, 284);
            this.groupControl11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(643, 310);
            this.groupControl11.TabIndex = 184;
            this.groupControl11.Text = "补料设定(按单个产品单个吸头确定)";
            // 
            // labelControl61
            // 
            this.labelControl61.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl61.Appearance.Options.UseFont = true;
            this.labelControl61.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl61.AppearanceDisabled.Options.UseFont = true;
            this.labelControl61.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl61.AppearanceHovered.Options.UseFont = true;
            this.labelControl61.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl61.AppearancePressed.Options.UseFont = true;
            this.labelControl61.Location = new System.Drawing.Point(324, 274);
            this.labelControl61.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(140, 17);
            this.labelControl61.TabIndex = 197;
            this.labelControl61.Text = "自定义行间距(9~10)(mm)";
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl54.AppearanceDisabled.Options.UseFont = true;
            this.labelControl54.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl54.AppearanceHovered.Options.UseFont = true;
            this.labelControl54.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl54.AppearancePressed.Options.UseFont = true;
            this.labelControl54.Location = new System.Drawing.Point(331, 243);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(133, 17);
            this.labelControl54.TabIndex = 196;
            this.labelControl54.Text = "自定义行间距(8~9)(mm)";
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl55.AppearanceDisabled.Options.UseFont = true;
            this.labelControl55.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl55.AppearanceHovered.Options.UseFont = true;
            this.labelControl55.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl55.AppearancePressed.Options.UseFont = true;
            this.labelControl55.Location = new System.Drawing.Point(331, 212);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(133, 17);
            this.labelControl55.TabIndex = 195;
            this.labelControl55.Text = "自定义行间距(7~8)(mm)";
            // 
            // labelControl56
            // 
            this.labelControl56.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl56.Appearance.Options.UseFont = true;
            this.labelControl56.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl56.AppearanceDisabled.Options.UseFont = true;
            this.labelControl56.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl56.AppearanceHovered.Options.UseFont = true;
            this.labelControl56.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl56.AppearancePressed.Options.UseFont = true;
            this.labelControl56.Location = new System.Drawing.Point(331, 181);
            this.labelControl56.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(133, 17);
            this.labelControl56.TabIndex = 194;
            this.labelControl56.Text = "自定义行间距(6~7)(mm)";
            // 
            // labelControl57
            // 
            this.labelControl57.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl57.Appearance.Options.UseFont = true;
            this.labelControl57.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl57.AppearanceDisabled.Options.UseFont = true;
            this.labelControl57.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl57.AppearanceHovered.Options.UseFont = true;
            this.labelControl57.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl57.AppearancePressed.Options.UseFont = true;
            this.labelControl57.Location = new System.Drawing.Point(331, 150);
            this.labelControl57.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(133, 17);
            this.labelControl57.TabIndex = 193;
            this.labelControl57.Text = "自定义行间距(5~6)(mm)";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl53.AppearanceDisabled.Options.UseFont = true;
            this.labelControl53.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl53.AppearanceHovered.Options.UseFont = true;
            this.labelControl53.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl53.AppearancePressed.Options.UseFont = true;
            this.labelControl53.Location = new System.Drawing.Point(331, 119);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(133, 17);
            this.labelControl53.TabIndex = 192;
            this.labelControl53.Text = "自定义行间距(4~5)(mm)";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl52.AppearanceDisabled.Options.UseFont = true;
            this.labelControl52.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl52.AppearanceHovered.Options.UseFont = true;
            this.labelControl52.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl52.AppearancePressed.Options.UseFont = true;
            this.labelControl52.Location = new System.Drawing.Point(331, 88);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(133, 17);
            this.labelControl52.TabIndex = 191;
            this.labelControl52.Text = "自定义行间距(3~4)(mm)";
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl51.AppearanceDisabled.Options.UseFont = true;
            this.labelControl51.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl51.AppearanceHovered.Options.UseFont = true;
            this.labelControl51.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl51.AppearancePressed.Options.UseFont = true;
            this.labelControl51.Location = new System.Drawing.Point(331, 57);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(133, 17);
            this.labelControl51.TabIndex = 190;
            this.labelControl51.Text = "自定义行间距(2~3)(mm)";
            // 
            // spn_Cust_Row9
            // 
            this.spn_Cust_Row9.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row9.Location = new System.Drawing.Point(470, 271);
            this.spn_Cust_Row9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row9.Name = "spn_Cust_Row9";
            this.spn_Cust_Row9.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row9.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row9.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row9.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row9.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row9.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row9.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row9.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row9.TabIndex = 188;
            // 
            // spn_Cust_Row8
            // 
            this.spn_Cust_Row8.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row8.Location = new System.Drawing.Point(470, 240);
            this.spn_Cust_Row8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row8.Name = "spn_Cust_Row8";
            this.spn_Cust_Row8.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row8.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row8.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row8.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row8.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row8.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row8.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row8.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row8.TabIndex = 187;
            // 
            // spn_Cust_Row7
            // 
            this.spn_Cust_Row7.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row7.Location = new System.Drawing.Point(470, 209);
            this.spn_Cust_Row7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row7.Name = "spn_Cust_Row7";
            this.spn_Cust_Row7.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row7.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row7.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row7.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row7.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row7.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row7.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row7.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row7.TabIndex = 186;
            // 
            // spn_Cust_Row6
            // 
            this.spn_Cust_Row6.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row6.Location = new System.Drawing.Point(470, 178);
            this.spn_Cust_Row6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row6.Name = "spn_Cust_Row6";
            this.spn_Cust_Row6.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row6.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row6.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row6.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row6.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row6.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row6.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row6.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row6.TabIndex = 185;
            // 
            // spn_Cust_Row5
            // 
            this.spn_Cust_Row5.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row5.Location = new System.Drawing.Point(470, 147);
            this.spn_Cust_Row5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row5.Name = "spn_Cust_Row5";
            this.spn_Cust_Row5.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row5.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row5.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row5.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row5.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row5.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row5.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row5.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row5.TabIndex = 184;
            // 
            // spn_Cust_Row4
            // 
            this.spn_Cust_Row4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row4.Location = new System.Drawing.Point(470, 116);
            this.spn_Cust_Row4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row4.Name = "spn_Cust_Row4";
            this.spn_Cust_Row4.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row4.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row4.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row4.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row4.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row4.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row4.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row4.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row4.TabIndex = 183;
            // 
            // spn_Cust_Row3
            // 
            this.spn_Cust_Row3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row3.Location = new System.Drawing.Point(470, 85);
            this.spn_Cust_Row3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row3.Name = "spn_Cust_Row3";
            this.spn_Cust_Row3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row3.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row3.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row3.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row3.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row3.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row3.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row3.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row3.TabIndex = 182;
            // 
            // spn_Cust_Row2
            // 
            this.spn_Cust_Row2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row2.Location = new System.Drawing.Point(470, 54);
            this.spn_Cust_Row2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row2.Name = "spn_Cust_Row2";
            this.spn_Cust_Row2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row2.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row2.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row2.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row2.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row2.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row2.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row2.TabIndex = 180;
            // 
            // chkCustomizeRowDst
            // 
            this.chkCustomizeRowDst.Location = new System.Drawing.Point(17, 115);
            this.chkCustomizeRowDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCustomizeRowDst.Name = "chkCustomizeRowDst";
            this.chkCustomizeRowDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCustomizeRowDst.Properties.Appearance.Options.UseFont = true;
            this.chkCustomizeRowDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkCustomizeRowDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkCustomizeRowDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkCustomizeRowDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkCustomizeRowDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkCustomizeRowDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkCustomizeRowDst.Properties.Caption = "启用自定义行间距补偿";
            this.chkCustomizeRowDst.Size = new System.Drawing.Size(148, 21);
            this.chkCustomizeRowDst.TabIndex = 179;
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl50.AppearanceDisabled.Options.UseFont = true;
            this.labelControl50.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl50.AppearanceHovered.Options.UseFont = true;
            this.labelControl50.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl50.AppearancePressed.Options.UseFont = true;
            this.labelControl50.Location = new System.Drawing.Point(331, 26);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(133, 17);
            this.labelControl50.TabIndex = 16;
            this.labelControl50.Text = "自定义行间距(1~2)(mm)";
            // 
            // spn_Cust_Row1
            // 
            this.spn_Cust_Row1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Cust_Row1.Location = new System.Drawing.Point(470, 23);
            this.spn_Cust_Row1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spn_Cust_Row1.Name = "spn_Cust_Row1";
            this.spn_Cust_Row1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row1.Properties.Appearance.Options.UseFont = true;
            this.spn_Cust_Row1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spn_Cust_Row1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row1.Properties.AppearanceFocused.Options.UseFont = true;
            this.spn_Cust_Row1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spn_Cust_Row1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spn_Cust_Row1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Cust_Row1.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.spn_Cust_Row1.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spn_Cust_Row1.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.spn_Cust_Row1.Size = new System.Drawing.Size(58, 24);
            this.spn_Cust_Row1.TabIndex = 15;
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl35.AppearanceDisabled.Options.UseFont = true;
            this.labelControl35.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl35.AppearanceHovered.Options.UseFont = true;
            this.labelControl35.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl35.AppearancePressed.Options.UseFont = true;
            this.labelControl35.Location = new System.Drawing.Point(157, 69);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(54, 17);
            this.labelControl35.TabIndex = 14;
            this.labelControl35.Text = "间距(mm)";
            // 
            // speColOfSingleDst
            // 
            this.speColOfSingleDst.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speColOfSingleDst.Location = new System.Drawing.Point(214, 67);
            this.speColOfSingleDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speColOfSingleDst.Name = "speColOfSingleDst";
            this.speColOfSingleDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingleDst.Properties.Appearance.Options.UseFont = true;
            this.speColOfSingleDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingleDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speColOfSingleDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingleDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.speColOfSingleDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingleDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speColOfSingleDst.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speColOfSingleDst.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.speColOfSingleDst.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.speColOfSingleDst.Size = new System.Drawing.Size(58, 24);
            this.speColOfSingleDst.TabIndex = 13;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl36.AppearanceDisabled.Options.UseFont = true;
            this.labelControl36.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl36.AppearanceHovered.Options.UseFont = true;
            this.labelControl36.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl36.AppearancePressed.Options.UseFont = true;
            this.labelControl36.Location = new System.Drawing.Point(157, 38);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(54, 17);
            this.labelControl36.TabIndex = 12;
            this.labelControl36.Text = "间距(mm)";
            // 
            // speRowOfSingleDst
            // 
            this.speRowOfSingleDst.EditValue = new decimal(new int[] {
            500,
            0,
            0,
            131072});
            this.speRowOfSingleDst.Location = new System.Drawing.Point(214, 36);
            this.speRowOfSingleDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speRowOfSingleDst.Name = "speRowOfSingleDst";
            this.speRowOfSingleDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowOfSingleDst.Properties.Appearance.Options.UseFont = true;
            this.speRowOfSingleDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowOfSingleDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speRowOfSingleDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowOfSingleDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.speRowOfSingleDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowOfSingleDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speRowOfSingleDst.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speRowOfSingleDst.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.speRowOfSingleDst.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.speRowOfSingleDst.Size = new System.Drawing.Size(58, 24);
            this.speRowOfSingleDst.TabIndex = 11;
            this.speRowOfSingleDst.EditValueChanged += new System.EventHandler(this.speRowOfSingleDst_EditValueChanged);
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl37.AppearanceDisabled.Options.UseFont = true;
            this.labelControl37.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl37.AppearanceHovered.Options.UseFont = true;
            this.labelControl37.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl37.AppearancePressed.Options.UseFont = true;
            this.labelControl37.Location = new System.Drawing.Point(17, 69);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(48, 17);
            this.labelControl37.TabIndex = 10;
            this.labelControl37.Text = "产品例数";
            // 
            // speColOfSingle
            // 
            this.speColOfSingle.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speColOfSingle.Location = new System.Drawing.Point(71, 67);
            this.speColOfSingle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speColOfSingle.Name = "speColOfSingle";
            this.speColOfSingle.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingle.Properties.Appearance.Options.UseFont = true;
            this.speColOfSingle.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingle.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speColOfSingle.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingle.Properties.AppearanceFocused.Options.UseFont = true;
            this.speColOfSingle.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColOfSingle.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speColOfSingle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speColOfSingle.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.speColOfSingle.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speColOfSingle.Size = new System.Drawing.Size(80, 24);
            this.speColOfSingle.TabIndex = 9;
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl38.AppearanceDisabled.Options.UseFont = true;
            this.labelControl38.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl38.AppearanceHovered.Options.UseFont = true;
            this.labelControl38.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl38.AppearancePressed.Options.UseFont = true;
            this.labelControl38.Location = new System.Drawing.Point(17, 38);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(48, 17);
            this.labelControl38.TabIndex = 8;
            this.labelControl38.Text = "产品行数";
            // 
            // speRowOfSingle
            // 
            this.speRowOfSingle.EditValue = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.speRowOfSingle.Location = new System.Drawing.Point(71, 35);
            this.speRowOfSingle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speRowOfSingle.Name = "speRowOfSingle";
            this.speRowOfSingle.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speRowOfSingle.Properties.Appearance.Options.UseFont = true;
            this.speRowOfSingle.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowOfSingle.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speRowOfSingle.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowOfSingle.Properties.AppearanceFocused.Options.UseFont = true;
            this.speRowOfSingle.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowOfSingle.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speRowOfSingle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speRowOfSingle.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.speRowOfSingle.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speRowOfSingle.Size = new System.Drawing.Size(80, 24);
            this.speRowOfSingle.TabIndex = 7;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton8.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearanceHovered.Options.UseFont = true;
            this.simpleButton8.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearancePressed.Options.UseFont = true;
            this.simpleButton8.Location = new System.Drawing.Point(694, 200);
            this.simpleButton8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(134, 68);
            this.simpleButton8.TabIndex = 5;
            this.simpleButton8.Text = "清除所有";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton9.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearanceHovered.Options.UseFont = true;
            this.simpleButton9.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearancePressed.Options.UseFont = true;
            this.simpleButton9.Location = new System.Drawing.Point(694, 125);
            this.simpleButton9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(134, 68);
            this.simpleButton9.TabIndex = 4;
            this.simpleButton9.Text = "删除路径";
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton10.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton10.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton10.AppearanceHovered.Options.UseFont = true;
            this.simpleButton10.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton10.AppearancePressed.Options.UseFont = true;
            this.simpleButton10.Location = new System.Drawing.Point(694, 276);
            this.simpleButton10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(134, 68);
            this.simpleButton10.TabIndex = 3;
            this.simpleButton10.Text = "学习当前(行)路径";
            // 
            // simpleButton24
            // 
            this.simpleButton24.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton24.Appearance.Options.UseFont = true;
            this.simpleButton24.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton24.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton24.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton24.AppearanceHovered.Options.UseFont = true;
            this.simpleButton24.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton24.AppearancePressed.Options.UseFont = true;
            this.simpleButton24.Location = new System.Drawing.Point(694, 50);
            this.simpleButton24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(134, 68);
            this.simpleButton24.TabIndex = 2;
            this.simpleButton24.Text = "增加路径";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label55.Location = new System.Drawing.Point(10, 90);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(334, 17);
            this.label55.TabIndex = 149;
            this.label55.Text = "放料时 下料盘放料的第一个位置(机器人1号吸嘴对第1个产品)";
            // 
            // btmRobotLearnLoadFirst_Go
            // 
            this.btmRobotLearnLoadFirst_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnLoadFirst_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnLoadFirst_Go.Location = new System.Drawing.Point(519, 97);
            this.btmRobotLearnLoadFirst_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnLoadFirst_Go.Name = "btmRobotLearnLoadFirst_Go";
            this.btmRobotLearnLoadFirst_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnLoadFirst_Go.TabIndex = 137;
            this.btmRobotLearnLoadFirst_Go.Text = "Go当前位置";
            this.btmRobotLearnLoadFirst_Go.Click += new System.EventHandler(this.btmRobotLearnLoadFirst_Go_Click);
            // 
            // btmRobotLearnLoadFirst
            // 
            this.btmRobotLearnLoadFirst.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnLoadFirst.Appearance.Options.UseFont = true;
            this.btmRobotLearnLoadFirst.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnLoadFirst.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnLoadFirst.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnLoadFirst.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnLoadFirst.Location = new System.Drawing.Point(362, 97);
            this.btmRobotLearnLoadFirst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnLoadFirst.Name = "btmRobotLearnLoadFirst";
            this.btmRobotLearnLoadFirst.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnLoadFirst.TabIndex = 136;
            this.btmRobotLearnLoadFirst.Text = "学习/得到当前位置";
            this.btmRobotLearnLoadFirst.Click += new System.EventHandler(this.btmRobotLearnLoadFirst_Click);
            // 
            // txtRobotPos_LoadFirst
            // 
            this.txtRobotPos_LoadFirst.Location = new System.Drawing.Point(8, 112);
            this.txtRobotPos_LoadFirst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotPos_LoadFirst.Name = "txtRobotPos_LoadFirst";
            this.txtRobotPos_LoadFirst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotPos_LoadFirst.Properties.Appearance.Options.UseFont = true;
            this.txtRobotPos_LoadFirst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_LoadFirst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotPos_LoadFirst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_LoadFirst.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotPos_LoadFirst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_LoadFirst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotPos_LoadFirst.Size = new System.Drawing.Size(307, 24);
            this.txtRobotPos_LoadFirst.TabIndex = 135;
            // 
            // btmRobotLearnBuffFirst_Go
            // 
            this.btmRobotLearnBuffFirst_Go.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnBuffFirst_Go.Appearance.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst_Go.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst_Go.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst_Go.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnBuffFirst_Go.Location = new System.Drawing.Point(519, 33);
            this.btmRobotLearnBuffFirst_Go.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnBuffFirst_Go.Name = "btmRobotLearnBuffFirst_Go";
            this.btmRobotLearnBuffFirst_Go.Size = new System.Drawing.Size(132, 53);
            this.btmRobotLearnBuffFirst_Go.TabIndex = 134;
            this.btmRobotLearnBuffFirst_Go.Text = "Go当前位置";
            this.btmRobotLearnBuffFirst_Go.Click += new System.EventHandler(this.btmRobotLearnBuffFirst_Go_Click);
            // 
            // btmRobotLearnBuffFirst
            // 
            this.btmRobotLearnBuffFirst.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotLearnBuffFirst.Appearance.Options.UseFont = true;
            this.btmRobotLearnBuffFirst.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotLearnBuffFirst.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst.AppearanceHovered.Options.UseFont = true;
            this.btmRobotLearnBuffFirst.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotLearnBuffFirst.AppearancePressed.Options.UseFont = true;
            this.btmRobotLearnBuffFirst.Location = new System.Drawing.Point(362, 33);
            this.btmRobotLearnBuffFirst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotLearnBuffFirst.Name = "btmRobotLearnBuffFirst";
            this.btmRobotLearnBuffFirst.Size = new System.Drawing.Size(151, 53);
            this.btmRobotLearnBuffFirst.TabIndex = 133;
            this.btmRobotLearnBuffFirst.Text = "学习/得到当前位置";
            this.btmRobotLearnBuffFirst.Click += new System.EventHandler(this.btmRobotLearnBuffFirst_Click);
            // 
            // txtRobotPos_BuffFirst
            // 
            this.txtRobotPos_BuffFirst.Location = new System.Drawing.Point(8, 48);
            this.txtRobotPos_BuffFirst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRobotPos_BuffFirst.Name = "txtRobotPos_BuffFirst";
            this.txtRobotPos_BuffFirst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRobotPos_BuffFirst.Properties.Appearance.Options.UseFont = true;
            this.txtRobotPos_BuffFirst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_BuffFirst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtRobotPos_BuffFirst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_BuffFirst.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtRobotPos_BuffFirst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtRobotPos_BuffFirst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtRobotPos_BuffFirst.Size = new System.Drawing.Size(307, 24);
            this.txtRobotPos_BuffFirst.TabIndex = 132;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label58.Location = new System.Drawing.Point(10, 26);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(346, 17);
            this.label58.TabIndex = 131;
            this.label58.Text = "放料时 缓存盘放料料的第一个位置(机器人1号吸嘴对第1个产品)";
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.groupControl6);
            this.xtraTabPage5.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage5.Image")));
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1452, 598);
            this.xtraTabPage5.Text = "机器人试运行路径定义";
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl6.AppearanceCaption.Options.UseFont = true;
            this.groupControl6.Controls.Add(this.btmRobotWayTrayRunCurrent);
            this.groupControl6.Controls.Add(this.groupControl7);
            this.groupControl6.Controls.Add(this.label22);
            this.groupControl6.Controls.Add(this.comboxRunTo);
            this.groupControl6.Controls.Add(this.label21);
            this.groupControl6.Controls.Add(this.comboxRunFrom);
            this.groupControl6.Controls.Add(this.btmRobotWayTrayRun);
            this.groupControl6.Controls.Add(this.btmRobotWayClear);
            this.groupControl6.Controls.Add(this.btmRobotWayDel);
            this.groupControl6.Controls.Add(this.btmRobotWayLearn);
            this.groupControl6.Controls.Add(this.gridRobotWayList);
            this.groupControl6.Controls.Add(this.btmRobotWayAdd);
            this.groupControl6.Location = new System.Drawing.Point(0, 0);
            this.groupControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(1457, 599);
            this.groupControl6.TabIndex = 97;
            this.groupControl6.Text = "放料路径";
            // 
            // btmRobotWayTrayRunCurrent
            // 
            this.btmRobotWayTrayRunCurrent.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotWayTrayRunCurrent.Appearance.Options.UseFont = true;
            this.btmRobotWayTrayRunCurrent.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayTrayRunCurrent.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotWayTrayRunCurrent.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayTrayRunCurrent.AppearanceHovered.Options.UseFont = true;
            this.btmRobotWayTrayRunCurrent.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayTrayRunCurrent.AppearancePressed.Options.UseFont = true;
            this.btmRobotWayTrayRunCurrent.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRobotWayTrayRunCurrent.ImageOptions.Image")));
            this.btmRobotWayTrayRunCurrent.Location = new System.Drawing.Point(147, 315);
            this.btmRobotWayTrayRunCurrent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotWayTrayRunCurrent.Name = "btmRobotWayTrayRunCurrent";
            this.btmRobotWayTrayRunCurrent.Size = new System.Drawing.Size(135, 67);
            this.btmRobotWayTrayRunCurrent.TabIndex = 134;
            this.btmRobotWayTrayRunCurrent.Text = "Go当前行路径";
            this.btmRobotWayTrayRunCurrent.Click += new System.EventHandler(this.btmRobotWayTrayRunCurrent_Click);
            // 
            // groupControl7
            // 
            this.groupControl7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl7.Appearance.Options.UseFont = true;
            this.groupControl7.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl7.AppearanceCaption.Options.UseFont = true;
            this.groupControl7.Controls.Add(this.simpleButton17);
            this.groupControl7.Controls.Add(this.simpleButton18);
            this.groupControl7.Controls.Add(this.simpleButton19);
            this.groupControl7.Controls.Add(this.simpleButton20);
            this.groupControl7.Location = new System.Drawing.Point(738, 324);
            this.groupControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(193, 75);
            this.groupControl7.TabIndex = 98;
            this.groupControl7.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // simpleButton17
            // 
            this.simpleButton17.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton17.Appearance.Options.UseFont = true;
            this.simpleButton17.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton17.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton17.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton17.AppearanceHovered.Options.UseFont = true;
            this.simpleButton17.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton17.AppearancePressed.Options.UseFont = true;
            this.simpleButton17.Location = new System.Drawing.Point(694, 200);
            this.simpleButton17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(134, 68);
            this.simpleButton17.TabIndex = 5;
            this.simpleButton17.Text = "清除所有";
            // 
            // simpleButton18
            // 
            this.simpleButton18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton18.Appearance.Options.UseFont = true;
            this.simpleButton18.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton18.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton18.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton18.AppearanceHovered.Options.UseFont = true;
            this.simpleButton18.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton18.AppearancePressed.Options.UseFont = true;
            this.simpleButton18.Location = new System.Drawing.Point(694, 125);
            this.simpleButton18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(134, 68);
            this.simpleButton18.TabIndex = 4;
            this.simpleButton18.Text = "删除路径";
            // 
            // simpleButton19
            // 
            this.simpleButton19.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton19.Appearance.Options.UseFont = true;
            this.simpleButton19.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton19.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton19.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton19.AppearanceHovered.Options.UseFont = true;
            this.simpleButton19.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton19.AppearancePressed.Options.UseFont = true;
            this.simpleButton19.Location = new System.Drawing.Point(694, 276);
            this.simpleButton19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(134, 68);
            this.simpleButton19.TabIndex = 3;
            this.simpleButton19.Text = "学习当前(行)路径";
            // 
            // simpleButton20
            // 
            this.simpleButton20.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton20.Appearance.Options.UseFont = true;
            this.simpleButton20.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton20.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton20.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton20.AppearanceHovered.Options.UseFont = true;
            this.simpleButton20.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton20.AppearancePressed.Options.UseFont = true;
            this.simpleButton20.Location = new System.Drawing.Point(694, 50);
            this.simpleButton20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(134, 68);
            this.simpleButton20.TabIndex = 2;
            this.simpleButton20.Text = "增加路径";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(646, 354);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 17);
            this.label22.TabIndex = 133;
            this.label22.Text = "to";
            // 
            // comboxRunTo
            // 
            this.comboxRunTo.Location = new System.Drawing.Point(668, 351);
            this.comboxRunTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboxRunTo.Name = "comboxRunTo";
            this.comboxRunTo.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboxRunTo.Properties.Appearance.Options.UseFont = true;
            this.comboxRunTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboxRunTo.Size = new System.Drawing.Size(51, 24);
            this.comboxRunTo.TabIndex = 133;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(588, 324);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(128, 17);
            this.label21.TabIndex = 132;
            this.label21.Text = "试运行开始to结束步骤";
            // 
            // comboxRunFrom
            // 
            this.comboxRunFrom.Location = new System.Drawing.Point(589, 351);
            this.comboxRunFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboxRunFrom.Name = "comboxRunFrom";
            this.comboxRunFrom.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboxRunFrom.Properties.Appearance.Options.UseFont = true;
            this.comboxRunFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboxRunFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.comboxRunFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboxRunFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.comboxRunFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboxRunFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.comboxRunFrom.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.comboxRunFrom.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.comboxRunFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboxRunFrom.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboxRunFrom.Size = new System.Drawing.Size(51, 24);
            this.comboxRunFrom.TabIndex = 7;
            // 
            // btmRobotWayTrayRun
            // 
            this.btmRobotWayTrayRun.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotWayTrayRun.Appearance.Options.UseFont = true;
            this.btmRobotWayTrayRun.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayTrayRun.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotWayTrayRun.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayTrayRun.AppearanceHovered.Options.UseFont = true;
            this.btmRobotWayTrayRun.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayTrayRun.AppearancePressed.Options.UseFont = true;
            this.btmRobotWayTrayRun.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRobotWayTrayRun.ImageOptions.Image")));
            this.btmRobotWayTrayRun.Location = new System.Drawing.Point(447, 316);
            this.btmRobotWayTrayRun.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotWayTrayRun.Name = "btmRobotWayTrayRun";
            this.btmRobotWayTrayRun.Size = new System.Drawing.Size(135, 68);
            this.btmRobotWayTrayRun.TabIndex = 6;
            this.btmRobotWayTrayRun.Text = "Go设定路径";
            this.btmRobotWayTrayRun.Click += new System.EventHandler(this.btmRobotWayTrayRun_Click);
            // 
            // btmRobotWayClear
            // 
            this.btmRobotWayClear.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotWayClear.Appearance.Options.UseFont = true;
            this.btmRobotWayClear.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayClear.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotWayClear.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayClear.AppearanceHovered.Options.UseFont = true;
            this.btmRobotWayClear.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayClear.AppearancePressed.Options.UseFont = true;
            this.btmRobotWayClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRobotWayClear.ImageOptions.Image")));
            this.btmRobotWayClear.Location = new System.Drawing.Point(900, 181);
            this.btmRobotWayClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotWayClear.Name = "btmRobotWayClear";
            this.btmRobotWayClear.Size = new System.Drawing.Size(135, 68);
            this.btmRobotWayClear.TabIndex = 5;
            this.btmRobotWayClear.Text = "清除所有";
            this.btmRobotWayClear.Click += new System.EventHandler(this.btmRobotWayClear_Click);
            // 
            // btmRobotWayDel
            // 
            this.btmRobotWayDel.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotWayDel.Appearance.Options.UseFont = true;
            this.btmRobotWayDel.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayDel.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotWayDel.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayDel.AppearanceHovered.Options.UseFont = true;
            this.btmRobotWayDel.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayDel.AppearancePressed.Options.UseFont = true;
            this.btmRobotWayDel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRobotWayDel.ImageOptions.Image")));
            this.btmRobotWayDel.Location = new System.Drawing.Point(900, 106);
            this.btmRobotWayDel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotWayDel.Name = "btmRobotWayDel";
            this.btmRobotWayDel.Size = new System.Drawing.Size(135, 68);
            this.btmRobotWayDel.TabIndex = 4;
            this.btmRobotWayDel.Text = "删除路径";
            this.btmRobotWayDel.Click += new System.EventHandler(this.btmRobotWayDel_Click);
            // 
            // btmRobotWayLearn
            // 
            this.btmRobotWayLearn.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotWayLearn.Appearance.Options.UseFont = true;
            this.btmRobotWayLearn.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayLearn.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotWayLearn.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayLearn.AppearanceHovered.Options.UseFont = true;
            this.btmRobotWayLearn.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayLearn.AppearancePressed.Options.UseFont = true;
            this.btmRobotWayLearn.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRobotWayLearn.ImageOptions.Image")));
            this.btmRobotWayLearn.Location = new System.Drawing.Point(6, 314);
            this.btmRobotWayLearn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotWayLearn.Name = "btmRobotWayLearn";
            this.btmRobotWayLearn.Size = new System.Drawing.Size(135, 68);
            this.btmRobotWayLearn.TabIndex = 3;
            this.btmRobotWayLearn.Text = "学习当前(行)路径";
            this.btmRobotWayLearn.Click += new System.EventHandler(this.btmRobotWayLearn_Click);
            // 
            // gridRobotWayList
            // 
            this.gridRobotWayList.AllowUserToAddRows = false;
            this.gridRobotWayList.AllowUserToDeleteRows = false;
            this.gridRobotWayList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridRobotWayList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridRobotWayList.ColumnHeadersHeight = 24;
            this.gridRobotWayList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridRobotWayList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column8,
            this.Column9,
            this.Column6,
            this.Column7});
            this.gridRobotWayList.Location = new System.Drawing.Point(6, 33);
            this.gridRobotWayList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridRobotWayList.Name = "gridRobotWayList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridRobotWayList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridRobotWayList.RowHeadersWidth = 150;
            this.gridRobotWayList.RowTemplate.Height = 23;
            this.gridRobotWayList.Size = new System.Drawing.Size(888, 273);
            this.gridRobotWayList.TabIndex = 1;
            // 
            // Column1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "X";
            this.Column1.Name = "Column1";
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Y";
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 80;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Z";
            this.Column3.Name = "Column3";
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 80;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "R";
            this.Column4.Name = "Column4";
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 80;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Left/Right";
            this.Column5.Items.AddRange(new object[] {
            "/L",
            "/R",
            "Auto"});
            this.Column5.Name = "Column5";
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column5.Width = 80;
            // 
            // Column8
            // 
            this.Column8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Column8.HeaderText = "运动方式";
            this.Column8.Items.AddRange(new object[] {
            "Move",
            "Move ROT",
            "Jump",
            "Arc"});
            this.Column8.Name = "Column8";
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column8.Width = 80;
            // 
            // Column9
            // 
            this.Column9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Column9.HeaderText = "连续运动？";
            this.Column9.Items.AddRange(new object[] {
            "CP"});
            this.Column9.Name = "Column9";
            this.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "ZLimit";
            this.Column6.Name = "Column6";
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column6.Width = 80;
            // 
            // Column7
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column7.HeaderText = "启用/禁用";
            this.Column7.Items.AddRange(new object[] {
            "true",
            "false"});
            this.Column7.Name = "Column7";
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column7.Width = 80;
            // 
            // btmRobotWayAdd
            // 
            this.btmRobotWayAdd.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRobotWayAdd.Appearance.Options.UseFont = true;
            this.btmRobotWayAdd.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayAdd.AppearanceDisabled.Options.UseFont = true;
            this.btmRobotWayAdd.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayAdd.AppearanceHovered.Options.UseFont = true;
            this.btmRobotWayAdd.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRobotWayAdd.AppearancePressed.Options.UseFont = true;
            this.btmRobotWayAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRobotWayAdd.ImageOptions.Image")));
            this.btmRobotWayAdd.Location = new System.Drawing.Point(900, 30);
            this.btmRobotWayAdd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRobotWayAdd.Name = "btmRobotWayAdd";
            this.btmRobotWayAdd.Size = new System.Drawing.Size(135, 68);
            this.btmRobotWayAdd.TabIndex = 2;
            this.btmRobotWayAdd.Text = "增加路径";
            this.btmRobotWayAdd.Click += new System.EventHandler(this.btmRobotWayAdd_Click);
            // 
            // groupControl13
            // 
            this.groupControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl13.Appearance.Options.UseFont = true;
            this.groupControl13.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl13.AppearanceCaption.Options.UseFont = true;
            this.groupControl13.Controls.Add(this.labelControl64);
            this.groupControl13.Controls.Add(this.spedOrgStepDst);
            this.groupControl13.Controls.Add(this.labelControl63);
            this.groupControl13.Controls.Add(this.btmSetDst);
            this.groupControl13.Controls.Add(this.spinDstRow);
            this.groupControl13.Controls.Add(this.label24);
            this.groupControl13.Controls.Add(this.simpleButton31);
            this.groupControl13.Controls.Add(this.设定吸嘴间距);
            this.groupControl13.Controls.Add(this.labelControl4);
            this.groupControl13.Controls.Add(this.simpleButton16);
            this.groupControl13.Controls.Add(this.speCathDst);
            this.groupControl13.Controls.Add(this.simpleButton21);
            this.groupControl13.Controls.Add(this.simpleButton22);
            this.groupControl13.Controls.Add(this.simpleButton23);
            this.groupControl13.Location = new System.Drawing.Point(0, 223);
            this.groupControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl13.Name = "groupControl13";
            this.groupControl13.Size = new System.Drawing.Size(226, 278);
            this.groupControl13.TabIndex = 177;
            this.groupControl13.Text = "电机设定";
            // 
            // labelControl64
            // 
            this.labelControl64.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelControl64.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl64.Appearance.Options.UseBackColor = true;
            this.labelControl64.Appearance.Options.UseFont = true;
            this.labelControl64.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl64.AppearanceDisabled.Options.UseFont = true;
            this.labelControl64.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl64.AppearanceHovered.Options.UseFont = true;
            this.labelControl64.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl64.AppearancePressed.Options.UseFont = true;
            this.labelControl64.Location = new System.Drawing.Point(7, 29);
            this.labelControl64.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(108, 17);
            this.labelControl64.TabIndex = 176;
            this.labelControl64.Text = "电机回原点之后间距";
            // 
            // spedOrgStepDst
            // 
            this.spedOrgStepDst.EditValue = new decimal(new int[] {
            80,
            0,
            0,
            65536});
            this.spedOrgStepDst.Location = new System.Drawing.Point(119, 26);
            this.spedOrgStepDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spedOrgStepDst.Name = "spedOrgStepDst";
            this.spedOrgStepDst.Properties.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.spedOrgStepDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedOrgStepDst.Properties.Appearance.Options.UseBackColor = true;
            this.spedOrgStepDst.Properties.Appearance.Options.UseFont = true;
            this.spedOrgStepDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedOrgStepDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedOrgStepDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedOrgStepDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedOrgStepDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedOrgStepDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedOrgStepDst.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedOrgStepDst.Size = new System.Drawing.Size(80, 24);
            this.spedOrgStepDst.TabIndex = 175;
            // 
            // labelControl63
            // 
            this.labelControl63.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl63.Appearance.Options.UseFont = true;
            this.labelControl63.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl63.AppearanceDisabled.Options.UseFont = true;
            this.labelControl63.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl63.AppearanceHovered.Options.UseFont = true;
            this.labelControl63.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl63.AppearancePressed.Options.UseFont = true;
            this.labelControl63.Location = new System.Drawing.Point(94, 103);
            this.labelControl63.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(30, 17);
            this.labelControl63.TabIndex = 174;
            this.labelControl63.Text = "(mm)";
            // 
            // btmSetDst
            // 
            this.btmSetDst.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetDst.Appearance.Options.UseFont = true;
            this.btmSetDst.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetDst.AppearanceDisabled.Options.UseFont = true;
            this.btmSetDst.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetDst.AppearanceHovered.Options.UseFont = true;
            this.btmSetDst.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetDst.AppearancePressed.Options.UseFont = true;
            this.btmSetDst.Location = new System.Drawing.Point(124, 98);
            this.btmSetDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetDst.Name = "btmSetDst";
            this.btmSetDst.Size = new System.Drawing.Size(93, 37);
            this.btmSetDst.TabIndex = 173;
            this.btmSetDst.Text = "设定间距";
            this.btmSetDst.Click += new System.EventHandler(this.btmSetDst_Click);
            // 
            // spinDstRow
            // 
            this.spinDstRow.EditValue = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.spinDstRow.Location = new System.Drawing.Point(33, 100);
            this.spinDstRow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.spinDstRow.Name = "spinDstRow";
            this.spinDstRow.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinDstRow.Properties.Appearance.Options.UseFont = true;
            this.spinDstRow.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinDstRow.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spinDstRow.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinDstRow.Properties.AppearanceFocused.Options.UseFont = true;
            this.spinDstRow.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spinDstRow.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spinDstRow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinDstRow.Size = new System.Drawing.Size(59, 24);
            this.spinDstRow.TabIndex = 172;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Yellow;
            this.label24.Location = new System.Drawing.Point(5, 212);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(216, 41);
            this.label24.TabIndex = 171;
            this.label24.Text = "在保存参数数据或操作之前先保证电机已经回原点";
            // 
            // simpleButton31
            // 
            this.simpleButton31.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton31.Appearance.Options.UseFont = true;
            this.simpleButton31.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton31.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton31.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton31.AppearanceHovered.Options.UseFont = true;
            this.simpleButton31.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton31.AppearancePressed.Options.UseFont = true;
            this.simpleButton31.Location = new System.Drawing.Point(8, 148);
            this.simpleButton31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton31.Name = "simpleButton31";
            this.simpleButton31.Size = new System.Drawing.Size(209, 37);
            this.simpleButton31.TabIndex = 170;
            this.simpleButton31.Text = "GO 位置";
            this.simpleButton31.Click += new System.EventHandler(this.simpleButton31_Click);
            // 
            // 设定吸嘴间距
            // 
            this.设定吸嘴间距.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.设定吸嘴间距.Appearance.Options.UseFont = true;
            this.设定吸嘴间距.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设定吸嘴间距.AppearanceDisabled.Options.UseFont = true;
            this.设定吸嘴间距.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设定吸嘴间距.AppearanceHovered.Options.UseFont = true;
            this.设定吸嘴间距.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.设定吸嘴间距.AppearancePressed.Options.UseFont = true;
            this.设定吸嘴间距.Location = new System.Drawing.Point(124, 59);
            this.设定吸嘴间距.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.设定吸嘴间距.Name = "设定吸嘴间距";
            this.设定吸嘴间距.Size = new System.Drawing.Size(93, 37);
            this.设定吸嘴间距.TabIndex = 169;
            this.设定吸嘴间距.Text = "读取当前位置";
            this.设定吸嘴间距.Click += new System.EventHandler(this.设定吸嘴间距_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl4.AppearanceDisabled.Options.UseFont = true;
            this.labelControl4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl4.AppearanceHovered.Options.UseFont = true;
            this.labelControl4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl4.AppearancePressed.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(6, 68);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(24, 17);
            this.labelControl4.TabIndex = 153;
            this.labelControl4.Text = "间距";
            // 
            // simpleButton16
            // 
            this.simpleButton16.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton16.Appearance.Options.UseFont = true;
            this.simpleButton16.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton16.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton16.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton16.AppearanceHovered.Options.UseFont = true;
            this.simpleButton16.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton16.AppearancePressed.Options.UseFont = true;
            this.simpleButton16.Location = new System.Drawing.Point(694, 200);
            this.simpleButton16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(134, 68);
            this.simpleButton16.TabIndex = 5;
            this.simpleButton16.Text = "清除所有";
            // 
            // speCathDst
            // 
            this.speCathDst.EditValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speCathDst.Location = new System.Drawing.Point(33, 66);
            this.speCathDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speCathDst.Name = "speCathDst";
            this.speCathDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCathDst.Properties.Appearance.Options.UseFont = true;
            this.speCathDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCathDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speCathDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCathDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.speCathDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCathDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speCathDst.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speCathDst.Size = new System.Drawing.Size(80, 24);
            this.speCathDst.TabIndex = 152;
            // 
            // simpleButton21
            // 
            this.simpleButton21.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton21.Appearance.Options.UseFont = true;
            this.simpleButton21.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton21.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton21.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton21.AppearanceHovered.Options.UseFont = true;
            this.simpleButton21.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton21.AppearancePressed.Options.UseFont = true;
            this.simpleButton21.Location = new System.Drawing.Point(694, 125);
            this.simpleButton21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton21.Name = "simpleButton21";
            this.simpleButton21.Size = new System.Drawing.Size(134, 68);
            this.simpleButton21.TabIndex = 4;
            this.simpleButton21.Text = "删除路径";
            // 
            // simpleButton22
            // 
            this.simpleButton22.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton22.Appearance.Options.UseFont = true;
            this.simpleButton22.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton22.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton22.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton22.AppearanceHovered.Options.UseFont = true;
            this.simpleButton22.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton22.AppearancePressed.Options.UseFont = true;
            this.simpleButton22.Location = new System.Drawing.Point(694, 276);
            this.simpleButton22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton22.Name = "simpleButton22";
            this.simpleButton22.Size = new System.Drawing.Size(134, 68);
            this.simpleButton22.TabIndex = 3;
            this.simpleButton22.Text = "学习当前(行)路径";
            // 
            // simpleButton23
            // 
            this.simpleButton23.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton23.Appearance.Options.UseFont = true;
            this.simpleButton23.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton23.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton23.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton23.AppearanceHovered.Options.UseFont = true;
            this.simpleButton23.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton23.AppearancePressed.Options.UseFont = true;
            this.simpleButton23.Location = new System.Drawing.Point(694, 50);
            this.simpleButton23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(134, 68);
            this.simpleButton23.TabIndex = 2;
            this.simpleButton23.Text = "增加路径";
            // 
            // btmSaveRobotConfig
            // 
            this.btmSaveRobotConfig.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.btmSaveRobotConfig.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmSaveRobotConfig.Appearance.Options.UseBackColor = true;
            this.btmSaveRobotConfig.Appearance.Options.UseFont = true;
            this.btmSaveRobotConfig.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSaveRobotConfig.AppearanceDisabled.Options.UseFont = true;
            this.btmSaveRobotConfig.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Italic);
            this.btmSaveRobotConfig.AppearanceHovered.Options.UseFont = true;
            this.btmSaveRobotConfig.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSaveRobotConfig.AppearancePressed.Options.UseFont = true;
            this.btmSaveRobotConfig.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmSaveRobotConfig.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSaveRobotConfig.ImageOptions.Image")));
            this.btmSaveRobotConfig.Location = new System.Drawing.Point(-1, 509);
            this.btmSaveRobotConfig.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSaveRobotConfig.Name = "btmSaveRobotConfig";
            this.btmSaveRobotConfig.Size = new System.Drawing.Size(228, 132);
            this.btmSaveRobotConfig.TabIndex = 175;
            this.btmSaveRobotConfig.Text = "保存所有";
            this.btmSaveRobotConfig.Click += new System.EventHandler(this.btmSaveRobotConfig_Click);
            // 
            // groupControl12
            // 
            this.groupControl12.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl12.Appearance.Options.UseFont = true;
            this.groupControl12.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl12.AppearanceCaption.Options.UseFont = true;
            this.groupControl12.Controls.Add(this.simpleButton12);
            this.groupControl12.Controls.Add(this.simpleButton13);
            this.groupControl12.Controls.Add(this.simpleButton14);
            this.groupControl12.Controls.Add(this.simpleButton15);
            this.groupControl12.Controls.Add(this.chkZero);
            this.groupControl12.Controls.Add(this.groupControl9);
            this.groupControl12.Location = new System.Drawing.Point(0, 0);
            this.groupControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl12.Name = "groupControl12";
            this.groupControl12.Size = new System.Drawing.Size(225, 215);
            this.groupControl12.TabIndex = 176;
            this.groupControl12.Text = "机器人其它设定";
            // 
            // simpleButton12
            // 
            this.simpleButton12.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton12.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton12.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton12.AppearanceHovered.Options.UseFont = true;
            this.simpleButton12.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton12.AppearancePressed.Options.UseFont = true;
            this.simpleButton12.Location = new System.Drawing.Point(694, 200);
            this.simpleButton12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(134, 68);
            this.simpleButton12.TabIndex = 5;
            this.simpleButton12.Text = "清除所有";
            // 
            // simpleButton13
            // 
            this.simpleButton13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton13.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton13.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton13.AppearanceHovered.Options.UseFont = true;
            this.simpleButton13.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton13.AppearancePressed.Options.UseFont = true;
            this.simpleButton13.Location = new System.Drawing.Point(694, 125);
            this.simpleButton13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(134, 68);
            this.simpleButton13.TabIndex = 4;
            this.simpleButton13.Text = "删除路径";
            // 
            // simpleButton14
            // 
            this.simpleButton14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton14.Appearance.Options.UseFont = true;
            this.simpleButton14.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton14.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton14.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton14.AppearanceHovered.Options.UseFont = true;
            this.simpleButton14.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton14.AppearancePressed.Options.UseFont = true;
            this.simpleButton14.Location = new System.Drawing.Point(694, 276);
            this.simpleButton14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(134, 68);
            this.simpleButton14.TabIndex = 3;
            this.simpleButton14.Text = "学习当前(行)路径";
            // 
            // simpleButton15
            // 
            this.simpleButton15.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton15.Appearance.Options.UseFont = true;
            this.simpleButton15.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton15.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton15.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton15.AppearanceHovered.Options.UseFont = true;
            this.simpleButton15.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton15.AppearancePressed.Options.UseFont = true;
            this.simpleButton15.Location = new System.Drawing.Point(694, 50);
            this.simpleButton15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(134, 68);
            this.simpleButton15.TabIndex = 2;
            this.simpleButton15.Text = "增加路径";
            // 
            // chkZero
            // 
            this.chkZero.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.chkZero.Location = new System.Drawing.Point(6, 158);
            this.chkZero.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkZero.Name = "chkZero";
            this.chkZero.Size = new System.Drawing.Size(215, 24);
            this.chkZero.TabIndex = 150;
            this.chkZero.Text = "学习坐标时R轴强置为0.00";
            this.chkZero.UseVisualStyleBackColor = true;
            // 
            // groupControl9
            // 
            this.groupControl9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl9.Appearance.Options.UseFont = true;
            this.groupControl9.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl9.AppearanceCaption.Options.UseFont = true;
            this.groupControl9.Controls.Add(this.chkHandelTypeRight);
            this.groupControl9.Controls.Add(this.chkHandelTypeLeft);
            this.groupControl9.Controls.Add(this.chkHandelTypeAuto);
            this.groupControl9.Location = new System.Drawing.Point(5, 26);
            this.groupControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(173, 113);
            this.groupControl9.TabIndex = 151;
            this.groupControl9.Text = "机器人移动手臂姿势";
            // 
            // chkHandelTypeRight
            // 
            this.chkHandelTypeRight.AutoSize = true;
            this.chkHandelTypeRight.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHandelTypeRight.Location = new System.Drawing.Point(18, 86);
            this.chkHandelTypeRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkHandelTypeRight.Name = "chkHandelTypeRight";
            this.chkHandelTypeRight.Size = new System.Drawing.Size(74, 21);
            this.chkHandelTypeRight.TabIndex = 2;
            this.chkHandelTypeRight.TabStop = true;
            this.chkHandelTypeRight.Text = "右手姿势";
            this.chkHandelTypeRight.UseVisualStyleBackColor = true;
            // 
            // chkHandelTypeLeft
            // 
            this.chkHandelTypeLeft.AutoSize = true;
            this.chkHandelTypeLeft.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHandelTypeLeft.Location = new System.Drawing.Point(18, 56);
            this.chkHandelTypeLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkHandelTypeLeft.Name = "chkHandelTypeLeft";
            this.chkHandelTypeLeft.Size = new System.Drawing.Size(74, 21);
            this.chkHandelTypeLeft.TabIndex = 1;
            this.chkHandelTypeLeft.TabStop = true;
            this.chkHandelTypeLeft.Text = "左手姿势";
            this.chkHandelTypeLeft.UseVisualStyleBackColor = true;
            // 
            // chkHandelTypeAuto
            // 
            this.chkHandelTypeAuto.AutoSize = true;
            this.chkHandelTypeAuto.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHandelTypeAuto.Location = new System.Drawing.Point(18, 26);
            this.chkHandelTypeAuto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkHandelTypeAuto.Name = "chkHandelTypeAuto";
            this.chkHandelTypeAuto.Size = new System.Drawing.Size(50, 21);
            this.chkHandelTypeAuto.TabIndex = 0;
            this.chkHandelTypeAuto.TabStop = true;
            this.chkHandelTypeAuto.Text = "自动";
            this.chkHandelTypeAuto.UseVisualStyleBackColor = true;
            // 
            // NP_手动调试
            // 
            this.NP_手动调试.Caption = " 手动&监控";
            this.NP_手动调试.Controls.Add(this.groupControl19);
            this.NP_手动调试.Controls.Add(this.groupControl8);
            this.NP_手动调试.Controls.Add(this.groupControl4);
            this.NP_手动调试.Controls.Add(this.groupControl5);
            this.NP_手动调试.Controls.Add(this.groupControl1);
            this.NP_手动调试.Controls.Add(this.groupControl14);
            this.NP_手动调试.Image = ((System.Drawing.Image)(resources.GetObject("NP_手动调试.Image")));
            this.NP_手动调试.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_手动调试.Name = "NP_手动调试";
            this.NP_手动调试.Size = new System.Drawing.Size(1684, 647);
            // 
            // groupControl19
            // 
            this.groupControl19.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl19.Appearance.Options.UseFont = true;
            this.groupControl19.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl19.AppearanceCaption.Options.UseFont = true;
            this.groupControl19.Controls.Add(this.强制camera2结果);
            this.groupControl19.Controls.Add(this.强制camera1结果);
            this.groupControl19.Controls.Add(this.chkEnableRobotIO);
            this.groupControl19.Controls.Add(this.chkCareFlashResult);
            this.groupControl19.Controls.Add(this.chkVacuuSensor);
            this.groupControl19.Controls.Add(this.textEdit9);
            this.groupControl19.Controls.Add(this.labelControl58);
            this.groupControl19.Controls.Add(this.bmtUnbindTray2);
            this.groupControl19.Location = new System.Drawing.Point(1, 294);
            this.groupControl19.Name = "groupControl19";
            this.groupControl19.Size = new System.Drawing.Size(291, 344);
            this.groupControl19.TabIndex = 184;
            this.groupControl19.Text = "手动调试选项";
            // 
            // 强制camera2结果
            // 
            this.强制camera2结果.Location = new System.Drawing.Point(13, 134);
            this.强制camera2结果.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.强制camera2结果.Name = "强制camera2结果";
            this.强制camera2结果.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.强制camera2结果.Properties.Appearance.Options.UseFont = true;
            this.强制camera2结果.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强制camera2结果.Properties.AppearanceDisabled.Options.UseFont = true;
            this.强制camera2结果.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强制camera2结果.Properties.AppearanceFocused.Options.UseFont = true;
            this.强制camera2结果.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强制camera2结果.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.强制camera2结果.Properties.Caption = "强制camera2结果";
            this.强制camera2结果.Size = new System.Drawing.Size(148, 21);
            this.强制camera2结果.TabIndex = 187;
            // 
            // 强制camera1结果
            // 
            this.强制camera1结果.Location = new System.Drawing.Point(13, 110);
            this.强制camera1结果.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.强制camera1结果.Name = "强制camera1结果";
            this.强制camera1结果.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.强制camera1结果.Properties.Appearance.Options.UseFont = true;
            this.强制camera1结果.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强制camera1结果.Properties.AppearanceDisabled.Options.UseFont = true;
            this.强制camera1结果.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强制camera1结果.Properties.AppearanceFocused.Options.UseFont = true;
            this.强制camera1结果.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强制camera1结果.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.强制camera1结果.Properties.Caption = "强制camera1结果";
            this.强制camera1结果.Size = new System.Drawing.Size(148, 21);
            this.强制camera1结果.TabIndex = 186;
            // 
            // chkEnableRobotIO
            // 
            this.chkEnableRobotIO.Location = new System.Drawing.Point(13, 37);
            this.chkEnableRobotIO.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkEnableRobotIO.Name = "chkEnableRobotIO";
            this.chkEnableRobotIO.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEnableRobotIO.Properties.Appearance.Options.UseFont = true;
            this.chkEnableRobotIO.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableRobotIO.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkEnableRobotIO.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableRobotIO.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkEnableRobotIO.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableRobotIO.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkEnableRobotIO.Properties.Caption = "启用机器人I/O监控";
            this.chkEnableRobotIO.Size = new System.Drawing.Size(148, 21);
            this.chkEnableRobotIO.TabIndex = 178;
            this.chkEnableRobotIO.CheckedChanged += new System.EventHandler(this.chkEnableRobotIO_CheckedChanged);
            // 
            // chkCareFlashResult
            // 
            this.chkCareFlashResult.EditValue = true;
            this.chkCareFlashResult.Location = new System.Drawing.Point(13, 86);
            this.chkCareFlashResult.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkCareFlashResult.Name = "chkCareFlashResult";
            this.chkCareFlashResult.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCareFlashResult.Properties.Appearance.Options.UseFont = true;
            this.chkCareFlashResult.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkCareFlashResult.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkCareFlashResult.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkCareFlashResult.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkCareFlashResult.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkCareFlashResult.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkCareFlashResult.Properties.Caption = "合并烧录测试结果";
            this.chkCareFlashResult.Size = new System.Drawing.Size(148, 21);
            this.chkCareFlashResult.TabIndex = 185;
            // 
            // chkVacuuSensor
            // 
            this.chkVacuuSensor.EditValue = true;
            this.chkVacuuSensor.Location = new System.Drawing.Point(13, 63);
            this.chkVacuuSensor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkVacuuSensor.Name = "chkVacuuSensor";
            this.chkVacuuSensor.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVacuuSensor.Properties.Appearance.Options.UseFont = true;
            this.chkVacuuSensor.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkVacuuSensor.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkVacuuSensor.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkVacuuSensor.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkVacuuSensor.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkVacuuSensor.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkVacuuSensor.Properties.Caption = "启用机器人真空吸嘴感应";
            this.chkVacuuSensor.Size = new System.Drawing.Size(174, 21);
            this.chkVacuuSensor.TabIndex = 184;
            // 
            // textEdit9
            // 
            this.textEdit9.EditValue = "";
            this.textEdit9.Location = new System.Drawing.Point(14, 202);
            this.textEdit9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.textEdit9.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.textEdit9.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseBorderColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit9.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.textEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.textEdit9.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.textEdit9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEdit9.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit9.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit9.Size = new System.Drawing.Size(260, 28);
            this.textEdit9.TabIndex = 215;
            // 
            // labelControl58
            // 
            this.labelControl58.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.Appearance.Options.UseFont = true;
            this.labelControl58.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.AppearanceDisabled.Options.UseFont = true;
            this.labelControl58.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.AppearanceHovered.Options.UseFont = true;
            this.labelControl58.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl58.AppearancePressed.Options.UseFont = true;
            this.labelControl58.Location = new System.Drawing.Point(17, 182);
            this.labelControl58.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(72, 17);
            this.labelControl58.TabIndex = 216;
            this.labelControl58.Text = "产品虚拟条码";
            // 
            // bmtUnbindTray2
            // 
            this.bmtUnbindTray2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bmtUnbindTray2.Appearance.Options.UseFont = true;
            this.bmtUnbindTray2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.bmtUnbindTray2.AppearanceDisabled.Options.UseFont = true;
            this.bmtUnbindTray2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.bmtUnbindTray2.AppearanceHovered.Options.UseFont = true;
            this.bmtUnbindTray2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.bmtUnbindTray2.AppearancePressed.Options.UseFont = true;
            this.bmtUnbindTray2.Location = new System.Drawing.Point(143, 229);
            this.bmtUnbindTray2.Name = "bmtUnbindTray2";
            this.bmtUnbindTray2.Size = new System.Drawing.Size(131, 48);
            this.bmtUnbindTray2.TabIndex = 214;
            this.bmtUnbindTray2.Text = "托盘解绑(完成下料)";
            this.bmtUnbindTray2.Click += new System.EventHandler(this.bmtUnbindTray2_Click);
            // 
            // groupControl8
            // 
            this.groupControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl8.Appearance.Options.UseFont = true;
            this.groupControl8.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl8.AppearanceCaption.Options.UseFont = true;
            this.groupControl8.Controls.Add(this.groupBox7);
            this.groupControl8.Controls.Add(this.groupBox6);
            this.groupControl8.Controls.Add(this.userLanternIO);
            this.groupControl8.Location = new System.Drawing.Point(879, 0);
            this.groupControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(560, 638);
            this.groupControl8.TabIndex = 168;
            this.groupControl8.Text = "I/O 输入输出";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox7.Controls.Add(this.LED_Out0);
            this.groupBox7.Controls.Add(this.LED_Out31);
            this.groupBox7.Controls.Add(this.LED_Out1);
            this.groupBox7.Controls.Add(this.X回原点);
            this.groupBox7.Controls.Add(this.Z回原点);
            this.groupBox7.Controls.Add(this.LED_Out30);
            this.groupBox7.Controls.Add(this.Y回原点);
            this.groupBox7.Controls.Add(this.LED_Out2);
            this.groupBox7.Controls.Add(this.R回原点);
            this.groupBox7.Controls.Add(this.LED_Out29);
            this.groupBox7.Controls.Add(this.LED_Out3);
            this.groupBox7.Controls.Add(this.LED_Out28);
            this.groupBox7.Controls.Add(this.LED_Out4);
            this.groupBox7.Controls.Add(this.LED_Out27);
            this.groupBox7.Controls.Add(this.LED_Out5);
            this.groupBox7.Controls.Add(this.LED_Out26);
            this.groupBox7.Controls.Add(this.LED_Out6);
            this.groupBox7.Controls.Add(this.LED_Out25);
            this.groupBox7.Controls.Add(this.LED_Out7);
            this.groupBox7.Controls.Add(this.LED_Out24);
            this.groupBox7.Controls.Add(this.LED_Out8);
            this.groupBox7.Controls.Add(this.LED_Out23);
            this.groupBox7.Controls.Add(this.LED_Out9);
            this.groupBox7.Controls.Add(this.LED_Out22);
            this.groupBox7.Controls.Add(this.LED_Out10);
            this.groupBox7.Controls.Add(this.LED_Out21);
            this.groupBox7.Controls.Add(this.LED_Out11);
            this.groupBox7.Controls.Add(this.LED_Out20);
            this.groupBox7.Controls.Add(this.LED_Out12);
            this.groupBox7.Controls.Add(this.LED_Out19);
            this.groupBox7.Controls.Add(this.LED_Out13);
            this.groupBox7.Controls.Add(this.LED_Out18);
            this.groupBox7.Controls.Add(this.LED_Out14);
            this.groupBox7.Controls.Add(this.LED_Out17);
            this.groupBox7.Controls.Add(this.LED_Out15);
            this.groupBox7.Controls.Add(this.LED_Out16);
            this.groupBox7.Location = new System.Drawing.Point(11, 298);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Size = new System.Drawing.Size(542, 335);
            this.groupBox7.TabIndex = 166;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " I/O 输出(Output)";
            // 
            // LED_Out0
            // 
            this.LED_Out0.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out0.Location = new System.Drawing.Point(15, 24);
            this.LED_Out0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out0.Name = "LED_Out0";
            this.LED_Out0.Size = new System.Drawing.Size(155, 39);
            this.LED_Out0.TabIndex = 132;
            this.LED_Out0.Text = "0 真空控制 1#";
            this.LED_Out0.UseVisualStyleBackColor = true;
            this.LED_Out0.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out31
            // 
            this.LED_Out31.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out31.Location = new System.Drawing.Point(583, 305);
            this.LED_Out31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out31.Name = "LED_Out31";
            this.LED_Out31.Size = new System.Drawing.Size(135, 39);
            this.LED_Out31.TabIndex = 163;
            this.LED_Out31.Text = "31 空";
            this.LED_Out31.UseVisualStyleBackColor = true;
            this.LED_Out31.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out1
            // 
            this.LED_Out1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out1.Location = new System.Drawing.Point(15, 63);
            this.LED_Out1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out1.Name = "LED_Out1";
            this.LED_Out1.Size = new System.Drawing.Size(155, 39);
            this.LED_Out1.TabIndex = 133;
            this.LED_Out1.Text = "1 真空控制 2#";
            this.LED_Out1.UseVisualStyleBackColor = true;
            this.LED_Out1.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // X回原点
            // 
            this.X回原点.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X回原点.Appearance.Options.UseFont = true;
            this.X回原点.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X回原点.AppearanceDisabled.Options.UseFont = true;
            this.X回原点.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X回原点.AppearanceHovered.Options.UseFont = true;
            this.X回原点.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X回原点.AppearancePressed.Options.UseFont = true;
            this.X回原点.Enabled = false;
            this.X回原点.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("X回原点.ImageOptions.Image")));
            this.X回原点.Location = new System.Drawing.Point(444, 15);
            this.X回原点.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.X回原点.Name = "X回原点";
            this.X回原点.Size = new System.Drawing.Size(92, 58);
            this.X回原点.TabIndex = 172;
            this.X回原点.Text = "X 回原点";
            this.X回原点.Visible = false;
            this.X回原点.Click += new System.EventHandler(this.X回原点_Click);
            // 
            // Z回原点
            // 
            this.Z回原点.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Z回原点.Appearance.Options.UseFont = true;
            this.Z回原点.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Z回原点.AppearanceDisabled.Options.UseFont = true;
            this.Z回原点.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Z回原点.AppearanceHovered.Options.UseFont = true;
            this.Z回原点.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Z回原点.AppearancePressed.Options.UseFont = true;
            this.Z回原点.Enabled = false;
            this.Z回原点.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Z回原点.ImageOptions.Image")));
            this.Z回原点.Location = new System.Drawing.Point(444, 146);
            this.Z回原点.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Z回原点.Name = "Z回原点";
            this.Z回原点.Size = new System.Drawing.Size(92, 58);
            this.Z回原点.TabIndex = 174;
            this.Z回原点.Text = "Z 回原点";
            this.Z回原点.Visible = false;
            this.Z回原点.Click += new System.EventHandler(this.Z回原点_Click);
            // 
            // LED_Out30
            // 
            this.LED_Out30.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out30.Location = new System.Drawing.Point(583, 265);
            this.LED_Out30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out30.Name = "LED_Out30";
            this.LED_Out30.Size = new System.Drawing.Size(135, 39);
            this.LED_Out30.TabIndex = 162;
            this.LED_Out30.Text = "30 空";
            this.LED_Out30.UseVisualStyleBackColor = true;
            this.LED_Out30.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // Y回原点
            // 
            this.Y回原点.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y回原点.Appearance.Options.UseFont = true;
            this.Y回原点.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y回原点.AppearanceDisabled.Options.UseFont = true;
            this.Y回原点.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y回原点.AppearanceHovered.Options.UseFont = true;
            this.Y回原点.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y回原点.AppearancePressed.Options.UseFont = true;
            this.Y回原点.Enabled = false;
            this.Y回原点.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Y回原点.ImageOptions.Image")));
            this.Y回原点.Location = new System.Drawing.Point(444, 80);
            this.Y回原点.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Y回原点.Name = "Y回原点";
            this.Y回原点.Size = new System.Drawing.Size(92, 58);
            this.Y回原点.TabIndex = 173;
            this.Y回原点.Text = "Y 回原点";
            this.Y回原点.Visible = false;
            this.Y回原点.Click += new System.EventHandler(this.Y回原点_Click);
            // 
            // LED_Out2
            // 
            this.LED_Out2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out2.Location = new System.Drawing.Point(15, 102);
            this.LED_Out2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out2.Name = "LED_Out2";
            this.LED_Out2.Size = new System.Drawing.Size(155, 39);
            this.LED_Out2.TabIndex = 134;
            this.LED_Out2.Text = "2 真空控制 3#";
            this.LED_Out2.UseVisualStyleBackColor = true;
            this.LED_Out2.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // R回原点
            // 
            this.R回原点.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.R回原点.Appearance.Options.UseFont = true;
            this.R回原点.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.R回原点.AppearanceDisabled.Options.UseFont = true;
            this.R回原点.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.R回原点.AppearanceHovered.Options.UseFont = true;
            this.R回原点.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.R回原点.AppearancePressed.Options.UseFont = true;
            this.R回原点.Enabled = false;
            this.R回原点.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("R回原点.ImageOptions.Image")));
            this.R回原点.Location = new System.Drawing.Point(444, 212);
            this.R回原点.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.R回原点.Name = "R回原点";
            this.R回原点.Size = new System.Drawing.Size(92, 58);
            this.R回原点.TabIndex = 175;
            this.R回原点.Text = "R回原点";
            this.R回原点.Visible = false;
            this.R回原点.Click += new System.EventHandler(this.R回原点_Click);
            // 
            // LED_Out29
            // 
            this.LED_Out29.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out29.Location = new System.Drawing.Point(583, 225);
            this.LED_Out29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out29.Name = "LED_Out29";
            this.LED_Out29.Size = new System.Drawing.Size(135, 39);
            this.LED_Out29.TabIndex = 161;
            this.LED_Out29.Text = "29 空";
            this.LED_Out29.UseVisualStyleBackColor = true;
            this.LED_Out29.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out3
            // 
            this.LED_Out3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out3.Location = new System.Drawing.Point(15, 141);
            this.LED_Out3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out3.Name = "LED_Out3";
            this.LED_Out3.Size = new System.Drawing.Size(155, 39);
            this.LED_Out3.TabIndex = 135;
            this.LED_Out3.Text = "3 真空控制 4#";
            this.LED_Out3.UseVisualStyleBackColor = true;
            this.LED_Out3.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out28
            // 
            this.LED_Out28.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out28.Location = new System.Drawing.Point(583, 185);
            this.LED_Out28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out28.Name = "LED_Out28";
            this.LED_Out28.Size = new System.Drawing.Size(135, 39);
            this.LED_Out28.TabIndex = 160;
            this.LED_Out28.Text = "28 空";
            this.LED_Out28.UseVisualStyleBackColor = true;
            this.LED_Out28.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out4
            // 
            this.LED_Out4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out4.Location = new System.Drawing.Point(15, 180);
            this.LED_Out4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out4.Name = "LED_Out4";
            this.LED_Out4.Size = new System.Drawing.Size(155, 39);
            this.LED_Out4.TabIndex = 136;
            this.LED_Out4.Text = "4 真空控制 5#";
            this.LED_Out4.UseVisualStyleBackColor = true;
            this.LED_Out4.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out27
            // 
            this.LED_Out27.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out27.Location = new System.Drawing.Point(583, 145);
            this.LED_Out27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out27.Name = "LED_Out27";
            this.LED_Out27.Size = new System.Drawing.Size(135, 39);
            this.LED_Out27.TabIndex = 159;
            this.LED_Out27.Text = "27 空";
            this.LED_Out27.UseVisualStyleBackColor = true;
            this.LED_Out27.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out5
            // 
            this.LED_Out5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out5.Location = new System.Drawing.Point(15, 219);
            this.LED_Out5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out5.Name = "LED_Out5";
            this.LED_Out5.Size = new System.Drawing.Size(155, 39);
            this.LED_Out5.TabIndex = 137;
            this.LED_Out5.Text = "5 真空控制 6#";
            this.LED_Out5.UseVisualStyleBackColor = true;
            this.LED_Out5.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out26
            // 
            this.LED_Out26.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out26.Location = new System.Drawing.Point(583, 104);
            this.LED_Out26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out26.Name = "LED_Out26";
            this.LED_Out26.Size = new System.Drawing.Size(135, 39);
            this.LED_Out26.TabIndex = 158;
            this.LED_Out26.Text = "26 空";
            this.LED_Out26.UseVisualStyleBackColor = true;
            this.LED_Out26.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out6
            // 
            this.LED_Out6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out6.Location = new System.Drawing.Point(15, 257);
            this.LED_Out6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out6.Name = "LED_Out6";
            this.LED_Out6.Size = new System.Drawing.Size(155, 39);
            this.LED_Out6.TabIndex = 138;
            this.LED_Out6.Text = "6 真空控制 7#";
            this.LED_Out6.UseVisualStyleBackColor = true;
            this.LED_Out6.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out25
            // 
            this.LED_Out25.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out25.Location = new System.Drawing.Point(583, 64);
            this.LED_Out25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out25.Name = "LED_Out25";
            this.LED_Out25.Size = new System.Drawing.Size(135, 39);
            this.LED_Out25.TabIndex = 157;
            this.LED_Out25.Text = "25 空";
            this.LED_Out25.UseVisualStyleBackColor = true;
            this.LED_Out25.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out7
            // 
            this.LED_Out7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out7.Location = new System.Drawing.Point(15, 296);
            this.LED_Out7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out7.Name = "LED_Out7";
            this.LED_Out7.Size = new System.Drawing.Size(155, 39);
            this.LED_Out7.TabIndex = 139;
            this.LED_Out7.Text = "7 空";
            this.LED_Out7.UseVisualStyleBackColor = true;
            this.LED_Out7.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out24
            // 
            this.LED_Out24.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out24.Location = new System.Drawing.Point(583, 24);
            this.LED_Out24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out24.Name = "LED_Out24";
            this.LED_Out24.Size = new System.Drawing.Size(135, 39);
            this.LED_Out24.TabIndex = 156;
            this.LED_Out24.Text = "24 空";
            this.LED_Out24.UseVisualStyleBackColor = true;
            this.LED_Out24.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out8
            // 
            this.LED_Out8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out8.Location = new System.Drawing.Point(189, 27);
            this.LED_Out8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out8.Name = "LED_Out8";
            this.LED_Out8.Size = new System.Drawing.Size(156, 39);
            this.LED_Out8.TabIndex = 140;
            this.LED_Out8.Text = "8 气缸控制 1#";
            this.LED_Out8.UseVisualStyleBackColor = true;
            this.LED_Out8.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out23
            // 
            this.LED_Out23.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out23.Location = new System.Drawing.Point(376, 299);
            this.LED_Out23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out23.Name = "LED_Out23";
            this.LED_Out23.Size = new System.Drawing.Size(166, 39);
            this.LED_Out23.TabIndex = 155;
            this.LED_Out23.Text = "23 空";
            this.LED_Out23.UseVisualStyleBackColor = true;
            this.LED_Out23.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out9
            // 
            this.LED_Out9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out9.Location = new System.Drawing.Point(189, 66);
            this.LED_Out9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out9.Name = "LED_Out9";
            this.LED_Out9.Size = new System.Drawing.Size(156, 39);
            this.LED_Out9.TabIndex = 141;
            this.LED_Out9.Text = "9 气缸控制 2#";
            this.LED_Out9.UseVisualStyleBackColor = true;
            this.LED_Out9.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out22
            // 
            this.LED_Out22.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out22.Location = new System.Drawing.Point(376, 260);
            this.LED_Out22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out22.Name = "LED_Out22";
            this.LED_Out22.Size = new System.Drawing.Size(166, 39);
            this.LED_Out22.TabIndex = 154;
            this.LED_Out22.Text = "22 照明灯";
            this.LED_Out22.UseVisualStyleBackColor = true;
            this.LED_Out22.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out10
            // 
            this.LED_Out10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out10.Location = new System.Drawing.Point(189, 104);
            this.LED_Out10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out10.Name = "LED_Out10";
            this.LED_Out10.Size = new System.Drawing.Size(156, 39);
            this.LED_Out10.TabIndex = 142;
            this.LED_Out10.Text = "10 气缸控制 3#";
            this.LED_Out10.UseVisualStyleBackColor = true;
            this.LED_Out10.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out21
            // 
            this.LED_Out21.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out21.Location = new System.Drawing.Point(376, 221);
            this.LED_Out21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out21.Name = "LED_Out21";
            this.LED_Out21.Size = new System.Drawing.Size(166, 39);
            this.LED_Out21.TabIndex = 153;
            this.LED_Out21.Text = "21 空";
            this.LED_Out21.UseVisualStyleBackColor = true;
            this.LED_Out21.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out11
            // 
            this.LED_Out11.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out11.Location = new System.Drawing.Point(189, 143);
            this.LED_Out11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out11.Name = "LED_Out11";
            this.LED_Out11.Size = new System.Drawing.Size(156, 39);
            this.LED_Out11.TabIndex = 143;
            this.LED_Out11.Text = "11 气缸控制 4#";
            this.LED_Out11.UseVisualStyleBackColor = true;
            this.LED_Out11.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out20
            // 
            this.LED_Out20.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out20.Location = new System.Drawing.Point(376, 182);
            this.LED_Out20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out20.Name = "LED_Out20";
            this.LED_Out20.Size = new System.Drawing.Size(166, 39);
            this.LED_Out20.TabIndex = 152;
            this.LED_Out20.Text = "20 空";
            this.LED_Out20.UseVisualStyleBackColor = true;
            this.LED_Out20.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out12
            // 
            this.LED_Out12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out12.Location = new System.Drawing.Point(189, 182);
            this.LED_Out12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out12.Name = "LED_Out12";
            this.LED_Out12.Size = new System.Drawing.Size(156, 39);
            this.LED_Out12.TabIndex = 144;
            this.LED_Out12.Text = "12 气缸控制 5#";
            this.LED_Out12.UseVisualStyleBackColor = true;
            this.LED_Out12.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out19
            // 
            this.LED_Out19.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out19.Location = new System.Drawing.Point(376, 143);
            this.LED_Out19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out19.Name = "LED_Out19";
            this.LED_Out19.Size = new System.Drawing.Size(166, 39);
            this.LED_Out19.TabIndex = 151;
            this.LED_Out19.Text = "19 空";
            this.LED_Out19.UseVisualStyleBackColor = true;
            this.LED_Out19.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out13
            // 
            this.LED_Out13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out13.Location = new System.Drawing.Point(189, 221);
            this.LED_Out13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out13.Name = "LED_Out13";
            this.LED_Out13.Size = new System.Drawing.Size(156, 39);
            this.LED_Out13.TabIndex = 145;
            this.LED_Out13.Text = "13 气缸控制 6#";
            this.LED_Out13.UseVisualStyleBackColor = true;
            this.LED_Out13.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out18
            // 
            this.LED_Out18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out18.Location = new System.Drawing.Point(376, 104);
            this.LED_Out18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out18.Name = "LED_Out18";
            this.LED_Out18.Size = new System.Drawing.Size(166, 39);
            this.LED_Out18.TabIndex = 150;
            this.LED_Out18.Text = "18 空";
            this.LED_Out18.UseVisualStyleBackColor = true;
            this.LED_Out18.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out14
            // 
            this.LED_Out14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out14.Location = new System.Drawing.Point(189, 260);
            this.LED_Out14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out14.Name = "LED_Out14";
            this.LED_Out14.Size = new System.Drawing.Size(156, 39);
            this.LED_Out14.TabIndex = 146;
            this.LED_Out14.Text = "14 气缸控制 7#";
            this.LED_Out14.UseVisualStyleBackColor = true;
            this.LED_Out14.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out17
            // 
            this.LED_Out17.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out17.Location = new System.Drawing.Point(376, 66);
            this.LED_Out17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out17.Name = "LED_Out17";
            this.LED_Out17.Size = new System.Drawing.Size(166, 39);
            this.LED_Out17.TabIndex = 149;
            this.LED_Out17.Text = "17 空";
            this.LED_Out17.UseVisualStyleBackColor = true;
            this.LED_Out17.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out15
            // 
            this.LED_Out15.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out15.Location = new System.Drawing.Point(189, 299);
            this.LED_Out15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out15.Name = "LED_Out15";
            this.LED_Out15.Size = new System.Drawing.Size(156, 39);
            this.LED_Out15.TabIndex = 147;
            this.LED_Out15.Text = "15 空";
            this.LED_Out15.UseVisualStyleBackColor = true;
            this.LED_Out15.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out16
            // 
            this.LED_Out16.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out16.Location = new System.Drawing.Point(376, 27);
            this.LED_Out16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out16.Name = "LED_Out16";
            this.LED_Out16.Size = new System.Drawing.Size(166, 39);
            this.LED_Out16.TabIndex = 148;
            this.LED_Out16.Text = "16 空";
            this.LED_Out16.UseVisualStyleBackColor = true;
            this.LED_Out16.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox6.Controls.Add(this.LED_In0);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label48);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label49);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.label50);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.label51);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label52);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.LED_In1);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.LED_In2);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.LED_In3);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.LED_In4);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this.LED_In5);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Controls.Add(this.LED_In6);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this.LED_In7);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.LED_In9);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.LED_In10);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.LED_In11);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.LED_In12);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.LED_In13);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.LED_In14);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.LED_In15);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.LED_In8);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.LED_In17);
            this.groupBox6.Controls.Add(this.LED_In24);
            this.groupBox6.Controls.Add(this.LED_In18);
            this.groupBox6.Controls.Add(this.LED_In31);
            this.groupBox6.Controls.Add(this.LED_In19);
            this.groupBox6.Controls.Add(this.LED_In30);
            this.groupBox6.Controls.Add(this.LED_In20);
            this.groupBox6.Controls.Add(this.LED_In29);
            this.groupBox6.Controls.Add(this.LED_In21);
            this.groupBox6.Controls.Add(this.LED_In28);
            this.groupBox6.Controls.Add(this.LED_In22);
            this.groupBox6.Controls.Add(this.LED_In27);
            this.groupBox6.Controls.Add(this.LED_In23);
            this.groupBox6.Controls.Add(this.LED_In26);
            this.groupBox6.Controls.Add(this.LED_In16);
            this.groupBox6.Controls.Add(this.LED_In25);
            this.groupBox6.Location = new System.Drawing.Point(11, 30);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(542, 265);
            this.groupBox6.TabIndex = 165;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "I/O 输入(Input)";
            // 
            // LED_In0
            // 
            this.LED_In0.BackColor = System.Drawing.Color.Transparent;
            this.LED_In0.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In0.Location = new System.Drawing.Point(15, 27);
            this.LED_In0.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In0.Name = "LED_In0";
            this.LED_In0.Size = new System.Drawing.Size(22, 28);
            this.LED_In0.TabIndex = 82;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(607, 227);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(38, 17);
            this.label45.TabIndex = 130;
            this.label45.Text = "31 空";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 57);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 17);
            this.label13.TabIndex = 42;
            this.label13.Text = "1 空";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(607, 199);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(38, 17);
            this.label46.TabIndex = 129;
            this.label46.Text = "30 空";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(39, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 17);
            this.label14.TabIndex = 43;
            this.label14.Text = "2 空";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(607, 171);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(38, 17);
            this.label47.TabIndex = 128;
            this.label47.Text = "29 空";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(39, 113);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 17);
            this.label15.TabIndex = 44;
            this.label15.Text = "3 空";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(607, 143);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(38, 17);
            this.label48.TabIndex = 127;
            this.label48.Text = "28 空";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(39, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(46, 17);
            this.label16.TabIndex = 45;
            this.label16.Text = "4 Stop";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(607, 115);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(38, 17);
            this.label49.TabIndex = 126;
            this.label49.Text = "27 空";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(39, 169);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 17);
            this.label18.TabIndex = 46;
            this.label18.Text = "5 Pause";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(607, 87);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(38, 17);
            this.label50.TabIndex = 125;
            this.label50.Text = "26 空";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(39, 197);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 17);
            this.label19.TabIndex = 47;
            this.label19.Text = "6 Continue";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(607, 60);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(38, 17);
            this.label51.TabIndex = 124;
            this.label51.Text = "25 空";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(39, 225);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(51, 17);
            this.label20.TabIndex = 48;
            this.label20.Text = "7 Reset";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(607, 32);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(38, 17);
            this.label52.TabIndex = 123;
            this.label52.Text = "24 空";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(39, 29);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(46, 17);
            this.label27.TabIndex = 59;
            this.label27.Tag = "0";
            this.label27.Text = "0 Start";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(399, 227);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(38, 17);
            this.label37.TabIndex = 122;
            this.label37.Text = "23 空";
            // 
            // LED_In1
            // 
            this.LED_In1.BackColor = System.Drawing.Color.Transparent;
            this.LED_In1.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In1.Location = new System.Drawing.Point(15, 56);
            this.LED_In1.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In1.Name = "LED_In1";
            this.LED_In1.Size = new System.Drawing.Size(22, 28);
            this.LED_In1.TabIndex = 75;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(399, 199);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(38, 17);
            this.label38.TabIndex = 121;
            this.label38.Text = "22 空";
            // 
            // LED_In2
            // 
            this.LED_In2.BackColor = System.Drawing.Color.Transparent;
            this.LED_In2.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In2.Location = new System.Drawing.Point(15, 83);
            this.LED_In2.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In2.Name = "LED_In2";
            this.LED_In2.Size = new System.Drawing.Size(22, 28);
            this.LED_In2.TabIndex = 76;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(399, 171);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(109, 17);
            this.label39.TabIndex = 120;
            this.label39.Text = "21 空 气缸原位 7#";
            // 
            // LED_In3
            // 
            this.LED_In3.BackColor = System.Drawing.Color.Transparent;
            this.LED_In3.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In3.Location = new System.Drawing.Point(15, 111);
            this.LED_In3.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In3.Name = "LED_In3";
            this.LED_In3.Size = new System.Drawing.Size(22, 28);
            this.LED_In3.TabIndex = 77;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(399, 143);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(109, 17);
            this.label40.TabIndex = 119;
            this.label40.Text = "20 空 气缸原位 6#";
            // 
            // LED_In4
            // 
            this.LED_In4.BackColor = System.Drawing.Color.Transparent;
            this.LED_In4.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In4.Location = new System.Drawing.Point(15, 138);
            this.LED_In4.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In4.Name = "LED_In4";
            this.LED_In4.Size = new System.Drawing.Size(22, 28);
            this.LED_In4.TabIndex = 78;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(399, 115);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(109, 17);
            this.label41.TabIndex = 118;
            this.label41.Text = "19 空 气缸原位 5#";
            // 
            // LED_In5
            // 
            this.LED_In5.BackColor = System.Drawing.Color.Transparent;
            this.LED_In5.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In5.Location = new System.Drawing.Point(15, 165);
            this.LED_In5.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In5.Name = "LED_In5";
            this.LED_In5.Size = new System.Drawing.Size(22, 28);
            this.LED_In5.TabIndex = 79;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(399, 87);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(109, 17);
            this.label42.TabIndex = 117;
            this.label42.Text = "18 空 气缸原位 4#";
            // 
            // LED_In6
            // 
            this.LED_In6.BackColor = System.Drawing.Color.Transparent;
            this.LED_In6.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In6.Location = new System.Drawing.Point(15, 194);
            this.LED_In6.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In6.Name = "LED_In6";
            this.LED_In6.Size = new System.Drawing.Size(22, 28);
            this.LED_In6.TabIndex = 80;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(399, 60);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(109, 17);
            this.label43.TabIndex = 116;
            this.label43.Text = "17 空 气缸原位 3#";
            // 
            // LED_In7
            // 
            this.LED_In7.BackColor = System.Drawing.Color.Transparent;
            this.LED_In7.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In7.Location = new System.Drawing.Point(15, 222);
            this.LED_In7.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In7.Name = "LED_In7";
            this.LED_In7.Size = new System.Drawing.Size(22, 28);
            this.LED_In7.TabIndex = 81;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(399, 32);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(109, 17);
            this.label44.TabIndex = 115;
            this.label44.Text = "16 空 气缸原位 2#";
            // 
            // LED_In9
            // 
            this.LED_In9.BackColor = System.Drawing.Color.Transparent;
            this.LED_In9.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In9.Location = new System.Drawing.Point(182, 58);
            this.LED_In9.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In9.Name = "LED_In9";
            this.LED_In9.Size = new System.Drawing.Size(22, 28);
            this.LED_In9.TabIndex = 83;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(205, 227);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(109, 17);
            this.label26.TabIndex = 114;
            this.label26.Text = "15 空 气缸原位 1#";
            // 
            // LED_In10
            // 
            this.LED_In10.BackColor = System.Drawing.Color.Transparent;
            this.LED_In10.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In10.Location = new System.Drawing.Point(182, 85);
            this.LED_In10.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In10.Name = "LED_In10";
            this.LED_In10.Size = new System.Drawing.Size(22, 28);
            this.LED_In10.TabIndex = 84;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(205, 199);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(93, 17);
            this.label30.TabIndex = 113;
            this.label30.Text = "14 真空到位 7#";
            // 
            // LED_In11
            // 
            this.LED_In11.BackColor = System.Drawing.Color.Transparent;
            this.LED_In11.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In11.Location = new System.Drawing.Point(182, 113);
            this.LED_In11.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In11.Name = "LED_In11";
            this.LED_In11.Size = new System.Drawing.Size(22, 28);
            this.LED_In11.TabIndex = 85;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(205, 171);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(93, 17);
            this.label31.TabIndex = 112;
            this.label31.Text = "13 真空到位 6#";
            // 
            // LED_In12
            // 
            this.LED_In12.BackColor = System.Drawing.Color.Transparent;
            this.LED_In12.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In12.Location = new System.Drawing.Point(182, 141);
            this.LED_In12.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In12.Name = "LED_In12";
            this.LED_In12.Size = new System.Drawing.Size(22, 28);
            this.LED_In12.TabIndex = 86;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(205, 143);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(93, 17);
            this.label32.TabIndex = 111;
            this.label32.Text = "12 真空到位 5#";
            // 
            // LED_In13
            // 
            this.LED_In13.BackColor = System.Drawing.Color.Transparent;
            this.LED_In13.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In13.Location = new System.Drawing.Point(182, 168);
            this.LED_In13.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In13.Name = "LED_In13";
            this.LED_In13.Size = new System.Drawing.Size(22, 28);
            this.LED_In13.TabIndex = 87;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(205, 115);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(89, 17);
            this.label33.TabIndex = 110;
            this.label33.Text = "11真空到位 4#";
            // 
            // LED_In14
            // 
            this.LED_In14.BackColor = System.Drawing.Color.Transparent;
            this.LED_In14.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In14.Location = new System.Drawing.Point(182, 197);
            this.LED_In14.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In14.Name = "LED_In14";
            this.LED_In14.Size = new System.Drawing.Size(22, 28);
            this.LED_In14.TabIndex = 88;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(205, 87);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(93, 17);
            this.label34.TabIndex = 109;
            this.label34.Text = "10 真空到位 3#";
            // 
            // LED_In15
            // 
            this.LED_In15.BackColor = System.Drawing.Color.Transparent;
            this.LED_In15.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In15.Location = new System.Drawing.Point(182, 225);
            this.LED_In15.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In15.Name = "LED_In15";
            this.LED_In15.Size = new System.Drawing.Size(22, 28);
            this.LED_In15.TabIndex = 89;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(205, 60);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(86, 17);
            this.label35.TabIndex = 108;
            this.label35.Text = "9 真空到位 2#";
            // 
            // LED_In8
            // 
            this.LED_In8.BackColor = System.Drawing.Color.Transparent;
            this.LED_In8.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In8.Location = new System.Drawing.Point(182, 29);
            this.LED_In8.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In8.Name = "LED_In8";
            this.LED_In8.Size = new System.Drawing.Size(22, 28);
            this.LED_In8.TabIndex = 90;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(205, 32);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(86, 17);
            this.label36.TabIndex = 107;
            this.label36.Text = "8 真空到位 1#";
            // 
            // LED_In17
            // 
            this.LED_In17.BackColor = System.Drawing.Color.Transparent;
            this.LED_In17.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In17.Location = new System.Drawing.Point(376, 58);
            this.LED_In17.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In17.Name = "LED_In17";
            this.LED_In17.Size = new System.Drawing.Size(22, 28);
            this.LED_In17.TabIndex = 91;
            // 
            // LED_In24
            // 
            this.LED_In24.BackColor = System.Drawing.Color.Transparent;
            this.LED_In24.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In24.Location = new System.Drawing.Point(583, 29);
            this.LED_In24.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In24.Name = "LED_In24";
            this.LED_In24.Size = new System.Drawing.Size(22, 28);
            this.LED_In24.TabIndex = 106;
            // 
            // LED_In18
            // 
            this.LED_In18.BackColor = System.Drawing.Color.Transparent;
            this.LED_In18.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In18.Location = new System.Drawing.Point(376, 85);
            this.LED_In18.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In18.Name = "LED_In18";
            this.LED_In18.Size = new System.Drawing.Size(22, 28);
            this.LED_In18.TabIndex = 92;
            // 
            // LED_In31
            // 
            this.LED_In31.BackColor = System.Drawing.Color.Transparent;
            this.LED_In31.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In31.Location = new System.Drawing.Point(583, 225);
            this.LED_In31.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In31.Name = "LED_In31";
            this.LED_In31.Size = new System.Drawing.Size(22, 28);
            this.LED_In31.TabIndex = 105;
            // 
            // LED_In19
            // 
            this.LED_In19.BackColor = System.Drawing.Color.Transparent;
            this.LED_In19.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In19.Location = new System.Drawing.Point(376, 113);
            this.LED_In19.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In19.Name = "LED_In19";
            this.LED_In19.Size = new System.Drawing.Size(22, 28);
            this.LED_In19.TabIndex = 93;
            // 
            // LED_In30
            // 
            this.LED_In30.BackColor = System.Drawing.Color.Transparent;
            this.LED_In30.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In30.Location = new System.Drawing.Point(583, 197);
            this.LED_In30.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In30.Name = "LED_In30";
            this.LED_In30.Size = new System.Drawing.Size(22, 28);
            this.LED_In30.TabIndex = 104;
            // 
            // LED_In20
            // 
            this.LED_In20.BackColor = System.Drawing.Color.Transparent;
            this.LED_In20.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In20.Location = new System.Drawing.Point(376, 141);
            this.LED_In20.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In20.Name = "LED_In20";
            this.LED_In20.Size = new System.Drawing.Size(22, 28);
            this.LED_In20.TabIndex = 94;
            // 
            // LED_In29
            // 
            this.LED_In29.BackColor = System.Drawing.Color.Transparent;
            this.LED_In29.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In29.Location = new System.Drawing.Point(583, 168);
            this.LED_In29.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In29.Name = "LED_In29";
            this.LED_In29.Size = new System.Drawing.Size(22, 28);
            this.LED_In29.TabIndex = 103;
            // 
            // LED_In21
            // 
            this.LED_In21.BackColor = System.Drawing.Color.Transparent;
            this.LED_In21.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In21.Location = new System.Drawing.Point(376, 168);
            this.LED_In21.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In21.Name = "LED_In21";
            this.LED_In21.Size = new System.Drawing.Size(22, 28);
            this.LED_In21.TabIndex = 95;
            // 
            // LED_In28
            // 
            this.LED_In28.BackColor = System.Drawing.Color.Transparent;
            this.LED_In28.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In28.Location = new System.Drawing.Point(583, 141);
            this.LED_In28.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In28.Name = "LED_In28";
            this.LED_In28.Size = new System.Drawing.Size(22, 28);
            this.LED_In28.TabIndex = 102;
            // 
            // LED_In22
            // 
            this.LED_In22.BackColor = System.Drawing.Color.Transparent;
            this.LED_In22.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In22.Location = new System.Drawing.Point(376, 197);
            this.LED_In22.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In22.Name = "LED_In22";
            this.LED_In22.Size = new System.Drawing.Size(22, 28);
            this.LED_In22.TabIndex = 96;
            // 
            // LED_In27
            // 
            this.LED_In27.BackColor = System.Drawing.Color.Transparent;
            this.LED_In27.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In27.Location = new System.Drawing.Point(583, 113);
            this.LED_In27.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In27.Name = "LED_In27";
            this.LED_In27.Size = new System.Drawing.Size(22, 28);
            this.LED_In27.TabIndex = 101;
            // 
            // LED_In23
            // 
            this.LED_In23.BackColor = System.Drawing.Color.Transparent;
            this.LED_In23.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In23.Location = new System.Drawing.Point(376, 225);
            this.LED_In23.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In23.Name = "LED_In23";
            this.LED_In23.Size = new System.Drawing.Size(22, 28);
            this.LED_In23.TabIndex = 97;
            // 
            // LED_In26
            // 
            this.LED_In26.BackColor = System.Drawing.Color.Transparent;
            this.LED_In26.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In26.Location = new System.Drawing.Point(583, 85);
            this.LED_In26.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In26.Name = "LED_In26";
            this.LED_In26.Size = new System.Drawing.Size(22, 28);
            this.LED_In26.TabIndex = 100;
            // 
            // LED_In16
            // 
            this.LED_In16.BackColor = System.Drawing.Color.Transparent;
            this.LED_In16.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In16.Location = new System.Drawing.Point(376, 29);
            this.LED_In16.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In16.Name = "LED_In16";
            this.LED_In16.Size = new System.Drawing.Size(22, 28);
            this.LED_In16.TabIndex = 98;
            // 
            // LED_In25
            // 
            this.LED_In25.BackColor = System.Drawing.Color.Transparent;
            this.LED_In25.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In25.Location = new System.Drawing.Point(583, 58);
            this.LED_In25.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In25.Name = "LED_In25";
            this.LED_In25.Size = new System.Drawing.Size(22, 28);
            this.LED_In25.TabIndex = 99;
            // 
            // userLanternIO
            // 
            this.userLanternIO.BackColor = System.Drawing.Color.Transparent;
            this.userLanternIO.LanternBackground = System.Drawing.Color.Gray;
            this.userLanternIO.Location = new System.Drawing.Point(89, 0);
            this.userLanternIO.Margin = new System.Windows.Forms.Padding(3, 40, 3, 40);
            this.userLanternIO.Name = "userLanternIO";
            this.userLanternIO.Size = new System.Drawing.Size(22, 27);
            this.userLanternIO.TabIndex = 165;
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.强行清料并包装模式);
            this.groupControl4.Controls.Add(this.labelControl40);
            this.groupControl4.Controls.Add(this.labelControl39);
            this.groupControl4.Controls.Add(this.userLantern3);
            this.groupControl4.Controls.Add(this.LEDLasetInvoke);
            this.groupControl4.Controls.Add(this.labelControl34);
            this.groupControl4.Controls.Add(this.txtWorkOrder);
            this.groupControl4.Controls.Add(this.btm手动切换型号);
            this.groupControl4.Controls.Add(this.下料数据交互触发);
            this.groupControl4.Controls.Add(this.btm清料);
            this.groupControl4.Controls.Add(this.相机1拍照触发);
            this.groupControl4.Controls.Add(this.下料组盘触发);
            this.groupControl4.Controls.Add(this.PLC设备安信号);
            this.groupControl4.Controls.Add(this.下料盘已满);
            this.groupControl4.Location = new System.Drawing.Point(298, 294);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(575, 344);
            this.groupControl4.TabIndex = 100;
            this.groupControl4.Text = "手动操作";
            // 
            // 强行清料并包装模式
            // 
            this.强行清料并包装模式.Location = new System.Drawing.Point(290, 94);
            this.强行清料并包装模式.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.强行清料并包装模式.Name = "强行清料并包装模式";
            this.强行清料并包装模式.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.强行清料并包装模式.Properties.Appearance.Options.UseFont = true;
            this.强行清料并包装模式.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强行清料并包装模式.Properties.AppearanceDisabled.Options.UseFont = true;
            this.强行清料并包装模式.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强行清料并包装模式.Properties.AppearanceFocused.Options.UseFont = true;
            this.强行清料并包装模式.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.强行清料并包装模式.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.强行清料并包装模式.Properties.Caption = "强行清料并包装模式";
            this.强行清料并包装模式.Size = new System.Drawing.Size(141, 21);
            this.强行清料并包装模式.TabIndex = 225;
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.AppearanceDisabled.Options.UseFont = true;
            this.labelControl40.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.AppearanceHovered.Options.UseFont = true;
            this.labelControl40.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl40.AppearancePressed.Options.UseFont = true;
            this.labelControl40.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl40.Location = new System.Drawing.Point(464, 184);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(88, 30);
            this.labelControl40.TabIndex = 224;
            this.labelControl40.Text = "尾料模式(分拣)";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.AppearanceDisabled.Options.UseFont = true;
            this.labelControl39.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.AppearanceHovered.Options.UseFont = true;
            this.labelControl39.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl39.AppearancePressed.Options.UseFont = true;
            this.labelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl39.Location = new System.Drawing.Point(464, 89);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(88, 30);
            this.labelControl39.TabIndex = 223;
            this.labelControl39.Text = "尾料模式(调度)";
            // 
            // userLantern3
            // 
            this.userLantern3.BackColor = System.Drawing.Color.Transparent;
            this.userLantern3.LanternBackground = System.Drawing.Color.Gray;
            this.userLantern3.Location = new System.Drawing.Point(484, 138);
            this.userLantern3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.userLantern3.Name = "userLantern3";
            this.userLantern3.Size = new System.Drawing.Size(32, 32);
            this.userLantern3.TabIndex = 222;
            // 
            // LEDLasetInvoke
            // 
            this.LEDLasetInvoke.BackColor = System.Drawing.Color.Transparent;
            this.LEDLasetInvoke.LanternBackground = System.Drawing.Color.Gray;
            this.LEDLasetInvoke.Location = new System.Drawing.Point(484, 39);
            this.LEDLasetInvoke.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LEDLasetInvoke.Name = "LEDLasetInvoke";
            this.LEDLasetInvoke.Size = new System.Drawing.Size(32, 32);
            this.LEDLasetInvoke.TabIndex = 221;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.labelControl34.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Appearance.Options.UseBackColor = true;
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl34.AppearanceDisabled.Options.UseFont = true;
            this.labelControl34.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl34.AppearanceHovered.Options.UseFont = true;
            this.labelControl34.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl34.AppearancePressed.Options.UseFont = true;
            this.labelControl34.Location = new System.Drawing.Point(13, 231);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(301, 17);
            this.labelControl34.TabIndex = 220;
            this.labelControl34.Text = "产品型号/打码模板字符串(请参考调度打码切割工艺数据)";
            // 
            // txtWorkOrder
            // 
            this.txtWorkOrder.EditValue = "545XL";
            this.txtWorkOrder.Location = new System.Drawing.Point(320, 228);
            this.txtWorkOrder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtWorkOrder.Name = "txtWorkOrder";
            this.txtWorkOrder.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWorkOrder.Properties.Appearance.Options.UseFont = true;
            this.txtWorkOrder.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkOrder.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtWorkOrder.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkOrder.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtWorkOrder.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtWorkOrder.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtWorkOrder.Size = new System.Drawing.Size(102, 24);
            this.txtWorkOrder.TabIndex = 219;
            // 
            // btm手动切换型号
            // 
            this.btm手动切换型号.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btm手动切换型号.Appearance.Options.UseFont = true;
            this.btm手动切换型号.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm手动切换型号.AppearanceDisabled.Options.UseFont = true;
            this.btm手动切换型号.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm手动切换型号.AppearanceHovered.Options.UseFont = true;
            this.btm手动切换型号.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btm手动切换型号.AppearancePressed.Options.UseFont = true;
            this.btm手动切换型号.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btm手动切换型号.Location = new System.Drawing.Point(13, 253);
            this.btm手动切换型号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btm手动切换型号.Name = "btm手动切换型号";
            this.btm手动切换型号.Size = new System.Drawing.Size(409, 82);
            this.btm手动切换型号.TabIndex = 218;
            this.btm手动切换型号.Text = "手动切换型号";
            this.btm手动切换型号.Click += new System.EventHandler(this.btm手动切换型号_Click);
            // 
            // 下料数据交互触发
            // 
            this.下料数据交互触发.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.下料数据交互触发.Appearance.Options.UseFont = true;
            this.下料数据交互触发.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料数据交互触发.AppearanceDisabled.Options.UseFont = true;
            this.下料数据交互触发.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料数据交互触发.AppearanceHovered.Options.UseFont = true;
            this.下料数据交互触发.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料数据交互触发.AppearancePressed.Options.UseFont = true;
            this.下料数据交互触发.Location = new System.Drawing.Point(290, 41);
            this.下料数据交互触发.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.下料数据交互触发.Name = "下料数据交互触发";
            this.下料数据交互触发.Size = new System.Drawing.Size(132, 39);
            this.下料数据交互触发.TabIndex = 217;
            this.下料数据交互触发.Text = "下料数据交互触发";
            this.下料数据交互触发.Click += new System.EventHandler(this.下料数据交互触发_Click);
            // 
            // btm清料
            // 
            this.btm清料.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btm清料.Appearance.Options.UseFont = true;
            this.btm清料.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btm清料.AppearanceDisabled.Options.UseFont = true;
            this.btm清料.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btm清料.AppearanceHovered.Options.UseFont = true;
            this.btm清料.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btm清料.AppearancePressed.Options.UseFont = true;
            this.btm清料.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btm清料.Location = new System.Drawing.Point(13, 138);
            this.btm清料.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btm清料.Name = "btm清料";
            this.btm清料.Size = new System.Drawing.Size(409, 80);
            this.btm清料.TabIndex = 183;
            this.btm清料.Text = "清料 (请确保在尾盘模式或手动进入尾盘模式)";
            this.btm清料.Click += new System.EventHandler(this.btm清料_Click);
            // 
            // 相机1拍照触发
            // 
            this.相机1拍照触发.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.相机1拍照触发.Appearance.Options.UseFont = true;
            this.相机1拍照触发.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.相机1拍照触发.AppearanceDisabled.Options.UseFont = true;
            this.相机1拍照触发.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.相机1拍照触发.AppearanceHovered.Options.UseFont = true;
            this.相机1拍照触发.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.相机1拍照触发.AppearancePressed.Options.UseFont = true;
            this.相机1拍照触发.Location = new System.Drawing.Point(152, 84);
            this.相机1拍照触发.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.相机1拍照触发.Name = "相机1拍照触发";
            this.相机1拍照触发.Size = new System.Drawing.Size(132, 39);
            this.相机1拍照触发.TabIndex = 182;
            this.相机1拍照触发.Text = "相机1拍照触发";
            this.相机1拍照触发.Click += new System.EventHandler(this.相机1拍照触发_Click);
            // 
            // 下料组盘触发
            // 
            this.下料组盘触发.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.下料组盘触发.Appearance.Options.UseFont = true;
            this.下料组盘触发.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料组盘触发.AppearanceDisabled.Options.UseFont = true;
            this.下料组盘触发.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料组盘触发.AppearanceHovered.Options.UseFont = true;
            this.下料组盘触发.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料组盘触发.AppearancePressed.Options.UseFont = true;
            this.下料组盘触发.Location = new System.Drawing.Point(152, 41);
            this.下料组盘触发.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.下料组盘触发.Name = "下料组盘触发";
            this.下料组盘触发.Size = new System.Drawing.Size(132, 39);
            this.下料组盘触发.TabIndex = 181;
            this.下料组盘触发.Text = "下料组盘触发";
            this.下料组盘触发.Click += new System.EventHandler(this.下料组盘触发_Click);
            // 
            // PLC设备安信号
            // 
            this.PLC设备安信号.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PLC设备安信号.Appearance.Options.UseFont = true;
            this.PLC设备安信号.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PLC设备安信号.AppearanceDisabled.Options.UseFont = true;
            this.PLC设备安信号.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PLC设备安信号.AppearanceHovered.Options.UseFont = true;
            this.PLC设备安信号.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.PLC设备安信号.AppearancePressed.Options.UseFont = true;
            this.PLC设备安信号.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.PLC设备安信号.Location = new System.Drawing.Point(13, 84);
            this.PLC设备安信号.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PLC设备安信号.Name = "PLC设备安信号";
            this.PLC设备安信号.Size = new System.Drawing.Size(132, 39);
            this.PLC设备安信号.TabIndex = 180;
            this.PLC设备安信号.Text = "PLC设备安信号";
            this.PLC设备安信号.CheckedChanged += new System.EventHandler(this.PLC设备安信号_CheckedChanged);
            // 
            // 下料盘已满
            // 
            this.下料盘已满.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.下料盘已满.Appearance.Options.UseFont = true;
            this.下料盘已满.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料盘已满.AppearanceDisabled.Options.UseFont = true;
            this.下料盘已满.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料盘已满.AppearanceHovered.Options.UseFont = true;
            this.下料盘已满.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料盘已满.AppearancePressed.Options.UseFont = true;
            this.下料盘已满.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.下料盘已满.Location = new System.Drawing.Point(13, 41);
            this.下料盘已满.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.下料盘已满.Name = "下料盘已满";
            this.下料盘已满.Size = new System.Drawing.Size(132, 39);
            this.下料盘已满.TabIndex = 179;
            this.下料盘已满.Text = "下料盘已满";
            this.下料盘已满.CheckedChanged += new System.EventHandler(this.下料盘已满_CheckedChanged);
            // 
            // groupControl5
            // 
            this.groupControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.Appearance.Options.UseFont = true;
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.Controls.Add(this.textEdit1);
            this.groupControl5.Controls.Add(this.labelControl25);
            this.groupControl5.Controls.Add(this.textEdit2);
            this.groupControl5.Controls.Add(this.labelControl26);
            this.groupControl5.Controls.Add(this.textEdit3);
            this.groupControl5.Controls.Add(this.labelControl27);
            this.groupControl5.Controls.Add(this.textEdit4);
            this.groupControl5.Controls.Add(this.labelControl28);
            this.groupControl5.Controls.Add(this.textEdit5);
            this.groupControl5.Controls.Add(this.textEdit6);
            this.groupControl5.Controls.Add(this.labelControl29);
            this.groupControl5.Controls.Add(this.labelControl30);
            this.groupControl5.Controls.Add(this.textEdit7);
            this.groupControl5.Controls.Add(this.labelControl31);
            this.groupControl5.Controls.Add(this.textEdit8);
            this.groupControl5.Controls.Add(this.labelControl32);
            this.groupControl5.Controls.Add(this.触发1303);
            this.groupControl5.Controls.Add(this.txtD1013);
            this.groupControl5.Controls.Add(this.txtD1000);
            this.groupControl5.Controls.Add(this.labelControl18);
            this.groupControl5.Controls.Add(this.labelControl14);
            this.groupControl5.Controls.Add(this.txtD1007);
            this.groupControl5.Controls.Add(this.txtD1008);
            this.groupControl5.Controls.Add(this.txtD1010);
            this.groupControl5.Controls.Add(this.labelControl19);
            this.groupControl5.Controls.Add(this.labelControl13);
            this.groupControl5.Controls.Add(this.labelControl9);
            this.groupControl5.Controls.Add(this.txtD1003);
            this.groupControl5.Controls.Add(this.labelControl17);
            this.groupControl5.Controls.Add(this.txtD1009);
            this.groupControl5.Controls.Add(this.txtD1002);
            this.groupControl5.Controls.Add(this.labelControl10);
            this.groupControl5.Controls.Add(this.labelControl11);
            this.groupControl5.Controls.Add(this.txtD1012);
            this.groupControl5.Controls.Add(this.txtD1004);
            this.groupControl5.Controls.Add(this.txtD1011);
            this.groupControl5.Controls.Add(this.labelControl12);
            this.groupControl5.Controls.Add(this.txtD1001);
            this.groupControl5.Controls.Add(this.labelControl15);
            this.groupControl5.Controls.Add(this.labelControl16);
            this.groupControl5.Controls.Add(this.labelControl20);
            this.groupControl5.Controls.Add(this.txtD1005);
            this.groupControl5.Controls.Add(this.labelControl21);
            this.groupControl5.Controls.Add(this.txtD1006);
            this.groupControl5.Controls.Add(this.labelControl22);
            this.groupControl5.Location = new System.Drawing.Point(1445, 0);
            this.groupControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(236, 638);
            this.groupControl5.TabIndex = 98;
            this.groupControl5.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "3";
            this.textEdit1.Location = new System.Drawing.Point(150, 415);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit1.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit1.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(37, 22);
            this.textEdit1.TabIndex = 158;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.Options.UseBackColor = true;
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl25.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl25.Location = new System.Drawing.Point(80, 417);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(65, 17);
            this.labelControl25.TabIndex = 173;
            this.labelControl25.Text = "D1000 心跳";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(150, 597);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(37, 22);
            this.textEdit2.TabIndex = 165;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl26.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl26.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl26.Location = new System.Drawing.Point(80, 521);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(65, 17);
            this.labelControl26.TabIndex = 169;
            this.labelControl26.Text = "D1004 复位";
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(150, 493);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit3.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit3.Properties.ReadOnly = true;
            this.textEdit3.Size = new System.Drawing.Size(37, 22);
            this.textEdit3.TabIndex = 161;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl27.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Appearance.Options.UseBackColor = true;
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl27.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl27.Location = new System.Drawing.Point(27, 443);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(118, 17);
            this.labelControl27.TabIndex = 166;
            this.labelControl27.Text = "D1001 禁用/启用工位";
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(150, 467);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit4.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit4.Properties.ReadOnly = true;
            this.textEdit4.Size = new System.Drawing.Size(37, 22);
            this.textEdit4.TabIndex = 160;
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl28.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.Options.UseBackColor = true;
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl28.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl28.Location = new System.Drawing.Point(80, 547);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(65, 17);
            this.labelControl28.TabIndex = 170;
            this.labelControl28.Text = "D1005 停止";
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(150, 519);
            this.textEdit5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit5.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit5.Properties.ReadOnly = true;
            this.textEdit5.Size = new System.Drawing.Size(37, 22);
            this.textEdit5.TabIndex = 162;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(150, 441);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit6.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit6.Properties.ReadOnly = true;
            this.textEdit6.Size = new System.Drawing.Size(37, 22);
            this.textEdit6.TabIndex = 159;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl29.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.Options.UseBackColor = true;
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl29.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl29.Location = new System.Drawing.Point(56, 573);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(89, 17);
            this.labelControl29.TabIndex = 171;
            this.labelControl29.Text = "D1006 工作状态";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl30.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.Options.UseBackColor = true;
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl30.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl30.Location = new System.Drawing.Point(56, 469);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(89, 17);
            this.labelControl30.TabIndex = 167;
            this.labelControl30.Text = "D1002 准备信号";
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(150, 545);
            this.textEdit7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit7.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit7.Properties.ReadOnly = true;
            this.textEdit7.Size = new System.Drawing.Size(37, 22);
            this.textEdit7.TabIndex = 163;
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl31.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Appearance.Options.UseBackColor = true;
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl31.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl31.Location = new System.Drawing.Point(80, 495);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(65, 17);
            this.labelControl31.TabIndex = 168;
            this.labelControl31.Text = "D1003 启动";
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(150, 571);
            this.textEdit8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.textEdit8.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEdit8.Properties.ReadOnly = true;
            this.textEdit8.Size = new System.Drawing.Size(37, 22);
            this.textEdit8.TabIndex = 164;
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl32.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Appearance.Options.UseBackColor = true;
            this.labelControl32.Appearance.Options.UseFont = true;
            this.labelControl32.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl32.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl32.Location = new System.Drawing.Point(51, 599);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(94, 17);
            this.labelControl32.TabIndex = 172;
            this.labelControl32.Text = "D1007 异常/错误";
            // 
            // 触发1303
            // 
            this.触发1303.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.触发1303.Appearance.Options.UseFont = true;
            this.触发1303.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.触发1303.AppearanceDisabled.Options.UseFont = true;
            this.触发1303.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.触发1303.AppearanceHovered.Options.UseFont = true;
            this.触发1303.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.触发1303.AppearancePressed.Options.UseFont = true;
            this.触发1303.Location = new System.Drawing.Point(190, 108);
            this.触发1303.Name = "触发1303";
            this.触发1303.Size = new System.Drawing.Size(43, 23);
            this.触发1303.TabIndex = 157;
            this.触发1303.Text = "触发";
            this.触发1303.Click += new System.EventHandler(this.触发1303_Click);
            // 
            // txtD1013
            // 
            this.txtD1013.Location = new System.Drawing.Point(150, 369);
            this.txtD1013.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1013.Name = "txtD1013";
            this.txtD1013.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1013.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1013.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1013.Properties.Appearance.Options.UseFont = true;
            this.txtD1013.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1013.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1013.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1013.Properties.ReadOnly = true;
            this.txtD1013.Size = new System.Drawing.Size(37, 22);
            this.txtD1013.TabIndex = 156;
            // 
            // txtD1000
            // 
            this.txtD1000.EditValue = "";
            this.txtD1000.Location = new System.Drawing.Point(150, 31);
            this.txtD1000.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1000.Name = "txtD1000";
            this.txtD1000.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1000.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1000.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1000.Properties.Appearance.Options.UseFont = true;
            this.txtD1000.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1000.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.ReadOnly = true;
            this.txtD1000.Size = new System.Drawing.Size(37, 22);
            this.txtD1000.TabIndex = 129;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.Options.UseBackColor = true;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl18.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl18.Location = new System.Drawing.Point(66, 34);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(80, 17);
            this.labelControl18.TabIndex = 154;
            this.labelControl18.Text = "1300 产品到位";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl14.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(30, 268);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(116, 17);
            this.labelControl14.TabIndex = 146;
            this.labelControl14.Text = "1309 机器人安全信号";
            // 
            // txtD1007
            // 
            this.txtD1007.Location = new System.Drawing.Point(150, 213);
            this.txtD1007.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1007.Name = "txtD1007";
            this.txtD1007.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1007.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1007.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1007.Properties.Appearance.Options.UseFont = true;
            this.txtD1007.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1007.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1007.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1007.Properties.ReadOnly = true;
            this.txtD1007.Size = new System.Drawing.Size(37, 22);
            this.txtD1007.TabIndex = 136;
            // 
            // txtD1008
            // 
            this.txtD1008.Location = new System.Drawing.Point(150, 239);
            this.txtD1008.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1008.Name = "txtD1008";
            this.txtD1008.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1008.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1008.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1008.Properties.Appearance.Options.UseFont = true;
            this.txtD1008.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1008.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1008.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1008.Properties.ReadOnly = true;
            this.txtD1008.Size = new System.Drawing.Size(37, 22);
            this.txtD1008.TabIndex = 137;
            // 
            // txtD1010
            // 
            this.txtD1010.Location = new System.Drawing.Point(150, 291);
            this.txtD1010.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1010.Name = "txtD1010";
            this.txtD1010.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1010.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1010.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1010.Properties.Appearance.Options.UseFont = true;
            this.txtD1010.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1010.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1010.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1010.Properties.ReadOnly = true;
            this.txtD1010.Size = new System.Drawing.Size(37, 22);
            this.txtD1010.TabIndex = 151;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl19.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl19.Location = new System.Drawing.Point(118, 372);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(28, 17);
            this.labelControl19.TabIndex = 153;
            this.labelControl19.Text = "1313";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl13.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl13.Location = new System.Drawing.Point(118, 294);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(28, 17);
            this.labelControl13.TabIndex = 147;
            this.labelControl13.Text = "1310";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl9.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(42, 138);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(104, 17);
            this.labelControl9.TabIndex = 141;
            this.labelControl9.Text = "1304 组盘完成信号";
            // 
            // txtD1003
            // 
            this.txtD1003.Location = new System.Drawing.Point(150, 109);
            this.txtD1003.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1003.Name = "txtD1003";
            this.txtD1003.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1003.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1003.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1003.Properties.Appearance.Options.UseFont = true;
            this.txtD1003.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1003.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1003.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1003.Properties.ReadOnly = true;
            this.txtD1003.Size = new System.Drawing.Size(37, 22);
            this.txtD1003.TabIndex = 132;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.Options.UseBackColor = true;
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl17.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl17.Location = new System.Drawing.Point(66, 60);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(80, 17);
            this.labelControl17.TabIndex = 138;
            this.labelControl17.Text = "1301 是否抽检";
            // 
            // txtD1009
            // 
            this.txtD1009.Location = new System.Drawing.Point(150, 265);
            this.txtD1009.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1009.Name = "txtD1009";
            this.txtD1009.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtD1009.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1009.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1009.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1009.Properties.Appearance.Options.UseFont = true;
            this.txtD1009.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1009.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1009.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1009.Properties.ReadOnly = true;
            this.txtD1009.Size = new System.Drawing.Size(37, 22);
            this.txtD1009.TabIndex = 150;
            // 
            // txtD1002
            // 
            this.txtD1002.Location = new System.Drawing.Point(150, 83);
            this.txtD1002.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1002.Name = "txtD1002";
            this.txtD1002.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1002.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1002.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1002.Properties.Appearance.Options.UseFont = true;
            this.txtD1002.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1002.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1002.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1002.Properties.ReadOnly = true;
            this.txtD1002.Size = new System.Drawing.Size(37, 22);
            this.txtD1002.TabIndex = 131;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseBackColor = true;
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl10.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl10.Location = new System.Drawing.Point(42, 164);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(104, 17);
            this.labelControl10.TabIndex = 142;
            this.labelControl10.Text = "1305 组盘结果信号";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl11.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl11.Location = new System.Drawing.Point(118, 320);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(28, 17);
            this.labelControl11.TabIndex = 148;
            this.labelControl11.Text = "1311";
            // 
            // txtD1012
            // 
            this.txtD1012.Location = new System.Drawing.Point(150, 343);
            this.txtD1012.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1012.Name = "txtD1012";
            this.txtD1012.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1012.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1012.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1012.Properties.Appearance.Options.UseFont = true;
            this.txtD1012.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1012.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1012.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1012.Properties.ReadOnly = true;
            this.txtD1012.Size = new System.Drawing.Size(37, 22);
            this.txtD1012.TabIndex = 155;
            // 
            // txtD1004
            // 
            this.txtD1004.Location = new System.Drawing.Point(150, 135);
            this.txtD1004.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1004.Name = "txtD1004";
            this.txtD1004.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1004.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1004.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1004.Properties.Appearance.Options.UseFont = true;
            this.txtD1004.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1004.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1004.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1004.Properties.ReadOnly = true;
            this.txtD1004.Size = new System.Drawing.Size(37, 22);
            this.txtD1004.TabIndex = 133;
            // 
            // txtD1011
            // 
            this.txtD1011.Location = new System.Drawing.Point(150, 317);
            this.txtD1011.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1011.Name = "txtD1011";
            this.txtD1011.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1011.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1011.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1011.Properties.Appearance.Options.UseFont = true;
            this.txtD1011.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1011.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1011.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1011.Properties.ReadOnly = true;
            this.txtD1011.Size = new System.Drawing.Size(37, 22);
            this.txtD1011.TabIndex = 152;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl12.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(118, 346);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(28, 17);
            this.labelControl12.TabIndex = 149;
            this.labelControl12.Text = "1312";
            // 
            // txtD1001
            // 
            this.txtD1001.Location = new System.Drawing.Point(150, 57);
            this.txtD1001.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1001.Name = "txtD1001";
            this.txtD1001.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1001.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1001.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1001.Properties.Appearance.Options.UseFont = true;
            this.txtD1001.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1001.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1001.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1001.Properties.ReadOnly = true;
            this.txtD1001.Size = new System.Drawing.Size(37, 22);
            this.txtD1001.TabIndex = 130;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl15.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl15.Location = new System.Drawing.Point(30, 190);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(116, 17);
            this.labelControl15.TabIndex = 143;
            this.labelControl15.Text = "1306 下料盘在位信号";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl16.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl16.Location = new System.Drawing.Point(6, 242);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(140, 17);
            this.labelControl16.TabIndex = 145;
            this.labelControl16.Text = "1308 下料盘上料完成信号";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl20.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl20.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl20.Location = new System.Drawing.Point(30, 86);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(116, 17);
            this.labelControl20.TabIndex = 139;
            this.labelControl20.Text = "1302 缓冲盘产品状态";
            // 
            // txtD1005
            // 
            this.txtD1005.Location = new System.Drawing.Point(150, 161);
            this.txtD1005.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1005.Name = "txtD1005";
            this.txtD1005.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1005.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1005.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1005.Properties.Appearance.Options.UseFont = true;
            this.txtD1005.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1005.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1005.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1005.Properties.ReadOnly = true;
            this.txtD1005.Size = new System.Drawing.Size(37, 22);
            this.txtD1005.TabIndex = 134;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseBackColor = true;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl21.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl21.Location = new System.Drawing.Point(46, 112);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(100, 17);
            this.labelControl21.TabIndex = 140;
            this.labelControl21.Text = "1303组盘触发信号";
            // 
            // txtD1006
            // 
            this.txtD1006.Location = new System.Drawing.Point(150, 187);
            this.txtD1006.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1006.Name = "txtD1006";
            this.txtD1006.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1006.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1006.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1006.Properties.Appearance.Options.UseFont = true;
            this.txtD1006.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1006.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1006.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1006.Properties.ReadOnly = true;
            this.txtD1006.Size = new System.Drawing.Size(37, 22);
            this.txtD1006.TabIndex = 135;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl22.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.Options.UseBackColor = true;
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl22.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl22.Location = new System.Drawing.Point(30, 216);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(116, 17);
            this.labelControl22.TabIndex = 144;
            this.labelControl22.Text = "1307 下料盘已满信号";
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.Appearance.Options.UseForeColor = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue;
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseForeColor = true;
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.btmRDecre);
            this.groupControl1.Controls.Add(this.btmXIncre);
            this.groupControl1.Controls.Add(this.btmXDecre);
            this.groupControl1.Controls.Add(this.机器人报警清除);
            this.groupControl1.Controls.Add(this.btmYDecre);
            this.groupControl1.Controls.Add(this.btmYIncre);
            this.groupControl1.Controls.Add(this.机器人回原点);
            this.groupControl1.Controls.Add(this.伺服启用禁用);
            this.groupControl1.Controls.Add(this.btmZDecre);
            this.groupControl1.Controls.Add(this.btmZIncre);
            this.groupControl1.Controls.Add(this.btmRIncre);
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(649, 288);
            this.groupControl1.TabIndex = 101;
            this.groupControl1.Text = "机器人单动";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.rbtmSeep_SupperHigh);
            this.groupControl2.Controls.Add(this.rbtmSeep_High);
            this.groupControl2.Controls.Add(this.label17);
            this.groupControl2.Controls.Add(this.cobMoveDst);
            this.groupControl2.Controls.Add(this.rbtmSeep_Middel);
            this.groupControl2.Controls.Add(this.rbtmSeep_Low);
            this.groupControl2.Location = new System.Drawing.Point(0, 221);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(459, 67);
            this.groupControl2.TabIndex = 131;
            this.groupControl2.Text = "机器人速度设定";
            // 
            // rbtmSeep_SupperHigh
            // 
            this.rbtmSeep_SupperHigh.AutoSize = true;
            this.rbtmSeep_SupperHigh.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtmSeep_SupperHigh.Location = new System.Drawing.Point(379, 28);
            this.rbtmSeep_SupperHigh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbtmSeep_SupperHigh.Name = "rbtmSeep_SupperHigh";
            this.rbtmSeep_SupperHigh.Size = new System.Drawing.Size(74, 21);
            this.rbtmSeep_SupperHigh.TabIndex = 131;
            this.rbtmSeep_SupperHigh.TabStop = true;
            this.rbtmSeep_SupperHigh.Text = "极速运行";
            this.rbtmSeep_SupperHigh.UseVisualStyleBackColor = true;
            this.rbtmSeep_SupperHigh.CheckedChanged += new System.EventHandler(this.rbtmSeep_SupperHigh_CheckedChanged);
            // 
            // rbtmSeep_High
            // 
            this.rbtmSeep_High.AutoSize = true;
            this.rbtmSeep_High.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtmSeep_High.Location = new System.Drawing.Point(302, 28);
            this.rbtmSeep_High.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbtmSeep_High.Name = "rbtmSeep_High";
            this.rbtmSeep_High.Size = new System.Drawing.Size(74, 21);
            this.rbtmSeep_High.TabIndex = 2;
            this.rbtmSeep_High.TabStop = true;
            this.rbtmSeep_High.Text = "高速运行";
            this.rbtmSeep_High.UseVisualStyleBackColor = true;
            this.rbtmSeep_High.CheckedChanged += new System.EventHandler(this.rbtmSeep_High_CheckedChanged);
            this.rbtmSeep_High.Click += new System.EventHandler(this.btmSpeedClick);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 31);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 17);
            this.label17.TabIndex = 130;
            this.label17.Text = "设定位移量";
            // 
            // cobMoveDst
            // 
            this.cobMoveDst.FormattingEnabled = true;
            this.cobMoveDst.Items.AddRange(new object[] {
            "0.02",
            "0.05",
            "0.1",
            "0.2",
            "0.5",
            "1.0",
            "2.0",
            "5.0",
            "8.2",
            "10"});
            this.cobMoveDst.Location = new System.Drawing.Point(83, 27);
            this.cobMoveDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cobMoveDst.Name = "cobMoveDst";
            this.cobMoveDst.Size = new System.Drawing.Size(49, 25);
            this.cobMoveDst.TabIndex = 107;
            this.cobMoveDst.Text = "0.5";
            // 
            // rbtmSeep_Middel
            // 
            this.rbtmSeep_Middel.AutoSize = true;
            this.rbtmSeep_Middel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtmSeep_Middel.Location = new System.Drawing.Point(218, 28);
            this.rbtmSeep_Middel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbtmSeep_Middel.Name = "rbtmSeep_Middel";
            this.rbtmSeep_Middel.Size = new System.Drawing.Size(74, 21);
            this.rbtmSeep_Middel.TabIndex = 1;
            this.rbtmSeep_Middel.TabStop = true;
            this.rbtmSeep_Middel.Text = "中速运行";
            this.rbtmSeep_Middel.UseVisualStyleBackColor = true;
            this.rbtmSeep_Middel.Click += new System.EventHandler(this.btmSpeedClick);
            // 
            // rbtmSeep_Low
            // 
            this.rbtmSeep_Low.AutoSize = true;
            this.rbtmSeep_Low.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtmSeep_Low.Location = new System.Drawing.Point(141, 28);
            this.rbtmSeep_Low.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbtmSeep_Low.Name = "rbtmSeep_Low";
            this.rbtmSeep_Low.Size = new System.Drawing.Size(74, 21);
            this.rbtmSeep_Low.TabIndex = 0;
            this.rbtmSeep_Low.TabStop = true;
            this.rbtmSeep_Low.Text = "低速运行";
            this.rbtmSeep_Low.UseVisualStyleBackColor = true;
            this.rbtmSeep_Low.CheckedChanged += new System.EventHandler(this.rbtmSeep_Low_CheckedChanged);
            this.rbtmSeep_Low.Click += new System.EventHandler(this.btmSpeedClick);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton1.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton1.AppearanceHovered.Options.UseFont = true;
            this.simpleButton1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton1.AppearancePressed.Options.UseFont = true;
            this.simpleButton1.Location = new System.Drawing.Point(243, 105);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(121, 28);
            this.simpleButton1.TabIndex = 176;
            this.simpleButton1.Text = "Test";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btmRDecre
            // 
            this.btmRDecre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRDecre.Appearance.Options.UseFont = true;
            this.btmRDecre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRDecre.AppearanceDisabled.Options.UseFont = true;
            this.btmRDecre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRDecre.AppearanceHovered.Options.UseFont = true;
            this.btmRDecre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRDecre.AppearancePressed.Options.UseFont = true;
            this.btmRDecre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRDecre.ImageOptions.Image")));
            this.btmRDecre.Location = new System.Drawing.Point(320, 157);
            this.btmRDecre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRDecre.Name = "btmRDecre";
            this.btmRDecre.Size = new System.Drawing.Size(77, 58);
            this.btmRDecre.TabIndex = 105;
            this.btmRDecre.Text = "R-";
            this.btmRDecre.Click += new System.EventHandler(this.btmRDecre_Click);
            // 
            // btmXIncre
            // 
            this.btmXIncre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmXIncre.Appearance.Options.UseFont = true;
            this.btmXIncre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmXIncre.AppearanceDisabled.Options.UseFont = true;
            this.btmXIncre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmXIncre.AppearanceHovered.Options.UseFont = true;
            this.btmXIncre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmXIncre.AppearancePressed.Options.UseFont = true;
            this.btmXIncre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmXIncre.ImageOptions.Image")));
            this.btmXIncre.Location = new System.Drawing.Point(5, 90);
            this.btmXIncre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmXIncre.Name = "btmXIncre";
            this.btmXIncre.Size = new System.Drawing.Size(77, 58);
            this.btmXIncre.TabIndex = 99;
            this.btmXIncre.Text = "X+";
            this.btmXIncre.Click += new System.EventHandler(this.btmXIncre_Click);
            // 
            // btmXDecre
            // 
            this.btmXDecre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmXDecre.Appearance.Options.UseFont = true;
            this.btmXDecre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmXDecre.AppearanceDisabled.Options.UseFont = true;
            this.btmXDecre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmXDecre.AppearanceHovered.Options.UseFont = true;
            this.btmXDecre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmXDecre.AppearancePressed.Options.UseFont = true;
            this.btmXDecre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmXDecre.ImageOptions.Image")));
            this.btmXDecre.Location = new System.Drawing.Point(102, 90);
            this.btmXDecre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmXDecre.Name = "btmXDecre";
            this.btmXDecre.Size = new System.Drawing.Size(77, 58);
            this.btmXDecre.TabIndex = 100;
            this.btmXDecre.Text = "X-";
            this.btmXDecre.Click += new System.EventHandler(this.btmXDecre_Click);
            // 
            // 机器人报警清除
            // 
            this.机器人报警清除.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.机器人报警清除.Appearance.Options.UseFont = true;
            this.机器人报警清除.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.机器人报警清除.AppearanceDisabled.Options.UseFont = true;
            this.机器人报警清除.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.机器人报警清除.AppearanceHovered.Options.UseFont = true;
            this.机器人报警清除.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.机器人报警清除.AppearancePressed.Options.UseFont = true;
            this.机器人报警清除.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("机器人报警清除.ImageOptions.Image")));
            this.机器人报警清除.Location = new System.Drawing.Point(465, 171);
            this.机器人报警清除.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.机器人报警清除.Name = "机器人报警清除";
            this.机器人报警清除.Size = new System.Drawing.Size(176, 57);
            this.机器人报警清除.TabIndex = 177;
            this.机器人报警清除.Text = "机器人报警清除";
            this.机器人报警清除.Click += new System.EventHandler(this.机器人报警清除_Click);
            // 
            // btmYDecre
            // 
            this.btmYDecre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmYDecre.Appearance.Options.UseFont = true;
            this.btmYDecre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmYDecre.AppearanceDisabled.Options.UseFont = true;
            this.btmYDecre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmYDecre.AppearanceHovered.Options.UseFont = true;
            this.btmYDecre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmYDecre.AppearancePressed.Options.UseFont = true;
            this.btmYDecre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmYDecre.ImageOptions.Image")));
            this.btmYDecre.Location = new System.Drawing.Point(55, 28);
            this.btmYDecre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmYDecre.Name = "btmYDecre";
            this.btmYDecre.Size = new System.Drawing.Size(77, 58);
            this.btmYDecre.TabIndex = 101;
            this.btmYDecre.Text = "Y-";
            this.btmYDecre.Click += new System.EventHandler(this.btmYDecre_Click);
            // 
            // btmYIncre
            // 
            this.btmYIncre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmYIncre.Appearance.Options.UseFont = true;
            this.btmYIncre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmYIncre.AppearanceDisabled.Options.UseFont = true;
            this.btmYIncre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmYIncre.AppearanceHovered.Options.UseFont = true;
            this.btmYIncre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmYIncre.AppearancePressed.Options.UseFont = true;
            this.btmYIncre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmYIncre.ImageOptions.Image")));
            this.btmYIncre.Location = new System.Drawing.Point(55, 155);
            this.btmYIncre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmYIncre.Name = "btmYIncre";
            this.btmYIncre.Size = new System.Drawing.Size(77, 58);
            this.btmYIncre.TabIndex = 102;
            this.btmYIncre.Text = "Y+";
            this.btmYIncre.Click += new System.EventHandler(this.btmYIncre_Click);
            // 
            // 机器人回原点
            // 
            this.机器人回原点.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.机器人回原点.Appearance.Options.UseFont = true;
            this.机器人回原点.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.机器人回原点.AppearanceDisabled.Options.UseFont = true;
            this.机器人回原点.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.机器人回原点.AppearanceHovered.Options.UseFont = true;
            this.机器人回原点.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.机器人回原点.AppearancePressed.Options.UseFont = true;
            this.机器人回原点.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("机器人回原点.ImageOptions.Image")));
            this.机器人回原点.Location = new System.Drawing.Point(463, 114);
            this.机器人回原点.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.机器人回原点.Name = "机器人回原点";
            this.机器人回原点.Size = new System.Drawing.Size(176, 54);
            this.机器人回原点.TabIndex = 107;
            this.机器人回原点.Text = "机器人回原点";
            this.机器人回原点.Click += new System.EventHandler(this.btmTestRobot_Click);
            // 
            // 伺服启用禁用
            // 
            this.伺服启用禁用.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.伺服启用禁用.Appearance.Options.UseFont = true;
            this.伺服启用禁用.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.伺服启用禁用.AppearanceDisabled.Options.UseFont = true;
            this.伺服启用禁用.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.伺服启用禁用.AppearanceHovered.Options.UseFont = true;
            this.伺服启用禁用.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.伺服启用禁用.AppearancePressed.Options.UseFont = true;
            this.伺服启用禁用.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.伺服启用禁用.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("伺服启用禁用.ImageOptions.Image")));
            this.伺服启用禁用.Location = new System.Drawing.Point(463, 23);
            this.伺服启用禁用.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.伺服启用禁用.Name = "伺服启用禁用";
            this.伺服启用禁用.Size = new System.Drawing.Size(176, 86);
            this.伺服启用禁用.TabIndex = 171;
            this.伺服启用禁用.Text = "伺服启用/禁用";
            this.伺服启用禁用.CheckedChanged += new System.EventHandler(this.伺服启用禁用_CheckedChanged);
            // 
            // btmZDecre
            // 
            this.btmZDecre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmZDecre.Appearance.Options.UseFont = true;
            this.btmZDecre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmZDecre.AppearanceDisabled.Options.UseFont = true;
            this.btmZDecre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmZDecre.AppearanceHovered.Options.UseFont = true;
            this.btmZDecre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmZDecre.AppearancePressed.Options.UseFont = true;
            this.btmZDecre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmZDecre.ImageOptions.Image")));
            this.btmZDecre.Location = new System.Drawing.Point(211, 157);
            this.btmZDecre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmZDecre.Name = "btmZDecre";
            this.btmZDecre.Size = new System.Drawing.Size(77, 58);
            this.btmZDecre.TabIndex = 103;
            this.btmZDecre.Text = "Z-";
            this.btmZDecre.Click += new System.EventHandler(this.btmZDecre_Click);
            // 
            // btmZIncre
            // 
            this.btmZIncre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmZIncre.Appearance.Options.UseFont = true;
            this.btmZIncre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmZIncre.AppearanceDisabled.Options.UseFont = true;
            this.btmZIncre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmZIncre.AppearanceHovered.Options.UseFont = true;
            this.btmZIncre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmZIncre.AppearancePressed.Options.UseFont = true;
            this.btmZIncre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmZIncre.ImageOptions.Image")));
            this.btmZIncre.Location = new System.Drawing.Point(211, 30);
            this.btmZIncre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmZIncre.Name = "btmZIncre";
            this.btmZIncre.Size = new System.Drawing.Size(77, 58);
            this.btmZIncre.TabIndex = 104;
            this.btmZIncre.Text = "Z+";
            this.btmZIncre.Click += new System.EventHandler(this.btmZIncre_Click);
            // 
            // btmRIncre
            // 
            this.btmRIncre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRIncre.Appearance.Options.UseFont = true;
            this.btmRIncre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRIncre.AppearanceDisabled.Options.UseFont = true;
            this.btmRIncre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRIncre.AppearanceHovered.Options.UseFont = true;
            this.btmRIncre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRIncre.AppearancePressed.Options.UseFont = true;
            this.btmRIncre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRIncre.ImageOptions.Image")));
            this.btmRIncre.Location = new System.Drawing.Point(320, 30);
            this.btmRIncre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRIncre.Name = "btmRIncre";
            this.btmRIncre.Size = new System.Drawing.Size(77, 58);
            this.btmRIncre.TabIndex = 106;
            this.btmRIncre.Text = "R+";
            this.btmRIncre.Click += new System.EventHandler(this.btmRIncre_Click);
            // 
            // groupControl14
            // 
            this.groupControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl14.Appearance.Options.UseFont = true;
            this.groupControl14.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl14.AppearanceCaption.Options.UseFont = true;
            this.groupControl14.Controls.Add(this.txtStepDst);
            this.groupControl14.Controls.Add(this.电机间距Dcre);
            this.groupControl14.Controls.Add(this.电机间距Incre);
            this.groupControl14.Controls.Add(this.间距电机回原点);
            this.groupControl14.Controls.Add(this.simpleButton25);
            this.groupControl14.Controls.Add(this.simpleButton26);
            this.groupControl14.Controls.Add(this.simpleButton27);
            this.groupControl14.Controls.Add(this.simpleButton28);
            this.groupControl14.Location = new System.Drawing.Point(647, 0);
            this.groupControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl14.Name = "groupControl14";
            this.groupControl14.Size = new System.Drawing.Size(226, 288);
            this.groupControl14.TabIndex = 183;
            this.groupControl14.Text = "变距电机设定";
            // 
            // txtStepDst
            // 
            this.txtStepDst.EditValue = "50000";
            this.txtStepDst.Location = new System.Drawing.Point(50, 101);
            this.txtStepDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStepDst.Name = "txtStepDst";
            this.txtStepDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStepDst.Properties.Appearance.Options.UseFont = true;
            this.txtStepDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStepDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtStepDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStepDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtStepDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStepDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtStepDst.Size = new System.Drawing.Size(132, 24);
            this.txtStepDst.TabIndex = 173;
            // 
            // 电机间距Dcre
            // 
            this.电机间距Dcre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.电机间距Dcre.Appearance.Options.UseFont = true;
            this.电机间距Dcre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.电机间距Dcre.AppearanceDisabled.Options.UseFont = true;
            this.电机间距Dcre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.电机间距Dcre.AppearanceHovered.Options.UseFont = true;
            this.电机间距Dcre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.电机间距Dcre.AppearancePressed.Options.UseFont = true;
            this.电机间距Dcre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("电机间距Dcre.ImageOptions.Image")));
            this.电机间距Dcre.Location = new System.Drawing.Point(25, 139);
            this.电机间距Dcre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.电机间距Dcre.Name = "电机间距Dcre";
            this.电机间距Dcre.Size = new System.Drawing.Size(77, 58);
            this.电机间距Dcre.TabIndex = 171;
            this.电机间距Dcre.Text = "X-";
            this.电机间距Dcre.Click += new System.EventHandler(this.电机间距Dcre_Click);
            // 
            // 电机间距Incre
            // 
            this.电机间距Incre.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.电机间距Incre.Appearance.Options.UseFont = true;
            this.电机间距Incre.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.电机间距Incre.AppearanceDisabled.Options.UseFont = true;
            this.电机间距Incre.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.电机间距Incre.AppearanceHovered.Options.UseFont = true;
            this.电机间距Incre.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.电机间距Incre.AppearancePressed.Options.UseFont = true;
            this.电机间距Incre.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("电机间距Incre.ImageOptions.Image")));
            this.电机间距Incre.Location = new System.Drawing.Point(126, 139);
            this.电机间距Incre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.电机间距Incre.Name = "电机间距Incre";
            this.电机间距Incre.Size = new System.Drawing.Size(77, 58);
            this.电机间距Incre.TabIndex = 172;
            this.电机间距Incre.Text = "X+";
            this.电机间距Incre.Click += new System.EventHandler(this.电机间距Incre_Click);
            // 
            // 间距电机回原点
            // 
            this.间距电机回原点.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.间距电机回原点.Appearance.Options.UseFont = true;
            this.间距电机回原点.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.间距电机回原点.AppearanceDisabled.Options.UseFont = true;
            this.间距电机回原点.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.间距电机回原点.AppearanceHovered.Options.UseFont = true;
            this.间距电机回原点.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.间距电机回原点.AppearancePressed.Options.UseFont = true;
            this.间距电机回原点.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("间距电机回原点.ImageOptions.Image")));
            this.间距电机回原点.Location = new System.Drawing.Point(50, 29);
            this.间距电机回原点.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.间距电机回原点.Name = "间距电机回原点";
            this.间距电机回原点.Size = new System.Drawing.Size(132, 58);
            this.间距电机回原点.TabIndex = 170;
            this.间距电机回原点.Text = "间距电机回原点";
            this.间距电机回原点.Click += new System.EventHandler(this.间距电机回原点_Click);
            // 
            // simpleButton25
            // 
            this.simpleButton25.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton25.Appearance.Options.UseFont = true;
            this.simpleButton25.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton25.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton25.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton25.AppearanceHovered.Options.UseFont = true;
            this.simpleButton25.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton25.AppearancePressed.Options.UseFont = true;
            this.simpleButton25.Location = new System.Drawing.Point(694, 200);
            this.simpleButton25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton25.Name = "simpleButton25";
            this.simpleButton25.Size = new System.Drawing.Size(134, 68);
            this.simpleButton25.TabIndex = 5;
            this.simpleButton25.Text = "清除所有";
            // 
            // simpleButton26
            // 
            this.simpleButton26.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton26.Appearance.Options.UseFont = true;
            this.simpleButton26.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton26.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton26.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton26.AppearanceHovered.Options.UseFont = true;
            this.simpleButton26.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton26.AppearancePressed.Options.UseFont = true;
            this.simpleButton26.Location = new System.Drawing.Point(694, 125);
            this.simpleButton26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton26.Name = "simpleButton26";
            this.simpleButton26.Size = new System.Drawing.Size(134, 68);
            this.simpleButton26.TabIndex = 4;
            this.simpleButton26.Text = "删除路径";
            // 
            // simpleButton27
            // 
            this.simpleButton27.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton27.Appearance.Options.UseFont = true;
            this.simpleButton27.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton27.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton27.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton27.AppearanceHovered.Options.UseFont = true;
            this.simpleButton27.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton27.AppearancePressed.Options.UseFont = true;
            this.simpleButton27.Location = new System.Drawing.Point(694, 276);
            this.simpleButton27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton27.Name = "simpleButton27";
            this.simpleButton27.Size = new System.Drawing.Size(134, 68);
            this.simpleButton27.TabIndex = 3;
            this.simpleButton27.Text = "学习当前(行)路径";
            // 
            // simpleButton28
            // 
            this.simpleButton28.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton28.Appearance.Options.UseFont = true;
            this.simpleButton28.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton28.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton28.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton28.AppearanceHovered.Options.UseFont = true;
            this.simpleButton28.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton28.AppearancePressed.Options.UseFont = true;
            this.simpleButton28.Location = new System.Drawing.Point(694, 50);
            this.simpleButton28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton28.Name = "simpleButton28";
            this.simpleButton28.Size = new System.Drawing.Size(134, 68);
            this.simpleButton28.TabIndex = 2;
            this.simpleButton28.Text = "增加路径";
            // 
            // NP_生产数据查询
            // 
            this.NP_生产数据查询.Caption = " 数据查询";
            this.NP_生产数据查询.Controls.Add(this.xtraTabControl2);
            this.NP_生产数据查询.Image = ((System.Drawing.Image)(resources.GetObject("NP_生产数据查询.Image")));
            this.NP_生产数据查询.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_生产数据查询.Name = "NP_生产数据查询";
            this.NP_生产数据查询.Size = new System.Drawing.Size(1688, 646);
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.xtraTabControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl2.Appearance.Options.UseBackColor = true;
            this.xtraTabControl2.Appearance.Options.UseFont = true;
            this.xtraTabControl2.AppearancePage.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl2.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl2.AppearancePage.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl2.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl2.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl2.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.xtraTabControl2.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl2.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabControl2.AppearancePage.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl2.AppearancePage.PageClient.Options.UseFont = true;
            this.xtraTabControl2.Location = new System.Drawing.Point(4, 4);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(1675, 640);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Appearance.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage3.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage3.Appearance.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage3.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage3.Appearance.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage3.Appearance.HeaderDisabled.Options.UseFont = true;
            this.xtraTabPage3.Appearance.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage3.Appearance.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabPage3.Appearance.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage3.Appearance.PageClient.Options.UseFont = true;
            this.xtraTabPage3.Controls.Add(this.xtraTabControl3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1669, 608);
            this.xtraTabPage3.Text = "烧录数据结果";
            // 
            // xtraTabControl3
            // 
            this.xtraTabControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.xtraTabControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl3.Appearance.Options.UseBackColor = true;
            this.xtraTabControl3.Appearance.Options.UseFont = true;
            this.xtraTabControl3.AppearancePage.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl3.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl3.AppearancePage.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl3.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl3.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl3.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.xtraTabControl3.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl3.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabControl3.AppearancePage.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl3.AppearancePage.PageClient.Options.UseFont = true;
            this.xtraTabControl3.Location = new System.Drawing.Point(3, 0);
            this.xtraTabControl3.Name = "xtraTabControl3";
            this.xtraTabControl3.SelectedTabPage = this.烧录区域1数据;
            this.xtraTabControl3.Size = new System.Drawing.Size(1671, 597);
            this.xtraTabControl3.TabIndex = 0;
            this.xtraTabControl3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.烧录区域1数据,
            this.烧录区域2数据,
            this.烧录区域3数据,
            this.烧录区域4数据,
            this.烧录区域5数据,
            this.烧录区域6数据,
            this.烧录区域7数据,
            this.烧录区域8数据});
            // 
            // 烧录区域1数据
            // 
            this.烧录区域1数据.Appearance.Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.烧录区域1数据.Appearance.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录区域1数据.Appearance.Header.Options.UseBackColor = true;
            this.烧录区域1数据.Appearance.Header.Options.UseFont = true;
            this.烧录区域1数据.Appearance.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录区域1数据.Appearance.HeaderActive.Options.UseFont = true;
            this.烧录区域1数据.Appearance.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录区域1数据.Appearance.HeaderDisabled.Options.UseFont = true;
            this.烧录区域1数据.Appearance.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录区域1数据.Appearance.HeaderHotTracked.Options.UseFont = true;
            this.烧录区域1数据.Appearance.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.烧录区域1数据.Appearance.PageClient.Options.UseFont = true;
            this.烧录区域1数据.Controls.Add(this.txtNameArea1);
            this.烧录区域1数据.Controls.Add(this.dataGridArea1);
            this.烧录区域1数据.Name = "烧录区域1数据";
            this.烧录区域1数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域1数据.Text = "烧录区域1数据";
            // 
            // txtNameArea1
            // 
            this.txtNameArea1.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea1.Name = "txtNameArea1";
            this.txtNameArea1.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea1.TabIndex = 2;
            // 
            // dataGridArea1
            // 
            this.dataGridArea1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("微软雅黑", 9F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridArea1.ColumnHeadersHeight = 24;
            this.dataGridArea1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea1.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea1.Name = "dataGridArea1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("微软雅黑", 9F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridArea1.RowTemplate.Height = 23;
            this.dataGridArea1.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea1.TabIndex = 1;
            // 
            // 烧录区域2数据
            // 
            this.烧录区域2数据.Controls.Add(this.txtNameArea2);
            this.烧录区域2数据.Controls.Add(this.dataGridArea2);
            this.烧录区域2数据.Name = "烧录区域2数据";
            this.烧录区域2数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域2数据.Text = "烧录区域2数据";
            // 
            // txtNameArea2
            // 
            this.txtNameArea2.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea2.Name = "txtNameArea2";
            this.txtNameArea2.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea2.TabIndex = 3;
            // 
            // dataGridArea2
            // 
            this.dataGridArea2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridArea2.ColumnHeadersHeight = 24;
            this.dataGridArea2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea2.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea2.Name = "dataGridArea2";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea2.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridArea2.RowTemplate.Height = 23;
            this.dataGridArea2.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea2.TabIndex = 2;
            // 
            // 烧录区域3数据
            // 
            this.烧录区域3数据.Controls.Add(this.txtNameArea3);
            this.烧录区域3数据.Controls.Add(this.dataGridArea3);
            this.烧录区域3数据.Name = "烧录区域3数据";
            this.烧录区域3数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域3数据.Text = "烧录区域3数据";
            // 
            // txtNameArea3
            // 
            this.txtNameArea3.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea3.Name = "txtNameArea3";
            this.txtNameArea3.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea3.TabIndex = 3;
            // 
            // dataGridArea3
            // 
            this.dataGridArea3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridArea3.ColumnHeadersHeight = 24;
            this.dataGridArea3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea3.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea3.Name = "dataGridArea3";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea3.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridArea3.RowTemplate.Height = 23;
            this.dataGridArea3.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea3.TabIndex = 2;
            // 
            // 烧录区域4数据
            // 
            this.烧录区域4数据.Controls.Add(this.txtNameArea4);
            this.烧录区域4数据.Controls.Add(this.dataGridArea4);
            this.烧录区域4数据.Name = "烧录区域4数据";
            this.烧录区域4数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域4数据.Text = "烧录区域4数据";
            // 
            // txtNameArea4
            // 
            this.txtNameArea4.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea4.Name = "txtNameArea4";
            this.txtNameArea4.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea4.TabIndex = 3;
            // 
            // dataGridArea4
            // 
            this.dataGridArea4.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridArea4.ColumnHeadersHeight = 24;
            this.dataGridArea4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea4.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea4.Name = "dataGridArea4";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea4.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridArea4.RowTemplate.Height = 23;
            this.dataGridArea4.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea4.TabIndex = 2;
            // 
            // 烧录区域5数据
            // 
            this.烧录区域5数据.Controls.Add(this.txtNameArea5);
            this.烧录区域5数据.Controls.Add(this.dataGridArea5);
            this.烧录区域5数据.Name = "烧录区域5数据";
            this.烧录区域5数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域5数据.Text = "烧录区域5数据";
            // 
            // txtNameArea5
            // 
            this.txtNameArea5.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea5.Name = "txtNameArea5";
            this.txtNameArea5.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea5.TabIndex = 3;
            // 
            // dataGridArea5
            // 
            this.dataGridArea5.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridArea5.ColumnHeadersHeight = 24;
            this.dataGridArea5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea5.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea5.Name = "dataGridArea5";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea5.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridArea5.RowTemplate.Height = 23;
            this.dataGridArea5.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea5.TabIndex = 2;
            // 
            // 烧录区域6数据
            // 
            this.烧录区域6数据.Controls.Add(this.txtNameArea6);
            this.烧录区域6数据.Controls.Add(this.dataGridArea6);
            this.烧录区域6数据.Name = "烧录区域6数据";
            this.烧录区域6数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域6数据.Text = "烧录区域6数据";
            // 
            // txtNameArea6
            // 
            this.txtNameArea6.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea6.Name = "txtNameArea6";
            this.txtNameArea6.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea6.TabIndex = 3;
            // 
            // dataGridArea6
            // 
            this.dataGridArea6.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea6.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridArea6.ColumnHeadersHeight = 24;
            this.dataGridArea6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea6.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea6.Name = "dataGridArea6";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea6.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridArea6.RowTemplate.Height = 23;
            this.dataGridArea6.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea6.TabIndex = 2;
            // 
            // 烧录区域7数据
            // 
            this.烧录区域7数据.Controls.Add(this.txtNameArea7);
            this.烧录区域7数据.Controls.Add(this.dataGridArea7);
            this.烧录区域7数据.Name = "烧录区域7数据";
            this.烧录区域7数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域7数据.Text = "烧录区域7数据";
            // 
            // txtNameArea7
            // 
            this.txtNameArea7.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea7.Name = "txtNameArea7";
            this.txtNameArea7.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea7.TabIndex = 5;
            // 
            // dataGridArea7
            // 
            this.dataGridArea7.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea7.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridArea7.ColumnHeadersHeight = 24;
            this.dataGridArea7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea7.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea7.Name = "dataGridArea7";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea7.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridArea7.RowTemplate.Height = 23;
            this.dataGridArea7.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea7.TabIndex = 4;
            // 
            // 烧录区域8数据
            // 
            this.烧录区域8数据.Controls.Add(this.txtNameArea8);
            this.烧录区域8数据.Controls.Add(this.dataGridArea8);
            this.烧录区域8数据.Name = "烧录区域8数据";
            this.烧录区域8数据.Size = new System.Drawing.Size(1665, 565);
            this.烧录区域8数据.Text = "烧录区域8数据";
            // 
            // txtNameArea8
            // 
            this.txtNameArea8.Location = new System.Drawing.Point(0, 7);
            this.txtNameArea8.Name = "txtNameArea8";
            this.txtNameArea8.Size = new System.Drawing.Size(321, 22);
            this.txtNameArea8.TabIndex = 5;
            // 
            // dataGridArea8
            // 
            this.dataGridArea8.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea8.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridArea8.ColumnHeadersHeight = 24;
            this.dataGridArea8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridArea8.Location = new System.Drawing.Point(0, 36);
            this.dataGridArea8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridArea8.Name = "dataGridArea8";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridArea8.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridArea8.RowTemplate.Height = 23;
            this.dataGridArea8.Size = new System.Drawing.Size(800, 663);
            this.dataGridArea8.TabIndex = 4;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1669, 608);
            this.xtraTabPage4.Text = "其它";
            // 
            // NP_工艺参数
            // 
            this.NP_工艺参数.Caption = " 工艺参数";
            this.NP_工艺参数.Controls.Add(this.spedSetCurrentTray);
            this.NP_工艺参数.Controls.Add(this.btmSetCurrentTray);
            this.NP_工艺参数.Controls.Add(this.simpleButton30);
            this.NP_工艺参数.Controls.Add(this.btmSaveConfig2);
            this.NP_工艺参数.Controls.Add(this.gridBuffTrayResult);
            this.NP_工艺参数.Controls.Add(this.gridLoadTrayResult);
            this.NP_工艺参数.Controls.Add(this.groupControl10);
            this.NP_工艺参数.Image = ((System.Drawing.Image)(resources.GetObject("NP_工艺参数.Image")));
            this.NP_工艺参数.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_工艺参数.Name = "NP_工艺参数";
            this.NP_工艺参数.Size = new System.Drawing.Size(1684, 647);
            // 
            // spedSetCurrentTray
            // 
            this.spedSetCurrentTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedSetCurrentTray.Enabled = false;
            this.spedSetCurrentTray.Location = new System.Drawing.Point(687, 373);
            this.spedSetCurrentTray.Name = "spedSetCurrentTray";
            this.spedSetCurrentTray.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.spedSetCurrentTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedSetCurrentTray.Properties.Appearance.Options.UseBackColor = true;
            this.spedSetCurrentTray.Properties.Appearance.Options.UseFont = true;
            this.spedSetCurrentTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetCurrentTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedSetCurrentTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetCurrentTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedSetCurrentTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetCurrentTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedSetCurrentTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedSetCurrentTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedSetCurrentTray.Size = new System.Drawing.Size(71, 24);
            this.spedSetCurrentTray.TabIndex = 194;
            this.spedSetCurrentTray.Visible = false;
            // 
            // btmSetCurrentTray
            // 
            this.btmSetCurrentTray.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetCurrentTray.Appearance.Options.UseFont = true;
            this.btmSetCurrentTray.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTray.AppearanceDisabled.Options.UseFont = true;
            this.btmSetCurrentTray.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTray.AppearanceHovered.Options.UseFont = true;
            this.btmSetCurrentTray.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTray.AppearancePressed.Options.UseFont = true;
            this.btmSetCurrentTray.Enabled = false;
            this.btmSetCurrentTray.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetCurrentTray.ImageOptions.Image")));
            this.btmSetCurrentTray.Location = new System.Drawing.Point(775, 362);
            this.btmSetCurrentTray.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetCurrentTray.Name = "btmSetCurrentTray";
            this.btmSetCurrentTray.Size = new System.Drawing.Size(177, 46);
            this.btmSetCurrentTray.TabIndex = 193;
            this.btmSetCurrentTray.Text = "设定当前实时包装盘数";
            this.btmSetCurrentTray.Visible = false;
            this.btmSetCurrentTray.Click += new System.EventHandler(this.btmSetCurrentTray_Click);
            // 
            // simpleButton30
            // 
            this.simpleButton30.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton30.Appearance.Options.UseFont = true;
            this.simpleButton30.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton30.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton30.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton30.AppearanceHovered.Options.UseFont = true;
            this.simpleButton30.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton30.AppearancePressed.Options.UseFont = true;
            this.simpleButton30.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton30.ImageOptions.Image")));
            this.simpleButton30.Location = new System.Drawing.Point(1387, 575);
            this.simpleButton30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton30.Name = "simpleButton30";
            this.simpleButton30.Size = new System.Drawing.Size(145, 62);
            this.simpleButton30.TabIndex = 190;
            this.simpleButton30.Text = "Test";
            this.simpleButton30.Visible = false;
            this.simpleButton30.Click += new System.EventHandler(this.simpleButton30_Click);
            // 
            // btmSaveConfig2
            // 
            this.btmSaveConfig2.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.btmSaveConfig2.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmSaveConfig2.Appearance.Options.UseBackColor = true;
            this.btmSaveConfig2.Appearance.Options.UseFont = true;
            this.btmSaveConfig2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSaveConfig2.AppearanceDisabled.Options.UseFont = true;
            this.btmSaveConfig2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Italic);
            this.btmSaveConfig2.AppearanceHovered.Options.UseFont = true;
            this.btmSaveConfig2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSaveConfig2.AppearancePressed.Options.UseFont = true;
            this.btmSaveConfig2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmSaveConfig2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSaveConfig2.ImageOptions.Image")));
            this.btmSaveConfig2.Location = new System.Drawing.Point(30, 443);
            this.btmSaveConfig2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSaveConfig2.Name = "btmSaveConfig2";
            this.btmSaveConfig2.Size = new System.Drawing.Size(228, 132);
            this.btmSaveConfig2.TabIndex = 183;
            this.btmSaveConfig2.Text = "保存所有";
            this.btmSaveConfig2.Click += new System.EventHandler(this.btmSaveRobotConfig_Click);
            // 
            // gridBuffTrayResult
            // 
            this.gridBuffTrayResult.AllowUserToAddRows = false;
            this.gridBuffTrayResult.AllowUserToDeleteRows = false;
            this.gridBuffTrayResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridBuffTrayResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.gridBuffTrayResult.ColumnHeadersHeight = 24;
            this.gridBuffTrayResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridBuffTrayResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewCheckBoxColumn7,
            this.dataGridViewCheckBoxColumn8,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewCheckBoxColumn10,
            this.dataGridViewCheckBoxColumn11,
            this.dataGridViewCheckBoxColumn12,
            this.dataGridViewCheckBoxColumn13,
            this.dataGridViewCheckBoxColumn14,
            this.dataGridViewCheckBoxColumn15,
            this.dataGridViewCheckBoxColumn16,
            this.dataGridViewCheckBoxColumn17,
            this.dataGridViewCheckBoxColumn18,
            this.dataGridViewCheckBoxColumn19,
            this.dataGridViewCheckBoxColumn20});
            this.gridBuffTrayResult.Location = new System.Drawing.Point(983, 4);
            this.gridBuffTrayResult.Name = "gridBuffTrayResult";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridBuffTrayResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.gridBuffTrayResult.RowHeadersWidth = 60;
            this.gridBuffTrayResult.RowTemplate.Height = 23;
            this.gridBuffTrayResult.Size = new System.Drawing.Size(663, 441);
            this.gridBuffTrayResult.TabIndex = 182;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "1";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn1.Width = 30;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "2";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn2.Width = 30;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "3";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn3.Width = 30;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "4";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn4.Width = 30;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "5";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn5.Width = 30;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "6";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn6.Width = 30;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "7";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn7.Width = 30;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "8";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn8.Width = 30;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "9";
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn9.Width = 30;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "10";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn10.Width = 30;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            this.dataGridViewCheckBoxColumn11.HeaderText = "11";
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn11.Width = 30;
            // 
            // dataGridViewCheckBoxColumn12
            // 
            this.dataGridViewCheckBoxColumn12.HeaderText = "12";
            this.dataGridViewCheckBoxColumn12.Name = "dataGridViewCheckBoxColumn12";
            this.dataGridViewCheckBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn12.Width = 30;
            // 
            // dataGridViewCheckBoxColumn13
            // 
            this.dataGridViewCheckBoxColumn13.HeaderText = "13";
            this.dataGridViewCheckBoxColumn13.Name = "dataGridViewCheckBoxColumn13";
            this.dataGridViewCheckBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn13.Width = 30;
            // 
            // dataGridViewCheckBoxColumn14
            // 
            this.dataGridViewCheckBoxColumn14.HeaderText = "14";
            this.dataGridViewCheckBoxColumn14.Name = "dataGridViewCheckBoxColumn14";
            this.dataGridViewCheckBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn14.Width = 30;
            // 
            // dataGridViewCheckBoxColumn15
            // 
            this.dataGridViewCheckBoxColumn15.HeaderText = "15";
            this.dataGridViewCheckBoxColumn15.Name = "dataGridViewCheckBoxColumn15";
            this.dataGridViewCheckBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn15.Width = 30;
            // 
            // dataGridViewCheckBoxColumn16
            // 
            this.dataGridViewCheckBoxColumn16.HeaderText = "16";
            this.dataGridViewCheckBoxColumn16.Name = "dataGridViewCheckBoxColumn16";
            this.dataGridViewCheckBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn16.Width = 30;
            // 
            // dataGridViewCheckBoxColumn17
            // 
            this.dataGridViewCheckBoxColumn17.HeaderText = "17";
            this.dataGridViewCheckBoxColumn17.Name = "dataGridViewCheckBoxColumn17";
            this.dataGridViewCheckBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn17.Width = 30;
            // 
            // dataGridViewCheckBoxColumn18
            // 
            this.dataGridViewCheckBoxColumn18.HeaderText = "18";
            this.dataGridViewCheckBoxColumn18.Name = "dataGridViewCheckBoxColumn18";
            this.dataGridViewCheckBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn18.Width = 30;
            // 
            // dataGridViewCheckBoxColumn19
            // 
            this.dataGridViewCheckBoxColumn19.HeaderText = "19";
            this.dataGridViewCheckBoxColumn19.Name = "dataGridViewCheckBoxColumn19";
            this.dataGridViewCheckBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn19.Width = 30;
            // 
            // dataGridViewCheckBoxColumn20
            // 
            this.dataGridViewCheckBoxColumn20.HeaderText = "20";
            this.dataGridViewCheckBoxColumn20.Name = "dataGridViewCheckBoxColumn20";
            this.dataGridViewCheckBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn20.Width = 30;
            // 
            // gridLoadTrayResult
            // 
            this.gridLoadTrayResult.AllowUserToAddRows = false;
            this.gridLoadTrayResult.AllowUserToDeleteRows = false;
            this.gridLoadTrayResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridLoadTrayResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.gridLoadTrayResult.ColumnHeadersHeight = 24;
            this.gridLoadTrayResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridLoadTrayResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20});
            this.gridLoadTrayResult.Location = new System.Drawing.Point(314, 4);
            this.gridLoadTrayResult.Name = "gridLoadTrayResult";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridLoadTrayResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.gridLoadTrayResult.RowHeadersWidth = 60;
            this.gridLoadTrayResult.RowTemplate.Height = 23;
            this.gridLoadTrayResult.Size = new System.Drawing.Size(663, 441);
            this.gridLoadTrayResult.TabIndex = 181;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "1";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn1.Width = 30;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "2";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn2.Width = 30;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "3";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn3.Width = 30;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "4";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn4.Width = 30;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "5";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn5.Width = 30;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "6";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn6.Width = 30;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "7";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn7.Width = 30;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "8";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn8.Width = 30;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "9";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn9.Width = 30;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "10";
            this.Column10.Name = "Column10";
            this.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column10.Width = 30;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "11";
            this.Column11.Name = "Column11";
            this.Column11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column11.Width = 30;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "12";
            this.Column12.Name = "Column12";
            this.Column12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column12.Width = 30;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "13";
            this.Column13.Name = "Column13";
            this.Column13.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column13.Width = 30;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "14";
            this.Column14.Name = "Column14";
            this.Column14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column14.Width = 30;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "15";
            this.Column15.Name = "Column15";
            this.Column15.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column15.Width = 30;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "16";
            this.Column16.Name = "Column16";
            this.Column16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column16.Width = 30;
            // 
            // Column17
            // 
            this.Column17.HeaderText = "17";
            this.Column17.Name = "Column17";
            this.Column17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column17.Width = 30;
            // 
            // Column18
            // 
            this.Column18.HeaderText = "18";
            this.Column18.Name = "Column18";
            this.Column18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column18.Width = 30;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "19";
            this.Column19.Name = "Column19";
            this.Column19.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column19.Width = 30;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "20";
            this.Column20.Name = "Column20";
            this.Column20.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column20.Width = 30;
            // 
            // groupControl10
            // 
            this.groupControl10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl10.Appearance.Options.UseFont = true;
            this.groupControl10.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl10.AppearanceCaption.Options.UseFont = true;
            this.groupControl10.Controls.Add(this.缓存盘设置为空盘);
            this.groupControl10.Controls.Add(this.labelControl44);
            this.groupControl10.Controls.Add(this.btmUpdateRow);
            this.groupControl10.Controls.Add(this.speStartBuffCol);
            this.groupControl10.Controls.Add(this.labelControl33);
            this.groupControl10.Controls.Add(this.labelControl45);
            this.groupControl10.Controls.Add(this.speCoutOfCatchUsed);
            this.groupControl10.Controls.Add(this.speStartBuffRow);
            this.groupControl10.Controls.Add(this.labelControl3);
            this.groupControl10.Controls.Add(this.btmSetBuffStartPos);
            this.groupControl10.Controls.Add(this.speColDst);
            this.groupControl10.Controls.Add(this.labelControl2);
            this.groupControl10.Controls.Add(this.speRowDst);
            this.groupControl10.Controls.Add(this.btmSetBuffTrayFull);
            this.groupControl10.Controls.Add(this.labelControl23);
            this.groupControl10.Controls.Add(this.speCols);
            this.groupControl10.Controls.Add(this.labelControl1);
            this.groupControl10.Controls.Add(this.speRows);
            this.groupControl10.Controls.Add(this.simpleButton3);
            this.groupControl10.Controls.Add(this.simpleButton4);
            this.groupControl10.Controls.Add(this.simpleButton5);
            this.groupControl10.Controls.Add(this.simpleButton6);
            this.groupControl10.Location = new System.Drawing.Point(1, 4);
            this.groupControl10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(307, 420);
            this.groupControl10.TabIndex = 177;
            this.groupControl10.Text = "产品设定";
            // 
            // 缓存盘设置为空盘
            // 
            this.缓存盘设置为空盘.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.缓存盘设置为空盘.Appearance.Options.UseFont = true;
            this.缓存盘设置为空盘.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.缓存盘设置为空盘.AppearanceDisabled.Options.UseFont = true;
            this.缓存盘设置为空盘.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.缓存盘设置为空盘.AppearanceHovered.Options.UseFont = true;
            this.缓存盘设置为空盘.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.缓存盘设置为空盘.AppearancePressed.Options.UseFont = true;
            this.缓存盘设置为空盘.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("缓存盘设置为空盘.ImageOptions.Image")));
            this.缓存盘设置为空盘.Location = new System.Drawing.Point(17, 281);
            this.缓存盘设置为空盘.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.缓存盘设置为空盘.Name = "缓存盘设置为空盘";
            this.缓存盘设置为空盘.Size = new System.Drawing.Size(145, 58);
            this.缓存盘设置为空盘.TabIndex = 191;
            this.缓存盘设置为空盘.Text = "缓存盘设置为空盘";
            this.缓存盘设置为空盘.Click += new System.EventHandler(this.缓存盘设置为空盘_Click);
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl44.AppearanceDisabled.Options.UseFont = true;
            this.labelControl44.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl44.AppearanceHovered.Options.UseFont = true;
            this.labelControl44.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl44.AppearancePressed.Options.UseFont = true;
            this.labelControl44.Location = new System.Drawing.Point(182, 384);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(36, 17);
            this.labelControl44.TabIndex = 189;
            this.labelControl44.Text = "起点例";
            // 
            // btmUpdateRow
            // 
            this.btmUpdateRow.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmUpdateRow.Appearance.Options.UseFont = true;
            this.btmUpdateRow.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmUpdateRow.AppearanceDisabled.Options.UseFont = true;
            this.btmUpdateRow.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmUpdateRow.AppearanceHovered.Options.UseFont = true;
            this.btmUpdateRow.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmUpdateRow.AppearancePressed.Options.UseFont = true;
            this.btmUpdateRow.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmUpdateRow.ImageOptions.Image")));
            this.btmUpdateRow.Location = new System.Drawing.Point(17, 154);
            this.btmUpdateRow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmUpdateRow.Name = "btmUpdateRow";
            this.btmUpdateRow.Size = new System.Drawing.Size(285, 53);
            this.btmUpdateRow.TabIndex = 17;
            this.btmUpdateRow.Text = "更新";
            this.btmUpdateRow.Click += new System.EventHandler(this.btmUpdateRow_Click);
            // 
            // speStartBuffCol
            // 
            this.speStartBuffCol.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speStartBuffCol.Location = new System.Drawing.Point(224, 380);
            this.speStartBuffCol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speStartBuffCol.Name = "speStartBuffCol";
            this.speStartBuffCol.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speStartBuffCol.Properties.Appearance.Options.UseFont = true;
            this.speStartBuffCol.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speStartBuffCol.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speStartBuffCol.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speStartBuffCol.Properties.AppearanceFocused.Options.UseFont = true;
            this.speStartBuffCol.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speStartBuffCol.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speStartBuffCol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speStartBuffCol.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.speStartBuffCol.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speStartBuffCol.Size = new System.Drawing.Size(60, 24);
            this.speStartBuffCol.TabIndex = 188;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Appearance.Options.UseFont = true;
            this.labelControl33.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl33.AppearanceDisabled.Options.UseFont = true;
            this.labelControl33.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl33.AppearanceHovered.Options.UseFont = true;
            this.labelControl33.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl33.AppearancePressed.Options.UseFont = true;
            this.labelControl33.Location = new System.Drawing.Point(17, 124);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(72, 17);
            this.labelControl33.TabIndex = 16;
            this.labelControl33.Text = "每例吸嘴个数";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl45.AppearanceDisabled.Options.UseFont = true;
            this.labelControl45.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl45.AppearanceHovered.Options.UseFont = true;
            this.labelControl45.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl45.AppearancePressed.Options.UseFont = true;
            this.labelControl45.Location = new System.Drawing.Point(182, 353);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(36, 17);
            this.labelControl45.TabIndex = 187;
            this.labelControl45.Text = "起点行";
            // 
            // speCoutOfCatchUsed
            // 
            this.speCoutOfCatchUsed.EditValue = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.speCoutOfCatchUsed.Location = new System.Drawing.Point(95, 122);
            this.speCoutOfCatchUsed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speCoutOfCatchUsed.Name = "speCoutOfCatchUsed";
            this.speCoutOfCatchUsed.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCoutOfCatchUsed.Properties.Appearance.Options.UseFont = true;
            this.speCoutOfCatchUsed.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCoutOfCatchUsed.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speCoutOfCatchUsed.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCoutOfCatchUsed.Properties.AppearanceFocused.Options.UseFont = true;
            this.speCoutOfCatchUsed.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCoutOfCatchUsed.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speCoutOfCatchUsed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speCoutOfCatchUsed.Properties.MaxValue = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.speCoutOfCatchUsed.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speCoutOfCatchUsed.Size = new System.Drawing.Size(56, 24);
            this.speCoutOfCatchUsed.TabIndex = 15;
            // 
            // speStartBuffRow
            // 
            this.speStartBuffRow.EditValue = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.speStartBuffRow.Location = new System.Drawing.Point(224, 348);
            this.speStartBuffRow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speStartBuffRow.Name = "speStartBuffRow";
            this.speStartBuffRow.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speStartBuffRow.Properties.Appearance.Options.UseFont = true;
            this.speStartBuffRow.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speStartBuffRow.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speStartBuffRow.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speStartBuffRow.Properties.AppearanceFocused.Options.UseFont = true;
            this.speStartBuffRow.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speStartBuffRow.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speStartBuffRow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speStartBuffRow.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.speStartBuffRow.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speStartBuffRow.Size = new System.Drawing.Size(60, 24);
            this.speStartBuffRow.TabIndex = 186;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearanceDisabled.Options.UseFont = true;
            this.labelControl3.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearanceHovered.Options.UseFont = true;
            this.labelControl3.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearancePressed.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(157, 80);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 17);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "间距(mm)";
            // 
            // btmSetBuffStartPos
            // 
            this.btmSetBuffStartPos.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetBuffStartPos.Appearance.Options.UseFont = true;
            this.btmSetBuffStartPos.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetBuffStartPos.AppearanceDisabled.Options.UseFont = true;
            this.btmSetBuffStartPos.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetBuffStartPos.AppearanceHovered.Options.UseFont = true;
            this.btmSetBuffStartPos.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetBuffStartPos.AppearancePressed.Options.UseFont = true;
            this.btmSetBuffStartPos.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetBuffStartPos.ImageOptions.Image")));
            this.btmSetBuffStartPos.Location = new System.Drawing.Point(17, 342);
            this.btmSetBuffStartPos.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetBuffStartPos.Name = "btmSetBuffStartPos";
            this.btmSetBuffStartPos.Size = new System.Drawing.Size(145, 62);
            this.btmSetBuffStartPos.TabIndex = 185;
            this.btmSetBuffStartPos.Text = "设为取料起点";
            this.btmSetBuffStartPos.Click += new System.EventHandler(this.btmSetBuffStartPos_Click);
            // 
            // speColDst
            // 
            this.speColDst.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speColDst.Location = new System.Drawing.Point(214, 78);
            this.speColDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speColDst.Name = "speColDst";
            this.speColDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColDst.Properties.Appearance.Options.UseFont = true;
            this.speColDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speColDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.speColDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speColDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speColDst.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speColDst.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.speColDst.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.speColDst.Size = new System.Drawing.Size(58, 24);
            this.speColDst.TabIndex = 13;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearanceDisabled.Options.UseFont = true;
            this.labelControl2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearanceHovered.Options.UseFont = true;
            this.labelControl2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearancePressed.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(157, 49);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 17);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "间距(mm)";
            // 
            // speRowDst
            // 
            this.speRowDst.EditValue = new decimal(new int[] {
            500,
            0,
            0,
            131072});
            this.speRowDst.Location = new System.Drawing.Point(214, 47);
            this.speRowDst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speRowDst.Name = "speRowDst";
            this.speRowDst.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowDst.Properties.Appearance.Options.UseFont = true;
            this.speRowDst.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowDst.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speRowDst.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowDst.Properties.AppearanceFocused.Options.UseFont = true;
            this.speRowDst.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRowDst.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speRowDst.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speRowDst.Properties.Increment = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.speRowDst.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.speRowDst.Size = new System.Drawing.Size(58, 24);
            this.speRowDst.TabIndex = 11;
            // 
            // btmSetBuffTrayFull
            // 
            this.btmSetBuffTrayFull.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetBuffTrayFull.Appearance.Options.UseFont = true;
            this.btmSetBuffTrayFull.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetBuffTrayFull.AppearanceDisabled.Options.UseFont = true;
            this.btmSetBuffTrayFull.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetBuffTrayFull.AppearanceHovered.Options.UseFont = true;
            this.btmSetBuffTrayFull.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetBuffTrayFull.AppearancePressed.Options.UseFont = true;
            this.btmSetBuffTrayFull.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetBuffTrayFull.ImageOptions.Image")));
            this.btmSetBuffTrayFull.Location = new System.Drawing.Point(17, 215);
            this.btmSetBuffTrayFull.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetBuffTrayFull.Name = "btmSetBuffTrayFull";
            this.btmSetBuffTrayFull.Size = new System.Drawing.Size(145, 58);
            this.btmSetBuffTrayFull.TabIndex = 184;
            this.btmSetBuffTrayFull.Text = "缓存盘设置为满盘";
            this.btmSetBuffTrayFull.Click += new System.EventHandler(this.btmSetBuffTrayFull_Click);
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearanceDisabled.Options.UseFont = true;
            this.labelControl23.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearanceHovered.Options.UseFont = true;
            this.labelControl23.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearancePressed.Options.UseFont = true;
            this.labelControl23.Location = new System.Drawing.Point(17, 80);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(48, 17);
            this.labelControl23.TabIndex = 10;
            this.labelControl23.Text = "产品例数";
            // 
            // speCols
            // 
            this.speCols.EditValue = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.speCols.Location = new System.Drawing.Point(71, 78);
            this.speCols.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speCols.Name = "speCols";
            this.speCols.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCols.Properties.Appearance.Options.UseFont = true;
            this.speCols.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCols.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speCols.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCols.Properties.AppearanceFocused.Options.UseFont = true;
            this.speCols.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCols.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speCols.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speCols.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.speCols.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speCols.Size = new System.Drawing.Size(80, 24);
            this.speCols.TabIndex = 9;
            this.speCols.EditValueChanged += new System.EventHandler(this.speCols_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearanceDisabled.Options.UseFont = true;
            this.labelControl1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearanceHovered.Options.UseFont = true;
            this.labelControl1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearancePressed.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(17, 49);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 17);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "产品行数";
            // 
            // speRows
            // 
            this.speRows.EditValue = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.speRows.Location = new System.Drawing.Point(71, 46);
            this.speRows.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.speRows.Name = "speRows";
            this.speRows.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speRows.Properties.Appearance.Options.UseFont = true;
            this.speRows.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRows.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speRows.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRows.Properties.AppearanceFocused.Options.UseFont = true;
            this.speRows.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speRows.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speRows.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speRows.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.speRows.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speRows.Size = new System.Drawing.Size(80, 24);
            this.speRows.TabIndex = 7;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton3.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton3.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton3.AppearanceHovered.Options.UseFont = true;
            this.simpleButton3.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton3.AppearancePressed.Options.UseFont = true;
            this.simpleButton3.Location = new System.Drawing.Point(694, 200);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(134, 68);
            this.simpleButton3.TabIndex = 5;
            this.simpleButton3.Text = "清除所有";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceHovered.Options.UseFont = true;
            this.simpleButton4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearancePressed.Options.UseFont = true;
            this.simpleButton4.Location = new System.Drawing.Point(694, 125);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(134, 68);
            this.simpleButton4.TabIndex = 4;
            this.simpleButton4.Text = "删除路径";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton5.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton5.AppearanceHovered.Options.UseFont = true;
            this.simpleButton5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton5.AppearancePressed.Options.UseFont = true;
            this.simpleButton5.Location = new System.Drawing.Point(694, 276);
            this.simpleButton5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(134, 68);
            this.simpleButton5.TabIndex = 3;
            this.simpleButton5.Text = "学习当前(行)路径";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton6.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearanceHovered.Options.UseFont = true;
            this.simpleButton6.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearancePressed.Options.UseFont = true;
            this.simpleButton6.Location = new System.Drawing.Point(694, 50);
            this.simpleButton6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(134, 68);
            this.simpleButton6.TabIndex = 2;
            this.simpleButton6.Text = "增加路径";
            // 
            // NP_视觉参数
            // 
            this.NP_视觉参数.Caption = "视觉参数";
            this.NP_视觉参数.Controls.Add(this.图像循环测试);
            this.NP_视觉参数.Controls.Add(this.chk采集新的图片);
            this.NP_视觉参数.Controls.Add(this.spedDelayCameraSet);
            this.NP_视觉参数.Controls.Add(this.label74);
            this.NP_视觉参数.Controls.Add(this.chkLaserMarkTest_Down);
            this.NP_视觉参数.Controls.Add(this.chkLaserMarkTest_Up);
            this.NP_视觉参数.Controls.Add(this.txtImageProcessFile);
            this.NP_视觉参数.Controls.Add(this.btmLoad);
            this.NP_视觉参数.Controls.Add(this.label28);
            this.NP_视觉参数.Controls.Add(this.chkNewTemplate);
            this.NP_视觉参数.Controls.Add(this.btmSaveVisionPara);
            this.NP_视觉参数.Controls.Add(this.tabPane1);
            this.NP_视觉参数.Image = ((System.Drawing.Image)(resources.GetObject("NP_视觉参数.Image")));
            this.NP_视觉参数.Name = "NP_视觉参数";
            this.NP_视觉参数.Size = new System.Drawing.Size(1684, 647);
            // 
            // 图像循环测试
            // 
            this.图像循环测试.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.图像循环测试.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.图像循环测试.Location = new System.Drawing.Point(12, 284);
            this.图像循环测试.Name = "图像循环测试";
            this.图像循环测试.Size = new System.Drawing.Size(192, 120);
            this.图像循环测试.TabIndex = 218;
            this.图像循环测试.Text = "图像循环测试";
            this.图像循环测试.UseVisualStyleBackColor = false;
            this.图像循环测试.Click += new System.EventHandler(this.图像循环测试_Click);
            // 
            // chk采集新的图片
            // 
            this.chk采集新的图片.EditValue = true;
            this.chk采集新的图片.Location = new System.Drawing.Point(1177, 11);
            this.chk采集新的图片.Name = "chk采集新的图片";
            this.chk采集新的图片.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk采集新的图片.Properties.Appearance.Options.UseFont = true;
            this.chk采集新的图片.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chk采集新的图片.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chk采集新的图片.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chk采集新的图片.Properties.AppearanceFocused.Options.UseFont = true;
            this.chk采集新的图片.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chk采集新的图片.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chk采集新的图片.Properties.Caption = "采集新的图片";
            this.chk采集新的图片.Size = new System.Drawing.Size(131, 21);
            this.chk采集新的图片.TabIndex = 217;
            // 
            // spedDelayCameraSet
            // 
            this.spedDelayCameraSet.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spedDelayCameraSet.Location = new System.Drawing.Point(95, 169);
            this.spedDelayCameraSet.Name = "spedDelayCameraSet";
            this.spedDelayCameraSet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedDelayCameraSet.Size = new System.Drawing.Size(74, 20);
            this.spedDelayCameraSet.TabIndex = 216;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label74.Location = new System.Drawing.Point(9, 171);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(80, 17);
            this.label74.TabIndex = 215;
            this.label74.Text = "相机设置延时";
            // 
            // chkLaserMarkTest_Down
            // 
            this.chkLaserMarkTest_Down.Location = new System.Drawing.Point(12, 87);
            this.chkLaserMarkTest_Down.Name = "chkLaserMarkTest_Down";
            this.chkLaserMarkTest_Down.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLaserMarkTest_Down.Properties.Appearance.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Down.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Down.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Down.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkLaserMarkTest_Down.Properties.Caption = "启用底部相机(2)打码检测";
            this.chkLaserMarkTest_Down.Size = new System.Drawing.Size(131, 21);
            this.chkLaserMarkTest_Down.TabIndex = 214;
            // 
            // chkLaserMarkTest_Up
            // 
            this.chkLaserMarkTest_Up.Location = new System.Drawing.Point(12, 60);
            this.chkLaserMarkTest_Up.Name = "chkLaserMarkTest_Up";
            this.chkLaserMarkTest_Up.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLaserMarkTest_Up.Properties.Appearance.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Up.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Up.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkLaserMarkTest_Up.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkLaserMarkTest_Up.Properties.Caption = "启用顶部相机(1)打码检测";
            this.chkLaserMarkTest_Up.Size = new System.Drawing.Size(131, 21);
            this.chkLaserMarkTest_Up.TabIndex = 213;
            // 
            // txtImageProcessFile
            // 
            this.txtImageProcessFile.FormattingEnabled = true;
            this.txtImageProcessFile.Location = new System.Drawing.Point(305, 10);
            this.txtImageProcessFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtImageProcessFile.Name = "txtImageProcessFile";
            this.txtImageProcessFile.Size = new System.Drawing.Size(774, 25);
            this.txtImageProcessFile.TabIndex = 212;
            this.txtImageProcessFile.Text = "D:\\My Program\\C#\\ZHZN\\HP All SW\\Apk_HP_Application\\Apk_HP_Application\\bin\\Debug\\C" +
    "onfigFile\\Up3.bmp";
            // 
            // btmLoad
            // 
            this.btmLoad.BackColor = System.Drawing.Color.Moccasin;
            this.btmLoad.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmLoad.Location = new System.Drawing.Point(1085, 8);
            this.btmLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmLoad.Name = "btmLoad";
            this.btmLoad.Size = new System.Drawing.Size(74, 29);
            this.btmLoad.TabIndex = 211;
            this.btmLoad.Text = "打开(&O)";
            this.btmLoad.UseVisualStyleBackColor = false;
            this.btmLoad.Click += new System.EventHandler(this.btmLoad_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(207, 13);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(92, 17);
            this.label28.TabIndex = 210;
            this.label28.Text = "请选择测试文件";
            // 
            // chkNewTemplate
            // 
            this.chkNewTemplate.Location = new System.Drawing.Point(12, 33);
            this.chkNewTemplate.Name = "chkNewTemplate";
            this.chkNewTemplate.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNewTemplate.Properties.Appearance.Options.UseFont = true;
            this.chkNewTemplate.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkNewTemplate.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkNewTemplate.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkNewTemplate.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkNewTemplate.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkNewTemplate.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkNewTemplate.Properties.Caption = "重新做模版";
            this.chkNewTemplate.Size = new System.Drawing.Size(131, 21);
            this.chkNewTemplate.TabIndex = 209;
            // 
            // btmSaveVisionPara
            // 
            this.btmSaveVisionPara.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmSaveVisionPara.Appearance.Options.UseFont = true;
            this.btmSaveVisionPara.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSaveVisionPara.AppearanceDisabled.Options.UseFont = true;
            this.btmSaveVisionPara.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Italic);
            this.btmSaveVisionPara.AppearanceHovered.Options.UseFont = true;
            this.btmSaveVisionPara.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSaveVisionPara.AppearancePressed.Options.UseFont = true;
            this.btmSaveVisionPara.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmSaveVisionPara.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSaveVisionPara.ImageOptions.Image")));
            this.btmSaveVisionPara.Location = new System.Drawing.Point(12, 483);
            this.btmSaveVisionPara.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSaveVisionPara.Name = "btmSaveVisionPara";
            this.btmSaveVisionPara.Size = new System.Drawing.Size(192, 160);
            this.btmSaveVisionPara.TabIndex = 208;
            this.btmSaveVisionPara.Text = "保存所有";
            this.btmSaveVisionPara.Click += new System.EventHandler(this.btmSaveVisionPara_Click);
            // 
            // tabPane1
            // 
            this.tabPane1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPane1.Appearance.Options.UseFont = true;
            this.tabPane1.AppearanceButton.Hovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.AppearanceButton.Hovered.Options.UseFont = true;
            this.tabPane1.AppearanceButton.Normal.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.AppearanceButton.Normal.Options.UseFont = true;
            this.tabPane1.AppearanceButton.Pressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.AppearanceButton.Pressed.Options.UseFont = true;
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Controls.Add(this.tabNavigationPage3);
            this.tabPane1.Location = new System.Drawing.Point(210, 42);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.PageProperties.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabPane1.PageProperties.AppearanceCaption.Options.UseFont = true;
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2,
            this.tabNavigationPage3});
            this.tabPane1.RegularSize = new System.Drawing.Size(1474, 605);
            this.tabPane1.SelectedPage = this.tabNavigationPage1;
            this.tabPane1.Size = new System.Drawing.Size(1474, 605);
            this.tabPane1.TabIndex = 205;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "相机1";
            this.tabNavigationPage1.Controls.Add(this.labelControl60);
            this.tabNavigationPage1.Controls.Add(this.spinEdit2);
            this.tabNavigationPage1.Controls.Add(this.spinEdit1);
            this.tabNavigationPage1.Controls.Add(this.label29);
            this.tabNavigationPage1.Controls.Add(this.trackContrast_1);
            this.tabNavigationPage1.Controls.Add(this.trackExposure_1);
            this.tabNavigationPage1.Controls.Add(this.测试删除1);
            this.tabNavigationPage1.Controls.Add(this.测试增加1);
            this.tabNavigationPage1.Controls.Add(this.listBoxControl1);
            this.tabNavigationPage1.Controls.Add(this.运行测试1);
            this.tabNavigationPage1.Controls.Add(this.propertyGrid1);
            this.tabNavigationPage1.Controls.Add(this.panelControl3);
            this.tabNavigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage1.Image")));
            this.tabNavigationPage1.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage1.Size = new System.Drawing.Size(1456, 541);
            // 
            // labelControl60
            // 
            this.labelControl60.Location = new System.Drawing.Point(826, 34);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(25, 14);
            this.labelControl60.TabIndex = 215;
            this.labelControl60.Text = "(ms)";
            // 
            // spinEdit2
            // 
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(1149, 31);
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit2.Size = new System.Drawing.Size(74, 20);
            this.spinEdit2.TabIndex = 214;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(750, 31);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(74, 20);
            this.spinEdit1.TabIndex = 213;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label29.Location = new System.Drawing.Point(424, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(109, 17);
            this.label29.TabIndex = 212;
            this.label29.Text = "相机亮度/曝光调整";
            // 
            // trackContrast_1
            // 
            this.trackContrast_1.EditValue = 50;
            this.trackContrast_1.Location = new System.Drawing.Point(909, 6);
            this.trackContrast_1.Name = "trackContrast_1";
            this.trackContrast_1.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackContrast_1.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackContrast_1.Properties.Maximum = 100;
            this.trackContrast_1.Properties.Minimum = 10;
            this.trackContrast_1.Properties.ShowLabels = true;
            this.trackContrast_1.Size = new System.Drawing.Size(314, 45);
            this.trackContrast_1.TabIndex = 211;
            this.trackContrast_1.Value = 50;
            this.trackContrast_1.EditValueChanged += new System.EventHandler(this.trackContrast_1_EditValueChanged);
            // 
            // trackExposure_1
            // 
            this.trackExposure_1.EditValue = 25;
            this.trackExposure_1.Location = new System.Drawing.Point(551, 6);
            this.trackExposure_1.Name = "trackExposure_1";
            this.trackExposure_1.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackExposure_1.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackExposure_1.Properties.Maximum = 500;
            this.trackExposure_1.Properties.Minimum = 1;
            this.trackExposure_1.Properties.ShowLabels = true;
            this.trackExposure_1.Size = new System.Drawing.Size(314, 45);
            this.trackExposure_1.TabIndex = 210;
            this.trackExposure_1.Value = 25;
            this.trackExposure_1.EditValueChanged += new System.EventHandler(this.trackExposure_1_EditValueChanged);
            // 
            // 测试删除1
            // 
            this.测试删除1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.测试删除1.Appearance.Options.UseFont = true;
            this.测试删除1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试删除1.AppearanceDisabled.Options.UseFont = true;
            this.测试删除1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试删除1.AppearanceHovered.Options.UseFont = true;
            this.测试删除1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试删除1.AppearancePressed.Options.UseFont = true;
            this.测试删除1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.测试删除1.Location = new System.Drawing.Point(3, 287);
            this.测试删除1.Name = "测试删除1";
            this.测试删除1.Size = new System.Drawing.Size(105, 122);
            this.测试删除1.TabIndex = 209;
            this.测试删除1.Text = "删除";
            this.测试删除1.Click += new System.EventHandler(this.测试删除1_Click);
            // 
            // 测试增加1
            // 
            this.测试增加1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.测试增加1.Appearance.Options.UseFont = true;
            this.测试增加1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试增加1.AppearanceDisabled.Options.UseFont = true;
            this.测试增加1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试增加1.AppearanceHovered.Options.UseFont = true;
            this.测试增加1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试增加1.AppearancePressed.Options.UseFont = true;
            this.测试增加1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.测试增加1.Location = new System.Drawing.Point(3, 146);
            this.测试增加1.Name = "测试增加1";
            this.测试增加1.Size = new System.Drawing.Size(105, 122);
            this.测试增加1.TabIndex = 208;
            this.测试增加1.Text = "增加";
            this.测试增加1.Click += new System.EventHandler(this.测试增加1_Click);
            // 
            // listBoxControl1
            // 
            this.listBoxControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.listBoxControl1.Appearance.Options.UseFont = true;
            this.listBoxControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.listBoxControl1.Items.AddRange(new object[] {
            "所有完整性检测",
            "打码有无测检测(备用)"});
            this.listBoxControl1.Location = new System.Drawing.Point(114, 1);
            this.listBoxControl1.Name = "listBoxControl1";
            this.listBoxControl1.Size = new System.Drawing.Size(296, 104);
            this.listBoxControl1.TabIndex = 207;
            this.listBoxControl1.SelectedIndexChanged += new System.EventHandler(this.listBoxControl1_SelectedIndexChanged);
            // 
            // 运行测试1
            // 
            this.运行测试1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.运行测试1.Appearance.Options.UseFont = true;
            this.运行测试1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.运行测试1.AppearanceDisabled.Options.UseFont = true;
            this.运行测试1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.运行测试1.AppearanceHovered.Options.UseFont = true;
            this.运行测试1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.运行测试1.AppearancePressed.Options.UseFont = true;
            this.运行测试1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.运行测试1.Location = new System.Drawing.Point(3, 3);
            this.运行测试1.Name = "运行测试1";
            this.运行测试1.Size = new System.Drawing.Size(105, 122);
            this.运行测试1.TabIndex = 207;
            this.运行测试1.Text = "运行测试";
            this.运行测试1.Click += new System.EventHandler(this.simpleButton32_Click);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Location = new System.Drawing.Point(114, 108);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(296, 430);
            this.propertyGrid1.TabIndex = 206;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            this.propertyGrid1.BindingContextChanged += new System.EventHandler(this.propertyGrid1_BindingContextChanged);
            this.propertyGrid1.Click += new System.EventHandler(this.propertyGrid1_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Location = new System.Drawing.Point(416, 57);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(826, 466);
            this.panelControl3.TabIndex = 205;
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabNavigationPage2.Appearance.Options.UseFont = true;
            this.tabNavigationPage2.Caption = "相机2";
            this.tabNavigationPage2.Controls.Add(this.labelControl62);
            this.tabNavigationPage2.Controls.Add(this.spinEdit3);
            this.tabNavigationPage2.Controls.Add(this.label66);
            this.tabNavigationPage2.Controls.Add(this.spinEdit4);
            this.tabNavigationPage2.Controls.Add(this.测试删除2);
            this.tabNavigationPage2.Controls.Add(this.测试增加2);
            this.tabNavigationPage2.Controls.Add(this.trackContrast_2);
            this.tabNavigationPage2.Controls.Add(this.运行测试2);
            this.tabNavigationPage2.Controls.Add(this.trackExposure_2);
            this.tabNavigationPage2.Controls.Add(this.listBoxControl2);
            this.tabNavigationPage2.Controls.Add(this.propertyGrid2);
            this.tabNavigationPage2.Controls.Add(this.panelControl4);
            this.tabNavigationPage2.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage2.Image")));
            this.tabNavigationPage2.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Properties.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.tabNavigationPage2.Properties.AppearanceCaption.Options.UseFont = true;
            this.tabNavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage2.Size = new System.Drawing.Size(1474, 605);
            // 
            // labelControl62
            // 
            this.labelControl62.Location = new System.Drawing.Point(826, 34);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(25, 14);
            this.labelControl62.TabIndex = 217;
            this.labelControl62.Text = "(ms)";
            // 
            // spinEdit3
            // 
            this.spinEdit3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit3.Location = new System.Drawing.Point(1149, 31);
            this.spinEdit3.Name = "spinEdit3";
            this.spinEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit3.Size = new System.Drawing.Size(74, 20);
            this.spinEdit3.TabIndex = 216;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label66.Location = new System.Drawing.Point(424, 16);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(109, 17);
            this.label66.TabIndex = 215;
            this.label66.Text = "相机亮度/曝光调整";
            // 
            // spinEdit4
            // 
            this.spinEdit4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit4.Location = new System.Drawing.Point(750, 31);
            this.spinEdit4.Name = "spinEdit4";
            this.spinEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit4.Size = new System.Drawing.Size(74, 20);
            this.spinEdit4.TabIndex = 215;
            // 
            // 测试删除2
            // 
            this.测试删除2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.测试删除2.Appearance.Options.UseFont = true;
            this.测试删除2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试删除2.AppearanceDisabled.Options.UseFont = true;
            this.测试删除2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试删除2.AppearanceHovered.Options.UseFont = true;
            this.测试删除2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试删除2.AppearancePressed.Options.UseFont = true;
            this.测试删除2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.测试删除2.Location = new System.Drawing.Point(3, 287);
            this.测试删除2.Name = "测试删除2";
            this.测试删除2.Size = new System.Drawing.Size(105, 122);
            this.测试删除2.TabIndex = 212;
            this.测试删除2.Text = "删除";
            this.测试删除2.Click += new System.EventHandler(this.测试删除2_Click);
            // 
            // 测试增加2
            // 
            this.测试增加2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.测试增加2.Appearance.Options.UseFont = true;
            this.测试增加2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试增加2.AppearanceDisabled.Options.UseFont = true;
            this.测试增加2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试增加2.AppearanceHovered.Options.UseFont = true;
            this.测试增加2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.测试增加2.AppearancePressed.Options.UseFont = true;
            this.测试增加2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.测试增加2.Location = new System.Drawing.Point(3, 146);
            this.测试增加2.Name = "测试增加2";
            this.测试增加2.Size = new System.Drawing.Size(105, 122);
            this.测试增加2.TabIndex = 211;
            this.测试增加2.Text = "增加";
            this.测试增加2.Click += new System.EventHandler(this.测试增加2_Click);
            // 
            // trackContrast_2
            // 
            this.trackContrast_2.EditValue = 50;
            this.trackContrast_2.Location = new System.Drawing.Point(909, 6);
            this.trackContrast_2.Name = "trackContrast_2";
            this.trackContrast_2.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackContrast_2.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackContrast_2.Properties.Maximum = 100;
            this.trackContrast_2.Properties.Minimum = 10;
            this.trackContrast_2.Size = new System.Drawing.Size(314, 45);
            this.trackContrast_2.TabIndex = 214;
            this.trackContrast_2.Value = 50;
            this.trackContrast_2.EditValueChanged += new System.EventHandler(this.trackContrast_2_EditValueChanged);
            // 
            // 运行测试2
            // 
            this.运行测试2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.运行测试2.Appearance.Options.UseFont = true;
            this.运行测试2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.运行测试2.AppearanceDisabled.Options.UseFont = true;
            this.运行测试2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.运行测试2.AppearanceHovered.Options.UseFont = true;
            this.运行测试2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.运行测试2.AppearancePressed.Options.UseFont = true;
            this.运行测试2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.运行测试2.Location = new System.Drawing.Point(3, 3);
            this.运行测试2.Name = "运行测试2";
            this.运行测试2.Size = new System.Drawing.Size(105, 122);
            this.运行测试2.TabIndex = 210;
            this.运行测试2.Text = "运行测试";
            this.运行测试2.Click += new System.EventHandler(this.运行测试2_Click);
            // 
            // trackExposure_2
            // 
            this.trackExposure_2.EditValue = 10;
            this.trackExposure_2.Location = new System.Drawing.Point(551, 6);
            this.trackExposure_2.Name = "trackExposure_2";
            this.trackExposure_2.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackExposure_2.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackExposure_2.Properties.Maximum = 500;
            this.trackExposure_2.Properties.Minimum = 1;
            this.trackExposure_2.Size = new System.Drawing.Size(314, 45);
            this.trackExposure_2.TabIndex = 213;
            this.trackExposure_2.Value = 10;
            this.trackExposure_2.EditValueChanged += new System.EventHandler(this.trackExposure_2_EditValueChanged);
            // 
            // listBoxControl2
            // 
            this.listBoxControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxControl2.Appearance.Options.UseFont = true;
            this.listBoxControl2.Cursor = System.Windows.Forms.Cursors.Default;
            this.listBoxControl2.Items.AddRange(new object[] {
            "所有完整性检测",
            "打码有无测检测(备用)"});
            this.listBoxControl2.Location = new System.Drawing.Point(114, 1);
            this.listBoxControl2.Name = "listBoxControl2";
            this.listBoxControl2.Size = new System.Drawing.Size(296, 104);
            this.listBoxControl2.TabIndex = 209;
            this.listBoxControl2.SelectedIndexChanged += new System.EventHandler(this.listBoxControl2_SelectedIndexChanged);
            // 
            // propertyGrid2
            // 
            this.propertyGrid2.Location = new System.Drawing.Point(114, 108);
            this.propertyGrid2.Name = "propertyGrid2";
            this.propertyGrid2.Size = new System.Drawing.Size(296, 430);
            this.propertyGrid2.TabIndex = 208;
            this.propertyGrid2.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid2_PropertyValueChanged);
            this.propertyGrid2.BindingContextChanged += new System.EventHandler(this.propertyGrid2_BindingContextChanged);
            // 
            // panelControl4
            // 
            this.panelControl4.Location = new System.Drawing.Point(416, 57);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(826, 466);
            this.panelControl4.TabIndex = 204;
            // 
            // tabNavigationPage3
            // 
            this.tabNavigationPage3.Caption = "光源设定";
            this.tabNavigationPage3.Controls.Add(this.label77);
            this.tabNavigationPage3.Controls.Add(this.spedLS_Value_4);
            this.tabNavigationPage3.Controls.Add(this.trackLightSource_4);
            this.tabNavigationPage3.Controls.Add(this.label78);
            this.tabNavigationPage3.Controls.Add(this.spedLS_Value_3);
            this.tabNavigationPage3.Controls.Add(this.trackLightSource_3);
            this.tabNavigationPage3.Controls.Add(this.label76);
            this.tabNavigationPage3.Controls.Add(this.spedLS_Value_2);
            this.tabNavigationPage3.Controls.Add(this.trackLightSource_2);
            this.tabNavigationPage3.Controls.Add(this.label75);
            this.tabNavigationPage3.Controls.Add(this.spedLS_Value_1);
            this.tabNavigationPage3.Controls.Add(this.trackLightSource_1);
            this.tabNavigationPage3.Image = ((System.Drawing.Image)(resources.GetObject("tabNavigationPage3.Image")));
            this.tabNavigationPage3.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage3.Name = "tabNavigationPage3";
            this.tabNavigationPage3.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNavigationPage3.Size = new System.Drawing.Size(1474, 605);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label77.Location = new System.Drawing.Point(12, 278);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(90, 17);
            this.label77.TabIndex = 225;
            this.label77.Text = "通道4(相机2后)";
            // 
            // spedLS_Value_4
            // 
            this.spedLS_Value_4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedLS_Value_4.Location = new System.Drawing.Point(712, 304);
            this.spedLS_Value_4.Name = "spedLS_Value_4";
            this.spedLS_Value_4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedLS_Value_4.Size = new System.Drawing.Size(74, 20);
            this.spedLS_Value_4.TabIndex = 224;
            // 
            // trackLightSource_4
            // 
            this.trackLightSource_4.EditValue = 25;
            this.trackLightSource_4.Location = new System.Drawing.Point(44, 301);
            this.trackLightSource_4.Name = "trackLightSource_4";
            this.trackLightSource_4.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackLightSource_4.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackLightSource_4.Properties.Maximum = 255;
            this.trackLightSource_4.Properties.ShowLabels = true;
            this.trackLightSource_4.Size = new System.Drawing.Size(662, 45);
            this.trackLightSource_4.TabIndex = 223;
            this.trackLightSource_4.Value = 25;
            this.trackLightSource_4.EditValueChanged += new System.EventHandler(this.trackLightSource_4_EditValueChanged);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label78.Location = new System.Drawing.Point(12, 190);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(90, 17);
            this.label78.TabIndex = 222;
            this.label78.Text = "通道3(相机2前)";
            // 
            // spedLS_Value_3
            // 
            this.spedLS_Value_3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedLS_Value_3.Location = new System.Drawing.Point(712, 216);
            this.spedLS_Value_3.Name = "spedLS_Value_3";
            this.spedLS_Value_3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedLS_Value_3.Size = new System.Drawing.Size(74, 20);
            this.spedLS_Value_3.TabIndex = 221;
            // 
            // trackLightSource_3
            // 
            this.trackLightSource_3.EditValue = 25;
            this.trackLightSource_3.Location = new System.Drawing.Point(44, 213);
            this.trackLightSource_3.Name = "trackLightSource_3";
            this.trackLightSource_3.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackLightSource_3.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackLightSource_3.Properties.Maximum = 255;
            this.trackLightSource_3.Properties.ShowLabels = true;
            this.trackLightSource_3.Size = new System.Drawing.Size(662, 45);
            this.trackLightSource_3.TabIndex = 220;
            this.trackLightSource_3.Value = 25;
            this.trackLightSource_3.EditValueChanged += new System.EventHandler(this.trackLightSource_3_EditValueChanged);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label76.Location = new System.Drawing.Point(12, 103);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(90, 17);
            this.label76.TabIndex = 219;
            this.label76.Text = "通道2(相机1后)";
            // 
            // spedLS_Value_2
            // 
            this.spedLS_Value_2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedLS_Value_2.Location = new System.Drawing.Point(712, 129);
            this.spedLS_Value_2.Name = "spedLS_Value_2";
            this.spedLS_Value_2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedLS_Value_2.Size = new System.Drawing.Size(74, 20);
            this.spedLS_Value_2.TabIndex = 218;
            // 
            // trackLightSource_2
            // 
            this.trackLightSource_2.EditValue = 25;
            this.trackLightSource_2.Location = new System.Drawing.Point(44, 126);
            this.trackLightSource_2.Name = "trackLightSource_2";
            this.trackLightSource_2.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackLightSource_2.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackLightSource_2.Properties.Maximum = 255;
            this.trackLightSource_2.Properties.ShowLabels = true;
            this.trackLightSource_2.Size = new System.Drawing.Size(662, 45);
            this.trackLightSource_2.TabIndex = 217;
            this.trackLightSource_2.Value = 25;
            this.trackLightSource_2.EditValueChanged += new System.EventHandler(this.trackLightSource_2_EditValueChanged);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label75.Location = new System.Drawing.Point(12, 15);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(90, 17);
            this.label75.TabIndex = 216;
            this.label75.Text = "通道1(相机1前)";
            // 
            // spedLS_Value_1
            // 
            this.spedLS_Value_1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedLS_Value_1.Location = new System.Drawing.Point(712, 41);
            this.spedLS_Value_1.Name = "spedLS_Value_1";
            this.spedLS_Value_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedLS_Value_1.Size = new System.Drawing.Size(74, 20);
            this.spedLS_Value_1.TabIndex = 215;
            // 
            // trackLightSource_1
            // 
            this.trackLightSource_1.EditValue = 25;
            this.trackLightSource_1.Location = new System.Drawing.Point(44, 38);
            this.trackLightSource_1.Name = "trackLightSource_1";
            this.trackLightSource_1.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.trackLightSource_1.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.trackLightSource_1.Properties.Maximum = 255;
            this.trackLightSource_1.Properties.ShowLabels = true;
            this.trackLightSource_1.Size = new System.Drawing.Size(662, 45);
            this.trackLightSource_1.TabIndex = 214;
            this.trackLightSource_1.Value = 25;
            this.trackLightSource_1.EditValueChanged += new System.EventHandler(this.trackLightSource_1_EditValueChanged);
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.popupContainerControl2.Appearance.Options.UseFont = true;
            this.popupContainerControl2.Controls.Add(this.label62);
            this.popupContainerControl2.Controls.Add(this.UnloadCfg_Remaind);
            this.popupContainerControl2.Controls.Add(this.UnloadCfg_TotalProduct);
            this.popupContainerControl2.Controls.Add(this.label57);
            this.popupContainerControl2.Controls.Add(this.label61);
            this.popupContainerControl2.Controls.Add(this.UnloadCfg_CountTrayBK);
            this.popupContainerControl2.Controls.Add(this.sped生产总盘数);
            this.popupContainerControl2.Controls.Add(this.生产最后打包总盘数);
            this.popupContainerControl2.Controls.Add(this.UnloadCfg_CountTray);
            this.popupContainerControl2.Controls.Add(this.UnloadCfg_Col);
            this.popupContainerControl2.Controls.Add(this.UnloadCfg_Total);
            this.popupContainerControl2.Controls.Add(this.labelControl59);
            this.popupContainerControl2.Controls.Add(this.label53);
            this.popupContainerControl2.Controls.Add(this.UnloadCfg_Row);
            this.popupContainerControl2.Controls.Add(this.label54);
            this.popupContainerControl2.Location = new System.Drawing.Point(1256, 717);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(279, 218);
            this.popupContainerControl2.TabIndex = 205;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label62.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(3, 67);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(145, 17);
            this.label62.TabIndex = 101;
            this.label62.Text = "最后打包总盘数/尾盘余量";
            // 
            // UnloadCfg_Remaind
            // 
            this.UnloadCfg_Remaind.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Remaind.Location = new System.Drawing.Point(216, 64);
            this.UnloadCfg_Remaind.Name = "UnloadCfg_Remaind";
            this.UnloadCfg_Remaind.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_Remaind.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Remaind.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Remaind.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Remaind.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Remaind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Remaind.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.UnloadCfg_Remaind.Properties.ReadOnly = true;
            this.UnloadCfg_Remaind.Size = new System.Drawing.Size(59, 24);
            this.UnloadCfg_Remaind.TabIndex = 87;
            // 
            // UnloadCfg_TotalProduct
            // 
            this.UnloadCfg_TotalProduct.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_TotalProduct.Location = new System.Drawing.Point(156, 4);
            this.UnloadCfg_TotalProduct.Name = "UnloadCfg_TotalProduct";
            this.UnloadCfg_TotalProduct.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_TotalProduct.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_TotalProduct.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_TotalProduct.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_TotalProduct.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_TotalProduct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_TotalProduct.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.UnloadCfg_TotalProduct.Properties.ReadOnly = true;
            this.UnloadCfg_TotalProduct.Size = new System.Drawing.Size(119, 24);
            this.UnloadCfg_TotalProduct.TabIndex = 91;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(92, 7);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(56, 17);
            this.label57.TabIndex = 92;
            this.label57.Text = "生产总量";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label61.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(80, 38);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(68, 17);
            this.label61.TabIndex = 99;
            this.label61.Text = "生产总盘数";
            // 
            // UnloadCfg_CountTrayBK
            // 
            this.UnloadCfg_CountTrayBK.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_CountTrayBK.Location = new System.Drawing.Point(156, 154);
            this.UnloadCfg_CountTrayBK.Name = "UnloadCfg_CountTrayBK";
            this.UnloadCfg_CountTrayBK.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_CountTrayBK.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTrayBK.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTrayBK.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTrayBK.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_CountTrayBK.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_CountTrayBK.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.UnloadCfg_CountTrayBK.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.UnloadCfg_CountTrayBK.Properties.ReadOnly = true;
            this.UnloadCfg_CountTrayBK.Size = new System.Drawing.Size(59, 24);
            this.UnloadCfg_CountTrayBK.TabIndex = 192;
            // 
            // sped生产总盘数
            // 
            this.sped生产总盘数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.sped生产总盘数.Location = new System.Drawing.Point(156, 34);
            this.sped生产总盘数.Name = "sped生产总盘数";
            this.sped生产总盘数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sped生产总盘数.Properties.Appearance.Options.UseFont = true;
            this.sped生产总盘数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.sped生产总盘数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数.Properties.AppearanceFocused.Options.UseFont = true;
            this.sped生产总盘数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.sped生产总盘数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.sped生产总盘数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sped生产总盘数.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sped生产总盘数.Properties.ReadOnly = true;
            this.sped生产总盘数.Size = new System.Drawing.Size(119, 24);
            this.sped生产总盘数.TabIndex = 98;
            // 
            // 生产最后打包总盘数
            // 
            this.生产最后打包总盘数.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.生产最后打包总盘数.Location = new System.Drawing.Point(156, 64);
            this.生产最后打包总盘数.Name = "生产最后打包总盘数";
            this.生产最后打包总盘数.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.生产最后打包总盘数.Properties.Appearance.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.生产最后打包总盘数.Properties.AppearanceDisabled.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.生产最后打包总盘数.Properties.AppearanceFocused.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.生产最后打包总盘数.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.生产最后打包总盘数.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.生产最后打包总盘数.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.生产最后打包总盘数.Properties.ReadOnly = true;
            this.生产最后打包总盘数.Size = new System.Drawing.Size(59, 24);
            this.生产最后打包总盘数.TabIndex = 100;
            // 
            // UnloadCfg_CountTray
            // 
            this.UnloadCfg_CountTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_CountTray.Location = new System.Drawing.Point(216, 154);
            this.UnloadCfg_CountTray.Name = "UnloadCfg_CountTray";
            this.UnloadCfg_CountTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_CountTray.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_CountTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_CountTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_CountTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.UnloadCfg_CountTray.Properties.ReadOnly = true;
            this.UnloadCfg_CountTray.Size = new System.Drawing.Size(59, 24);
            this.UnloadCfg_CountTray.TabIndex = 10;
            // 
            // UnloadCfg_Col
            // 
            this.UnloadCfg_Col.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Col.Location = new System.Drawing.Point(216, 123);
            this.UnloadCfg_Col.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UnloadCfg_Col.Name = "UnloadCfg_Col";
            this.UnloadCfg_Col.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Col.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Col.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Col.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.UnloadCfg_Col.Properties.ReadOnly = true;
            this.UnloadCfg_Col.Size = new System.Drawing.Size(59, 24);
            this.UnloadCfg_Col.TabIndex = 82;
            // 
            // UnloadCfg_Total
            // 
            this.UnloadCfg_Total.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Total.Location = new System.Drawing.Point(156, 94);
            this.UnloadCfg_Total.Name = "UnloadCfg_Total";
            this.UnloadCfg_Total.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_Total.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Total.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Total.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Total.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Total.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Total.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.UnloadCfg_Total.Properties.ReadOnly = true;
            this.UnloadCfg_Total.Size = new System.Drawing.Size(119, 24);
            this.UnloadCfg_Total.TabIndex = 84;
            // 
            // labelControl59
            // 
            this.labelControl59.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl59.Appearance.Options.UseFont = true;
            this.labelControl59.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl59.AppearanceDisabled.Options.UseFont = true;
            this.labelControl59.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl59.AppearanceHovered.Options.UseFont = true;
            this.labelControl59.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl59.AppearancePressed.Options.UseFont = true;
            this.labelControl59.Location = new System.Drawing.Point(83, 129);
            this.labelControl59.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(65, 17);
            this.labelControl59.TabIndex = 81;
            this.labelControl59.Text = "产品行/例数";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(92, 98);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(56, 17);
            this.label53.TabIndex = 85;
            this.label53.Text = "包装总量";
            // 
            // UnloadCfg_Row
            // 
            this.UnloadCfg_Row.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnloadCfg_Row.Location = new System.Drawing.Point(156, 124);
            this.UnloadCfg_Row.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UnloadCfg_Row.Name = "UnloadCfg_Row";
            this.UnloadCfg_Row.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnloadCfg_Row.Properties.Appearance.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Row.Properties.AppearanceDisabled.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Row.Properties.AppearanceFocused.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.UnloadCfg_Row.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.UnloadCfg_Row.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnloadCfg_Row.Properties.MaxValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.UnloadCfg_Row.Properties.ReadOnly = true;
            this.UnloadCfg_Row.Size = new System.Drawing.Size(59, 24);
            this.UnloadCfg_Row.TabIndex = 80;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label54.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(15, 157);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(133, 17);
            this.label54.TabIndex = 79;
            this.label54.Text = "正常包装/尾盘包装总盘";
            // 
            // groupControl18
            // 
            this.groupControl18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl18.Appearance.Options.UseFont = true;
            this.groupControl18.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl18.AppearanceCaption.Options.UseFont = true;
            this.groupControl18.Controls.Add(this.更新下料总数);
            this.groupControl18.Controls.Add(this.spedCurrentUnitUnloadedToPackingStationNew);
            this.groupControl18.Controls.Add(this.spedTotalFailedCount);
            this.groupControl18.Controls.Add(this.label68);
            this.groupControl18.Controls.Add(this.spedCurrentUnitUnloadedToPackingStation);
            this.groupControl18.Controls.Add(this.label67);
            this.groupControl18.Controls.Add(this.TotalLineOutAll);
            this.groupControl18.Controls.Add(this.speCountOfBuffTray);
            this.groupControl18.Controls.Add(this.popupContainerEdit1);
            this.groupControl18.Controls.Add(this.label65);
            this.groupControl18.Controls.Add(this.speCountOfLoadTray);
            this.groupControl18.Controls.Add(this.label64);
            this.groupControl18.Controls.Add(this.label63);
            this.groupControl18.Controls.Add(this.btmSetCurrentTrayOut);
            this.groupControl18.Controls.Add(this.label60);
            this.groupControl18.Controls.Add(this.TotalTrayOfLineOutPCS);
            this.groupControl18.Controls.Add(this.spedSetTrayOuted);
            this.groupControl18.Controls.Add(this.TotalTrayOfLineOut);
            this.groupControl18.Controls.Add(this.label59);
            this.groupControl18.Controls.Add(this.CurrentCountTray);
            this.groupControl18.Controls.Add(this.label56);
            this.groupControl18.Location = new System.Drawing.Point(1560, 703);
            this.groupControl18.Name = "groupControl18";
            this.groupControl18.Size = new System.Drawing.Size(344, 238);
            this.groupControl18.TabIndex = 191;
            this.groupControl18.Text = "总调度工艺参数";
            // 
            // 更新下料总数
            // 
            this.更新下料总数.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.更新下料总数.Appearance.Options.UseFont = true;
            this.更新下料总数.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.更新下料总数.AppearanceDisabled.Options.UseFont = true;
            this.更新下料总数.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.更新下料总数.AppearanceHovered.Options.UseFont = true;
            this.更新下料总数.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.更新下料总数.AppearancePressed.Options.UseFont = true;
            this.更新下料总数.Location = new System.Drawing.Point(281, 184);
            this.更新下料总数.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.更新下料总数.Name = "更新下料总数";
            this.更新下料总数.Size = new System.Drawing.Size(58, 51);
            this.更新下料总数.TabIndex = 204;
            this.更新下料总数.Text = "更新数量";
            this.更新下料总数.Click += new System.EventHandler(this.更新下料总数_Click);
            // 
            // spedCurrentUnitUnloadedToPackingStationNew
            // 
            this.spedCurrentUnitUnloadedToPackingStationNew.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedCurrentUnitUnloadedToPackingStationNew.Location = new System.Drawing.Point(207, 209);
            this.spedCurrentUnitUnloadedToPackingStationNew.Name = "spedCurrentUnitUnloadedToPackingStationNew";
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.Appearance.Options.UseBackColor = true;
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.Appearance.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedCurrentUnitUnloadedToPackingStationNew.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedCurrentUnitUnloadedToPackingStationNew.Size = new System.Drawing.Size(68, 24);
            this.spedCurrentUnitUnloadedToPackingStationNew.TabIndex = 203;
            // 
            // spedTotalFailedCount
            // 
            this.spedTotalFailedCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedTotalFailedCount.Location = new System.Drawing.Point(135, 152);
            this.spedTotalFailedCount.Name = "spedTotalFailedCount";
            this.spedTotalFailedCount.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.spedTotalFailedCount.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedTotalFailedCount.Properties.Appearance.Options.UseBackColor = true;
            this.spedTotalFailedCount.Properties.Appearance.Options.UseFont = true;
            this.spedTotalFailedCount.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedTotalFailedCount.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedTotalFailedCount.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedTotalFailedCount.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedTotalFailedCount.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedTotalFailedCount.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedTotalFailedCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedTotalFailedCount.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedTotalFailedCount.Properties.ReadOnly = true;
            this.spedTotalFailedCount.Size = new System.Drawing.Size(69, 24);
            this.spedTotalFailedCount.TabIndex = 201;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(5, 156);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(124, 17);
            this.label68.TabIndex = 202;
            this.label68.Text = "总弃包数(含坏品&分捡)";
            // 
            // spedCurrentUnitUnloadedToPackingStation
            // 
            this.spedCurrentUnitUnloadedToPackingStation.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedCurrentUnitUnloadedToPackingStation.Location = new System.Drawing.Point(135, 208);
            this.spedCurrentUnitUnloadedToPackingStation.Name = "spedCurrentUnitUnloadedToPackingStation";
            this.spedCurrentUnitUnloadedToPackingStation.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.spedCurrentUnitUnloadedToPackingStation.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedCurrentUnitUnloadedToPackingStation.Properties.Appearance.Options.UseBackColor = true;
            this.spedCurrentUnitUnloadedToPackingStation.Properties.Appearance.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedCurrentUnitUnloadedToPackingStation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStation.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedCurrentUnitUnloadedToPackingStation.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStation.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedCurrentUnitUnloadedToPackingStation.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedCurrentUnitUnloadedToPackingStation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedCurrentUnitUnloadedToPackingStation.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedCurrentUnitUnloadedToPackingStation.Properties.ReadOnly = true;
            this.spedCurrentUnitUnloadedToPackingStation.Size = new System.Drawing.Size(69, 24);
            this.spedCurrentUnitUnloadedToPackingStation.TabIndex = 200;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(13, 212);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(116, 17);
            this.label67.TabIndex = 199;
            this.label67.Text = "已下包装位产品数量";
            // 
            // TotalLineOutAll
            // 
            this.TotalLineOutAll.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalLineOutAll.Location = new System.Drawing.Point(135, 180);
            this.TotalLineOutAll.Name = "TotalLineOutAll";
            this.TotalLineOutAll.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotalLineOutAll.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalLineOutAll.Properties.Appearance.Options.UseBackColor = true;
            this.TotalLineOutAll.Properties.Appearance.Options.UseFont = true;
            this.TotalLineOutAll.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TotalLineOutAll.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll.Properties.AppearanceFocused.Options.UseFont = true;
            this.TotalLineOutAll.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalLineOutAll.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TotalLineOutAll.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalLineOutAll.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TotalLineOutAll.Properties.ReadOnly = true;
            this.TotalLineOutAll.Size = new System.Drawing.Size(69, 24);
            this.TotalLineOutAll.TabIndex = 198;
            // 
            // speCountOfBuffTray
            // 
            this.speCountOfBuffTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speCountOfBuffTray.Location = new System.Drawing.Point(135, 124);
            this.speCountOfBuffTray.Name = "speCountOfBuffTray";
            this.speCountOfBuffTray.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.speCountOfBuffTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speCountOfBuffTray.Properties.Appearance.Options.UseBackColor = true;
            this.speCountOfBuffTray.Properties.Appearance.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfBuffTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfBuffTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfBuffTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speCountOfBuffTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speCountOfBuffTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speCountOfBuffTray.Properties.ReadOnly = true;
            this.speCountOfBuffTray.Size = new System.Drawing.Size(69, 24);
            this.speCountOfBuffTray.TabIndex = 196;
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "详细生产信息";
            this.popupContainerEdit1.Location = new System.Drawing.Point(194, 0);
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.popupContainerEdit1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.popupContainerEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.popupContainerEdit1.Properties.Appearance.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.popupContainerEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.popupContainerEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.popupContainerEdit1.Properties.AppearanceDropDown.Options.UseBackColor = true;
            this.popupContainerEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.popupContainerEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.popupContainerEdit1.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.popupContainerEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.popupContainerEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 8.25F);
            this.popupContainerEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControl2;
            this.popupContainerEdit1.Size = new System.Drawing.Size(137, 22);
            this.popupContainerEdit1.TabIndex = 6;
            this.popupContainerEdit1.EditValueChanged += new System.EventHandler(this.popupContainerEdit1_EditValueChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(13, 128);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(116, 17);
            this.label65.TabIndex = 197;
            this.label65.Text = "当前缓存盘好品个数";
            // 
            // speCountOfLoadTray
            // 
            this.speCountOfLoadTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speCountOfLoadTray.Location = new System.Drawing.Point(135, 96);
            this.speCountOfLoadTray.Name = "speCountOfLoadTray";
            this.speCountOfLoadTray.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.speCountOfLoadTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.speCountOfLoadTray.Properties.Appearance.Options.UseBackColor = true;
            this.speCountOfLoadTray.Properties.Appearance.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfLoadTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfLoadTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.speCountOfLoadTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.speCountOfLoadTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.speCountOfLoadTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.speCountOfLoadTray.Properties.ReadOnly = true;
            this.speCountOfLoadTray.Size = new System.Drawing.Size(69, 24);
            this.speCountOfLoadTray.TabIndex = 194;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(13, 100);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(116, 17);
            this.label64.TabIndex = 195;
            this.label64.Text = "当前下料盘好品个数";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label63.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(16, 45);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(113, 17);
            this.label63.TabIndex = 193;
            this.label63.Text = "PLC界面上手动更改";
            // 
            // btmSetCurrentTrayOut
            // 
            this.btmSetCurrentTrayOut.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetCurrentTrayOut.Appearance.Options.UseFont = true;
            this.btmSetCurrentTrayOut.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTrayOut.AppearanceDisabled.Options.UseFont = true;
            this.btmSetCurrentTrayOut.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTrayOut.AppearanceHovered.Options.UseFont = true;
            this.btmSetCurrentTrayOut.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetCurrentTrayOut.AppearancePressed.Options.UseFont = true;
            this.btmSetCurrentTrayOut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetCurrentTrayOut.ImageOptions.Image")));
            this.btmSetCurrentTrayOut.Location = new System.Drawing.Point(207, 105);
            this.btmSetCurrentTrayOut.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetCurrentTrayOut.Name = "btmSetCurrentTrayOut";
            this.btmSetCurrentTrayOut.Size = new System.Drawing.Size(103, 43);
            this.btmSetCurrentTrayOut.TabIndex = 191;
            this.btmSetCurrentTrayOut.Text = "设定当前已包装总盘数";
            this.btmSetCurrentTrayOut.Click += new System.EventHandler(this.btmSetCurrentTrayOut_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(8, 184);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(121, 17);
            this.label60.TabIndex = 97;
            this.label60.Text = "已生产总量/包装总量";
            // 
            // TotalTrayOfLineOutPCS
            // 
            this.TotalTrayOfLineOutPCS.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalTrayOfLineOutPCS.Location = new System.Drawing.Point(207, 181);
            this.TotalTrayOfLineOutPCS.Name = "TotalTrayOfLineOutPCS";
            this.TotalTrayOfLineOutPCS.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotalTrayOfLineOutPCS.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalTrayOfLineOutPCS.Properties.Appearance.Options.UseBackColor = true;
            this.TotalTrayOfLineOutPCS.Properties.Appearance.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOutPCS.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOutPCS.Properties.AppearanceFocused.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOutPCS.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TotalTrayOfLineOutPCS.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalTrayOfLineOutPCS.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TotalTrayOfLineOutPCS.Properties.ReadOnly = true;
            this.TotalTrayOfLineOutPCS.Size = new System.Drawing.Size(68, 24);
            this.TotalTrayOfLineOutPCS.TabIndex = 96;
            // 
            // spedSetTrayOuted
            // 
            this.spedSetTrayOuted.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spedSetTrayOuted.Location = new System.Drawing.Point(207, 70);
            this.spedSetTrayOuted.Name = "spedSetTrayOuted";
            this.spedSetTrayOuted.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.spedSetTrayOuted.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spedSetTrayOuted.Properties.Appearance.Options.UseBackColor = true;
            this.spedSetTrayOuted.Properties.Appearance.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetTrayOuted.Properties.AppearanceDisabled.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetTrayOuted.Properties.AppearanceFocused.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.spedSetTrayOuted.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.spedSetTrayOuted.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spedSetTrayOuted.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spedSetTrayOuted.Size = new System.Drawing.Size(71, 24);
            this.spedSetTrayOuted.TabIndex = 95;
            // 
            // TotalTrayOfLineOut
            // 
            this.TotalTrayOfLineOut.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalTrayOfLineOut.Location = new System.Drawing.Point(135, 68);
            this.TotalTrayOfLineOut.Name = "TotalTrayOfLineOut";
            this.TotalTrayOfLineOut.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TotalTrayOfLineOut.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalTrayOfLineOut.Properties.Appearance.Options.UseBackColor = true;
            this.TotalTrayOfLineOut.Properties.Appearance.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOut.Properties.AppearanceDisabled.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOut.Properties.AppearanceFocused.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.TotalTrayOfLineOut.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.TotalTrayOfLineOut.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TotalTrayOfLineOut.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TotalTrayOfLineOut.Properties.ReadOnly = true;
            this.TotalTrayOfLineOut.Size = new System.Drawing.Size(69, 24);
            this.TotalTrayOfLineOut.TabIndex = 93;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(5, 72);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(124, 17);
            this.label59.TabIndex = 94;
            this.label59.Text = "当前已包装盘数(所有)";
            // 
            // CurrentCountTray
            // 
            this.CurrentCountTray.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CurrentCountTray.Location = new System.Drawing.Point(135, 32);
            this.CurrentCountTray.Name = "CurrentCountTray";
            this.CurrentCountTray.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.CurrentCountTray.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentCountTray.Properties.Appearance.Options.UseBackColor = true;
            this.CurrentCountTray.Properties.Appearance.Options.UseFont = true;
            this.CurrentCountTray.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CurrentCountTray.Properties.AppearanceDisabled.Options.UseFont = true;
            this.CurrentCountTray.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CurrentCountTray.Properties.AppearanceFocused.Options.UseFont = true;
            this.CurrentCountTray.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.CurrentCountTray.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.CurrentCountTray.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CurrentCountTray.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.CurrentCountTray.Properties.ReadOnly = true;
            this.CurrentCountTray.Size = new System.Drawing.Size(69, 24);
            this.CurrentCountTray.TabIndex = 89;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(5, 26);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(124, 17);
            this.label56.TabIndex = 90;
            this.label56.Text = "实时包装盘数(包装机)";
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(445, 850);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(233, 59);
            this.simpleButton11.TabIndex = 190;
            this.simpleButton11.Text = "simpleButton11";
            this.simpleButton11.Visible = false;
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // timer_IO
            // 
            this.timer_IO.Enabled = true;
            this.timer_IO.Interval = 150;
            this.timer_IO.Tick += new System.EventHandler(this.timer_IO_Tick);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2010 Blue";
            // 
            // timerFlashData
            // 
            this.timerFlashData.Interval = 2000;
            this.timerFlashData.Tick += new System.EventHandler(this.timerFlashData_Tick);
            // 
            // timer_GC
            // 
            this.timer_GC.Enabled = true;
            this.timer_GC.Interval = 60000;
            this.timer_GC.Tick += new System.EventHandler(this.timer_GC_Tick);
            // 
            // timer_Display_ExeStatus
            // 
            this.timer_Display_ExeStatus.Enabled = true;
            this.timer_Display_ExeStatus.Interval = 50;
            this.timer_Display_ExeStatus.Tick += new System.EventHandler(this.timer_Display_ExeStatus_Tick);
            // 
            // frm_RobotControl
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 940);
            this.Controls.Add(this.simpleButton11);
            this.Controls.Add(this.popupContainerControl2);
            this.Controls.Add(this.groupControl18);
            this.Controls.Add(this.navigationPane1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_RobotControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "打码切割I/O 监控";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_Closing);
            this.Load += new System.EventHandler(this.frm_RobotControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.NP_主页运行.ResumeLayout(false);
            this.NP_主页运行.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            this.popupContainerControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).EndInit();
            this.groupControl17.ResumeLayout(false);
            this.groupControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt已识别的好品数量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPHCamera2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUPH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailedCamera2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassedCamera2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFailed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPHCamera1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalPassed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailedCamera1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassedCamera1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupInfor2)).EndInit();
            this.PopupInfor2.ResumeLayout(false);
            this.PopupInfor2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl4)).EndInit();
            this.xtraTabControl4.ResumeLayout(false);
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.viewFlashResult)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.NP_设置.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotFailed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotQA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotTransit_VToQA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotTransit_ULToF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotTransit_LToF.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotCamera.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotUnload.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotCatch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotOrg.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).EndInit();
            this.groupControl15.ResumeLayout(false);
            this.groupControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_LoadFirst2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_BuffFirst2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            this.groupControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCustomizeRowDst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Cust_Row1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speColOfSingleDst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRowOfSingleDst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speColOfSingle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRowOfSingle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_LoadFirst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRobotPos_BuffFirst.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboxRunTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboxRunFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridRobotWayList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).EndInit();
            this.groupControl13.ResumeLayout(false);
            this.groupControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spedOrgStepDst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDstRow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCathDst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).EndInit();
            this.groupControl12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            this.NP_手动调试.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl19)).EndInit();
            this.groupControl19.ResumeLayout(false);
            this.groupControl19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.强制camera2结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.强制camera1结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableRobotIO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCareFlashResult.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVacuuSensor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.强行清料并包装模式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWorkOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1013.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1000.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1007.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1008.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1010.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1003.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1009.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1002.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1012.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1004.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1011.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1001.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1005.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1006.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).EndInit();
            this.groupControl14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtStepDst.Properties)).EndInit();
            this.NP_生产数据查询.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).EndInit();
            this.xtraTabControl3.ResumeLayout(false);
            this.烧录区域1数据.ResumeLayout(false);
            this.烧录区域1数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea1)).EndInit();
            this.烧录区域2数据.ResumeLayout(false);
            this.烧录区域2数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea2)).EndInit();
            this.烧录区域3数据.ResumeLayout(false);
            this.烧录区域3数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea3)).EndInit();
            this.烧录区域4数据.ResumeLayout(false);
            this.烧录区域4数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea4)).EndInit();
            this.烧录区域5数据.ResumeLayout(false);
            this.烧录区域5数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea5)).EndInit();
            this.烧录区域6数据.ResumeLayout(false);
            this.烧录区域6数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea6)).EndInit();
            this.烧录区域7数据.ResumeLayout(false);
            this.烧录区域7数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea7)).EndInit();
            this.烧录区域8数据.ResumeLayout(false);
            this.烧录区域8数据.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridArea8)).EndInit();
            this.NP_工艺参数.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spedSetCurrentTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBuffTrayResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLoadTrayResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speStartBuffCol.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCoutOfCatchUsed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speStartBuffRow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speColDst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRowDst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCols.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speRows.Properties)).EndInit();
            this.NP_视觉参数.ResumeLayout(false);
            this.NP_视觉参数.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk采集新的图片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedDelayCameraSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Down.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLaserMarkTest_Up.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkNewTemplate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage1.ResumeLayout(false);
            this.tabNavigationPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.tabNavigationPage2.ResumeLayout(false);
            this.tabNavigationPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackContrast_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackExposure_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.tabNavigationPage3.ResumeLayout(false);
            this.tabNavigationPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedLS_Value_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackLightSource_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            this.popupContainerControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Remaind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_TotalProduct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTrayBK.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sped生产总盘数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.生产最后打包总盘数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_CountTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Col.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Total.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnloadCfg_Row.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).EndInit();
            this.groupControl18.ResumeLayout(false);
            this.groupControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spedCurrentUnitUnloadedToPackingStationNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedTotalFailedCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedCurrentUnitUnloadedToPackingStation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalLineOutAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfBuffTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speCountOfLoadTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOutPCS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spedSetTrayOuted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalTrayOfLineOut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentCountTray.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_主页运行;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_StopMachine;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_StartMachine;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_设置;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_手动调试;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_生产数据查询;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_工艺参数;
        private System.Windows.Forms.Timer timer_IO;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.SimpleButton btmXIncre;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton btmYIncre;
        private DevExpress.XtraEditors.SimpleButton btmYDecre;
        private DevExpress.XtraEditors.SimpleButton btmXDecre;
        private DevExpress.XtraEditors.SimpleButton btmRIncre;
        private DevExpress.XtraEditors.SimpleButton btmRDecre;
        private DevExpress.XtraEditors.SimpleButton btmZIncre;
        private DevExpress.XtraEditors.SimpleButton btmZDecre;
        private DevExpress.XtraEditors.SimpleButton 机器人回原点;
        private DevExpress.XtraEditors.CheckButton 伺服启用禁用;
        private DevExpress.XtraEditors.SimpleButton R回原点;
        private DevExpress.XtraEditors.SimpleButton Y回原点;
        private DevExpress.XtraEditors.SimpleButton Z回原点;
        private DevExpress.XtraEditors.SimpleButton X回原点;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.ComboBox cobMoveDst;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.RadioButton rbtmSeep_Low;
        private System.Windows.Forms.RadioButton rbtmSeep_High;
        private System.Windows.Forms.RadioButton rbtmSeep_Middel;
        private DevExpress.XtraEditors.SimpleButton 机器人报警清除;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtRobotOrg;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnLF_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnLF;
        private DevExpress.XtraEditors.TextEdit txtRobotTransit_LToF;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnCamera_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnCamera;
        private DevExpress.XtraEditors.TextEdit txtRobotCamera;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnUnload_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnUnload;
        private DevExpress.XtraEditors.TextEdit txtRobotUnload;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnCatch_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnCatch;
        private DevExpress.XtraEditors.TextEdit txtRobotCatch;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnOrg_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnOrg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gridRobotWayList;
        private DevExpress.XtraEditors.SimpleButton btmRobotWayClear;
        private DevExpress.XtraEditors.SimpleButton btmRobotWayDel;
        private DevExpress.XtraEditors.SimpleButton btmRobotWayLearn;
        private DevExpress.XtraEditors.SimpleButton btmRobotWayAdd;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton20;
        private DevExpress.XtraEditors.SimpleButton btmRobotWayTrayRun;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox LED_Out0;
        private System.Windows.Forms.CheckBox LED_Out31;
        private System.Windows.Forms.CheckBox LED_Out1;
        private System.Windows.Forms.CheckBox LED_Out30;
        private System.Windows.Forms.CheckBox LED_Out2;
        private System.Windows.Forms.CheckBox LED_Out29;
        private System.Windows.Forms.CheckBox LED_Out3;
        private System.Windows.Forms.CheckBox LED_Out28;
        private System.Windows.Forms.CheckBox LED_Out4;
        private System.Windows.Forms.CheckBox LED_Out27;
        private System.Windows.Forms.CheckBox LED_Out5;
        private System.Windows.Forms.CheckBox LED_Out26;
        private System.Windows.Forms.CheckBox LED_Out6;
        private System.Windows.Forms.CheckBox LED_Out25;
        private System.Windows.Forms.CheckBox LED_Out7;
        private System.Windows.Forms.CheckBox LED_Out24;
        private System.Windows.Forms.CheckBox LED_Out8;
        private System.Windows.Forms.CheckBox LED_Out23;
        private System.Windows.Forms.CheckBox LED_Out9;
        private System.Windows.Forms.CheckBox LED_Out22;
        private System.Windows.Forms.CheckBox LED_Out10;
        private System.Windows.Forms.CheckBox LED_Out21;
        private System.Windows.Forms.CheckBox LED_Out11;
        private System.Windows.Forms.CheckBox LED_Out20;
        private System.Windows.Forms.CheckBox LED_Out12;
        private System.Windows.Forms.CheckBox LED_Out19;
        private System.Windows.Forms.CheckBox LED_Out13;
        private System.Windows.Forms.CheckBox LED_Out18;
        private System.Windows.Forms.CheckBox LED_Out14;
        private System.Windows.Forms.CheckBox LED_Out17;
        private System.Windows.Forms.CheckBox LED_Out15;
        private System.Windows.Forms.CheckBox LED_Out16;
        private System.Windows.Forms.GroupBox groupBox6;
        private HslCommunication.Controls.UserLantern LED_In0;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label37;
        private HslCommunication.Controls.UserLantern LED_In1;
        private System.Windows.Forms.Label label38;
        private HslCommunication.Controls.UserLantern LED_In2;
        private System.Windows.Forms.Label label39;
        private HslCommunication.Controls.UserLantern LED_In3;
        private System.Windows.Forms.Label label40;
        private HslCommunication.Controls.UserLantern LED_In4;
        private System.Windows.Forms.Label label41;
        private HslCommunication.Controls.UserLantern LED_In5;
        private System.Windows.Forms.Label label42;
        private HslCommunication.Controls.UserLantern LED_In6;
        private System.Windows.Forms.Label label43;
        private HslCommunication.Controls.UserLantern LED_In7;
        private System.Windows.Forms.Label label44;
        private HslCommunication.Controls.UserLantern LED_In9;
        private System.Windows.Forms.Label label26;
        private HslCommunication.Controls.UserLantern LED_In10;
        private System.Windows.Forms.Label label30;
        private HslCommunication.Controls.UserLantern LED_In11;
        private System.Windows.Forms.Label label31;
        private HslCommunication.Controls.UserLantern LED_In12;
        private System.Windows.Forms.Label label32;
        private HslCommunication.Controls.UserLantern LED_In13;
        private System.Windows.Forms.Label label33;
        private HslCommunication.Controls.UserLantern LED_In14;
        private System.Windows.Forms.Label label34;
        private HslCommunication.Controls.UserLantern LED_In15;
        private System.Windows.Forms.Label label35;
        private HslCommunication.Controls.UserLantern LED_In8;
        private System.Windows.Forms.Label label36;
        private HslCommunication.Controls.UserLantern LED_In17;
        private HslCommunication.Controls.UserLantern LED_In24;
        private HslCommunication.Controls.UserLantern LED_In18;
        private HslCommunication.Controls.UserLantern LED_In31;
        private HslCommunication.Controls.UserLantern LED_In19;
        private HslCommunication.Controls.UserLantern LED_In30;
        private HslCommunication.Controls.UserLantern LED_In20;
        private HslCommunication.Controls.UserLantern LED_In29;
        private HslCommunication.Controls.UserLantern LED_In21;
        private HslCommunication.Controls.UserLantern LED_In28;
        private HslCommunication.Controls.UserLantern LED_In22;
        private HslCommunication.Controls.UserLantern LED_In27;
        private HslCommunication.Controls.UserLantern LED_In23;
        private HslCommunication.Controls.UserLantern LED_In26;
        private HslCommunication.Controls.UserLantern LED_In16;
        private HslCommunication.Controls.UserLantern LED_In25;
        private HslCommunication.Controls.UserLantern userLanternIO;
        private System.Windows.Forms.CheckBox chkZero;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private System.Windows.Forms.RadioButton chkHandelTypeRight;
        private System.Windows.Forms.RadioButton chkHandelTypeLeft;
        private System.Windows.Forms.RadioButton chkHandelTypeAuto;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.RichTextBox richTextRobot;
        private HslCommunication.Controls.UserLantern userLantern2;
        private HslCommunication.Controls.UserLantern userLantern1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtD1013;
        private DevExpress.XtraEditors.TextEdit txtD1000;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtD1007;
        private DevExpress.XtraEditors.TextEdit txtD1008;
        private DevExpress.XtraEditors.TextEdit txtD1010;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtD1003;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtD1009;
        private DevExpress.XtraEditors.TextEdit txtD1002;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtD1012;
        private DevExpress.XtraEditors.TextEdit txtD1004;
        private DevExpress.XtraEditors.TextEdit txtD1011;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtD1001;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtD1005;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtD1006;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.ComboBoxEdit comboxRunTo;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.ComboBoxEdit comboxRunFrom;
        private DevExpress.XtraEditors.SimpleButton btmSaveRobotConfig;
        private DevExpress.XtraEditors.SimpleButton btmRobotWayTrayRunCurrent;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column5;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column8;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column7;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_ResetMachine;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.SpinEdit speCols;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit speRows;
        private DevExpress.XtraEditors.CheckEdit chkEnableRobotIO;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private System.Windows.Forms.ImageList imageList16;
        private DevExpress.XtraEditors.CheckButton checkButton1;
        private DevExpress.XtraEditors.CheckButton checkButton2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit txtRobotTransit_ULToF;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit txtRobotTransit_VToQA;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnVF_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnVF;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnUNF_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnUNF;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SpinEdit speColDst;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit speRowDst;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnFailed_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnFailed;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit txtRobotFailed;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnQA_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnQA;
        private DevExpress.XtraEditors.TextEdit txtRobotQA;
        private DevExpress.XtraEditors.SimpleButton 触发1303;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton 相机1拍照触发;
        private DevExpress.XtraEditors.SimpleButton 下料组盘触发;
        private DevExpress.XtraEditors.CheckButton PLC设备安信号;
        private DevExpress.XtraEditors.CheckButton 下料盘已满;
        private System.Windows.Forms.RichTextBox richTextCamera1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox richTextCamera2;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.GroupControl groupControl13;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SimpleButton simpleButton21;
        private DevExpress.XtraEditors.SimpleButton simpleButton22;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        private DevExpress.XtraEditors.GroupControl groupControl12;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton 设定吸嘴间距;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit speCathDst;
        private DevExpress.XtraEditors.GroupControl groupControl14;
        private DevExpress.XtraEditors.SimpleButton 电机间距Dcre;
        private DevExpress.XtraEditors.SimpleButton 电机间距Incre;
        private DevExpress.XtraEditors.SimpleButton 间距电机回原点;
        private DevExpress.XtraEditors.SimpleButton simpleButton25;
        private DevExpress.XtraEditors.SimpleButton simpleButton26;
        private DevExpress.XtraEditors.SimpleButton simpleButton27;
        private DevExpress.XtraEditors.SimpleButton simpleButton28;
        private DevExpress.XtraEditors.TextEdit txtStepDst;
        private HslCommunication.Controls.UserLantern LEDModbusServer;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_视觉参数;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.SpinEdit speCoutOfCatchUsed;
        private System.Windows.Forms.DataGridView gridLoadTrayResult;
        private DevExpress.XtraEditors.SimpleButton btmUpdateRow;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column12;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column14;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column15;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column16;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column18;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column20;
        private System.Windows.Forms.DataGridView gridBuffTrayResult;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn12;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn14;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn15;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn16;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn18;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn20;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.GroupControl groupControl15;
        private System.Windows.Forms.Label label55;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnLoadFirst_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnLoadFirst;
        private DevExpress.XtraEditors.TextEdit txtRobotPos_LoadFirst;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnBuffFirst_Go;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnBuffFirst;
        private DevExpress.XtraEditors.TextEdit txtRobotPos_BuffFirst;
        private System.Windows.Forms.Label label58;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.SpinEdit speColOfSingleDst;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.SpinEdit speRowOfSingleDst;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.SpinEdit speColOfSingle;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.SpinEdit speRowOfSingle;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.TextEdit txtTotalUPH;
        private DevExpress.XtraEditors.TextEdit txtTotalFailed;
        private DevExpress.XtraEditors.TextEdit txtTotalPassed;
        private DevExpress.XtraEditors.GroupControl groupControl17;
        private DevExpress.XtraEditors.TextEdit txtUPHCamera2;
        private DevExpress.XtraEditors.TextEdit txtFailedCamera2;
        private DevExpress.XtraEditors.TextEdit txtPassedCamera2;
        private DevExpress.XtraEditors.SimpleButton simpleButton29;
        private DevExpress.XtraEditors.TextEdit txtUPHCamera1;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.TextEdit txtFailedCamera1;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit txtPassedCamera1;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private DevExpress.XtraEditors.SimpleButton btmSaveConfig2;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.SpinEdit speStartBuffCol;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.SpinEdit speStartBuffRow;
        private DevExpress.XtraEditors.SimpleButton btmSetBuffStartPos;
        private DevExpress.XtraEditors.SimpleButton btmSetBuffTrayFull;
        private DevExpress.XtraEditors.SimpleButton simpleButton30;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.SimpleButton simpleButton31;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.SimpleButton 运行测试1;
        private DevExpress.XtraEditors.SimpleButton btmSaveVisionPara;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl2;
        private System.Windows.Forms.PropertyGrid propertyGrid2;
        private DevExpress.XtraEditors.SimpleButton 测试删除1;
        private DevExpress.XtraEditors.SimpleButton 测试增加1;
        private DevExpress.XtraEditors.SimpleButton 测试删除2;
        private DevExpress.XtraEditors.SimpleButton 测试增加2;
        private DevExpress.XtraEditors.SimpleButton 运行测试2;
        private DevExpress.XtraEditors.CheckEdit chkNewTemplate;
        private System.Windows.Forms.ComboBox txtImageProcessFile;
        private System.Windows.Forms.Button btmLoad;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.CheckEdit chkVacuuSensor;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl3;
        private DevExpress.XtraTab.XtraTabPage 烧录区域1数据;
        private System.Windows.Forms.DataGridView dataGridArea1;
        private DevExpress.XtraTab.XtraTabPage 烧录区域2数据;
        private System.Windows.Forms.DataGridView dataGridArea2;
        private DevExpress.XtraTab.XtraTabPage 烧录区域3数据;
        private System.Windows.Forms.DataGridView dataGridArea3;
        private DevExpress.XtraTab.XtraTabPage 烧录区域4数据;
        private System.Windows.Forms.DataGridView dataGridArea4;
        private DevExpress.XtraTab.XtraTabPage 烧录区域5数据;
        private System.Windows.Forms.DataGridView dataGridArea5;
        private DevExpress.XtraTab.XtraTabPage 烧录区域6数据;
        private System.Windows.Forms.DataGridView dataGridArea6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private System.Windows.Forms.Timer timerFlashData;
        private System.Windows.Forms.TextBox txtNameArea1;
        private System.Windows.Forms.TextBox txtNameArea2;
        private System.Windows.Forms.TextBox txtNameArea3;
        private System.Windows.Forms.TextBox txtNameArea4;
        private System.Windows.Forms.TextBox txtNameArea5;
        private System.Windows.Forms.TextBox txtNameArea6;
        private DevExpress.XtraEditors.TextEdit txtPCBSN;
        private DevExpress.XtraEditors.TextEdit txtPCBSN2;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.CheckEdit chkCareFlashResult;
        private DevExpress.XtraEditors.CheckEdit 强制camera2结果;
        private DevExpress.XtraEditors.CheckEdit 强制camera1结果;
        private DevExpress.XtraEditors.SimpleButton 缓存盘设置为空盘;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row1;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row9;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row8;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row7;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row6;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row5;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row4;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row3;
        private DevExpress.XtraEditors.SpinEdit spn_Cust_Row2;
        private DevExpress.XtraEditors.CheckEdit chkCustomizeRowDst;
        private DevExpress.XtraEditors.GroupControl groupControl18;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_CountTray;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Remaind;
        private System.Windows.Forms.Label label54;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Row;
        private System.Windows.Forms.Label label53;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Total;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_Col;
        private DevExpress.XtraEditors.SpinEdit CurrentCountTray;
        private System.Windows.Forms.Label label56;
        private DevExpress.XtraTab.XtraTabPage 烧录区域7数据;
        private DevExpress.XtraTab.XtraTabPage 烧录区域8数据;
        private System.Windows.Forms.TextBox txtNameArea7;
        private System.Windows.Forms.DataGridView dataGridArea7;
        private System.Windows.Forms.TextBox txtNameArea8;
        private System.Windows.Forms.DataGridView dataGridArea8;
        private DevExpress.XtraEditors.PopupContainerControl PopupInfor2;
        private DevExpress.XtraEditors.PopupContainerEdit txtTrayID2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraEditors.PopupContainerEdit txtTrayID;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private System.Windows.Forms.Label label57;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_TotalProduct;
        private DevExpress.XtraEditors.SpinEdit TotalTrayOfLineOut;
        private System.Windows.Forms.Label label59;
        private DevExpress.XtraEditors.SpinEdit spedSetTrayOuted;
        private DevExpress.XtraEditors.SpinEdit TotalTrayOfLineOutPCS;
        private System.Windows.Forms.Label label60;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.GroupControl groupControl19;
        private DevExpress.XtraEditors.SimpleButton btm清料;
        private DevExpress.XtraEditors.SpinEdit sped生产总盘数;
        private System.Windows.Forms.Label label61;
        private DevExpress.XtraEditors.SpinEdit 生产最后打包总盘数;
        private System.Windows.Forms.Label label62;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private System.Windows.Forms.DataGridView viewFlashResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private DevExpress.XtraEditors.SimpleButton btmGetFlashData;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.SimpleButton btmSetCurrentTrayOut;
        private DevExpress.XtraEditors.SpinEdit UnloadCfg_CountTrayBK;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl1;
        private DevExpress.XtraEditors.CheckEdit chkLaserMarkTest_Up;
        private DevExpress.XtraEditors.CheckEdit chkLaserMarkTest_Down;
        private DevExpress.XtraEditors.SimpleButton btmSetCurrentTray;
        private DevExpress.XtraEditors.SpinEdit spedSetCurrentTray;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.SpinEdit speCountOfBuffTray;
        private System.Windows.Forms.Label label65;
        private DevExpress.XtraEditors.SpinEdit speCountOfLoadTray;
        private System.Windows.Forms.Label label64;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraEditors.SpinEdit TotalLineOutAll;
        private DevExpress.XtraEditors.SimpleButton btmUnbindTray;
        private DevExpress.XtraEditors.SimpleButton bmtUnbindTray2;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private HslCommunication.Controls.UserLantern LEDModbusServer_Robot;
        private HslCommunication.Controls.UserLantern LEDUnloadPLCState;
        private DevExpress.XtraEditors.TrackBarControl trackContrast_1;
        private DevExpress.XtraEditors.TrackBarControl trackExposure_1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label66;
        private DevExpress.XtraEditors.TrackBarControl trackContrast_2;
        private DevExpress.XtraEditors.TrackBarControl trackExposure_2;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEdit3;
        private DevExpress.XtraEditors.SpinEdit spinEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.SpinEdit spinDstRow;
        private DevExpress.XtraEditors.SimpleButton btmSetDst;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.SpinEdit spedCurrentUnitUnloadedToPackingStation;
        private System.Windows.Forms.Label label67;
        private DevExpress.XtraEditors.SimpleButton 下料数据交互触发;
        private HslCommunication.Controls.UserLantern LEDRobot;
        private DevExpress.XtraEditors.CheckButton btm进入尾料模式;
        private DevExpress.XtraEditors.SpinEdit spedTotalFailedCount;
        private System.Windows.Forms.Label label68;
        private DevExpress.XtraEditors.SimpleButton btm手动切换型号;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.TextEdit txtWorkOrder;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private HslCommunication.Controls.UserLantern userLantern3;
        private HslCommunication.Controls.UserLantern LEDLasetInvoke;
        private System.Windows.Forms.RadioButton rbtmSeep_SupperHigh;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnBuffFirst_Go2;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnBuffFirst2;
        private DevExpress.XtraEditors.TextEdit txtRobotPos_BuffFirst2;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnLoadFirst_Go2;
        private DevExpress.XtraEditors.SimpleButton btmRobotLearnLoadFirst2;
        private DevExpress.XtraEditors.TextEdit txtRobotPos_LoadFirst2;
        private System.Windows.Forms.Timer timer_GC;
        private DevExpress.XtraEditors.SimpleButton 得到顶部测试结果;
        private DevExpress.XtraEditors.SpinEdit spedDelayCameraSet;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Timer timer_Display_ExeStatus;
        private DevExpress.XtraEditors.CheckEdit chk采集新的图片;
        private System.Windows.Forms.Button 图像循环测试;
        private DevExpress.XtraEditors.SpinEdit spedCurrentUnitUnloadedToPackingStationNew;
        private DevExpress.XtraEditors.SimpleButton 更新下料总数;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage3;
        private System.Windows.Forms.Label label77;
        private DevExpress.XtraEditors.SpinEdit spedLS_Value_4;
        private DevExpress.XtraEditors.TrackBarControl trackLightSource_4;
        private System.Windows.Forms.Label label78;
        private DevExpress.XtraEditors.SpinEdit spedLS_Value_3;
        private DevExpress.XtraEditors.TrackBarControl trackLightSource_3;
        private System.Windows.Forms.Label label76;
        private DevExpress.XtraEditors.SpinEdit spedLS_Value_2;
        private DevExpress.XtraEditors.TrackBarControl trackLightSource_2;
        private System.Windows.Forms.Label label75;
        private DevExpress.XtraEditors.SpinEdit spedLS_Value_1;
        private DevExpress.XtraEditors.TrackBarControl trackLightSource_1;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.SpinEdit spedOrgStepDst;
        private DevExpress.XtraEditors.TextEdit txt已识别的好品数量;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.CheckEdit 强行清料并包装模式;
    }
}