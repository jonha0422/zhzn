using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Windows.Forms.XmlTree;

namespace daitou
{
	public class SchemaController : XtreeNodeController
	{
        protected TableDef tableDef;

        protected SchemaDef schemaDef;
		
		/// <summary>
		/// Gets/sets schemaService
		/// </summary>
		public SchemaDef SchemaDef
		{
			get { return schemaDef; }
			set { schemaDef = value; }
		}

		public override string Name
		{
			get { return "�ͺ�ѡ��"; }
			set { ;}
		}
        public override string Pridata
        {
            get { return "0"; }
            set {; }
        }
        public override object Item
		{
			get { return schemaDef; }
		}

		public SchemaController()
		{
		}

		public SchemaController(SchemaDef schemaDef)
		{
			this.schemaDef = schemaDef;
		}

		public override int Index(object item)
		{
			throw new XmlTreeException("Calling Index for SchemaController is not permitted.");
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
            schemaDef = ((SchemaController)parentInstance).SchemaDef;
            tableDef = new TableDef();
            schemaDef.Tables.Add(tableDef);
            return true;

            //return false;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            schemaDef = ((SchemaController)parentInstance).SchemaDef;
            schemaDef.Tables.Remove(tableDef);
            return true;
            //return false;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
			throw new XmlTreeException("Calling AutoDeleteNode for SchemaController is not permitted.");
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
			throw new XmlTreeException("Calling InsertNode for SchemaController is not permitted.");
		}

		public override void Select(TreeNode tn)
		{
			//Program.Properties.SelectedObject = schemaDef;
		}

		public override void MoveTo(IXtreeNode newParent, IXtreeNode oldParent, int idx)
		{
		}
	}
}
