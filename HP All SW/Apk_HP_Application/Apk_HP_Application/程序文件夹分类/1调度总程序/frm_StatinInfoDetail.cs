﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using CommunicationZH;
using HslCommunication;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public partial class frm_StatinInfoDetail : DevExpress.XtraEditors.XtraForm
    {
        public HPStationBase m_statinBase = null;
        DevExpress.XtraEditors.XtraForm m_frmStation;
        public frm_StatinInfoDetail()
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw |
                               ControlStyles.OptimizedDoubleBuffer |
                               ControlStyles.AllPaintingInWmPaint, true);
            this.UpdateStyles();

            timer_StatusMonitor.Enabled = true;
        }

        public void BindStationForm(HPStationBase stationBase, DevExpress.XtraEditors.XtraForm frmStation)
        {
            m_statinBase = stationBase;
            m_frmStation = frmStation;

            if(m_frmStation != null)
            {
                m_frmStation.TopLevel = false;
                m_frmStation.Size = panelControl1.Size;
                panelControl1.Controls.Add(m_frmStation);
                m_frmStation.Show();
                m_frmStation.SendToBack();
                m_frmStation.BringToFront();
            }

            this.Text = stationBase.strStationName + " 详细信息";
        }

        private void frm_StatinInfoDetail_Load(object sender, EventArgs e)
        {

        }

        private void form_Closing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        public void UpdateStatus()
        {
            try
            {
                #region>>>>显示好板，坏板统计数据
                txtPassed.Text = m_statinBase.CountOfPassed.ToString();
                txtPassed.Text = m_statinBase.CountOfPassed.ToString();

                txtPassedUnit.Text = (m_statinBase.CountOfPassed * m_statinBase.CountOfUnit).ToString();
                txtFailedUnit.Text = (m_statinBase.CountOfFailed * m_statinBase.CountOfUnit).ToString();

                m_statinBase.fUPH = (double)m_statinBase.CountOfPassed / (double)(m_statinBase.CountOfPassed + m_statinBase.CountOfFailed) * 100.00f;
                txtUPH.Text = m_statinBase.fUPH.ToString("F2") + "%";
                #endregion

                #region>>>>显示设备的连接状态
                //===============================
                if (m_statinBase.IsDeviceConnected)
                    LEDPLC.LanternBackground = (LEDPLC.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                else
                    LEDPLC.LanternBackground = (LEDPLC.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);
                
                //===============================
                if (m_statinBase.isScanner1Connected)
                    LEDScanner1.LanternBackground = (LEDScanner1.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                else
                    LEDScanner1.LanternBackground = (LEDScanner1.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);

                //===============================
                if (m_statinBase.isScanner2Connected)
                    LEDScanner2.LanternBackground = (LEDScanner2.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                else
                    LEDScanner2.LanternBackground = (LEDScanner2.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);

                #endregion

                #region>>>>>>IO 输入的状态
                txtD1000.Text = m_statinBase.D1000Values[0].ToString() ;
                txtD1001.Text = m_statinBase.D1000Values[1].ToString();
                txtD1002.Text = m_statinBase.D1000Values[2].ToString();
                txtD1003.Text = m_statinBase.D1000Values[3].ToString();
                txtD1004.Text = m_statinBase.D1000Values[4].ToString();
                txtD1005.Text = m_statinBase.D1000Values[5].ToString();
                txtD1006.Text = m_statinBase.D1000Values[6].ToString();
                txtD1007.Text = m_statinBase.D1000Values[7].ToString();
                txtD1008.Text = m_statinBase.D1000Values[8].ToString();
                txtD1009.Text = m_statinBase.D1000Values[9].ToString();
                txtD1010.Text = m_statinBase.D1000Values[10].ToString();
                txtD1011.Text = m_statinBase.D1000Values[11].ToString();
                txtD1012.Text = m_statinBase.D1000Values[12].ToString();
                txtD1013.Text = m_statinBase.D1000Values[13].ToString();

                txtD1200.Text = m_statinBase.D1200Values[0].ToString();
                txtD1201.Text = m_statinBase.D1200Values[1].ToString();
                txtD1202.Text = m_statinBase.D1200Values[2].ToString();
                txtD1203.Text = m_statinBase.D1200Values[3].ToString();
                txtD1204.Text = m_statinBase.D1200Values[4].ToString();
                txtD1205.Text = m_statinBase.D1200Values[5].ToString();
                txtD1206.Text = m_statinBase.D1200Values[6].ToString();
                txtD1207.Text = m_statinBase.D1200Values[7].ToString();
                txtD1208.Text = m_statinBase.D1200Values[8].ToString();
                txtD1209.Text = m_statinBase.D1200Values[9].ToString();
                txtD1210.Text = m_statinBase.D1200Values[10].ToString();
                txtD1211.Text = m_statinBase.D1200Values[11].ToString();
                txtD1212.Text = m_statinBase.D1200Values[12].ToString();
                txtD1213.Text = m_statinBase.D1200Values[13].ToString();

                txtD1300.Text = m_statinBase.D1300Values[0].ToString();
                txtD1301.Text = m_statinBase.D1300Values[1].ToString();
                txtD1302.Text = m_statinBase.D1300Values[2].ToString();
                txtD1303.Text = m_statinBase.D1300Values[3].ToString();
                txtD1304.Text = m_statinBase.D1300Values[4].ToString();
                txtD1305.Text = m_statinBase.D1300Values[5].ToString();
                txtD1306.Text = m_statinBase.D1300Values[6].ToString();
                txtD1307.Text = m_statinBase.D1300Values[7].ToString();
                txtD1308.Text = m_statinBase.D1300Values[8].ToString();
                txtD1309.Text = m_statinBase.D1300Values[9].ToString();
                txtD1310.Text = m_statinBase.D1300Values[10].ToString();
                txtD1311.Text = m_statinBase.D1300Values[11].ToString();
                txtD1312.Text = m_statinBase.D1300Values[12].ToString();
                txtD1313.Text = m_statinBase.D1300Values[13].ToString();

                txtPassed.Text = "";
                txtFailed.Text = "";
                txtPassedUnit.Text = m_statinBase.D1000Values[38].ToString();
                txtFailedUnit.Text = m_statinBase.D1000Values[39].ToString();

                double passed = double.Parse(txtPassedUnit.Text.Trim());
                double failed = double.Parse(txtFailedUnit.Text.Trim());
                if(passed + failed > 0.00)
                {
                    double uph = (passed) / (passed + failed);
                    txtUPH.Text = $"{(uph * 100.00).ToString("F02")}%";
                }
                else
                    txtUPH.Text = $"0.00%";

                #endregion

                #region>>>>>>显示载板状态
                txtTrayID.Text = m_statinBase.strTrayIDName;
                txtPCBVituralSN.Text = m_statinBase.strVirtrualPCBSN;

                txtTrayID2.Text = m_statinBase.strTrayIDName2;
                txtPCBVituralSN2.Text = m_statinBase.strVirtrualPCBSN2;

                HPglobal.ShowColorStatus(txtStatus, m_statinBase.emState);
                #endregion

                #region>>>>>>显示MES状态
                if(m_statinBase.bEnbalScanner1)
                {
                    if (m_statinBase.bIsMESChecking1 == 1)
                    {
                        txtMESResult.Text = "MES 校验中....";
                        txtMESResult.BackColor = Color.Yellow;
                    }
                    else
                    {
                        txtMESResult.Text = ((m_statinBase.nMes1VerifyResult == 1 || m_statinBase.nMes1VerifyResult == 3) ? $"MES OK({m_statinBase.nMes1VerifyResult})" : $"MES NG({m_statinBase.nMes1VerifyResult})");
                        txtMESResult.BackColor = ((m_statinBase.nMes1VerifyResult == 1 || m_statinBase.nMes1VerifyResult == 3) ? Color.LightGreen : Color.Red);
                    }
                }
                else
                {
                    if (txtMESResult.BackColor != Color.Gray)
                        txtMESResult.BackColor = Color.Gray;
                }

                if (m_statinBase.bEnbalScanner2)
                {
                    if (m_statinBase.bIsMESChecking2 == 1)
                    {
                        txtMESResult2.Text = "MES 校验中....";
                        txtMESResult2.BackColor = Color.Yellow;
                    }
                    else
                    {
                        txtMESResult2.Text = ((m_statinBase.nMes2VerifyResult == 1 || m_statinBase.nMes2VerifyResult == 3) ? $"MES OK({m_statinBase.nMes2VerifyResult})" : $"MES NG({m_statinBase.nMes2VerifyResult})");
                        txtMESResult2.BackColor = ((m_statinBase.nMes2VerifyResult == 1 || m_statinBase.nMes2VerifyResult == 3) ? Color.LightGreen : Color.Red);
                    }
                }
                else
                {
                    if (txtMESResult2.BackColor != Color.Gray)
                        txtMESResult2.BackColor = Color.Gray;
                }

                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_StatinInfoDetail UpdateStatus 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void timer_StatusMonitor_Tick(object sender, EventArgs e)
        {
            try
            {
                if(this.Visible == true)
                    UpdateStatus();

                #region>>>>>>取出接受与发出的命
                HPglobal.outMessage(richTxt_Msg_Out, m_statinBase.m_queOutMsg);
                HPglobal.outMessage(richTxt_Msg_Out2, m_statinBase.m_queOutMsg2);
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("timer_StatusMonitor_Tick 异常\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            m_statinBase.WriteInt16("D1038", 0 );
            m_statinBase.WriteInt16("D1039", 0 );
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            bool bresult = m_statinBase.WriteInt16("D1008", 1);
        }

        private void btmTriggerBD2_Click(object sender, EventArgs e)
        {
            bool bresult = m_statinBase.WriteInt16("D1011", 1);
        }

        private void 错误清除_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => m_statinBase.ClearError());

        }

        private void 启用禁用_CheckedChanged(object sender, EventArgs e)
        {
            m_statinBase.EnableStation(启用禁用.Checked);
        }

        private void 启动_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => m_statinBase.Start());
            if (m_statinBase.strStationName == "下料组盘工位")
            {
                Task<bool> task7 = Task.Factory.StartNew<bool>(() => { return HPInvoke.panel_RobotUnload.Start(); });
            }
        }

        private void 停止_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => m_statinBase.Stop());
            if (m_statinBase.strStationName == "下料组盘工位")
            {
                Task<bool> task7 = Task.Factory.StartNew<bool>(() => { return HPInvoke.panel_RobotUnload.Stop(); });
            }
        }

        private void simpleButton4_Click_2(object sender, EventArgs e)
        {
            if(m_statinBase.strStationName == "下料组盘工位")
            {
                Task<bool> task7 = Task.Factory.StartNew<bool>(() =>
                {
                    HPInvoke.panel_RobotUnload.Reset();
                    m_statinBase.Reset();
                    return true;
                });
            }
            else
            {
                Task.Factory.StartNew<bool>(() => m_statinBase.Reset());
            }
        }
    }
}