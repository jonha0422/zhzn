﻿namespace Apk_HP_Application.程序文件夹分类._3打码功割控制程序
{
    partial class myCtrlAxis
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.LED_INPOS = new HslCommunication.Controls.UserLantern();
            this.LED_ALARM = new HslCommunication.Controls.UserLantern();
            this.LED_ELN = new HslCommunication.Controls.UserLantern();
            this.LED_ELP = new HslCommunication.Controls.UserLantern();
            this.LED_ORG = new HslCommunication.Controls.UserLantern();
            this.chkEnableAxis = new System.Windows.Forms.CheckBox();
            this.回原点 = new System.Windows.Forms.Button();
            this.启动运动 = new System.Windows.Forms.Button();
            this.停止运动 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkDirect = new System.Windows.Forms.CheckBox();
            this.chkAlso = new System.Windows.Forms.CheckBox();
            this.txtDistance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.报警清除 = new System.Windows.Forms.Button();
            this.txtCurrentPos = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(31, 83);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(59, 17);
            this.label47.TabIndex = 142;
            this.label47.Text = "到位(Inp)";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(31, 57);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(74, 17);
            this.label48.TabIndex = 141;
            this.label48.Text = "报警(Alarm)";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(135, 54);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(70, 17);
            this.label49.TabIndex = 140;
            this.label49.Text = "负限位(EL-)";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(135, 28);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(74, 17);
            this.label50.TabIndex = 139;
            this.label50.Text = "正限位(EL+)";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(31, 28);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(63, 17);
            this.label51.TabIndex = 138;
            this.label51.Text = "原点(Org)";
            // 
            // LED_INPOS
            // 
            this.LED_INPOS.BackColor = System.Drawing.Color.Transparent;
            this.LED_INPOS.LanternBackground = System.Drawing.Color.Gray;
            this.LED_INPOS.Location = new System.Drawing.Point(6, 80);
            this.LED_INPOS.Margin = new System.Windows.Forms.Padding(3, 47, 3, 47);
            this.LED_INPOS.Name = "LED_INPOS";
            this.LED_INPOS.Size = new System.Drawing.Size(24, 24);
            this.LED_INPOS.TabIndex = 135;
            // 
            // LED_ALARM
            // 
            this.LED_ALARM.BackColor = System.Drawing.Color.Transparent;
            this.LED_ALARM.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ALARM.Location = new System.Drawing.Point(6, 54);
            this.LED_ALARM.Margin = new System.Windows.Forms.Padding(3, 47, 3, 47);
            this.LED_ALARM.Name = "LED_ALARM";
            this.LED_ALARM.Size = new System.Drawing.Size(24, 24);
            this.LED_ALARM.TabIndex = 134;
            // 
            // LED_ELN
            // 
            this.LED_ELN.BackColor = System.Drawing.Color.Transparent;
            this.LED_ELN.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ELN.Location = new System.Drawing.Point(110, 51);
            this.LED_ELN.Margin = new System.Windows.Forms.Padding(3, 47, 3, 47);
            this.LED_ELN.Name = "LED_ELN";
            this.LED_ELN.Size = new System.Drawing.Size(24, 24);
            this.LED_ELN.TabIndex = 133;
            // 
            // LED_ELP
            // 
            this.LED_ELP.BackColor = System.Drawing.Color.Transparent;
            this.LED_ELP.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ELP.Location = new System.Drawing.Point(110, 25);
            this.LED_ELP.Margin = new System.Windows.Forms.Padding(3, 47, 3, 47);
            this.LED_ELP.Name = "LED_ELP";
            this.LED_ELP.Size = new System.Drawing.Size(24, 24);
            this.LED_ELP.TabIndex = 132;
            // 
            // LED_ORG
            // 
            this.LED_ORG.BackColor = System.Drawing.Color.Transparent;
            this.LED_ORG.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ORG.Location = new System.Drawing.Point(6, 25);
            this.LED_ORG.Margin = new System.Windows.Forms.Padding(3, 33, 3, 33);
            this.LED_ORG.Name = "LED_ORG";
            this.LED_ORG.Size = new System.Drawing.Size(24, 24);
            this.LED_ORG.TabIndex = 131;
            // 
            // chkEnableAxis
            // 
            this.chkEnableAxis.AutoSize = true;
            this.chkEnableAxis.Location = new System.Drawing.Point(113, 79);
            this.chkEnableAxis.Name = "chkEnableAxis";
            this.chkEnableAxis.Size = new System.Drawing.Size(92, 21);
            this.chkEnableAxis.TabIndex = 143;
            this.chkEnableAxis.Text = "启用/禁用轴";
            this.chkEnableAxis.UseVisualStyleBackColor = true;
            this.chkEnableAxis.CheckedChanged += new System.EventHandler(this.chkEnableAxis_CheckedChanged);
            // 
            // 回原点
            // 
            this.回原点.Location = new System.Drawing.Point(0, 206);
            this.回原点.Name = "回原点";
            this.回原点.Size = new System.Drawing.Size(100, 43);
            this.回原点.TabIndex = 144;
            this.回原点.Text = "回原点";
            this.回原点.UseVisualStyleBackColor = true;
            this.回原点.Click += new System.EventHandler(this.回原点_Click);
            // 
            // 启动运动
            // 
            this.启动运动.Location = new System.Drawing.Point(105, 206);
            this.启动运动.Name = "启动运动";
            this.启动运动.Size = new System.Drawing.Size(100, 43);
            this.启动运动.TabIndex = 145;
            this.启动运动.Text = "启动运动";
            this.启动运动.UseVisualStyleBackColor = true;
            this.启动运动.Click += new System.EventHandler(this.启动运动_Click);
            // 
            // 停止运动
            // 
            this.停止运动.Location = new System.Drawing.Point(105, 255);
            this.停止运动.Name = "停止运动";
            this.停止运动.Size = new System.Drawing.Size(100, 43);
            this.停止运动.TabIndex = 146;
            this.停止运动.Text = "停止运动";
            this.停止运动.UseVisualStyleBackColor = true;
            this.停止运动.Click += new System.EventHandler(this.停止运动_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LavenderBlush;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtCurrentPos);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtDistance);
            this.groupBox1.Controls.Add(this.chkAlso);
            this.groupBox1.Controls.Add(this.chkDirect);
            this.groupBox1.Location = new System.Drawing.Point(6, 106);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(234, 98);
            this.groupBox1.TabIndex = 147;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "运动选项";
            // 
            // chkDirect
            // 
            this.chkDirect.AutoSize = true;
            this.chkDirect.Location = new System.Drawing.Point(7, 22);
            this.chkDirect.Name = "chkDirect";
            this.chkDirect.Size = new System.Drawing.Size(80, 21);
            this.chkDirect.TabIndex = 148;
            this.chkDirect.Text = "正/反方向";
            this.chkDirect.UseVisualStyleBackColor = true;
            this.chkDirect.CheckedChanged += new System.EventHandler(this.chkDirect_CheckedChanged);
            // 
            // chkAlso
            // 
            this.chkAlso.AutoSize = true;
            this.chkAlso.Location = new System.Drawing.Point(7, 48);
            this.chkAlso.Name = "chkAlso";
            this.chkAlso.Size = new System.Drawing.Size(104, 21);
            this.chkAlso.TabIndex = 149;
            this.chkAlso.Text = "绝对/相对运动";
            this.chkAlso.UseVisualStyleBackColor = true;
            this.chkAlso.CheckedChanged += new System.EventHandler(this.chkAlso_CheckedChanged);
            // 
            // txtDistance
            // 
            this.txtDistance.Location = new System.Drawing.Point(114, 34);
            this.txtDistance.Name = "txtDistance";
            this.txtDistance.Size = new System.Drawing.Size(66, 23);
            this.txtDistance.TabIndex = 150;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(111, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 148;
            this.label1.Text = "运动距离:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.报警清除);
            this.groupBox2.Controls.Add(this.LED_ORG);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.LED_ELP);
            this.groupBox2.Controls.Add(this.停止运动);
            this.groupBox2.Controls.Add(this.LED_ELN);
            this.groupBox2.Controls.Add(this.启动运动);
            this.groupBox2.Controls.Add(this.LED_ALARM);
            this.groupBox2.Controls.Add(this.回原点);
            this.groupBox2.Controls.Add(this.LED_INPOS);
            this.groupBox2.Controls.Add(this.chkEnableAxis);
            this.groupBox2.Controls.Add(this.label51);
            this.groupBox2.Controls.Add(this.label47);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.label48);
            this.groupBox2.Controls.Add(this.label49);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(240, 297);
            this.groupBox2.TabIndex = 148;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Axis 名字:";
            // 
            // 报警清除
            // 
            this.报警清除.Location = new System.Drawing.Point(0, 255);
            this.报警清除.Name = "报警清除";
            this.报警清除.Size = new System.Drawing.Size(100, 43);
            this.报警清除.TabIndex = 148;
            this.报警清除.Text = "报警清除";
            this.报警清除.UseVisualStyleBackColor = true;
            this.报警清除.Click += new System.EventHandler(this.报警清除_Click);
            // 
            // txtCurrentPos
            // 
            this.txtCurrentPos.BackColor = System.Drawing.Color.White;
            this.txtCurrentPos.Location = new System.Drawing.Point(107, 70);
            this.txtCurrentPos.Name = "txtCurrentPos";
            this.txtCurrentPos.ReadOnly = true;
            this.txtCurrentPos.Size = new System.Drawing.Size(73, 23);
            this.txtCurrentPos.TabIndex = 151;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 17);
            this.label2.TabIndex = 152;
            this.label2.Text = "当前位置(pulse)";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 150;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // myCtrlAxis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "myCtrlAxis";
            this.Size = new System.Drawing.Size(249, 305);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private HslCommunication.Controls.UserLantern LED_INPOS;
        private HslCommunication.Controls.UserLantern LED_ALARM;
        private HslCommunication.Controls.UserLantern LED_ELN;
        private HslCommunication.Controls.UserLantern LED_ELP;
        private HslCommunication.Controls.UserLantern LED_ORG;
        private System.Windows.Forms.CheckBox chkEnableAxis;
        private System.Windows.Forms.Button 回原点;
        private System.Windows.Forms.Button 启动运动;
        private System.Windows.Forms.Button 停止运动;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDistance;
        private System.Windows.Forms.CheckBox chkAlso;
        private System.Windows.Forms.CheckBox chkDirect;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button 报警清除;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCurrentPos;
        private System.Windows.Forms.Timer timer1;
    }
}
