﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.IO;
using HalconDotNet;
using static ZHToolBox.HalconVision.HDVisionTypes;

namespace ZHToolBox.HalconVision
{
    public class MethodBase : Object
    {
        [Category("基本"), DefaultValue(""), DisplayName("名称")]
        public string Name { get; set; }

        [Category("基本"), DefaultValue(true), DisplayName("是否启用")]
        public bool Enabled { get; set; }

        [Category("视觉方法参数"), DefaultValue(true), DisplayName("相机曝光")]
        public double Exposure { get; set; }

        [Category("视觉方法参数"), DefaultValue(true), DisplayName("相机对比度")]
        public short Contrast { get; set; }

        [Category("视觉方法参数"), DefaultValue(true), DisplayName("备用数据1")]
        public int Reserved1 { get; set; }

        [Category("视觉方法参数"), DefaultValue(true), DisplayName("备用数据2")]
        public int Reserved2 { get; set; }

        [Category("视觉方法参数"), DefaultValue(true), DisplayName("备用数据3")]
        public int Reserved3 { get; set; }

        public MethodBase()
        {
            DateTime dateTime = DateTime.Now;
            Name = dateTime.ToString("yyyyMMddHHmmss") + $".{dateTime.Millisecond.ToString("D03")}请定义功能名称";

            Enabled = true;

            Exposure = 20000.00;
            Contrast = 50;
        }
        //ZHToolBox.General

        public bool Xml_SerializeObjToFile(string strPathName)
        {
            try
            {
                string strPath = System.IO.Path.GetDirectoryName(strPathName);
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);

                Type typeObj = this.GetType();
                XmlSerializer xs = new XmlSerializer(typeObj);//typeof(ProductSettings)
                StringWriter sw = new StringWriter();

                xs.Serialize(sw, this);
                string strxml = sw.ToString();


                FileStream fs = new FileStream(strPathName, FileMode.Create, FileAccess.Write);
                StreamWriter stream = new StreamWriter(fs, Encoding.UTF8);
                stream.WriteLine(strxml);
                stream.Close();
                fs.Close();

                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public object Xml_DeserializeObjFromFile(string strPathName)
        {
            string strPath = System.IO.Path.GetDirectoryName(strPathName);
            if (!Directory.Exists(strPath))
                Directory.CreateDirectory(strPath);


            StreamReader sr = new StreamReader(strPathName, Encoding.UTF8);
            string strTemp;
            string strXml = "";
            while ((strTemp = sr.ReadLine()) != null)
            {
                strXml += strTemp;
            }
            sr.Close();


            XmlSerializer xs = new XmlSerializer(this.GetType());
            StringReader strReader = new StringReader(strXml);
            object obj = xs.Deserialize(strReader);
            sr.Close();

            return obj;
        }
    }

    public class CheckResultDef
    {
        [TypeConverter(typeof(ExpandableObjectConverter)), Category("结果"), DefaultValue(3), DisplayName("检测区域")]
        public RIO_DES CheckedRIO { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter)), Category("结果"), DefaultValue(3), DisplayName("检测结果")]
        public MATCHINGRESULT_DES MatchingResult { get; set; }

        [Category("结果"), DefaultValue(0), DisplayName("检测结果")]
        public int Result { get; set; }

        [Category("结果"), DefaultValue(0), DisplayName("检测状态")]
        public bool ResultStatus { get; set; }

        public CheckResultDef()
        {
            CheckedRIO = new RIO_DES();
            MatchingResult = new MATCHINGRESULT_DES();
        }
    }

    public class HalconGeneral
    {
        public static double PI = 3.1415926;
        public static double PI2 = PI * 2;

       public HalconGeneral()
        {

        }
        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static void HD_DisplayMessage(HTuple hv_WindowHandle, HTuple hv_String, HTuple hv_CoordSystem, HTuple hv_Row, HTuple hv_Column, HTuple hv_Color, HTuple hv_Box)
        {
            try
            {
                // Local iconic variables 

                // Local control variables 

                HTuple hv_Red = null, hv_Green = null, hv_Blue = null;
                HTuple hv_Row1Part = null, hv_Column1Part = null, hv_Row2Part = null;
                HTuple hv_Column2Part = null, hv_RowWin = null, hv_ColumnWin = null;
                HTuple hv_WidthWin = null, hv_HeightWin = null, hv_MaxAscent = null;
                HTuple hv_MaxDescent = null, hv_MaxWidth = null, hv_MaxHeight = null;
                HTuple hv_R1 = new HTuple(), hv_C1 = new HTuple(), hv_FactorRow = new HTuple();
                HTuple hv_FactorColumn = new HTuple(), hv_UseShadow = null;
                HTuple hv_ShadowColor = null, hv_Exception = new HTuple();
                HTuple hv_Width = new HTuple(), hv_Index = new HTuple();
                HTuple hv_Ascent = new HTuple(), hv_Descent = new HTuple();
                HTuple hv_W = new HTuple(), hv_H = new HTuple(), hv_FrameHeight = new HTuple();
                HTuple hv_FrameWidth = new HTuple(), hv_R2 = new HTuple();
                HTuple hv_C2 = new HTuple(), hv_DrawMode = new HTuple();
                HTuple hv_CurrentColor = new HTuple();
                HTuple hv_Box_COPY_INP_TMP = hv_Box.Clone();
                HTuple hv_Color_COPY_INP_TMP = hv_Color.Clone();
                HTuple hv_Column_COPY_INP_TMP = hv_Column.Clone();
                HTuple hv_Row_COPY_INP_TMP = hv_Row.Clone();
                HTuple hv_String_COPY_INP_TMP = hv_String.Clone();

                // Initialize local and output iconic variables 
                //This procedure displays text in a graphics window.
                //
                //Input parameters:
                //WindowHandle: The WindowHandle of the graphics window, where
                //   the message should be displayed
                //String: A tuple of strings containing the text message to be displayed
                //CoordSystem: If set to 'window', the text position is given
                //   with respect to the window coordinate system.
                //   If set to 'image', image coordinates are used.
                //   (This may be useful in zoomed images.)
                //Row: The row coordinate of the desired text position
                //   If set to -1, a default value of 12 is used.
                //Column: The column coordinate of the desired text position
                //   If set to -1, a default value of 12 is used.
                //Color: defines the color of the text as string.
                //   If set to [], '' or 'auto' the currently set color is used.
                //   If a tuple of strings is passed, the colors are used cyclically
                //   for each new textline.
                //Box: If Box[0] is set to 'true', the text is written within an orange box.
                //     If set to' false', no box is displayed.
                //     If set to a color string (e.g. 'white', '#FF00CC', etc.),
                //       the text is written in a box of that color.
                //     An optional second value for Box (Box[1]) controls if a shadow is displayed:
                //       'true' -> display a shadow in a default color
                //       'false' -> display no shadow (same as if no second value is given)
                //       otherwise -> use given string as color string for the shadow color
                //
                //Prepare window
                HOperatorSet.GetRgb(hv_WindowHandle, out hv_Red, out hv_Green, out hv_Blue);
                HOperatorSet.GetPart(hv_WindowHandle, out hv_Row1Part, out hv_Column1Part, out hv_Row2Part, out hv_Column2Part);
                HOperatorSet.GetWindowExtents(hv_WindowHandle, out hv_RowWin, out hv_ColumnWin, out hv_WidthWin, out hv_HeightWin);
                HOperatorSet.SetPart(hv_WindowHandle, 0, 0, hv_HeightWin - 1, hv_WidthWin - 1);
                //
                //default settings
                if ((int)(new HTuple(hv_Row_COPY_INP_TMP.TupleEqual(-1))) != 0)
                {
                    hv_Row_COPY_INP_TMP = 12;
                }
                if ((int)(new HTuple(hv_Column_COPY_INP_TMP.TupleEqual(-1))) != 0)
                {
                    hv_Column_COPY_INP_TMP = 12;
                }
                if ((int)(new HTuple(hv_Color_COPY_INP_TMP.TupleEqual(new HTuple()))) != 0)
                {
                    hv_Color_COPY_INP_TMP = "";
                }
                //
                hv_String_COPY_INP_TMP = ((("" + hv_String_COPY_INP_TMP) + "")).TupleSplit("\n");
                //
                //Estimate extentions of text depending on font size.
                HOperatorSet.GetFontExtents(hv_WindowHandle, out hv_MaxAscent, out hv_MaxDescent, out hv_MaxWidth, out hv_MaxHeight);
                if ((int)(new HTuple(hv_CoordSystem.TupleEqual("window"))) != 0)
                {
                    hv_R1 = hv_Row_COPY_INP_TMP.Clone();
                    hv_C1 = hv_Column_COPY_INP_TMP.Clone();
                }
                else
                {
                    //Transform image to window coordinates
                    hv_FactorRow = (1.0 * hv_HeightWin) / ((hv_Row2Part - hv_Row1Part) + 1);
                    hv_FactorColumn = (1.0 * hv_WidthWin) / ((hv_Column2Part - hv_Column1Part) + 1);
                    hv_R1 = ((hv_Row_COPY_INP_TMP - hv_Row1Part) + 0.5) * hv_FactorRow;
                    hv_C1 = ((hv_Column_COPY_INP_TMP - hv_Column1Part) + 0.5) * hv_FactorColumn;
                }
                //
                //Display text box depending on text size
                hv_UseShadow = 1;
                hv_ShadowColor = "gray";
                if ((int)(new HTuple(((hv_Box_COPY_INP_TMP.TupleSelect(0))).TupleEqual("true"))) != 0)
                {
                    if (hv_Box_COPY_INP_TMP == null)
                        hv_Box_COPY_INP_TMP = new HTuple();
                    hv_Box_COPY_INP_TMP[0] = "#fce9d4";
                    hv_ShadowColor = "#f28d26";
                }
                if ((int)(new HTuple((new HTuple(hv_Box_COPY_INP_TMP.TupleLength())).TupleGreater(1))) != 0)
                {
                    if ((int)(new HTuple(((hv_Box_COPY_INP_TMP.TupleSelect(1))).TupleEqual("true"))) != 0)
                    {
                        //Use default ShadowColor set above
                    }
                    else if ((int)(new HTuple(((hv_Box_COPY_INP_TMP.TupleSelect(1))).TupleEqual("false"))) != 0)
                    {
                        hv_UseShadow = 0;
                    }
                    else
                    {
                        hv_ShadowColor = hv_Box_COPY_INP_TMP[1];
                        //Valid color?
                        try
                        {
                            HOperatorSet.SetColor(hv_WindowHandle, hv_Box_COPY_INP_TMP.TupleSelect(1));
                        }
                        // catch (Exception) 
                        catch (HalconException HDevExpDefaultException1)
                        {
                            HDevExpDefaultException1.ToHTuple(out hv_Exception);
                            hv_Exception = "Wrong value of control parameter Box[1] (must be a 'true', 'false', or a valid color string)";
                            throw new HalconException(hv_Exception);
                        }
                    }
                }
                if ((int)(new HTuple(((hv_Box_COPY_INP_TMP.TupleSelect(0))).TupleNotEqual("false"))) != 0)
                {
                    //Valid color?
                    try
                    {
                        HOperatorSet.SetColor(hv_WindowHandle, hv_Box_COPY_INP_TMP.TupleSelect(0));
                    }
                    // catch (Exception) 
                    catch (HalconException HDevExpDefaultException1)
                    {
                        HDevExpDefaultException1.ToHTuple(out hv_Exception);
                        hv_Exception = "Wrong value of control parameter Box[0] (must be a 'true', 'false', or a valid color string)";
                        throw new HalconException(hv_Exception);
                    }
                    //Calculate box extents
                    hv_String_COPY_INP_TMP = (" " + hv_String_COPY_INP_TMP) + " ";
                    hv_Width = new HTuple();
                    for (hv_Index = 0; (int)hv_Index <= (int)((new HTuple(hv_String_COPY_INP_TMP.TupleLength())) - 1); hv_Index = (int)hv_Index + 1)
                    {
                        HOperatorSet.GetStringExtents(hv_WindowHandle, hv_String_COPY_INP_TMP.TupleSelect(hv_Index), out hv_Ascent, out hv_Descent, out hv_W, out hv_H);
                        hv_Width = hv_Width.TupleConcat(hv_W);
                    }
                    hv_FrameHeight = hv_MaxHeight * (new HTuple(hv_String_COPY_INP_TMP.TupleLength()));
                    hv_FrameWidth = (((new HTuple(0)).TupleConcat(hv_Width))).TupleMax();
                    hv_R2 = hv_R1 + hv_FrameHeight;
                    hv_C2 = hv_C1 + hv_FrameWidth;
                    //Display rectangles
                    HOperatorSet.GetDraw(hv_WindowHandle, out hv_DrawMode);
                    HOperatorSet.SetDraw(hv_WindowHandle, "fill");
                    //Set shadow color
                    HOperatorSet.SetColor(hv_WindowHandle, hv_ShadowColor);
                    if ((int)(hv_UseShadow) != 0)
                    {
                        HOperatorSet.DispRectangle1(hv_WindowHandle, hv_R1 + 1, hv_C1 + 1, hv_R2 + 1, hv_C2 + 1);
                    }
                    //Set box color
                    HOperatorSet.SetColor(hv_WindowHandle, hv_Box_COPY_INP_TMP.TupleSelect(0));
                    HOperatorSet.DispRectangle1(hv_WindowHandle, hv_R1, hv_C1, hv_R2, hv_C2);
                    HOperatorSet.SetDraw(hv_WindowHandle, hv_DrawMode);
                }
                //Write text.
                for (hv_Index = 0; (int)hv_Index <= (int)((new HTuple(hv_String_COPY_INP_TMP.TupleLength())) - 1); hv_Index = (int)hv_Index + 1)
                {
                    hv_CurrentColor = hv_Color_COPY_INP_TMP.TupleSelect(hv_Index % (new HTuple(hv_Color_COPY_INP_TMP.TupleLength())));
                    if ((int)((new HTuple(hv_CurrentColor.TupleNotEqual(""))).TupleAnd(new HTuple(hv_CurrentColor.TupleNotEqual("auto")))) != 0)
                    {
                        HOperatorSet.SetColor(hv_WindowHandle, hv_CurrentColor);
                    }
                    else
                    {
                        HOperatorSet.SetRgb(hv_WindowHandle, hv_Red, hv_Green, hv_Blue);
                    }
                    hv_Row_COPY_INP_TMP = hv_R1 + (hv_MaxHeight * hv_Index);
                    HOperatorSet.SetTposition(hv_WindowHandle, hv_Row_COPY_INP_TMP, hv_C1);
                    HOperatorSet.WriteString(hv_WindowHandle, hv_String_COPY_INP_TMP.TupleSelect(hv_Index));
                }
                //Reset changed window settings
                HOperatorSet.SetRgb(hv_WindowHandle, hv_Red, hv_Green, hv_Blue);
                HOperatorSet.SetPart(hv_WindowHandle, hv_Row1Part, hv_Column1Part, hv_Row2Part, hv_Column2Part);

                return;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static void HD_SetDisplayFont(HTuple hv_WindowHandle, HTuple hv_Size, HTuple hv_Font, HTuple hv_Bold, HTuple hv_Slant)
        {
            try
            {
                // Local iconic variables 

                // Local control variables 

                HTuple hv_OS = null, hv_BufferWindowHandle = new HTuple();
                HTuple hv_Ascent = new HTuple(), hv_Descent = new HTuple();
                HTuple hv_Width = new HTuple(), hv_Height = new HTuple();
                HTuple hv_Scale = new HTuple(), hv_Exception = new HTuple();
                HTuple hv_SubFamily = new HTuple(), hv_Fonts = new HTuple();
                HTuple hv_SystemFonts = new HTuple(), hv_Guess = new HTuple();
                HTuple hv_I = new HTuple(), hv_Index = new HTuple(), hv_AllowedFontSizes = new HTuple();
                HTuple hv_Distances = new HTuple(), hv_Indices = new HTuple();
                HTuple hv_FontSelRegexp = new HTuple(), hv_FontsCourier = new HTuple();
                HTuple hv_Bold_COPY_INP_TMP = hv_Bold.Clone();
                HTuple hv_Font_COPY_INP_TMP = hv_Font.Clone();
                HTuple hv_Size_COPY_INP_TMP = hv_Size.Clone();
                HTuple hv_Slant_COPY_INP_TMP = hv_Slant.Clone();

                // Initialize local and output iconic variables 
                //This procedure sets the text font of the current window with
                //the specified attributes.
                //It is assumed that following fonts are installed on the system:
                //Windows: Courier New, Arial Times New Roman
                //Mac OS X: CourierNewPS, Arial, TimesNewRomanPS
                //Linux: courier, helvetica, times
                //Because fonts are displayed smaller on Linux than on Windows,
                //a scaling factor of 1.25 is used the get comparable results.
                //For Linux, only a limited number of font sizes is supported,
                //to get comparable results, it is recommended to use one of the
                //following sizes: 9, 11, 14, 16, 20, 27
                //(which will be mapped internally on Linux systems to 11, 14, 17, 20, 25, 34)
                //
                //Input parameters:
                //WindowHandle: The graphics window for which the font will be set
                //Size: The font size. If Size=-1, the default of 16 is used.
                //Bold: If set to 'true', a bold font is used
                //Slant: If set to 'true', a slanted font is used
                //
                HOperatorSet.GetSystem("operating_system", out hv_OS);
                // dev_get_preferences(...); only in hdevelop
                // dev_set_preferences(...); only in hdevelop
                if ((int)((new HTuple(hv_Size_COPY_INP_TMP.TupleEqual(new HTuple()))).TupleOr(new HTuple(hv_Size_COPY_INP_TMP.TupleEqual(-1)))) != 0)
                {
                    hv_Size_COPY_INP_TMP = 16;
                }
                if ((int)(new HTuple(((hv_OS.TupleSubstr(0, 2))).TupleEqual("Win"))) != 0)
                {
                    //Set font on Windows systems
                    try
                    {
                        //Check, if font scaling is switched on
                        HOperatorSet.OpenWindow(0, 0, 256, 256, 0, "buffer", "", out hv_BufferWindowHandle);
                        HOperatorSet.SetFont(hv_BufferWindowHandle, "-Consolas-16-*-0-*-*-1-");
                        HOperatorSet.GetStringExtents(hv_BufferWindowHandle, "test_string", out hv_Ascent, out hv_Descent, out hv_Width, out hv_Height);
                        //Expected width is 110
                        hv_Scale = 110.0 / hv_Width;
                        hv_Size_COPY_INP_TMP = ((hv_Size_COPY_INP_TMP * hv_Scale)).TupleInt();
                        HOperatorSet.CloseWindow(hv_BufferWindowHandle);
                    }
                    // catch (Exception) 
                    catch (HalconException HDevExpDefaultException1)
                    {
                        HDevExpDefaultException1.ToHTuple(out hv_Exception);
                        //throw (Exception)
                    }
                    if ((int)((new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("Courier"))).TupleOr(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("courier")))) != 0)
                    {
                        hv_Font_COPY_INP_TMP = "Courier New";
                    }
                    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("mono"))) != 0)
                    {
                        hv_Font_COPY_INP_TMP = "Consolas";
                    }
                    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("sans"))) != 0)
                    {
                        hv_Font_COPY_INP_TMP = "Arial";
                    }
                    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("serif"))) != 0)
                    {
                        hv_Font_COPY_INP_TMP = "Times New Roman";
                    }
                    if ((int)(new HTuple(hv_Bold_COPY_INP_TMP.TupleEqual("true"))) != 0)
                    {
                        hv_Bold_COPY_INP_TMP = 1;
                    }
                    else if ((int)(new HTuple(hv_Bold_COPY_INP_TMP.TupleEqual("false"))) != 0)
                    {
                        hv_Bold_COPY_INP_TMP = 0;
                    }
                    else
                    {
                        hv_Exception = "Wrong value of control parameter Bold";
                        throw new HalconException(hv_Exception);
                    }
                    if ((int)(new HTuple(hv_Slant_COPY_INP_TMP.TupleEqual("true"))) != 0)
                    {
                        hv_Slant_COPY_INP_TMP = 1;
                    }
                    else if ((int)(new HTuple(hv_Slant_COPY_INP_TMP.TupleEqual("false"))) != 0)
                    {
                        hv_Slant_COPY_INP_TMP = 0;
                    }
                    else
                    {
                        hv_Exception = "Wrong value of control parameter Slant";
                        throw new HalconException(hv_Exception);
                    }
                    try
                    {
                        HOperatorSet.SetFont(hv_WindowHandle, ((((((("-" + hv_Font_COPY_INP_TMP) + "-") + hv_Size_COPY_INP_TMP) + "-*-") + hv_Slant_COPY_INP_TMP) + "-*-*-") + hv_Bold_COPY_INP_TMP) + "-");
                    }
                    // catch (Exception) 
                    catch (HalconException HDevExpDefaultException1)
                    {
                        HDevExpDefaultException1.ToHTuple(out hv_Exception);
                        //throw (Exception)
                    }
                }
                else if ((int)(new HTuple(((hv_OS.TupleSubstr(0, 2))).TupleEqual("Dar"))) != 0)
                {
                    //Set font on Mac OS X systems. Since OS X does not have a strict naming
                    //scheme for font attributes, we use tables to determine the correct font
                    //name.
                    hv_SubFamily = 0;
                    if ((int)(new HTuple(hv_Slant_COPY_INP_TMP.TupleEqual("true"))) != 0)
                    {
                        hv_SubFamily = hv_SubFamily.TupleBor(1);
                    }
                    else if ((int)(new HTuple(hv_Slant_COPY_INP_TMP.TupleNotEqual("false"))) != 0)
                    {
                        hv_Exception = "Wrong value of control parameter Slant";
                        throw new HalconException(hv_Exception);
                    }
                    if ((int)(new HTuple(hv_Bold_COPY_INP_TMP.TupleEqual("true"))) != 0)
                    {
                        hv_SubFamily = hv_SubFamily.TupleBor(2);
                    }
                    else if ((int)(new HTuple(hv_Bold_COPY_INP_TMP.TupleNotEqual("false"))) != 0)
                    {
                        hv_Exception = "Wrong value of control parameter Bold";
                        throw new HalconException(hv_Exception);
                    }
                    if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("mono"))) != 0)
                    {
                        hv_Fonts = new HTuple();
                        hv_Fonts[0] = "Menlo-Regular";
                        hv_Fonts[1] = "Menlo-Italic";
                        hv_Fonts[2] = "Menlo-Bold";
                        hv_Fonts[3] = "Menlo-BoldItalic";
                    }
                    else if ((int)((new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("Courier"))).TupleOr(
                        new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("courier")))) != 0)
                    {
                        hv_Fonts = new HTuple();
                        hv_Fonts[0] = "CourierNewPSMT";
                        hv_Fonts[1] = "CourierNewPS-ItalicMT";
                        hv_Fonts[2] = "CourierNewPS-BoldMT";
                        hv_Fonts[3] = "CourierNewPS-BoldItalicMT";
                    }
                    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("sans"))) != 0)
                    {
                        hv_Fonts = new HTuple();
                        hv_Fonts[0] = "ArialMT";
                        hv_Fonts[1] = "Arial-ItalicMT";
                        hv_Fonts[2] = "Arial-BoldMT";
                        hv_Fonts[3] = "Arial-BoldItalicMT";
                    }
                    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("serif"))) != 0)
                    {
                        hv_Fonts = new HTuple();
                        hv_Fonts[0] = "TimesNewRomanPSMT";
                        hv_Fonts[1] = "TimesNewRomanPS-ItalicMT";
                        hv_Fonts[2] = "TimesNewRomanPS-BoldMT";
                        hv_Fonts[3] = "TimesNewRomanPS-BoldItalicMT";
                    }
                    else
                    {
                        //Attempt to figure out which of the fonts installed on the system
                        //the user could have meant.
                        HOperatorSet.QueryFont(hv_WindowHandle, out hv_SystemFonts);
                        hv_Fonts = new HTuple();
                        hv_Fonts = hv_Fonts.TupleConcat(hv_Font_COPY_INP_TMP);
                        hv_Fonts = hv_Fonts.TupleConcat(hv_Font_COPY_INP_TMP);
                        hv_Fonts = hv_Fonts.TupleConcat(hv_Font_COPY_INP_TMP);
                        hv_Fonts = hv_Fonts.TupleConcat(hv_Font_COPY_INP_TMP);
                        hv_Guess = new HTuple();
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP);
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-Regular");
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "MT");
                        for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Guess.TupleLength())) - 1); hv_I = (int)hv_I + 1)
                        {
                            HOperatorSet.TupleFind(hv_SystemFonts, hv_Guess.TupleSelect(hv_I), out hv_Index);
                            if ((int)(new HTuple(hv_Index.TupleNotEqual(-1))) != 0)
                            {
                                if (hv_Fonts == null)
                                    hv_Fonts = new HTuple();
                                hv_Fonts[0] = hv_Guess.TupleSelect(hv_I);
                                break;
                            }
                        }
                        //Guess name of slanted font
                        hv_Guess = new HTuple();
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-Italic");
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-ItalicMT");
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-Oblique");
                        for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Guess.TupleLength())) - 1); hv_I = (int)hv_I + 1)
                        {
                            HOperatorSet.TupleFind(hv_SystemFonts, hv_Guess.TupleSelect(hv_I), out hv_Index);
                            if ((int)(new HTuple(hv_Index.TupleNotEqual(-1))) != 0)
                            {
                                if (hv_Fonts == null)
                                    hv_Fonts = new HTuple();
                                hv_Fonts[1] = hv_Guess.TupleSelect(hv_I);
                                break;
                            }
                        }
                        //Guess name of bold font
                        hv_Guess = new HTuple();
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-Bold");
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-BoldMT");
                        for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Guess.TupleLength())) - 1); hv_I = (int)hv_I + 1)
                        {
                            HOperatorSet.TupleFind(hv_SystemFonts, hv_Guess.TupleSelect(hv_I), out hv_Index);
                            if ((int)(new HTuple(hv_Index.TupleNotEqual(-1))) != 0)
                            {
                                if (hv_Fonts == null)
                                    hv_Fonts = new HTuple();
                                hv_Fonts[2] = hv_Guess.TupleSelect(hv_I);
                                break;
                            }
                        }
                        //Guess name of bold slanted font
                        hv_Guess = new HTuple();
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-BoldItalic");
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-BoldItalicMT");
                        hv_Guess = hv_Guess.TupleConcat(hv_Font_COPY_INP_TMP + "-BoldOblique");
                        for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Guess.TupleLength())) - 1); hv_I = (int)hv_I + 1)
                        {
                            HOperatorSet.TupleFind(hv_SystemFonts, hv_Guess.TupleSelect(hv_I), out hv_Index);
                            if ((int)(new HTuple(hv_Index.TupleNotEqual(-1))) != 0)
                            {
                                if (hv_Fonts == null)
                                    hv_Fonts = new HTuple();
                                hv_Fonts[3] = hv_Guess.TupleSelect(hv_I);
                                break;
                            }
                        }
                    }
                    hv_Font_COPY_INP_TMP = hv_Fonts.TupleSelect(hv_SubFamily);
                    try
                    {
                        HOperatorSet.SetFont(hv_WindowHandle, (hv_Font_COPY_INP_TMP + "-") + hv_Size_COPY_INP_TMP);
                    }
                    // catch (Exception) 
                    catch (HalconException HDevExpDefaultException1)
                    {
                        HDevExpDefaultException1.ToHTuple(out hv_Exception);
                        //throw (Exception)
                    }
                }
                else
                {
                    //Set font for UNIX systems
                    hv_Size_COPY_INP_TMP = hv_Size_COPY_INP_TMP * 1.25;
                    hv_AllowedFontSizes = new HTuple();
                    hv_AllowedFontSizes[0] = 11;
                    hv_AllowedFontSizes[1] = 14;
                    hv_AllowedFontSizes[2] = 17;
                    hv_AllowedFontSizes[3] = 20;
                    hv_AllowedFontSizes[4] = 25;
                    hv_AllowedFontSizes[5] = 34;
                    if ((int)(new HTuple(((hv_AllowedFontSizes.TupleFind(hv_Size_COPY_INP_TMP))).TupleEqual(-1))) != 0)
                    {
                        hv_Distances = ((hv_AllowedFontSizes - hv_Size_COPY_INP_TMP)).TupleAbs();
                        HOperatorSet.TupleSortIndex(hv_Distances, out hv_Indices);
                        hv_Size_COPY_INP_TMP = hv_AllowedFontSizes.TupleSelect(hv_Indices.TupleSelect(0));
                    }
                    if ((int)((new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("mono"))).TupleOr(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("Courier")))) != 0)
                    {
                        hv_Font_COPY_INP_TMP = "courier";
                    }
                    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("sans"))) != 0)
                    {
                        hv_Font_COPY_INP_TMP = "helvetica";
                    }
                    else if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("serif"))) != 0)
                    {
                        hv_Font_COPY_INP_TMP = "times";
                    }
                    if ((int)(new HTuple(hv_Bold_COPY_INP_TMP.TupleEqual("true"))) != 0)
                    {
                        hv_Bold_COPY_INP_TMP = "bold";
                    }
                    else if ((int)(new HTuple(hv_Bold_COPY_INP_TMP.TupleEqual("false"))) != 0)
                    {
                        hv_Bold_COPY_INP_TMP = "medium";
                    }
                    else
                    {
                        hv_Exception = "Wrong value of control parameter Bold";
                        throw new HalconException(hv_Exception);
                    }
                    if ((int)(new HTuple(hv_Slant_COPY_INP_TMP.TupleEqual("true"))) != 0)
                    {
                        if ((int)(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("times"))) != 0)
                        {
                            hv_Slant_COPY_INP_TMP = "i";
                        }
                        else
                        {
                            hv_Slant_COPY_INP_TMP = "o";
                        }
                    }
                    else if ((int)(new HTuple(hv_Slant_COPY_INP_TMP.TupleEqual("false"))) != 0)
                    {
                        hv_Slant_COPY_INP_TMP = "r";
                    }
                    else
                    {
                        hv_Exception = "Wrong value of control parameter Slant";
                        throw new HalconException(hv_Exception);
                    }
                    try
                    {
                        HOperatorSet.SetFont(hv_WindowHandle, ((((((("-adobe-" + hv_Font_COPY_INP_TMP) + "-") + hv_Bold_COPY_INP_TMP) + "-") + hv_Slant_COPY_INP_TMP) + "-normal-*-") + hv_Size_COPY_INP_TMP) + "-*-*-*-*-*-*-*");
                    }
                    // catch (Exception) 
                    catch (HalconException HDevExpDefaultException1)
                    {
                        HDevExpDefaultException1.ToHTuple(out hv_Exception);
                        if ((int)((new HTuple(((hv_OS.TupleSubstr(0, 4))).TupleEqual("Linux"))).TupleAnd(new HTuple(hv_Font_COPY_INP_TMP.TupleEqual("courier")))) != 0)
                        {
                            HOperatorSet.QueryFont(hv_WindowHandle, out hv_Fonts);
                            hv_FontSelRegexp = (("^-[^-]*-[^-]*[Cc]ourier[^-]*-" + hv_Bold_COPY_INP_TMP) + "-") + hv_Slant_COPY_INP_TMP;
                            hv_FontsCourier = ((hv_Fonts.TupleRegexpSelect(hv_FontSelRegexp))).TupleRegexpMatch(hv_FontSelRegexp);
                            if ((int)(new HTuple((new HTuple(hv_FontsCourier.TupleLength())).TupleEqual(0))) != 0)
                            {
                                hv_Exception = "Wrong font name";
                                //throw (Exception)
                            }
                            else
                            {
                                try
                                {
                                    HOperatorSet.SetFont(hv_WindowHandle, (((hv_FontsCourier.TupleSelect(0)) + "-normal-*-") + hv_Size_COPY_INP_TMP) + "-*-*-*-*-*-*-*");
                                }
                                // catch (Exception) 
                                catch (HalconException HDevExpDefaultException2)
                                {
                                    HDevExpDefaultException2.ToHTuple(out hv_Exception);
                                    //throw (Exception)
                                }
                            }
                        }
                        //throw (Exception)
                    }
                }
                // dev_set_preferences(...); only in hdevelop
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static void HD_DevOpenWindowFitImage(HObject ho_Image, HTuple hv_Row, HTuple hv_Column, HTuple hv_WidthLimit, HTuple hv_HeightLimit, out HTuple hv_WindowHandle)
        {

            // Local iconic variables 

            // Local control variables 

            HTuple hv_MinWidth = new HTuple(), hv_MaxWidth = new HTuple();
            HTuple hv_MinHeight = new HTuple(), hv_MaxHeight = new HTuple();
            HTuple hv_ResizeFactor = null, hv_ImageWidth = null, hv_ImageHeight = null;
            HTuple hv_TempWidth = null, hv_TempHeight = null, hv_WindowWidth = null;
            HTuple hv_WindowHeight = null;
            // Initialize local and output iconic variables 
            //This procedure opens a new graphics window and adjusts the size
            //such that it fits into the limits specified by WidthLimit
            //and HeightLimit, but also maintains the correct image aspect ratio.
            //
            //If it is impossible to match the minimum and maximum extent requirements
            //at the same time (f.e. if the image is very long but narrow),
            //the maximum value gets a higher priority,
            //
            //Parse input tuple WidthLimit
            if ((int)((new HTuple((new HTuple(hv_WidthLimit.TupleLength())).TupleEqual(0))).TupleOr(new HTuple(hv_WidthLimit.TupleLess(0)))) != 0)
            {
                hv_MinWidth = 500;
                hv_MaxWidth = 800;
            }
            else if ((int)(new HTuple((new HTuple(hv_WidthLimit.TupleLength())).TupleEqual(1))) != 0)
            {
                hv_MinWidth = 0;
                hv_MaxWidth = hv_WidthLimit.Clone();
            }
            else
            {
                hv_MinWidth = hv_WidthLimit[0];
                hv_MaxWidth = hv_WidthLimit[1];
            }
            //Parse input tuple HeightLimit
            if ((int)((new HTuple((new HTuple(hv_HeightLimit.TupleLength())).TupleEqual(0))).TupleOr(new HTuple(hv_HeightLimit.TupleLess(0)))) != 0)
            {
                hv_MinHeight = 400;
                hv_MaxHeight = 600;
            }
            else if ((int)(new HTuple((new HTuple(hv_HeightLimit.TupleLength())).TupleEqual(1))) != 0)
            {
                hv_MinHeight = 0;
                hv_MaxHeight = hv_HeightLimit.Clone();
            }
            else
            {
                hv_MinHeight = hv_HeightLimit[0];
                hv_MaxHeight = hv_HeightLimit[1];
            }
            //
            //Test, if window size has to be changed.
            hv_ResizeFactor = 1;
            HOperatorSet.GetImageSize(ho_Image, out hv_ImageWidth, out hv_ImageHeight);
            //First, expand window to the minimum extents (if necessary).
            if ((int)((new HTuple(hv_MinWidth.TupleGreater(hv_ImageWidth))).TupleOr(new HTuple(hv_MinHeight.TupleGreater(hv_ImageHeight)))) != 0)
            {
                hv_ResizeFactor = (((((hv_MinWidth.TupleReal()) / hv_ImageWidth)).TupleConcat((hv_MinHeight.TupleReal()) / hv_ImageHeight))).TupleMax();
            }
            hv_TempWidth = hv_ImageWidth * hv_ResizeFactor;
            hv_TempHeight = hv_ImageHeight * hv_ResizeFactor;
            //Then, shrink window to maximum extents (if necessary).
            if ((int)((new HTuple(hv_MaxWidth.TupleLess(hv_TempWidth))).TupleOr(new HTuple(hv_MaxHeight.TupleLess(hv_TempHeight)))) != 0)
            {
                hv_ResizeFactor = hv_ResizeFactor * ((((((hv_MaxWidth.TupleReal()) / hv_TempWidth)).TupleConcat((hv_MaxHeight.TupleReal()) / hv_TempHeight))).TupleMin());
            }
            hv_WindowWidth = hv_ImageWidth * hv_ResizeFactor;
            hv_WindowHeight = hv_ImageHeight * hv_ResizeFactor;
            //Resize window
            HOperatorSet.SetWindowAttr("background_color", "black");
            HOperatorSet.OpenWindow(hv_Row, hv_Column, hv_WindowWidth, hv_WindowHeight, 0, "", "", out hv_WindowHandle);
            HDevWindowStack.Push(hv_WindowHandle);
            if (HDevWindowStack.IsOpen())
            {
                HOperatorSet.SetPart(HDevWindowStack.GetActive(), 0, 0, hv_ImageHeight - 1, hv_ImageWidth - 1);
            }

            return;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static void HD_DispContinueMessage(HTuple hv_WindowHandle, HTuple hv_Color, HTuple hv_Box)
        {
            // Local iconic variables 

            // Local control variables 

            HTuple hv_ContinueMessage = null, hv_Row = null;
            HTuple hv_Column = null, hv_Width = null, hv_Height = null;
            HTuple hv_Ascent = null, hv_Descent = null, hv_TextWidth = null;
            HTuple hv_TextHeight = null;
            // Initialize local and output iconic variables 
            //This procedure displays 'Press Run (F5) to continue' in the
            //lower right corner of the screen.
            //It uses the procedure disp_message.
            //
            //Input parameters:
            //WindowHandle: The window, where the text shall be displayed
            //Color: defines the text color.
            //   If set to '' or 'auto', the currently set color is used.
            //Box: If set to 'true', the text is displayed in a box.
            //
            hv_ContinueMessage = "Press Run (F5) to continue";
            HOperatorSet.GetWindowExtents(hv_WindowHandle, out hv_Row, out hv_Column, out hv_Width, out hv_Height);
            HOperatorSet.GetStringExtents(hv_WindowHandle, (" " + hv_ContinueMessage) + " ", out hv_Ascent, out hv_Descent, out hv_TextWidth, out hv_TextHeight);
            HD_DisplayMessage(hv_WindowHandle, hv_ContinueMessage, "image", (hv_Height - hv_TextHeight) - 12, (hv_Width - hv_TextWidth) - 12, hv_Color, hv_Box);

            return;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static void HD_DisplayShapeMatchingResults(HTuple WindowHandle, HTuple ModelID, HTuple Color, HTuple Row, HTuple Column, HTuple Angle, HTuple ScaleR, HTuple ScaleC, HTuple Model)
        {
            // Local iconic variables 
            HObject ModelContours, ContoursAffinTrans;
            // Local control variables 
            HTuple NumMatches, Index, Match, HomMat2DIdentity;
            HTuple HomMat2DScale, HomMat2DRotate, HomMat2DTranslate;

            //This procedure displays the results of Shape-Based Matching.
            //
            try
            {
                NumMatches = Row.Length;
                if (0 != NumMatches)
                {
                    if (0 != ScaleR.Length)
                    {
                        HOperatorSet.TupleGenConst(NumMatches, ScaleR, out ScaleR);
                    }
                    if (0 != ScaleC.Length)
                    {
                        HOperatorSet.TupleGenConst(NumMatches, ScaleC, out ScaleC);
                    }
                    if (0 != Model.Length)
                    {
                        HOperatorSet.TupleGenConst(NumMatches, 0, out Model);
                    }
                    else if (0 != Model.Length)
                    {
                        HOperatorSet.TupleGenConst(NumMatches, Model, out Model);
                    }

                    for (Index = 0; Index <= (ModelID.Length) - 1; Index += 1)
                    {
                        HOperatorSet.GetShapeModelContours(out ModelContours, ModelID, 1);
                        HOperatorSet.SetColor(WindowHandle, Color[Index % (Color.Length)]);

                        for (Match = 0; Match <= NumMatches - 1; Match += 1)
                        {
                            HOperatorSet.HomMat2dIdentity(out HomMat2DIdentity);
                            HOperatorSet.HomMat2dScale(HomMat2DIdentity, ScaleR[Match], ScaleC[Match], 0, 0, out HomMat2DScale);
                            HOperatorSet.HomMat2dRotate(HomMat2DScale, Angle[Match], 0, 0, out HomMat2DRotate);
                            HOperatorSet.HomMat2dTranslate(HomMat2DRotate, Row[Match], Column[Match], out HomMat2DTranslate);
                            HOperatorSet.AffineTransContourXld(ModelContours, out ContoursAffinTrans, HomMat2DTranslate);

                            HOperatorSet.DispObj(ContoursAffinTrans, WindowHandle);
                        }
                    }
                }
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool IsImageObj(HObject Image)
        {
            try
            {
                HTuple szClass;
                HTuple nImageWidth = 0, nImageHeight = 0;

                if (Image == null)
                    return false;

                HOperatorSet.GetObjClass(Image, out szClass);
                if (szClass != "image") //  && strstr(szClass, "region") == null
                    return false;

                HOperatorSet.GetImageSize(Image, out nImageWidth, out nImageHeight);
                if (nImageWidth <= 0 || nImageHeight <= 0)
                    return false;

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool RIO_To_Image(HObject imageSrc, out HObject imageRIO, HTuple saveFileName, HTuple fRow, HTuple fCol, HTuple fPhi, HTuple fLen1, HTuple fLen2, bool rotateImage = true)
        {
            HTuple nMin = 10, nMax = 99999;

            HTuple dThreshold_Min = 0;
            HTuple dThreshold_Max = 128;

            try
            {
                imageRIO = null;
                if (!IsImageObj(imageSrc))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                if (fLen1 <= 0.001 || fLen2 <= 0.111)
                    throw new HalconException("fLen1, fLen2 输入图像保存范围太小，可能有误，请重新选择!");

                HObject rect, rect2, ImageReduced, RioImage;
                if (!rotateImage)
                {
                    HOperatorSet.GenRectangle2(out rect, fRow, fCol, fPhi, fLen1, fLen2);
                    HOperatorSet.ReduceDomain(imageSrc, rect, out ImageReduced);
                    HOperatorSet.CropDomain(ImageReduced, out RioImage);
                }
                else
                {
                    HObject ImageAffinTrans = new HObject();
                    double RIOAngle = fPhi / PI * 180;
                    HTuple HomMat2DIdentity, HomMat2DRotate, HomMat2DScale;
                    HOperatorSet.HomMat2dIdentity(out HomMat2DIdentity);
                    if(fRow.Length > 1)
                    {
                        HOperatorSet.GenRectangle2(out rect, fRow[0], fCol[0], fPhi[0], fLen1[0], fLen2[0]);
                        HOperatorSet.GenRectangle2(out rect2, fRow[1], fCol[1], fPhi[1], fLen1[1], fLen2[1]);
                        HObject regionUnion;
                        HOperatorSet.Union2(rect, rect2, out regionUnion);
                        HOperatorSet.ReduceDomain(imageSrc, regionUnion, out ImageReduced);
                        HOperatorSet.CropDomain(ImageReduced, out RioImage);
                    }
                    else
                    {
                        HOperatorSet.HomMat2dRotate(HomMat2DIdentity, 0 - fPhi, fRow, fCol, out HomMat2DRotate);
                        HOperatorSet.HomMat2dScale(HomMat2DRotate, 1, 1, fRow, fCol, out HomMat2DScale);
                        HOperatorSet.AffineTransImage(imageSrc, out ImageAffinTrans, HomMat2DScale, "constant", "false");
                        HOperatorSet.GenRectangle2(out rect, fRow, fCol, 0, fLen1, fLen2);
                        HOperatorSet.ReduceDomain(ImageAffinTrans, rect, out ImageReduced);
                        HOperatorSet.CropDomain(ImageReduced, out RioImage);
                    }

                }

                HOperatorSet.WriteImage(RioImage, "bmp", 0, saveFileName);
                return true;

            }

            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_CreateTemplaterFromFile(string szFileName, ref SHAPE_MODE_DES pShapeModeDes, ShapeModeMatchType nModeType)
        {
            try
            {
                if (File.Exists(szFileName))
                {
                    HObject objImage;
                    HOperatorSet.ReadImage(out objImage, szFileName);
                    pShapeModeDes.objImage = objImage;
                    if (ShapeModel_CreateTemplaterFromImage(ref pShapeModeDes, nModeType))
                        return true;
                }
            }

            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_CreateTemplaterFromImage(ref SHAPE_MODE_DES pShapeModeDes, ShapeModeMatchType nModeType)
        {
            try
            {
                // 确认输入文件是否是个图片
                if (!IsImageObj(pShapeModeDes.objImage))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                switch (nModeType)
                {
                    default:
                    case ShapeModeMatchType.ShapeModeType_ShapeBase:           //基础外形匹配 (不可缩放比例）
                        return ShapeModel_Create_ShapeBase(ref pShapeModeDes);

                    case ShapeModeMatchType.ShapeModeType_ShapeBase_XLD:   //基础外形XLD匹配 (不可缩放比例）
                        return ShapeModel_Create_ShapeBase_XLD(ref pShapeModeDes);

                    case ShapeModeMatchType.ShapeModeType_Scaled:                  //基础外形匹配 (可缩放比例）
                        return ShapeModel_Create_Scaled(ref pShapeModeDes);

                    case ShapeModeMatchType.ShapeModeType_Scaled_XLD:          //基础外形XLD匹配 (可缩放比例）
                        return ShapeModel_Create_Scaled_XLD(ref pShapeModeDes);

                    case ShapeModeMatchType.ShapeModeType_AnisoScaled:                     //基础外形匹配 (X, Y 方向可缩放比例）
                        return ShapeModel_Create_AnisoScaled(ref pShapeModeDes);

                    case ShapeModeMatchType.ShapeModeType_AnisoScaled_XLD:     //基础外形XLD匹配 (X, Y 方向可缩放比例）
                        return ShapeModel_Create_AnisoScaled_XLD(ref pShapeModeDes);
                }
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_Create_ShapeBase(ref SHAPE_MODE_DES pShapeModeDes)
        {
            HTuple Exception;
            try
            {
                HTuple ParameterName, ParameterValue;

                HObject Rectangle1, Rectangle2, TemplateBottleRegion;
                HObject MicTemplate, Circle, ImageReduce, ImageReduced;
                HObject ModelImages, ModelRegions, Image;

                // 确认输入文件是否是个图片
                if (!IsImageObj(pShapeModeDes.objImage))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple Image1WidthTemp, Image1HeightTemp;
                HOperatorSet.GetImageSize(pShapeModeDes.objImage, out Image1WidthTemp, out Image1HeightTemp);

                HTuple fRow_1 = -1, fCol_1 = -1, fPhi_1 = -1, fLen1_1 = -1, fLen2_1 = -1;
                HTuple fRow_2 = -1, fCol_2 = -1, fPhi_2 = -1, fLen1_2 = -1, fLen2_2 = -1;

                //创建RIO_1
                if (pShapeModeDes.strRIO1.fRow <= 0.01 ||
                    pShapeModeDes.strRIO1.fCol <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen2 <= 0.01)
                {
                    fRow_1 = (double)Image1HeightTemp / 2;
                    fCol_1 = (double)Image1WidthTemp / 2;
                    fPhi_1 = 0;
                    fLen1_1 = (double)Image1WidthTemp / 2 - 2;
                    fLen2_1 = (double)Image1HeightTemp / 2 - 2;
                }
                else
                {
                    fRow_1 = pShapeModeDes.strRIO1.fRow;
                    fCol_1 = pShapeModeDes.strRIO1.fCol;
                    fPhi_1 = 0;
                    fLen1_1 = pShapeModeDes.strRIO1.fLen1;
                    fLen2_1 = pShapeModeDes.strRIO1.fLen2;
                }
                HOperatorSet.GenRectangle2(out Rectangle1, fRow_1, fCol_1, fPhi_1, fLen1_1, fLen2_1);

                //创建RIO_2
                if (pShapeModeDes.strRIO2.fRow <= 0.01 ||
                    pShapeModeDes.strRIO2.fCol <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen2 <= 0.01)
                {
                    fRow_2 = (double)Image1HeightTemp / 2;
                    fCol_2 = (double)Image1WidthTemp / 2;
                    fPhi_2 = 0;
                    fLen1_2 = (double)Image1WidthTemp / 2 - 2;
                    fLen2_2 = (double)Image1HeightTemp / 2 - 2;
                }
                else
                {
                    fRow_2 = pShapeModeDes.strRIO2.fRow;
                    fCol_2 = pShapeModeDes.strRIO2.fCol;
                    fPhi_2 = 0;
                    fLen1_2 = pShapeModeDes.strRIO2.fLen1;
                    fLen2_2 = pShapeModeDes.strRIO2.fLen2;
                }
                HOperatorSet.GenRectangle2(out Rectangle2, fRow_2, fCol_2, fPhi_2, fLen1_2, fLen2_2);

                HOperatorSet.Union2(Rectangle1, Rectangle2, out TemplateBottleRegion);

                //生成模板
                HOperatorSet.ReduceDomain(pShapeModeDes.objImage, TemplateBottleRegion, out ImageReduce);
                HTuple MinScore = 0.7;
                HTuple NumMatches = 3;
                HTuple MaxOverlap = 0.5;
                HTuple SubPixel = "least_squares";
                HTuple NumLevels = 1;
                HTuple Greediness = 0.3;

                pShapeModeDes.MatchingType = ShapeModeMatchType.ShapeModeType_ShapeBase;
                HTuple shapeModel3DID = null;
                HOperatorSet.CreateShapeModel(ImageReduce, "auto",
                                                                      (new HTuple(pShapeModeDes.fAngleStart_T)).TupleRad(), 
                                                                      (new HTuple(pShapeModeDes.fAngleExtent_T)).TupleRad(),
                                                                      (new HTuple(pShapeModeDes.fAngleStep_T)).TupleRad(),
                                                                      (new HTuple("none")).TupleConcat(new HTuple("no_pregeneration")), //(new HTuple("point_reduction_high")).TupleConcat(new HTuple("no_pregeneration"))
                                                                      "use_polarity",  
                                                                      "auto",
                                                                      "auto", 
                                                                      out shapeModel3DID);
                pShapeModeDes.TupleModelID = shapeModel3DID.Clone();
                shapeModel3DID = null;
                HTuple width, height;
                HOperatorSet.GetImageSize(pShapeModeDes.objImage, out width, out height);
//                 globalWnd.ShowImage(pShapeModeDes.objImage, width, height, 0);
//                 globalWnd.Invoke(new Func<SHAPE_MODE_DES, int> ((SHAPE_MODE_DES objectPara) =>
//                 {
//                     HOperatorSet.ClearWindow(globalWnd.hWindow);
//                     HObject ModelContours;
//                     HOperatorSet.GetShapeModelContours(out ModelContours, objectPara.TupleModelID, 1);
//                     HOperatorSet.SetColor(globalWnd.hWindow, "green"); //yellow
//                     HOperatorSet.DispObj(ModelContours, globalWnd.hWindow);
//                     return 0;
// 
//                 }), pShapeModeDes);

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_Create_ShapeBase_XLD(ref SHAPE_MODE_DES pShapeModeDes)
        {
            HTuple Exception;
            try
            {
                HTuple ParameterName, ParameterValue;

                HObject Rectangle1, Rectangle2, TemplateBottleRegion;
                HObject MicTemplate, Circle, ImageReduce, ImageReduced;
                HObject ModelImages, ModelRegions, Image;
                if (pShapeModeDes == null)
                    return false;

                // 确认输入文件是否是个图片
                if (!IsImageObj(pShapeModeDes.objImage))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple Image1WidthTemp, Image1HeightTemp;
                HOperatorSet.GetImageSize(pShapeModeDes.objImage, out Image1WidthTemp, out Image1HeightTemp);

                //创建RIO_1
                if (pShapeModeDes.strRIO1.fRow <= 0.01 ||
                    pShapeModeDes.strRIO1.fCol <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO1.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO1.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO1.fPhi = 0;
                    pShapeModeDes.strRIO1.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO1.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle1, pShapeModeDes.strRIO1.fRow, pShapeModeDes.strRIO1.fCol, pShapeModeDes.strRIO1.fPhi, pShapeModeDes.strRIO1.fLen1, pShapeModeDes.strRIO1.fLen2);

                //创建RIO_2
                if (pShapeModeDes.strRIO2.fRow <= 0.01 ||
                    pShapeModeDes.strRIO2.fCol <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO2.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO2.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO2.fPhi = 0;
                    pShapeModeDes.strRIO2.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO2.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle2, pShapeModeDes.strRIO2.fRow, pShapeModeDes.strRIO2.fCol, pShapeModeDes.strRIO2.fPhi, pShapeModeDes.strRIO2.fLen1, pShapeModeDes.strRIO2.fLen2);

                HOperatorSet.Union2(Rectangle1, Rectangle2, out TemplateBottleRegion);

                //生成模板
                HOperatorSet.ReduceDomain(pShapeModeDes.objImage, TemplateBottleRegion, out ImageReduce);
                HTuple MinScore = 0.6;
                HTuple NumMatches = 3;
                HTuple MaxOverlap = 0.5;
                HTuple SubPixel = "least_squares";
                HTuple NumLevels = 1;
                HTuple Greediness = 0.3;

                HObject Edges;
                HOperatorSet.EdgesSubPix(pShapeModeDes.objImage, out Edges, "canny", 0.5, 20, 40);

                pShapeModeDes.MatchingType = ShapeModeMatchType.ShapeModeType_ShapeBase_XLD;
                HTuple shapeModel3DID = null;
                HOperatorSet.CreateShapeModelXld(Edges, "auto", (new HTuple(pShapeModeDes.fAngleStart_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleExtent_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleStep_T)).TupleRad(), "auto", "use_polarity", 5, out shapeModel3DID);
                pShapeModeDes.TupleModelID = shapeModel3DID;

//                 globalWnd.Invoke(new Func<SHAPE_MODE_DES, int>((SHAPE_MODE_DES objectPara) =>
//                 {
//                     return 0;
//                 }), pShapeModeDes);

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_Create_Scaled(ref SHAPE_MODE_DES pShapeModeDes)
        {
            HTuple Exception;
            try
            {
                HTuple ParameterName, ParameterValue;

                HObject Rectangle1, Rectangle2, TemplateBottleRegion;
                HObject MicTemplate, Circle, ImageReduce, ImageReduced;
                HObject ModelImages, ModelRegions, Image;
                if (pShapeModeDes == null)
                    return false;

                // 确认输入文件是否是个图片
                if (!IsImageObj(pShapeModeDes.objImage))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple Image1WidthTemp, Image1HeightTemp;
                HOperatorSet.GetImageSize(pShapeModeDes.objImage, out Image1WidthTemp, out Image1HeightTemp);

                //创建RIO_1
                if (pShapeModeDes.strRIO1.fRow <= 0.01 ||
                    pShapeModeDes.strRIO1.fCol <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO1.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO1.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO1.fPhi = 0;
                    pShapeModeDes.strRIO1.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO1.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle1, pShapeModeDes.strRIO1.fRow, pShapeModeDes.strRIO1.fCol, pShapeModeDes.strRIO1.fPhi, pShapeModeDes.strRIO1.fLen1, pShapeModeDes.strRIO1.fLen2);

                //创建RIO_2
                if (pShapeModeDes.strRIO2.fRow <= 0.01 ||
                    pShapeModeDes.strRIO2.fCol <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO2.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO2.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO2.fPhi = 0;
                    pShapeModeDes.strRIO2.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO2.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle2, pShapeModeDes.strRIO2.fRow, pShapeModeDes.strRIO2.fCol, pShapeModeDes.strRIO2.fPhi, pShapeModeDes.strRIO2.fLen1, pShapeModeDes.strRIO2.fLen2);

                HOperatorSet.Union2(Rectangle1, Rectangle2, out TemplateBottleRegion);

                //生成模板
                HOperatorSet.ReduceDomain(pShapeModeDes.objImage, TemplateBottleRegion, out ImageReduce);
                HTuple MinScore = 0.9;
                HTuple NumMatches = 3;
                HTuple MaxOverlap = 0.5;
                HTuple SubPixel = "least_squares";
                HTuple NumLevels = 1;
                HTuple Greediness = 0.3;

                pShapeModeDes.MatchingType = ShapeModeMatchType.ShapeModeType_Scaled;
                HTuple shapeModel3DID = null;
                HOperatorSet.CreateScaledShapeModel(ImageReduce, "auto", (new HTuple(pShapeModeDes.fAngleStart_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleExtent_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleStep_T)).TupleRad(), pShapeModeDes.fScaledMin, pShapeModeDes.fScaledMax, "auto", "auto", "use_polarity", "auto", "auto", out shapeModel3DID);
                pShapeModeDes.TupleModelID = shapeModel3DID;

//                 globalWnd.Invoke(new Func<SHAPE_MODE_DES, int>((SHAPE_MODE_DES objectPara) =>
//                 {
//                     return 0;
//                 }), pShapeModeDes);

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_Create_Scaled_XLD(ref SHAPE_MODE_DES pShapeModeDes)
        {
            HTuple Exception;
            try
            {
                HTuple ParameterName, ParameterValue;

                HObject Rectangle1, Rectangle2, TemplateBottleRegion;
                HObject MicTemplate, Circle, ImageReduce, ImageReduced;
                HObject ModelImages, ModelRegions, Image;
                if (pShapeModeDes == null)
                    return false;

                // 确认输入文件是否是个图片
                if (!IsImageObj(pShapeModeDes.objImage))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple Image1WidthTemp, Image1HeightTemp;
                HOperatorSet.GetImageSize(pShapeModeDes.objImage, out Image1WidthTemp, out Image1HeightTemp);

                //创建RIO_1
                if (pShapeModeDes.strRIO1.fRow <= 0.01 ||
                    pShapeModeDes.strRIO1.fCol <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO1.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO1.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO1.fPhi = 0;
                    pShapeModeDes.strRIO1.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO1.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle1, pShapeModeDes.strRIO1.fRow, pShapeModeDes.strRIO1.fCol, pShapeModeDes.strRIO1.fPhi, pShapeModeDes.strRIO1.fLen1, pShapeModeDes.strRIO1.fLen2);

                //创建RIO_2
                if (pShapeModeDes.strRIO2.fRow <= 0.01 ||
                    pShapeModeDes.strRIO2.fCol <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO2.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO2.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO2.fPhi = 0;
                    pShapeModeDes.strRIO2.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO2.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle2, pShapeModeDes.strRIO2.fRow, pShapeModeDes.strRIO2.fCol, pShapeModeDes.strRIO2.fPhi, pShapeModeDes.strRIO2.fLen1, pShapeModeDes.strRIO2.fLen2);

                HOperatorSet.Union2(Rectangle1, Rectangle2, out TemplateBottleRegion);

                //生成模板

                HOperatorSet.ReduceDomain(pShapeModeDes.objImage, TemplateBottleRegion, out ImageReduce);
                HTuple MinScore = 0.9;
                HTuple NumMatches = 3;
                HTuple MaxOverlap = 0.5;
                HTuple SubPixel = "least_squares";
                HTuple NumLevels = 1;
                HTuple Greediness = 0.3;

                HObject Edges;
                HOperatorSet.EdgesSubPix(pShapeModeDes.objImage, out Edges, "canny", 0.5, 20, 40);
                pShapeModeDes.MatchingType = ShapeModeMatchType.ShapeModeType_Scaled_XLD;
                HTuple shapeModel3DID = null;
                HOperatorSet.CreateScaledShapeModelXld(Edges, "auto", (new HTuple(pShapeModeDes.fAngleStart_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleExtent_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleStep_T)).TupleRad(), pShapeModeDes.fScaledMin, pShapeModeDes.fScaledMax, "auto", "auto", "ignore_global_polarity", 5, out shapeModel3DID);
                pShapeModeDes.TupleModelID = shapeModel3DID;

//                 globalWnd.Invoke(new Func<SHAPE_MODE_DES, int>((SHAPE_MODE_DES objectPara) =>
//                 {
//                     return 0;
//                 }), pShapeModeDes);

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_Create_AnisoScaled(ref SHAPE_MODE_DES pShapeModeDes)
        {
            HTuple Exception;
            try
            {
                HTuple ParameterName, ParameterValue;

                HObject Rectangle1, Rectangle2, TemplateBottleRegion;
                HObject MicTemplate, Circle, ImageReduce, ImageReduced;
                HObject ModelImages, ModelRegions, Image;
                if (pShapeModeDes == null)
                    return false;

                // 确认输入文件是否是个图片
                if (!IsImageObj(pShapeModeDes.objImage))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple Image1WidthTemp, Image1HeightTemp;
                HOperatorSet.GetImageSize(pShapeModeDes.objImage, out Image1WidthTemp, out Image1HeightTemp);

                //创建RIO_1
                if (pShapeModeDes.strRIO1.fRow <= 0.01 ||
                    pShapeModeDes.strRIO1.fCol <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO1.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO1.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO1.fPhi = 0;
                    pShapeModeDes.strRIO1.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO1.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle1, pShapeModeDes.strRIO1.fRow, pShapeModeDes.strRIO1.fCol, pShapeModeDes.strRIO1.fPhi, pShapeModeDes.strRIO1.fLen1, pShapeModeDes.strRIO1.fLen2);

                //创建RIO_2
                if (pShapeModeDes.strRIO2.fRow <= 0.01 ||
                    pShapeModeDes.strRIO2.fCol <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO2.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO2.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO2.fPhi = 0;
                    pShapeModeDes.strRIO2.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO2.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle2, pShapeModeDes.strRIO2.fRow, pShapeModeDes.strRIO2.fCol, pShapeModeDes.strRIO2.fPhi, pShapeModeDes.strRIO2.fLen1, pShapeModeDes.strRIO2.fLen2);

                HOperatorSet.Union2(Rectangle1, Rectangle2, out TemplateBottleRegion);

                //生成模板
                HOperatorSet.ReduceDomain(pShapeModeDes.objImage, TemplateBottleRegion, out ImageReduce);
                HTuple MinScore = 0.9;
                HTuple NumMatches = 3;
                HTuple MaxOverlap = 0.5;
                HTuple SubPixel = "least_squares";
                HTuple NumLevels = 1;
                HTuple Greediness = 0.3;

                pShapeModeDes.MatchingType = ShapeModeMatchType.ShapeModeType_AnisoScaled;
                HTuple shapeModel3DID = null;
                HOperatorSet.CreateAnisoShapeModel(ImageReduce, "auto", (new HTuple(pShapeModeDes.fAngleStart_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleExtent_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleStep_T)).TupleRad(),
                    pShapeModeDes.fScaledMin, pShapeModeDes.fScaledMax, "auto",
                    pShapeModeDes.fScaledMin, pShapeModeDes.fScaledMax, "auto",
                    "none", "use_polarity", "auto", "auto", out shapeModel3DID);
                pShapeModeDes.TupleModelID = shapeModel3DID;

//                 globalWnd.Invoke(new Func<SHAPE_MODE_DES, int>((SHAPE_MODE_DES objectPara) =>
//                 {
//                     return 0;
//                 }), pShapeModeDes);

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_Create_AnisoScaled_XLD(ref SHAPE_MODE_DES pShapeModeDes)
        {
            try
            {
                HObject Rectangle1, Rectangle2, TemplateBottleRegion, ImageReduce;
                if (pShapeModeDes == null)
                    return false;

                // 确认输入文件是否是个图片
                if (!IsImageObj(pShapeModeDes.objImage))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple Image1WidthTemp, Image1HeightTemp;
                HOperatorSet.GetImageSize(pShapeModeDes.objImage, out Image1WidthTemp, out Image1HeightTemp);

                //创建RIO_1
                if (pShapeModeDes.strRIO1.fRow <= 0.01 ||
                    pShapeModeDes.strRIO1.fCol <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO1.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO1.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO1.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO1.fPhi = 0;
                    pShapeModeDes.strRIO1.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO1.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle1, pShapeModeDes.strRIO1.fRow, pShapeModeDes.strRIO1.fCol, pShapeModeDes.strRIO1.fPhi, pShapeModeDes.strRIO1.fLen1, pShapeModeDes.strRIO1.fLen2);

                //创建RIO_2
                if (pShapeModeDes.strRIO2.fRow <= 0.01 ||
                    pShapeModeDes.strRIO2.fCol <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen1 <= 0.01 ||
                    pShapeModeDes.strRIO2.fLen2 <= 0.01)
                {
                    pShapeModeDes.strRIO2.fRow = (double)Image1HeightTemp / 2;
                    pShapeModeDes.strRIO2.fCol = (double)Image1WidthTemp / 2;
                    pShapeModeDes.strRIO2.fPhi = 0;
                    pShapeModeDes.strRIO2.fLen1 = (double)Image1WidthTemp / 2 - 2;
                    pShapeModeDes.strRIO2.fLen2 = (double)Image1HeightTemp / 2 - 2;
                }
                HOperatorSet.GenRectangle2(out Rectangle2, pShapeModeDes.strRIO2.fRow, pShapeModeDes.strRIO2.fCol, pShapeModeDes.strRIO2.fPhi, pShapeModeDes.strRIO2.fLen1, pShapeModeDes.strRIO2.fLen2);

                HOperatorSet.Union2(Rectangle1, Rectangle2, out TemplateBottleRegion);

                //生成模板

                HOperatorSet.ReduceDomain(pShapeModeDes.objImage, TemplateBottleRegion, out ImageReduce);
                HTuple MinScore = 0.9;
                HTuple NumMatches = 3;
                HTuple MaxOverlap = 0.5;
                HTuple SubPixel = "least_squares";
                HTuple NumLevels = 1;
                HTuple Greediness = 0.3;

                HObject Edges;
                HOperatorSet.EdgesSubPix(pShapeModeDes.objImage, out Edges, "canny", 0.5, 20, 40);
                pShapeModeDes.MatchingType = ShapeModeMatchType.ShapeModeType_AnisoScaled_XLD;
                HTuple shapeModel3DID = null;
                HOperatorSet.CreateAnisoShapeModelXld(Edges, "auto", (new HTuple(pShapeModeDes.fAngleStart_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleExtent_T)).TupleRad(), (new HTuple(pShapeModeDes.fAngleStep_T)).TupleRad(),
                    pShapeModeDes.fScaledMin, pShapeModeDes.fScaledMax, "auto",
                    pShapeModeDes.fScaledMin, pShapeModeDes.fScaledMax, "auto",
                    "auto", "ignore_local_polarity", "auto", out shapeModel3DID);
                pShapeModeDes.TupleModelID = shapeModel3DID;

//                 globalWnd.Invoke(new Func<SHAPE_MODE_DES, int>((SHAPE_MODE_DES objectPara) =>
//                 {
//                     return 0;
//                 }), pShapeModeDes);

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_GetMatchingResultFromFile(string szFileName, SHAPE_MODE_DES ShapeModeDes, ref MATCHINGRESULT_DES strMatchingResult, ShapeModeMatchType nModeType)
        {
            try
            {
                if (File.Exists(szFileName))
                {
                    HObject objImage;
                    HOperatorSet.ReadImage(out objImage, szFileName);
                    if (ShapeModel_GetMatchingResultFromImage(objImage, ShapeModeDes, ref strMatchingResult, nModeType))
                        return true;
                }

                return false;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_GetMatching_ShapeBase(HObject image, SHAPE_MODE_DES ShapeModeDes, ref MATCHINGRESULT_DES strMatchingResult)
        {
            HTuple MaxOverlap = 0.1;
            HTuple SubPixel = "least_squares";
            HTuple NumLevels = 5;
            HTuple Greediness = 0.5;

            try
            {
                // 确认输入文件是否是个图片
                if (!IsImageObj(image))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple Channels = 0;
                HTuple TupleRow, TupleCol, TupleAngle, TupleScore, TupleModel;
                HOperatorSet.CountChannels(image, out Channels);
                HOperatorSet.FindShapeModels(image,
                    ShapeModeDes.TupleModelID,
                    (new HTuple(ShapeModeDes.fAngleStart)).TupleRad(),
                    (new HTuple(ShapeModeDes.fAngleExtent)).TupleRad(),
                    (new HTuple(ShapeModeDes.fMinScore)).TupleReal(),
                    (new HTuple(ShapeModeDes.dwNumMatches)).TupleNumber(),
                    ShapeModeDes.fMaxOverlap,
                    "least_squares",
                    ShapeModeDes.dwNumLevels,
                    ShapeModeDes.fGreediness,
                    out TupleRow,
                    out TupleCol,
                    out TupleAngle,
                    out TupleScore,
                    out TupleModel);


                // 确定是否找到匹配的对像
                if (TupleRow.Length <= 0)
                    return false;

                strMatchingResult.TupleRow = TupleRow;
                strMatchingResult.TupleCol = TupleCol;
                strMatchingResult.TupleAngle = TupleAngle;
                strMatchingResult.TupleScore = TupleScore;
                strMatchingResult.TupleModel = TupleModel;

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_GetMatching_Scaled(HObject image, SHAPE_MODE_DES ShapeModeDes, ref MATCHINGRESULT_DES strMatchingResult)
        {
            HTuple MaxOverlap = 0.1;
            HTuple SubPixel = "least_squares";
            HTuple NumLevels = 5;
            HTuple Greediness = 0.5;

            try
            {
                // 确认输入文件是否是个图片
                if (!IsImageObj(image))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple TupleRow, TupleCol, TupleAngle, TupleScore, TupleModel, TupleRScaled, TupleCScaled;
                HOperatorSet.FindScaledShapeModels(image,
                    ShapeModeDes.TupleModelID,
                    (new HTuple(ShapeModeDes.fAngleStart)).TupleRad(),
                    (new HTuple(ShapeModeDes.fAngleExtent)).TupleRad(),
                    ShapeModeDes.fScaledMin,
                    ShapeModeDes.fScaledMax,
                    ShapeModeDes.fMinScore,
                    ShapeModeDes.dwNumMatches,
                    ShapeModeDes.fMaxOverlap,
                    "least_squares",
                    ShapeModeDes.dwNumLevels,
                    ShapeModeDes.fGreediness,
                    out TupleRow,
                    out TupleCol,
                    out TupleAngle,
                    out TupleRScaled,
                    out TupleScore,
                    out TupleModel);

                strMatchingResult.TupleCScaled = strMatchingResult.TupleRScaled;


                // 确定是否找到匹配的对像
                if (TupleRow.Length <= 0)
                    return false;

                strMatchingResult.TupleRow = TupleRow;
                strMatchingResult.TupleCol = TupleCol;
                strMatchingResult.TupleAngle = TupleAngle;
                strMatchingResult.TupleScore = TupleScore;
                strMatchingResult.TupleModel = TupleModel;
                strMatchingResult.TupleRScaled = TupleRScaled;

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_GetMatching_AnisoScaled(HObject image, SHAPE_MODE_DES ShapeModeDes, ref MATCHINGRESULT_DES strMatchingResult)
        {
            try
            {
                // 确认输入文件是否是个图片
                if (!IsImageObj(image))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HTuple TupleRow, TupleCol, TupleAngle, TupleScore, TupleModel, TupleRScaled, TupleCScaled;
                HOperatorSet.FindAnisoShapeModels(image,
                    ShapeModeDes.TupleModelID,
                    (new HTuple(ShapeModeDes.fAngleStart)).TupleRad(),
                    (new HTuple(ShapeModeDes.fAngleExtent)).TupleRad(),
                    ShapeModeDes.fScaledMin,
                    ShapeModeDes.fScaledMax,
                    ShapeModeDes.fScaledMin,
                    ShapeModeDes.fScaledMax,
                    ShapeModeDes.fMinScore,
                    ShapeModeDes.dwNumMatches,
                    ShapeModeDes.fMaxOverlap,
                    "least_squares",
                    ShapeModeDes.dwNumLevels,
                    ShapeModeDes.fGreediness,
                    out TupleRow,
                    out TupleCol,
                    out TupleAngle,
                    out TupleRScaled,
                    out TupleCScaled,
                    out TupleScore,
                    out TupleModel);

                // 确定是否找到匹配的对像
                if (TupleRow.Length <= 0)
                    return false;

                strMatchingResult.TupleRow = TupleRow;
                strMatchingResult.TupleCol = TupleCol;
                strMatchingResult.TupleAngle = TupleAngle;
                strMatchingResult.TupleScore = TupleScore;
                strMatchingResult.TupleModel = TupleModel;
                strMatchingResult.TupleRScaled = TupleRScaled;
                strMatchingResult.TupleCScaled = TupleCScaled;

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool ShapeModel_GetMatchingResultFromImage(HObject image, SHAPE_MODE_DES ShapeModeDes, ref MATCHINGRESULT_DES strMatchingResult, ShapeModeMatchType nModeType = ShapeModeMatchType.ShapeModeType_ShapeBase)
        {
            try
            {
                // 确认输入文件是否是个图片
                if (!IsImageObj(image))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                switch (nModeType)
                {
                    default:
                    case ShapeModeMatchType.ShapeModeType_ShapeBase:
                    case ShapeModeMatchType.ShapeModeType_ShapeBase_XLD:
                        return ShapeModel_GetMatching_ShapeBase(image, ShapeModeDes, ref strMatchingResult);

                    case ShapeModeMatchType.ShapeModeType_Scaled:
                    case ShapeModeMatchType.ShapeModeType_Scaled_XLD:
                        return ShapeModel_GetMatching_Scaled(image, ShapeModeDes, ref strMatchingResult);

                    case ShapeModeMatchType.ShapeModeType_AnisoScaled:
                    case ShapeModeMatchType.ShapeModeType_AnisoScaled_XLD:
                        return ShapeModel_GetMatching_AnisoScaled(image, ShapeModeDes, ref strMatchingResult);

                }

                // 确定是否找到匹配的对像
                if (strMatchingResult.TupleRow.Length <= 0)
                    return false;

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static double CaculateTwoLineAngle(HTuple Line1_Row1, HTuple Line1_Column1, HTuple Line1_Row2, HTuple Line1_Column2, HTuple Line2_Row1, HTuple Line2_Column1, HTuple Line2_Row2, HTuple Line2_Column2)
        {
            try
            {
                HTuple Angle;
                HOperatorSet.AngleLl(Line1_Row1, Line1_Column1, Line1_Row2, Line1_Column2, Line2_Row1, Line2_Column1, Line2_Row2, Line2_Column2, out Angle);
                Angle = Angle * 360 / PI2;

                return Angle;

            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static double CaculateOneLineAngle(double Row1, double Column1, double Row2, double Column2)
        {
            try
            {
                HTuple Angle;
                HOperatorSet.AngleLx(Row1, Column1, Row2, Column2, out Angle);
                Angle = Angle * 360 / PI2;
                return Angle;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static Int32 FindCircleFromImage(HObject ImageFind, RIO_DES RIOToFind, double fMinGrayThreshold, double fMaxGrayThreshold,
                                                                        ref CIRCLE_DES[] ArryCircle, HTuple HalconWnd,
                                                                        double fRadiuxMin, double fRadiuxMax,
                                                                        double fSmoothAlpha = 5, double fGrayLow = 20, double fGrayHigh = 40,
                                                                        bool bShowToWnd = false)
        {
            DateTime time1, time2;
            time1 = time2 = DateTime.Now;
            try
            {
                // 确认输入文件是否是个图片
                if (!IsImageObj(ImageFind))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                if (bShowToWnd && HalconWnd != 0)
                {
                    HOperatorSet.SetColored(HalconWnd, 12);
                    HOperatorSet.SetDraw(HalconWnd, "margin"); //margin
                }

                //生成RIO
                HObject RIO_Rectangle;
                HObject BinImage;
                HTuple dwWidth, dwHeight;
                HObject binThresholdRegion, ImageReduce;
                HTuple nNumber = 0;
                HOperatorSet.GetImageSize(ImageFind, out dwWidth, out dwHeight);
                if (RIOToFind.fRow <= 0 && RIOToFind.fCol <= 0)
                {
                    HOperatorSet.GenRectangle2(out RIO_Rectangle, dwHeight / 2, dwWidth / 2, 0, dwWidth / 2, dwHeight / 2);
                }
                else
                {
                    HOperatorSet.GenRectangle2(out RIO_Rectangle, RIOToFind.fRow, RIOToFind.fCol, RIOToFind.fPhi, RIOToFind.fLen1, RIOToFind.fLen2);
                }

                if (HalconWnd != null && bShowToWnd)
                    HOperatorSet.DispObj(RIO_Rectangle, HalconWnd);

                // 如果二值化有定义进行二值化操作
                if (fMinGrayThreshold >= 0 && fMaxGrayThreshold > 0)
                {
                    HOperatorSet.FastThreshold(ImageFind, out binThresholdRegion, fMinGrayThreshold, fMaxGrayThreshold, 5);
                    HOperatorSet.RegionToBin(binThresholdRegion, out BinImage, 0, 255, dwWidth, dwHeight);
                    HOperatorSet.ReduceDomain(BinImage, RIO_Rectangle, out ImageReduce);
                }
                else
                {
                    HOperatorSet.ReduceDomain(ImageFind, RIO_Rectangle, out ImageReduce);
                }

                //边缘分割并找圆
                HObject Edges, ContoursSplit, SelectedContours, Circles;
                HOperatorSet.EdgesColorSubPix(ImageReduce, out Edges, "canny", fSmoothAlpha, fGrayLow, fGrayHigh);
                HOperatorSet.SegmentContoursXld(Edges, out ContoursSplit, "lines_circles", 5, 4, 2); //fRadiuxMin fRadiuxMax
                HOperatorSet.SelectShapeXld(ContoursSplit, out SelectedContours, (new HTuple("area_points")).TupleConcat("outer_radius"), "and", new HTuple(50).TupleConcat(10), new HTuple(99999999).TupleConcat(9999999));  //fRadiuxMax + 800
                HOperatorSet.CountObj(SelectedContours, out nNumber);
                HOperatorSet.GenEmptyObj(out Circles);
                for (int nIndex = 1; nIndex <= nNumber; nIndex++)
                {
                    HObject ObjectSelected;
                    HOperatorSet.SelectObj(SelectedContours, out ObjectSelected, nIndex);
                    HTuple Attrib;
                    HOperatorSet.GetContourGlobalAttribXld(ObjectSelected, "cont_approx", out Attrib);
                    if (Attrib == 1)
                        HOperatorSet.ConcatObj(Circles, ObjectSelected, out Circles);
                }

                HOperatorSet.CountObj(Circles, out nNumber);
                if (nNumber == 0)
                    return 0;

                //合并相同圆周的圆
                HObject UnionContours;
                HOperatorSet.UnionCocircularContoursXld(Circles, out UnionContours, 0.5, 0.2, 0.8, 300, 100, 100, "true", 1);
                HOperatorSet.CountObj(UnionContours, out nNumber);

                //显示图片
                int nNumberOfCircle = 0;
                if (bShowToWnd)
                {
                    HOperatorSet.ClearWindow(HalconWnd);
                    HOperatorSet.DispObj(ImageFind, HalconWnd);

                    HOperatorSet.SetColor(HalconWnd, "magenta");
                    HOperatorSet.SetDraw(HalconWnd, "margin");
                    HOperatorSet.DispObj(RIO_Rectangle, HalconWnd);

                    HOperatorSet.SetColor(HalconWnd, "red");
                    HOperatorSet.SetDraw(HalconWnd, "fill");
                    HOperatorSet.DispObj(Edges, HalconWnd);
                }

                if (ArryCircle == null)
                {
                    ArryCircle = new CIRCLE_DES[0];
                }
                else
                {
                    //                     List<CIRCLE_DES> listCircle = ArryCircle.ToList<CIRCLE_DES>();
                    //                     listCircle.RemoveAll<CIRCLE_DES>();
                }

                int nAuctureNumer = 0;

                //对找到圆进行拟合
                HTuple[] fRadiuxScore = new HTuple[nNumber];
                HTuple RadiuxCenter = (fRadiuxMin + fRadiuxMax) / 2;
                for (int nIndex = 1; nIndex <= nNumber; nIndex++)
                {
                    HObject selectObj;
                    HOperatorSet.SelectObj(UnionContours, out selectObj, nIndex);

                    HTuple l_row, l_col, l_radius, startPhi, endPhi, pointOrder;
                    HOperatorSet.FitCircleContourXld(selectObj, "algebraic", -1, 0, 0, 3, 2, out l_row, out l_col, out l_radius, out startPhi, out endPhi, out pointOrder);
                    HObject ContCircle;
                    ArryCircle[nIndex - 1] = new CIRCLE_DES();
                    HOperatorSet.GenCircleContourXld(out ContCircle, ArryCircle[nIndex - 1].fRow, ArryCircle[nIndex - 1].fCol, ArryCircle[nIndex - 1].fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                    if (l_radius >= fRadiuxMin && l_radius <= fRadiuxMax)
                    {
                        CIRCLE_DES[] tempCircle = new CIRCLE_DES[1];
                        tempCircle[0] = new CIRCLE_DES();

                        tempCircle[0].fRow = l_row;
                        tempCircle[0].fCol = l_col;
                        tempCircle[0].fRadius = l_radius;
                        fRadiuxScore[nAuctureNumer] = System.Math.Abs(ArryCircle[nAuctureNumer].fRadius - RadiuxCenter);

                        ArryCircle = ArryCircle.Concat(tempCircle).ToArray();
                        nAuctureNumer++;

                        if (bShowToWnd)
                        {
                            HOperatorSet.SetColor(HalconWnd, "blue");
                            HOperatorSet.SetDraw(HalconWnd, "margin");
                            HOperatorSet.DispObj(ContCircle, HalconWnd);
                        }
                    }
                }

                //对所有拟合的圆进行排序，半径越靠近上下限中间值排在最前
                double dMinScore = fRadiuxScore[0];
                for (int nIndex = 0; nIndex <= ArryCircle.Length; nIndex++)
                {
                    double ScoreIndex = System.Math.Abs(ArryCircle[nIndex].fRadius - RadiuxCenter);
                    dMinScore = ScoreIndex;
                    for (int nIndexNext = nIndex + 1; nIndexNext < nAuctureNumer; nIndexNext++)
                    {
                        double ScoreNext = System.Math.Abs(ArryCircle[nIndexNext].fRadius - RadiuxCenter);

                        if (ScoreNext < dMinScore)
                        {
                            double TempRow = ArryCircle[nIndex].fRow;
                            double TempCol = ArryCircle[nIndex].fCol;
                            double TempRadiux = ArryCircle[nIndex].fRadius;

                            ArryCircle[nIndex].fRow = ArryCircle[nIndexNext].fRow;
                            ArryCircle[nIndex].fCol = ArryCircle[nIndexNext].fCol;
                            ArryCircle[nIndex].fRadius = ArryCircle[nIndexNext].fRadius;

                            ArryCircle[nIndexNext].fRow = TempRow;
                            ArryCircle[nIndexNext].fCol = TempCol;
                            ArryCircle[nIndexNext].fRadius = TempRadiux;

                            dMinScore = ScoreNext;
                        }
                    }
                }

                //显示图片
                if (HalconWnd != null && bShowToWnd)
                {
                    time2 = DateTime.Now;
                    HD_SetDisplayFont(HalconWnd, 10, "Arial", "true", "false");
                    HD_DisplayMessage(HalconWnd, $"Time: {(time2 - time1).TotalMilliseconds}(ms)", "image", 5, 5, "blue", "false");
                    HD_DisplayMessage(HalconWnd, $"Total circle count: {nAuctureNumer}", "image", 15, 5, "blue", "false");
                }

                return nAuctureNumer;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static Int32 Mark_FindCircleFromImageTemplate(HObject ImageFind, HObject ImageTemplate, double fMinGrayThreshold, double fMaxGrayThreshold,
                                                                                                    CIRCLE_DES[] ArryCircle, HTuple HalconWnd,
                                                                                                    ref RIO_DES pRIOToFind,
                                                                                                    double fRadiuxMin = 0.00, double fRadiuxMax = 99999,
                                                                                                    double fSmoothAlpha = 5, double fGrayLow = 20, double fGrayHigh = 40,
                                                                                                    bool bShowToWnd = false)
        {
            SHAPE_MODE_DES ShapeModeDes = new SHAPE_MODE_DES();

            try
            {
                // 确认输入文件是否是个图片             
                if (!IsImageObj(ImageFind) ||
                    !IsImageObj(ImageTemplate))
                    throw new HalconException("imageSrc 对像输出不包含图像文件");

                HObject objImage;
                HOperatorSet.CopyObj(ImageTemplate, out objImage, 1, 1);
                ShapeModeDes.objImage = objImage;
                ShapeModel_CreateTemplaterFromImage(ref ShapeModeDes, ShapeModeMatchType.ShapeModeType_ShapeBase);

                HTuple dwWidth = 0, dwHeight = 0;
                HOperatorSet.GetImageSize(ImageTemplate, out dwWidth, out dwHeight);
                ShapeModeDes.strRIO1.fLen1 = dwWidth / 2;
                ShapeModeDes.strRIO1.fLen2 = dwHeight / 2;

                MATCHINGRESULT_DES strMatchingResult = new MATCHINGRESULT_DES();
                ShapeModeDes.dwNumMatches = 9999;

                RIO_DES RIOToFind = new RIO_DES();
                RIOToFind.InitRIO();
                return Mark_FindCircleFromShapeMode(ImageFind, ShapeModeDes, fMinGrayThreshold, fMaxGrayThreshold, ref ArryCircle, HalconWnd,
                                                                                ref pRIOToFind,
                                                                                fRadiuxMin, fRadiuxMax,
                                                                                fSmoothAlpha, fGrayLow, fGrayHigh,
                                                                                bShowToWnd);
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static Int32 Mark_FindCircleFromShapeMode(HObject ImageFind, SHAPE_MODE_DES ShapeModeDes, double fMinGrayThreshold,
                                                                                            double fMaxGrayThreshold, ref CIRCLE_DES[] ArryCircle,
                                                                                            HTuple HalconWnd,
                                                                                            ref RIO_DES pRIOToFind,
                                                                                            double fRadiuxMin = 0.000, double fRadiuxMax = 99999,
                                                                                            double fSmoothAlpha = 5, double fGrayLow = 20, double fGrayHigh = 40,
                                                                                            bool bShowToWnd = false)
        {
            int nNumber = 0;
            SHAPE_MODE_DES ShapeModDes = new SHAPE_MODE_DES();

            try
            {
                // 确认输入文件是否是个图片
                if (!IsImageObj(ImageFind))
                    return 0;

                MATCHINGRESULT_DES strMatchingResult = new MATCHINGRESULT_DES();
                ShapeModeDes.dwNumMatches = 9999;

                RIO_DES RIOToFind = new RIO_DES();

                if (ShapeModel_GetMatchingResultFromImage(ImageFind, ShapeModeDes, ref strMatchingResult, ShapeModeDes.MatchingType))
                {
                    RIOToFind.fRow = strMatchingResult.TupleRow;
                    RIOToFind.fCol = strMatchingResult.TupleCol;
                    RIOToFind.fPhi = strMatchingResult.TupleAngle;

                    RIOToFind.fLen1 = ShapeModeDes.strRIO1.fLen1;
                    RIOToFind.fLen2 = ShapeModeDes.strRIO1.fLen2;

                    RIOToFind.fLen1 += 5;
                    RIOToFind.fLen2 += 5;
                    pRIOToFind = RIOToFind;

                    nNumber = FindCircleFromImage(ImageFind, RIOToFind, fMinGrayThreshold, fMaxGrayThreshold, ref ArryCircle, HalconWnd,
                        fRadiuxMin, fRadiuxMax,
                        fSmoothAlpha, fGrayLow, fGrayHigh,
                        bShowToWnd);
                }

                return nNumber;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool Mark_CombineMarkPicture(out HObject imageCombine, HObject imageMark1, HObject imageMark2, HTuple dwDistanceWidth, HTuple dwDistanceHeight)
        {
            HTuple nImageWidth1 = 0, nImageHeight1 = 0;
            HTuple nImageWidth2 = 0, nImageHeight2 = 0;

            byte[] pRedPoint = null;
            byte[] pGreenPoint = null;
            byte[] pBluePoint = null;
            byte[] pGrayPoint = null;

            imageCombine = null;
            int nScaleOfCombine = 1;

            if (imageCombine == null)
                return false;

            try
            {
                HOperatorSet.GetImageSize(imageMark1, out nImageWidth1, out nImageHeight1);
                HOperatorSet.GetImageSize(imageMark2, out nImageWidth2, out nImageHeight2);

                //计算总图像的尺寸
                HTuple nImageWidthCombine = dwDistanceWidth + nImageWidth2 / 2 + nImageWidth1 / 2;
                HTuple nImageHeightCombine = dwDistanceHeight + nImageHeight2 / 2 + nImageHeight1 / 2;

                nImageWidthCombine = System.Math.Max(nImageWidthCombine, System.Math.Max(nImageWidth1, nImageWidth2));
                nImageHeightCombine = System.Math.Max(nImageHeightCombine, System.Math.Max(nImageHeight1, nImageHeight2));

                //由于系统的内存分配可能不足，需要重新定图片的大小，算出缩放的比例
                nScaleOfCombine = 1;
                if (nImageWidthCombine * nImageHeightCombine > 100000000)
                {
                    nImageWidthCombine /= 10;
                    nImageWidthCombine /= 10;
                    nScaleOfCombine = 10;
                }
                else if (nImageWidthCombine * nImageHeightCombine > 50000000)
                {
                    nImageWidthCombine /= 5;
                    nImageWidthCombine /= 5;
                    nScaleOfCombine = 5;
                }
                else
                {
                    nScaleOfCombine = 1;
                }

                //根据缩放比例缩小图片
                HObject imageMark1_Zoom;
                HObject imageMark2_Zoom;
                double dZoomScale = (double)1 / nScaleOfCombine;
                HOperatorSet.ZoomImageFactor(imageMark1, out imageMark1_Zoom, dZoomScale, dZoomScale, "constant");
                HOperatorSet.ZoomImageFactor(imageMark2, out imageMark2_Zoom, dZoomScale, dZoomScale, "constant");

                HOperatorSet.GetImageSize(imageMark1_Zoom, out nImageWidth1, out nImageHeight1);
                HOperatorSet.GetImageSize(imageMark2_Zoom, out nImageWidth2, out nImageHeight2);

                nImageWidthCombine = (long)(dwDistanceWidth / nScaleOfCombine) + nImageWidth2 / 2 + nImageWidth1 / 2;
                nImageHeightCombine = (long)(dwDistanceHeight / nScaleOfCombine) + nImageHeight2 / 2 + nImageHeight1 / 2;

                //生成相应的图片
                HTuple nNumberOfChannel = 0;
                HOperatorSet.CountChannels(imageMark1, out nNumberOfChannel);

                HTuple dwImageSize = nImageWidthCombine * nImageHeightCombine * sizeof(byte);
                if (nNumberOfChannel == 3)
                {
                    // 生成拼合之后图片
                    pRedPoint = new byte[dwImageSize];
                    pGreenPoint = new byte[dwImageSize];
                    pBluePoint = new byte[dwImageSize];

                    // 将原来的Mark1 及 Mark2 图片分别放在左上角及右下角
                    HTuple PointerRed = null;
                    HTuple PointerGreen = null;
                    HTuple PointerBlue = null;
                    HTuple szType;
                    HTuple width, height;

                    // Mark2 图片放在左上角
                    HOperatorSet.GetImagePointer3(imageMark2_Zoom, out PointerRed, out PointerGreen, out PointerBlue, out szType, out width, out height);
                    for (int nIndex = 0; nIndex < height; nIndex++)
                    {
                        //                         memcpy(pRedPoint + nImageWidthCombine * nIndex, PointerRed + width * nIndex, width);
                        //                         memcpy(pGreenPoint + nImageWidthCombine * nIndex, PointerGreen + width * nIndex, width);
                        //                         memcpy(pBluePoint + nImageWidthCombine * nIndex, PointerBlue + width * nIndex, width);
                    }

                    // Mark1 图片放在右下角
                    HOperatorSet.GetImagePointer3(imageMark1_Zoom, out PointerRed, out PointerGreen, out PointerBlue, out szType, out width, out height);
                    long startRow = nImageHeightCombine - nImageHeight1;
                    for (int nIndex = 0; nIndex < height; nIndex++)
                    {
                        //                         memcpy(pRedPoint + startRow * nImageWidthCombine + (nImageWidthCombine - width), PointerRed + width * nIndex, width);
                        //                         memcpy(pGreenPoint + startRow * nImageWidthCombine + (nImageWidthCombine - width), PointerGreen + width * nIndex, width);
                        //                         memcpy(pBluePoint + startRow * nImageWidthCombine + (nImageWidthCombine - width), PointerBlue + width * nIndex, width);
                        //                         startRow++;

                    }

                    //                   HOperatorSet.GenImage3(out imageCombine, "byte", nImageWidthCombine, nImageHeightCombine, pRedPoint, pGreenPoint, pBluePoint);

                    //                     if (pRedPoint != null) { delete[] pRedPoint; pRedPoint = null; }
                    //                     if (pBluePoint != null) { delete[] pBluePoint; pBluePoint = null; }
                    //                     if (pGreenPoint != null) { delete[] pGreenPoint; pGreenPoint = null; }

                    return true;
                }
                else
                {
                    pGrayPoint = new byte[dwImageSize];

                    // 将原来的Mark1 及 Mark2 图片分别放在左上角及右下角
                    HTuple GrayPoint;
                    HTuple szType;
                    HTuple width, height;

                    // Mark2 图片放在右上角
                    HOperatorSet.GetImagePointer1(imageMark2_Zoom, out GrayPoint, out szType, out width, out height);
                    for (int nIndex = 0; nIndex < height; nIndex++)
                    {
                        /*                        memcpy(pGrayPoint + nImageWidthCombine * nIndex, GrayPoint + width * nIndex, width);*/
                    }

                    // Mark1 图片放在左下角
                    HOperatorSet.GetImagePointer1(imageMark1_Zoom, out GrayPoint, out szType, out width, out height);
                    for (int nIndex = 0; nIndex < height; nIndex++)
                    {
                        //                         memcpy(pGrayPoint + (nImageHeightCombine - height + nIndex) * nImageWidthCombine + nImageWidthCombine - width, GrayPoint + width * nIndex, width);
                    }

                    //                  HOperatorSet.GenImage1(imageCombine, "byte", nImageWidthCombine, nImageHeightCombine, pGrayPoint);

                    /*                    if (pGrayPoint != null) { delete[] pGrayPoint; pGrayPoint = null; }*/

                    return true;
                }
            }
            catch (HalconException HDevExpDefaultException)
            {
                //                 if (pRedPoint != null) { delete[] pRedPoint; pRedPoint = null; }
                //                 if (pBluePoint != null) { delete[] pBluePoint; pBluePoint = null; }
                //                 if (pGreenPoint != null) { delete[] pGreenPoint; pGreenPoint = null; }
                //                 if (pGrayPoint != null) { delete[] pGrayPoint; pGrayPoint = null; }

               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool Location_CreateCircleStandard(HObject imageMark, RIO_DES RIODes, double fMinGrayThreshold, double fMaxGrayThreshold, double fRadiuxMin, double fRadiuxMax, ref CIRCLE_DES pCircleDes)
        {
            if (pCircleDes == null)
                return false;

            CIRCLE_DES[] ArryCircle = null;
            int nNumberOfCircle = FindCircleFromImage(imageMark, RIODes, fMinGrayThreshold, fMaxGrayThreshold, ref ArryCircle, null, fRadiuxMin, fRadiuxMax);
            if (nNumberOfCircle == 0)
            {
                return false;
            }

            //            RIO_To_Image(imageMark, out pCircleDes.CircleImage, RIODes.fRow, RIODes.fCol, RIODes.fPhi, RIODes.fLen1, RIODes.fLen2);

            return true;
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool Location_TwoImageDifferenceTemplate(HObject imageMark1, HObject imageMark2, SHAPE_MODE_DES ShapeModeDesMark1, SHAPE_MODE_DES ShapeModeDesMark2,
                                                                                Int32 dwDistanceWidth, Int32 dwDistanceHeight, ref LINE_DES LineTemplate, ref LINE_DES LineImage,
                                                                                ref double fRowDiff, ref double fColDiff, ref double fAngleDiff, ref double fScore, HTuple HalconWnd, bool bShowToWnd)
        {
            DateTime time1, time2;
            time1 = time2 = DateTime.Now;

            try
            {
                // 分别得到两张图片的尺寸
                HTuple Image1WidthTemp, Image1HeightTemp;
                HTuple Image2WidthTemp, Image2HeightTemp;
                HOperatorSet.GetImageSize(ShapeModeDesMark1.objImage, out Image1WidthTemp, out Image1HeightTemp);
                HOperatorSet.GetImageSize(ShapeModeDesMark2.objImage, out Image2WidthTemp, out Image2HeightTemp);

                HTuple Image1Width, Image1Height;
                HTuple Image2Width, Image2Height;
                HOperatorSet.GetImageSize(imageMark1, out Image1Width, out Image1Height);
                HOperatorSet.GetImageSize(imageMark2, out Image2Width, out Image2Height);

                // 计算输入图片Image1, Image2与相对应的Mark1, Mark2模版匹配的结果
                MATCHINGRESULT_DES strMatchingResult1 = new MATCHINGRESULT_DES();
                MATCHINGRESULT_DES strMatchingResult2 = new MATCHINGRESULT_DES();
                if (!ShapeModel_GetMatchingResultFromImage(imageMark1, ShapeModeDesMark1, ref strMatchingResult1) ||
                    !ShapeModel_GetMatchingResultFromImage(imageMark2, ShapeModeDesMark2, ref strMatchingResult2))
                {
                    return false;
                }

                HTuple tupleDistanceWidth = (HTuple)dwDistanceWidth;
                HTuple tupleDistanceHeight = (HTuple)dwDistanceHeight;

                // 基于标准模板两个Mark点对应的Row, Col生成一条线段
                HTuple Line1_Row1 = ShapeModeDesMark2.MatchingResultOrg.TupleRow;
                HTuple Line1_Column1 = ShapeModeDesMark2.MatchingResultOrg.TupleCol;
                HTuple Line1_Row2 = Image2HeightTemp / 2 + Image1HeightTemp / 2 + tupleDistanceHeight - (Image1HeightTemp - ShapeModeDesMark1.MatchingResultOrg.TupleRow);
                HTuple Line1_Column2 = Image2WidthTemp / 2 + Image1WidthTemp / 2 + tupleDistanceWidth - (Image1WidthTemp - ShapeModeDesMark1.MatchingResultOrg.TupleCol);

                // 基于输入图片所找到的两个Mark点对应的Row, Col生成一条线段
                HTuple Line2_Row1 = strMatchingResult2.TupleRow;
                HTuple Line2_Column1 = strMatchingResult2.TupleCol;
                HTuple Line2_Row2 = Image2Height / 2 + Image1Height / 2 + tupleDistanceHeight - (Image1Height - strMatchingResult1.TupleRow);
                HTuple Line2_Column2 = Image2Width / 2 + Image1Width / 2 + tupleDistanceWidth - (Image1Width - strMatchingResult1.TupleCol);

                HTuple dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2, dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2;

                HOperatorSet.TupleReal(Line1_Row1, out dLine1_Row1);
                HOperatorSet.TupleReal(Line1_Column1, out dLine1_Column1);
                HOperatorSet.TupleReal(Line1_Row2, out dLine1_Row2);
                HOperatorSet.TupleReal(Line1_Column2, out dLine1_Column2);

                HOperatorSet.TupleReal(Line2_Row1, out dLine2_Row1);
                HOperatorSet.TupleReal(Line2_Column1, out dLine2_Column1);
                HOperatorSet.TupleReal(Line2_Row2, out dLine2_Row2);
                HOperatorSet.TupleReal(Line2_Column2, out dLine2_Column2);

                //计算Line2(图片内部虚拟线段) 与 Line1(模板内部虚拟线段)的夹角, 即为旋转角度, 如果为正, 表示
                // Line2 相对于Line1逆时针的角度
                double Angle = CaculateTwoLineAngle(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2,
                    dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);

                HTuple RowCenterTemp, ColCenterTemp, LengthTemp, PhiTemp;
                HTuple RowCenterImage, ColCenterImage, LengthImage, PhiImage;
                HTuple AngleAA;

                //分别计算两个线段的重点(中心点), 通过如下两个函数也能计算出夹角"AngleAA = Angle"
                HOperatorSet.LinePosition(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2, out RowCenterTemp, out ColCenterTemp, out LengthTemp, out PhiTemp);
                HOperatorSet.LinePosition(dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2, out RowCenterImage, out ColCenterImage, out LengthImage, out PhiImage);
                AngleAA = (PhiImage - PhiTemp) * 360 / PI2;

                // 将结果赋值
                fRowDiff = RowCenterImage - RowCenterTemp;
                fColDiff = ColCenterImage - ColCenterTemp;
                fAngleDiff = Angle;

                LineTemplate.fRowStart1 = dLine1_Row1;
                LineTemplate.fColStart1 = dLine1_Column1;
                LineTemplate.fRowEnd1 = dLine1_Row2;
                LineTemplate.fColEnd1 = dLine1_Column2;
                LineTemplate.fLength = LengthTemp;
                LineTemplate.fCenternRow = RowCenterTemp;
                LineTemplate.fCenternCol = ColCenterTemp;
                LineTemplate.fAngle = PhiTemp * 360 / PI2;

                LineImage.fRowStart1 = dLine2_Row1;
                LineImage.fColStart1 = dLine2_Column1;
                LineImage.fRowEnd1 = dLine2_Row2;
                LineImage.fColEnd1 = dLine2_Column2;
                LineImage.fLength = LengthImage;
                LineImage.fCenternRow = RowCenterImage;
                LineImage.fCenternCol = ColCenterImage;
                LineImage.fAngle = PhiImage * 360 / PI2;

                // 如果定义了将结果输出到一个Halcon窗口, 那么将结果输出, 请注意保证窗口够显示所有的字

                if (bShowToWnd)
                {
                    HObject imageCombine;
                    if (!Mark_CombineMarkPicture(out imageCombine, imageMark1, imageMark2, dwDistanceWidth, dwDistanceHeight))
                    {
                        return false;
                    }

                    HOperatorSet.DispObj(imageCombine, HalconWnd);

                    if (true)
                    {
                        HTuple tupleString = "";
                        int nRowStart = 60;

                        //显示两条线的信息
                        HD_SetDisplayFont(HalconWnd, 10, "Arial", "true", "false");
                        tupleString = "Line1_Temp(mark1): " + ((double)ShapeModeDesMark1.MatchingResultOrg.TupleRow).ToString("F3") + " , " + ((double)ShapeModeDesMark1.MatchingResultOrg.TupleCol).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart, 5, "blue", "false");

                        tupleString = "Line1_Temp(mark2): " + ((double)ShapeModeDesMark2.MatchingResultOrg.TupleRow).ToString("F3") + " , " + ((double)ShapeModeDesMark2.MatchingResultOrg.TupleCol).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 15, 5, "blue", "false");

                        tupleString = "Line1_Image(mark1): " + ((double)strMatchingResult1.TupleRow).ToString("F3") + " , " + ((double)strMatchingResult1.TupleCol).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 30, 5, "blue", "false");

                        tupleString = "Line1_Image(mark2): " + ((double)strMatchingResult2.TupleRow).ToString("F3") + " , " + ((double)strMatchingResult2.TupleCol).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 45, 5, "blue", "false");

                        //显示X, Y, 及角度的偏差信息
                        HTuple tupleAngle = Angle;
                        tupleString = "Angle: " + ((double)tupleAngle).ToString("F3") + "(正为逆时针)";
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 60, 5, "blue", "false");

                        tupleString = "Row: " + ((double)(RowCenterImage)).ToString("F3") + " - (" + ((double)(RowCenterTemp)).ToString("F3") + ") = " + ((double)(RowCenterImage - RowCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 75, 5, "blue", "false");

                        tupleString = "Col: " + ((double)(ColCenterImage)).ToString("F3") + " - (" + ((double)(ColCenterTemp)).ToString("F3") + ") = " + ((double)(ColCenterImage - ColCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 90, 5, "blue", "false");

                        HObject CrossTemp, CrossImage;
                        HOperatorSet.GenCrossContourXld(out CrossTemp, RowCenterTemp, ColCenterTemp, 50, 0);
                        HOperatorSet.GenCrossContourXld(out CrossImage, RowCenterImage, ColCenterImage, 50, 0);

                        //画线
                        HObject Line;
                        HOperatorSet.SetColor(HalconWnd, "red");
                        HOperatorSet.GenRegionLine(out Line, dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HD_DisplayShapeMatchingResults(HalconWnd, ShapeModeDesMark1.TupleModelID, "red", dLine1_Row2, dLine1_Column2, 0, 1, 1, 0);
                        HD_DisplayShapeMatchingResults(HalconWnd, ShapeModeDesMark2.TupleModelID, "red", dLine1_Row1, dLine1_Column1, 0, 1, 1, 0);
                        HOperatorSet.DispObj(CrossTemp, HalconWnd);

                        HOperatorSet.SetColor(HalconWnd, "blue");
                        HOperatorSet.GenRegionLine(out Line, dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HD_DisplayShapeMatchingResults(HalconWnd, ShapeModeDesMark1.TupleModelID, "blue", dLine2_Row2, dLine2_Column2, strMatchingResult1.TupleAngle, 1, 1, 0);
                        HD_DisplayShapeMatchingResults(HalconWnd, ShapeModeDesMark2.TupleModelID, "blue", dLine2_Row1, dLine2_Column1, strMatchingResult2.TupleAngle, 1, 1, 0);
                        HOperatorSet.DispObj(CrossImage, HalconWnd);

                        time2 = DateTime.Now;
                        HD_DisplayMessage(HalconWnd, $"Time: {(time2 - time1).TotalMilliseconds}(ms)", "image", nRowStart + 115, 5, "blue", "false");
                    }

                }

                return true;

                //计算模版原始角度
                // 		double NewWidth = dwDistanceWidth +(fColDiff2 - Image2Width) - (fColDiff1 - Image1Width); // 
                // 		double NewHeight = dwDistanceHeight + (fRowDiff2 - Image2Height) - (fRowDiff1 - Image1Height);
                // 
                // 		double NewHypotenuse = sqrt(pow((double)NewWidth, (double)2) + pow((double)NewHeight, (double)2));
                // 		double fNewAngle1 = asin((double)NewWidth / NewHypotenuse) * 360 / (2 * 3.14159);
                // 		double fNewAngle2 = acos((double)NewHeight / NewHypotenuse) * 360 / (2 * 3.14159);
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool Location_TwoImageDifferenceCircleStandard(HObject imageMark1, HObject imageMark2, CIRCLE_DES CircleMark1Standard, CIRCLE_DES CircleMark2Standard,
                                                                                                                double fMinGrayThreshold1, double fMaxGrayThreshold1,
                                                                                                                double fMinGrayThreshold2, double fMaxGrayThreshold2,
                                                                                                                RIO_DES RIOToFind1, RIO_DES RIOToFind2,
                                                                                                                double fMatchScore,
                                                                                                                Int32 dwDistanceWidth, Int32 dwDistanceHeight, ref LINE_DES LineTemplate, ref LINE_DES LineImage,
                                                                                                                ref double fRowDiff, ref double fColDiff, ref double fAngleDiff, ref double fScore, HTuple HalconWnd, bool bShowToWnd)

        {
            HTuple Exception;
            DateTime time1, time2;

            time1 = time2 = DateTime.Now;
            try
            {
                double fRowDiff1, fColDiff1, fAngleDiff1, fScore1;
                double fRowDiff2, fColDiff2, fAngleDiff2, fScore2;

                HTuple Image1Width, Image1Height;
                HTuple Image2Width, Image2Height;
                HOperatorSet.GetImageSize(imageMark1, out Image1Width, out Image1Height);
                HOperatorSet.GetImageSize(imageMark2, out Image2Width, out Image2Height);

                // 计算输入图片Image1, Image2与相对应的Mark1, Mark2模版匹配的结果
                MATCHINGRESULT_DES strMatchingResult1;
                MATCHINGRESULT_DES strMatchingResult2;
                CIRCLE_DES[] ArryCircle1 = null;
                RIO_DES RIO_Return_1 = new RIO_DES(), RIO_Return_2 = new RIO_DES();
                RIO_Return_1.InitRIO();
                RIO_Return_2.InitRIO();

                int nNumberOfCircleMark1 = Mark_FindCircleFromImageTemplate(imageMark1, CircleMark1Standard.CircleImage,
                    fMinGrayThreshold1,
                    fMaxGrayThreshold1,
                    ArryCircle1,
                    0,
                    ref RIO_Return_1,
                    CircleMark1Standard.fRadius * fMatchScore,
                    CircleMark1Standard.fRadius * (2 - fMatchScore));

                CIRCLE_DES[] ArryCircle2 = null;
                int nNumberOfCircleMark2 = Mark_FindCircleFromImageTemplate(imageMark2, CircleMark1Standard.CircleImage,
                    fMinGrayThreshold2,
                    fMaxGrayThreshold2,
                    ArryCircle2,
                    0,
                    ref RIO_Return_2,
                    CircleMark2Standard.fRadius * fMatchScore,
                    CircleMark2Standard.fRadius * (2 - fMatchScore));

                if (nNumberOfCircleMark1 == 0 || nNumberOfCircleMark2 == 0)
                {
                    return false;
                }

                double tupleDistanceWidth = (double)dwDistanceWidth;
                double tupleDistanceHeight = (double)dwDistanceHeight;

                // 基于标准模板两个Mark点对应的Row, Col生成一条线段
                double dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2,
                    dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2;

                dLine1_Row1 = CircleMark2Standard.fRow;
                dLine1_Column1 = CircleMark2Standard.fCol;
                dLine1_Row2 = Image2Height / 2 + Image1Height / 2 + tupleDistanceHeight - (Image1Height - CircleMark1Standard.fRow);
                dLine1_Column2 = Image2Width / 2 + Image1Width / 2 + tupleDistanceWidth - (Image1Width - CircleMark1Standard.fCol);

                // 基于输入图片所找到的两个Mark点对应的Row, Col生成一条线段
                dLine2_Row1 = ArryCircle2[0].fRow;
                dLine2_Column1 = ArryCircle2[0].fCol;
                dLine2_Row2 = Image2Height / 2 + Image1Height / 2 + tupleDistanceHeight - (Image1Height - ArryCircle1[0].fRow);
                dLine2_Column2 = Image2Width / 2 + Image1Width / 2 + tupleDistanceWidth - (Image1Width - ArryCircle1[0].fCol);

                //计算Line2(图片内部虚拟线段) 与 Line1(模板内部虚拟线段)的夹角, 即为旋转角度, 如果为正, 表示
                // Line2 相对于Line1逆时针的角度
                double Angle = CaculateTwoLineAngle(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2,
                    dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);

                HTuple RowCenterTemp, ColCenterTemp, LengthTemp, PhiTemp;
                HTuple RowCenterImage, ColCenterImage, LengthImage, PhiImage;
                HTuple AngleAA;

                //分别计算两个线段的重点(中心点), 通过如下两个函数也能计算出夹角"AngleAA = Angle"
                HOperatorSet.LinePosition(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2, out RowCenterTemp, out ColCenterTemp, out LengthTemp, out PhiTemp);
                HOperatorSet.LinePosition(dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2, out RowCenterImage, out ColCenterImage, out LengthImage, out PhiImage);
                AngleAA = (PhiImage - PhiTemp) * 360 / PI2;

                // 将结果赋值
                fRowDiff = RowCenterImage - RowCenterTemp;
                fColDiff = ColCenterImage - ColCenterTemp;
                fAngleDiff = Angle;

                LineTemplate.fRowStart1 = dLine1_Row1;
                LineTemplate.fColStart1 = dLine1_Column1;
                LineTemplate.fRowEnd1 = dLine1_Row2;
                LineTemplate.fColEnd1 = dLine1_Column2;
                LineTemplate.fLength = LengthTemp;
                LineTemplate.fCenternRow = RowCenterTemp;
                LineTemplate.fCenternCol = ColCenterTemp;
                LineTemplate.fAngle = PhiTemp * 360 / PI2;

                LineImage.fRowStart1 = dLine2_Row1;
                LineImage.fColStart1 = dLine2_Column1;
                LineImage.fRowEnd1 = dLine2_Row2;
                LineImage.fColEnd1 = dLine2_Column2;
                LineImage.fLength = LengthImage;
                LineImage.fCenternRow = RowCenterImage;
                LineImage.fCenternCol = ColCenterImage;
                LineImage.fAngle = PhiImage * 360 / PI2;


                // 如果定义了将结果输出到一个Halcon窗口, 那么将结果输出, 请注意保证窗口够显示所有的字
                if (bShowToWnd)
                {
                    HObject imageCombine;
                    if (!Mark_CombineMarkPicture(out imageCombine, imageMark1, imageMark2, dwDistanceWidth, dwDistanceHeight))
                    {
                        return false;
                    }

                    HOperatorSet.DispObj(imageCombine, HalconWnd);
                    if (true)
                    {
                        HTuple tupleString = "";

                        int nRowStart = 60;

                        //显示两条线的信息
                        HD_SetDisplayFont(HalconWnd, 10, "Arial", "true", "false");  //mono
                        tupleString = "Line1_Temp(mark1): " + ((double)(CircleMark1Standard.fRow)).ToString("F3") + " , " + ((double)(CircleMark1Standard.fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart, 5, "blue", "false");

                        tupleString = "Line1_Temp(mark2): " + ((double)(CircleMark2Standard.fRow)).ToString("F3") + " , " + ((double)(CircleMark2Standard.fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 15, 5, "blue", "false");

                        tupleString = "Line1_Image(mark1): " + ((double)(ArryCircle1[0].fRow)).ToString("F3") + " , " + ((double)(ArryCircle1[0].fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 30, 5, "blue", "false");

                        tupleString = "Line1_Image(mark2): " + ((double)(ArryCircle2[0].fRow)).ToString("F3") + " , " + ((double)(ArryCircle2[0].fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 45, 5, "blue", "false");

                        //显示X, Y, 及角度的偏差信息
                        HTuple tupleAngle = Angle;
                        tupleString = "Angle: " + ((double)tupleAngle).ToString("F3") + "(正为逆时针)";
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 60, 5, "blue", "false");

                        tupleString = "Row: " + ((double)(RowCenterImage)).ToString("F3") + " - (" + ((double)(RowCenterTemp)).ToString("F3") + ") = " + ((double)(RowCenterImage - RowCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 75, 5, "blue", "false");

                        tupleString = "Col: " + ((double)(ColCenterImage)).ToString("F3") + " - (" + ((double)(ColCenterTemp)).ToString("F3") + ") = " + ((double)(ColCenterImage - ColCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 90, 5, "blue", "false");

                        HObject CrossTemp, CrossImage;
                        HOperatorSet.GenCrossContourXld(out CrossTemp, RowCenterTemp, ColCenterTemp, 50, 0);
                        HOperatorSet.GenCrossContourXld(out CrossImage, RowCenterImage, ColCenterImage, 50, 0);

                        //画圆
                        HObject Line;
                        HOperatorSet.SetColor(HalconWnd, "red");
                        HOperatorSet.GenRegionLine(out Line, dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HObject ContCircle1Std, ContCircle2Std;
                        HOperatorSet.GenCircleContourXld(out ContCircle1Std, dLine1_Row2, dLine1_Column2, CircleMark2Standard.fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.GenCircleContourXld(out ContCircle2Std, dLine1_Row1, dLine1_Column1, CircleMark1Standard.fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.DispObj(CrossTemp, HalconWnd);
                        HOperatorSet.DispObj(ContCircle1Std, HalconWnd);
                        HOperatorSet.DispObj(ContCircle2Std, HalconWnd);

                        HOperatorSet.SetColor(HalconWnd, "blue");
                        HOperatorSet.GenRegionLine(out Line, dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HObject ContCircle1Find, ContCircle2Find;
                        HOperatorSet.GenCircleContourXld(out ContCircle1Find, dLine2_Row2, dLine2_Column2, ArryCircle2[0].fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.GenCircleContourXld(out ContCircle2Find, dLine2_Row1, dLine2_Column1, ArryCircle1[0].fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.DispObj(CrossImage, HalconWnd);
                        HOperatorSet.DispObj(ContCircle1Find, HalconWnd);
                        HOperatorSet.DispObj(ContCircle2Find, HalconWnd);

                        time2 = DateTime.Now;
                        HD_DisplayMessage(HalconWnd, new HTuple($"Time:  {(time2 - time1).TotalMilliseconds} ms"), "image", nRowStart + 115, 5, "blue", "false");
                    }

                }

                return true;

                //计算模版原始角度
                // 		double NewWidth = dwDistanceWidth +(fColDiff2 - Image2Width) - (fColDiff1 - Image1Width); // 
                // 		double NewHeight = dwDistanceHeight + (fRowDiff2 - Image2Height) - (fRowDiff1 - Image1Height);
                // 
                // 		double NewHypotenuse = sqrt(pow((double)NewWidth, (double)2) + pow((double)NewHeight, (double)2));
                // 		double fNewAngle1 = asin((double)NewWidth / NewHypotenuse) * 360 / (2 * 3.14159);
                // 		double fNewAngle2 = acos((double)NewHeight / NewHypotenuse) * 360 / (2 * 3.14159);
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool Location_TwoImageDifferenceCircleDirect(HObject imageMark1, HObject imageMark2, CIRCLE_DES CircleMark1Standard, CIRCLE_DES CircleMark2Standard,
                                                                                                        double fMinGrayThreshold1, double fMaxGrayThreshold1,
                                                                                                        double fMinGrayThreshold2, double fMaxGrayThreshold2,
                                                                                                        RIO_DES RIOToFind1, RIO_DES RIOToFind2,
                                                                                                        double fMatchScore,
                                                                                                        Int32 dwDistanceWidth, Int32 dwDistanceHeight, ref LINE_DES LineTemplate, ref LINE_DES LineImage,
                                                                                                        ref double fRowDiff, ref double fColDiff, ref double fAngleDiff, ref double fScore, HTuple HalconWnd, bool bShowToWnd)


        {
            HTuple Exception;
            DateTime time1, time2;

            time1 = time2 = DateTime.Now;
            try
            {
                double fRowDiff1, fColDiff1, fAngleDiff1, fScore1;
                double fRowDiff2, fColDiff2, fAngleDiff2, fScore2;

                HTuple Image1Width, Image1Height;
                HTuple Image2Width, Image2Height;
                HOperatorSet.GetImageSize(imageMark1, out Image1Width, out Image1Height);
                HOperatorSet.GetImageSize(imageMark2, out Image2Width, out Image2Height);

                // 计算输入图片Image1, Image2与相对应的Mark1, Mark2模版匹配的结果
                MATCHINGRESULT_DES strMatchingResult1;
                MATCHINGRESULT_DES strMatchingResult2;
                CIRCLE_DES[] ArryCircle1 = null;
                RIO_DES RIOToFind = new RIO_DES();
                int nNumberOfCircleMark1 = FindCircleFromImage(imageMark1, RIOToFind, fMinGrayThreshold2, fMaxGrayThreshold2, ref ArryCircle1, 0, CircleMark1Standard.fRadius * fMatchScore, CircleMark1Standard.fRadius * (2 - fMatchScore));

                CIRCLE_DES[] ArryCircle2 = null;
                int nNumberOfCircleMark2 = FindCircleFromImage(imageMark2, RIOToFind, fMinGrayThreshold2, fMaxGrayThreshold2, ref ArryCircle2, 0, CircleMark2Standard.fRadius * fMatchScore, CircleMark2Standard.fRadius * (2 - fMatchScore));
                if (nNumberOfCircleMark1 == 0 || nNumberOfCircleMark2 == 0)
                {
                    return false;
                }

                double tupleDistanceWidth = (double)dwDistanceWidth;
                double tupleDistanceHeight = (double)dwDistanceHeight;

                // 基于标准模板两个Mark点对应的Row, Col生成一条线段
                double dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2,
                    dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2;

                dLine1_Row1 = CircleMark2Standard.fRow;
                dLine1_Column1 = CircleMark2Standard.fCol;
                dLine1_Row2 = Image2Height / 2 + Image1Height / 2 + tupleDistanceHeight - (Image1Height - CircleMark1Standard.fRow);
                dLine1_Column2 = Image2Width / 2 + Image1Width / 2 + tupleDistanceWidth - (Image1Width - CircleMark1Standard.fCol);

                // 基于输入图片所找到的两个Mark点对应的Row, Col生成一条线段
                dLine2_Row1 = ArryCircle2[0].fRow;
                dLine2_Column1 = ArryCircle2[0].fCol;
                dLine2_Row2 = Image2Height / 2 + Image1Height / 2 + tupleDistanceHeight - (Image1Height - ArryCircle1[0].fRow);
                dLine2_Column2 = Image2Width / 2 + Image1Width / 2 + tupleDistanceWidth - (Image1Width - ArryCircle1[0].fCol);

                //计算Line2(图片内部虚拟线段) 与 Line1(模板内部虚拟线段)的夹角, 即为旋转角度, 如果为正, 表示
                // Line2 相对于Line1逆时针的角度
                double Angle = CaculateTwoLineAngle(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2,
                    dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);

                HTuple RowCenterTemp, ColCenterTemp, LengthTemp, PhiTemp;
                HTuple RowCenterImage, ColCenterImage, LengthImage, PhiImage;
                HTuple AngleAA;

                //分别计算两个线段的重点(中心点), 通过如下两个函数也能计算出夹角"AngleAA = Angle"
                HOperatorSet.LinePosition(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2, out RowCenterTemp, out ColCenterTemp, out LengthTemp, out PhiTemp);
                HOperatorSet.LinePosition(dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2, out RowCenterImage, out ColCenterImage, out LengthImage, out PhiImage);
                AngleAA = (PhiImage - PhiTemp) * 360 / PI2;

                // 将结果赋值
                fRowDiff = RowCenterImage - RowCenterTemp;
                fColDiff = ColCenterImage - ColCenterTemp;
                fAngleDiff = Angle;

                LineTemplate.fRowStart1 = dLine1_Row1;
                LineTemplate.fColStart1 = dLine1_Column1;
                LineTemplate.fRowEnd1 = dLine1_Row2;
                LineTemplate.fColEnd1 = dLine1_Column2;
                LineTemplate.fLength = LengthTemp;
                LineTemplate.fCenternRow = RowCenterTemp;
                LineTemplate.fCenternCol = ColCenterTemp;
                LineTemplate.fAngle = PhiTemp * 360 / PI2;

                LineImage.fRowStart1 = dLine2_Row1;
                LineImage.fColStart1 = dLine2_Column1;
                LineImage.fRowEnd1 = dLine2_Row2;
                LineImage.fColEnd1 = dLine2_Column2;
                LineImage.fLength = LengthImage;
                LineImage.fCenternRow = RowCenterImage;
                LineImage.fCenternCol = ColCenterImage;
                LineImage.fAngle = PhiImage * 360 / PI2;


                // 如果定义了将结果输出到一个Halcon窗口, 那么将结果输出, 请注意保证窗口够显示所有的字
                if (bShowToWnd)
                {
                    HObject imageCombine;
                    if (!Mark_CombineMarkPicture(out imageCombine, imageMark1, imageMark2, dwDistanceWidth, dwDistanceHeight))
                    {
                        return false;
                    }

                    HOperatorSet.DispObj(imageCombine, HalconWnd);
                    if (true)
                    {
                        HTuple tupleString = "";
                        int nRowStart = 60;

                        //显示两条线的信息
                        HD_SetDisplayFont(HalconWnd, 10, "Arial", "true", "false");  //mono
                        tupleString = "Line1_Temp(mark1): " + ((double)(CircleMark1Standard.fRow)).ToString("F3") + " , " + ((double)(CircleMark1Standard.fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart, 5, "blue", "false");

                        tupleString = "Line1_Temp(mark2): " + ((double)(CircleMark2Standard.fRow)).ToString("F3") + " , " + ((double)(CircleMark2Standard.fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 15, 5, "blue", "false");

                        tupleString = "Line1_Image(mark1): " + ((double)(ArryCircle1[0].fRow)).ToString("F3") + " , " + ((double)(ArryCircle1[0].fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 30, 5, "blue", "false");

                        tupleString = "Line1_Image(mark2): " + ((double)(ArryCircle2[0].fRow)).ToString("F3") + " , " + ((double)(ArryCircle2[0].fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 45, 5, "blue", "false");

                        //显示X, Y, 及角度的偏差信息
                        HTuple tupleAngle = Angle;
                        tupleString = "Angle: " + ((double)tupleAngle).ToString("F3") + "(正为逆时针)";
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 60, 5, "blue", "false");

                        tupleString = "Row: " + ((double)(RowCenterImage)).ToString("F3") + " - (" + ((double)(RowCenterTemp)).ToString("F3") + ") = " + ((double)(RowCenterImage - RowCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 75, 5, "blue", "false");

                        tupleString = "Col: " + ((double)(ColCenterImage)).ToString("F3") + " - (" + ((double)(ColCenterTemp)).ToString("F3") + ") = " + ((double)(ColCenterImage - ColCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 90, 5, "blue", "false");

                        HObject CrossTemp, CrossImage;
                        HOperatorSet.GenCrossContourXld(out CrossTemp, RowCenterTemp, ColCenterTemp, 50, 0);
                        HOperatorSet.GenCrossContourXld(out CrossImage, RowCenterImage, ColCenterImage, 50, 0);

                        //画圆
                        HObject Line;
                        HOperatorSet.SetColor(HalconWnd, "red");
                        HOperatorSet.GenRegionLine(out Line, dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HObject ContCircle1Std, ContCircle2Std;
                        HOperatorSet.GenCircleContourXld(out ContCircle1Std, dLine1_Row2, dLine1_Column2, CircleMark2Standard.fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.GenCircleContourXld(out ContCircle2Std, dLine1_Row1, dLine1_Column1, CircleMark1Standard.fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.DispObj(CrossTemp, HalconWnd);
                        HOperatorSet.DispObj(ContCircle1Std, HalconWnd);
                        HOperatorSet.DispObj(ContCircle2Std, HalconWnd);

                        HOperatorSet.SetColor(HalconWnd, "blue");
                        HOperatorSet.GenRegionLine(out Line, dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HObject ContCircle1Find, ContCircle2Find;
                        HOperatorSet.GenCircleContourXld(out ContCircle1Find, dLine2_Row2, dLine2_Column2, ArryCircle2[0].fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.GenCircleContourXld(out ContCircle2Find, dLine2_Row1, dLine2_Column1, ArryCircle1[0].fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.DispObj(CrossImage, HalconWnd);
                        HOperatorSet.DispObj(ContCircle1Find, HalconWnd);
                        HOperatorSet.DispObj(ContCircle2Find, HalconWnd);

                        time2 = DateTime.Now;
                        HD_DisplayMessage(HalconWnd, new HTuple($"Time:  {(time2 - time1).TotalMilliseconds} ms"), "image", nRowStart + 115, 5, "blue", "false");
                    }

                }

                return true;

                //计算模版原始角度
                // 		double NewWidth = dwDistanceWidth +(fColDiff2 - Image2Width) - (fColDiff1 - Image1Width); // 
                // 		double NewHeight = dwDistanceHeight + (fRowDiff2 - Image2Height) - (fRowDiff1 - Image1Height);
                // 
                // 		double NewHypotenuse = sqrt(pow((double)NewWidth, (double)2) + pow((double)NewHeight, (double)2));
                // 		double fNewAngle1 = asin((double)NewWidth / NewHypotenuse) * 360 / (2 * 3.14159);
                // 		double fNewAngle2 = acos((double)NewHeight / NewHypotenuse) * 360 / (2 * 3.14159);
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //=========================================================================================================================
        //摘要：
        //
        //
        //=========================================================================================================================
        public static bool Location_TwoImageDifferenceCircleImageCenter(HObject imageMark1, HObject imageMark2, double Circle1Radiux, double Circle2Radiux,
                                                                                                                    double fMinGrayThreshold1, double fMaxGrayThreshold1,
                                                                                                                    double fMinGrayThreshold2, double fMaxGrayThreshold2,
                                                                                                                    RIO_DES RIOToFind1, RIO_DES RIOToFind2,
                                                                                                                    double fMatchScore,
                                                                                                                    Int32 dwDistanceWidth, Int32 dwDistanceHeight, ref LINE_DES LineTemplate, ref LINE_DES LineImage,
                                                                                                                    ref double fRowDiff, ref double fColDiff, ref double fAngleDiff, ref double fScore, HTuple HalconWnd, bool bShowToWnd)
        {
            HTuple Exception;
            DateTime time1, time2;

            time1 = time2 = DateTime.Now;
            try
            {
                double fRowDiff1, fColDiff1, fAngleDiff1, fScore1;
                double fRowDiff2, fColDiff2, fAngleDiff2, fScore2;

                HTuple Image1Width, Image1Height;
                HTuple Image2Width, Image2Height;
                HOperatorSet.GetImageSize(imageMark1, out Image1Width, out Image1Height);
                HOperatorSet.GetImageSize(imageMark2, out Image2Width, out Image2Height);

                // 计算输入图片Image1, Image2与相对应的Mark1, Mark2模版匹配的结果
                MATCHINGRESULT_DES strMatchingResult1;
                MATCHINGRESULT_DES strMatchingResult2;
                CIRCLE_DES[] ArryCircle1 = null;
                int nNumberOfCircleMark1 = FindCircleFromImage(imageMark1, RIOToFind1, fMinGrayThreshold1, fMaxGrayThreshold1, ref ArryCircle1, 0, Circle1Radiux * fMatchScore, Circle1Radiux * (2 - fMatchScore));

                CIRCLE_DES[] ArryCircle2 = null;
                int nNumberOfCircleMark2 = FindCircleFromImage(imageMark2, RIOToFind2, fMinGrayThreshold2, fMaxGrayThreshold2, ref ArryCircle2, 0, Circle2Radiux * fMatchScore, Circle2Radiux * (2 - fMatchScore));
                if (nNumberOfCircleMark1 == 0 || nNumberOfCircleMark2 == 0)
                {
                    return false;
                }

                double tupleDistanceWidth = (double)dwDistanceWidth;
                double tupleDistanceHeight = (double)dwDistanceHeight;

                long nImageWidthCombine = (long)dwDistanceWidth + Image2Width / 2 + Image1Width / 2;
                long nImageHeightCombine = (long)dwDistanceHeight + Image2Height / 2 + Image1Height / 2;

                nImageWidthCombine = System.Math.Max(nImageWidthCombine, System.Math.Max(Image1Width, Image2Width));
                nImageHeightCombine = System.Math.Max(nImageHeightCombine, System.Math.Max(Image1Height, Image2Height));

                // 基于标准模板两个Mark点对应的Row, Col生成一条线段
                double dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2,
                    dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2;

                dLine1_Row1 = Image2Height / 2.00;
                dLine1_Column1 = Image2Width / 2.00;
                dLine1_Row2 = Image2Height / 2 + tupleDistanceHeight;
                dLine1_Column2 = Image2Width / 2 + tupleDistanceWidth;

                // 基于输入图片所找到的两个Mark点对应的Row, Col生成一条线段
                dLine2_Row1 = ArryCircle2[0].fRow;
                dLine2_Column1 = ArryCircle2[0].fCol;
                dLine2_Row2 = Image2Height / 2 + Image1Height / 2 + tupleDistanceHeight - (Image1Height - ArryCircle1[0].fRow);
                dLine2_Column2 = Image2Width / 2 + Image1Width / 2 + tupleDistanceWidth - (Image1Width - ArryCircle1[0].fCol);

                //计算Line2(图片内部虚拟线段) 与 Line1(模板内部虚拟线段)的夹角, 即为旋转角度, 如果为正, 表示
                // Line2 相对于Line1逆时针的角度
                double Angle = CaculateTwoLineAngle(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2,
                    dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);

                HTuple RowCenterTemp, ColCenterTemp, LengthTemp, PhiTemp;
                HTuple RowCenterImage, ColCenterImage, LengthImage, PhiImage;
                HTuple AngleAA;

                //分别计算两个线段的重点(中心点), 通过如下两个函数也能计算出夹角"AngleAA = Angle"
                HOperatorSet.LinePosition(dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2, out RowCenterTemp, out ColCenterTemp, out LengthTemp, out PhiTemp);
                HOperatorSet.LinePosition(dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2, out RowCenterImage, out ColCenterImage, out LengthImage, out PhiImage);
                AngleAA = (PhiImage - PhiTemp) * 360 / PI2;

                //计算

                // 将结果赋值
                fRowDiff = RowCenterImage - RowCenterTemp;
                fColDiff = ColCenterImage - ColCenterTemp;
                fAngleDiff = Angle;

                LineTemplate.fRowStart1 = dLine1_Row1;
                LineTemplate.fColStart1 = dLine1_Column1;
                LineTemplate.fRowEnd1 = dLine1_Row2;
                LineTemplate.fColEnd1 = dLine1_Column2;
                LineTemplate.fLength = LengthTemp;
                LineTemplate.fCenternRow = RowCenterTemp;
                LineTemplate.fCenternCol = ColCenterTemp;
                LineTemplate.fAngle = PhiTemp * 360 / PI2;

                LineImage.fRowStart1 = dLine2_Row1;
                LineImage.fColStart1 = dLine2_Column1;
                LineImage.fRowEnd1 = dLine2_Row2;
                LineImage.fColEnd1 = dLine2_Column2;
                LineImage.fLength = LengthImage;
                LineImage.fCenternRow = RowCenterImage;
                LineImage.fCenternCol = ColCenterImage;
                LineImage.fAngle = PhiImage * 360 / PI2;


                // 如果定义了将结果输出到一个Halcon窗口, 那么将结果输出, 请注意保证窗口够显示所有的字
                if (bShowToWnd)
                {
                    HObject imageCombine;
                    if (!Mark_CombineMarkPicture(out imageCombine, imageMark1, imageMark2, dwDistanceWidth, dwDistanceHeight))
                    {
                        return false;
                    }

                    HOperatorSet.DispObj(imageCombine, HalconWnd);
                    if (true)
                    {
                        HTuple tupleString = "";

                        int nRowStart = 60;

                        //显示两条线的信息
                        HD_SetDisplayFont(HalconWnd, 10, "Arial", "true", "false");  //mono
                        tupleString = "Line1_Temp(mark1): " + ((double)(Image1Height / 2.0)).ToString("F3") + " , " + ((double)(Image1Width / 2.0)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart, 5, "blue", "false");

                        tupleString = "Line1_Temp(mark2): " + ((double)(Image2Height / 2.0)).ToString("F3") + " , " + ((double)(Image2Width / 2.0)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 15, 5, "blue", "false");

                        tupleString = "Line1_Image(mark1): " + ((double)(ArryCircle1[0].fRow)).ToString("F3") + " , " + ((double)(ArryCircle1[0].fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 30, 5, "blue", "false");

                        tupleString = "Line1_Image(mark2): " + ((double)(ArryCircle2[0].fRow)).ToString("F3") + " , " + ((double)(ArryCircle2[0].fCol)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 45, 5, "blue", "false");

                        //显示X, Y, 及角度的偏差信息
                        HTuple tupleAngle = Angle;
                        // 				tupleString = "Angle: " + tupleAngle.ToString("F3") + "(正为逆时针)";
                        // 				HD_DisplayMessage (HalconWnd, tupleString, "image", nRowStart + 60, 5, "blue", "false");

                        tupleString = "Angle: " + ((double)(LineImage.fAngle)).ToString("F3") + " - (" + ((double)(LineTemplate.fAngle)).ToString("F3") + ") = " + ((double)(tupleAngle)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 60, 5, "blue", "false");

                        tupleString = "Row: " + ((double)(RowCenterImage)).ToString("F3") + " - (" + ((double)(RowCenterTemp)).ToString("F3") + ") = " + ((double)(RowCenterImage - RowCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 75, 5, "blue", "false");

                        tupleString = "Col: " + ((double)(ColCenterImage)).ToString("F3") + " - (" + ((double)(ColCenterTemp)).ToString("F3") + ") = " + ((double)(ColCenterImage - ColCenterTemp)).ToString("F3");
                        HD_DisplayMessage(HalconWnd, tupleString, "image", nRowStart + 90, 5, "blue", "false");

                        HObject CrossTemp, CrossImage;
                        HOperatorSet.GenCrossContourXld(out CrossTemp, RowCenterTemp, ColCenterTemp, 100, 0);
                        HOperatorSet.GenCrossContourXld(out CrossImage, RowCenterImage, ColCenterImage, 100, 0);

                        //画圆
                        HObject Line;
                        HOperatorSet.SetColor(HalconWnd, "red");
                        HOperatorSet.GenRegionLine(out Line, dLine1_Row1, dLine1_Column1, dLine1_Row2, dLine1_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HObject ContCircle1Std, ContCircle2Std;
                        HOperatorSet.GenCrossContourXld(out ContCircle1Std, dLine1_Row2, dLine1_Column2, 100, 0);
                        HOperatorSet.GenCrossContourXld(out ContCircle2Std, dLine1_Row1, dLine1_Column1, 100, 0);
                        // 				HOperatorSet.GenCircleContourXld (out ContCircle1Std, dLine1_Row2, dLine1_Column2, Circle2Radiux, -3.1415926, 3.1415926, "positive", 10.5);
                        // 				HOperatorSet.GenCircleContourXld (out ContCircle2Std, dLine1_Row1, dLine1_Column1, Circle1Radiux, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.DispObj(CrossTemp, HalconWnd);
                        HOperatorSet.DispObj(ContCircle1Std, HalconWnd);
                        HOperatorSet.DispObj(ContCircle2Std, HalconWnd);

                        HOperatorSet.SetColor(HalconWnd, "blue");
                        HOperatorSet.GenRegionLine(out Line, dLine2_Row1, dLine2_Column1, dLine2_Row2, dLine2_Column2);
                        HOperatorSet.DispObj(Line, HalconWnd);
                        HObject ContCircle1Find, ContCircle2Find;
                        HOperatorSet.GenCircleContourXld(out ContCircle1Find, dLine2_Row2, dLine2_Column2, ArryCircle2[0].fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.GenCircleContourXld(out ContCircle2Find, dLine2_Row1, dLine2_Column1, ArryCircle1[0].fRadius, -3.1415926, 3.1415926, "positive", 10.5);
                        HOperatorSet.DispObj(CrossImage, HalconWnd);
                        HOperatorSet.DispObj(ContCircle1Find, HalconWnd);
                        HOperatorSet.DispObj(ContCircle2Find, HalconWnd);

                        time2 = DateTime.Now;
                        HD_DisplayMessage(HalconWnd, new HTuple($"Time:  {(time2 - time1).TotalMilliseconds} ms"), "image", nRowStart + 115, 5, "blue", "false");
                    }

                }

                return true;

                //计算模版原始角度
                // 		double NewWidth = dwDistanceWidth +(fColDiff2 - Image2Width) - (fColDiff1 - Image1Width); // 
                // 		double NewHeight = dwDistanceHeight + (fRowDiff2 - Image2Height) - (fRowDiff1 - Image1Height);
                // 
                // 		double NewHypotenuse = sqrt(pow((double)NewWidth, (double)2) + pow((double)NewHeight, (double)2));
                // 		double fNewAngle1 = asin((double)NewWidth / NewHypotenuse) * 360 / (2 * 3.14159);
                // 		double fNewAngle2 = acos((double)NewHeight / NewHypotenuse) * 360 / (2 * 3.14159);
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
