﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib.ImageProcess
{
    public class BlobSplitAnalysis : ImageProcessBase
    {

        public double MinGray { get; set; }//灰度值 ---HSV
        public double MaxGray { get; set; }//灰度值 ---HSV

        public double ColInterval { get; set; }//
        public int BlobNumber{ get; set; }
        public BlobSplitAnalysis()
        {
            cName = "Blob间隔分析";

        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            int suc = getTempColor();
            return 0;
        }
        public int getTempColor()
        {
            if (ModelRoi==null || ModelRoi.Count == 0)
            {
                return -1;
            }
            HTuple channels = 0;
            HOperatorSet.CountChannels(TempImage, out channels);
            HObject grayObject = null;
            if (channels == 3)
            {

                HOperatorSet.Rgb1ToGray(TempImage, out grayObject);
            }
            else
            {
                grayObject = TempImage;
            }

            HRegion hReg = ModelRoi[0].getRegion();
            double []Rows=ModelRoi[0].Rows;
            double[] Cols = ModelRoi[0].Cols;
            double MinC = ModelRoi[0].MidC;
            double MidR = ModelRoi[0].MidR;
            
            double RLength = ModelRoi[0].Length2 * 2;
            double CLength = ModelRoi[0].Length1 * 2;
            double Phi = ModelRoi[0].Phi;


            double RLength1 = Rows[3] - Rows[0];
            double CLength1 = Cols[1] - Cols[0];
            int colSpit = 10;
            int rowSpit = 10;
            double RowLength = RLength / rowSpit;
            double ColLength = CLength / colSpit;

            double RowStart = MidR - (RLength) / 2 + RowLength;
            double ColStart = MinC - (CLength) / 2 + ColLength;
            //double RowStart = Rows[0] + RowLength;// MidR - (Length2 - RowLength);
            //double ColStart = Cols[0] + ColLength;// MinC - (Length1 - ColLength);
            HTuple hv_HomMat2D, hRowStart, hColStart;
            HTuple hv_HomMat2DIdentity = new HTuple();
            HOperatorSet.HomMat2dIdentity(out hv_HomMat2DIdentity);
            //HOperatorSet.VectorAngleToRigid(MidR, MinC, 0, MidR, MinC, -Phi, out hv_HomMat2D);
            HOperatorSet.HomMat2dRotate(hv_HomMat2DIdentity, Phi, MinC, MidR, out hv_HomMat2D);
            for (int r = 0; r < 9; r++)
            {
                for (int c = 0; c < 9; c++)
                {
                    HRegion aReg = new HRegion();

                    double nRow=RowStart + RowLength * r;
                    double nCol=ColStart + ColLength * c;
                    HOperatorSet.AffineTransPoint2d(hv_HomMat2D, nCol, nRow, out hColStart, out hRowStart);
                    aReg.GenRectangle2(hRowStart.D, hColStart.D, -Phi, ColLength, RowLength);
                    if (ViewPort != null)
                    {
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                        HOperatorSet.DispRegion(aReg, ViewPort.HalconWindow);
                    }

                    HObject ho_ImageReduced, ThresholdObj, Balls, SingleBalls, IntermediateBalls;
                    HOperatorSet.ReduceDomain(grayObject, aReg, out  ho_ImageReduced);
                    HOperatorSet.Threshold(ho_ImageReduced, out ThresholdObj, MinGray, MaxGray);
                    HOperatorSet.OpeningCircle(ThresholdObj, out  Balls, 5.3);
                    if (ViewPort != null)
                    {
                        //HOperatorSet.cl
                        //HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "blue");
                       // HOperatorSet.DispObj(ThresholdObj, ViewPort.HalconWindow);
                    }
                    HOperatorSet.Connection(Balls, out SingleBalls);
                    HOperatorSet.SelectShape(SingleBalls, out IntermediateBalls, "circularity", "and", 0.35, 1.0);
                    if (ViewPort != null)
                    {
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                        HOperatorSet.DispObj(IntermediateBalls, ViewPort.HalconWindow);
                    }
                }
            }
            double R = hReg.Row.D;

            
            
            //   select_shape (SingleBalls, IntermediateBalls, 'circularity', 'and', 0.85, 1.0);
            return 0;
        }
        public override int ImageProcess()
        {
             if (SrcObject == null)
            {
                return 0;
            }

             if (SearchRoi == null || SearchRoi.Count == 0)
             {
                 return -1;
             }

            DesObject = null ;
            HObject SearchObj = SrcObject;
            HObject ho_ImageReduced,ThresholdObj,Balls;
            HRegion hReg = SearchRoi[0].getRegion();
            double R = hReg.Row.D;

            HOperatorSet.ReduceDomain(SearchObj, hReg, out  ho_ImageReduced);
            HTuple channels = 0;
            HOperatorSet.CountChannels(ho_ImageReduced, out channels);
           
            if (channels == 3)
            {
                HObject gObject=null;
                HOperatorSet.Rgb1ToGray(ho_ImageReduced, out gObject);
                HOperatorSet.Threshold(gObject, out ThresholdObj, MinGray, MaxGray);
            }
            else
            {
                HOperatorSet.Threshold(ho_ImageReduced, out ThresholdObj, MinGray, MaxGray);
            }
            if (ViewPort != null)
            {
                //HOperatorSet.cl
                HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                HOperatorSet.DispObj(ThresholdObj, ViewPort.HalconWindow);
            }
            
            HObject SingleBalls,IntermediateBalls;
            HOperatorSet.OpeningCircle (ThresholdObj,out  Balls, 16.3) ;
            if (ViewPort != null)
            {
                //HOperatorSet.cl
                HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                HOperatorSet.SetColor(ViewPort.HalconWindow, "blue");
                HOperatorSet.DispObj(Balls, ViewPort.HalconWindow);
            }
            HOperatorSet.Connection (Balls, out SingleBalls);
            HOperatorSet.SelectShape(SingleBalls, out IntermediateBalls, "circularity", "and", 0.55, 1.0);
 
            HTuple Row,Column,Radius;
            HOperatorSet.SmallestCircle(IntermediateBalls, out Row, out Column, out Radius);
            HTuple Number=0;
            HOperatorSet.CountObj(IntermediateBalls, out Number);
            Result aR = new Result();
            aR.cX = Number.I;
            if (Number.I >= BlobNumber)
            {
                if (ViewPort != null)
                {
                    HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                    HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                    HOperatorSet.DispObj(IntermediateBalls, ViewPort.HalconWindow);
                }
                aR.isOk = true;
                ResultSta = true;
            }
            else
            {
                if (ViewPort != null)
                {
                    HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                    HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                    HOperatorSet.DispObj(IntermediateBalls, ViewPort.HalconWindow);
                }
                aR.isOk = false;
                ResultSta = false;
            }
            mResult.Add(aR);
            
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            BlobSplitAnalysis aFun = CoverLib.JsonCover.DeStrLSerialize<BlobSplitAnalysis>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<BlobSplitAnalysis>(this);
            return aFunPara;
        }
    }
}
