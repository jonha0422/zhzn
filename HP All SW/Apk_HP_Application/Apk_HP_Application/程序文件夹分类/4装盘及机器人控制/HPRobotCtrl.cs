﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using CommunicationZH;
using HslCommunication;
using HslCommunication.ModBus;
using HslCommunication.Core.Net;
using System.IO.Ports;
using Apk_HP_Application.程序文件夹分类._0全局全量;
using Apk_HP_Application.程序文件夹分类._4装盘及机器人控制;
using Apk_HP_Application.通讯及接口;
using ZHToolBox.HalconVision;
using System.ComponentModel;
using System.Xml.Serialization;
using APK_HP.MES.HPSql;

namespace Apk_HP_Application.程序文件夹分类._4装盘及机器人控制
{
    class PropertyTest
    {
        public string name { get; set; }
        public string age { get; set; }
        public List<HPglobal.MessageOut> message { get; set; }
    }

    class HPRobotCtrl
    {
        [XmlIgnoreAttribute]
        public static frm_RobotControl frm_MainPanel = null;

        [XmlIgnoreAttribute]
        public static PropertyTest objPropertyTest = new PropertyTest();

        [XmlIgnoreAttribute]
        public static bool bQuitHPRobot = false;

        [XmlIgnoreAttribute]
        public static Thread threadIOMonitor = null;

        [XmlIgnoreAttribute]
        public static Thread threadPCRequire = null;

        [XmlIgnoreAttribute]
        public static Thread threadOrderRequire = null;

        [XmlIgnoreAttribute]
        public static bool HardwareInitialized = false;

        [XmlIgnoreAttribute]
        public static EpsonTCPNet ObjRobot = new EpsonTCPNet();

        [XmlIgnoreAttribute]
        public static PLC_Panasonic PLC_Unload = new PLC_Panasonic();

        [XmlIgnoreAttribute]
        public static bool bIsPLCUnloadConnected = false;

        [XmlIgnoreAttribute]
        public static HslCommunication.ModBus.ModbusRtu StepSevo = new ModbusRtu();

        [XmlIgnoreAttribute]
        public static LightSource_FH_DP8 lightSource = new LightSource_FH_DP8();


        [XmlIgnoreAttribute]
        public static HslCommunication.ModBus.ModbusTcpServer modebusTCPServer = new HslCommunication.ModBus.ModbusTcpServer();

        [XmlIgnoreAttribute]
        public static OperateResult<short[]> D1300Values;
        public static bool IsLoadTrayInPos = false;     //产品到位	1300	1表示料盘有料，0表示无料盘
        public static bool IsNeedQA = false;                //是否抽检	1301	1表示要抽检
        public static bool IsBuffTrayInPos = false;        //缓冲盘产品状态	1302	1表示有缓冲盘，0表示无料盘
        public static bool IsLoadTrigger = false;           //组盘触发信号	1303	1触发机器人下料
        public static bool IsUnloadCompleted = false;//组盘完成信号	1304	1触发机器人完成下料(下料盘不一定满)
        public static int nUnloadResult = 0;       //组盘结果信号	1305	1触发机器人完成下料
        public static bool IsUnloadTrayInPos = false;   //下料盘在位信号	1306	1表示料盘有料，0表示无料盘
        public static bool IsUnloadTrayFull = false;    //下料盘已满信号	1307	1表示料盘已满，等待PLC设备取料盘（同时复位等待料盘上料完成信号)
        public static bool IsUnloadTrayLoadCompleted = false; //下料盘上料完成信号	1308	1表示正在上下料盘，2表示上下料盘完成(同时1307为1)
        public static bool IsRoboteSafe = false;            //PLC动作机器人安全信号	1309	1表示机器人在安全位置

        [XmlIgnoreAttribute]
        public static bool[,] BuffTrayIsInPos = null;

        [XmlIgnoreAttribute]
        public static bool[] arryCacthIsEnable = new bool[7] { true, true, true, true, true, true, true};

        [XmlIgnoreAttribute]
        public static Queue<HPglobal.MessageOut> m_queRobotLoadUnload = new Queue<HPglobal.MessageOut>();

        [XmlIgnoreAttribute]
        public static Queue<HPglobal.MessageOut> m_queCamera1 = new Queue<HPglobal.MessageOut>();

        [XmlIgnoreAttribute]
        public static Queue<HPglobal.MessageOut> m_queCamera2 = new Queue<HPglobal.MessageOut>();

        [XmlIgnoreAttribute]
        public static HalconForm Camera1 = new HalconForm("相机1窗口");

        [XmlIgnoreAttribute]
        public static HalconForm Camera2 = new HalconForm("相机2窗口");

        [XmlIgnoreAttribute]
        public static HalconForm Camera3 = new HalconForm("相机1窗口");

        [XmlIgnoreAttribute]
        public static HalconForm Camera4 = new HalconForm("相机2窗口");

        [XmlIgnoreAttribute]
        public static bool[,] bArryCamera1_Result = new bool[10, 10];  //相机工位1的结果，最多10行10例

        [XmlIgnoreAttribute]
        public static bool[] bArryCamera2_Result = null;  //相机工位1的结果，最多10行10例
        public static bool[,] bArryCamera2_RT2_Temp = new bool[10, 10];  //相机工位1的结果，最多10行10例
        public struct Visition1IsProductInPos
        {
            public string trayID;
            public bool[,] VisitionTestResults;
        };

        [XmlIgnoreAttribute]
        public static Queue<Visition1IsProductInPos> m_queVistion1ProductInPos = new Queue<Visition1IsProductInPos>();

        [XmlIgnoreAttribute]

        public static int PassedCamera1;
        public static int FailedCamera1;

        public static int PassedCamera2;
        public static int FailedCamera2;

        public static int PassedTotal;
        public static int FailedTotal;
        public static double UphTotal;

        public static Int16[,] FlashAreaData = new Int16[8, 100];
        public static string strTrayIDName;
        public static string strTrayIDName2;
        public static string strVirtrualPCBSN;
        public static string strVirtrualPCBSN2;

        public static bool bIsEndUnloadFlag = false;
        public static int CurrentUnitUnloadedToPackingStation = 0;
        public static short CurrentUnitInTray;

        public static bool PulseAllRemaindProduct = false;

        [XmlIgnoreAttribute]
        public static HPSQLService dataBaseSQL = new HPSQLService();

        public static object objSetPassedFiled = new object();

        public static bool InitializeHardware(bool bForceInitialize = false)
        {
            bool bResult = true;

            try
            {
                if (HardwareInitialized)
                    return true;

                HardwareInitialized = false;

                #region>>>>>>>> 初始化Modbus Server
                modebusTCPServer.Port = 5000;
                modebusTCPServer.ServerStart(modebusTCPServer.Port);
                if (!modebusTCPServer.IsStarted)
                {
                    HPglobal.RecordeAllMessage("机器人软件启动Modbus Server失败", true, false, "");
                    bResult = false;
                }

                HPglobal.RecordeAllMessage($"机器人软件启动Modbus Server成功", true, true, "");
                #endregion

                #region>>>>>>>> 连接光源控制器
                if (!lightSource.Connect("Com3"))
                {
                    HPglobal.RecordeAllMessage("连接光源控制器失败(Com3, 9600)，请确认端口是否正确或设备在线", true, false, "");
                    bResult = false;
                }
                #endregion

                #region >>>>>>>>>>>>>连接数据库
                HPRobotCtrl.dataBaseSQL.IpAddress = "192.168.1.100";
                HPRobotCtrl.dataBaseSQL.Port = 1433;
                HPRobotCtrl.dataBaseSQL.User = "sa";
                HPRobotCtrl.dataBaseSQL.Database = "HPMesSQL";
                HPRobotCtrl.dataBaseSQL.Password = "123456";
                if (!HPRobotCtrl.dataBaseSQL.Connect())
                {
                    HPglobal.RecordeAllMessage($"连接MySQL数据库失败: {dataBaseSQL.IpAddress}:{1433})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                }

                HPglobal.RecordeAllMessage($"连接MySQL数据库成功", true, true, "");
                #endregion

                #region>>>>>>>> 连接组盘PLC
                OperateResult result = PLC_Unload.ConnectPLC(HPglobal.strIPAddress_下料PLC, 5001);
                bIsPLCUnloadConnected = result.IsSuccess;
                if (!result.IsSuccess)
                {
                    HPglobal.RecordeAllMessage("连接组盘PLC失败", true, false, "");
                    bResult = false;
                }
                else
                {
                    HPglobal.RecordeAllMessage($"连接组盘PLC成功", true, true, "");
                }

                #endregion

                #region>>>>>>>> 启动数据监控
                bQuitHPRobot = false;
                threadIOMonitor = new Thread(new ParameterizedThreadStart(thread_ForIOMonitor));
                threadIOMonitor.Start(null);
                #endregion

                #region>>>>>>>> 连接Robot
                string ip1 = "127.0.0.1";
                string ip2 = "192.168.1.155";
                if (ObjRobot.Connect(ip2, 5000, "", true) != 0)
                {
                    HPglobal.RecordeAllMessage("连接Robot失败", true, false, "");
                    bResult = false;
                }
                else
                {
                    HPglobal.RecordeAllMessage($"连接Robot成功", true, true, "");
                }

                #endregion

                #region>>>>>>>>连接间距调整电机
                StepSevo.Station = 2;
                StepSevo.SerialPortInni("COM5", 9600, 8, StopBits.One, Parity.None);
                StepSevo.Open();
                Thread.Sleep(500);
                OperateResult<byte[]> stepvalues = StepSevo.Read("160", 16);
                if (!stepvalues.IsSuccess)
                {
                    HPglobal.RecordeAllMessage("连接间距调整电机失败，请确认端口是否正确或设备在线", true, false, "");
                    bResult = false;
                }

                HPglobal.RecordeAllMessage($"连接间距调整电机成功", true, true, "");
                #endregion

                HardwareInitialized = true;

                return bResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("RobotControl InitializeHardware() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public static void thread_ForIOMonitor(object wsarg)
        {
            while (!HPglobal.m_bQuitApp || bQuitHPRobot)
            {
                Thread.Sleep(100);
                try
                {
                    ReadFalshResultToRobotPLC();

                    #region>>>>>>读取D1300的数据
                    D1300Values = PLC_Unload.ReadInt16("D1300", 20);
                    if (D1300Values.IsSuccess)
                    {
                        IsLoadTrayInPos = (D1300Values.Content[0] == 1 ? true : false);     //产品到位	1300	1表示料盘有料，0表示无料盘
                        IsNeedQA = (D1300Values.Content[1] == 1 ? true : false);                //是否抽检	1301	1表示要抽检
                        IsBuffTrayInPos = (D1300Values.Content[2] == 1 ? true : false);        //缓冲盘产品状态	1302	1表示有缓冲盘，0表示无料盘
                        IsLoadTrigger = (D1300Values.Content[3] == 1 ? true : false);           //组盘触发信号	1303	1触发机器人下料
                        IsUnloadCompleted = (D1300Values.Content[4] == 1 ? true : false);//组盘完成信号	1304	1触发机器人完成下料(下料盘不一定满)
                        nUnloadResult = D1300Values.Content[5];       //组盘结果信号	1305	1触发机器人完成下料
                        IsUnloadTrayInPos = (D1300Values.Content[6] == 1 ? true : false);   //下料盘在位信号	1306	1表示料盘有料，0表示无料盘
                        IsUnloadTrayFull = (D1300Values.Content[7] == 1 ? true : false);    //下料盘已满信号	1307	1表示料盘已满，等待PLC设备取料盘（同时复位等待料盘上料完成信号)
                        IsUnloadTrayLoadCompleted = (D1300Values.Content[8] == 1 ? true : false); //下料盘上料完成信号	1308	1表示正在上下料盘，2表示上下料盘完成(同时1307为1)
                        IsRoboteSafe = (D1300Values.Content[9] == 1 ? true : false);            //PLC动作机器人安全信号	1309	1表示机器人在安全位置
                    }
                    else
                    {
                        bIsPLCUnloadConnected = false;
                        OperateResult result = PLC_Unload.ConnectPLC(HPglobal.strIPAddress_下料PLC, 5001);
                        bIsPLCUnloadConnected = result.IsSuccess;
                    }

                    #endregion

                    #region>>>>>>读取D1030的数据
                    OperateResult< short[] > values = PLC_Unload.ReadInt16("D1030", 50);
                    if(values.IsSuccess && frm_MainPanel != null)
                    {
                        frm_MainPanel.ParameterAllObj.CurrentCountTray = values.Content[10];
                        frm_MainPanel.ParameterAllObj.Cfg_Total = values.Content[11];
                        frm_MainPanel.ParameterAllObj.Cfg_CountTray = values.Content[12];
                        frm_MainPanel.ParameterAllObj.Cfg_CountTrayBK = values.Content[17];
                        frm_MainPanel.ParameterAllObj.Cfg_Remaind = values.Content[13];
                        frm_MainPanel.ParameterAllObj.Cfg_Row = values.Content[1];
                        frm_MainPanel.ParameterAllObj.Cfg_Col = values.Content[2];
                        frm_MainPanel.ParameterAllObj.TotalProductCount =(((int)values.Content[15] & 0x00FFFF) << 16) | ((int)values.Content[14] & 0x00FFFF);
                        frm_MainPanel.ParameterAllObj.CurrentTotalCountTray = values.Content[16];
                        frm_MainPanel.ParameterAllObj.Total_FailCount = values.Content[33];
                        frm_MainPanel.ParameterAllObj.Total_PassedAndPackedCount = (((int)values.Content[21] & 0x0000FFFF) << 16) | ((int)values.Content[20] & 0x0000FFFF);
                        frm_MainPanel.ParameterAllObj.Total_CheckedPassed = (((int)values.Content[40] & 0x00FFFF) << 16) | ((int)values.Content[39] & 0x00FFFF);
                        if (frm_MainPanel.ParameterAllObj.Cfg_Row > 0 && frm_MainPanel.ParameterAllObj.Cfg_Col > 0)
                        {
                            if (frm_MainPanel.ParameterAllObj.TotalProductCount % (frm_MainPanel.ParameterAllObj.Cfg_Row * frm_MainPanel.ParameterAllObj.Cfg_Col) == 0)
                                frm_MainPanel.ParameterAllObj.TotalProductCountTray = (short)(frm_MainPanel.ParameterAllObj.TotalProductCount / (int)(frm_MainPanel.ParameterAllObj.Cfg_Row * (int)frm_MainPanel.ParameterAllObj.Cfg_Col));
                            else
                                frm_MainPanel.ParameterAllObj.TotalProductCountTray = (short)(frm_MainPanel.ParameterAllObj.TotalProductCount / (frm_MainPanel.ParameterAllObj.Cfg_Row * frm_MainPanel.ParameterAllObj.Cfg_Col) + 1);
                        }
                        else
                        {
                            frm_MainPanel.ParameterAllObj.TotalProductCountTray = 0;
                        }

                        if(frm_MainPanel.ParameterAllObj.Cfg_CountTrayBK > 0)
                            frm_MainPanel.ParameterAllObj.TotalProductCountTrayLast = (short)(frm_MainPanel.ParameterAllObj.TotalProductCountTray % frm_MainPanel.ParameterAllObj.Cfg_CountTrayBK);

                        if (frm_MainPanel.ParameterAllObj.TotalProductCountTrayLast == 0)
                            frm_MainPanel.ParameterAllObj.TotalProductCountTrayLast = frm_MainPanel.ParameterAllObj.Cfg_CountTrayBK;

                        if (frm_MainPanel.ParameterAllObj.TotalProductCountTray - frm_MainPanel.ParameterAllObj.CurrentTotalCountTray <= frm_MainPanel.ParameterAllObj.TotalProductCountTrayLast)
                        {
                            HPRobotCtrl.PLC_Unload.Write("D1042", frm_MainPanel.ParameterAllObj.TotalProductCountTrayLast);
                        }
                        else
                        {
                            OperateResult<short> countTray = PLC_Unload.ReadInt16("D1047");
                            HPRobotCtrl.PLC_Unload.Write("D1042", countTray.Content);
                        }

                        HPRobotCtrl.PLC_Unload.Write("D1048", frm_MainPanel.ParameterAllObj.CurrentCountOfLoadTray);
                        HPRobotCtrl.PLC_Unload.Write("D1049", frm_MainPanel.ParameterAllObj.CurrentCountOfBuffTray);

                        frm_MainPanel.ParameterAllObj.Total_PassedAndPackedCount = frm_MainPanel.ParameterAllObj.CurrentCountOfLoadTray + frm_MainPanel.ParameterAllObj.CurrentCountOfBuffTray;
                        frm_MainPanel.ParameterAllObj.Total_PassedAndPackedCount += frm_MainPanel.ParameterAllObj.CurrentTotalCountTray * frm_MainPanel.ParameterAllObj.Cfg_Row * frm_MainPanel.ParameterAllObj.Cfg_Col;
                        HPRobotCtrl.PLC_Unload.Write("D1050", (int) frm_MainPanel.ParameterAllObj.Total_PassedAndPackedCount);

                        frm_MainPanel.ParameterAllObj.MarkFace = (short)PLC_Unload.ReadInt16("D2249")?.Content;

                        HPRobotCtrl.PassedTotal = values.Content[8];
                        HPRobotCtrl.FailedTotal = values.Content[9];
                        if ((HPRobotCtrl.PassedTotal + HPRobotCtrl.FailedTotal) > 0.00)
                            UphTotal = (HPRobotCtrl.PassedTotal) / (HPRobotCtrl.PassedTotal + HPRobotCtrl.FailedTotal);
                        else
                            UphTotal = 0.00;
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPRobotCtrl thread_ForIOMonitor  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

                }
            }
        }

        public static void QuitApplicationAndClear()
        {
            if (frm_MainPanel != null)
                frm_MainPanel._StopMachine();

            bQuitHPRobot = true;
            if(threadIOMonitor != null)
            {
                threadIOMonitor.Join(500);
                if (threadIOMonitor != null && threadIOMonitor.IsAlive)
                    threadIOMonitor.Abort();
            }

            #region>>>>>>>>>停止所有服务及关闭仪器
            ObjRobot.DisConnect();
            if(PLC_Unload.isConnected)
                PLC_Unload.ConnectClose();

            modebusTCPServer.ServerClose();

            #endregion

            #region>>>>>>>>>释放全局变量及对话框

            #endregion

            Camera1.CloseCamera();
            Camera2.CloseCamera();
            Camera3.CloseCamera();
            Camera4.CloseCamera();

            Camera1.Dispose();
            Camera2.Dispose();
            Camera3.Dispose();
            Camera4.Dispose();
        }

        public static bool CatchStep_Home()
        {
            try
            {
                OperateResult<Int16> status = HPRobotCtrl.StepSevo.ReadInt16(0x0040.ToString());
                HPRobotCtrl.StepSevo.Write(0x29.ToString(), new byte[] { 0x00, 0x01 });
                HPRobotCtrl.StepSevo.Write(0x31.ToString(), new byte[] { 0x00, 0x03 });
                HPRobotCtrl.StepSevo.Write(0x32.ToString(), new byte[] { 0x00, 0x30 });
                HPRobotCtrl.StepSevo.Write(0x33.ToString(), new byte[] { 0x00, 0x15 });
                HPRobotCtrl.StepSevo.Write(0x34.ToString(), new byte[] { 0x00, 0xFF });
                Thread.Sleep(1000);
                OperateResult result = HPRobotCtrl.StepSevo.Write(0x30.ToString(), new byte[] { 0x00, 0x01 });

                DateTime time1, time2;
                time2 = time1 = DateTime.Now;
                while (true)
                {
                    status = HPRobotCtrl.StepSevo.ReadInt16(0x0007.ToString());

                    if (status.IsSuccess == false)
                        return false;
                   else  if (status.IsSuccess &&
                        (status.Content & ((Int16)0x0001 << 1)) > 0 &&
                        (status.Content & ((Int16)0x0001 << 2)) == 0)
                        return true;

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 120)
                        return false;
                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("CatchStep_Org  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public static bool CatchStep_StepMove(Int32 dst, bool Relative = true)
        {
            try
            {
                OperateResult<Int16> status = HPRobotCtrl.StepSevo.ReadInt16(0x0007.ToString());
                HPRobotCtrl.StepSevo.Write(0x20.ToString(), new byte[] { 0x00, 0x05 }); // 起始速度 2-300
                HPRobotCtrl.StepSevo.Write(0x21.ToString(), new byte[] { 0x00, 0x64 });  // 加速时间 0-2000
                HPRobotCtrl.StepSevo.Write(0x22.ToString(), new byte[] { 0x00, 0x64 });  // 减速时间 0-2000
                HPRobotCtrl.StepSevo.Write(0x23.ToString(), new byte[] { 0x07, 0xD0 });  // 最大速度 -3000~3000
                HPRobotCtrl.StepSevo.Write(0x24.ToString(), dst);  //24 总脉冲数高位, 25 总脉冲数低位
//                 HPRobotCtrl.StepSevo.Write(0x24.ToString(), new byte[] { 0x00, 0x00, 0x13, 0xE8 }); //0x00, 0x01, 0x86, 0xA0
//                 HPRobotCtrl.StepSevo.Write(0x24.ToString(), new byte[] { 0x00, 0x00, 0x13, 0xE8 }); //0x00, 0x01, 0x86, 0xA0
//                 HPRobotCtrl.StepSevo.Write(0x24.ToString(), new byte[] { 0xFF, 0xFF, 0x3C, 0xB0 }); //0x00, 0x01, 0x86, 0xA0

                if (Relative)//相对位置 / 绝对位置 0：相对位置：以当前静止点为起点 1：绝对位置：以上电启动位置或回
                    HPRobotCtrl.StepSevo.Write(0x26.ToString(), new byte[] { 0x00, 0x00 });
                else
                    HPRobotCtrl.StepSevo.Write(0x26.ToString(), new byte[] { 0x00, 0x01 });

                Thread.Sleep(50);
                OperateResult result = HPRobotCtrl.StepSevo.Write(0x27.ToString(), new byte[] { 0x00, 0x01 });
                //OperateResult result = HPRobotCtrl.StepSevo.Write(0x27.ToString(), new byte[] { 0x00, 0x01 }); // Bit0: 0：正常停止； 1：急停；

                DateTime time1, time2;
                time2 = time1 = DateTime.Now;
                while (true)
                {
                    status = HPRobotCtrl.StepSevo.ReadInt16(0x0007.ToString());
                    if (status.IsSuccess &&
                       (status.Content & ((Int16)0x0001 << 2)) == 0)
                        return true;

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 10)
                        return false;
                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("CatchStep_StepMove  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public static bool ReadFalshResultToRobotPLC()
        {
            try
            {
                bool bWriteResult = false;
                int strArea1Addr = 7000;
                int strArea2Addr = strArea1Addr + 16 + 100;
                int strArea3Addr = strArea2Addr + 16 + 100;
                int strArea4Addr = strArea3Addr + 16 + 100;
                int strArea5Addr = strArea4Addr + 16 + 100;
                int strArea6Addr = strArea5Addr + 16 + 100;
                int strArea7Addr = strArea6Addr + 16 + 100;
                int strArea8Addr = strArea7Addr + 16 + 100;
                int[] nArryAddr = new int[] { strArea1Addr, strArea2Addr, strArea3Addr, strArea4Addr, strArea5Addr, strArea6Addr, strArea7Addr, strArea8Addr };

                #region>>>>>>>>读取条码
                OperateResult<string> sn = PLC_Unload.ReadString(HPglobal.PCBID1_Address, 16);
                OperateResult<string> tr = PLC_Unload.ReadString(HPglobal.TrayID1_Address, 16);

                OperateResult<string> sn2 = PLC_Unload.ReadString(HPglobal.PCBID2_Address, 16);
                OperateResult<string> tr2 = PLC_Unload.ReadString(HPglobal.TrayID2_Address, 16);

                if (sn.IsSuccess)
                    HPRobotCtrl.strVirtrualPCBSN = sn.Content.TrimEnd("\0".ToCharArray());

                if (tr.IsSuccess)
                    HPRobotCtrl.strTrayIDName = tr.Content.TrimEnd("\0".ToCharArray());

                if (sn2.IsSuccess)
                    HPRobotCtrl.strVirtrualPCBSN2 = sn2.Content.TrimEnd("\0".ToCharArray());

                if (tr2.IsSuccess)
                    HPRobotCtrl.strTrayIDName2 = tr2.Content.TrimEnd("\0".ToCharArray());

                #endregion

                #region>>>>>>>>读取烧录结果区域
//                 OperateResult<string> res1 = PLC_Unload.ReadString($"D{strArea1Addr}", 16);
//                 OperateResult<string> res2 = PLC_Unload.ReadString($"D{strArea2Addr}", 16);
//                 OperateResult<string> res3 = PLC_Unload.ReadString($"D{strArea3Addr}", 16);
//                 OperateResult<string> res4 = PLC_Unload.ReadString($"D{strArea4Addr}", 16);
//                 OperateResult<string> res5 = PLC_Unload.ReadString($"D{strArea5Addr}", 16);
//                 OperateResult<string> res6 = PLC_Unload.ReadString($"D{strArea6Addr}", 16);
//                 OperateResult<string> res7 = PLC_Unload.ReadString($"D{strArea7Addr}", 16);
//                 OperateResult<string> res8 = PLC_Unload.ReadString($"D{strArea8Addr}", 16);
// 
//                 if (res1.IsSuccess)
//                 {
//                     int n = res1.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[0] = res1.Content.Substring(0, n < 0 ? res1.Content.Length : n);
//                 }
// 
//                 if (res2.IsSuccess)
//                 {
//                     int n = res2.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[1] = res2.Content.Substring(0, n < 0 ? res2.Content.Length : n);
//                 }
// 
//                 if (res3.IsSuccess)
//                 {
//                     int n = res3.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[2] = res3.Content.Substring(0, n < 0 ? res3.Content.Length : n);
//                 }
// 
//                 if (res4.IsSuccess)
//                 {
//                     int n = res4.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[3] = res4.Content.Substring(0, n < 0 ? res4.Content.Length : n);
//                 }
// 
//                 if (res5.IsSuccess)
//                  {
//                     int n = res5.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[4] = res5.Content.Substring(0, n < 0 ? res5.Content.Length : n);
//                 }
// 
//                 if (res6.IsSuccess)
//                 {
//                     int n = res6.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[5] = res6.Content.Substring(0, n < 0 ? res6.Content.Length : n);
//                 }
// 
//                 if (res7.IsSuccess)
//                 {
//                     int n = res7.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[6] = res7.Content.Substring(0, n < 0 ? res7.Content.Length : n);
//                 }
// 
//                 if (res8.IsSuccess)
//                 {
//                     int n = res8.Content.IndexOf("\0");
//                     HPRobotCtrl.FlashDataPN[7] = res8.Content.Substring(0, n < 0 ? res8.Content.Length : n);
//                 }
// 
//                 if (res1.IsSuccess)
//                 {
//                     for (int nIndexAre = 0; nIndexAre < nArryAddr.Length; nIndexAre++)
//                     {
//                         OperateResult<Int16[]> results = PLC_Unload.ReadInt16($"D{nArryAddr[nIndexAre] + 16}", 100);
//                         if (results != null && results.IsSuccess)
//                         {
//                             for (int nIndexData = 0; nIndexData < 100; nIndexData++)
//                                 HPRobotCtrl.FlashAreaData[nIndexAre, nIndexData] = results.Content[nIndexData];
//                         }
//                     }
// 
//                     //=====================================================
//                     int nIndex = 0;
//                     for (int nArea = 0; nArea < 8; nArea++)
//                     {
//                         HPRobotCtrl.dtFlashDT[nArea] = new DataTable();
// 
//                         for (int i = 0; i < 10; i++)
//                             dtFlashDT[nArea].Columns.Add(i.ToString(), typeof(string));
// 
//                         nIndex = 0;
//                         for (int nRow = 0; nRow < 10; nRow++)
//                         {
//                             DataRow dr = dtFlashDT[nArea].NewRow();
//                             for (int nCol = 0; nCol < 10; nCol++, nIndex++)
//                                 dr[nCol] = $"0x{((byte)HPRobotCtrl.FlashAreaData[nArea, nIndex]).ToString("X02")}";
// 
//                             dtFlashDT[nArea].Rows.Add(dr);
//                         }
//                     }
//                     //====================================================
//                 }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("ReadFalshResultToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                throw new Exception(ex.Message);
            }
        }

        public static bool RecordUnloaedPCBID_ToRobotPLC(string PCBID)
        {
            bool bWriteResult = false;
            int nLenOfAddr = 20;
            int[] arryAreaAddr = new int[20];

            try
            {
                if (PCBID == null || PCBID.Length <= 0)
                    return false;

                for (int nIndex = 0; nIndex < nLenOfAddr; nIndex++)
                {
                    arryAreaAddr[nIndex] = 8000 + nIndex * 16;
                }

                for (int nIndex = 0; nIndex < nLenOfAddr; nIndex++)
                {
                    string strPN = PLC_Unload.ReadString($"D{arryAreaAddr[nIndex]}", 16).Content;
                    if ((strPN != null && strPN.Length > 0 && strPN.Contains("Empty")))
                    {
                        PLC_Unload.Write($"D{arryAreaAddr[nIndex]}", PCBID);
                        bWriteResult = true;
                        break;
                    }
                }

                if (!bWriteResult)
                {
                    HPglobal.m_strLastException = $"RecordUnloaedPCBID_ToRobotPLC 写入已下料PCBID失败! PCBID编号: {PCBID}";
                    HPglobal.GlobalWorkingMessageOutput(HPglobal.m_strLastException, Color.Red, 9, 1);
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }

                return bWriteResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("RecordUnloaedPCBID_ToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public static void ResetFlashAreaData(string sn, bool clearAll = false)
        {
            int strArea1Addr = 7000;
            int strArea2Addr = strArea1Addr + 16 + 100;
            int strArea3Addr = strArea2Addr + 16 + 100;
            int strArea4Addr = strArea3Addr + 16 + 100;
            int strArea5Addr = strArea4Addr + 16 + 100;
            int strArea6Addr = strArea5Addr + 16 + 100;
            int strArea7Addr = strArea6Addr + 16 + 100;
            int strArea8Addr = strArea7Addr + 16 + 100;

            if (sn == null || sn.Length <= 0)
                return;

            int[] nArryAddr = new int[] { strArea1Addr, strArea2Addr, strArea3Addr, strArea4Addr, strArea5Addr, strArea6Addr, strArea7Addr, strArea8Addr };
            try
            {
                for (int nIndex = 0; nIndex < nArryAddr.Length; nIndex++)
                {
                    int nAddr = nArryAddr[nIndex];
                    OperateResult<string> strPN = PLC_Unload.ReadString($"D{nAddr}", 16);
                    if (clearAll ||
                       (!clearAll && (strPN.IsSuccess && strPN.Content.Length > 0 && strPN.Content.Contains(sn))))
                    {
                        PLC_Unload.Write($"D{nAddr}", "Empty");

                        Int16[] resetData = new Int16[100];

                        for (int n = 0; n < 100; n++)
                            resetData[n] = 0xFF;

                        PLC_Unload.Write($"D{nAddr + 16}", resetData);
                    }
                }

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteFalshResultToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                throw new Exception(ex.Message);
            }
        }

        public static short[] GetFlashAreaDataRow(int nRow, string sn, bool clear = false)
        {
            Int16[] arrayData = new Int16[10];
            for (int n = 0; n < 10; n++)
            {
                arrayData[n] = 0xFF;
            }

            try
            {
                //得到烧录区域的结果
                /*                short[,] FlashResult = GetFlashAreaData2(sn, clear);*/

                Int16[,] FlashResult = null;
                HPRobotCtrl.dataBaseSQL.SQL_GetFlashResultFromTray(sn, out FlashResult);
                if (FlashResult == null || FlashResult.Length < 100)
                    return arrayData;

                for (int n = 0; n < 10; n++)
                {
                    arrayData[n] = FlashResult[nRow, n];
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("GetFlashAreaDataRow  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return arrayData;
        }

        public static short[,] GetFlashAreaData2(string sn, bool clear = false)
        {
            try
            {
                //得到烧录区域的结果
                short[] FalshResult = GetFlashAreaData(sn, clear);

                if (FalshResult == null)
                    return null;

                short[,] arrayData = new short[10, 10];
                int nIndexCurrent = 0;
                for (int nIndexRow = 0; nIndexRow < 10; nIndexRow++)
                {
                    for (int nIndexCol = 0; nIndexCol < 10; nIndexCol++)
                    {
                        arrayData[nIndexRow, nIndexCol] = FalshResult[nIndexCurrent];
                        nIndexCurrent++;
                    }
                }

                return arrayData;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteFalshResultToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return null;
            }
        }

        public static short[]  GetFlashAreaData(string sn, bool clear = false)
        {
            int strArea1Addr = 7000;
            int strArea2Addr = strArea1Addr + 16 + 100;
            int strArea3Addr = strArea2Addr + 16 + 100;
            int strArea4Addr = strArea3Addr + 16 + 100;
            int strArea5Addr = strArea4Addr + 16 + 100;
            int strArea6Addr = strArea5Addr + 16 + 100;
            int strArea7Addr = strArea6Addr + 16 + 100;
            int strArea8Addr = strArea7Addr + 16 + 100;
            int[] nArryAddr = new int[] { strArea1Addr, strArea2Addr, strArea3Addr, strArea4Addr, strArea5Addr, strArea6Addr, strArea7Addr, strArea8Addr };

            try
            {
                for (int nIndex = 0; nIndex < nArryAddr.Length; nIndex++)
                {
                    OperateResult<string> snFlash = PLC_Unload.ReadString($"D{nArryAddr[nIndex]}", 16);
                    int n = snFlash.Content.IndexOf("\0");
                    string strSNFlash = snFlash.Content.Substring(0, n < 0 ? snFlash.Content.Length : n);
                    if (strSNFlash.Length  > 0 && sn.Contains(strSNFlash))
                    {
                        OperateResult<short[]> values = PLC_Unload.ReadInt16($"D{nArryAddr[nIndex] + 16}", 100);
                        return values.Content;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteFalshResultToRobotPLC  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return null;
            }
        }

    }
}
