﻿using GenericLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CamaeraLib
{
    public class CameraConfig: AsynNotifyPropertyChanged{
        //[CategoryAttribute("规则"), DisplayNameAttribute("贴目")]
        public string Type { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public bool Trigger { get; set; }
        public int TriggerSource { get; set; }
        public double ExposureTime { get; set; }

        public int GainVal { get; set; }
        public CameraConfig()
        {
            Name = "";
            Trigger = false;
            TriggerSource = 0;
            ExposureTime = 1000;
            GainVal = 1;
        }
    }
    public class Image
    {
        public Image(int newWidth, int newHeight, Byte[] newBuffer, bool color)
        {
            Width = newWidth;
            Height = newHeight;
            Buffer = newBuffer;
            Color = color;
        }
        public Bitmap ToBitmap()
        {
            Bitmap m_bitmap = null;
            BitmapFactory.CreateBitmap(out m_bitmap,Width, Height, Color);
            BitmapFactory.UpdateBitmap(m_bitmap, Buffer, Width, Height, Color);
            return m_bitmap;
        }
        public readonly int Width; /* The width of the image. */
        public readonly int Height; /* The height of the image. */
        public readonly Byte[] Buffer; /* The raw image data. */
        public readonly bool Color; /* If false the buffer contains a Mono8 image. Otherwise, RGBA8packed is provided. */
    }
    public class CameraEventArgs : EventArgs
    {
        public int CameraStatusID { get; set; }//1-Open 2-Close 3-Loss

    }
    public class CameraBase
    {

        public CameraConfig _CameraConfig=new CameraConfig();
        
        public delegate void OnGrabbed(object sender, EventArgs e);//建立委托
        public event OnGrabbed onGrabbed;

        public event EventHandler<CameraEventArgs> OnCameraStatusChanged;/* 相机状态事件 */

        public int camID { get; set; }
        public string camName { get; set; }
       
        public double m_scale { get; set; }
        public bool m_bShowByGDI { get; set; }

        public bool m_open = false;
        public string  LastError { get; set; }
        public Graphics _g { get; set; }

        public Mutex m_mutex = new Mutex();        /* 锁，保证多线程安全 */
        public virtual int OpenCamByID(short id)
        {
            return 0;
        }
        public virtual int OpenCameraByName(string Name)
        {
            return 0;
        }
        public virtual int Close()
        {
            return 0;
        }
        public virtual int OneShot()
        {
            return 0;
        }
        public virtual int ContinuousShot()
        {
            return 0;
        }
        public virtual int Stop()
        {
            return 0;
        }
        public virtual int SetTrigger(bool Trigger)
        {
            return 0;
        }
        public virtual int SetTriggerSource(int TriggerSource)
        {
            return 0;
        }

        public virtual int SetExposureTime(int ExposureTime)
        {
            return 0;
        }
        public virtual int getExposureTime(out int ExposureTime)
        {
            ExposureTime = 0;
            return 0;
        }
        public virtual int SetGainVal(int GainVal)
        {
            return 0;
        }
        public virtual int GetGainVal(out int GainVal)
        {
            GainVal = 0;
            return 0;
        }
        public virtual Image GetLatestImage()
        {
            return null;
        }
        public virtual Bitmap GetLatestBitmap()
        {
            return null;
        }

        public delegate void DeviceOpenedEventHandler(int id);
        public event DeviceOpenedEventHandler DeviceOpenedEvent;

        public delegate void DeviceClosingEventHandler(int id);
        public event DeviceClosingEventHandler DeviceClosingEvent;

        public delegate void DeviceClosedEventHandler(int id);
        public event DeviceClosedEventHandler DeviceClosedEvent;

        public delegate void GrabbingStartedEventHandler(int id);
        public event GrabbingStartedEventHandler GrabbingStartedEvent;

        public delegate void ImageReadyEventHandler();
        public event ImageReadyEventHandler ImageReadyEvent;

        public delegate void GrabbingStoppedEventHandler(int id);
        public event GrabbingStoppedEventHandler GrabbingStoppedEvent;

        public delegate void GrabErrorEventHandler(Exception grabException, string additionalErrorMessage);
        public event GrabErrorEventHandler GrabErrorEvent;

        public delegate void DeviceRemovedEventHandler(int id);
        public event DeviceRemovedEventHandler DeviceRemovedEvent;

        /* Notify that ImageProvider is open and ready for grabbing and configuration. */
        protected void OnDeviceOpenedEvent()
        {
            m_open = true;
            if (DeviceOpenedEvent != null)
            {
                DeviceOpenedEvent(camID);
            }
            if (OnCameraStatusChanged != null)
            {
                CameraEventArgs cameraEventArgs = new CameraEventArgs();
                cameraEventArgs.CameraStatusID = 1;
                OnCameraStatusChanged(null, cameraEventArgs);
            }
        }

        /* Notify that ImageProvider is about to close the device to give other objects the chance to do clean up operations. */
        protected void OnDeviceClosingEvent()
        {
            m_open = false;
            if (DeviceClosingEvent != null)
            {
                DeviceClosingEvent(camID);
            }
           
        }

        /* Notify that ImageProvider is now closed.*/
        protected void OnDeviceClosedEvent()
        {
            m_open = false;
            if (DeviceClosedEvent != null)
            {
                DeviceClosedEvent(camID);
            }
            if (OnCameraStatusChanged != null)
            {
                CameraEventArgs cameraEventArgs = new CameraEventArgs();
                cameraEventArgs.CameraStatusID = 2;
                OnCameraStatusChanged(null, cameraEventArgs);
            }
        }

        /* Notify that grabbing has started. This event could be used to update the state of the GUI. */
        protected void OnGrabbingStartedEvent()
        {
            if (GrabbingStartedEvent != null)
            {
                GrabbingStartedEvent(camID);
            }
        }

        /* Notify that an image has been added to the output queue. The receiver of the event can use GetCurrentImage() to acquire and process the image
         and ReleaseImage() to remove the image from the queue and return it to the stream grabber.*/
        protected void OnImageReadyEvent()
        {
            if (ImageReadyEvent != null)
            {
                ImageReadyEvent();
            }
        }

        /* Notify that grabbing has stopped. This event could be used to update the state of the GUI. */
        protected void OnGrabbingStoppedEvent()
        {
            if (GrabbingStoppedEvent != null)
            {
                GrabbingStoppedEvent(camID);
            }
        }

        /* Notify that the grabbing had errors and deliver the information. */
        protected void OnGrabErrorEvent(Exception grabException, string additionalErrorMessage)
        {
            if (GrabErrorEvent != null)
            {
                GrabErrorEvent(grabException, additionalErrorMessage);
            }
        }

        /* Notify that the device has been removed from the PC. */
        protected void OnDeviceRemovedEvent()
        {
            
            if (DeviceRemovedEvent != null)
            {
                DeviceRemovedEvent(camID);
            }
        }
    }
}
