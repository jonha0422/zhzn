﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib.ImageProcess
{
    public class ColorCheck : ImageProcessBase
    {
        public int chkMode { get; set; }
        public int R{get;set;}
        public int G { get; set; }

        public int B { get; set; }

        public double Gray { get; set; }//灰度值 ---HSV

        public double ColorOffser { get; set; }
         public ColorCheck()
        {
            cName = "颜色检查";

        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            int suc = getTempColor();
            return 0;
        }
        public int getTempColor()
        {
            if (ModelRoi==null || ModelRoi.Count == 0)
            {
                return -1;
            }
            HObject ho_ImageReduced;
            HRegion hReg = ModelRoi[0].getRegion();
            HOperatorSet.ReduceDomain(TempImage, hReg, out  ho_ImageReduced);
            HTuple channels = 0;
            HOperatorSet.CountChannels(ho_ImageReduced, out channels);
            HTuple mean, deviation;
            if (channels == 3 && chkMode != 0)
            {
                HObject Red,Green,Blue;
                HObject  Hue, Saturation, Value;
                HOperatorSet.Decompose3(ho_ImageReduced,out Red,out Green,out Blue);
                HOperatorSet.TransFromRgb(Red, Green, Blue,out Hue,out Saturation,out Value,"hsv");
                //HOperatorSet.ReduceDomain(Hue, hReg, out  Hue);
                HOperatorSet.Intensity(hReg,Hue,out mean,out deviation);
            }
            else
            {
                if (channels == 3)
                {
                    HObject gObject=null;
                    HOperatorSet.Rgb1ToGray(TempImage, out gObject);
                    HOperatorSet.Intensity(hReg, gObject, out mean, out deviation);
                }
                else
                {
                    HOperatorSet.Intensity(hReg, TempImage, out mean, out deviation);
                }
                
            }
            Gray=mean.D ;
            return 0;
        }
        public override int ImageProcess()
        {
             if (SrcObject == null)
            {
                return 0;
            }

             DesObject = null ;
            HObject SearchObj = SrcObject;
            HObject ho_ImageReduced;
            ResultSta = true;
            for (int i = 0; i < SearchRoi.Count; i++)
            {
                HTuple mean, deviation;
                HTuple channels = 0;
                HOperatorSet.CountChannels(SearchObj, out channels);
                HRegion hReg = SearchRoi[i].getRegion();
                HOperatorSet.ReduceDomain(SrcObject, hReg, out  ho_ImageReduced);
                if (channels == 3 && chkMode!=0)
                {
                    HObject Red, Green, Blue;
                    HObject Hue, Saturation, Value;
                    HOperatorSet.Decompose3(ho_ImageReduced, out Red, out Green, out Blue);
                    HOperatorSet.TransFromRgb(Red, Green, Blue, out Hue, out Saturation, out Value, "hsv");
                    //HOperatorSet.ReduceDomain(Hue, hReg, out  Hue);
                    HOperatorSet.Intensity(hReg, Hue, out mean, out deviation);
                }
                else
                {
                    if (channels == 3)
                    {
                        HObject gObject = null;
                        HOperatorSet.Rgb1ToGray(SearchObj, out gObject);
                        HOperatorSet.Intensity(hReg, gObject, out mean, out deviation);
                    }
                    else
                    {
                        HOperatorSet.Intensity(hReg, SearchObj, out mean, out deviation);
                    }
                }

                Result aR = new Result();
                aR.cX = mean;
                if (mean > Gray + ColorOffser|| mean < Gray - ColorOffser)
                {
                    //HOperatorSet.DrawCircle()
                   // HOperatorSet.OverpaintRegion
                    if (ViewPort != null)
                    {
                        HObject aRegion = SearchRoi[i].getRegion();
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                        HOperatorSet.DispRegion(aRegion, ViewPort.HalconWindow);
                    }
                    else
                    {
                        //HOperatorSet.PaintRegion(SearchRoi[i].getRegion(), DesObject, out DesObject, (((new HTuple(255)).TupleConcat(0)).TupleConcat(0)), new HTuple("margin"));
                    }
                    aR.isOk = false;
                    ResultSta = false;
                }
                else
                {
                    if (ViewPort != null)
                    {
                        HObject aRegion = SearchRoi[i].getRegion();
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                        HOperatorSet.DispRegion(aRegion, ViewPort.HalconWindow);
                    }
                    else
                    {
                       // HOperatorSet.PaintRegion(SearchRoi[i].getRegion(), DesObject, out DesObject, (((new HTuple(0)).TupleConcat(255)).TupleConcat(0)), new HTuple("margin"));
                    }
                    
                    aR.isOk = true;
                }
                mResult.Add(aR);
            }
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            ColorCheck aFun = CoverLib.JsonCover.DeStrLSerialize<ColorCheck>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<ColorCheck>(this);
            return aFunPara;
        }
    }
}
