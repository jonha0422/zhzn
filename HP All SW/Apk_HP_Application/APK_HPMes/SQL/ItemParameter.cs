﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APK_HP.MES.HPSql
{
    public class ItemParameter
    {
        /// <summary>
        /// 工站名称 （上料工站，烧录工站，打码工站，切割工站，顶部测试工站，底部测试工站，包装工站）
        /// </summary>
        public string WorkStationName;
        /// <summary>
        /// 数据个数
        /// </summary>
        public int DataNumber;
        /// <summary>
        /// 执行结果
        /// </summary>
        public bool ExecutionResult;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName1;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria1;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin1;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax1;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName2;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria2;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin2;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax2;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName3;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria3;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin3;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax3;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName4;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria4;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin4;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax4;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName5;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria5;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin5;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax5;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName6;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria6;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin6;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax6;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName7;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria7;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin7;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax7;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName8;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria8;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin8;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax8;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName9;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria9;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin9;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax9;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ItemName10;
        /// <summary>
        /// 项目标准
        /// </summary>
        public string ItemCriteria10;
        /// <summary>
        /// 项目最小
        /// </summary>
        public string ItemMin10;
        /// <summary>
        /// 项目最大
        /// </summary>
        public string ItemMax10;
    }
}
