﻿namespace APK_HPMes
{
    partial class WinSQL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.绑盘 = new System.Windows.Forms.Button();
            this.上料 = new System.Windows.Forms.Button();
            this.上料历史 = new System.Windows.Forms.Button();
            this.烧录 = new System.Windows.Forms.Button();
            this.烧录历史 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.打码历史 = new System.Windows.Forms.Button();
            this.切割 = new System.Windows.Forms.Button();
            this.切割历史 = new System.Windows.Forms.Button();
            this.顶部检测 = new System.Windows.Forms.Button();
            this.顶部检测历史 = new System.Windows.Forms.Button();
            this.底部检测 = new System.Windows.Forms.Button();
            this.底部检测历史 = new System.Windows.Forms.Button();
            this.包装 = new System.Windows.Forms.Button();
            this.包装历史 = new System.Windows.Forms.Button();
            this.解盘 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Aqua;
            this.button1.Location = new System.Drawing.Point(661, 54);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(240, 79);
            this.button1.TabIndex = 0;
            this.button1.Text = "连接数据库";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // 绑盘
            // 
            this.绑盘.BackColor = System.Drawing.Color.Aqua;
            this.绑盘.Location = new System.Drawing.Point(108, 56);
            this.绑盘.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.绑盘.Name = "绑盘";
            this.绑盘.Size = new System.Drawing.Size(83, 74);
            this.绑盘.TabIndex = 1;
            this.绑盘.Text = "绑盘";
            this.绑盘.UseVisualStyleBackColor = false;
            this.绑盘.Click += new System.EventHandler(this.绑盘_Click);
            // 
            // 上料
            // 
            this.上料.BackColor = System.Drawing.Color.Aqua;
            this.上料.Location = new System.Drawing.Point(108, 279);
            this.上料.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.上料.Name = "上料";
            this.上料.Size = new System.Drawing.Size(83, 74);
            this.上料.TabIndex = 1;
            this.上料.Text = "上料";
            this.上料.UseVisualStyleBackColor = false;
            this.上料.Click += new System.EventHandler(this.上料_Click);
            // 
            // 上料历史
            // 
            this.上料历史.BackColor = System.Drawing.Color.Aqua;
            this.上料历史.Location = new System.Drawing.Point(108, 185);
            this.上料历史.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.上料历史.Name = "上料历史";
            this.上料历史.Size = new System.Drawing.Size(83, 74);
            this.上料历史.TabIndex = 1;
            this.上料历史.Text = "上料历史";
            this.上料历史.UseVisualStyleBackColor = false;
            this.上料历史.Click += new System.EventHandler(this.上料历史_Click);
            // 
            // 烧录
            // 
            this.烧录.BackColor = System.Drawing.Color.Aqua;
            this.烧录.Location = new System.Drawing.Point(223, 279);
            this.烧录.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.烧录.Name = "烧录";
            this.烧录.Size = new System.Drawing.Size(83, 74);
            this.烧录.TabIndex = 1;
            this.烧录.Text = "烧录";
            this.烧录.UseVisualStyleBackColor = false;
            this.烧录.Click += new System.EventHandler(this.烧录_Click);
            // 
            // 烧录历史
            // 
            this.烧录历史.BackColor = System.Drawing.Color.Aqua;
            this.烧录历史.Location = new System.Drawing.Point(223, 185);
            this.烧录历史.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.烧录历史.Name = "烧录历史";
            this.烧录历史.Size = new System.Drawing.Size(83, 74);
            this.烧录历史.TabIndex = 1;
            this.烧录历史.Text = "烧录历史";
            this.烧录历史.UseVisualStyleBackColor = false;
            this.烧录历史.Click += new System.EventHandler(this.烧录历史_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Aqua;
            this.button2.Location = new System.Drawing.Point(333, 279);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 74);
            this.button2.TabIndex = 1;
            this.button2.Text = "打码";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // 打码历史
            // 
            this.打码历史.BackColor = System.Drawing.Color.Aqua;
            this.打码历史.Location = new System.Drawing.Point(333, 185);
            this.打码历史.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.打码历史.Name = "打码历史";
            this.打码历史.Size = new System.Drawing.Size(83, 74);
            this.打码历史.TabIndex = 1;
            this.打码历史.Text = "打码历史";
            this.打码历史.UseVisualStyleBackColor = false;
            this.打码历史.Click += new System.EventHandler(this.打码历史_Click);
            // 
            // 切割
            // 
            this.切割.BackColor = System.Drawing.Color.Aqua;
            this.切割.Location = new System.Drawing.Point(442, 279);
            this.切割.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.切割.Name = "切割";
            this.切割.Size = new System.Drawing.Size(83, 74);
            this.切割.TabIndex = 1;
            this.切割.Text = "切割";
            this.切割.UseVisualStyleBackColor = false;
            this.切割.Click += new System.EventHandler(this.切割_Click);
            // 
            // 切割历史
            // 
            this.切割历史.BackColor = System.Drawing.Color.Aqua;
            this.切割历史.Location = new System.Drawing.Point(442, 185);
            this.切割历史.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.切割历史.Name = "切割历史";
            this.切割历史.Size = new System.Drawing.Size(83, 74);
            this.切割历史.TabIndex = 1;
            this.切割历史.Text = "切割历史";
            this.切割历史.UseVisualStyleBackColor = false;
            this.切割历史.Click += new System.EventHandler(this.切割历史_Click);
            // 
            // 顶部检测
            // 
            this.顶部检测.Location = new System.Drawing.Point(544, 279);
            this.顶部检测.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.顶部检测.Name = "顶部检测";
            this.顶部检测.Size = new System.Drawing.Size(83, 74);
            this.顶部检测.TabIndex = 1;
            this.顶部检测.Text = "顶部检测";
            this.顶部检测.UseVisualStyleBackColor = true;
            this.顶部检测.Click += new System.EventHandler(this.顶部检测_Click);
            // 
            // 顶部检测历史
            // 
            this.顶部检测历史.BackColor = System.Drawing.Color.Aqua;
            this.顶部检测历史.Location = new System.Drawing.Point(544, 185);
            this.顶部检测历史.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.顶部检测历史.Name = "顶部检测历史";
            this.顶部检测历史.Size = new System.Drawing.Size(83, 74);
            this.顶部检测历史.TabIndex = 1;
            this.顶部检测历史.Text = "顶部检测历史";
            this.顶部检测历史.UseVisualStyleBackColor = false;
            this.顶部检测历史.Click += new System.EventHandler(this.顶部检测历史_Click);
            // 
            // 底部检测
            // 
            this.底部检测.Location = new System.Drawing.Point(650, 279);
            this.底部检测.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.底部检测.Name = "底部检测";
            this.底部检测.Size = new System.Drawing.Size(83, 74);
            this.底部检测.TabIndex = 1;
            this.底部检测.Text = "底部检测";
            this.底部检测.UseVisualStyleBackColor = true;
            this.底部检测.Click += new System.EventHandler(this.底部检测_Click);
            // 
            // 底部检测历史
            // 
            this.底部检测历史.BackColor = System.Drawing.Color.Aqua;
            this.底部检测历史.Location = new System.Drawing.Point(650, 185);
            this.底部检测历史.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.底部检测历史.Name = "底部检测历史";
            this.底部检测历史.Size = new System.Drawing.Size(83, 74);
            this.底部检测历史.TabIndex = 1;
            this.底部检测历史.Text = "底部检测历史";
            this.底部检测历史.UseVisualStyleBackColor = false;
            this.底部检测历史.Click += new System.EventHandler(this.底部检测历史_Click);
            // 
            // 包装
            // 
            this.包装.BackColor = System.Drawing.Color.Aqua;
            this.包装.Location = new System.Drawing.Point(761, 279);
            this.包装.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.包装.Name = "包装";
            this.包装.Size = new System.Drawing.Size(83, 74);
            this.包装.TabIndex = 1;
            this.包装.Text = "包装";
            this.包装.UseVisualStyleBackColor = false;
            this.包装.Click += new System.EventHandler(this.包装_Click);
            // 
            // 包装历史
            // 
            this.包装历史.BackColor = System.Drawing.Color.Aqua;
            this.包装历史.Location = new System.Drawing.Point(761, 185);
            this.包装历史.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.包装历史.Name = "包装历史";
            this.包装历史.Size = new System.Drawing.Size(83, 74);
            this.包装历史.TabIndex = 1;
            this.包装历史.Text = "包装历史";
            this.包装历史.UseVisualStyleBackColor = false;
            this.包装历史.Click += new System.EventHandler(this.包装历史_Click);
            // 
            // 解盘
            // 
            this.解盘.BackColor = System.Drawing.Color.Aqua;
            this.解盘.Location = new System.Drawing.Point(892, 224);
            this.解盘.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.解盘.Name = "解盘";
            this.解盘.Size = new System.Drawing.Size(83, 74);
            this.解盘.TabIndex = 1;
            this.解盘.Text = "解盘";
            this.解盘.UseVisualStyleBackColor = false;
            this.解盘.Click += new System.EventHandler(this.解盘_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(650, 374);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 74);
            this.button3.TabIndex = 1;
            this.button3.Text = "底部检测";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // WinSQL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 486);
            this.Controls.Add(this.包装历史);
            this.Controls.Add(this.包装);
            this.Controls.Add(this.底部检测历史);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.底部检测);
            this.Controls.Add(this.顶部检测历史);
            this.Controls.Add(this.顶部检测);
            this.Controls.Add(this.切割历史);
            this.Controls.Add(this.切割);
            this.Controls.Add(this.打码历史);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.烧录历史);
            this.Controls.Add(this.烧录);
            this.Controls.Add(this.上料历史);
            this.Controls.Add(this.上料);
            this.Controls.Add(this.解盘);
            this.Controls.Add(this.绑盘);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "WinSQL";
            this.Text = "WinSQL";
            this.Load += new System.EventHandler(this.WinSQL_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button 绑盘;
        private System.Windows.Forms.Button 上料;
        private System.Windows.Forms.Button 上料历史;
        private System.Windows.Forms.Button 烧录;
        private System.Windows.Forms.Button 烧录历史;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button 打码历史;
        private System.Windows.Forms.Button 切割;
        private System.Windows.Forms.Button 切割历史;
        private System.Windows.Forms.Button 顶部检测;
        private System.Windows.Forms.Button 顶部检测历史;
        private System.Windows.Forms.Button 底部检测;
        private System.Windows.Forms.Button 底部检测历史;
        private System.Windows.Forms.Button 包装;
        private System.Windows.Forms.Button 包装历史;
        private System.Windows.Forms.Button 解盘;
        private System.Windows.Forms.Button button3;
    }
}