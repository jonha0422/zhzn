﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib.ImageProcess
{
    public class FindAnisoShapeMode: ImageProcessBase
    {
        private HTuple hv_ModelID = null;
        public int numLevels { get; set; }
        public int angleStart { get; set; }
        public int angleExtent { get; set; }
      //  public int angleStep { get; set; }
        public double scaleRMin { get; set; }
        public double scaleRMax { get; set; }

        public double scaleCMin { get; set; }
        public double scaleCMax { get; set; }
        //public int scaleStep { get; set; }

        public int contrast { get; set; }

        public int minContrast { get; set; }

        public double MinScore { get; set; }
        public int NumMatches { get; set; }

        public double MaxOverlap { get; set; }
        public double Greediness { get; set; }


       
        private HObject ho_Model;
        public FindAnisoShapeMode()
        {
            cName = "模板定位";
            numLevels = 5;
            angleStart = -45;
            angleExtent = 90;
            scaleRMin = 0.8;
            scaleRMax = 1.1;
            scaleCMin = 0.8;
            scaleCMax = 1.1;
            contrast = 40;
            minContrast = 10;

            MinScore = 0.5;
            NumMatches = 1;
            MaxOverlap = 0.5;
            Greediness = 0.5;
            SearchRoiCount = 5;
        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            create_model();
            return 0;
        }
        public override int ImageProcess()
        {
            if (SrcObject == null)
            {
                return 0;
            }
            if (hv_ModelID == null)
            {
                return 0;
            }
            DesObject = null;
            HObject SearchObj = SrcObject;
            HTuple channels = 0;
            HOperatorSet.CountChannels(SearchObj, out channels);
            if (channels == 3)
            {
                HOperatorSet.Rgb1ToGray(SearchObj, out SearchObj);
            }
            ResultSta = true;
            for (int i = 0; i < SearchRoi.Count; i++)
            {
                if (SearchRoi.Count > 0)
                {
                    HRegion hReg = SearchRoi[i].getRegion();
                    // HOperatorSet.GenRectangle2(out RecRoi, hReg.Row, hReg.Column, ModelRoi[0].Phi, ModelRoi[0].Length1, ModelRoi[0].Length2);
                    //  HOperatorSet.ReduceDomain(ho_Image, hReg, out ho_ImageReduced);
                    HOperatorSet.ReduceDomain(SrcObject, hReg, out  SearchObj);

                }
                HTuple hv_Row = null;
                HTuple hv_Column = null, hv_Angle = null, hv_RScale = null, hv_CScale = null;
                HTuple hv_Score = null;

                HOperatorSet.FindAnisoShapeModel(SearchObj, hv_ModelID, angleStart, angleExtent, scaleRMin, scaleRMax, scaleCMin,scaleCMax, MinScore, NumMatches, MaxOverlap, "least_squares", numLevels, Greediness,
                    out hv_Row, out hv_Column, out hv_Angle, out hv_RScale, out hv_CScale, out hv_Score);

                HTuple hv_I = null, hv_HomMat2DIdentity = new HTuple();
                HTuple hv_HomMat2DTranslate = new HTuple(), hv_HomMat2DRotate = new HTuple();
                HTuple hv_HomMat2DScale = new HTuple();
                HObject ho_ModelTrans;
                int drawID = 0;
                HOperatorSet.GenEmptyObj(out ho_ModelTrans);
                for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
                {
                    HOperatorSet.HomMat2dIdentity(out hv_HomMat2DIdentity);
                    HOperatorSet.HomMat2dTranslate(hv_HomMat2DIdentity, hv_Row.TupleSelect(hv_I),
                        hv_Column.TupleSelect(hv_I), out hv_HomMat2DTranslate);
                    HOperatorSet.HomMat2dRotate(hv_HomMat2DTranslate, hv_Angle.TupleSelect(hv_I),
                        hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out hv_HomMat2DRotate);
                    HOperatorSet.HomMat2dScale(hv_HomMat2DRotate, hv_RScale.TupleSelect(hv_I), hv_CScale.TupleSelect(
                        hv_I), hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out hv_HomMat2DScale);
                    ho_ModelTrans.Dispose();
                    HOperatorSet.AffineTransContourXld(ho_Model, out ho_ModelTrans, hv_HomMat2DScale);
                    if (ViewPort != null)
                    {
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                        HOperatorSet.DispXld(ho_ModelTrans, ViewPort.HalconWindow);
                    }
                    else
                    {
                        if (drawID == 0)
                        {
                            drawID++;
                            HOperatorSet.PaintXld(ho_ModelTrans, DesObject, out DesObject, (((new HTuple(100)).TupleConcat(255)).TupleConcat(0)));
                        }
                        else
                        {
                            HOperatorSet.PaintXld(ho_ModelTrans, DesObject, out DesObject, (((new HTuple(100)).TupleConcat(255)).TupleConcat(0)));
                        }
                    }

                    Result aR = new Result();
                    aR.cX = hv_Column.TupleSelect(hv_I);
                    aR.cY = hv_Row.TupleSelect(hv_I);
                    aR.Phi = hv_Angle.TupleSelect(hv_I);
                    aR.isOk = true;
                    mResult.Add(aR);

                    //if (HDevWindowStack.IsOpen())
                    //{
                      //  HOperatorSet.DispObj(ho_ModelTrans, HDevWindowStack.GetActive());
                    //}
                }
                if ((int)(new HTuple(hv_Score.TupleLength())) > 0)
                {
                    // ResultSta = true;
                }
                else
                {
                    ResultSta = false;
                    if (drawID == 0)
                    {
                        drawID++;
                       // HOperatorSet.PaintRegion(SearchRoi[i].getRegion(), SrcObject, out DesObject, (((new HTuple(255)).TupleConcat(0)).TupleConcat(0)), new HTuple("margin"));
                        
                    }
                    else
                    {
                        //HOperatorSet.PaintRegion(SearchRoi[i].getRegion(), DesObject, out DesObject, (((new HTuple(255)).TupleConcat(0)).TupleConcat(0)), new HTuple("margin"));
                       
                    }
                   
                }
            }
           
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            FindAnisoShapeMode aFun = CoverLib.JsonCover.DeStrLSerialize<FindAnisoShapeMode>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<FindAnisoShapeMode>(this);
            return aFunPara;
        }

        public int create_model()
        {
            if (hv_ModelID != null)
            {
                HOperatorSet.ClearShapeModel(hv_ModelID);
            }
            if (ModelRoi==null || ModelRoi.Count == 0)
            {
                return -1;
                
            }
            HObject RecRoi, ho_ImageReduced;
            HRegion hReg = ModelRoi[0].getRegion();
            // HOperatorSet.GenRectangle2(out RecRoi, hReg.Row, hReg.Column, ModelRoi[0].Phi, ModelRoi[0].Length1, ModelRoi[0].Length2);
            //  HOperatorSet.ReduceDomain(ho_Image, hReg, out ho_ImageReduced);
            HOperatorSet.ReduceDomain(TempImage, hReg, out  ho_ImageReduced);
            HTuple channels=0;
            HOperatorSet.CountChannels(ho_ImageReduced, out channels);
            if (channels == 3)
            {
                HOperatorSet.Rgb1ToGray(ho_ImageReduced, out ho_ImageReduced);
            }
            HOperatorSet.CreateAnisoShapeModel(ho_ImageReduced, numLevels, (new HTuple(angleStart)).TupleRad()
                        , (new HTuple(angleExtent)).TupleRad(), "auto", scaleRMin, scaleRMax, "auto", scaleCMin, scaleCMax, "auto", "none", "ignore_global_polarity",
                        contrast, minContrast, out hv_ModelID);

            HObject ho_ModelTrans;
            HTuple hv_Area = null, hv_RowRef = null;
            HTuple hv_ColumnRef = null, hv_HomMat2D = null, hv_Row = null;
            HTuple hv_Column = null, hv_Angle = null, hv_Scale = null;
            HOperatorSet.GenEmptyObj(out ho_Model);
            HOperatorSet.GenEmptyObj(out ho_ModelTrans);
            HOperatorSet.GetShapeModelContours(out ho_Model, hv_ModelID, 1);
            HOperatorSet.AreaCenter(ho_ModelTrans, out hv_Area, out hv_RowRef, out hv_ColumnRef);
            //HOperatorSet.VectorAngleToRigid(0, 0, 0, hv_RowRef, hv_ColumnRef, 0, out hv_HomMat2D);
            HOperatorSet.VectorAngleToRigid(0, 0, 0, ModelRoi[0].MidR, ModelRoi[0].MidC, 0, out hv_HomMat2D);
            ho_ModelTrans.Dispose();
            HOperatorSet.AffineTransContourXld(ho_Model, out ho_ModelTrans, hv_HomMat2D);

           // HOperatorSet.PaintXld(ho_ModelTrans, TempImage, out TempDesImage, (((new HTuple(0)).TupleConcat(255)).TupleConcat(0)));
            if (ViewPort!=null)
            {
                HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                HOperatorSet.DispXld(ho_ModelTrans, ViewPort.HalconWindow);     
            }
           // HOperatorSet
            return 0;
        }
    }
}
