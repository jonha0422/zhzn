﻿namespace ZHToolBox.PropertyGridRIOCollectorUI
{
    partial class frm_TempleteResourceEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtRIO = new System.Windows.Forms.TextBox();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.btmLoadFile = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btmCreatedByRIO = new System.Windows.Forms.RadioButton();
            this.btmCreateByFile = new System.Windows.Forms.RadioButton();
            this.btmSaveRIO = new System.Windows.Forms.Button();
            this.chkUseDir = new System.Windows.Forms.CheckBox();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1006, 622);
            this.panel1.TabIndex = 3;
            this.panel1.Resize += new System.EventHandler(this.panel1_Resize);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.chkUseDir);
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Location = new System.Drawing.Point(0, 629);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1006, 101);
            this.panel3.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btmSaveRIO);
            this.groupBox2.Controls.Add(this.txtRIO);
            this.groupBox2.Controls.Add(this.txtFileName);
            this.groupBox2.Controls.Add(this.btmLoadFile);
            this.groupBox2.Location = new System.Drawing.Point(232, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(747, 92);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "模版资源";
            // 
            // txtRIO
            // 
            this.txtRIO.Enabled = false;
            this.txtRIO.Location = new System.Drawing.Point(7, 64);
            this.txtRIO.Name = "txtRIO";
            this.txtRIO.Size = new System.Drawing.Size(590, 23);
            this.txtRIO.TabIndex = 15;
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(7, 23);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(590, 23);
            this.txtFileName.TabIndex = 14;
            // 
            // btmLoadFile
            // 
            this.btmLoadFile.Location = new System.Drawing.Point(603, 16);
            this.btmLoadFile.Name = "btmLoadFile";
            this.btmLoadFile.Size = new System.Drawing.Size(138, 36);
            this.btmLoadFile.TabIndex = 13;
            this.btmLoadFile.Text = "选择文件";
            this.btmLoadFile.UseVisualStyleBackColor = true;
            this.btmLoadFile.Click += new System.EventHandler(this.btmLoadFile_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btmCreatedByRIO);
            this.groupBox1.Controls.Add(this.btmCreateByFile);
            this.groupBox1.Location = new System.Drawing.Point(10, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(216, 70);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "模版";
            // 
            // btmCreatedByRIO
            // 
            this.btmCreatedByRIO.AutoSize = true;
            this.btmCreatedByRIO.Location = new System.Drawing.Point(7, 42);
            this.btmCreatedByRIO.Name = "btmCreatedByRIO";
            this.btmCreatedByRIO.Size = new System.Drawing.Size(202, 21);
            this.btmCreatedByRIO.TabIndex = 1;
            this.btmCreatedByRIO.TabStop = true;
            this.btmCreatedByRIO.Text = "从图像窗口(同要需要保存到文件)";
            this.btmCreatedByRIO.UseVisualStyleBackColor = true;
            this.btmCreatedByRIO.CheckedChanged += new System.EventHandler(this.btmCreatedByRIO_CheckedChanged);
            // 
            // btmCreateByFile
            // 
            this.btmCreateByFile.AutoSize = true;
            this.btmCreateByFile.Location = new System.Drawing.Point(7, 20);
            this.btmCreateByFile.Name = "btmCreateByFile";
            this.btmCreateByFile.Size = new System.Drawing.Size(86, 21);
            this.btmCreateByFile.TabIndex = 0;
            this.btmCreateByFile.TabStop = true;
            this.btmCreateByFile.Text = "从文件创建";
            this.btmCreateByFile.UseVisualStyleBackColor = true;
            this.btmCreateByFile.CheckedChanged += new System.EventHandler(this.btmCreateByFile_CheckedChanged);
            // 
            // btmSaveRIO
            // 
            this.btmSaveRIO.Enabled = false;
            this.btmSaveRIO.Location = new System.Drawing.Point(603, 58);
            this.btmSaveRIO.Name = "btmSaveRIO";
            this.btmSaveRIO.Size = new System.Drawing.Size(138, 33);
            this.btmSaveRIO.TabIndex = 16;
            this.btmSaveRIO.Text = "保存RIO到文件";
            this.btmSaveRIO.UseVisualStyleBackColor = true;
            this.btmSaveRIO.Click += new System.EventHandler(this.btmSaveRIO_Click);
            // 
            // chkUseDir
            // 
            this.chkUseDir.AutoSize = true;
            this.chkUseDir.Checked = true;
            this.chkUseDir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUseDir.Location = new System.Drawing.Point(17, 74);
            this.chkUseDir.Name = "chkUseDir";
            this.chkUseDir.Size = new System.Drawing.Size(135, 21);
            this.chkUseDir.TabIndex = 15;
            this.chkUseDir.Text = "文件名使用相对路径";
            this.chkUseDir.UseVisualStyleBackColor = true;
            // 
            // frm_TempleteResourceEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_TempleteResourceEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_RIOCollectorUI";
            this.MaximumSizeChanged += new System.EventHandler(this.frm_RIOCollectorUI_MaximumSizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_RIOCollectorUI_FormClosing);
            this.Load += new System.EventHandler(this.frm_RIOCollectorUI_Load);
            this.ResizeEnd += new System.EventHandler(this.frm_RIOCollectorUI_ResizeEnd);
            this.SizeChanged += new System.EventHandler(this.frm_RIOCollectorUI_SizeChanged);
            this.Resize += new System.EventHandler(this.frm_RIOCollectorUI_Resize);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btmLoadFile;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtRIO;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton btmCreatedByRIO;
        private System.Windows.Forms.RadioButton btmCreateByFile;
        private System.Windows.Forms.Button btmSaveRIO;
        private System.Windows.Forms.CheckBox chkUseDir;
    }
}