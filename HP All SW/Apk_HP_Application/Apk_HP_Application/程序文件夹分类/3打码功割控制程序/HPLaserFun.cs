﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application.程序文件夹分类._3打码功割控制程序
{
    class HPLaserFun
    {
        public static IntPtr hHandleECI0064;
        public static IntPtr hHandleECI2406;
        public static object ObjMove = new object();

        public static bool bLaserMachineIsStarted = false;

        public static bool EnableRotate = false;  //设备是否启用翻转工位
        public static bool AutoRotate = false;   //是否自动翻转

        public static bool bAllowAcceptPCB_Load = false;   //上料位允许接料
        public static bool bAcceptPCBCompleted_Load = false;//上料接料完成

        public static bool bAllowAcceptPCB_LaserMark = false; //打码允许接料
        public static bool bAcceptPCBCompleted_LaserMark = false;//打码接料完成

        public static bool bAllowAcceptPCB_LaserCuuting = false; //切割位允许接料
        public static bool bAcceptPCBCompleted_LaserCuuting = false;//切割接料完成

        public static bool bAllowAcceptPCB_Unload = false; //下料位允许接料(后端)
        public static bool bAcceptPCBCompleted_Unload = false;  //下料接料完成(后端)

        public static object ObjRotate= new object();

        public static HslCommunication.ModBus.ModbusTcpServer modebusTCPServer = new HslCommunication.ModBus.ModbusTcpServer();
        public static PLC_Panasonic PLC_Unload = new PLC_Panasonic();

        public static bool bIsPLCUnloadConnected = false;

        // 允许送板信号(本机PLC)   1014
        // 送板完成信号(本机PLC)   1015

        public static bool bIsUnloadAllow = false; //允许送板信号(下位机PLC)  1016
        public static bool bisUnloadCompleted = false;//送板完成信号(下位机PLC)  1017

        public static Queue<HPglobal.MessageOut> m_queLoad = new Queue<HPglobal.MessageOut>();
        public static Queue<HPglobal.MessageOut> m_queLaserMark = new Queue<HPglobal.MessageOut>();
        public static Queue<HPglobal.MessageOut> m_queRotate = new Queue<HPglobal.MessageOut>();
        public static Queue<HPglobal.MessageOut> m_queLaserCutting = new Queue<HPglobal.MessageOut>();

        public static bool InitializeHardware()
        {
            bool bResult = true;
            try
            {
                #region>>>>>>>> 初始化运动制卡
                Int32 dwResult1 = zmcaux.ZAux_OpenEth(HPglobal.strIPAddress_打码切割ECI0064, out hHandleECI0064);
                Int32 dwResult2 = zmcaux.ZAux_OpenEth(HPglobal.strIPAddress_打码切割ECI2406, out hHandleECI2406);

                if (dwResult1  != 0 || dwResult2 != 0)
                {
                    HPglobal.RecordeAllMessage("打码切割位初始化运动控制卡失败", true, false, "");
                    bResult = false;
                }

                //停止所有轴运动
                zmcaux.ZAux_Direct_Rapidstop(hHandleECI2406, 2);

                //设定脉冲+方向的控制模式
                zmcaux.ZAux_Direct_SetAtype(hHandleECI2406, 0, 1);
                zmcaux.ZAux_Direct_SetAtype(hHandleECI2406, 1, 1);

                //设定脉冲高低电平方向
                zmcaux.ZAux_Direct_SetInvertStep(hHandleECI2406, 0, 0);
                zmcaux.ZAux_Direct_SetInvertStep(hHandleECI2406, 1, 0);

                zmcaux.ZAux_Direct_SetFwdIn(hHandleECI2406, 0, 1);
                zmcaux.ZAux_Direct_SetRevIn(hHandleECI2406, 0, 2);
                zmcaux.ZAux_Direct_SetDatumIn(hHandleECI2406, 0, 0);
                zmcaux.ZAux_Direct_SetInvertIn(hHandleECI2406, 1, 1);
                zmcaux.ZAux_Direct_SetInvertIn(hHandleECI2406, 2, 1);

                zmcaux.ZAux_Direct_SetFwdIn(hHandleECI2406, 1, 4);
                zmcaux.ZAux_Direct_SetRevIn(hHandleECI2406, 1, 5);
                zmcaux.ZAux_Direct_SetDatumIn(hHandleECI2406, 1, 3);
                zmcaux.ZAux_Direct_SetInvertIn(hHandleECI2406, 4, 1);
                zmcaux.ZAux_Direct_SetInvertIn(hHandleECI2406, 5, 1);

                //取消轴的报警信号，设置为通用输入
                zmcaux.ZAux_Direct_SetAlmIn(hHandleECI2406, 0, 24);
                zmcaux.ZAux_Direct_SetAlmIn(hHandleECI2406, 1, 25);
                zmcaux.ZAux_Direct_SetInvertIn(hHandleECI2406, 24, 1);
                zmcaux.ZAux_Direct_SetInvertIn(hHandleECI2406, 25, 1);

                #endregion

                #region>>>>>>>> 初始化Modbus Server
                modebusTCPServer.Port = 5000;
                modebusTCPServer.ServerStart(modebusTCPServer.Port);
                if (!modebusTCPServer.IsStarted)
                {
                    HPglobal.RecordeAllMessage("打码切割位启动Modbus Server失败", true, false, "");
                    bResult = false;
                }
                #endregion

                #region>>>>>>>> 连接组盘PLC
                bIsPLCUnloadConnected = true;
                OperateResult result = PLC_Unload.ConnectPLC(HPglobal.strIPAddress_下料PLC, 5002);
                if (!result.IsSuccess)
                {
                    bIsPLCUnloadConnected = false;
                    HPglobal.RecordeAllMessage("打码切割位连接组盘PLC失败", true, false, "");
                    bResult = false;
                }
                #endregion

                #region>>>>>>>> 启动数据监控
                Thread threadIOMonitor = new Thread(new ParameterizedThreadStart(thread_ForIOMonitor));
                threadIOMonitor.Start(null);
                #endregion

                return bResult;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("LaserStation InitializeHardware() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public static void thread_ForIOMonitor(object wsarg)
        {
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(250);
                try
                {
                    OperateResult<short[]> values = PLC_Unload.ReadInt16("D1016", 2);
                    if(values.IsSuccess)
                    {
                        bIsPLCUnloadConnected = true;
                        bIsUnloadAllow = (values.Content[0] == 1 ? true : false);
                        bisUnloadCompleted = (values.Content[1] == 1 ? true : false);
                    }
                    else
                    {
                        bIsPLCUnloadConnected = false;
                        bIsUnloadAllow = false;
                        bisUnloadCompleted = false;
                        PLC_Unload.ConnectPLC(HPglobal.strIPAddress_下料PLC, 5002);
                        PLC_Unload.SetPersistentConnection();
                    }
                }
                catch(Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_ForIOMonitor  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

                }
            }
        }

        //所有气缸电磁阀控制函数
        public static bool Cylinder_Control(int magneticOrgLine, int magneticOrgControl, int WaitSignalLine, bool statusSetCtrlPos, int ControlLevel = 1, int WaitLineLevel = 1, bool bWaitCompleted = true)
        {
            DateTime time1, time2;
            UInt32 off = (ControlLevel == 0 ? (UInt32)1 : (UInt32)0);
            UInt32 on = (ControlLevel == 0 ? (UInt32)0 : (UInt32)1);

            try
            {
                RE_DO:
                #region>>>>>控制电磁阀
                //单线圈电磁阀控制
                if (magneticOrgControl == -1)
                {
                    if (statusSetCtrlPos)
                        zmcaux.ZAux_Direct_SetOp(hHandleECI0064, magneticOrgLine, on);
                    else
                        zmcaux.ZAux_Direct_SetOp(hHandleECI0064, magneticOrgLine, off);
                }
                //双线圈电磁阀控制
                else
                {
                    if (statusSetCtrlPos)
                    {
                        zmcaux.ZAux_Direct_SetOp(hHandleECI0064, magneticOrgLine, off);
                        zmcaux.ZAux_Direct_SetOp(hHandleECI0064, magneticOrgControl, on);
                    }
                    else
                    {
                        zmcaux.ZAux_Direct_SetOp(hHandleECI0064, magneticOrgLine, on);
                        zmcaux.ZAux_Direct_SetOp(hHandleECI0064, magneticOrgControl, off);
                    }
                }
                #endregion

                #region>>>>>等待输入
                if (bWaitCompleted && WaitSignalLine >= 0)
                {
                    Thread.Sleep(300);

                    //等待感应器到位
                    time1 = time2 = DateTime.Now;
                    while (true)
                    {
                        Thread.Sleep(100);
                        if (HPglobal.m_bQuitApp)
                            return false;

                        uint nValue = 0;
                        uint WaitLevel = 1;
                        zmcaux.ZAux_Direct_GetIn(hHandleECI0064, WaitSignalLine, ref nValue);
                        if (WaitLevel == nValue)
                            return true;

                        time2 = DateTime.Now;
                        TimeSpan span = time2 - time1;
                        if (span.TotalMilliseconds > 5000)
                        {
                            string errorMsg = $"气缸控制失败！\r\n电磁阀1输出: {magneticOrgLine} ; \r\n电磁阀2输出 {magneticOrgControl}; \r\n输出状态: {statusSetCtrlPos}\r\n等待输入信号: {WaitSignalLine}";
                            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                            emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                            if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                                goto RE_DO;
                            else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                                return false;
                        }
                    }
                }
                else
                    return true;
                #endregion
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Cylinder_Control() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public static bool IsLoadPosHasTray() //上料待料载板有无检测
        {
            try
            {
                uint nValue = 0;
                zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI0064, 0, ref nValue);
                return (nValue == 0 ? false : true);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("IsLoadHasTray() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        } 
        
        public static bool IsLaserMarkingPosHasTray()//打码工位载板到位检测
        {
            try
            {
                uint nValue = 0;
                zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI0064, 3, ref nValue);
                return (nValue == 0 ? false : true);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("IsLoadHasTray() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }   

        public static bool IsRotatePosHasTray() //翻转工位载板有无检测
        {
            try
            {
                uint nValue = 0;
                zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI0064, 8, ref nValue);
                return (nValue == 0 ? false : true);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("IsLoadHasTray() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool IsXYFeedingHasTray()//XY平台皮带载板进料感应
        {
            try
            {
                uint nValue = 0;
                zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI0064, 17, ref nValue);
                return (nValue == 0 ? false : true);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("IsLoadHasTray() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }  

        public static bool IsXYPosHasTray()//XY平台皮带载板到位感应
        {
            try
            {
                uint nValue = 0;
                zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI0064, 18, ref nValue); 
                return (nValue == 0 ? false : true);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("IsLoadHasTray() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }  

        public static bool Action_LoadLineControl(bool onOrOff) //上料皮带线电机正转控制
        {
            try
            {
                int magneticOrgLine = 16;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;
                if (onOrOff)
                    statusSetCtrlPos = true;
                else
                    statusSetCtrlPos = false;
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_LoadLineControl() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }

        }

        public static bool Action_LaserCuttingLineControl(bool onOrOff) //切割皮带线电机正转控制
        {
            try
            {
                int magneticOrgLine = 17;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;

                if (onOrOff)
                    statusSetCtrlPos = true;
                else
                    statusSetCtrlPos = false;
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_LaserCuttingLineControl() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }

        }    

        public static bool Action_RotateLineControl(bool onOrOff) //回流皮带线电机正转控制
        {
            try
            {
                int magneticOrgLine = 18;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;

                if (onOrOff)
                    statusSetCtrlPos = true;
                else
                    statusSetCtrlPos = false;
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_RotateLineControl() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }

        }

        public static bool Action_Load_CylinderStop(bool onOrOff) //待料阻档气缸
        {
            try
            {
                int magneticOrgLine = 0;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 0;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 2;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 1;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_Load_CylinderStop() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }

        }

        public static bool Action_LaserMark_CylinderStop(bool onOrOff) //打码阻档气缸
        {
            try
            {
                int magneticOrgLine = 1;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 0;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 5;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 4;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_LaserMark_CylinderStop() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_LaserMark_CylinderUpDown(bool onOrOff) //打码顶升气缸
        {
            try
            {
                int magneticOrgLine = 2;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 7;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 6;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_LaserMark_CylinderUpDown() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_Rotate_CylinderStop(bool onOrOff) //翻转阻挡气缸
        {
            try
            {
                int magneticOrgLine = 3;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 0;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 10;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 9;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_Rotate_CylinderStop() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_Rotate_CylinderUpDown(bool onOrOff) //翻转顶升气缸
        {
            try
            {
                int magneticOrgLine = 4;
                int magneticOrgControl = 5;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 11;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 12;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_Rotate_CylinderUpDown() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_Rotate_CylinderRotate(bool onOrOff) //翻转旋转气缸
        {
            try
            {
                int magneticOrgLine = 6;
                int magneticOrgControl = 7;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 14;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 13;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_Rotate_CylinderRotate() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_Rotate_CylinderOpenClose(bool onOrOff) //翻转夹紧气缸
        {
            try
            {
                int magneticOrgLine = 8;
                int magneticOrgControl = 9;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 16;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 15;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_Rotate_CylinderOpenClose() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_LaserCutting_CylinderStop(bool onOrOff) //切割阻挡气缸
        {
            try
            {
                int magneticOrgLine = 10;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 0;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 20;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 19;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_LaserCutting_CylinderStop() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_LaserCutting_CylinderUpDown(bool onOrOff) //切割顶升气缸
        {
            try
            {
                int magneticOrgLine = 11;
                int magneticOrgControl = -1;
                int WaitSignalLine = -1;
                bool statusSetCtrlPos = true;
                int ControlLevel = 1;
                int WaitLineLevel = 1;

                if (onOrOff)
                {
                    WaitSignalLine = 22;
                    statusSetCtrlPos = true;
                }
                else
                {
                    WaitSignalLine = 21;
                    statusSetCtrlPos = false;
                }
                return Cylinder_Control(magneticOrgLine, magneticOrgControl, WaitSignalLine, statusSetCtrlPos, ControlLevel, WaitLineLevel);

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_LaserCutting_CylinderUpDown() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static bool Action_RotatePCB(bool bDown) //切割顶升气缸
        {
            try
            {
                lock(ObjRotate)
                {
                RE_DO:
                    //夹紧
                    Action_Rotate_CylinderOpenClose(true);
                    Thread.Sleep(500);

                    //上升
                    Action_Rotate_CylinderUpDown(true);
                    Thread.Sleep(500);

                    //旋转
                    uint nValue13 = 0;
                    uint nValue14 = 0;
                    zmcaux.ZAux_Direct_GetIn(hHandleECI0064, 13, ref nValue13);
                    zmcaux.ZAux_Direct_GetIn(hHandleECI0064, 14, ref nValue14);
                    if (nValue13 == 1 && nValue14 == 0)
                        Action_Rotate_CylinderRotate(true);
                    else if (nValue13 == 0 && nValue14 == 1)
                        Action_Rotate_CylinderRotate(false);
                    else
                    {
                        string errorMsg = $"旋转气缸控制失败！\r\n无法读取旋转气缸两个传感的状态或信号不一致!";
                        emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                            goto RE_DO;
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                            return false;
                    }

                    //下降
                    Action_Rotate_CylinderUpDown(false);

                    //松开
                    Action_Rotate_CylinderOpenClose(false);

                    return true;
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Action_LaserCutting_CylinderUpDown() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                return false;
            }
        }

        public static void QuitApplicationAndClear()
        {

            #region>>>>>>>>>停止所有服务及关闭仪器
         
            #endregion

            #region>>>>>>>>>释放全局变量及对话框

            #endregion
        }
    }
}
