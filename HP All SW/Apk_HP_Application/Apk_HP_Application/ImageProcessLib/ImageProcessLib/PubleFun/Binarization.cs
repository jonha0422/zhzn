﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessLib.PubleFun
{
    public class Binarization
    {
        private static Binarization instance;
        private static object theLock = new object();
        public static Binarization Instance
        {
            get
            {
                lock (theLock)
                {
                    if (instance == null)
                    {
                        instance = new Binarization();
                    }
                    return instance;
                }
            }
        }
       
    }
}
