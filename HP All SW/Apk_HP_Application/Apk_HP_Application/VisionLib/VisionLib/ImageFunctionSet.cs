﻿using CamaeraLib;
using HalconDotNet;
using InteractiveROI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisionLib
{
    public partial class ImageFunctionSet : Form
    {
        public int curCamID=0;
        public int curFunID = 0;
        public int curWordSplaceID = 0;

        ROIController roiController;
        public ImageFunctionSet()
        {
            InitializeComponent();
        }
        private void showResult()
        {
            ResultpropertyGrid.SelectedObject = null;

            if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].mResult.Count > 0)
            {
                int id = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].mResult.Count - 1;
                ResultpropertyGrid.SelectedObject = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].mResult;
            }
           
        }
        private void ImageFunctionSet_Load(object sender, EventArgs e)
        {
            //tabControl1.Height = tabControl1.Parent.Height;
            ucCameraView1.camID = curCamID;
            init();
            upDataListView();
          
        }
        private void init()
        {
            toolStrip1.Height = toolStrip1.Parent.Height;
            propertyGrid1.SelectedObject = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID];
            showResult();

            if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SearchRoiCount > 1)
            {
                tsSearchRoi0.Visible = false;
                tsSearchRoi1.Visible = true;
            }
            else
            {
                tsSearchRoi0.Visible = true;
                tsSearchRoi1.Visible = false;
            }

            if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoiCount > 1)
            {
                tsFindRoi0.Visible = false;
                tsFindRoi1.Visible = true;
            }
            else if(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoiCount == 1)
            {
                tsFindRoi0.Visible = true;
                tsFindRoi1.Visible = false;
            }
            else if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoiCount == 0)
            {
                tsFindRoi0.Visible = false;
                tsFindRoi1.Visible = false;
            }
            if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].isEpson == true)
            {
                bStartPos.Visible = true;
                bEpsonMark.Visible = true;
            }else
            {
                bStartPos.Visible = false;
                bEpsonMark.Visible = false;
            }
            ucCameraView1.Init();
            if (roiController != null)
            {
                roiController.reset();
            }
            
            roiController = new ROIController();
            roiController.ROIRec2List = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SearchRoi;
            roiController.Read();

            if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage != null)
            {
                ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage);
            }


            ucCameraView1.viewController.useROIController(roiController);
            ucCameraView1.viewController.repaint();

            VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ViewPort = ucCameraView1.viewPort;
        }
        private void upDataListView()
        {
            for (int i = 0; i < VisionSystem.Instance.WordSpaceList.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(VisionSystem.Instance.WordSpaceList[i].wsName);
                listView1.Items.Add(lvi);
            }
        }

        private void bCameraPara_Click(object sender, EventArgs e)
        {
            try
            {
                CamaeraLib.CameraParaFrm aFrm = new CameraParaFrm();
                aFrm.CamID = this.curCamID;
                aFrm.Show();
            }
            catch(Exception ex)
            {

            }

        }

        private void bTempImage_Click(object sender, EventArgs e)
        {
            int suc=CamaeraLib.CameraSystem.Instance.CameraList[curCamID].OneShot();
            if (suc == 0)
            {
                CamaeraLib.Image aImage =CamaeraLib.CameraSystem.Instance.CameraList[curCamID].GetLatestImage();
                HImage hImage;
                suc =ImageConvert.ImageTohImage(aImage,out hImage);
                if (suc == 0){
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage = hImage;

                    ucCameraView1.viewController.clearList();
                    ucCameraView1.viewController.addIconicVar(hImage);
                    ucCameraView1.viewController.repaint();
                }
            }
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            VisionSystem.Instance.Writer();
        }

        private void tsSearchRoi_Click(object sender, EventArgs e)
        {
            roiController = new ROIController();
            roiController.ROIRec2List = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SearchRoi;
            roiController.Read();
            ucCameraView1.viewController.useROIController(roiController);
            if (roiController.ROIList.Count == 0)
            {
                roiController.setROIShape(new ROIRectangle2());
            }
            else
            {
                roiController.setActiveROIIdx(0);
            }
            
            roiController.mode = ROI_Mode.SearchROI;
            ucCameraView1.viewController.repaint();
        }

        private void ucCameraView1_DoubleClick(object sender, EventArgs e)
        {
            
        }

        

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
           
            
        }

        private void tsFindRoi_Click(object sender, EventArgs e)
        {
            roiController = new ROIController();
            roiController.ROIRec2List = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoi;
            roiController.Read();
            ucCameraView1.viewController.useROIController(roiController);
            if (roiController.ROIList.Count == 0)
            {
                roiController.setROIShape(new ROIRectangle2());
            }
            else
            {
                roiController.setActiveROIIdx(0);
            }

            roiController.mode = ROI_Mode.ModelRoi;
            roiController.setDrawColor("green", "red", "blue");
            ucCameraView1.viewController.repaint();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (roiController.mode == ROI_Mode.SearchROI)
                {
                    roiController.Write();
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SearchRoi = roiController.ROIRec2List;

                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SrcObject = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].TempImage;
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ImageProcess();
                    if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject != null)
                    {
                        ucCameraView1.viewController.clearList();
                        ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject);
                        ucCameraView1.viewController.repaint();
                    }
                }
                else
                {
                    roiController.Write();
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoi = roiController.ROIRec2List;
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].Init(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage);
                    //if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].TempDesImage != null)
                    //{
                    //    ucCameraView1.viewController.clearList();
                    //    ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].TempDesImage);
                    //    ucCameraView1.viewController.repaint();
                    //}
                }

            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOpenImage_Click(object sender, EventArgs e)
        {
            String fileName = "patras";
            //HImage image;
           // viewController.setDispLevel(HWndCtrl.MODE_INCLUDE_ROI);
            //viewController.useROIController(roiController);
           // viewController.setViewState(HWndCtrl.MODE_VIEW_NONE);

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "BMP文件|*.bmp|JPEG文件|*.jpg|TIFF文件|*.tiff";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.FilterIndex = 1;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //DahuaCamera.Close();
                fileName = openFileDialog1.FileName;

                VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage = new HImage(fileName);
                for (int i = 0; i < VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList.Count; i++)
                {
                    //VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].Init(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage);
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].TempImage = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage;
                }

                ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].TempImage);
                ucCameraView1.viewController.repaint();
            }
        }

        private void bThreshold_Click(object sender, EventArgs e)
        {
            if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold == null)
            {
                VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold = new ImageProcess.Threshold();
            }
            VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold.ModelRoi = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoi;
            VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold.SearchRoi = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoi;
            VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold.SrcObject = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].TempImage;

            propertyGrid2.SelectedObject = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold;
        }

        private void propertyGrid2_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold.ImageProcess();
            ucCameraView1.viewController.clearList();
            HOperatorSet.SetColor(ucCameraView1.viewPort.HalconWindow, "red");
            ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].PreThreshold.DesObject);
            ucCameraView1.viewController.repaint();
        }

        private void bPhotograph_Click(object sender, EventArgs e)
        {
            int suc = CamaeraLib.CameraSystem.Instance.CameraList[curCamID].OneShot();
            if (suc == 0)
            {
                CamaeraLib.Image aImage = CamaeraLib.CameraSystem.Instance.CameraList[curCamID].GetLatestImage();
                HImage hImage;
                suc = ImageConvert.ImageTohImage(aImage, out hImage);
                if (suc == 0)
                {
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SrcObject = hImage;

                    ucCameraView1.viewController.clearList();
                    ucCameraView1.viewController.addIconicVar(hImage);
                    ucCameraView1.viewController.repaint();

                    //VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ImageProcess();
                    //if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject != null)
                    //{
                    //    ucCameraView1.m_bShowCallBack = false;
                    //    ucCameraView1.viewController.clearList();
                    //    ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject);
                    //    ucCameraView1.viewController.repaint();
                    //    showResult();
                    //}
                    for (int i = 0; i < VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList.Count; i++)
                    {
                        VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].SrcObject = hImage;
                        VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].ImageProcess();
                        showResult();
                        if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject != null)
                        {

                            ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject);
                            ucCameraView1.viewController.repaint();
                        }

                    }
                }
            }

            
        }

        private void bOpenTest_Click(object sender, EventArgs e)
        {
            try
            {
                String fileName = "patras";
                //HImage image;
                // viewController.setDispLevel(HWndCtrl.MODE_INCLUDE_ROI);
                //viewController.useROIController(roiController);
                // viewController.setViewState(HWndCtrl.MODE_VIEW_NONE);

                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "BMP文件|*.bmp|JPEG文件|*.jpg|TIFF文件|*.tiff";
                openFileDialog1.RestoreDirectory = true;
                openFileDialog1.FilterIndex = 1;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //DahuaCamera.Close();
                    fileName = openFileDialog1.FileName;
                    ucCameraView1.viewController.clearList();
                    VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].SrcImage = new HImage(fileName);
                    ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].SrcImage);
                    ucCameraView1.viewController.repaint();
                    for (int i = 0; i < VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList.Count; i++)
                    {
                        VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].SrcObject = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].SrcImage;
                        VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].ImageProcess();
                        showResult();
                        if (VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject != null)
                        {

                            ucCameraView1.viewController.addIconicVar(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].DesObject);
                            ucCameraView1.viewController.repaint();
                        }

                    }


                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SearchROIAdd_Click(object sender, EventArgs e)
        {
            roiController = new ROIController();
            roiController.ROIRec2List = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SearchRoi;
            roiController.Read();
            ucCameraView1.viewController.useROIController(roiController);
           
            roiController.setROIShape(new ROIRectangle2());
            

            roiController.mode = ROI_Mode.SearchROI;
            ucCameraView1.viewController.repaint();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            roiController = new ROIController();
            roiController.ROIRec2List = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].SearchRoi;
            roiController.Read();
            ucCameraView1.viewController.useROIController(roiController);
            if (roiController.ROIList.Count == 0)
            {
                roiController.setROIShape(new ROIRectangle2());
            }
            else
            {
                roiController.setActiveROIIdx(0);
            }

            roiController.mode = ROI_Mode.SearchROI;
            ucCameraView1.viewController.repaint();
        }

        private void SearchROIDel_Click(object sender, EventArgs e)
        {
            int Idx = roiController.getActiveROIIdx();
            if ( Idx>= 0 && Idx<roiController.ROIRec2List.Count)
            {
                roiController.ROIRec2List.RemoveAt(Idx);
                roiController.Read();
            }
            roiController.setActiveROIIdx(-1);
            ucCameraView1.viewController.useROIController(roiController);
            ucCameraView1.viewController.repaint();

           
        }

        private void bContinuousShot_Click(object sender, EventArgs e)
        {
            if (bContinuousShot.Text == "   连续拍照")
            {
                ucCameraView1.m_bShowCallBack = true;
                CamaeraLib.CameraSystem.Instance.CameraList[curCamID].ContinuousShot();
                bContinuousShot.Text = " 停止   ";
                bContinuousShot.ImageIndex = 9;
                
            }
            else
            {
                CamaeraLib.CameraSystem.Instance.CameraList[curCamID].Stop();
                bContinuousShot.Text = "   连续拍照";
                bContinuousShot.ImageIndex = 8;
                ucCameraView1.m_bShowCallBack = false;
            }
            
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                return;
            }
            curWordSplaceID = listView1.SelectedItems[0].Index;
            curFunID = 0;
            updataFunList();
            init();
        }
        private void updataFunList()
        {
            listView2.Items.Clear();
            for (int i = 0; i < VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].cName);
                listView2.Items.Add(lvi);
            }
        }
        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count == 0)
            {
                return;
            }
            curFunID = listView2.SelectedItems[0].Index;
            init();
        }
        private void ModelROIAdd_Click(object sender, EventArgs e)
        {
            roiController = new ROIController();
            roiController.ROIRec2List = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoi;
            roiController.Read();
            ucCameraView1.viewController.useROIController(roiController);

            roiController.setROIShape(new ROIRectangle2());


            roiController.mode = ROI_Mode.ModelRoi;
            ucCameraView1.viewController.repaint();
        }

        private void ModelROIEdit_Click(object sender, EventArgs e)
        {
            roiController = new ROIController();
            roiController.ROIRec2List = VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].ModelRoi;
            roiController.Read();
            ucCameraView1.viewController.useROIController(roiController);
            if (roiController.ROIList.Count == 0)
            {
                roiController.setROIShape(new ROIRectangle2());
            }
            else
            {
                roiController.setActiveROIIdx(0);
            }

            roiController.mode = ROI_Mode.ModelRoi;
            ucCameraView1.viewController.repaint();
        }

        private void ModelROIDelete_Click(object sender, EventArgs e)
        {
            int Idx = roiController.getActiveROIIdx();
            if (Idx >= 0 && Idx < roiController.ROIRec2List.Count)
            {
                roiController.ROIRec2List.RemoveAt(Idx);
                roiController.Read();
            }
            roiController.setActiveROIIdx(-1);
            ucCameraView1.viewController.useROIController(roiController);
            ucCameraView1.viewController.repaint();
        }

        private void ImageFunctionSet_ResizeEnd(object sender, EventArgs e)
        {
            
        }

        private void ImageFunctionSet_Resize(object sender, EventArgs e)
        {
            tabControl1.Height = panel4.Height - 120;
        }

        private void ImageFunctionSet_FormClosing(object sender, FormClosingEventArgs e)
        {
            ucCameraView1.Dispose();
        }

        private void bEpsonMark_Click(object sender, EventArgs e)
        {
            for(int i=0;i< VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList.Count; i++)
            {
                VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[i].calibration();
            }
        }

        private void bStartPos_Click(object sender, EventArgs e)//
        {
            VisionSystem.Instance.WordSpaceList[curWordSplaceID].CamImageFunList[curCamID].ImageProcessList[curFunID].MoveToStartPos();
        }
    }
}
