﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ThridLibray;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace CamaeraLib.Camera
{
    
    public class CameraDH:CameraBase
    {
        private IDevice m_Camera;
        List<IGrabbedRawData> m_frameList = new List<IGrabbedRawData>();        /* 图像缓存列表 */
        IGrabbedRawData _GrabbedRawData;
        Thread renderThread = null;         /* 显示线程  */
        bool m_bShowLoop = true;            /* 线程控制变量 */
        
        IGrabbedRawData frame;
        public int CamId;
        public string CamName;
        private string m_TriggerSource;
        protected Object m_lockObject;                     /* Lock object used for thread synchronization. */
        private bool bColor;
        public string  PHJ { get; set; }
        private bool isOneShot = false;
        public CameraDH()
        {
            m_lockObject = new Object();
        }
        private int setCamDefPara()
        {
            /* 打开Software Trigger */
            //if (TriggerOpen)
            //{
                m_Camera.TriggerSet.Open(TriggerSourceEnum.Software);//启动软件触发

            //}
            //else
            //{
            //    m_Camera.TriggerSet.Close(); //关闭软件触发
            //}
            using (IEnumParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.AcquisitionMode])
            {

                string i = p.GetValue();
            }
            //* 设置图像格式 
            using (IEnumParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.ImagePixelFormat])
            {
                //p.SetValue("Mono8");
                string ImagePixelFormat = p.GetValue();
                if (ImagePixelFormat != "Mono8")
                {
                    bColor = true;
                }
                else
                {
                    bColor = false;
                }
            }
         
            /* 设置曝光 */
            using (IFloatParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.ExposureTime])
            {
                p.SetValue(_CameraConfig.ExposureTime);
            }

            /* 设置增益 */
            using (IFloatParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.GainRaw])
            {
                p.SetValue(_CameraConfig.GainVal);
            }

            /* 设置缓存个数为8（默认值为16） */
             m_Camera.StreamGrabber.SetBufferCount(2);

            m_Camera.StreamGrabber.GrabStarted += OnGrabStarted;
            /* 注册码流回调事件 */
            m_Camera.StreamGrabber.ImageGrabbed += OnImageGrabbed;

            /* 开启码流 */
            if (!m_Camera.GrabUsingGrabLoopThread())
            {
                return 1;
            }
            return 0;
        }
        public override int OpenCamByID(short id)
        {
            CamId = id;
            try
            {
                /* 设备搜索 */
                List<IDeviceInfo> li = Enumerator.EnumerateDevices();
                if (li.Count > id)
                {
                    /* 获取搜索到的第一个设备 */
                    m_Camera = Enumerator.GetDeviceByIndex(id);

                    /* 注册链接事件 */
                    m_Camera.CameraOpened += OnCameraOpen;
                    m_Camera.ConnectionLost += OnConnectLoss;
                    m_Camera.CameraClosed += OnCameraClose;

                    /* 打开设备 */
                    if (!m_Camera.Open())
                    {
                        return 1;
                    }
                    setCamDefPara();
                    
                }
                else
                {
                    return 2;
                }
            }
            catch (Exception exception)
            {
                Catcher.Show(exception);
            }

            return 0;
        }
        public override int OpenCameraByName(string Name)
        {
            bool isFind = false;
            try
            {
                /* 设备搜索 */
                List<IDeviceInfo> li = Enumerator.EnumerateDevices();
                for(int i=0;i<li.Count;i++)
                {
                    /* 获取搜索到的第一个设备 */
                   
                    m_Camera = Enumerator.GetDeviceByIndex(i);
                    if (m_Camera.DeviceInfo.Name== Name)
                    {
                        isFind = true;
                        CamName = Name;
                        /* 注册链接事件 */
                        m_Camera.CameraOpened += OnCameraOpen;
                        m_Camera.ConnectionLost += OnConnectLoss;
                        m_Camera.CameraClosed += OnCameraClose;

                        /* 打开设备 */
                        if (!m_Camera.Open())
                        {
                            return 1;
                        }

                        setCamDefPara();
                        return 0;
                    }
                   
                }
               
            }
            catch (Exception exception)
            {
                Catcher.Show(exception);
            }
            if (isFind == false)
            {
                return 2;
            }
            return 0;
        }
        public override int Close()
        {
            try
            {
                if (m_Camera == null)
                {
                    return -1;
                }

                m_Camera.StreamGrabber.ImageGrabbed -= OnImageGrabbed;         /* 反注册回调 */
                m_Camera.ShutdownGrab();                                       /* 停止码流 */
                m_Camera.Close();                                              /* 关闭相机 */
            }
            catch (Exception exception)
            {
                Catcher.Show(exception);
                return -1;
            }
            return 0;
        }
        public override int OneShot()
        {
            if (m_Camera == null)
            {
                m_open = false;
                //throw new InvalidOperationException("Device is invalid");
            }

            try
            {
                isOneShot = true;
                m_Camera.ExecuteSoftwareTrigger();
                int chkTime = 0;
                while (isOneShot == true && chkTime < 100)
                {
                    chkTime++;
                    Thread.Sleep(10);
                }
                if (isOneShot == true)
                {
                    return -1;
                }
            }
            catch (Exception exception)
            {
               // Catcher.Show(exception);
                return -1;
            }
            return 0;
        }
        public override int ContinuousShot()
        {
            if (m_open == false)
            {
                return -1;
            }
            m_Camera.ShutdownGrab();
            m_Camera.TriggerSet.Close(); //关闭软件触发
            if (!m_Camera.GrabUsingGrabLoopThread())
            {
                return 1;
            }
            return 0;
        }
        public override int Stop()
        {
            if (m_open == false)
            {
                return -1;
            }
            if (!m_Camera.ShutdownGrab())
            {
                return 1;
            }
            m_Camera.TriggerSet.Open(TriggerSourceEnum.Software);//启动软件触发
            if (!m_Camera.GrabUsingGrabLoopThread())
            {
                return 1;
            }
            return 0;
        }

        public override int SetTrigger(bool Trigger)
        {
            if (m_open == false)
            {
                return -1;
            }
            if (Trigger == true)
            {
                m_Camera.TriggerSet.Open(m_TriggerSource);//启动触发
            }
            else
            {
                m_Camera.TriggerSet.Close();
            }
           
            return 0;
        }
        public override int SetTriggerSource(int TriggerSource)
        {
            if (m_open == false)
            {
                return -1;
            }
            _CameraConfig.TriggerSource = TriggerSource;
            if (TriggerSource == 1)
            {
                m_TriggerSource = TriggerSourceEnum.Line1;
            }else if (TriggerSource == 2)
            {
                m_TriggerSource = TriggerSourceEnum.Line2;
            }else
            {
                m_TriggerSource = TriggerSourceEnum.Software;
            }
            //if (_CameraConfig.Trigger)
            //{
            //    m_Camera.TriggerSet.Open(m_TriggerSource);//启动触发
            //}else
            //{
            //    m_Camera.TriggerSet.Close();
            //}
            
            return 0;
        }

        public override int SetExposureTime(int ExposureTime)
        {
            if (m_open == false)
            {
                return -1;
            }
            using (IFloatParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.ExposureTime])
            {
                p.SetValue(ExposureTime);
            }
            return 0;
        }
        public override int getExposureTime(out int ExposureTime)
        {
            if (m_open == false)
            {
                ExposureTime = 0;
                return -1;
            }
            using (IFloatParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.ExposureTime])
            {
                ExposureTime =(int)p.GetValue();
            }
            return 0;
        }
        public override int SetGainVal(int GainVal)
        {
            if (m_open == false)
            {
                return -1;
            }
            using (IFloatParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.GainRaw])
            {
                p.SetValue(GainVal);
            }
            return 0;
        }
        public override int GetGainVal(out int GainVal)
        {
            if (m_open == false)
            {
                GainVal = 0;
                return -1;
            }
            using (IFloatParameter p = m_Camera.ParameterCollection[ParametrizeNameSet.GainRaw])
            {
                GainVal = (int)p.GetValue();
            }
            return 0;
        }
        public bool TriggerOpen
        {
            get
            {
                return _CameraConfig.Trigger;
            }

            set
            {
                _CameraConfig.Trigger = value;
                if (_CameraConfig.Trigger)
                {
                    m_Camera.TriggerSet.Open(m_TriggerSource);//启动触发
                }
                else
                {
                    m_Camera.TriggerSet.Close();
                }
            }
        }
       
        
        /* 相机打开回调 */
        private void OnCameraOpen(object sender, EventArgs e)
        {
            base.OnDeviceOpenedEvent();

        }

        /* 相机关闭回调 */
        private void OnCameraClose(object sender, EventArgs e)
        {
            base.OnDeviceClosedEvent();

        }

        /* 相机丢失回调 */
        private void OnConnectLoss(object sender, EventArgs e)
        {
            m_Camera.ShutdownGrab();
            m_Camera.Dispose();
            m_Camera = null;

            base.OnDeviceClosedEvent();

        }
        private void OnGrabStarted(Object sender, EventArgs e)
        {
            base.OnGrabbingStartedEvent();

        }
        /* 码流数据回调 */
        private void OnImageGrabbed(Object sender, GrabbedEventArgs e)
        {
            isOneShot = false;
             m_mutex.WaitOne();
            _GrabbedRawData = e.GrabResult.Clone();
            lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
            {
                m_frameList.Add(_GrabbedRawData);
                if (m_frameList.Count > 5)
                {
                    m_frameList.RemoveAt(0);
                }
            }
            m_mutex.ReleaseMutex();
            base.OnImageReadyEvent();

        }
        public override Image GetLatestImage()
        {
            
            lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
            {
                /* Release all images but the latest. */
                while (m_frameList.Count > 1)
                {
                    m_frameList.RemoveAt(0);
                }
                if (m_frameList.Count > 0) /* If images available. */
                {
                    if (bColor == true)
                    {
                        int nRGB=RGBFactory.EncodeLen(m_frameList[0].Width, m_frameList[0].Height,true);
                        IntPtr pData=Marshal.AllocHGlobal(nRGB);
                        RGBFactory.ToRGB(m_frameList[0].Image, m_frameList[0].Width, m_frameList[0].Height, true, m_frameList[0].PixelFmt, pData, nRGB);
                        byte[] bData = new byte[nRGB];
                        Marshal.Copy(pData, bData, 0, nRGB);
                        Image mImage = new Image(m_frameList[0].Width, m_frameList[0].Height, bData, true);
                        //Marshal.Release(pData);
                        Marshal.FreeHGlobal(pData);
                        /* 主动调用回收垃圾 */
                        GC.Collect();
                        return mImage;
                    }
                    else
                    {
                        Image mImage = new Image(m_frameList[0].Width, m_frameList[0].Height, m_frameList[0].Image, false);
                        return mImage;
                    }
                    m_frameList.Clear();
                }
            }
            return null; /* No image available. */
        }
        public override Bitmap GetLatestBitmap()
        {

            lock (m_lockObject) /* Lock the grab result queue to avoid that two threads modify the same data. */
            {
                /* Release all images but the latest. */
                while (m_frameList.Count > 1)
                {
                    m_frameList.RemoveAt(0);
                }
                if (m_frameList.Count > 0) /* If images available. */
                {
                   
                    return m_frameList[0].ToBitmap(true);
                }
            }
            return null; /* No image available. */
        }

        /* 转码显示线程 */
        private void ShowThread()
        {
            while (m_bShowLoop)
            {
                if (m_frameList.Count == 0)
                {
                    Thread.Sleep(10);
                    continue;
                }

                /* 图像队列取最新帧 */
                m_mutex.WaitOne();
                Image frame = new Image(m_frameList[0].Width, m_frameList[0].Height, m_frameList[0].Image, false);
                m_frameList.Clear();
                m_mutex.ReleaseMutex();

                /* 主动调用回收垃圾 */
                GC.Collect();

                /* 控制显示最高帧率为25FPS */
                if (false == isTimeToDisplay())
                {
                    continue;
                }

                try
                {
                    /* 图像转码成bitmap图像 */
                    Bitmap bitmap = frame.ToBitmap();
                    m_bShowByGDI = true;
                    Control ff ;
                    if (m_bShowByGDI)
                    {
                        /* 使用GDI绘图 */
                        double w = _g.ClipBounds.Width;
                        double h = _g.ClipBounds.Height;
                        if (m_scale == 0)
                        {
                            double n = Math.Max((double)frame.Width / w, (double)frame.Height / h);
                            m_scale = 1 / n;
                        }
                        w = (long)(frame.Width / m_scale);
                        h = (long)(frame.Height / m_scale);

                        _g.DrawImage(bitmap, new Rectangle(0, 0, (int)w, (int)h),
                        new Rectangle(0, 0, bitmap.Width, bitmap.Height), GraphicsUnit.Pixel);
                        bitmap.Dispose();
                    }
                    
                }
                catch (Exception exception)
                {
                    Catcher.Show(exception);
                }
            }
        }

        const int DEFAULT_INTERVAL = 40;
        Stopwatch m_stopWatch = new Stopwatch();    /* 时间统计器 */

        /* 判断是否应该做显示操作 */
        private bool isTimeToDisplay()
        {
            m_stopWatch.Stop();
            long m_lDisplayInterval = m_stopWatch.ElapsedMilliseconds;
            if (m_lDisplayInterval <= DEFAULT_INTERVAL)
            {
                m_stopWatch.Start();
                return false;
            }
            else
            {
                m_stopWatch.Reset();
                m_stopWatch.Start();
                return true;
            }
        }

    }
}
