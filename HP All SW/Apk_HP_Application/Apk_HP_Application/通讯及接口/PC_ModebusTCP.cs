﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslCommunication.Profinet.Omron;
using HslCommunication;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace CommunicationZH
{
    public class PC_ModeBusNet : HslCommunication.ModBus.ModbusTcpNet
    {
        public bool isConnected = false;
        public PC_ModeBusNet() : base()
        {
        }

        public OperateResult ConnectModebusTCPServer(string ip =  null, int port = 0, byte station = 0x02)
        {
            try
            {
                if (ip != null)
                    IpAddress = ip;

                if (port != 0)
                    Port = port;

                Station = station;
                base.ConnectTimeOut = 3000;
                base.ReceiveTimeOut = 3000;

                OperateResult result = base.ConnectServer();
                if (result != null && result.IsSuccess)
                    isConnected = (result.IsSuccess ? true : false);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public override OperateResult<byte[]> Read(string address, ushort length)
        {
            try
            {
                return base.Read(address, length);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public override string ToString()
        {
            try
            {
                return base.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public override OperateResult Write(string address, byte[] value)
        {
            try
            {
                return base.Write(address, value);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public OperateResult Write(string address, bool value)
        {
            try
            {
                return base.Write(address, value ? 1 : 0);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public new OperateResult<bool> ReadBool(string address)
        {
            try
            {
                OperateResult<bool> ON = new OperateResult<bool>();
                OperateResult<bool> OFF = new OperateResult<bool>();

                ON.Content = true;
                OFF.Content = false;

                OperateResult<short> result = ReadInt16(address);
                if (result != null && result.IsSuccess)
                    return (result.Content == 0 ? OFF : ON);
                else
                    return OFF;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public Int16[] ReadInt16Arry(string PLCAddr, ushort nLen)
        {
            OperateResult<Int16[]> result = null;
            try
            {
                PLCAddr = PLCAddr.Replace("D", "");
                result = base.ReadInt16(PLCAddr, nLen);
                if (result.IsSuccess && result.Content != null)
                    return result.Content;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
