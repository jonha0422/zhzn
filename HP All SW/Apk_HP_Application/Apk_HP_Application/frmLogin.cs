﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            调度系统.Checked = true;
            txtUser.Text = "Operator";

            HPglobal.m_strXmlFilename = "";
            string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            string folderPath = System.IO.Path.GetDirectoryName(exeFileName) + "\\ConfigFile";

            try
            {
                string[] folders = Directory.GetDirectories(folderPath);
                for(int nIndex = 0; nIndex < folders.Length; nIndex++)
                {
                    if(folders[nIndex].Contains("Model_") && File.Exists($"{folders[nIndex]}\\HP_Debug.xml"))
                    {
                        string ModelName = folders[nIndex].Substring(folders[nIndex].LastIndexOf("\\") + 1);
                        textBox2.Items.Add(ModelName);
                    }
                }

                textBox2.SelectedIndex = 0;
                //                 string[] strFiles = Directory.GetFiles(folderPath);
                //                 for (int nIndex = 0; nIndex < strFiles.Length; nIndex++)
                //                     if (System.IO.Path.GetExtension(strFiles[nIndex]).Contains("xml"))
                //                         textBox2.Items.Add(strFiles[nIndex]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
//             radioBtmWater.Checked = (global_data.m_emAppProcessType == global_data.emAppProcessType.emAppProcessType_WaterTest ? true : false);
//             radioBtmCenter.Checked = (global_data.m_emAppProcessType == global_data.emAppProcessType.emAppProcessType_CenterAll ? true : false);
//             radioBtmA.Checked = (global_data.m_emAppProcessType == global_data.emAppProcessType.emAppProcessType_A ? true : false);
//             radioBtmB.Checked = (global_data.m_emAppProcessType == global_data.emAppProcessType.emAppProcessType_B ? true : false);
// 
//             chkDisableAllPLCEvent.Checked = HPglobal.m_bDisablePLCEvent;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void btmNext_Click(object sender, EventArgs e)
        {
            string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            string folderPath = System.IO.Path.GetDirectoryName(exeFileName) + "\\ConfigFile";

            HPglobal.m_strModelName = textBox2.Text.Trim();
            HPglobal.m_strXmlFilename = $"{folderPath}\\{HPglobal.m_strModelName}\\HP_Debug.xml"; 
            if (!File.Exists(HPglobal.m_strXmlFilename))
            {
                MessageBox.Show("当前文件不存在，请重新选择！");
                return;
            }

            if (txtUser.Text.Trim() == "Administrator" && txtPsw.Text.Trim() != HPglobal.m_strAdminPSW)
            {
                MessageBox.Show("用户与密码不匹配，请重新输入");
                return;
            }

            HPglobal.m_strUserApp = txtUser.Text.Trim();
/*            HPglobal.m_bDisablePLCEvent = chkDisableAllPLCEvent.Checked;*/

            UpdateRunStatus();

            this.Close();
        }

        private void btmLoad_Click(object sender, EventArgs e)
        {
            string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            string folderPath = System.IO.Path.GetDirectoryName(exeFileName) + "\\ConfigFile";
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.FileName = "SystemConfig.xml";
            fileDlg.DefaultExt = "xml";
            fileDlg.InitialDirectory = folderPath;
            fileDlg.Multiselect = false;
            DialogResult result = fileDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBox2.Text = fileDlg.FileName;
            }
        }

        private void frm_Closing(object sender, FormClosingEventArgs e)
        {
            if (HPglobal.m_strXmlFilename == "")
                System.Environment.Exit(0);
        }

        void UpdateRunStatus()
        {

        }

        private void radioBtmCenter_CheckedChanged(object sender, EventArgs e)
        {
            UpdateRunStatus();
        }

        private void radioBtmA_CheckedChanged(object sender, EventArgs e)
        {
            UpdateRunStatus();
        }

        private void radioBtmB_CheckedChanged(object sender, EventArgs e)
        {
            UpdateRunStatus();
        }

        private void radioBtmWater_CheckedChanged(object sender, EventArgs e)
        {
            UpdateRunStatus();
        }

        private void richEditControl1_Click(object sender, EventArgs e)
        {

        }

        private void btmNext_Click_1(object sender, EventArgs e)
        {
            btmNext_Click(null, null);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void btmLoad_Click_1(object sender, EventArgs e)
        {
            btmLoad_Click(null, null);
        }

        private void radioBtmCenter_CheckedChanged_1(object sender, EventArgs e)
        {
            radioBtmCenter_CheckedChanged(null, null);
        }

        private void radioBtmA_CheckedChanged_1(object sender, EventArgs e)
        {
            radioBtmA_CheckedChanged(null, null);
        }

        private void radioBtmB_CheckedChanged_1(object sender, EventArgs e)
        {
            radioBtmB_CheckedChanged(null, null);
        }

        private void radioBtmWater_CheckedChanged_1(object sender, EventArgs e)
        {
            radioBtmWater_CheckedChanged(null, null);
        }

        private void chkEnableA_Up_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void chkEnableA_Down_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void chkEnableB_Up_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void chkEnableB_Down_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void chkDisableAllPLCEvent_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkEnableRemoteDebug_CheckedChanged(object sender, EventArgs e)
        {
            HPglobal.m_bEnableRemoteDebug = chkEnableRemoteDebug.Checked;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btmSelectProgram(object sender, EventArgs e)
        {
            if (调度系统.Checked)
                HPglobal.nAppFlage = 0;
            else  if (上料视觉软件.Checked)
                HPglobal.nAppFlage = 1;
            else if (打码切割软件.Checked)
                HPglobal.nAppFlage = 2;
            else if (机器人下料控制软件.Checked)
                HPglobal.nAppFlage = 3;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
