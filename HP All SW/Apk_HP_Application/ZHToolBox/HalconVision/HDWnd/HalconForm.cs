﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using HalconDotNet;
using static ZHToolBox.HalconVision.HDVisionTypes;

namespace ZHToolBox.HalconVision
{
    public partial class HalconForm : Form
    {
        public HWindow hWindow = new HWindow();
        public HImage hImage = new HImage();
        public int ImageWidth = 1280;
        public int ImageHeight = 1024;
        public int nIndexOfRio = 0;
        public RIO_DES[] m_RIO = new RIO_DES[2];

        public int offsetHeight = 0;
        public int offsetWidth = 0;

        Size LastSize;
        Control parentForm = null;
        Point oldPoint = new Point(0, 0);
        int nMouseClick = 0;

        public HTuple acqHandle = new HTuple();
        public string hAcqDriverName = "";
        public string cameraName = "";
        public int nPort = -1;

        Parameters parametersForm = null;
        bool bIsCameraOpened = false;

        bool bStopGrab = true;
        int nDrawType = 0;

        private double _Exposure = 25000.00; //us
        public double Exposure
        {
            get
            {
                return _Exposure;
            }

            set
            {
                if (value > 0)
                    _Exposure = value;
            }
        }

        private long _Contrast = 50; //0~100
        public long Contrast
        {
            get
            {
                return _Contrast;
            }

            set
            {
                if (value > 0 && value <= 100)
                    _Contrast = value;
            }
        }

        public delegate void GrabEventCall(HImage image);

        public HalconForm(string name)
        {
            CheckForIllegalCrossThreadCalls = true;

            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw |
                               ControlStyles.OptimizedDoubleBuffer |
                               ControlStyles.AllPaintingInWmPaint, true);
            this.UpdateStyles();

            InitializeComponent();
            this.Text = name;

            m_RIO[0] = new RIO_DES();
            m_RIO[1] = new RIO_DES();

            if(hWindow == null)
                hWindow = new HWindow();

            Size size = panel1.Size;
            hWindow.OpenWindow(0, 0, size.Width, size.Height, (HTuple)panel1.Handle, "visible", "");
            hWindow.SetWindowParam("background_color", "white");
   //         hWindow.SetWindowParam("border_color", "black");
            //	set_part_style(m_HalconWnd, 1);
            hWindow.SetPart( 0, 0, ImageHeight - 1, ImageWidth - 1);
            hWindow.SetDraw( "margin"); //fill
            hWindow.SetColor( "red");

            parametersForm = new Parameters(this);
            parametersForm.Hide();
            S1_Click(null, null);
        }

        ~HalconForm()
        {
            bStopGrab = true;
            CloseCamera();

            try
            {
                if(hWindow != null)
                {
                    hWindow.CloseWindow();
                    hWindow.Dispose();
                    hWindow = null;
                }
            }
            catch(HalconException ex)
            {

            }
        }

        public struct visionResult
        {
            public bool IsSuccess;
            public string ErrorMsg;
        }

        public visionResult ShowImage(HObject HalconImage, int nNewWidth = -1, int nNewHeight = 1)
        {
            try
            {
                HTuple nImageWidth = 0, nImageHeight = 0;
                HTuple szClass = "";
                int l_nNewWidth = 0, l_nNewHeight = 0;

                if(HalconImage == null)
                    return new visionResult() { IsSuccess = false, ErrorMsg = "ShowImage 传入图片参数为 null" };

                szClass = HalconImage.GetObjClass();
                if (szClass != "image") //  && strstr(szClass, "region") == NULL
                    return new visionResult() { IsSuccess = false, ErrorMsg = "当前显示的对像不是一张图片" };

                HObject obj = HalconImage.CopyObj(1, 1);
                hImage = new HImage(obj);
                Size size = this.panel1.Size;
                hImage.GetImageSize(out nImageWidth, out nImageHeight);

                if (nNewWidth <= 0 || nNewHeight <= 0)
                {
                    l_nNewWidth = size.Width;
                    l_nNewHeight = size.Height;
                }
                else
                {
                    l_nNewWidth = System.Math.Min(nNewWidth, size.Width);
                    l_nNewHeight = System.Math.Min(nNewHeight, size.Height);
                }

                hWindow.ClearWindow();
                if (nImageWidth > l_nNewWidth || nImageHeight > l_nNewHeight)
                {
                    double n = System.Math.Max((double)nImageWidth / l_nNewWidth, (double)nImageHeight / l_nNewHeight);
                    int nNewW = (int)((double)nImageWidth / n);
                    int nNewH = (int)((double)nImageHeight / n);

                    hWindow.SetWindowExtents(0, 0, nNewW, nNewH);
                    hWindow.SetColor("red");
                    hWindow.SetPart((HTuple)0, (HTuple)0, nImageHeight - 1, nImageWidth - 1); //显示图像, 图像的尺寸与窗口的尺寸一样
                }
                else
                {
                    hWindow.SetWindowExtents(0, 0, nImageWidth, nImageHeight);
                    hWindow.SetColor("red");
                    hWindow.SetPart((HTuple)0, (HTuple)0, nImageHeight - 1, nImageWidth - 1); //自动显示, 不放大, 原始图像
                }

                hWindow.DispObj(HalconImage);

                return new visionResult() { IsSuccess = true, ErrorMsg = "" };
            }
            catch (HalconException ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
            catch (Exception ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
        }

        public void ShowResult(CheckResultDef[] CheckResult, double rowOffset = 0.00, double colOffset = 0.00, bool clear = true)
        {
            try
            {
                if (clear && hWindow != null)
                    HOperatorSet.ClearWindow(hWindow);

                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    HObject Rectangle1 = new HObject();
                    HOperatorSet.GenRectangle2(out Rectangle1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol,
                                                                    (new HTuple(CheckResult[nIndexAOI].CheckedRIO.fPhi)).TupleRad(), CheckResult[nIndexAOI].CheckedRIO.fLen1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fLen2);

                    if(CheckResult[nIndexAOI].ResultStatus)
                    {
                        HOperatorSet.SetColor(hWindow, "green");
                        HOperatorSet.DispObj(Rectangle1, hWindow);
                        HalconGeneral.HD_DisplayMessage(hWindow, $"{(nIndexAOI + 1).ToString()} : OK", "image", CheckResult[nIndexAOI].CheckedRIO.fRow + rowOffset, CheckResult[nIndexAOI].CheckedRIO.fCol + colOffset, "forest green", "true");
                    }
                    else
                    {
                        HOperatorSet.SetColor(hWindow, "red");
                        HOperatorSet.DispObj(Rectangle1, hWindow);
                        HalconGeneral.HD_DisplayMessage(hWindow, $"{(nIndexAOI + 1).ToString()}: NG", "image", CheckResult[nIndexAOI].CheckedRIO.fRow + rowOffset, CheckResult[nIndexAOI].CheckedRIO.fCol + colOffset, "red", "true");
                    }

                    Rectangle1.Dispose();
                }
            }
            catch (HalconException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsContainImage()
        {
            try
            {
                string szClass = "";
                szClass = hImage.GetObjClass();
                if (szClass == "image")
                    return true;
                else
                    return false;
            }
            catch (HalconException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        public int SetNewSize(int nNewWidth, int nNewHeight, bool bReference)
        {
            try
            {
                panel1.Width = nNewWidth;
                panel1.Height = nNewHeight;
                hWindow.SetWindowExtents(0, 0, nNewWidth, nNewHeight);
                hWindow.SetPart(0, 0, -1, -1);

                if (bReference)
                {
                    hWindow.ClearWindow();
                    ShowImage(hImage, nNewWidth, nNewHeight);
                }
            }
            catch (HalconException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return 0;
        }

        public visionResult OpenCamera()
        {
            try
            {
                return OpenCamera(hAcqDriverName, cameraName, nPort);
            }
            catch (HalconException ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
        }

        public visionResult SetCameraExposure(double exposure)
        {
            try
            {
                HOperatorSet.SetFramegrabberParam(acqHandle, "ExposureTime", exposure);
                Thread.Sleep(20);
                return new visionResult() { IsSuccess = true, ErrorMsg = "" };
            }
            catch (HalconException ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = $"SetCameraExposure 错误, 请检相应的参数是否正确." };
            }
            catch (Exception ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = $"SetCameraExposure 错误, 请检相应的参数是否正确." };
            }
        }

        public visionResult SetCameraContrast(long contrast)
        {
            try
            {
                HOperatorSet.SetFramegrabberParam(acqHandle, "Contrast", contrast);
                Thread.Sleep(20);
                return new visionResult() { IsSuccess = true, ErrorMsg = "" };
            }
            catch (HalconException ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = $"SetCameraContrast 错误, 请检相应的参数是否正确." };
            }
            catch (Exception ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = $"SetCameraContrast 错误, 请检相应的参数是否正确." };
            }
        }

        public visionResult OpenCamera(string driver, string name, int port = 0)
        {
            try
            {
                if (!bIsCameraOpened)
                {
                    HOperatorSet.OpenFramegrabber(driver, 0, 0, 0, 0, 0, 0, "default", -1, "default", -1, "false", "default", name, port, -1, out acqHandle);
                    HOperatorSet.SetFramegrabberParam(acqHandle, "ExposureTime", Exposure);
                    HOperatorSet.SetFramegrabberParam(acqHandle, "Contrast", Contrast);
                    HOperatorSet.SetFramegrabberParam(acqHandle, "TriggerMode", "software_trigger");
                    isSoftwareTrigger = true;
                    HOperatorSet.GrabImageStart(acqHandle, -1);
                    /*                    HOperatorSet.SetFramegrabberParam(acqHandle, "grab_timeout", 1000);*/
                    hAcqDriverName = driver;
                    cameraName = name;
                    nPort = port;
                    bIsCameraOpened = true;

                    return new visionResult() { IsSuccess = true, ErrorMsg = "打开成功" };
                }
                else
                    return new visionResult() { IsSuccess = true, ErrorMsg = "相机已打开" };
            }
            catch (HalconException ex)
            {
                bIsCameraOpened = false;
                HOperatorSet.CloseFramegrabber(acqHandle);
                return new visionResult() { IsSuccess = false, ErrorMsg = $"OpenCamera 错误, 请检相应的参数是否正确，或有没有安装正确的驱动!\n驱动名: {driver}\n相机名: {name}\n端口名: {port}\n{ex.Message}" };
            }
            catch (Exception ex)
            {
                bIsCameraOpened = false;
                HOperatorSet.CloseFramegrabber(acqHandle);
                return new visionResult() { IsSuccess = false, ErrorMsg = $"OpenCamera 错误, 请检相应的参数是否正确，或有没有安装正确的驱动!\n驱动名: {driver}\n相机名: {name}\n端口名: {port}\n{ex.Message}" };
            }
        }

        public visionResult CloseCamera()
        {
            try
            {
                bStopGrab = true;
                if (bIsCameraOpened)
                {
                    HOperatorSet.CloseFramegrabber(acqHandle);
                }

                return new visionResult() { IsSuccess = true, ErrorMsg = "" };

            }
            catch (HalconException ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = $"CloseCamera 错误, 请检相应的参数是否正确"};
            }
            catch (Exception ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
           
        }

        public visionResult SnapImage(out HImage image)
        {
            bStopGrab = true;

            try
            {
                if (OpenCamera().IsSuccess == false)
                {
                    HObject objNull = new HObject();
                    objNull.GenEmptyObj();
                    image = new HImage(objNull);
                    return new visionResult() { IsSuccess = false, ErrorMsg = "无法连接或打开相机" };
                }

                HObject obj = null;
                if(isSoftwareTrigger == false)
                {
                    HOperatorSet.SetFramegrabberParam(acqHandle, "TriggerMode", "software_trigger");
                    isSoftwareTrigger = true;
                }

                HOperatorSet.SetFramegrabberParam(acqHandle, "do_TriggerSoftware", "");
                Thread.Sleep(50);
                HOperatorSet.GrabImageAsync(out obj, acqHandle, 100);
/*                HOperatorSet.GrabImage(out obj, acqHandle);*/
                if (obj != null)
                {
                    image = new HImage(obj);
                    obj.Dispose();
                    ShowImage(image, -1, -1);
                    return new visionResult() { IsSuccess = true, ErrorMsg = "" };
                }
                else
                {
                    image = null;
                    return new visionResult() { IsSuccess = false, ErrorMsg = "采集空的图片" };
                }
            }
            catch (HalconException ex)
            {
                image = null;
                return new visionResult() { IsSuccess = true, ErrorMsg = ex.Message };
            }
            catch (Exception ex)
            {
                image = null;
                return new visionResult() { IsSuccess = true, ErrorMsg = ex.Message };
            }
        }

        public visionResult GrabStop()
        {
            bStopGrab = true;
            Grab.Checked = false;
            return new visionResult() { IsSuccess = true, ErrorMsg = "" };
        }

        bool isSoftwareTrigger = true;
        public visionResult GrabContinue(GrabEventCall call)
        {
            try
            {
                if (OpenCamera().IsSuccess == false)
                {
                    return new visionResult() { IsSuccess = false, ErrorMsg = "无法连接或打开相机" };
                }

                if (isSoftwareTrigger)
                {
                    HOperatorSet.SetFramegrabberParam(acqHandle, "TriggerMode", "continuous");
                    isSoftwareTrigger = false;
                }

                HOperatorSet.GrabImageStart(acqHandle, -1);
                bStopGrab = false;
                Grab.Checked = true;
                Thread continueGrab = new Thread(new ParameterizedThreadStart(thread_ForIOMonitor));
                continueGrab.Start(call);
                return new visionResult() { IsSuccess = true, ErrorMsg = "" };
            }
            catch (HalconException ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
            catch (Exception ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
        }

        public RIO_DES GetCurrentRIO(int nIndex = 0)
        {
            RIO_DES value = new RIO_DES();
            try
            {
                int curr = nIndex == 0 ? 0 : 1;

                value.fRow = m_RIO[curr].fRow;
                value.fCol = m_RIO[curr].fCol;
                value.fPhi = m_RIO[curr].fPhi;
                value.fLen1 = m_RIO[curr].fLen1;
                value.fLen2 = m_RIO[curr].fLen2;
                value.szItemRemark = m_RIO[curr].szItemRemark;
                value.fRadix = m_RIO[curr].fRadix;
                value.fRowStart = m_RIO[curr].fRowStart;
                value.fColStart = m_RIO[curr].fColStart;
                value.fRowEnd = m_RIO[curr].fRowEnd;
                value.fColEnd = m_RIO[curr].fColEnd;
                return value;
            }
            catch (HalconException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DrawRIO(int nType)
        {
            try
            {
                if(nType == 1)
                {
                    double row = 0, col = 0, row2 = 0, col2 = 0;
                    hWindow.SetColor("red");
                    hWindow.DrawRectangle1(out row, out col, out row2, out col2);
                    if (row < 0.1 || col < 0.1 || row2 < 0.1 || col2 < 0.1)
                        goto END;

                    if (m_RIO[nIndexOfRio] == null)
                        m_RIO[nIndexOfRio] = new RIO_DES();

                    m_RIO[nIndexOfRio].InitRIO();

                    m_RIO[nIndexOfRio].fRow = row + (row2 - row) / 2.00f;
                    m_RIO[nIndexOfRio].fCol = col + (col2 - col) / 2.00f;
                    m_RIO[nIndexOfRio].fLen1 = (col2 - col) / 2.00f;
                    m_RIO[nIndexOfRio].fLen2 = (row2 - row) / 2.00f;

                    HTuple tupleDeg = 0;
                    m_RIO[nIndexOfRio].fPhi = tupleDeg.TupleRad();

                    m_RIO[nIndexOfRio].fRowStart = row;
                    m_RIO[nIndexOfRio].fColStart = col;
                    m_RIO[nIndexOfRio].fRowEnd = row2;
                    m_RIO[nIndexOfRio].fColEnd = col2;

                    HRegion objRect = new HRegion();
                    objRect.GenRectangle2(m_RIO[nIndexOfRio].fRow, m_RIO[nIndexOfRio].fCol, m_RIO[nIndexOfRio].fPhi,
                                           m_RIO[nIndexOfRio].fLen1, m_RIO[nIndexOfRio].fLen2);
                    hWindow.DispObj(objRect);
                }

                else if (nType == 2)
                {
                    double row = 0, col = 0, phi = 0, len1 = 0, len2 = 0;
                    hWindow.SetColor("red");
                    hWindow.DrawRectangle2(out row, out col, out phi, out len1, out len2);
                    if (row < 0.1 || col < 0.1 || len1 < 0.1 || len2 < 0.1)
                        goto END;

                    m_RIO[nIndexOfRio].InitRIO();

                    m_RIO[nIndexOfRio].fRow = row;
                    m_RIO[nIndexOfRio].fCol = col;
                    m_RIO[nIndexOfRio].fLen1 = len1;
                    m_RIO[nIndexOfRio].fLen2 = len2;
                    m_RIO[nIndexOfRio].fPhi = phi;

                    HRegion objRect = new HRegion();
                    objRect.GenRectangle2(m_RIO[nIndexOfRio].fRow, m_RIO[nIndexOfRio].fCol, m_RIO[nIndexOfRio].fPhi,
                                          m_RIO[nIndexOfRio].fLen1, m_RIO[nIndexOfRio].fLen2);
                    hWindow.DispObj(objRect);
                }

                else if (nType == 3)
                {
                    double row = 0, col = 0, radius = 0.00;
                    hWindow.SetColor("red");
                    hWindow.DrawCircle(out row, out col, out radius);
                    if (row < 0.1 || col < 0.1 || radius < 0.1)
                        goto END;

                    m_RIO[nIndexOfRio].InitRIO();

                    m_RIO[nIndexOfRio].fRow = row;
                    m_RIO[nIndexOfRio].fCol = col;
                    m_RIO[nIndexOfRio].fRadix = radius;

                    HRegion objRect = new HRegion();
                    objRect.GenCircle(m_RIO[nIndexOfRio].fRow, m_RIO[nIndexOfRio].fCol, m_RIO[nIndexOfRio].fRadix);
                    hWindow.DispObj(objRect);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show($"DrawRIO 错误\n{ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            END:
            nDrawType = 0;
            Rectangle1.Checked = false;
            Rectangle2.Checked = false;
            Circle.Checked = false;
        }

        private void Rectangle1_Click(object sender, EventArgs e)
        {
            nDrawType = 1;
            Application.DoEvents();
            DrawRIO(nDrawType);
        }

        private void Rectangle2_Click(object sender, EventArgs e)
        {
            nDrawType = 2;
            Application.DoEvents();
            DrawRIO(nDrawType);
        }

        private void Circle_Click(object sender, EventArgs e)
        {
            nDrawType = 3;
            Application.DoEvents();
            DrawRIO(nDrawType);
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            try
            {
                hWindow.ClearWindow();
                hWindow.DispObj(hImage);
                Update();
            }
            catch(Exception ex)
            {

            }
        }

        private void ZoonOut_Click(object sender, EventArgs e)
        {

        }

        private void ZoonIn_Click(object sender, EventArgs e)
        {

        }

        private void Zoom100_Click(object sender, EventArgs e)
        {

        }

        private void Open_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog file = new OpenFileDialog();
                file.Title = "请选择图片";
                file.Filter = "(*.bmp)|*.bmp|(*.jpg)|*.jpg|(*.png)|*.png|(*.*)|*.*";
                //file.InitialDirectory = @"E:\计算机资料\01 C#视频\基础实训4\1110C#基础\资料\img";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    string strNames = file.FileName;
                    hImage.ReadImage(strNames);
                    ShowImage(hImage, -1, -1);
                }
            }
            catch(Exception ex)
            {

            }
        }

        public visionResult ShowImage(string imageFileNamel)
        {
            try
            {
                hImage.ReadImage(imageFileNamel);
                ShowImage(hImage, -1, -1);

                return new visionResult() { IsSuccess = true, ErrorMsg = "" };
            }
            catch (HalconException ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
            catch (Exception ex)
            {
                return new visionResult() { IsSuccess = false, ErrorMsg = ex.Message };
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsContainImage())
                    return;

                SaveFileDialog file = new SaveFileDialog();
                file.Title = "请选择图片";
                file.Filter = "(*.bmp)|*.bmp|(*.png)|*.png|(*.jpg)|*.jpg";
                file.FileName = $"{DateTime.Now.ToLongDateString()}";
                //file.InitialDirectory = @"E:\计算机资料\01 C#视频\基础实训4\1110C#基础\资料\img";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    string strNames = file.FileName;
                    hImage.WriteImage("bmp", 0, strNames);
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void SaveRIO_Click(object sender, EventArgs e)
        {
            try
            {
                RIO_DES rio = new RIO_DES();
                rio.InitRIO();

                if (!IsContainImage())
                    return;

                SaveFileDialog file = new SaveFileDialog();
                file.Title = "请选择图片";
                file.Filter = "(*.bmp)|*.bmp|(*.png)|*.png|(*.jpg)|*.jpg";
                file.FileName = $"{DateTime.Now.ToLongDateString()}";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    if (S1.Checked)
                        rio = m_RIO[0];
                    else
                        rio = m_RIO[1];
                    string strNames = file.FileName;

                    HObject RIOImage;
                    HalconGeneral.RIO_To_Image(hImage, out RIOImage, strNames, rio.fRow, rio.fCol, rio.fPhi, rio.fLen1, rio.fLen2, true);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, $"保存图片出现异常: {ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void form_Size(object sender, EventArgs e)
        {

        }

        private void S1_Click(object sender, EventArgs e)
        {
            S1.Checked = true;
            S2.Checked = false;
            nIndexOfRio = 0;
        }

        private void S2_Click(object sender, EventArgs e)
        {
            S2.Checked = true;
            S1.Checked = false;
            nIndexOfRio = 1;
        }

        private void panel_Size(object sender, EventArgs e)
        {
            LastSize = this.Size;
        }

        private void form_ReSizeEnd(object sender, EventArgs e)
        {
            LastSize = this.Size;
        }

        private void panel_SizeChanged(object sender, EventArgs e)
        {
            Size size = panel1.Size;
            Size size2 = this.Size;

            SetNewSize(size.Width, size.Height, true);
        }

        private void MainPanel_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Parent == null && parentForm == null)
                {
                    return;
                }
                else if (this.Parent != null && parentForm == null)
                {
                    parentForm = this.Parent;
                    oldPoint = this.Location;
                    this.Parent.Controls.Remove(this);
                    LastSize = this.Size;
                    TopLevel = true;
                    FormBorderStyle = FormBorderStyle.Sizable;
                    this.Parent = null;
                }
                else if (this.Parent == null && parentForm != null)
                {
                    TopLevel = false;
                    FormBorderStyle = FormBorderStyle.None;
                    parentForm.Controls.Add(this);
                    Location = oldPoint;
                    Size = parentForm.Size;
                    parentForm = null;
                    Show();
                    parentForm.SendToBack();
                    BringToFront();
                }
            }
            catch(Exception ex)
            {
               
            }
        
        }

        private void Snap_Click(object sender, EventArgs e)
        {
            try
            {
                HImage image;
               if(SnapImage(out image).IsSuccess == false)
                    MessageBox.Show($"采集错误\n", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch(Exception ex)
            {
                MessageBox.Show($"采集异常\n{ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void thread_ForIOMonitor(object wsarg)
        {
            while (!bStopGrab)
            {
                try
                {
                    Application.DoEvents();
                    HObject obj = null;
                    HOperatorSet.GrabImageAsync(out obj, acqHandle, 100);
                    this.Invoke(new Action(()=> { ShowImage(new HImage(obj), -1, -1); }));
                   
                }
                catch (HalconException ex)
                {

                }
            }
        }

        private void Grab_Click(object sender, EventArgs e)
        {
            try
            {
                if (Grab.Checked)
                {
                    GrabStop();
                    return;
                }

                GrabContinue(null);
            }
            catch (HalconException ex)
            {
                throw new Exception($"连续采集错误\n{ ex.Message }");
            }
            catch (Exception ex)
            {
                throw new Exception($"连续采集错误\n{ ex.Message }");
            }
        }

        private void Setting_Click(object sender, EventArgs e)
        {
            try
            {
                parametersForm.Show();
                parametersForm.WindowState = FormWindowState.Normal;
                parametersForm.BringToFront();
                parametersForm.updataParameters();
            }
            catch(Exception ex)
            {
                return;
            }
        }

        private void panel_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void panel_Click(object sender, EventArgs e)
        {
           
        }

        private void HalconForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
