using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace daitou
{
	public class TableControllerFive : XtreeNodeController
	{
		protected TableDefFive tableDefFive;
		protected TableDefFour tableDef;

		public override string Name
		{
			get { return tableDefFive.Name; }
			set {tableDefFive.Name=value;}
		}
        public override string Pridata
        {
            get { return tableDefFive.Pridata; }
            set { tableDefFive.Pridata = value; }
        }
        public TableDefFive TableDefFive
		{
			get { return tableDefFive; }
		}

		public override object Item
		{
			get { return tableDefFive; }
		}

		public TableControllerFive()
		{
		}

		public TableControllerFive(TableDefFive tableDefFive)
		{
			this.tableDefFive = tableDefFive;
		}

		public override int Index(object item)
		{
			return tableDefFive.Fields.IndexOf(((TableFieldController)item).TableFieldDef);
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
            tableDef = ((TableControllerFour)parentInstance).TableDefFour;
			tableDefFive = new TableDefFive();
            tableDef.Fields.Add(tableDefFive);
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableControllerFour)parentInstance).TableDefFour;
            tableDef.Fields.Remove(tableDefFive);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableControllerFour)parentInstance).TableDefFour;
            tableDef.Fields.Remove(tableDefFive);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            tableDef = ((TableControllerFour)parentInstance).TableDefFour;
            tableDef.Fields.Insert(idx, tableDefFive);
		}

		public override void Select(TreeNode tn)
		{
			//Program.Properties.SelectedObject = tableDefFive;
		}
	}
}
