﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public partial class myStationForm : UserControl
    {
        public HPStationBase stationBase = null;
        public frm_StatinInfoDetail frmStationInf = new frm_StatinInfoDetail();

        public myStationForm(string name, DevExpress.XtraEditors.XtraForm frmStation)
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw |
                               ControlStyles.OptimizedDoubleBuffer |
                               ControlStyles.AllPaintingInWmPaint, true);
            this.UpdateStyles();

            stationBase = new HPStationBase(name);
            frmStationInf.BindStationForm(stationBase, frmStation);
        }

        ~myStationForm()
        {
            stationBase.Dispose();
            frmStationInf.Dispose();
        }

        public bool ConnectPLC(string ip, int port)
        {
            return stationBase.ConnectPLC(ip, port);
        }

        public bool ConnectPCModbus(string ip, int port, byte station)
        {
            return stationBase.ConnectPCModbus(ip, port, station);
        }

        private void popupContainerEdit2_EditValueChanged(object sender, EventArgs e)
        {

        }

        public bool Intialize()
        {
          try
            {
                timer1.Enabled = true;
                return stationBase.Intialize();
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm Intialize() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
            return false;
        }

        public bool Start()
        {
            try
            {
                return stationBase.Start();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm Start() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
            return false;
        }

        public bool Stop()
        {
            try
            {
                return stationBase.Stop();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm Stop() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
            return false;
        }

        public bool Reset()
        {
            try
            {
                return stationBase.Reset();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm Reset() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
            return false;
        }

        public bool ClearError()
        {
            try
            {
                return stationBase.ClearError();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm ClearError() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
            return false;
        }

        public bool IsReady()
        {
            try
            {
                return (stationBase.ReadInt16("D1002") == 1 ? true : false);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm Reset() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
            return false;
        }

        private void labelControl13_Click(object sender, EventArgs e)
        {

        }

        private void frm_mouseDoubleClick(object sender, MouseEventArgs e)
        {
            frmStationInf.Show();
            frmStationInf.WindowState = FormWindowState.Normal;
            frmStationInf.BringToFront();
/*            frmStationInf.DisplayObjData();*/
        }

        private void frm_MouseHover(object sender, EventArgs e)
        {
//             if(!frmStationInf.Visible)
//             {
//                 frmStationInf.Show();
//                 frmStationInf.WindowState = FormWindowState.Normal;
//                 frmStationInf.BringToFront();
//             }

        }

        private void frm_MouseLeave(object sender, EventArgs e)
        {
//             if (frmStationInf.Visible)
//             {
//                 frmStationInf.Hide();
//             }
        }

        public void UpdateStatus()
        {
            try
            {
                #region>>>>显示设备的连接状态
                //===============================
                LED_Status.LanternBackground = (LED_Status.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                #endregion

                #region>>>>>>显示载板状态
                txtTRAYID.Text = stationBase.strTrayIDName;
                txtPCBSN.Text = stationBase.strVirtrualPCBSN;


                txtTRAYID2.Text = stationBase.strTrayIDName2;
                txtPCBSN2.Text = stationBase.strVirtrualPCBSN2;

                #endregion

                #region>>>>>>显示MES状态
                if (stationBase.bEnbalScanner1)
                {
                    if (stationBase.bIsMESChecking1 == 1)
                    {
                        txtMES1.Text = "MES 校验中....";
                        txtMES1.BackColor = Color.Yellow;
                    }
                    else
                    {
                        txtMES1.Text = ((stationBase.nMes1VerifyResult == 1 || stationBase.nMes1VerifyResult == 3) ? "MES OK" : "MES NG");
                        txtMES1.BackColor = ((stationBase.nMes1VerifyResult == 1 || stationBase.nMes1VerifyResult == 3) ? Color.Green : Color.Red);
                    }
                }
                else
                {
                    if (txtMES1.BackColor != Color.Gray)
                        txtMES1.BackColor = Color.Gray;
                }

                if (stationBase.bEnbalScanner2)
                {
                    if (stationBase.bIsMESChecking2 == 1)
                    {
                        txtMES2.Text = "MES 校验中....";
                        txtMES2.BackColor = Color.Yellow;
                    }
                    else
                    {
                        txtMES2.Text = ((stationBase.nMes2VerifyResult == 1 || stationBase.nMes2VerifyResult == 3) ? "MES OK" : "MES NG");
                        txtMES2.BackColor = ((stationBase.nMes2VerifyResult == 1 || stationBase.nMes2VerifyResult == 3) ? Color.Green : Color.Red);
                    }
                }
                else
                {
                    if (txtMES2.BackColor != Color.Gray)
                        txtMES2.BackColor = Color.Gray;
                }


                #endregion

                HPglobal.ShowColorStatus(txtStatus, stationBase.emState);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm UpdateStatus异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                UpdateStatus();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm timer1_Tick() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }
    }
}
