﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel;

namespace HpWebService
{
    public class Result
    {
        public int XH { get; set; }//小板的序号
        public int Image1sta { get; set; }//前拍照
        public int Image2sta { get; set; }//后拍照
        public int Burnsta { get; set; }//烧录结果
        public int Codesta { get; set; }//打码结果
        public int Sta
        {//汇总的结果
            get
            {
                if (Image1sta == 0 && Image2sta == 0 && Burnsta == 0 && Codesta == 0)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }

        }
        public Result()
        {
            Image1sta = 0;
            Image2sta = 0;
            Burnsta = 0;
            Codesta = 0;
        }
        public Result Clone()
        {
            Result aResult = new Result();
            aResult.Image1sta = this.Image1sta;
            aResult.Image2sta = this.Image2sta;
            aResult.Burnsta = this.Burnsta;
            aResult.Codesta = this.Codesta;
            //aResult = (Result)this.MemberwiseClone();
            return aResult;
        }

    }

    //生产日志
    public class ProductLog
    {
        public int Station { get; set; } //工位
        public int Datatime { get; set; } //时间日期
        public int LogMsg { get; set; } //日志内容
    }

    public class HpMesParameter
    {
        //======================================================
        //****************************************************************************************
        //======================================================
        [Category("单板尺寸"), DefaultValue(0.00), DisplayName("单板长度")]
        public double DBLength { get; set; }

        [Category("单板尺寸"), DefaultValue(0.00), DisplayName("芯片X方向间隔距离")]
        public double DBPACING_X { get; set; }

        [Category("单板尺寸"), DefaultValue(0.00), DisplayName("芯片Y方向间隔距离")]
        public double DBPACING_Y { get; set; }

        [Category("单板尺寸"), DefaultValue(0.00), DisplayName("第二个芯片Y方向间隔距离")]
        public double DBPACING_Y2 { get; set; }  //  固定为8

        [Category("单板尺寸"), DefaultValue(0.00), DisplayName("单板宽度")]
        public double DBWidth { get; set; }

        //======================================================
        //****************************************************************************************
        //======================================================
        [Category("拼板数量"), DefaultValue(0.00), DisplayName("单板长度")]
        public double PBLength { get; set; }

        [Category("拼板数量"), DefaultValue(0.00), DisplayName("芯片X方向间隔距离")]
        public double PBPACING_X { get; set; }

        [Category("拼板数量"), DefaultValue(0.00), DisplayName("芯片Y方向间隔距离")]
        public double PBPACING_Y { get; set; }

        [Category("拼板数量"), DefaultValue(0.00), DisplayName("单板宽度")]
        public double PBWidth { get; set; }

        //======================================================
        //****************************************************************************************
        //======================================================
        [Category("拼板外观尺寸"), DefaultValue(0.00), DisplayName("单板长度")]
        public double WGCCLength { get; set; }

        [Category("拼板外观尺寸"), DefaultValue(0.00), DisplayName("芯片X方向间隔距离")]
        public double WGCCPACING_X { get; set; }

        [Category("拼板外观尺寸"), DefaultValue(0.00), DisplayName("芯片Y方向间隔距离")]
        public double WGCCPACING_Y { get; set; }

        [Category("拼板外观尺寸"), DefaultValue(0.00), DisplayName("单板宽度")]
        public double WGCCWidth { get; set; }

        //======================================================
        //****************************************************************************************
        //======================================================
        [Category("工单信息"), DefaultValue(""), DisplayName("打码内容")]
        public string MarkText { get; set; }

        [Category("工单信息"), DefaultValue(""), DisplayName("烧录描述")]
        public string DescRiption { get; set; }
        public string matNo { get; set; }
        //======================================================
        //****************************************************************************************
        //======================================================
        [Category("工单信息"), DefaultValue(""), DisplayName("工单号")]
        public string WorkSheet { get; set; }//工单

        [Category("工单信息"), DefaultValue(""), DisplayName("PCB型号")]
        public string PcbNumber { get; set; }//PCB型号

        [Category("工单信息"), DefaultValue(""), DisplayName("产品型号")]
        public string ProductNumber { get; set; }//产品型号--new

        [Category("工单信息"), DefaultValue(""), DisplayName("单包数量")]
        public string PageNumber { get; set; }//单包数量--new

        [Category("工单信息"), DefaultValue(""), DisplayName("计划总数")]
        public string PlanNumber { get; set; }//计划总数--new

        [Category("工单信息"), DefaultValue(0), DisplayName("X方向芯片个数")]
        public int colNumber { get; set; }//X方向芯片个数

        [Category("工单信息"), DefaultValue(0), DisplayName("Y方向芯片个数")]
        public int rowNumber { get; set; }//Y方向芯片个数

        [Category("工单信息"), DefaultValue(0.00), DisplayName("芯片宽度")]
        public double chipWidth { get; set; }//芯片宽度

        [Category("工单信息"), DefaultValue(0.00), DisplayName("芯片高度")]
        public double chipHeight { get; set; }//芯片高度

        [Category("工单信息"), DefaultValue(0.00), DisplayName("芯片X方向间隔距离")]
        public double IntervalDisX { get; set; }//芯片X方向间隔距离

        [Category("工单信息"), DefaultValue(0.00), DisplayName("芯片Y方向间隔距离")]
        public double IntervalDisY { get; set; }//芯片Y方向间隔距离

        [Category("工单信息"), DefaultValue(0.00), DisplayName("整板宽度")]
        public double PCBWidth { get; set; }//整板宽度

        [Category("工单信息"), DefaultValue(0.00), DisplayName("整板高度")]
        public double PCBHeight { get; set; }//整板高度

        #region 定义正反面
        [Category("工单信息"), DefaultValue(-1), DisplayName("烧录正反面")]
        public int BurnFace { get; set; }//烧录正反面 0；正面 1：反面

        [Category("工单信息"), DefaultValue(-1), DisplayName("打码正反面(0/1)")]
        public int MarkFace { get; set; }//打码正反面 0；正面 1：反面
        #endregion

        #region 定义状态变量烧录
        [Category("工单信息"), DefaultValue(""), DisplayName("烧录程序名称")]
        public string BurnProgramName { get; set; }//烧录程序名称
        #endregion

        //打标点
        #region 整版打码参数
        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点X1")]
        public double MarkX1 { get; set; }//第一个打标点X1

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点Y1")]
        public double MarkY1 { get; set; }//第一个打标点Y1

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点角度")]
        public double MarkAngle1 { get; set; }//第一个打标点角度

        [Category("工单信息"), DefaultValue(""), DisplayName("第一个打标点打标内容")]
        public string MarkText1 { get; set; }//第一个打标点打标内容 

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点X2")]
        public double MarkX2 { get; set; }//第一个打标点X2

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点Y2")]
        public double MarkY2 { get; set; }//第一个打标点Y2

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点角度2")]
        public double MarkAngle2 { get; set; }//第一个打标点角度2

        [Category("工单信息"), DefaultValue(""), DisplayName("第一个打标点打标内容")]
        public string MarkText2 { get; set; }//第一个打标点打标内容 2

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点X3")]
        public double MarkX3 { get; set; }//第一个打标点X3

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点Y3")]
        public double MarkY3 { get; set; }//第一个打标点Y3

        [Category("工单信息"), DefaultValue(0.00), DisplayName("第一个打标点角度")]
        public double MarkAngle3 { get; set; }//第一个打标点角度

        [Category("工单信息"), DefaultValue(""), DisplayName("第一个打标点打标内容")]
        public string MarkText3 { get; set; }//第一个打标点打标内容 
        #endregion

        #region 拍照模板
        [Category("工单信息"), DefaultValue(""), DisplayName("正面模板")]
        public string ImageTempPositive { get; set; }//正面模板

        [Category("工单信息"), DefaultValue(""), DisplayName("反面模板")]
        public string ImageTempReverse { get; set; }//反面模板

        [Category("工单信息"), DefaultValue(""), DisplayName("烧录上料模板")]
        public string ImageTempBurn { get; set; }//烧录上料模板

        [Category("工单信息"), DefaultValue(""), DisplayName("转盘取料模板")]
        public string ImageTempTurntable { get; set; }//转盘取料模板

        [Category("工单信息"), DefaultValue(""), DisplayName("转盘测试模板")]
        public string ImageTempTurntableTest { get; set; }//转盘测试模板

        [Category("工单信息"), DefaultValue(""), DisplayName("不良品机器人")]
        public string ImageTempEpsonNG { get; set; }//不良平机器人

        [Category("工单信息"), DefaultValue(""), DisplayName("包装机器人")]
        public string ImageTempEpsonPacking { get; set; }//包装机器人
        #endregion

        [Category("工单信息"), DefaultValue(""), DisplayName("数据库中的关联ID")]
        public string GUID { get; set; }//数据库中的关联ID

        [Category("工单信息"), DefaultValue(""), DisplayName("前段对应条码")]
        public string code1 { get; set; }//前段对应条码

        [Category("工单信息"), DefaultValue(""), DisplayName("第二段RFID")]
        public string code2 { get; set; }//第二段RFID

        [Category("工单信息"), DefaultValue(-1), DisplayName("当前工位")]
        public int curStation { get; set; }//当前工位   1-5

        [Category("工单信息"), DefaultValue(-1), DisplayName("当前子工位")]
        public int curSubStation { get; set; }//当前工位   各工位自定义多少个子工位

        [Category("工单信息"), DefaultValue(-1), DisplayName("烧录机编号")]
        public int BurnStation { get; set; }//烧录工位 记录由那台烧录机烧录

        [XmlIgnoreAttribute]
        public string BeginTime { get; set; }

        [XmlIgnoreAttribute]
        public string EndTime { get; set; }

        [XmlIgnoreAttribute]
        public int isFinish { get; set; }

        [XmlIgnoreAttribute]
        public List<Result> ResultList { get; set; }     //每一块小板的结果


        public HpMesParameter()
        {
            DBLength = 0.00;
            DBPACING_X = 0.00;
            DBPACING_Y = 0.00;
            DBPACING_Y2 = 0.00;//  固定为8
            DBWidth = 0.00;
            PBLength = 0.00;
            PBPACING_X = 0.00;
            PBPACING_Y = 0.00;
            PBWidth = 0.00;
            WGCCLength = 0.00;
            WGCCPACING_X = 0.00;
            WGCCPACING_Y = 0.00;
            WGCCWidth = 0.00;
            MarkText = "";

            WorkSheet = "未定义工单编号";
            PcbNumber = "";

            GUID = Guid.NewGuid().ToString();
            code1 = "";//测试用
            DateTime _ts = System.DateTime.Now;
            BeginTime = _ts.ToString("yyyy-MM-dd hh:mm:ss fff");

            curStation = 1;
            curSubStation = 1;

            PlanNumber = "0";
            rowNumber = 0;
            colNumber = 0;

            ResultList = new List<Result>();

        }
        //public int CreatResultList()
        //{
        //    for (int i = 0; i < rowNumber * colNumber; i++)
        //    {
        //        Result r = new Result();
        //        r.XH = i + 1;
        //        ResultList.Add(r);
        //    }
        //    return 0;
        //}
        //public ProductData Clone()
        //{
        //    ProductData aData = new ProductData();
        //    aData = (ProductData)this.MemberwiseClone();
        //    aData.ResultList = new List<Result>();
        //    for (int i = 0; i < this.ResultList.Count; i++)
        //    {
        //        aData.ResultList.Add(this.ResultList[i].Clone());
        //    }
        //    return aData;
        //}
        //取错误小板的数量
        public int getErrPcbNum()
        {
            int number = 0;
            for (int i = 0; i < ResultList.Count; i++)
            {
                if (ResultList[i].Sta != 0)
                {
                    number++;
                }
            }
            return number;
        }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public int getPassPcbNum()
        {
            int number = 0;
            for (int i = 0; i < ResultList.Count; i++)
            {
                if (ResultList[i].Sta == 0)
                {
                    number++;
                }
            }
            return number;
        }
        // 增加产品数据到数据库
        public int addProductDataToDb()
        {
            //try
            //{
            //    if (GUID == "")
            //    {
            //        GUID = Guid.NewGuid().ToString();
            //    }

            //    string sql = string.Format("insert into ProductData (GUID,WorkSheet,PcbNumber,code1,code2,curStation,curSubStation,BurnStation,BeginTime,EndTime,isFinish) values ('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7'},'{8'},{9}) ", GUID, WorkSheet, PcbNumber, code1, code2, curStation, curSubStation, BurnStation, BeginTime, EndTime, isFinish);

            //    bool suc = DbMsSql.Instance.ExecuteSQLNonquery(sql);
            //    if (suc == false)
            //    {
            //        LogLib.Log.Warning("addProductDataToDb 错误：" + sql);
            //        return -1;
            //    }
            //}
            //catch (Exception e)
            //{
            //    LogLib.Log.Warning("addResultToDb 错误：" + e.Message);
            //    return -1;
            //}
            return 0;
        }

        // 增加产品结果到数据库---只有工位1调用这函数
        public int addResultToDb()
        {
            //try
            //{
            //    string sql = "";
            //    for (int i = 0; i < ResultList.Count; i++)
            //    {
            //        string sguid=Guid.NewGuid().ToString();
            //        sql = string.Format("insert into ProductResult (GUID,mGUID,XH,Image1sta) values ('{0}','{1}',{2},{3})",sguid,GUID,i,ResultList[i].Image1sta);
            //        bool suc=DbMsSql.Instance.ExecuteSQLNonquery(sql);
            //        if (suc == false)
            //        {
            //            LogLib.Log.Warning("addResultToDb 错误：" + sql);
            //            return -1;
            //        }
            //    }
            //}
            //catch (Exception e)
            //{

            //    LogLib.Log.Warning("addResultToDb 错误：" + e.Message);
            //    return -1;
            //}
            return 0;
        }

        /* 更新产品结果到数据库

         * 参数
         *     resultType  更新结果的类型，更新不同的结果
         * 
         * */
        public int UpdataResultToDb(int resultType)
        {
            try
            {

            }
            catch (Exception e)
            {
                //LogLib.Log.Warning("UpdataResultToDb 错误：" + e.Message);
            }
            return 0;
        }
        /// <summary>
        /// 增加产品日志到数据库
        /// </summary>
        /// <param name="LogMsg">日志内容</param>
        /// <returns></returns>
        public int AddProductLogToDb(string LogMsg)
        {
            try
            {
                //LogLib.Log.Info(LogMsg);
            }
            catch (Exception e)
            {
                //LogLib.Log.Warning("UpdataResultToDb 错误：" + e.Message);
            }
            return 0;
        }
    }
}
