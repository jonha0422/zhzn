﻿using GenericLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace CamaeraLib
{
    public class CameraSystem
    {
        private static CameraSystem instance;
        private static object theLock = new object();
        object lockIO = new object();
        public List<CameraBase> CameraList;
        public List<CameraConfig> CameraConfigList;
        public static CameraSystem Instance
        {
            get
            {   
                lock (theLock)
                {
                    if (instance == null)
                    {
                        instance = new CameraSystem();
                        instance.Read();
                    }
                    return instance;
                }
            }
        }
        public CameraSystem()
        {
            CameraList = new List<CameraBase>();
            CameraConfigList = new List<CameraConfig>();
        }

        public bool Read()
        {
            Hasher Hs = new Hasher();
            int sn = Hs.ChkSN2();
            if (sn == -1)
            {
                MessageBox.Show("SN Error:系统未注册!");
                return false;
            }
            
            CameraConfigList.Clear();
            using (TextReader reader1 = new StreamReader(Application.StartupPath + "\\Config\\CameraConfig.xml"))
            //using (TextReader reader1 = new StreamReader("MotionCardConfig.xml"))
            {
                XmlSerializer serializer1 = new XmlSerializer(typeof(List<CameraConfig>));
                CameraConfigList = (List<CameraConfig>)serializer1.Deserialize(reader1);
                reader1.Close();
            }
            
            if (CameraConfigList.Count == 0)
            {
                CameraConfigList.Add(new CameraConfig());
                Writer();
            }
            for (int i = 0; i < CameraConfigList.Count; i++)
            {
                if (CameraConfigList[i].Type == "DH")
                {
                    CameraBase aCamera = new Camera.CameraDH();
                    CameraList.Add(aCamera);
                    aCamera._CameraConfig = CameraConfigList[i];
                    if (CameraConfigList[i].Name == "")
                    {
                        aCamera.OpenCamByID((short)i);
                    }
                    else
                    {
                        aCamera.OpenCameraByName(CameraConfigList[i].Name);
                    }
                }
                else if (CameraConfigList[i].Type == "Basler")
                {
                    CameraBase aCamera = new Camera.CameraBasler();
                    CameraList.Add(aCamera);
                    aCamera._CameraConfig = CameraConfigList[i];
                    if (CameraConfigList[i].Name == "")
                    {
                        aCamera.OpenCamByID((short)i);
                    }
                    else
                    {
                        aCamera.OpenCameraByName(CameraConfigList[i].Name);
                    }
                }
                else if (CameraConfigList[i].Type == "Daheng")
                {
                    Camera.CameraDaheng aCamera = new Camera.CameraDaheng();
                    CameraList.Add(aCamera);
                    aCamera._CameraConfig = CameraConfigList[i];
                    if (CameraConfigList[i].Name == "")
                    {
                        aCamera.OpenCamByID((short)i);
                    }
                    else
                    {
                        aCamera.OpenCameraByName(CameraConfigList[i].Name);
                    }
                }
            }
            return true;
        }
        public bool Writer()
        {
            StreamWriter writer = new StreamWriter(Application.StartupPath + "\\Config\\CameraConfig.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(List<CameraConfig>));
            serializer.Serialize(writer, CameraConfigList);
            writer.Close();
            return true;
        }
        public int CloseALl()
        {
            for (int i = 0; i < CameraList.Count; i++)
            {
                CameraList[i].Close();
            }
            return 0;
        }
    }
}
