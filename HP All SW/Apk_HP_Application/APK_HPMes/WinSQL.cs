﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using APK_HP.MES.HPSql;

namespace APK_HPMes
{
    public partial class WinSQL : Form
    {
        public WinSQL()
        {
            InitializeComponent();
        }
        string QrTest = "QrTest0003";
        string PCBID = "PCBIDTest0008";
        private void 绑盘_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.SQL_TrayBind("QrTest0006", "", "HP495", "PCBIDTest0009");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.Connect();
        }
        ItemParameter itemParameter = new ItemParameter();
        private void 上料_Click(object sender, EventArgs e)
        { 
            HPSQLService._instance.FeddingStationDate("QrTest0006", "PCBIDTest0009", itemParameter);
        }

        private void 上料历史_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.FeddingAddHistoryData("QrTest0006", "PCBIDTest0009");
        }

        private void 烧录历史_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.BurnAddHistoryData("QrTest0006", "PCBIDTest0009");
        }

        private void 打码历史_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.HitCodeAddHistoryData("QrTest0006", "PCBIDTest0009");
        }

        private void 切割历史_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.CuttingAddHistoryData("QrTest0006", "PCBIDTest0009");
        }

        private void 包装历史_Click(object sender, EventArgs e)
        {
          HPSQLService._instance. PackagingAddHistoryData("QrTest0006", "PCBIDTest0009");
        }

        private void 底部检测历史_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.BottomAddHistoryData("QrTest0006", "PCBIDTest0009");
        }

        private void 顶部检测历史_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.TopTestAddHistoryData("QrTest0006", "PCBIDTest0009");
        }

        private void 烧录_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.BurnStationDate("QrTest0006", "PCBIDTest0009", itemParameter);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.HitCodeStationDate("QrTest0006", "PCBIDTest0009", itemParameter);
        }

        private void 切割_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.CuttingDataStationDate("QrTest0006", "PCBIDTest0009", itemParameter);
        }

        private void 顶部检测_Click(object sender, EventArgs e)
        {
        }

        private void 底部检测_Click(object sender, EventArgs e)
        {

        }

        private void 包装_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.PackagingStationData("QrTest0006", "PCBIDTest0009", itemParameter);
        }

        private void 解盘_Click(object sender, EventArgs e)
        {
            HPSQLService._instance.SQL_TrayUnBind("QrTest0006", "PCBIDTest0009");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //short[] aa = new short[100];
            //var a = HPSQLService._instance.StringConversionShortArray("1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0");
            //var b = HPSQLService._instance.ShortArrayConversionString(a);
            //short[,] aaa = new short[2, 5] { { 1, 2, 3, 5, 6 }, { 1, 2, 3, 4, 5 } };
            //var ss = HPSQLService._instance.ShortArrayConversionString(aa);
            //var gg = HPSQLService._instance.StringConversionShortArrayDimen(ss);
            var a = HPSQLService._instance.StringConversionShortArrayDimen("1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;46;47;48;49;50;51;52;53;54;55;56;57;58;59;60;61;62;63;64;65;66;67;68;69;70;71;72;73;74;75;76;77;78;79;80;81;82;83;84;85;86;87;88;89;90;91;92;93;94;95;96;97;98;99;100;");
            var b = HPSQLService._instance.ShortArrayConversionString(a);
            var c = HPSQLService._instance.StringConversionShortArrayDimen(b);
        }

        private void WinSQL_Load(object sender, EventArgs e)
        {

        }
    }
}
