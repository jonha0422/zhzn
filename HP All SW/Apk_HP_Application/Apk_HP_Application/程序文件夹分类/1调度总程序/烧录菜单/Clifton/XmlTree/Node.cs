using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

namespace Clifton.Windows.Forms.XmlTree
{
	public class Node : ISupportInitialize
	{
		protected string name;
		protected string refName;
		protected bool isReadOnly;
		protected string text;
		protected bool isRequired;
		protected string iconFilename;
		protected Icon icon;

		protected List<Popup> parentPopupItems;
		protected List<Popup> popupItems;
		protected List<Node> nodes;

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public string RefName
		{
			get { return refName; }
			set { refName = value; }
		}

		public bool IsReadOnly
		{
			get { return isReadOnly; }
			set { isReadOnly = value; }
		}

		public string Text
		{
			get { return text; }
			set { text = value; }
		}

		public bool IsRequired
		{
			get { return isRequired; }
			set { isRequired = value; }
		}

		public List<Popup> ParentPopupItems
		{
			get { return parentPopupItems; }
		}

		public List<Popup> PopupItems
		{
			get { return popupItems; }
		}

		public List<Node> Nodes
		{
			get { return nodes; }
		}

		public bool IsRef
		{
			get { return refName != null; }
		}

		public string IconFilename
		{
			get { return iconFilename; }
			set { iconFilename = value; }
		}

		public Icon Icon
		{
			get { return icon; }
			set { icon = value; }
		}

		public Node()
		{
			parentPopupItems = new List<Popup>();
			popupItems = new List<Popup>();
			nodes = new List<Node>();
		}

		public virtual void BeginInit()
		{
		}

		public virtual void EndInit()
		{
			if (iconFilename != null)
			{
				icon = new Icon(iconFilename);
			}
		}
	}
}
