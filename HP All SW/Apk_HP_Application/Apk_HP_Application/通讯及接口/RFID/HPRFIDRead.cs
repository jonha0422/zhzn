﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CommunicationZH
{
    class HPRFIDRead
    {
        public enum LEDDef
        {
            LED_OFF = 0,
            LED_RED,
            LED_GREEN,
            LED_YELLOW,
        }

        //连接读写器
        //参数：port->串口号，例："com1"
        //      addr->读卡器地址，这个数值由程序员指定，相当于给读卡器一个身份标识，范围0~9，
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //返回：成功true，失败返回false
        [DllImport("IDReaderAPI.dll", EntryPoint = "gConnReader", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gConnReader(string port, short addr = 0);

        //断开连接
        //参数：addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //成功返回true，失败返回false
        [DllImport("IDReaderAPI.dll", EntryPoint = "gDiscReader", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gDiscReader(short addr = 0);

        //设置读写器波特率
        //参数：baud->标准波特率
        //      addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //成功返回true，失败返回false
        [DllImport("IDReaderAPI.dll", EntryPoint = "gSetBaud", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gSetBaud(Int32 baud, short addr = 0);

        //设置串口等待时间
        //参数：time->等待时间，单位ms
        //      addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //成功返回true，失败返回false
        [DllImport("IDReaderAPI.dll", EntryPoint = "gSetTimeOut", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gSetTimeOut(short time, short addr = 0);

        //获取读写器型号
        //参数：model->返回读写器型号
        //      len  ->返回型号长度
        //      addr ->读卡器地址，必须与 gConnReader 函数的保持一致；
        //             缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //成功返回true，失败返回false
        [DllImport("IDReaderAPI.dll", EntryPoint = "gGetModel", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gGetModel(byte[] model, ref short len, short addr = 0);




        //设置LED颜色
        //参数：color->0x00 = 熄灭
        //             0x01 = 红色
        //             0x02 = 绿色
        //             0x03 = 黄色
        //      addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //成功返回true，失败返回false
        [DllImport("IDReaderAPI.dll", EntryPoint = "gSetLED", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gSetLED(byte color, int addr = 0);


        //读EM4100卡
        //参数：ID ->返回卡号，5 BYTES
        //      addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //读卡成功返回1，读卡失败返回2，通信失败返回3
        [DllImport("IDReaderAPI.dll", EntryPoint = "gReadEM4100", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gReadEM4100( byte[] ID, short addr = 0);

        //直通函数
        //参数：cmd     ->数据包命令
        //      para    ->数据包参数
        //      para_len->数据包参数长度
        //      resp    ->返回数据包数据
        //      resp_len->返回数据包数据长度
        //      addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //操作成功返回1，操作失败返回2，通信失败返回3
        [DllImport("IDReaderAPI.dll", EntryPoint = "gDirecteAccess", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short gDirecteAccess(short cmd, byte[] para, int para_len, byte[] resp, ref short resp_len, short addr = 0);



        //把 ATA5567 格式化成 EM4100 卡
        //参数：ID  ->EM4100卡号，5 BYTES
        //      lock->锁卡，0为不锁，否则锁卡
        //      addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //返回true表示与读写器通信成功，否则返回false，
        //可以调用 gReadEM4100 函数验证是否写卡成功
        [DllImport("IDReaderAPI.dll", EntryPoint = "ATA_WriteEM4100", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short ATA_WriteEM4100(byte[] ID, byte byteLock, short addr = 0);


        //把 EM4305 格式化成 EM4100 卡
        //参数：ID  ->EM4100卡号，5 BYTES
        //      addr->读卡器地址，必须与 gConnReader 函数的保持一致；
        //      lock->0为不锁，否则锁卡
        //            缺省为0，如果你的程序只操作一个读写器，建议采用缺省值；
        //返回0写卡成功，返回1写卡失败，返回2读写串口失败
        [DllImport("IDReaderAPI.dll", EntryPoint = "EM_WriteEM4100", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern short EM_WriteEM4100(byte[] ID, byte byteLock, short addr = 0);


    }
}
