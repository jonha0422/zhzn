using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace daitou
{
	public class TableControllerThree : XtreeNodeController
	{
		protected TableDefThree tableDefThree;
		protected TableDefTwo tableDef;

		public override string Name
		{
			get { return tableDefThree.Name; }
			set {tableDefThree.Name=value;}
		}
        public override string Pridata
        {
            get { return tableDefThree.Pridata; }
            set { tableDefThree.Pridata = value; }
        }
        public TableDefThree TableDefThree
		{
			get { return tableDefThree; }
		}

		public override object Item
		{
			get { return tableDefThree; }
		}

		public TableControllerThree()
		{
		}

		public TableControllerThree(TableDefThree tableDefThree)
		{
			this.tableDefThree = tableDefThree;
		}

		public override int Index(object item)
		{
			return tableDefThree.Fields.IndexOf(((TableControllerFour)item).TableDefFour);
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
            tableDef = ((TableControllerTwo)parentInstance).TableDefTwo;
			tableDefThree = new TableDefThree();
            tableDef.Fields.Add(tableDefThree);
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableControllerTwo)parentInstance).TableDefTwo;
            tableDef.Fields.Remove(tableDefThree);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableControllerTwo)parentInstance).TableDefTwo;
            tableDef.Fields.Remove(tableDefThree);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            tableDef = ((TableControllerTwo)parentInstance).TableDefTwo;
            tableDef.Fields.Insert(idx, tableDefThree);
		}

		public override void Select(TreeNode tn)
		{
			//Program.Properties.SelectedObject = tableDefThree;
		}
	}
}
