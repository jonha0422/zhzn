﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using ZHToolBox.HalconVision;
using HalconDotNet;
using static ZHToolBox.HalconVision.HDVisionTypes;

namespace ZHToolBox.PropertyGridRIOCollectorUI
{
    public partial class frm_TempleteResourceEditor : Form
    {
        public class DataProperty
        {
           public string ResourceName { get; set; }

            public DataProperty()
            {
                ResourceName = "";
            }
        }

        public DataProperty rapteClass = new DataProperty();

        public HalconForm Camera = new HalconForm("图像窗口");

        public frm_TempleteResourceEditor()
        {
            InitializeComponent();
            Size size = panel1.Size;
            Camera.TopLevel = false;
            Camera.FormBorderStyle = FormBorderStyle.None;
            panel1.Controls.Add(Camera);
            Camera.Size = size;
            Camera.Show();
            Camera.SendToBack();
            Camera.BringToFront();
            Camera.hAcqDriverName = "HuaRay";
            Camera.cameraName = "cam0";
            Camera.nPort = 0;

            btmCreateByFile.Checked = true;
            Text = "请先打开一张图片，再先技相应的RIO";
        }

        private void UpdateValues()
        {
            try
            {
                txtFileName.Text = rapteClass.ResourceName;
                if (rapteClass.ResourceName != null && rapteClass.ResourceName.Length > 0 && File.Exists(rapteClass.ResourceName))
                {
                    Camera.ShowImage(rapteClass.ResourceName);
                }
            }
            catch(Exception ex)
            {

            }
        }

        ~frm_TempleteResourceEditor()
        {
            Camera.Close();
        }

        private void DeleteRIO_Click(object sender, EventArgs e)
        {

        }

        private void UpdateRIO_Click(object sender, EventArgs e)
        {

        }

        private void frm_RIOCollectorUI_Load(object sender, EventArgs e)
        {
            UpdateValues();
        }

        private void btmAutoRowCol_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Resize(object sender, EventArgs e)
        {

        }

        private void frm_RIOCollectorUI_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void frm_RIOCollectorUI_SizeChanged(object sender, EventArgs e)
        {
            panel1.Location = new Point(0, 0);
            Camera.Location = new Point(0, 0);

            panel3.Location = new Point(0, this.ClientSize.Height - panel3.Height);

            Camera.Size = panel1.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - panel3.Size.Height);
        }

        private void frm_RIOCollectorUI_ResizeEnd(object sender, EventArgs e)
        {
            Size size = panel1.Size;
            Size size2 = this.Size;

            panel1.Location = new Point(0, 0);
            Camera.Location = new Point(0, 0);

            panel3.Location = new Point(0, this.ClientSize.Height - panel3.Height);

            Camera.Size = panel1.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - panel3.Size.Height);
        }

        private void frm_RIOCollectorUI_Resize(object sender, EventArgs e)
        {

        }

        private void frm_RIOCollectorUI_MaximumSizeChanged(object sender, EventArgs e)
        {
            panel1.Location = new Point(0, 0);
            Camera.Location = new Point(0, 0);

            panel3.Location = new Point(this.ClientSize.Width - panel3.Width, 0);

            panel1.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
            Camera.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
        }

        private void btmLoadFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog file = new OpenFileDialog();
                file.Title = "请选择图片";
                file.Filter = "(*.bmp)|*.bmp|(*.jpg)|*.jpg|(*.png)|*.png|(*.*)|*.*";
                //file.InitialDirectory = @"E:\计算机资料\01 C#视频\基础实训4\1110C#基础\资料\img";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    rapteClass.ResourceName = file.FileName;
                    txtFileName.Text = rapteClass.ResourceName;
                    if(chkUseDir.Checked)
                    {
                        string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                        string exeFolderPath = System.IO.Path.GetDirectoryName(exeFileName);
                        if (rapteClass.ResourceName.Contains(exeFolderPath))
                        {
                            rapteClass.ResourceName = rapteClass.ResourceName.Replace($"{exeFolderPath}\\", "");
                            txtFileName.Text = rapteClass.ResourceName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btmCreateByFile_CheckedChanged(object sender, EventArgs e)
        {
            if(btmCreateByFile.Checked)
            {
                txtRIO.Text = "";
                txtRIO.Enabled = false;
                btmSaveRIO.Enabled = false;

                txtFileName.Text = "";
                txtFileName.Enabled = true;
                btmLoadFile.Enabled = true;
            }
        }

        private void btmCreatedByRIO_CheckedChanged(object sender, EventArgs e)
        {
            if (btmCreatedByRIO.Checked)
            {
                txtFileName.Text = "";
                txtFileName.Enabled = false;
                btmLoadFile.Enabled = false;

                txtRIO.Text = "";
                txtRIO.Enabled = true;
                btmSaveRIO.Enabled = true;
            }
        }

        private void btmSaveRIO_Click(object sender, EventArgs e)
        {
            try
            {
                RIO_DES rio = new RIO_DES();
                rio.InitRIO();

                if (!Camera.IsContainImage())
                    throw new Exception("请选择一张图片!");

                if (Camera.m_RIO[0].fCol <= -1 || Camera.m_RIO[0].fRow <= -1)
                    throw new Exception("请先使用RIO_1(S1) 选中一个区域!");

                HTuple tupleRow = new HTuple();
                HTuple tupleCol = new HTuple();
                HTuple tuplePhi = new HTuple();
                HTuple tupleLen1 = new HTuple();
                HTuple tupleLen2 = new HTuple();

                if (Camera.m_RIO[0].fRow > 0 && Camera.m_RIO[0].fCol > 0)
                {
                    tupleRow.Append(Camera.m_RIO[0].fRow);
                    tupleCol.Append(Camera.m_RIO[0].fCol);
                    tuplePhi.Append(Camera.m_RIO[0].fPhi);
                    tupleLen1.Append(Camera.m_RIO[0].fLen1);
                    tupleLen2.Append(Camera.m_RIO[0].fLen2);
                }

                if (Camera.m_RIO[1].fRow > 0 && Camera.m_RIO[1].fCol > 0)
                {
                    tupleRow.Append(Camera.m_RIO[1].fRow);
                    tupleCol.Append(Camera.m_RIO[1].fCol);
                    tuplePhi.Append(Camera.m_RIO[1].fPhi);
                    tupleLen1.Append(Camera.m_RIO[1].fLen1);
                    tupleLen2.Append(Camera.m_RIO[1].fLen2);
                }

                SaveFileDialog file = new SaveFileDialog();
                file.Title = "请选择图片";
                file.Filter = "(*.bmp)|*.bmp|(*.jpg)|*.jpg";
                rio = Camera.m_RIO[0];
                if (file.ShowDialog() == DialogResult.OK)
                {

                    string strNames = file.FileName;

                    HObject RIOImage;
                    HalconGeneral.RIO_To_Image(Camera.hImage, out RIOImage, strNames, tupleRow, tupleCol, tuplePhi, tupleLen1, tupleLen2, true);

                    Camera.ShowImage(strNames);

                    rapteClass.ResourceName = strNames;
                    txtRIO.Text = rapteClass.ResourceName;

                    if (chkUseDir.Checked)
                    {
                        string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                        string exeFolderPath = System.IO.Path.GetDirectoryName(exeFileName);
                        if (rapteClass.ResourceName.Contains(exeFolderPath))
                        {
                            rapteClass.ResourceName = rapteClass.ResourceName.Replace($"{exeFolderPath}\\", "");
                            txtRIO.Text = rapteClass.ResourceName;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, $"保存图片出现异常: {ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
