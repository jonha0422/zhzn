﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using Apk_HP_Application.程序文件夹分类._0全局全量;
using APK_HP.MES.HPSql;
using daitou;
using Clifton.Tools.Strings;
using Clifton.Windows.Forms;
using Clifton.Windows.Forms.XmlTree;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public partial class frm_AutomaticSmartLine : DevExpress.XtraEditors.XtraForm
    {
        SynchronizationContext m_SyncContext = null;
        bool WorkOderChanged = false;

        public frm_AutomaticSmartLine()
        {
            InitializeComponent();

            #region>>>>>初始化表格
            gridFlagMark.Rows.Clear();
            int nRow = gridFlagMark.Rows.Add(10);
            for (int nIndexRow = 0; nIndexRow < 10; nIndexRow++)
            {
                gridFlagMark.Rows[nIndexRow].HeaderCell.Value = (nIndexRow + 1).ToString();
                gridFlagMark.Rows[nIndexRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                gridFlagMark.Rows[nIndexRow].Height = 20;
                /*                gridFlashResult.Rows[nIndexRow].DefaultHeaderCellType.*/
            }

//             int nCountOfClomn = gridFlagMark.Columns.Count;
//             for (int nIndexCol = 0; nIndexCol < nCountOfClomn; nIndexCol++)
//             {
//                 gridFlagMark.Columns[nIndexCol].Width = 20;
//                 gridFlagMark.Columns[nIndexCol].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
//             }


            gridFlagMark.TopLeftHeaderCell.Value = "行/例";
            gridFlagMark.RowHeadersWidth = 60;

            gridFlagMark.Refresh();
            #endregion

            m_SyncContext = SynchronizationContext.Current;
            HPInvoke.frm_MainPanel = this;

            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw |
                               ControlStyles.OptimizedDoubleBuffer |
                               ControlStyles.AllPaintingInWmPaint, true);
            this.UpdateStyles();

            timer1.Enabled = false;

            string errorMsg = $"\r\n！！！启动总调度之前请确保下位机及所有PLC开启，否则将导致数据不正常！！！\r\n\r\n确认之后点击  继续 \r\n\r\n退出请点击  终止";
            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
            emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
            if (actionHandle_result == emActionHandle.ActionHandle_Abort || actionHandle_result == emActionHandle.ActionHandle_Stop)
            {
                HPglobal.m_bQuitApp = true;
                HPglobal.QuitApplicationAndClear();
                System.Environment.Exit(-1);
            }

            propertyGrid1.SelectedObject = HPInvoke.ObjAllParameters;
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(SplashScreen1));

            try
            {
                HPInvoke.ObjAllParameters = (ProcessSaveFile)ZHToolBox.General.ZHToolBoxGeneral.DeserializeObjFromFile<ProcessSaveFile>($"ConfigFile\\{HPglobal.m_strModelName}\\HPInvokeParameters.xml");
                propertyGrid1.SelectedObject = HPInvoke.ObjAllParameters;

                #region>>>>>>>添加工位面板
                HPInvoke.strStartMsg = "添加工位面板........";
                Size frmSize = HPInvoke.LoadStation.Size;
                panelControl6.Controls.Add(HPInvoke.LoadStation);
                HPInvoke.LoadStation.Show();

                panelControl5.Controls.Add(HPInvoke.Flash1Station);
                HPInvoke.Flash1Station.Show();

                panelControl4.Controls.Add(HPInvoke.Flash2Station);
                HPInvoke.Flash2Station.Show();

                panelControl3.Controls.Add(HPInvoke.LaserStation);
                HPInvoke.LaserStation.Show();

                panelControl2.Controls.Add(HPInvoke.UnloadStation);
                HPInvoke.UnloadStation.Show();

                panelControl1.Controls.Add(HPInvoke.PackingStation);
                HPInvoke.PackingStation.Show();
                #endregion

                #region>>>>>>>初始化硬件
                HPInvoke.strStartMsg = "初始化硬件........";
                if (!HPInvoke.Machine_Initialize())
                {
                    if (MessageBox.Show("！！！初始化设备异常，建议重新启动程序！！！请慎重启动！！！\n\nYes: 继续运\nNo: 退出程序\n\n",
                                                    "!!!确认!!!(安全退出程序)",
                                                    MessageBoxButtons.YesNo,
                                                    MessageBoxIcon.Question,
                                                    MessageBoxDefaultButton.Button2) == DialogResult.No)
                        System.Environment.Exit(-1);
                }
                #endregion

                bool bInitalizeHW = true; //调试用，无意义
                if(bInitalizeHW)
                {
                    Thread.Sleep(2000);

                    #region>>>>>>>初始化调度工艺参数并写入
                    Set_Invokde_Values_ToCtrl();
                    #endregion

                    WriteInvokeInformationToAllPLC();

                    #region>>>>>>>清除设备错误
                    btmInvoke_ClearErrorMachine_Click(null, null);
                    Thread.Sleep(500);
                    btmInvoke_ClearErrorMachine_Click(null, null);
                    Thread.Sleep(500);
                    btmInvoke_ClearErrorMachine_Click(null, null);
                    #endregion
                }

                #region>>>>>>>建立烧录菜单
                CreateRootNode();
                txtFlashMenuFile.Text = HPInvoke.ObjAllParameters.schemaFilename;
                txtFlashNumFile.Text = HPInvoke.ObjAllParameters.FlashCFilename;
                chk切换行例数据.Checked = HPInvoke.ObjAllParameters.flashDataMode;
                HPInvoke.panel_Flash1.FlashResultSwitchDataMode(HPInvoke.ObjAllParameters.flashDataMode);
                HPInvoke.panel_Flash2.FlashResultSwitchDataMode(HPInvoke.ObjAllParameters.flashDataMode);
                
                #endregion

                Thread threadWR = new Thread(new ParameterizedThreadStart(PrcessSeverTestHerat));
                threadWR.Start("1");

                Thread threadMonitor = new Thread(new ParameterizedThreadStart(thread_ForStatusMonitor));
                threadMonitor.Start("1");

                timer1.Enabled = true;

            }
            catch(Exception ex)
            {
                MessageBox.Show($"frm_AutomaticSmartLine 初始化异常: {ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
        }

        public void Set_Invokde_Values_ToCtrl()
        {
            try
            {
                //============================================================
                //调度系统参数(所有工位)
                //============================================================
                上料是否翻转.Checked = HPInvoke.ObjAllParameters.chk_Rotate_Load;
                烧录是否翻转.Checked = HPInvoke.ObjAllParameters.chk_Rotate_Flash1;
                烧录2是否翻转.Checked = HPInvoke.ObjAllParameters.chk_Rotate_Flash2;
                打码切割是否翻转.Checked = HPInvoke.ObjAllParameters.chk_Rotate_Laser;
                下料组盘是否翻转.Checked = HPInvoke.ObjAllParameters.chk_Rotate_Unload;
                包装是否翻转.Checked = HPInvoke.ObjAllParameters.chk_Rotate_Packing;

                //============================================================
                //烧录
                //============================================================
                /*                ObjAllParameters.nameOfType;*/

                //============================================================
                //打码切割
                //============================================================
                /*                ObjAllParameters.laserPN;*/

                //============================================================
                //下料组盘
                //============================================================
                UnloadCfg_Row.Value = HPInvoke.ObjAllParameters.cfg_Row;
                UnloadCfg_Col.Value = HPInvoke.ObjAllParameters.cfg_Col;
                UnloadCfg_TotalProduct.Value = HPInvoke.ObjAllParameters.totalProductCount;
                UnloadCfg_CountTray.Value = HPInvoke.ObjAllParameters.cfg_CountTray;

                UnloadCfg_Total.Value = HPInvoke.ObjAllParameters.cfg_Total;
                UnloadCfg_Remaind.Value = HPInvoke.ObjAllParameters.cfg_Remaind;

                //============================================================
                //包装
                //============================================================
                /*                ObjAllParameters.strSN = "12345";*/

                speTrayLength.Value = (decimal)HPInvoke.ObjAllParameters.TrayWidth;
                speTrayWidth.Value = (decimal)HPInvoke.ObjAllParameters.TrayHeight;
                speDstMaduo.Value = (decimal)HPInvoke.ObjAllParameters.MaduoDst;
                spePCBLength.Value = (decimal)HPInvoke.ObjAllParameters.APKWorkOrderInfo.PCBWidth;
                spePCBWidth.Value = (decimal)HPInvoke.ObjAllParameters.APKWorkOrderInfo.PCBHeight;
                spePCBDstRow.Value = (decimal)HPInvoke.ObjAllParameters.APKWorkOrderInfo.IntervalDisX;
                spePCBDstCol.Value = (decimal)HPInvoke.ObjAllParameters.APKWorkOrderInfo.IntervalDisY;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Set_Invokde_Values_ToCtrl  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

            }
        }

        public void thread_ForStatusMonitor(object wsarg)
        {
            //HPInvoke.BarcodeRequireEvent requirementObj = (HPInvoke.BarcodeRequireEvent)wsarg;

            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(500);
                try
                {
                    #region>>>>>初始化数据
                    HPInvoke.IsLoadStationReady = HPInvoke.LoadStation.IsReady();
                    HPInvoke.IsFlash1StationReady = HPInvoke.Flash1Station.IsReady();
                    HPInvoke.IsFlash2StationReady = HPInvoke.Flash2Station.IsReady();
                    HPInvoke.IsLaserStationReady = HPInvoke.LaserStation.IsReady();
                    HPInvoke.IsUnLoadStationReady = HPInvoke.UnloadStation.IsReady();
                    HPInvoke.IsPackingStationReady = HPInvoke.PackingStation.IsReady();
                    HPInvoke.IsRobotProgramReady = HPInvoke.panel_RobotUnload.IsReady();

                    上料是否翻转_CheckedChanged(null, null);
                    烧录是否翻转_CheckedChanged(null, null);
                    打码切割是否翻转_CheckedChanged(null, null);
                    下料组盘是否翻转_CheckedChanged(null, null);
                    包装是否翻转_CheckedChanged(null, null);

                    #endregion
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_ForStatusMonitor  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        private void labelControl9_Click(object sender, EventArgs e)
        {

        }

        private void frm_ASL_Closing(object sender, FormClosingEventArgs e)
        {

        }

        private void frm_ASL_Load(object sender, EventArgs e)
        {
            if(HPglobal.m_strUserApp == "Administrator")
                SetPrivilegy("管理员");
            else
                SetPrivilegy("操作员");
        }

        private void frm_ASL_SizeChanged(object sender, EventArgs e)
        {

        }

        private void frm_ASL_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void frm_ASL_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void frm_ASL_HelpBtmClick(object sender, CancelEventArgs e)
        {

        }

        private void frm_ASL_ControlClick(object sender, EventArgs e)
        {

        }

        private void frm_ASL_ControlDoubleClick(object sender, EventArgs e)
        {

        }

        private void frm_ASL_ContrlMouseHover(object sender, EventArgs e)
        {

        }

        private void frm_ASL_ContrlDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void frm_ASL_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void btmInvoke_ResetMachine_Click(object sender, EventArgs e)
        {
            if(!WorkOderChanged)
            {
                string errorMsg = $"\r\n 当前没有录入工单，如果继续运行将以目前的参数运行\r\n请慎重选择启动\r\n";
                emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
            }

            if (HPglobal.emMachineState != HPglobal.emMachineWorkingState.emMachineWorkingState_NotReset &&
                HPglobal.emMachineState != HPglobal.emMachineWorkingState.emMachineWorkingState_Idle)
            {
                MessageBox.Show("设备未停止或设备在工作状态中，请先停止设备！", "确认", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Reseting;
            Task<bool> taskReset = Task.Factory.StartNew<bool>(() => { return HPInvoke.Machine_Reset(); });
            while(taskReset.Status == TaskStatus.WaitingToRun || taskReset.Status == TaskStatus.Running)
            {
                Application.DoEvents();
            }

            taskReset.Wait();
            if (!taskReset.Result)
            {
                MessageBox.Show("！！！复位设备异常，建议重新启动程序！！！请慎重启动！！！\n\nYes: 退出程序\nNo: 继续运行\n\n",
                                                 "!!!确认!!!(安全退出程序)",
                                                 MessageBoxButtons.OK,
                                                 MessageBoxIcon.Error);


            }
            else
            {
                MessageBox.Show("！！！复位设备成功，请启动设备！！",    "!!!确认!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btmInvoke_StartMachine_Click(object sender, EventArgs e)
        {
            if (HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Working)
            {
                MessageBox.Show("设备已经在工作当中！", "确认", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!WorkOderChanged)
            {
                string errorMsg = $"\r\n 当前没有录入工单，如果继续运行将以目前的参数运行\r\n请慎重选择启动\r\n";
                emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
            }

            if (!WorkOderChanged)
            {
                string errorMsg = $"当前没有录入工单，如果继续运行将以目前的参数运行\r\n请慎重选择启动\r\n";
                HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                emActionHandle actionHandle_result = HPglobal.HandlerShow(errorMsg);
                if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                {}
                else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                { return; }
            }

            Task<bool> taskReset = Task.Factory.StartNew<bool>(() => { return HPInvoke.Machine_Start(); });
            while (taskReset.Status == TaskStatus.WaitingToRun || taskReset.Status == TaskStatus.Running)
            {
                Application.DoEvents();
            }

            taskReset.Wait();
            if (!taskReset.Result)
            {
                HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                MessageBox.Show("！！！启动设备失败！！！！！", "!!!确认!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            HPglobal.emMachineState = HPglobal.emMachineWorkingState.emMachineWorkingState_Working;
        }

        private void btmInvoke_StopMachine_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("！！！确定强制停止设备！！！\n\nYes: 停止\nNo: 继续运行\n\n",
                                                 "!!!确认!!!(安全退出程序)",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                return;

            Task<bool> taskReset = Task.Factory.StartNew<bool>(() => { return HPInvoke.Machine_Stop(); });
            while (taskReset.Status == TaskStatus.WaitingToRun || taskReset.Status == TaskStatus.Running)
            {
                Application.DoEvents();
            }

            taskReset.Wait();
            if (!taskReset.Result)
            {
                MessageBox.Show("！！！停止设备失败！！！！！", "!!!确认!!!", MessageBoxButtons.OK,MessageBoxIcon.Error);

            }
            else
            {
                MessageBox.Show("！！！停止设备成功，请启动设备！！", "!!!确认!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void UpdateStatus()
        {
            try
            {
                #region>>>>显示设备的连接状态
                //===============================
                chkReadyLoad.Checked = HPInvoke.IsLoadStationReady;
                chkReadyFlash1.Checked = HPInvoke.IsFlash1StationReady;
                chkReadyFlash2.Checked = HPInvoke.IsFlash2StationReady;
                chkReadyLaser.Checked = HPInvoke.IsLaserStationReady;
                chkReadyUnload.Checked = HPInvoke.IsUnLoadStationReady;
                chkReadyPacking.Checked = HPInvoke.IsPackingStationReady;
                #endregion

                txtD2001.Text = HPInvoke.SafeMonitorArry1 == null ? "error" : HPInvoke.SafeMonitorArry1[0].ToString();
                txtD2002.Text = HPInvoke.SafeMonitorArry1 == null ? "error" : HPInvoke.SafeMonitorArry1[1].ToString();

                txtD2003.Text = HPInvoke.SafeMonitorArry2 == null ? "error" : HPInvoke.SafeMonitorArry2[0].ToString();
                txtD2004.Text = HPInvoke.SafeMonitorArry2 == null ? "error" : HPInvoke.SafeMonitorArry2[1].ToString();

                txtD2005.Text = HPInvoke.SafeMonitorArry3 == null ? "error" : HPInvoke.SafeMonitorArry3[0].ToString();
                txtD2006.Text = HPInvoke.SafeMonitorArry3 == null ? "error" : HPInvoke.SafeMonitorArry3[1].ToString();

                txtD2007.Text = HPInvoke.SafeMonitorArry4 == null ? "error" : HPInvoke.SafeMonitorArry4[0].ToString();
                txtD2008.Text = HPInvoke.SafeMonitorArry4 == null ? "error" : HPInvoke.SafeMonitorArry4[1].ToString();

                txtD2009.Text = HPInvoke.SafeMonitorArry5 == null ? "error" : HPInvoke.SafeMonitorArry5[0].ToString();
                txtD2010.Text = HPInvoke.SafeMonitorArry5 == null ? "error" : HPInvoke.SafeMonitorArry5[1].ToString();

                txtD2011.Text = HPInvoke.SafeMonitorArry6 == null ? "error" : HPInvoke.SafeMonitorArry6[0].ToString();
                txtD2012.Text = HPInvoke.SafeMonitorArry6 == null ? "error" : HPInvoke.SafeMonitorArry6[1].ToString();

                txtD2013.Text = HPInvoke.SafeMonitorArry7 == null ? "error" : HPInvoke.SafeMonitorArry7[0].ToString();
                txtD2014.Text = HPInvoke.SafeMonitorArry7 == null ? "error" : HPInvoke.SafeMonitorArry7[1].ToString();

                //===================================================================
                cmbUnloadPCBs.Items.Clear();
                if (HPInvoke.ObjAllParameters.UnloadPCBs != null && HPInvoke.ObjAllParameters.UnloadPCBs.Length != cmbUnloadPCBs.Items.Count)
                    cmbUnloadPCBs.Items.AddRange(HPInvoke.ObjAllParameters.UnloadPCBs);

                //===================================================================
                cmbPackingPCBs.Items.Clear();
                if (HPInvoke.m_queUnloadPCBs.Count > 0)
                {
                    string[] strPackingPCBIDs = HPInvoke.m_queUnloadPCBs.ToArray();
                    cmbPackingPCBs.Items.AddRange(strPackingPCBIDs);
                }

                if(HPInvoke.ProductInfoData_Unload != null &&
                    (HPInvoke.ProductInfoData_Unload[1] * HPInvoke.ProductInfoData_Unload[2] != 0)) //数据有效
                {
                    //===================================================================
                    spedUnloadPackedCount.Value = HPInvoke.ProductInfoData_Unload[31];
                    spedUnloadPackedCount2.Value = HPInvoke.ProductInfoData_Unload[32];

                    spedPackingPackedCount.Value = HPInvoke.ProductInfoData_Packing[31];
                    spedPackingPackedCount2.Value = HPInvoke.ProductInfoData_Packing[32];

                    当前标签包装数量1068.Value = HPInvoke.ProductInfoData_Packing[38];

                    sped当前坏品总数量2.Value = sped当前坏品总数量.Value = HPInvoke.ProductInfoData_Unload[33];
                    sped生产应上盘数.Value = HPInvoke.ProductInfoData_Unload[34];
                    sped生产实际总盘数2.Value = sped生产实际总盘数.Value = HPInvoke.ActureTrayCountOfLoaded = HPInvoke.ProductInfoData_Unload[35];

                    int checkPassed = (((int)HPInvoke.ProductInfoData_Unload[40] & 0x00FFFF) << 16) | ((int)HPInvoke.ProductInfoData_Unload[39] & 0x00FFFF);
                    txt已识别的好品数量.Text = checkPassed.ToString();
                    //===================================================================
                    Sped包装总量.Value = HPInvoke.ProductInfoData_Unload[11];
                    spinEdit4.Value = HPInvoke.ProductInfoData_Unload[12];
                    UnloadCfg_CountTrayBK.Value = HPInvoke.ProductInfoData_Unload[17];
                    spinEdit2.Value = HPInvoke.ProductInfoData_Unload[13];
                    sped产品行数.Value = HPInvoke.ProductInfoData_Unload[1];
                    sped产品例数.Value = HPInvoke.ProductInfoData_Unload[2];
                    CurrentCountTray.Value = HPInvoke.ProductInfoData_Unload[10];
                    Sped生产总量2.Value = Sped生产总量.Value = (((int)HPInvoke.ProductInfoData_Unload[15] & 0x00FFFF) << 16) | ((int)HPInvoke.ProductInfoData_Unload[14] & 0x00FFFF);
                    TotalTrayOfLineOut.Value = HPInvoke.ProductInfoData_Unload[16];
                    TotalTrayOfLineOutPCS.Value = TotalTrayOfLineOut.Value * UnloadCfg_Row.Value * UnloadCfg_Col.Value;

                    if (Sped生产总量.Value % (UnloadCfg_Row.Value * UnloadCfg_Col.Value) == 0)
                        sped生产总盘数2.Value = sped生产总盘数.Value = (short)(Sped生产总量.Value / (UnloadCfg_Row.Value * UnloadCfg_Col.Value));
                    else
                        sped生产总盘数2.Value = sped生产总盘数.Value = (short)(Sped生产总量.Value / (UnloadCfg_Row.Value * UnloadCfg_Col.Value) + 1);

                    生产最后打包总盘数.Value = (short)(sped生产总盘数.Value % UnloadCfg_CountTrayBK.Value);
                    if (生产最后打包总盘数.Value == 0)
                        生产最后打包总盘数.Value = UnloadCfg_CountTrayBK.Value;

                    speCountOfLoadTray.Value = HPInvoke.ProductInfoData_Unload[18]; 
                    speCountOfBuffTray.Value = HPInvoke.ProductInfoData_Unload[19];
                    
                    //计算实需要投入盘的数量
                    int nActuralPlanCountAfterFailed = (int)(Sped生产总量.Value + sped当前坏品总数量.Value);
                    if (nActuralPlanCountAfterFailed % (UnloadCfg_Row.Value * UnloadCfg_Col.Value) == 0)
                        HPInvoke.PlanTrayCount_AutoCal = (short)(nActuralPlanCountAfterFailed / (UnloadCfg_Row.Value * UnloadCfg_Col.Value));
                    else
                        HPInvoke.PlanTrayCount_AutoCal = (short)(nActuralPlanCountAfterFailed / (UnloadCfg_Row.Value * UnloadCfg_Col.Value) + 1);

                    sped生产应上盘数2.Value = sped生产应上盘数.Value = HPInvoke.PlanTrayCount_AutoCal;

                    //总出货量
                    TotalLineOutAll2.Value = TotalLineOutAll.Value = HPInvoke.ActuralProductedCount = (((int)HPInvoke.ProductInfoData_Unload[21] & 0x00FFFF) << 16) | ((int)HPInvoke.ProductInfoData_Unload[20] & 0x00FFFF); //speCountOfLoadTray.Value + speCountOfBuffTray.Value + TotalTrayOfLineOutPCS.Value;

                    spedLaserRemaind2.Value = spedLaserRemaind.Value = HPInvoke.LaserRemaind_ForLaserStation = (int)(Sped生产总量.Value - TotalLineOutAll.Value);

                    sped最后托盘剩余个数2.Value = sped最后托盘剩余个数.Value = HPInvoke.countRemaind_ForLastTray;

                    //生产总量和实际出货量已相等
                    if (Sped生产总量.Value > 0 && HPInvoke.ActuralProductedCount > 0 && HPInvoke.ActuralProductedCount == Sped生产总量.Value)
                        HPInvoke.CurrentPlaneProductCompleted = true;
                    else
                        HPInvoke.CurrentPlaneProductCompleted = false;

                    if (sped当前坏品总数量2.Value + TotalLineOutAll2.Value > 0)
                    {
                        double uphTotal = ((double)TotalLineOutAll2.Value / ((double)(sped当前坏品总数量2.Value + TotalLineOutAll2.Value))) * 100.000;
                        良品率.Text = uphTotal.ToString("F2") + "%";
                    }
                    else
                        良品率.Text = "0.00%";

                    txtLastTrayID.Text = HPInvoke.strLastTryaID_ForLastProductMode ?? "";

                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_AutomaticSmartLine UpdateStatus 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                UpdateStatus();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("myStationForm timer1_Tick() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmInvoke_ClearErrorMachine_Click(object sender, EventArgs e)
        {
            Task task1 = Task.Factory.StartNew<bool>(() => HPInvoke.LoadStation.ClearError());
            Task task2 = Task.Factory.StartNew<bool>(() => HPInvoke.Flash1Station.ClearError());
            Task task3 = Task.Factory.StartNew<bool>(() => HPInvoke.Flash2Station.ClearError());
            Task task4 = Task.Factory.StartNew<bool>(() => HPInvoke.LaserStation.ClearError());
            Task task5 = Task.Factory.StartNew<bool>(() => HPInvoke.UnloadStation.ClearError());
            Task task6 = Task.Factory.StartNew<bool>(() => HPInvoke.PackingStation.ClearError());

            task1.Wait();
            task2.Wait();
            task3.Wait();
            task4.Wait();
            task5.Wait();
            task6.Wait();
        }

        public void WriteRotateAll()
        {
            try
            {
                HPInvoke.ObjAllParameters.chk_Rotate_Load = 上料是否翻转.Checked;
                HPInvoke.ObjAllParameters.chk_Rotate_Flash1 = 烧录是否翻转.Checked;
                HPInvoke.ObjAllParameters.chk_Rotate_Flash2 = 烧录2是否翻转.Checked;
                HPInvoke.ObjAllParameters.chk_Rotate_Laser = 打码切割是否翻转.Checked;
                HPInvoke.ObjAllParameters.chk_Rotate_Unload = 下料组盘是否翻转.Checked;
                HPInvoke.ObjAllParameters.chk_Rotate_Packing = 包装是否翻转.Checked;

                Task task1 = Task.Factory.StartNew<bool>(() => HPInvoke.LoadStation.stationBase.WriteInt16("D1030", 上料是否翻转.Checked ? (short)1 : (short)0));
                Task task2 = Task.Factory.StartNew<bool>(() => HPInvoke.Flash1Station.stationBase.WriteInt16("D1030", 烧录是否翻转.Checked ? (short)1 : (short)0));
                Task task3 = Task.Factory.StartNew<bool>(() => HPInvoke.Flash2Station.stationBase.WriteInt16("D1030", 烧录2是否翻转.Checked ? (short)1 : (short)0));
                Task task4 = Task.Factory.StartNew<bool>(() => HPInvoke.LaserStation.stationBase.WriteInt16("D1030", 打码切割是否翻转.Checked ? (short)1 : (short)0));
                Task task5 = Task.Factory.StartNew<bool>(() => HPInvoke.UnloadStation.stationBase.WriteInt16("D1030", 下料组盘是否翻转.Checked ? (short)1 : (short)0));
                Task task6 = Task.Factory.StartNew<bool>(() => HPInvoke.PackingStation.stationBase.WriteInt16("D1030", 包装是否翻转.Checked ? (short)1 : (short)0));

                task1.Wait();
                task2.Wait();
                task3.Wait();
                task4.Wait();
                task5.Wait();
                task6.Wait();
                propertyGrid1.SelectedObject = HPInvoke.ObjAllParameters;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteRotateAll 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void 上料是否翻转_CheckedChanged(object sender, EventArgs e)
        {
            Task task1 = Task.Factory.StartNew<bool>(() => HPInvoke.LoadStation.stationBase.WriteInt16("D1030", 上料是否翻转.Checked ? (short)1 : (short)0));
        }

        private void 烧录是否翻转_CheckedChanged(object sender, EventArgs e)
        {
            Task task2 = Task.Factory.StartNew<bool>(() => HPInvoke.Flash1Station.stationBase.WriteInt16("D1030", 烧录是否翻转.Checked ? (short)1 : (short)0));
        }

        private void 烧录2是否翻转_CheckedChanged(object sender, EventArgs e)
        {
            Task task3 = Task.Factory.StartNew<bool>(() => HPInvoke.Flash2Station.stationBase.WriteInt16("D1030", 烧录2是否翻转.Checked ? (short)1 : (short)0));
        }

        private void 打码切割是否翻转_CheckedChanged(object sender, EventArgs e)
        {
            Task task4 = Task.Factory.StartNew<bool>(() => HPInvoke.LaserStation.stationBase.WriteInt16("D1030", 打码切割是否翻转.Checked ? (short)1 : (short)0));
        }

        private void 下料组盘是否翻转_CheckedChanged(object sender, EventArgs e)
        {
            Task task5 = Task.Factory.StartNew<bool>(() => HPInvoke.UnloadStation.stationBase.WriteInt16("D1030", 下料组盘是否翻转.Checked ? (short)1 : (short)0));
        }

        private void 包装是否翻转_CheckedChanged(object sender, EventArgs e)
        {
            Task task6 = Task.Factory.StartNew<bool>(() => HPInvoke.PackingStation.stationBase.WriteInt16("D1030", 包装是否翻转.Checked ? (short)1 : (short)0));
        }

        private void btmAllOn_Click(object sender, EventArgs e)
        {
            上料是否翻转.Checked = true;
            烧录是否翻转.Checked = true;
            烧录2是否翻转.Checked = true;
            打码切割是否翻转.Checked = true;
            下料组盘是否翻转.Checked = true;
            包装是否翻转.Checked = true;

            WriteRotateAll();
        }

        private void btmAllOff_Click(object sender, EventArgs e)
        {
            上料是否翻转.Checked = false;
            烧录是否翻转.Checked = false;
            烧录2是否翻转.Checked = false;
            打码切割是否翻转.Checked = false;
            下料组盘是否翻转.Checked = false;
            包装是否翻转.Checked = false;

            WriteRotateAll();
        }

        private void btmSaveInvokeCfg_Click(object sender, EventArgs e)
        {
            try
            {
                if (保存时写入所有信息.Checked)
                    WriteInvokeInformationToAllPLC();

                ZHToolBox.General.ZHToolBoxGeneral.SerializeObjToFile<ProcessSaveFile>(HPInvoke.ObjAllParameters, $"ConfigFile\\{HPglobal.m_strModelName}\\HPInvokeParameters.xml");
                propertyGrid1.SelectedObject = HPInvoke.ObjAllParameters;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmSaveInvokeCfg_Click 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

        }

        private void 复位所有烧录结果数据_Click(object sender, EventArgs e)
        {
            HPInvoke.bLastProductMode = false;

            HPInvoke.ResetFlashAreaData(0, true);
            HPInvoke.ResetAllUnloadPCBs();

            HPInvoke.UnloadStation.stationBase.WriteInt16("D1061", (short) 0);
            HPInvoke.UnloadStation.stationBase.WriteInt16("D1062", (short)0);

            HPInvoke.PackingStation.stationBase.WriteInt16("D1061", (short) 0);
            HPInvoke.PackingStation.stationBase.WriteInt16("D1062", (short)0);

            short[] reset0 = new short[26];
            HPInvoke.UnloadStation.stationBase.WriteInt16("D1040", reset0);

            //重新写入工单数据
            WriteOrderInfoToProductionLine();
        }

        public void Get_Invokde_Values_FromCtrl()
        {
            //============================================================
            //调度系统参数(所有工位)
            //============================================================
            HPInvoke.ObjAllParameters.chk_Rotate_Load =上料是否翻转.Checked;
            HPInvoke.ObjAllParameters.chk_Rotate_Flash1 = 烧录是否翻转.Checked;
            HPInvoke.ObjAllParameters.chk_Rotate_Flash2 = 烧录2是否翻转.Checked ;
            HPInvoke.ObjAllParameters.chk_Rotate_Laser = 打码切割是否翻转.Checked ;
            HPInvoke.ObjAllParameters.chk_Rotate_Unload = 下料组盘是否翻转.Checked ;
            HPInvoke.ObjAllParameters.chk_Rotate_Packing = 包装是否翻转.Checked ;

            //============================================================
            //下料组盘
            //============================================================
            HPInvoke.ObjAllParameters.totalProductCount = (short)UnloadCfg_TotalProduct.Value;
            HPInvoke.ObjAllParameters.cfg_Row = (short) UnloadCfg_Row.Value;
            HPInvoke.ObjAllParameters.cfg_Col = (short) UnloadCfg_Col.Value;
            HPInvoke.ObjAllParameters.cfg_CountTray = (short) UnloadCfg_CountTray.Value;

            HPInvoke.ObjAllParameters.cfg_Total = (short)(HPInvoke.ObjAllParameters.cfg_Row * HPInvoke.ObjAllParameters.cfg_Col * HPInvoke.ObjAllParameters.cfg_CountTray); //每次包装的总量
            UnloadCfg_Total.Value = HPInvoke.ObjAllParameters.cfg_Total;

            decimal countPerTray = UnloadCfg_Row.Value * UnloadCfg_Col.Value;
            if (UnloadCfg_TotalProduct.Value % countPerTray == 0)
            {
                 UnloadCfg_Remaind.Value = HPInvoke.ObjAllParameters.cfg_Remaind = (short) countPerTray;
            }
            else
            {
                UnloadCfg_Remaind.Value = HPInvoke.ObjAllParameters.cfg_Remaind = (short)(UnloadCfg_TotalProduct.Value % countPerTray);
            }

            //============================================================
            //包装
            //============================================================
            HPInvoke.ObjAllParameters.TrayWidth = (double)speTrayLength.Value;
            HPInvoke.ObjAllParameters.TrayHeight = (double)speTrayWidth.Value;
            HPInvoke.ObjAllParameters.MaduoDst = (double)speDstMaduo.Value;
            HPInvoke.ObjAllParameters.APKWorkOrderInfo.PCBWidth = (double)spePCBLength.Value;
            HPInvoke.ObjAllParameters.APKWorkOrderInfo.PCBHeight = (double)spePCBWidth.Value;
            HPInvoke.ObjAllParameters.APKWorkOrderInfo.IntervalDisX = (double)spePCBDstRow.Value;
            HPInvoke.ObjAllParameters.APKWorkOrderInfo.IntervalDisY = (double)spePCBDstCol.Value;

            propertyGrid1.SelectedObject = HPInvoke.ObjAllParameters;

        }

        private void UnloadCfg_Total_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void UnloadCfg_Row_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void UnloadCfg_Col_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        public bool WriteInvokeInfomationToSinglePLC(myStationForm baseStationPLC)
        {
            try
            {
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1031", (short)UnloadCfg_Row.Value);
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1032", (short)UnloadCfg_Col.Value);

                HPInvoke.UnloadStation.stationBase.WriteInt16("D1041", (short)UnloadCfg_Total.Value);
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1042", (short)UnloadCfg_CountTray.Value);
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1043", (short)UnloadCfg_Remaind.Value);
                HPInvoke.UnloadStation.stationBase.WriteInt32("D1044", (int)UnloadCfg_TotalProduct.Value);
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1047", (short)UnloadCfg_CountTray.Value);

                HPInvoke.UnloadStation.stationBase.WriteInt32("D2200", (Int32)(speTrayLength.Value * 1000));
                HPInvoke.UnloadStation.stationBase.WriteInt32("D2202", (Int32)(speTrayWidth.Value * 1000));
                HPInvoke.UnloadStation.stationBase.WriteInt32("D2204", (Int32)(speDstMaduo.Value * 1000));
                HPInvoke.UnloadStation.stationBase.WriteInt32("D2206", (Int32)(spePCBLength.Value * 1000));
                HPInvoke.UnloadStation.stationBase.WriteInt32("D2208", (Int32)(spePCBWidth.Value * 1000));
                HPInvoke.UnloadStation.stationBase.WriteInt32("D2210", (Int32)(spePCBDstRow.Value * 1000));
                HPInvoke.UnloadStation.stationBase.WriteInt32("D2212", (Int32)(spePCBDstCol.Value * 1000));

                HPInvoke.UnloadStation.stationBase.WriteInt16("D2249", (short)(HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkFace == 0 ? 0 : 1));

                return true;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteInvokeInfomationToSinglePLC 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public bool WriteInvokeInformationToAllPLC()
        {
            try
            {
                Get_Invokde_Values_FromCtrl();

                Task task1 = Task.Factory.StartNew<bool>(() => WriteInvokeInfomationToSinglePLC(HPInvoke.LoadStation));
                Task task2 = Task.Factory.StartNew<bool>(() => WriteInvokeInfomationToSinglePLC(HPInvoke.Flash1Station));
                Task task3 = Task.Factory.StartNew<bool>(() => WriteInvokeInfomationToSinglePLC(HPInvoke.Flash2Station));
                Task task4 = Task.Factory.StartNew<bool>(() => WriteInvokeInfomationToSinglePLC(HPInvoke.LaserStation));
                Task task5 = Task.Factory.StartNew<bool>(() => WriteInvokeInfomationToSinglePLC(HPInvoke.UnloadStation));
                Task task6 = Task.Factory.StartNew<bool>(() => WriteInvokeInfomationToSinglePLC(HPInvoke.PackingStation));

                //定入打码定义字符
                Int16[] resetString = new Int16[16];
                for (int nIndex = 0; nIndex < 16; nIndex++)
                    resetString[nIndex] = 0x00;

                HPInvoke.LoadStation.stationBase.WriteInt16("D2250", resetString);
                HPInvoke.LoadStation.stationBase.WriteString("D2250", HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkText);

                HPInvoke.Flash1Station.stationBase.WriteInt16("D2250", resetString);
                HPInvoke.Flash1Station.stationBase.WriteString("D2250", HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkText);

                HPInvoke.Flash2Station.stationBase.WriteInt16("D2250", resetString);
                HPInvoke.Flash2Station.stationBase.WriteString("D2250", HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkText);

                HPInvoke.LaserStation.stationBase.WriteInt16("2250", resetString);
                HPInvoke.LaserStation.stationBase.WriteString("2250", HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkText);

                HPInvoke.UnloadStation.stationBase.WriteInt16("D2250", resetString);
                HPInvoke.UnloadStation.stationBase.WriteString("D2250", HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkText);

                HPInvoke.PackingStation.stationBase.WriteInt16("D2250", resetString);
                HPInvoke.PackingStation.stationBase.WriteString("D2250", HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkText);

                task1.Wait();
                task2.Wait();
                task3.Wait();
                task4.Wait();
                task5.Wait();
                task6.Wait();

                propertyGrid1.SelectedObject = HPInvoke.ObjAllParameters;

                return true;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteInvokeInformationToAllPLC 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        private void btmWriteUnload_Click(object sender, EventArgs e)
        {
            WriteInvokeInformationToAllPLC();
        }

        private void UnloadCfg_TotalProduct_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void UnloadCfg_CountTray_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        private void propertyGrid1_Click(object sender, EventArgs e)
        {
           
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
/*            InitializeGlobalData();*/
        }

        private void UnloadCfg_Remaind_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void 自动生成PCBID_Click(object sender, EventArgs e)
        {

            txtPCB虚拟编号.Text = DateTime.Now.ToString("yyyyMMddHHmmss");
        }

        private void 手动绑盘_Click(object sender, EventArgs e)
        {
            if(!HPInvoke.dataBaseSQL.SQL_TrayBind(txt托盘编号.Text.Trim(), txtWorkID.Text.Trim(), txtPCB板号.Text.Trim(), txtPCB虚拟编号.Text.Trim()))
            {
                MessageBox.Show(this, $"手动绑盘失败({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void 手动解盘_Click(object sender, EventArgs e)
        {
            if (!HPInvoke.dataBaseSQL.SQL_TrayUnBind(txt托盘编号.Text.Trim()))
            {
                MessageBox.Show(this, $"手动解盘失败({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void 设置工位结果_Click(object sender, EventArgs e)
        {
            if (txtPCB虚拟编号.Text.Trim().Length <= 0 || cmbEmStation.SelectedIndex < 0)
                return;

            if (!HPInvoke.dataBaseSQL.SQL_SetStationResultStatus(txtPCB虚拟编号.Text.Trim(), (APK_HP.MES.HPSql.WorkStationNameEnum)cmbEmStation.SelectedIndex, chkStationResultStatus.Checked))
            {
                MessageBox.Show(this, $"设置工位结果失败({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtPCB虚拟编号_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void chkStationResultStatus_CheckedChanged(object sender, EventArgs e)
        {
            if(chkStationResultStatus.Checked)
            {
                txtStatusDisplay.Text = "Passed";
                txtStatusDisplay.BackColor = Color.LightGreen;
            }
            else
            {
                txtStatusDisplay.Text = "Failed";
                txtStatusDisplay.BackColor = Color.Red;
            }

        }

        private void 设置工位结果数据_Click(object sender, EventArgs e)
        {
            if (txtPCB虚拟编号.Text.Trim().Length <= 0 || cmbEmStation.SelectedIndex < 0)
                return;

            ResultInformation result = new ResultInformation(HPInvoke.dataBaseSQL.SQL_GetResultFiledFromID((APK_HP.MES.HPSql.WorkStationNameEnum)cmbEmStation.SelectedIndex));
            result.Results[0].SetResult(memoEdit4.Text.Trim());
            if (!HPInvoke.dataBaseSQL.SQL_SetStationResult(txtPCB虚拟编号.Text.Trim(), (APK_HP.MES.HPSql.WorkStationNameEnum)cmbEmStation.SelectedIndex, result))
            {
                MessageBox.Show(this, $"设置工位结果数据失败({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void 得到托盘当前状态_Click(object sender, EventArgs e)
        {
            if (txt托盘编号.Text.Trim().Length <= 0 || cmbEmStation.SelectedIndex < 0)
                return;

            chkStationResultStatus.Checked = HPInvoke.dataBaseSQL.SQL_GetStationResultStatusFromTray(txt托盘编号.Text.Trim(), (APK_HP.MES.HPSql.WorkStationNameEnum)cmbEmStation.SelectedIndex);
        }

        private void 得到PCBID当前状态_Click(object sender, EventArgs e)
        {
            if (txtPCB虚拟编号.Text.Trim().Length <= 0 || cmbEmStation.SelectedIndex < 0)
                return;

            chkStationResultStatus.Checked = HPInvoke.dataBaseSQL.SQL_GetStationResultStatus(txtPCB虚拟编号.Text.Trim(), (APK_HP.MES.HPSql.WorkStationNameEnum)cmbEmStation.SelectedIndex);
        }

        private void 得到PCBID当前结果_Click(object sender, EventArgs e)
        {
            if (txtPCB虚拟编号.Text.Trim().Length <= 0 || cmbEmStation.SelectedIndex < 0)
                return;

            memoEdit4.Text = "";
            Application.DoEvents();
            ResultInformation resultList = null;
            chkStationResultStatus.Checked = HPInvoke.dataBaseSQL.SQL_GetStationResult(txtPCB虚拟编号.Text.Trim(), (APK_HP.MES.HPSql.WorkStationNameEnum)cmbEmStation.SelectedIndex, out resultList);

            if (resultList != null)
            {
                string temp = "";
                for (int nIndex = 0; nIndex < resultList.Results.Length; nIndex++)
                {
                    temp += $"CriteriaName = {resultList.Results[nIndex].CriteriaName}\r\n";
                    temp += $"CriteriaStantard = {resultList.Results[nIndex].CriteriaStantard}\r\n";
                    temp += $"CriteriaMin = {resultList.Results[nIndex].CriteriaMin}\r\n";
                    temp += $"CriteriaMax = {resultList.Results[nIndex].CriteriaMax}\r\n";
                    temp += $"DataType = {resultList.Results[nIndex].DataType.ToString()}\r\n";
                    temp += $"CompareType = {resultList.Results[nIndex].CompareType.ToString()}\r\n";
                    temp += $"Result = {resultList.Results[nIndex].Result}\r\n";

                    temp += "=================================================\r\n";
                }

                memoEdit4.Text = temp;
            }
        }

        private void 得到托盘当前结果_Click(object sender, EventArgs e)
        {
            if (txt托盘编号.Text.Trim().Length <= 0 || cmbEmStation.SelectedIndex < 0)
                return;

            memoEdit4.Text = "";
            Application.DoEvents();
            ResultInformation resultList = null;
            chkStationResultStatus.Checked = HPInvoke.dataBaseSQL.SQL_GetStationResultFromTray(txt托盘编号.Text.Trim(), (APK_HP.MES.HPSql.WorkStationNameEnum)cmbEmStation.SelectedIndex, out resultList);

            if (resultList != null)
            {
                string temp = "";
                for (int nIndex = 0; nIndex < resultList.Results.Length; nIndex++)
                {
                    temp += $"CriteriaName = {resultList.Results[nIndex].CriteriaName}\r\n";
                    temp += $"CriteriaStantard = {resultList.Results[nIndex].CriteriaStantard}\r\n";
                    temp += $"CriteriaMin = {resultList.Results[nIndex].CriteriaMin}\r\n";
                    temp += $"CriteriaMax = {resultList.Results[nIndex].CriteriaMax}\r\n";
                    temp += $"DataType = {resultList.Results[nIndex].DataType.ToString()}\r\n";
                    temp += $"CompareType = {resultList.Results[nIndex].CompareType.ToString()}\r\n";
                    temp += $"Result = {resultList.Results[nIndex].Result}\r\n";

                    temp += "=================================================\r\n";
                }

                memoEdit4.Text = temp;
            }
        }

        private void 设置烧录工位结果_Click(object sender, EventArgs e)
        {
            if (txt托盘编号.Text.Trim().Length <= 0)
                return;

            Int16[] FalshResults = new Int16[100];
            FalshResults[20] = 0x30;
            FalshResults[45] = 0x20;
            if (!HPInvoke.dataBaseSQL.SQL_SetFlashResultStationResultFromTray(txt托盘编号.Text.Trim(), FalshResults))
            {
                MessageBox.Show(this, $"设置烧录工位结果_Click({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void 得到烧录工位结果_Click(object sender, EventArgs e)
        {
            memoEdit4.Text = "";
            Application.DoEvents();
            if (txtPCB虚拟编号.Text.Trim().Length <= 0)
                return;

            Int16[] FlashResult = null;
            if (!HPInvoke.dataBaseSQL.SQL_GetFlashResult(txtPCB虚拟编号.Text.Trim(), out FlashResult))
            {
                MessageBox.Show(this, $"设置烧录工位结果_Click({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                string temp = "";
                for (int col = 0; col < FlashResult.Length; col++)
                {
                    temp += $"{(col + 1).ToString()}:(0x{FlashResult[col].ToString("X02")})\r\n";
                }

                memoEdit4.Text = temp;
            }
        }

        private void 得到烧录TRYA工位结果_Click(object sender, EventArgs e)
        {
            memoEdit4.Text = "";
            Application.DoEvents();
            if (txt托盘编号.Text.Trim().Length <= 0)
                return;

            Int16[] FlashResult = null;
            if (!HPInvoke.dataBaseSQL.SQL_GetFlashResultFromTray(txt托盘编号.Text.Trim(), out FlashResult))
            {
                MessageBox.Show(this, $"设置烧录工位结果_Click({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                string temp = "";
                for (int col = 0; col < FlashResult.Length; col++)
                {
                    temp += $"{(col + 1).ToString()}:(0x{FlashResult[col].ToString("X02")})\r\n";
                }

                memoEdit4.Text = temp;
            }
        }

        private void 强制MES验证通过_CheckedChanged(object sender, EventArgs e)
        {
            HPInvoke.ObjAllParameters.bForceMesCheckOK = 强制MES验证通过.Checked;
        }

        public void PrcessSeverTestHerat(object wsarg)
        {
            while (!HPglobal.m_bQuitApp)
            {
                try
                {
                    //重新开始新的数据读取
                    Thread.Sleep(3000);
                    if (HPInvoke.PC_PackingProcess.Client == null || HPInvoke.PC_PackingProcess.Client.Poll(1, SelectMode.SelectRead) && (HPInvoke.PC_PackingProcess.Client.Available == 0) ||
                        HPInvoke.PC_PackingProcess.Connected == false ||
                        HPInvoke.PC_PackingProcess.Client.Send(System.Text.Encoding.ASCII.GetBytes("Test\r\n")) <= 0)
                    {
                        if (LED_ProcessServerStatus.LanternBackground != System.Drawing.Color.Red)
                            LED_ProcessServerStatus.LanternBackground = System.Drawing.Color.Red;

                        string error = string.Format("请注意: 客户端网络已断开(计算机：{0})", HPInvoke.PC_PackingProcess.Client.RemoteEndPoint);
                        HPglobal.RecordeAllMessage(error, true, false, "请检查电脑或下位机是否开机或连机及网线是否连接好");
                        HPInvoke.PC_PackingProcess.Close();
                        HPInvoke.PC_PackingProcess = TimeOutSocket.Connect(new IPEndPoint(IPAddress.Parse(HPglobal.strIPAddress_包装PC), 5230), 2000);
                    }
                    else
                        LED_ProcessServerStatus.LanternBackground = (LED_ProcessServerStatus.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);

                    //尾料模式
                    if (HPInvoke.bLastProductMode == true)
                    {
                        LastProductModeLED.LanternBackground = (LastProductModeLED.LanternBackground == System.Drawing.Color.Yellow ? System.Drawing.Color.Green : System.Drawing.Color.Yellow);
                        btm进入尾料模式.Checked = true;
                    }
                    else if(HPInvoke.bLastProductMode == false && btm进入尾料模式.Checked == true)
                    {
                        LastProductModeLED.LanternBackground = Color.Gray;
                        btm进入尾料模式.Checked = false;
                    }

                }
                catch (Exception ex)
                {
                    HPInvoke.PC_PackingProcess = TimeOutSocket.Connect(new IPEndPoint(IPAddress.Parse(HPglobal.strIPAddress_包装PC), 5230), 2000);
                    HPglobal.m_strLastException = "PrcessSeverTestHerat:\r\n" + ex.Message;
                }
            }

            //断开连接
            try
            {
                if (HPInvoke.PC_PackingProcess.Client != null)
                    HPInvoke.PC_PackingProcess.Client.Disconnect(false);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("PrcessSeverTestHerat() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");

            }
        }

        public void ManualChangeWorkOrder(string workOrder, bool forceChange = false)
        {
            try
            {
                HPInvoke.bLastProductMode = false;
                string responed = HPInvoke.GetWorkOrdeInfomation(workOrder, false);
                if (responed != null && responed.Length > 0)
                {
                    popupContainerEdit1.Text = $"当前工单: {workOrder}";
                    Application.DoEvents();

                    HPInvoke.ObjAllParameters.APKWorkOrderInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<HpWebService.HpMesParameter>(responed);
                    propertyProcess.SelectedObject = HPInvoke.ObjAllParameters.APKWorkOrderInfo;

                    if (chkWriteWorkSheetToSQL.Checked || forceChange)
                    {
                        //写入工单单信息到调度
                        WriteOrderInfoToProductionLine();

                        //记录当前的工单到数据库, 并设当前的工单为激活工单
                        bool bResult = HPInvoke.dataBaseSQL.SQL_WriteWrokSheetInfo(txtWorkID.Text.Trim(), responed, true);

                        WorkOderChanged = true;
                        HPInvoke.bLastProductMode = false;
                    }
                }
                else
               {
                    popupContainerEdit1.Text = $"当前工单: {""}";
                    propertyProcess.SelectedObject = HPInvoke.ObjAllParameters.APKWorkOrderInfo;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void 获取工单信息_Click(object sender, EventArgs e)
        {
            try
            {
                ManualChangeWorkOrder(txtWorkID.Text.Trim());
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("获取工单信息_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
            }

        }

        public void ReferenshData(string tableName, System.Windows.Forms.DataGridView gridView)
        {
            try
            {
                //select* from leakage where DateTime > '2019-08-28 00:00:00' and DateTime< '2019-08-28 23:59:59'
                DateTime time = DateTime.Now;
                string strTime = time.ToString("u").Replace("T", " ").Replace("Z", "");

                string strDateTimeStart = dateTimePicker1.Value.ToString("u").Replace("T", " ").Replace("Z", "");
                string strDateTimeEnd = dateTimePicker2.Value.ToString("u").Replace("T", " ").Replace("Z", "");

                string CommandText = "";
                if (tableName == "TrayTable") 
                    CommandText = $"SELECT * FROM {tableName}";
                else if (tableName == "PackingPrint") 
                   CommandText = $"SELECT * FROM {tableName}";
                else if (tableName == "WorkSheetTable")
                    CommandText = $"SELECT * FROM {tableName}";
                else
                {
                    CommandText = $"SELECT * FROM {tableName} where BindTime > '{strDateTimeStart}' and BindTime< '{strDateTimeEnd}'";

                    if (txtTrayIDForFind.Text.Trim().Length > 0)
                        CommandText += $" and TrayQrCode= '{txtTrayIDForFind.Text.Trim()}'";

                    if (txtPCBIDForFind.Text.Trim().Length > 0)
                        CommandText += $" and PCBID= '{txtPCBIDForFind.Text.Trim()}'";

                }

                DataTable mTable = HPInvoke.dataBaseSQL.DtQuery(CommandText);

                // 绑定数据到DataGridView
                gridView.DataSource = mTable;
                for(int nIndex = 0; nIndex < gridView.ColumnCount; nIndex++)
                {
                    gridView.Columns[nIndex].Width = 150;
                }

                gridView.Refresh();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("ReferenshData 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        private void btmRefenceTrayID_Click(object sender, EventArgs e)
        {
            try
            {
                ReferenshData("TrayTable", dataGridView1);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRefenceTrayID_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        private void btmReferenceResults_Click(object sender, EventArgs e)
        {
            try
            {
                ReferenshData("ResultsInfoTable", dataGridView2);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmReferenceResults_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        private void btmPrintLable_Click(object sender, EventArgs e)
        {
            try
            {
                int nCount = int.Parse(txtTestPC.Text.Trim());
                if (!HPInvoke.PrintWrokOrderLable(txtWorkID.Text.Trim(), nCount))
                {
                    MessageBox.Show(this, "打印 Lable 错误", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmPrintLable_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void 输入包装信息_Click(object sender, EventArgs e)
        {
            if (!HPInvoke.dataBaseSQL.SQL_RecordePackingInfo(HPInvoke.ObjAllParameters.workOrder, "123456"/*HPInvoke.ObjAllParameters.strBarCodePrint*/, "Demo1234, Demo5678"))
            {
                MessageBox.Show(this, $"输入包装信息_Click({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btmRefencePacking_Click(object sender, EventArgs e)
        {
            try
            {
                ReferenshData("PackingPrint", dataGridView3);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRefencePacking_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, $"确认清除所有已下料盘信息，有可能丢失包装信息 ", "确认????", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                HPInvoke.ResetAllUnloadPCBs();
            }

            //             Apk_HP_Application.程序文件夹分类._4装盘及机器人控制.HPRobotCtrl.RecordUnloaedPCBID_ToRobotPLC("1234567");
            //             Apk_HP_Application.程序文件夹分类._4装盘及机器人控制.HPRobotCtrl.RecordUnloaedPCBID_ToRobotPLC("asdfghj");
        }

        private void btmChangeWorkOrder_Click(object sender, EventArgs e)
        {
            Byte[] trigger = { 0x16, 0x54, 0x0D };
            Byte[] stop = { 0x16, 0x55, 0x0D };

            System.IO.Ports.SerialPort com1 = new System.IO.Ports.SerialPort();

            try
            {
                if (HPglobal.emMachineState == HPglobal.emMachineWorkingState.emMachineWorkingState_Working)
                {
                    MessageBox.Show("设备已经在工作当中, 必须停止设备并收完所有的料才能转工单！", "确认", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                WorkOderChanged = false;
                HPInvoke.bLastProductMode = false;

                #region>>>>>> 从条码枪读取工单二维码
                com1.BaudRate = 115200;
                com1.Open();

                com1.Write(trigger, 0, trigger.Length);
                Thread.Sleep(200);
                com1.Write(stop, 0, stop.Length);

                Thread.Sleep(500);
                if (com1.BytesToRead <= 0)
                {
                    com1.Close();
                    MessageBox.Show(this, $"！！！！！切换工单失败!！！！！！！\r\nt条码枪没有任何数据近回!", "错误(^_^)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                byte[] res = new byte[com1.BytesToRead];
                com1.Read(res, 0, com1.BytesToRead);
                com1.Close();

                string strOder = System.Text.Encoding.ASCII.GetString(res);
                strOder = strOder.Trim("\0".ToArray()).Trim("\r".ToArray()).Trim("\n".ToArray());
                if(strOder.Length <= 0)
                {
                    MessageBox.Show(this, $"！！！！！切换工单失败!！！！！！！\r\nt条码枪没有任何数据近回!", "错误(^_^)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                #endregion

                #region>>>>>>获取工单信息
                txtWorkID.Text = strOder;
                popupContainerEdit1.Text = $"当前工单: {strOder}";
                Application.DoEvents();
                string responed = HPInvoke.GetWorkOrdeInfomation(txtWorkID.Text.Trim());

                #endregion

                if (responed != null && responed.Length > 0)
                {
                    lock (HPInvoke.ObjAllParameters)
                    {
                        HPInvoke.ObjAllParameters.APKWorkOrderInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<HpWebService.HpMesParameter>(responed);
                        propertyProcess.SelectedObject = HPInvoke.ObjAllParameters.APKWorkOrderInfo;
                    }

                    #region>>>>>>得到工单其它信息
                    bool mode = false;
                    if (!HPInvoke.GetFlashDataModeFromFile(HPInvoke.ObjAllParameters.PCBAName, out mode))
                    {
                        MessageBox.Show(this, $"！！！！！谨慎!！！！！！！\r\n此工单无法确认烧录行例切换信息，请人工确认并设置好保存!!!!\r\n工单号：{strOder} \r\n工单数量: {HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber}", "错误(^_^)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        this.Invoke(new Action(() =>
                        {
                            chk切换行例数据.Checked = HPInvoke.ObjAllParameters.flashDataMode = mode;

                            HPInvoke.panel_Flash1.FlashResultSwitchDataMode(HPInvoke.ObjAllParameters.flashDataMode);
                            HPInvoke.panel_Flash2.FlashResultSwitchDataMode(HPInvoke.ObjAllParameters.flashDataMode);
                        }));
                    }
                    #endregion

                    //写入工单单信息到调度
                    bool bResult1 = WriteOrderInfoToProductionLine();

                    //记录当前的工单到数据库, 并设当前的工单为激活工单
                    bool bResult2 = HPInvoke.dataBaseSQL.SQL_WriteWrokSheetInfo(strOder, responed, true);

                    //提示
                    if(HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber  == null ||
                        HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber == "0" ||
                        !bResult1 ||
                        !bResult2)
                    {
                        MessageBox.Show(this, $"！！！！！切换工单失败!！！！！！！\r\n工单号：{strOder} \r\n工单数量: {HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber}", "错误(^_^)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(this, $"！！！！！切换工单成功!！！！！！！\r\n工单号：{strOder}  \r\n工单数量: {HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber}", "恭喜(^_^)", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        WorkOderChanged = true;
                    }
                }
                else
                    MessageBox.Show(this, $"切换工单失败，获取工单信息失败，请确认是否连接工艺服务器或其它问题! ", "错误(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
            catch (Exception ex)
            {
                com1.Close();
                HPglobal.m_strLastException = string.Format("btmChangeWorkOrder_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                MessageBox.Show(this, $"切换工单异常{ex.Message}\r\n获取工单信息失败，请确认是否连接工艺服务器或其它问题! ", "错误(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool WriteOrderInfoToProductionLine()
        {
            try
            {
                if (HPInvoke.ObjAllParameters.APKWorkOrderInfo == null || HPInvoke.ObjAllParameters.APKWorkOrderInfo.WorkSheet.Length <= 0)
                    return false;

                lock(HPInvoke.ObjAllParameters)
                {
                    this.Invoke(new Action(() =>
                    {
                        txtWorkOrder2.Text = HPInvoke.ObjAllParameters.workOrder = HPInvoke.ObjAllParameters.APKWorkOrderInfo.WorkSheet;
                        txtPCBPNNumber.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.PcbNumber;

                        //下料工位工艺参数
                        if (HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber == null ||
                            HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber.Length <= 0)
                            HPInvoke.ObjAllParameters.totalProductCount = 0;
                        else
                            HPInvoke.ObjAllParameters.totalProductCount = int.Parse(HPInvoke.ObjAllParameters.APKWorkOrderInfo.PlanNumber);

                        UnloadCfg_TotalProduct.Text = HPInvoke.ObjAllParameters.totalProductCount.ToString();

                        UnloadCfg_Row.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.PBWidth.ToString(); //HPInvoke.ObjAllParameters.APKWorkOrderInfo.rowNumber.ToString();
                        UnloadCfg_Col.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.PBLength.ToString(); // PBWidthHPInvoke.ObjAllParameters.APKWorkOrderInfo.colNumber.ToString();

                        UnloadCfg_Row.Value = (short)HPInvoke.ObjAllParameters.APKWorkOrderInfo.rowNumber;
                        UnloadCfg_Col.Value = (short)HPInvoke.ObjAllParameters.APKWorkOrderInfo.colNumber;

                        HPInvoke.ObjAllParameters.cfg_Row = (short)UnloadCfg_Row.Value;
                        HPInvoke.ObjAllParameters.cfg_Col = (short)UnloadCfg_Col.Value;
                        HPInvoke.ObjAllParameters.cfg_Total = (short)(HPInvoke.ObjAllParameters.cfg_Row * HPInvoke.ObjAllParameters.cfg_Col * HPInvoke.ObjAllParameters.cfg_CountTray); //每次包装的总量
                        UnloadCfg_Total.Value = HPInvoke.ObjAllParameters.cfg_Total;

                        decimal countPerTray = UnloadCfg_Row.Value * UnloadCfg_Col.Value;
                        if (UnloadCfg_TotalProduct.Value % countPerTray == 0)
                        {
                            UnloadCfg_Remaind.Value = HPInvoke.ObjAllParameters.cfg_Remaind = (short)countPerTray;
                        }
                        else
                        {
                            UnloadCfg_Remaind.Value = HPInvoke.ObjAllParameters.cfg_Remaind = (short)(UnloadCfg_TotalProduct.Value % countPerTray);
                        }

                        //包装工位工艺参数
                        speTrayLength.Text = HPInvoke.ObjAllParameters.TrayWidth.ToString("F2");
                        speTrayWidth.Text = HPInvoke.ObjAllParameters.TrayHeight.ToString("F2");
                        speDstMaduo.Text = HPInvoke.ObjAllParameters.MaduoDst.ToString("F2");
                        spePCBLength.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.WGCCLength.ToString("F2");
                        spePCBWidth.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.WGCCWidth.ToString("F2");

                        spePCBDstRow.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.DBPACING_Y.ToString("F2");
                        spePCBDstCol.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.DBPACING_X.ToString("F2");

                        //打码工位工艺参数
                        txtMarkText.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkText;

                        //下料
                        if (HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkFace == 0)
                        {
                            chkLaserMarkTest_Up.Checked = true;
                            chkLaserMarkTest_Down.Checked = false;
                        }
                        else
                        {
                            chkLaserMarkTest_Up.Checked = false;
                            chkLaserMarkTest_Down.Checked = true;
                        }

                    }));

                    HPInvoke.panel_Flash1.flashServer.SetRowColNumber((byte)HPInvoke.ObjAllParameters.cfg_Row, (byte)HPInvoke.ObjAllParameters.cfg_Col);
                    HPInvoke.panel_Flash2.flashServer.SetRowColNumber((byte)HPInvoke.ObjAllParameters.cfg_Row, (byte)HPInvoke.ObjAllParameters.cfg_Col);

                    //定入所有工单信息
                    WriteInvokeInformationToAllPLC();

                    return true;
                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("WriteOrderInfoToProductionLine() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        public string GetFalshMenuFromWorkOrder(ref string refCustomerName,  ref string TwoRefName, ref string ThreeRefName)
        {
            try
            {
                refCustomerName = "";
                TwoRefName = "";
                ThreeRefName = "";

                //string strDes = "C4HA594-BC345XL/U-原装-带头五代";
                if (HPInvoke.ObjAllParameters.APKWorkOrderInfo.DescRiption == null ||
                    HPInvoke.ObjAllParameters.APKWorkOrderInfo.DescRiption.Length <= 0)
                    return "";

                string strDes = HPInvoke.ObjAllParameters.APKWorkOrderInfo.DescRiption;
                string[] arryDes = strDes.Split("-".ToArray());
                if (arryDes.Length < 2)
                    return "";

                refCustomerName = arryDes[0];
                ThreeRefName = arryDes[1];

                string[] strArryType = arryDes[1].Split("/".ToArray());
                if (strArryType.Length < 2)
                    return "";

                TwoRefName = strArryType[1];

                return GetFalshProgramDiscription(refCustomerName, TwoRefName, ThreeRefName);
            }
            catch(Exception ex)
            {
                return "";
            }
        }

        public string GetFalshProgramDiscription(string refCustomerName, string TwoRefName, string ThreeRefName)
        {
            string strFlashType = "";
            try
            {
//                 string menu1st = "URe";
//                 string menu2nd = "CL816XL/URe";
                XmlSerializer xs = new XmlSerializer(typeof(SchemaDef));
                StreamReader sr = new StreamReader(HPInvoke.ObjAllParameters.schemaFilename);
                schemaDefParse = (SchemaDef)xs.Deserialize(sr);
//                 IXtreeNode controller;
//                 TreeNode tn = null;
//                 TreeNode tn2 = null;
//                 TreeNode tn3 = null;
//                 TreeNode tn4 = null;
//                 TreeNode tn5 = null;
                SchemaDef schemaDefRef = new SchemaDef();
                TableDef tableDefRef = new TableDef();

                if(refCustomerName[0] == 'H')
                    tableDefRef.Name = "HP";// refCustomerName; //"CANON"; //需根据工单解析到 //CANON
                else if (refCustomerName[0] == 'C')
                    tableDefRef.Name = "CANON";

                TableDefTwo tableDefTwoRef = new TableDefTwo();
                tableDefTwoRef.Name = TwoRefName; // "URe";  //需根据工单解析到
                TableDefThree tableDefThreeRef = new TableDefThree();
                tableDefThreeRef.Name = ThreeRefName; // "PG810/URe";  //需根据工单解析到

                TableDefFour tableDefFourRef = new TableDefFour();
                TableDefFive tableDefFiveRef = new TableDefFive();
                TableFieldDef tableFieldDefRef = new TableFieldDef();

                foreach (TableDef tableDef in schemaDefParse.Tables)
                {
                    if (tableDefRef.Name.Equals(tableDef.Name.Trim()))
                    {
                        tableDefRef.Name = tableDef.Name;
                        tableDefRef.Pridata = tableDef.Pridata;
                        foreach (TableDefTwo tfd2 in tableDef.Fields)
                        {
                            Console.WriteLine(tfd2.Name);
                            if (tableDefTwoRef.Name.Equals(tfd2.Name.Trim()))
                            {
                                tableDefTwoRef.Name = tfd2.Name;
                                tableDefTwoRef.Pridata = tfd2.Pridata;
                                foreach (TableDefThree tfd3 in tfd2.Fields)
                                {
                                    if (tableDefThreeRef.Name.Equals(tfd3.Name.Trim()))
                                    {
                                        tableDefThreeRef.Name = tfd3.Name;
                                        tableDefThreeRef.Pridata = tfd3.Pridata;
                                        foreach (TableDefFour tfd4 in tfd3.Fields)
                                        {
                                            //if (!tableDefThreeRef.Name.Equals(tfd4.Name))
                                            //{
                                            //    tableDefThreeRef.Name = tfd4.Name;
                                            //    tableDefThreeRef.Pridata = tfd4.Pridata;
                                            //    break;
                                            //}
                                            //foreach (TableDefFive tfd5 in tfd4.Fields)
                                            //{
                                            //    if (!tableDefFourRef.Name.Equals(tfd5.Name))
                                            //    {
                                            //        tableDefFourRef.Name = tfd5.Name;
                                            //    }
                                            //    foreach (TableFieldDef tfd6 in tfd5.Fields)
                                            //    {
                                            //        if (!tableDefFiveRef.Name.Equals(tfd6.Name))
                                            //        {
                                            //            tableDefFiveRef.Name = tfd6.Name;
                                            //        }
                                            //    }
                                            //}
                                        }
                                        break;
                                    }

                                }
                                break;
                            }
                        }
                        break;
                    }

                }

                if (tableDefRef.Pridata != null && tableDefTwoRef.Pridata != null && tableDefThreeRef.Pridata != null)
                    strFlashType = tableDefRef.Pridata + "|" + tableDefTwoRef.Pridata + "|" + tableDefThreeRef.Pridata;
                else
                    strFlashType = "";

                return strFlashType;
            }
            catch(Exception ex)
            {

            }
        
            return "";
        }

        private void btmGetFalshPro_Click(object sender, EventArgs e)
        {
            string refCustomerName = "";
            string TwoRefName = "";
            string ThreeRefName = "";

            string strPro = GetFalshMenuFromWorkOrder(ref refCustomerName, ref TwoRefName, ref ThreeRefName);
            if(strPro != null && strPro.Length > 0)
                MessageBox.Show(strPro);
            else
                MessageBox.Show("得不到烧录型号");           
        }

        //================================================================================================
        //
        //
        //
        //
        //
        //================================================================================================

        public static string[] pridataM4 = new string[10];
        public static int[] priData_node = new int[20];
        public static string icDataFile = "";
        public static string DownloadFilePath = Environment.CurrentDirectory + "\\DATA\\";
        protected SchemaDef schemaDef = null;
        protected SchemaDef schemaDefParse = null;

        protected TreeNode rootNode = null;
        XTree sdTree = null;

        protected void CreateRootNode()
        {
            schemaDef = new SchemaDef();
            SchemaController sc = new SchemaController(schemaDef);
            sdTree = new XTree();

            rootNode = sdTree.AddNode(sc, null);
            sdTree.SelectedNode = sdTree.Nodes[0];

            LoadMenu();

            sdTree.Width = 682;
            sdTree.Height = 660;
            panelControl7.Controls.Add(sdTree);

        }

        protected void LoadMenu()
        {
            XmlSerializer xs = new XmlSerializer(typeof(SchemaDef));
            StreamReader sr = new StreamReader(HPInvoke.ObjAllParameters.schemaFilename);
            schemaDef = (SchemaDef)xs.Deserialize(sr);
            ((SchemaController)((NodeInstance)rootNode.Tag).Instance).SchemaDef = schemaDef;
            sr.Close();
            PopulateTree();
        }
        protected void PopulateTree()
        {
            IXtreeNode controller;
            TreeNode tn = null;
            TreeNode tn2 = null;
            TreeNode tn3 = null;
            TreeNode tn4 = null;
            TreeNode tn5 = null;
            SchemaDef schemaDefRef = new SchemaDef();
            TableDef tableDefRef = new TableDef();
            TableDefTwo tableDefTwoRef = new TableDefTwo();
            TableDefThree tableDefThreeRef = new TableDefThree();
            TableDefFour tableDefFourRef = new TableDefFour();
            TableDefFive tableDefFiveRef = new TableDefFive();
            TableFieldDef tableFieldDefRef = new TableFieldDef();
            sdTree.SuspendLayout();
            schemaDef.Tables.Sort((x, y) =>
            {
                int result;
                if (x.Name.CompareTo(y.Name) > 0)
                {
                    result = 1;
                }
                else if (x.Name == y.Name)
                {
                    result = 1;
                }
                else
                {
                    result = -1;
                }
                return result;
            });
            foreach (TableDef tableDef in schemaDef.Tables)
            {

                if (!schemaDefRef.Name.Equals(tableDef.Name))
                {
                    schemaDefRef.Name = tableDef.Name;
                    controller = new TableController(tableDef);
                    tn = sdTree.AddNode(controller, rootNode);

                }
                tableDef.Fields.Sort((x, y) =>
                {
                    int result;
                    if (x.Name.CompareTo(y.Name) > 0)
                    {
                        result = 1;
                    }
                    else if (x.Name == y.Name)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = -1;
                    }
                    return result;
                });
                foreach (TableDefTwo tfd2 in tableDef.Fields)
                {

                    if (!tableDefRef.Name.Equals(tfd2.Name))
                    {
                        tableDefRef.Name = tfd2.Name;
                        controller = new TableControllerTwo(tfd2);

                        tn2 = sdTree.AddNode(controller, tn);
                    }
                    tfd2.Fields.Sort((x, y) =>
                    {
                        int result;
                        if (x.Name.CompareTo(y.Name) > 0)
                        {
                            result = 1;
                        }
                        else if (x.Name == y.Name)
                        {
                            result = 1;
                        }
                        else
                        {
                            result = -1;
                        }
                        return result;
                    });
                    foreach (TableDefThree tfd3 in tfd2.Fields)
                    {

                        if (!tableDefTwoRef.Name.Equals(tfd3.Name))
                        {
                            tableDefTwoRef.Name = tfd3.Name;
                            controller = new TableControllerThree(tfd3);

                            tn3 = sdTree.AddNode(controller, tn2);
                        }
                        tfd3.Fields.Sort((x, y) =>
                        {
                            int result;
                            if (x.Name.CompareTo(y.Name) > 0)
                            {
                                result = 1;
                            }
                            else if (x.Name == y.Name)
                            {
                                result = 1;
                            }
                            else
                            {
                                result = -1;
                            }
                            return result;
                        });
                        foreach (TableDefFour tfd4 in tfd3.Fields)
                        {

                            if (!tableDefThreeRef.Name.Equals(tfd4.Name))
                            {
                                tableDefThreeRef.Name = tfd4.Name;
                                controller = new TableControllerFour(tfd4);

                                tn4 = sdTree.AddNode(controller, tn3);

                            }
                            tfd4.Fields.Sort((x, y) =>
                            {
                                int result;
                                if (x.Name.CompareTo(y.Name) > 0)
                                {
                                    result = 1;
                                }
                                else if (x.Name == y.Name)
                                {
                                    result = 1;
                                }
                                else
                                {
                                    result = -1;
                                }
                                return result;
                            });
                            foreach (TableDefFive tfd5 in tfd4.Fields)
                            {

                                if (!tableDefFourRef.Name.Equals(tfd5.Name))
                                {
                                    tableDefFourRef.Name = tfd5.Name;
                                    controller = new TableControllerFive(tfd5);

                                    tn5 = sdTree.AddNode(controller, tn4);
                                }
                                tfd5.Fields.Sort((x, y) =>
                                {
                                    int result;
                                    if (x.Name.CompareTo(y.Name) > 0)
                                    {
                                        result = 1;
                                    }
                                    else if (x.Name == y.Name)
                                    {
                                        result = 1;
                                    }
                                    else
                                    {
                                        result = -1;
                                    }
                                    return result;
                                });
                                foreach (TableFieldDef tfd6 in tfd5.Fields)
                                {

                                    if (!tableDefFiveRef.Name.Equals(tfd6.Name))
                                    {
                                        tableDefFiveRef.Name = tfd6.Name;
                                        controller = new TableFieldController(tfd6);

                                        sdTree.AddNode(controller, tn5);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //sdTree.CollapseAll();
            sdTree.Nodes[0].Expand();
            //sdTree.ExpandAll();
            sdTree.ResumeLayout();
            sdTree.SelectedNode = sdTree.Nodes[0];
        }

        private void chkRealUpdateWorkSheet_CheckedChanged(object sender, EventArgs e)
        {
            HPInvoke.bRealUpdataWorkSheet = chkRealUpdateWorkSheet.Checked;
        }

        private void btmGetCurrentWorkSheet_Click(object sender, EventArgs e)
        {
            try
            {
                string workSheet;
                bool bActived;
                HpWebService.HpMesParameter info;

                propertyGrid2.SelectedObject = null;
                if (HPInvoke.dataBaseSQL.SQL_GetActivedWorkSheetInfo(out workSheet, out bActived, out info))
                {
                    if (info != null)
                        propertyGrid2.SelectedObject = info;
                }
            }
            catch(Exception ex)
            {

            }

        }

        private void chkEnableMarkTest_CheckedChanged(object sender, EventArgs e)
        {
            if(chkEnableMarkTest.Checked)
            {
                if (HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkFace == 0)
                {
                    chkLaserMarkTest_Up.Checked = true;
                    chkLaserMarkTest_Down.Checked = false;
                }
                else
                {
                    chkLaserMarkTest_Up.Checked = false;
                    chkLaserMarkTest_Down.Checked = true;
                }

                HPInvoke.UnloadStation.stationBase.WriteInt16("D2249", (short)(HPInvoke.ObjAllParameters.APKWorkOrderInfo.MarkFace == 0 ? 0 : 1));
            }
            else
            {
                HPInvoke.UnloadStation.stationBase.WriteInt16("D2249", (short) 10);

            }
        }

        private void btmSetPackingCount_Click(object sender, EventArgs e)
        {
            try
            {
                short n1061 = HPInvoke.PackingStation.stationBase.ReadInt16("D1061");
                HPInvoke.PackingStation.stationBase.WriteInt16("D1061", (short)spedPackingPackedCountSet.Value);
                HPInvoke.PackingStation.stationBase.WriteInt16("D1062", (short) n1061 );
            }
            catch(Exception ex)
            {

            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                short n1061 = HPInvoke.UnloadStation.stationBase.ReadInt16("D1061");
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1061", (short)spedUnloadPackedCountSet.Value);
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1062", (short)n1061);
            }
            catch(Exception ex)
            {

            }

        }

        private void btmSetCurrentTrayOut_Click(object sender, EventArgs e)
        {

        }

        private void btmClearFailed_Click(object sender, EventArgs e)
        {
            try
            {
                HPInvoke.UnloadStation.stationBase.WriteInt16("D1063", (short)spinEdit3.Value);
            }
            catch(Exception ex)
            {

            }
        }

        private void chkEnableLastMode_CheckedChanged(object sender, EventArgs e)
        {
            HPInvoke.bEnableLastModeInvode = chkEnableLastMode.Checked;
            if (!chkEnableLastMode.Checked)
                HPInvoke.bLastProductMode = false;
        }

        private void TotalTrayOfLineOutPCS_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btm进入尾料模式_CheckedChanged(object sender, EventArgs e)
        {
            if(btm进入尾料模式.Checked)
            {
                HPInvoke.bLastProductMode = true;
            }
            else
                HPInvoke.bLastProductMode = false;
        }

        private void btm得到最后打码标志_Click(object sender, EventArgs e)
        {
            Int16[,] FlagResult = null;
            memoEdit4.Text = "";
            if (!HPInvoke.dataBaseSQL.SQL_GetLastModeFlagAllProcessFromTray(txt托盘编号.Text.Trim(), out FlagResult))
            {
                MessageBox.Show(this, $"btm得到最后打码标志_Click({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})! ", "出错了(V_V)", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                string temp = "";
                for(int nRow = 0; nRow < 10; nRow++)
                {
                    for(int nCol = 0; nCol < 10; nCol++)
                    {
                        temp += $"{(nRow * 10 + nCol + 1).ToString()}:(0x{FlagResult[nRow, nCol].ToString("X02")})\r\n";
                    }
                }
                memoEdit4.Text = temp;
            }
        }

        private void chkFocePackingSNOK_CheckedChanged(object sender, EventArgs e)
        {
            HPInvoke.bForceOK_PackingSN = chkFocePackingSNOK.Checked;
        }

        private void btmSetLastTrayRemaindCount_Click(object sender, EventArgs e)
        {
            HPInvoke.countRemaind_ForLastTray = (int)spinEdit5.Value;
        }

        public void updataLastMark()
        {
            try
            {
                this.Invoke(new Action(() =>
                {
                    if(HPInvoke.lastModeFlag != null && HPInvoke.lastModeFlag.Length == 100)
                    {
                        for(int nIndex  = 0; nIndex < 100; nIndex++)
                        {
                            gridFlagMark.Rows[nIndex / 10].Cells[nIndex % 10].Value = (HPInvoke.lastModeFlag[nIndex] == 0x01 ? "Y" : "N");
                            gridFlagMark.Rows[nIndex / 10].Cells[nIndex % 10].Style.BackColor = (HPInvoke.lastModeFlag[nIndex] == 0x01 ? Color.Green  : Color.Red);
                        }
                    }
                }));

            }
            catch(Exception ex)
            {

            }
        }

        private void brm刷新最后打码标志_Click(object sender, EventArgs e)
        {
            updataLastMark();
        }

        private void btmLoad_Click(object sender, EventArgs e)
        {
            string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            string folderPath = System.IO.Path.GetDirectoryName(exeFileName);
            OpenFileDialog fileDlg = new OpenFileDialog();
            fileDlg.FileName = "虚拟工单清单.csv";
            fileDlg.DefaultExt = "csv";
            fileDlg.InitialDirectory = folderPath;
            fileDlg.Filter = "CSV 文件|*.csv|TXT 文件|*.txt|所有文件|*.*";
            fileDlg.Multiselect = false;
            DialogResult result = fileDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                using (StreamReader sr = new StreamReader(fileDlg.FileName, Encoding.GetEncoding("GB2312"))) //Encoding.GetEncoding("GB2312")
                {
                    string line;
                    int nCount = 0;

                    listView1.Items.Clear();
                    listView1.Columns.Clear();
                   
                    while ((line = sr.ReadLine()) != null)
                    {
                        string lineStr = line.Trim();
                        lineStr = System.Text.Encoding.Unicode.GetString(System.Text.Encoding.Unicode.GetBytes(lineStr));
                        if (lineStr.Equals("") == true)//文件中有空行则不加
                            continue;

                        if(nCount == 0)
                        {
                            string[] arry = lineStr.Split(",".ToCharArray());
                            foreach(string data in arry)
                            {
                                listView1.Columns.Add(data);
                            }

                            listView1.Columns[0].Width = 80;
                            listView1.Columns[1].Width = 80;
                            listView1.Columns[5].Width = 120;
                            listView1.Columns[6].Width = 400;
                            nCount++;
                        }
                        else
                        {
                            string[] arry = lineStr.Split(",".ToCharArray());
                            listView1.Items.Add(new ListViewItem(arry));
                            nCount++;
                        }
                       


                    }

                    sr.Close();
                }
               // textBox2.Text = fileDlg.FileName;
            }
        }

        private void 手动获取当前选中工单及转工单_Click(object sender, EventArgs e)
        {
            try
            {
                string workOrder = listView1.SelectedItems[0].Text;
                if (workOrder.Length > 0)
                {
                    string strTextDsp = "";
                    foreach (ListViewItem.ListViewSubItem data in listView1.SelectedItems[0].SubItems)
                    {
                        strTextDsp += $"{ data.Text} | ";
                    }

                    textEdit1.Text = strTextDsp;
                    txtWorkID.Text = workOrder.Trim();
                    ManualChangeWorkOrder(workOrder.Trim(), true);
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void btm当前工单完成_Click(object sender, EventArgs e)
        {
            try
            {
                HPInvoke.dataBaseSQL.SQL_SetWorkSheetStartEndTime(HPInvoke.ObjAllParameters.workOrder, false);
            }
            catch(Exception ex)
            {

            }
        }

        private void btm工单信息刷新_Click(object sender, EventArgs e)
        {
            try
            {
                ReferenshData("WorkSheetTable", dataGridView4);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btmRefenceTrayID_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return;
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            try
            {
                HPInvoke.PackingStation.stationBase.WriteInt16("D1068", (short)spinEdit6.Value);
            }
            catch (Exception ex)
            {

            }
        }

        public void SetPrivilegy(string group)
        {
            try
            {
                this.Invoke(new Action(() =>
                {
                    if (group == "管理员")
                    {
                        foreach (Control objControl in NP_设置.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_工艺参数.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_手动调试.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_监控.Controls)
                            objControl.Enabled = true;
                    }
                    else if (group == "工程师")
                    {
                        foreach (Control objControl in NP_设置.Controls)
                            objControl.Enabled = false;

                        foreach (Control objControl in NP_工艺参数.Controls)
                            objControl.Enabled = false;

                        foreach (Control objControl in NP_手动调试.Controls)
                            objControl.Enabled = true;

                        foreach (Control objControl in NP_监控.Controls)
                            objControl.Enabled = true;
                    }
                    else //操作员
                    {
                        foreach (Control objControl in NP_设置.Controls)
                            objControl.Enabled = false;

                        foreach (Control objControl in NP_工艺参数.Controls)
                            objControl.Enabled = false;

                        foreach (Control objControl in NP_手动调试.Controls)
                            objControl.Enabled = false;

                        foreach (Control objControl in NP_监控.Controls)
                            objControl.Enabled = false;
                    }

                    HPglobal.RecordeAllMessage($"注意: 当前用户{HPglobal.m_strRFID}, 切换权限({group})成功!", true, false, "");
                }));
            }
            catch(Exception ex)
            {

            }
        }

        private void sped生产实际总盘数_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog file = new OpenFileDialog();
                file.Title = "请选择烧录菜单文件";
                file.Filter = "(*.xml)|*.xml";
                file.InitialDirectory = @"XML";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    HPInvoke.ObjAllParameters.schemaFilename = file.FileName;
                    txtFlashMenuFile.Text = HPInvoke.ObjAllParameters.schemaFilename;
                    CreateRootNode();
                }
            }
            catch (Exception ex)
            {
                HPglobal.RecordeAllMessage($"btmLoad_Click 异常:{ex.Message}", true, false, "");
            }
        }

        private void chk切换行例数据_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                HPInvoke.ObjAllParameters.flashDataMode = chk切换行例数据.Checked;
                HPInvoke.panel_Flash1.FlashResultSwitchDataMode(HPInvoke.ObjAllParameters.flashDataMode);
                HPInvoke.panel_Flash2.FlashResultSwitchDataMode(HPInvoke.ObjAllParameters.flashDataMode);
            }
            catch(Exception ex)
            {
                HPglobal.RecordeAllMessage($"btmLoad_Click 异常:{ex.Message}", true, false, "");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog file = new OpenFileDialog();
                file.Title = "请选择烧录菜单文件";
                file.Filter = "(*.c)|*.c";
                file.InitialDirectory = @"c";
                if (file.ShowDialog() == DialogResult.OK)
                {
                    HPInvoke.ObjAllParameters.FlashCFilename = file.FileName;
                    txtFlashNumFile.Text = HPInvoke.ObjAllParameters.FlashCFilename;
                }
            }
            catch (Exception ex)
            {
                HPglobal.RecordeAllMessage($"btmLoad_Click 异常:{ex.Message}", true, false, "");
            }
        }

        private void btmReconnectSQL_Click(object sender, EventArgs e)
        {
            try
            {
                HPInvoke.dataBaseSQL.IpAddress = HPInvoke.ObjAllParameters.dataBaseSQL_IpAddress;// "JONHA-HP\\SQLEXPRESS"; // "192.168.1.232";
                HPInvoke.dataBaseSQL.Port = HPInvoke.ObjAllParameters.dataBaseSQL_Port; // 1433;
                HPInvoke.dataBaseSQL.User = HPInvoke.ObjAllParameters.dataBaseSQL_User; // "sa";
                HPInvoke.dataBaseSQL.Database = HPInvoke.ObjAllParameters.dataBaseSQL_Database; // "HPDatabase";
                HPInvoke.dataBaseSQL.Password = HPInvoke.ObjAllParameters.dataBaseSQL_Password; // "123456";
                HPInvoke.dataBaseSQL.DisConnect();

                if (!HPInvoke.dataBaseSQL.Connect())
                {
                    HPglobal.RecordeAllMessage($"连接MySQL数据库失败: {HPInvoke.dataBaseSQL.IpAddress}:{1433})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                    return;
                }

                HPglobal.RecordeAllMessage($"连接MySQL数据库成功", true, true, "");

                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, $"连接数据库 初始化异常\r\n{ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void TestFunction_Click(object sender, EventArgs e)
        {
            try
            {
                bool mode = false;
                HPInvoke.GetFlashDataModeFromFile("HP594", out mode);

                HPInvoke.SaveFlashDataModeToFile("HP594", mode);
            }
            catch(Exception ex)
            {

            }
        }
    }
}