﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslCommunication.Profinet.Omron;
using HslCommunication;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace CommunicationZH
{
    public class PLC_Panasonic : HslCommunication.Profinet.Panasonic.PanasonicMcNet
    {
        public bool isConnected = false;
        public PLC_Panasonic() : base()
        {
        }

        public OperateResult ConnectPLC(string ip, int port)
        {
            IpAddress = ip;
            Port = port;
            base.ConnectTimeOut = 3000;
            base.ReceiveTimeOut = 3000;
            OperateResult result = base.ConnectServer();
            isConnected = result.IsSuccess;
            return result;
        }

        public override OperateResult<byte[]> Read(string address, ushort length)
        {
            try
            {
                if (!isConnected)
                    return new OperateResult<byte[]>("未连接");

                return base.Read(address, length);
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
      
        public override string ToString()
        {
            return base.ToString();
        }
   
        public override OperateResult Write(string address, byte[] value)
        {
            try
            {
                if (!isConnected)
                    return new OperateResult("未连接");

                return base.Write(address, value);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
