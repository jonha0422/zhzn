﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        public static frm_Main AppForm;
        public static string AppNameOrg;
        public static bool bAppIsStared = false;

        [STAThread]
        static void Main()
        {
            try
            {
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

                //处理UI线程异常
                //Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
                Application.ThreadException += Application_ThreadException;

                //处理未捕获的异常
                //AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

                //处理线程异常
                TaskScheduler.UnobservedTaskException += (sender, erro) =>
                {
                    MessageBox.Show("程序出现异常(task):" + erro.Exception.ToString());
                };

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                #region 设置默认字体、日期格式、汉化dev
                DevExpress.Utils.AppearanceObject.DefaultFont = new System.Drawing.Font("微软雅黑", 9);
                //             System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(“zh - CHS”);//使用DEV汉化资源文件
                //                                                                                                                        //设置程序区域语言设置中日期格式
                //             System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("zh - CHS");
                //             System.Globalization.DateTimeFormatInfo di = (System.Globalization.DateTimeFormatInfo)System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.Clone();
                //             di.DateSeparator = "-";
                //             di.ShortDatePattern = "yyyy - MM - dd";
                //             di.LongDatePattern = "yyyy 年 M 月 d 日";
                //             di.ShortTimePattern = "H: mm: ss";
                //             di.LongTimePattern = "H 时 mm 分 ss 秒 ";
                //             ci.DateTimeFormat = di;
                //             System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                #endregion

                //             WindowsFormsSettings.DefaultFont = new Font(“Segoe UI”, fontSize);
                //             WindowsFormsSettings.DefaultMenuFont = new Font(“Segoe UI”, fontSize);         

                BonusSkins.Register();
                SkinManager.EnableFormSkins();

                frmLogin dialog = new frmLogin();
                if (dialog.ShowDialog() == DialogResult.Cancel)
                    Application.Exit();

                #region 确定程序是否重复启动
                string procName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
                if (System.Diagnostics.Process.GetProcessesByName(procName).GetUpperBound(0) > 0)
                {
                    string message = string.Format("当前程序已经运行，请退出之后重新启动!\n程序名: {0}", procName);
                    MessageBox.Show(message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    System.Environment.Exit(0);
                }
                #endregion

                #region 确定是否进入远程模式
                if (HPglobal.m_bEnableRemoteDebug)
                {
                    while (!System.Diagnostics.Debugger.IsAttached)
                    {
                        Thread.Sleep(300);
                    }
                }
                #endregion

                #region 打开XML文件
                if (System.IO.File.Exists(HPglobal.m_strXmlFilename))
                {
                    HPglobal.m_MesInterfaceNode = null;
                    HPglobal.m_xmlSystemConfig.Load(HPglobal.m_strXmlFilename);
                }
                else
                {
                    string message = string.Format("当前文件不存在: {0}", HPglobal.m_strXmlFilename);
                    System.Environment.Exit(0);
                }
                #endregion

                #region 运行主界面
                AppForm = new frm_Main();

                AppNameOrg = AppForm.Text;
                AppForm.Text += " -- " + HPglobal.m_strModelName;
                bAppIsStared = true;

                Application.Run(AppForm);
                #endregion

            }
            catch (Exception ex)
            {
                string str = GetExceptionMsg(ex, string.Empty);
                MessageBox.Show(str, "系统错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            string str = GetExceptionMsg(ex, string.Empty);
            MessageBox.Show(str, "系统错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            var ex = e.Exception as Exception;
            string str = GetExceptionMsg(ex, string.Empty);
            MessageBox.Show(str, "系统错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        static string GetExceptionMsg(Exception ex, string backStr)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("****************************异常文本****************************");
            sb.AppendLine("【出现时间】：" + DateTime.Now.ToString());
            if (ex != null)
            {
                sb.AppendLine("【异常类型】：" + ex.GetType().Name);
                sb.AppendLine("【异常信息】：" + ex.Message);
                sb.AppendLine("【堆栈调用】：" + ex.StackTrace);
            }
            else
            {
                sb.AppendLine("【未处理异常】：" + backStr);
            }
            sb.AppendLine("***************************************************************");
            return sb.ToString();
        }

    }
}
