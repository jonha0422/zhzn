﻿namespace VisionLib
{
    partial class ucCameraView
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucCameraView));
            this.viewPort = new HalconDotNet.HWindowControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.None = new System.Windows.Forms.ToolStripMenuItem();
            this.Zoom = new System.Windows.Forms.ToolStripMenuItem();
            this.Move = new System.Windows.Forms.ToolStripMenuItem();
            this.Reset = new System.Windows.Forms.ToolStripMenuItem();
            this.tsParameters = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSaveImage = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // viewPort
            // 
            this.viewPort.BackColor = System.Drawing.Color.Black;
            this.viewPort.BorderColor = System.Drawing.Color.Black;
            this.viewPort.ContextMenuStrip = this.contextMenuStrip1;
            this.viewPort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewPort.ImagePart = new System.Drawing.Rectangle(0, 0, 640, 480);
            this.viewPort.Location = new System.Drawing.Point(0, 0);
            this.viewPort.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.viewPort.Name = "viewPort";
            this.viewPort.Size = new System.Drawing.Size(987, 746);
            this.viewPort.TabIndex = 0;
            this.viewPort.WindowSize = new System.Drawing.Size(987, 746);
            this.viewPort.HMouseMove += new HalconDotNet.HMouseEventHandler(this.viewPort_HMouseMove);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.None,
            this.Zoom,
            this.Move,
            this.Reset,
            this.tsParameters,
            this.tsSaveImage});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(186, 184);
            // 
            // None
            // 
            this.None.Checked = true;
            this.None.CheckState = System.Windows.Forms.CheckState.Checked;
            this.None.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.None.Name = "None";
            this.None.Size = new System.Drawing.Size(185, 30);
            this.None.Text = "None";
            this.None.Click += new System.EventHandler(this.None_Click);
            // 
            // Zoom
            // 
            this.Zoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Zoom.Image = ((System.Drawing.Image)(resources.GetObject("Zoom.Image")));
            this.Zoom.Name = "Zoom";
            this.Zoom.Size = new System.Drawing.Size(185, 30);
            this.Zoom.Text = "Zoom";
            this.Zoom.Click += new System.EventHandler(this.Zoom_Click);
            // 
            // Move
            // 
            this.Move.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Move.Image = ((System.Drawing.Image)(resources.GetObject("Move.Image")));
            this.Move.Name = "Move";
            this.Move.Size = new System.Drawing.Size(185, 30);
            this.Move.Text = "Move";
            this.Move.Click += new System.EventHandler(this.Move_Click);
            // 
            // Reset
            // 
            this.Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Reset.Image = ((System.Drawing.Image)(resources.GetObject("Reset.Image")));
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(185, 30);
            this.Reset.Text = "Reset";
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // tsParameters
            // 
            this.tsParameters.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsParameters.Image = ((System.Drawing.Image)(resources.GetObject("tsParameters.Image")));
            this.tsParameters.Name = "tsParameters";
            this.tsParameters.Size = new System.Drawing.Size(185, 30);
            this.tsParameters.Text = "Parameters";
            this.tsParameters.Click += new System.EventHandler(this.tsParameters_Click);
            // 
            // tsSaveImage
            // 
            this.tsSaveImage.Name = "tsSaveImage";
            this.tsSaveImage.Size = new System.Drawing.Size(185, 30);
            this.tsSaveImage.Text = "Save Image";
            this.tsSaveImage.Click += new System.EventHandler(this.tsSaveImage_Click);
            // 
            // ucCameraView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.viewPort);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ucCameraView";
            this.Size = new System.Drawing.Size(987, 746);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem None;
        private System.Windows.Forms.ToolStripMenuItem Zoom;
        private System.Windows.Forms.ToolStripMenuItem Move;
        private System.Windows.Forms.ToolStripMenuItem Reset;
        public HalconDotNet.HWindowControl viewPort;
        private System.Windows.Forms.ToolStripMenuItem tsParameters;
        private System.Windows.Forms.ToolStripMenuItem tsSaveImage;
    }
}
