﻿namespace Apk_HP_Application.程序文件夹分类._3打码功割控制程序
{
    partial class frm_LaserStationMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_LaserStationMain));
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.NP_主页运行 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.checkButton1 = new DevExpress.XtraEditors.CheckButton();
            this.imageList16 = new System.Windows.Forms.ImageList(this.components);
            this.checkButton2 = new DevExpress.XtraEditors.CheckButton();
            this.LEDModbusServer = new HslCommunication.Controls.UserLantern();
            this.LEDUnloadPLCState = new HslCommunication.Controls.UserLantern();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.userLantern3 = new HslCommunication.Controls.UserLantern();
            this.userLantern4 = new HslCommunication.Controls.UserLantern();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.label53 = new System.Windows.Forms.Label();
            this.richTextLaseCutting = new System.Windows.Forms.RichTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.richTextRotate = new System.Windows.Forms.RichTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.richTextLaserMarking = new System.Windows.Forms.RichTextBox();
            this.richTextLoad = new System.Windows.Forms.RichTextBox();
            this.btmInvoke_StopMachine = new DevExpress.XtraEditors.SimpleButton();
            this.btmInvoke_StartMachine = new DevExpress.XtraEditors.SimpleButton();
            this.btmInvoke_ResetMachine = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.NP_设置 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.启用禁用翻转 = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.NP_视觉 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.连续采集 = new DevExpress.XtraEditors.SimpleButton();
            this.单次采集 = new DevExpress.XtraEditors.SimpleButton();
            this.NP_手动监控 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.下料工位上料完成 = new DevExpress.XtraEditors.CheckButton();
            this.下料工位允许上料 = new DevExpress.XtraEditors.CheckButton();
            this.切割工位上料完成 = new DevExpress.XtraEditors.CheckButton();
            this.切割工位允许上料 = new DevExpress.XtraEditors.CheckButton();
            this.打码工位上料完成 = new DevExpress.XtraEditors.CheckButton();
            this.打码工位允许上料 = new DevExpress.XtraEditors.CheckButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LED_Out0 = new System.Windows.Forms.CheckBox();
            this.LED_Out31 = new System.Windows.Forms.CheckBox();
            this.LED_Out1 = new System.Windows.Forms.CheckBox();
            this.LED_Out30 = new System.Windows.Forms.CheckBox();
            this.LED_Out2 = new System.Windows.Forms.CheckBox();
            this.LED_Out29 = new System.Windows.Forms.CheckBox();
            this.LED_Out3 = new System.Windows.Forms.CheckBox();
            this.LED_Out28 = new System.Windows.Forms.CheckBox();
            this.LED_Out4 = new System.Windows.Forms.CheckBox();
            this.LED_Out27 = new System.Windows.Forms.CheckBox();
            this.LED_Out5 = new System.Windows.Forms.CheckBox();
            this.LED_Out26 = new System.Windows.Forms.CheckBox();
            this.LED_Out6 = new System.Windows.Forms.CheckBox();
            this.LED_Out25 = new System.Windows.Forms.CheckBox();
            this.LED_Out7 = new System.Windows.Forms.CheckBox();
            this.LED_Out24 = new System.Windows.Forms.CheckBox();
            this.LED_Out8 = new System.Windows.Forms.CheckBox();
            this.LED_Out23 = new System.Windows.Forms.CheckBox();
            this.LED_Out9 = new System.Windows.Forms.CheckBox();
            this.LED_Out22 = new System.Windows.Forms.CheckBox();
            this.LED_Out10 = new System.Windows.Forms.CheckBox();
            this.LED_Out21 = new System.Windows.Forms.CheckBox();
            this.LED_Out11 = new System.Windows.Forms.CheckBox();
            this.LED_Out20 = new System.Windows.Forms.CheckBox();
            this.LED_Out12 = new System.Windows.Forms.CheckBox();
            this.LED_Out19 = new System.Windows.Forms.CheckBox();
            this.LED_Out13 = new System.Windows.Forms.CheckBox();
            this.LED_Out18 = new System.Windows.Forms.CheckBox();
            this.LED_Out14 = new System.Windows.Forms.CheckBox();
            this.LED_Out17 = new System.Windows.Forms.CheckBox();
            this.LED_Out15 = new System.Windows.Forms.CheckBox();
            this.LED_Out16 = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LED_In0 = new HslCommunication.Controls.UserLantern();
            this.label45 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.LED_In1 = new HslCommunication.Controls.UserLantern();
            this.label38 = new System.Windows.Forms.Label();
            this.LED_In2 = new HslCommunication.Controls.UserLantern();
            this.label39 = new System.Windows.Forms.Label();
            this.LED_In3 = new HslCommunication.Controls.UserLantern();
            this.label40 = new System.Windows.Forms.Label();
            this.LED_In4 = new HslCommunication.Controls.UserLantern();
            this.label41 = new System.Windows.Forms.Label();
            this.LED_In5 = new HslCommunication.Controls.UserLantern();
            this.label42 = new System.Windows.Forms.Label();
            this.LED_In6 = new HslCommunication.Controls.UserLantern();
            this.label43 = new System.Windows.Forms.Label();
            this.LED_In7 = new HslCommunication.Controls.UserLantern();
            this.label44 = new System.Windows.Forms.Label();
            this.LED_In9 = new HslCommunication.Controls.UserLantern();
            this.label26 = new System.Windows.Forms.Label();
            this.LED_In10 = new HslCommunication.Controls.UserLantern();
            this.label30 = new System.Windows.Forms.Label();
            this.LED_In11 = new HslCommunication.Controls.UserLantern();
            this.label31 = new System.Windows.Forms.Label();
            this.LED_In12 = new HslCommunication.Controls.UserLantern();
            this.label32 = new System.Windows.Forms.Label();
            this.LED_In13 = new HslCommunication.Controls.UserLantern();
            this.label33 = new System.Windows.Forms.Label();
            this.LED_In14 = new HslCommunication.Controls.UserLantern();
            this.label34 = new System.Windows.Forms.Label();
            this.LED_In15 = new HslCommunication.Controls.UserLantern();
            this.label35 = new System.Windows.Forms.Label();
            this.LED_In8 = new HslCommunication.Controls.UserLantern();
            this.label36 = new System.Windows.Forms.Label();
            this.LED_In17 = new HslCommunication.Controls.UserLantern();
            this.LED_In24 = new HslCommunication.Controls.UserLantern();
            this.LED_In18 = new HslCommunication.Controls.UserLantern();
            this.LED_In31 = new HslCommunication.Controls.UserLantern();
            this.LED_In19 = new HslCommunication.Controls.UserLantern();
            this.LED_In30 = new HslCommunication.Controls.UserLantern();
            this.LED_In20 = new HslCommunication.Controls.UserLantern();
            this.LED_In29 = new HslCommunication.Controls.UserLantern();
            this.LED_In21 = new HslCommunication.Controls.UserLantern();
            this.LED_In28 = new HslCommunication.Controls.UserLantern();
            this.LED_In22 = new HslCommunication.Controls.UserLantern();
            this.LED_In27 = new HslCommunication.Controls.UserLantern();
            this.LED_In23 = new HslCommunication.Controls.UserLantern();
            this.LED_In26 = new HslCommunication.Controls.UserLantern();
            this.LED_In16 = new HslCommunication.Controls.UserLantern();
            this.LED_In25 = new HslCommunication.Controls.UserLantern();
            this.userLanternIO = new HslCommunication.Controls.UserLantern();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.Y轴停止 = new DevExpress.XtraEditors.SimpleButton();
            this.label55 = new System.Windows.Forms.Label();
            this.X轴停止 = new DevExpress.XtraEditors.SimpleButton();
            this.label54 = new System.Windows.Forms.Label();
            this.Y轴报警清除 = new DevExpress.XtraEditors.SimpleButton();
            this.X轴报警清除 = new DevExpress.XtraEditors.SimpleButton();
            this.txtDst_Y = new DevExpress.XtraEditors.TextEdit();
            this.txtDst_X = new DevExpress.XtraEditors.TextEdit();
            this.Y轴启用禁用 = new DevExpress.XtraEditors.CheckButton();
            this.X轴启用禁用 = new DevExpress.XtraEditors.CheckButton();
            this.userLanternAXIS = new HslCommunication.Controls.UserLantern();
            this.label9 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.LED_ALARM_Y = new HslCommunication.Controls.UserLantern();
            this.LED_INP_Y = new HslCommunication.Controls.UserLantern();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.LED_ALARM_X = new HslCommunication.Controls.UserLantern();
            this.LED_INP_X = new HslCommunication.Controls.UserLantern();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.LED_ORG_Y = new HslCommunication.Controls.UserLantern();
            this.LED_ELN_Y = new HslCommunication.Controls.UserLantern();
            this.LED_ELP_Y = new HslCommunication.Controls.UserLantern();
            this.Y轴反转 = new DevExpress.XtraEditors.SimpleButton();
            this.Y轴正转 = new DevExpress.XtraEditors.SimpleButton();
            this.lable11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.LED_ORG_X = new HslCommunication.Controls.UserLantern();
            this.LED_ELN_X = new HslCommunication.Controls.UserLantern();
            this.LED_ELP_X = new HslCommunication.Controls.UserLantern();
            this.X轴反转 = new DevExpress.XtraEditors.SimpleButton();
            this.X轴正转 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.翻转工位联动 = new DevExpress.XtraEditors.SimpleButton();
            this.回流皮带控制 = new DevExpress.XtraEditors.CheckButton();
            this.切割皮带控制 = new DevExpress.XtraEditors.CheckButton();
            this.上料皮带控制 = new DevExpress.XtraEditors.CheckButton();
            this.切割气缸顶升 = new DevExpress.XtraEditors.CheckButton();
            this.打码气缸顶升 = new DevExpress.XtraEditors.CheckButton();
            this.切割气缸阻挡 = new DevExpress.XtraEditors.CheckButton();
            this.翻转工位夹紧 = new DevExpress.XtraEditors.CheckButton();
            this.翻转工位旋转 = new DevExpress.XtraEditors.CheckButton();
            this.翻转气缸顶升 = new DevExpress.XtraEditors.CheckButton();
            this.翻转气缸阻挡 = new DevExpress.XtraEditors.CheckButton();
            this.打码气缸阻挡 = new DevExpress.XtraEditors.CheckButton();
            this.待料气缸阻挡 = new DevExpress.XtraEditors.CheckButton();
            this.NP_生产数据查询 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.NP_工艺参数 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNG_上料调度 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNG_烧录 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNG_打码切割 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNG_下料组盘 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNG_打标包装 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.timer_IO = new System.Windows.Forms.Timer(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.NP_主页运行.SuspendLayout();
            this.NP_设置.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.启用禁用翻转.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.NP_视觉.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            this.NP_手动监控.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDst_Y.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDst_X.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            this.NP_工艺参数.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // navigationPane1
            // 
            this.navigationPane1.AllowHtmlDraw = true;
            this.navigationPane1.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.Appearance.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Hovered.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.AppearanceButton.Hovered.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Normal.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.navigationPane1.AppearanceButton.Normal.Options.UseFont = true;
            this.navigationPane1.AppearanceButton.Pressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.navigationPane1.AppearanceButton.Pressed.Options.UseFont = true;
            this.navigationPane1.Controls.Add(this.NP_主页运行);
            this.navigationPane1.Controls.Add(this.NP_设置);
            this.navigationPane1.Controls.Add(this.NP_视觉);
            this.navigationPane1.Controls.Add(this.NP_手动监控);
            this.navigationPane1.Controls.Add(this.NP_生产数据查询);
            this.navigationPane1.Controls.Add(this.NP_工艺参数);
            this.navigationPane1.Location = new System.Drawing.Point(0, 0);
            this.navigationPane1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AllowBorderColorBlending = true;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.NP_主页运行,
            this.NP_设置,
            this.NP_工艺参数,
            this.NP_手动监控,
            this.NP_视觉,
            this.NP_生产数据查询});
            this.navigationPane1.RegularSize = new System.Drawing.Size(1904, 706);
            this.navigationPane1.RibbonAndBarsMergeStyle = DevExpress.XtraBars.Docking2010.Views.RibbonAndBarsMergeStyle.Always;
            this.navigationPane1.SelectedPage = this.NP_主页运行;
            this.navigationPane1.ShowToolTips = DevExpress.Utils.DefaultBoolean.True;
            this.navigationPane1.Size = new System.Drawing.Size(1904, 706);
            this.navigationPane1.TabIndex = 1;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // NP_主页运行
            // 
            this.NP_主页运行.Caption = " 主页运行";
            this.NP_主页运行.Controls.Add(this.checkButton1);
            this.NP_主页运行.Controls.Add(this.checkButton2);
            this.NP_主页运行.Controls.Add(this.LEDModbusServer);
            this.NP_主页运行.Controls.Add(this.LEDUnloadPLCState);
            this.NP_主页运行.Controls.Add(this.labelControl7);
            this.NP_主页运行.Controls.Add(this.labelControl8);
            this.NP_主页运行.Controls.Add(this.userLantern3);
            this.NP_主页运行.Controls.Add(this.userLantern4);
            this.NP_主页运行.Controls.Add(this.labelControl6);
            this.NP_主页运行.Controls.Add(this.labelControl5);
            this.NP_主页运行.Controls.Add(this.label53);
            this.NP_主页运行.Controls.Add(this.richTextLaseCutting);
            this.NP_主页运行.Controls.Add(this.label29);
            this.NP_主页运行.Controls.Add(this.richTextRotate);
            this.NP_主页运行.Controls.Add(this.label28);
            this.NP_主页运行.Controls.Add(this.label25);
            this.NP_主页运行.Controls.Add(this.richTextLaserMarking);
            this.NP_主页运行.Controls.Add(this.richTextLoad);
            this.NP_主页运行.Controls.Add(this.btmInvoke_StopMachine);
            this.NP_主页运行.Controls.Add(this.btmInvoke_StartMachine);
            this.NP_主页运行.Controls.Add(this.btmInvoke_ResetMachine);
            this.NP_主页运行.Controls.Add(this.labelControl4);
            this.NP_主页运行.Controls.Add(this.labelControl3);
            this.NP_主页运行.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.NP_主页运行.Image = ((System.Drawing.Image)(resources.GetObject("NP_主页运行.Image")));
            this.NP_主页运行.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_主页运行.Name = "NP_主页运行";
            this.NP_主页运行.Size = new System.Drawing.Size(1684, 647);
            // 
            // checkButton1
            // 
            this.checkButton1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkButton1.Appearance.Options.UseFont = true;
            this.checkButton1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearanceDisabled.Options.UseFont = true;
            this.checkButton1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearanceHovered.Options.UseFont = true;
            this.checkButton1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton1.AppearancePressed.Options.UseFont = true;
            this.checkButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.checkButton1.ImageOptions.ImageIndex = 0;
            this.checkButton1.ImageOptions.ImageList = this.imageList16;
            this.checkButton1.Location = new System.Drawing.Point(0, 284);
            this.checkButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkButton1.Name = "checkButton1";
            this.checkButton1.Size = new System.Drawing.Size(133, 71);
            this.checkButton1.TabIndex = 191;
            this.checkButton1.Text = "急停关闭/打开";
            // 
            // imageList16
            // 
            this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
            this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList16.Images.SetKeyName(0, "emblem-nowrite.png");
            this.imageList16.Images.SetKeyName(1, "emblem-ok.png");
            this.imageList16.Images.SetKeyName(2, "stock_lock-ok.png");
            this.imageList16.Images.SetKeyName(3, "stock_lock-open.png");
            this.imageList16.Images.SetKeyName(4, "stock_music-library.png");
            this.imageList16.Images.SetKeyName(5, "stock_new-drawing.png");
            this.imageList16.Images.SetKeyName(6, "stock_new-formula.png");
            this.imageList16.Images.SetKeyName(7, "stock_new-html.png");
            this.imageList16.Images.SetKeyName(8, "stock_new-master-document.png");
            this.imageList16.Images.SetKeyName(9, "stock_new-presentation.png");
            // 
            // checkButton2
            // 
            this.checkButton2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkButton2.Appearance.Options.UseFont = true;
            this.checkButton2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton2.AppearanceDisabled.Options.UseFont = true;
            this.checkButton2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton2.AppearanceHovered.Options.UseFont = true;
            this.checkButton2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.checkButton2.AppearancePressed.Options.UseFont = true;
            this.checkButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.checkButton2.ImageOptions.ImageIndex = 1;
            this.checkButton2.ImageOptions.ImageList = this.imageList16;
            this.checkButton2.Location = new System.Drawing.Point(0, 214);
            this.checkButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkButton2.Name = "checkButton2";
            this.checkButton2.Size = new System.Drawing.Size(133, 71);
            this.checkButton2.TabIndex = 190;
            this.checkButton2.Text = "安全门关闭/打开";
            // 
            // LEDModbusServer
            // 
            this.LEDModbusServer.BackColor = System.Drawing.Color.Transparent;
            this.LEDModbusServer.LanternBackground = System.Drawing.Color.Gray;
            this.LEDModbusServer.Location = new System.Drawing.Point(1616, 68);
            this.LEDModbusServer.Margin = new System.Windows.Forms.Padding(156, 2777, 156, 2777);
            this.LEDModbusServer.Name = "LEDModbusServer";
            this.LEDModbusServer.Size = new System.Drawing.Size(32, 39);
            this.LEDModbusServer.TabIndex = 185;
            // 
            // LEDUnloadPLCState
            // 
            this.LEDUnloadPLCState.BackColor = System.Drawing.Color.Transparent;
            this.LEDUnloadPLCState.LanternBackground = System.Drawing.Color.Gray;
            this.LEDUnloadPLCState.Location = new System.Drawing.Point(1476, 68);
            this.LEDUnloadPLCState.Margin = new System.Windows.Forms.Padding(72, 1190, 72, 1190);
            this.LEDUnloadPLCState.Name = "LEDUnloadPLCState";
            this.LEDUnloadPLCState.Size = new System.Drawing.Size(32, 39);
            this.LEDUnloadPLCState.TabIndex = 184;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearanceDisabled.Options.UseFont = true;
            this.labelControl7.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearanceHovered.Options.UseFont = true;
            this.labelControl7.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl7.AppearancePressed.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(-817, 9);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(93, 17);
            this.labelControl7.TabIndex = 183;
            this.labelControl7.Text = "下料PLC连接状态";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceDisabled.Options.UseFont = true;
            this.labelControl8.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceHovered.Options.UseFont = true;
            this.labelControl8.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearancePressed.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(-817, -52);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(132, 17);
            this.labelControl8.TabIndex = 182;
            this.labelControl8.Text = "Modbus TCP/IP Server";
            // 
            // userLantern3
            // 
            this.userLantern3.BackColor = System.Drawing.Color.Transparent;
            this.userLantern3.LanternBackground = System.Drawing.Color.Gray;
            this.userLantern3.Location = new System.Drawing.Point(2371, 679);
            this.userLantern3.Margin = new System.Windows.Forms.Padding(72, 1190, 72, 1190);
            this.userLantern3.Name = "userLantern3";
            this.userLantern3.Size = new System.Drawing.Size(150, 213);
            this.userLantern3.TabIndex = 181;
            // 
            // userLantern4
            // 
            this.userLantern4.BackColor = System.Drawing.Color.Transparent;
            this.userLantern4.LanternBackground = System.Drawing.Color.Gray;
            this.userLantern4.Location = new System.Drawing.Point(2371, 342);
            this.userLantern4.Margin = new System.Windows.Forms.Padding(33, 510, 33, 510);
            this.userLantern4.Name = "userLantern4";
            this.userLantern4.Size = new System.Drawing.Size(150, 213);
            this.userLantern4.TabIndex = 180;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl6.AppearanceDisabled.Options.UseFont = true;
            this.labelControl6.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl6.AppearanceHovered.Options.UseFont = true;
            this.labelControl6.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl6.AppearancePressed.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(1437, 109);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(93, 17);
            this.labelControl6.TabIndex = 179;
            this.labelControl6.Text = "下料PLC连接状态";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceDisabled.Options.UseFont = true;
            this.labelControl5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceHovered.Options.UseFont = true;
            this.labelControl5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearancePressed.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(1551, 109);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(132, 17);
            this.labelControl5.TabIndex = 178;
            this.labelControl5.Text = "Modbus TCP/IP Server";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.LightSalmon;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label53.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label53.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label53.Location = new System.Drawing.Point(1355, 669);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(106, 19);
            this.label53.TabIndex = 112;
            this.label53.Text = "切割运行信息输出";
            // 
            // richTextLaseCutting
            // 
            this.richTextLaseCutting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextLaseCutting.Location = new System.Drawing.Point(1354, 696);
            this.richTextLaseCutting.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextLaseCutting.Name = "richTextLaseCutting";
            this.richTextLaseCutting.Size = new System.Drawing.Size(326, 147);
            this.richTextLaseCutting.TabIndex = 111;
            this.richTextLaseCutting.Text = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.LightSalmon;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label29.Location = new System.Drawing.Point(1355, 489);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(106, 19);
            this.label29.TabIndex = 110;
            this.label29.Text = "翻转运行信息输出";
            // 
            // richTextRotate
            // 
            this.richTextRotate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextRotate.Location = new System.Drawing.Point(1354, 516);
            this.richTextRotate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextRotate.Name = "richTextRotate";
            this.richTextRotate.Size = new System.Drawing.Size(326, 147);
            this.richTextRotate.TabIndex = 109;
            this.richTextRotate.Text = "";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.LightSalmon;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label28.Location = new System.Drawing.Point(1355, 311);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(106, 19);
            this.label28.TabIndex = 108;
            this.label28.Text = "打码运行信息输出";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.LightSalmon;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(1357, 134);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(106, 19);
            this.label25.TabIndex = 107;
            this.label25.Text = "上料运行信息输出";
            // 
            // richTextLaserMarking
            // 
            this.richTextLaserMarking.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextLaserMarking.Location = new System.Drawing.Point(1354, 338);
            this.richTextLaserMarking.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextLaserMarking.Name = "richTextLaserMarking";
            this.richTextLaserMarking.Size = new System.Drawing.Size(326, 147);
            this.richTextLaserMarking.TabIndex = 105;
            this.richTextLaserMarking.Text = "";
            // 
            // richTextLoad
            // 
            this.richTextLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextLoad.Location = new System.Drawing.Point(1356, 160);
            this.richTextLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextLoad.Name = "richTextLoad";
            this.richTextLoad.Size = new System.Drawing.Size(327, 147);
            this.richTextLoad.TabIndex = 106;
            this.richTextLoad.Text = "";
            // 
            // btmInvoke_StopMachine
            // 
            this.btmInvoke_StopMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_StopMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StopMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_StopMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_StopMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_StopMachine.ImageOptions.Image")));
            this.btmInvoke_StopMachine.Location = new System.Drawing.Point(0, 144);
            this.btmInvoke_StopMachine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmInvoke_StopMachine.Name = "btmInvoke_StopMachine";
            this.btmInvoke_StopMachine.Size = new System.Drawing.Size(133, 71);
            this.btmInvoke_StopMachine.TabIndex = 104;
            this.btmInvoke_StopMachine.Text = "停止";
            this.btmInvoke_StopMachine.Click += new System.EventHandler(this.btmInvoke_StopMachine_Click);
            // 
            // btmInvoke_StartMachine
            // 
            this.btmInvoke_StartMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_StartMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_StartMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_StartMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_StartMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_StartMachine.ImageOptions.Image")));
            this.btmInvoke_StartMachine.Location = new System.Drawing.Point(0, 74);
            this.btmInvoke_StartMachine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmInvoke_StartMachine.Name = "btmInvoke_StartMachine";
            this.btmInvoke_StartMachine.Size = new System.Drawing.Size(133, 71);
            this.btmInvoke_StartMachine.TabIndex = 103;
            this.btmInvoke_StartMachine.Text = "启动";
            this.btmInvoke_StartMachine.Click += new System.EventHandler(this.btmInvoke_StartMachine_Click);
            // 
            // btmInvoke_ResetMachine
            // 
            this.btmInvoke_ResetMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmInvoke_ResetMachine.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ResetMachine.Appearance.Image")));
            this.btmInvoke_ResetMachine.Appearance.Options.UseFont = true;
            this.btmInvoke_ResetMachine.Appearance.Options.UseImage = true;
            this.btmInvoke_ResetMachine.Appearance.Options.UseTextOptions = true;
            this.btmInvoke_ResetMachine.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btmInvoke_ResetMachine.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btmInvoke_ResetMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmInvoke_ResetMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearanceHovered.Options.UseFont = true;
            this.btmInvoke_ResetMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.btmInvoke_ResetMachine.AppearancePressed.Options.UseFont = true;
            this.btmInvoke_ResetMachine.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmInvoke_ResetMachine.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmInvoke_ResetMachine.ImageOptions.Image")));
            this.btmInvoke_ResetMachine.Location = new System.Drawing.Point(0, 4);
            this.btmInvoke_ResetMachine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmInvoke_ResetMachine.Name = "btmInvoke_ResetMachine";
            this.btmInvoke_ResetMachine.Size = new System.Drawing.Size(133, 71);
            this.btmInvoke_ResetMachine.TabIndex = 102;
            this.btmInvoke_ResetMachine.Text = "复位";
            this.btmInvoke_ResetMachine.Click += new System.EventHandler(this.btmInvoke_ResetMachine_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl4.AppearanceDisabled.Options.UseFont = true;
            this.labelControl4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl4.AppearanceHovered.Options.UseFont = true;
            this.labelControl4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl4.AppearancePressed.Options.UseFont = true;
            this.labelControl4.AutoEllipsis = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.labelControl4.Location = new System.Drawing.Point(1437, 4);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(128, 61);
            this.labelControl4.TabIndex = 94;
            this.labelControl4.Text = "急停";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl3.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl3.AppearanceDisabled.Options.UseFont = true;
            this.labelControl3.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl3.AppearanceHovered.Options.UseFont = true;
            this.labelControl3.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 15.75F);
            this.labelControl3.AppearancePressed.Options.UseFont = true;
            this.labelControl3.AutoEllipsis = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.labelControl3.Location = new System.Drawing.Point(1568, 4);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(128, 61);
            this.labelControl3.TabIndex = 93;
            this.labelControl3.Text = "安全门关闭";
            // 
            // NP_设置
            // 
            this.NP_设置.Caption = " 系统设置";
            this.NP_设置.Controls.Add(this.groupControl8);
            this.NP_设置.Controls.Add(this.启用禁用翻转);
            this.NP_设置.Controls.Add(this.groupControl3);
            this.NP_设置.Image = ((System.Drawing.Image)(resources.GetObject("NP_设置.Image")));
            this.NP_设置.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_设置.Name = "NP_设置";
            this.NP_设置.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.NP_设置.Size = new System.Drawing.Size(1684, 647);
            // 
            // groupControl8
            // 
            this.groupControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl8.Appearance.Options.UseFont = true;
            this.groupControl8.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl8.AppearanceCaption.Options.UseFont = true;
            this.groupControl8.Location = new System.Drawing.Point(311, 4);
            this.groupControl8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(291, 186);
            this.groupControl8.TabIndex = 98;
            this.groupControl8.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // 启用禁用翻转
            // 
            this.启用禁用翻转.Location = new System.Drawing.Point(3, 214);
            this.启用禁用翻转.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.启用禁用翻转.Name = "启用禁用翻转";
            this.启用禁用翻转.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.启用禁用翻转.Properties.Appearance.Options.UseFont = true;
            this.启用禁用翻转.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用翻转.Properties.AppearanceDisabled.Options.UseFont = true;
            this.启用禁用翻转.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用翻转.Properties.AppearanceFocused.Options.UseFont = true;
            this.启用禁用翻转.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用翻转.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.启用禁用翻转.Properties.Caption = "启用/禁用翻转";
            this.启用禁用翻转.Size = new System.Drawing.Size(106, 21);
            this.启用禁用翻转.TabIndex = 97;
            this.启用禁用翻转.CheckedChanged += new System.EventHandler(this.启用禁用翻转_CheckedChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Location = new System.Drawing.Point(3, 4);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(291, 186);
            this.groupControl3.TabIndex = 96;
            this.groupControl3.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // NP_视觉
            // 
            this.NP_视觉.Caption = "   视觉";
            this.NP_视觉.Controls.Add(this.groupControl7);
            this.NP_视觉.Image = ((System.Drawing.Image)(resources.GetObject("NP_视觉.Image")));
            this.NP_视觉.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_视觉.Name = "NP_视觉";
            this.NP_视觉.Size = new System.Drawing.Size(1683, 1021);
            // 
            // groupControl7
            // 
            this.groupControl7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl7.Appearance.Options.UseFont = true;
            this.groupControl7.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl7.AppearanceCaption.Options.UseFont = true;
            this.groupControl7.Controls.Add(this.连续采集);
            this.groupControl7.Controls.Add(this.单次采集);
            this.groupControl7.Location = new System.Drawing.Point(2, 0);
            this.groupControl7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(972, 829);
            this.groupControl7.TabIndex = 169;
            this.groupControl7.Text = "图像操作";
            // 
            // 连续采集
            // 
            this.连续采集.Appearance.BackColor = System.Drawing.Color.Red;
            this.连续采集.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.连续采集.Appearance.Options.UseBackColor = true;
            this.连续采集.Appearance.Options.UseFont = true;
            this.连续采集.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.连续采集.AppearanceDisabled.Options.UseFont = true;
            this.连续采集.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.连续采集.AppearanceHovered.Options.UseFont = true;
            this.连续采集.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.连续采集.AppearancePressed.Options.UseFont = true;
            this.连续采集.Location = new System.Drawing.Point(5, 112);
            this.连续采集.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.连续采集.Name = "连续采集";
            this.连续采集.Size = new System.Drawing.Size(128, 58);
            this.连续采集.TabIndex = 176;
            this.连续采集.Text = "连续采集";
            // 
            // 单次采集
            // 
            this.单次采集.Appearance.BackColor = System.Drawing.Color.Red;
            this.单次采集.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.单次采集.Appearance.Options.UseBackColor = true;
            this.单次采集.Appearance.Options.UseFont = true;
            this.单次采集.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.单次采集.AppearanceDisabled.Options.UseFont = true;
            this.单次采集.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.单次采集.AppearanceHovered.Options.UseFont = true;
            this.单次采集.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.单次采集.AppearancePressed.Options.UseFont = true;
            this.单次采集.Location = new System.Drawing.Point(5, 46);
            this.单次采集.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.单次采集.Name = "单次采集";
            this.单次采集.Size = new System.Drawing.Size(128, 58);
            this.单次采集.TabIndex = 175;
            this.单次采集.Text = "单次采集";
            // 
            // NP_手动监控
            // 
            this.NP_手动监控.Caption = " 手动&监控";
            this.NP_手动监控.Controls.Add(this.groupControl9);
            this.NP_手动监控.Controls.Add(this.groupControl1);
            this.NP_手动监控.Controls.Add(this.groupControl6);
            this.NP_手动监控.Controls.Add(this.groupControl2);
            this.NP_手动监控.Controls.Add(this.groupControl4);
            this.NP_手动监控.Controls.Add(this.groupControl5);
            this.NP_手动监控.Image = ((System.Drawing.Image)(resources.GetObject("NP_手动监控.Image")));
            this.NP_手动监控.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_手动监控.Name = "NP_手动监控";
            this.NP_手动监控.Size = new System.Drawing.Size(1684, 647);
            // 
            // groupControl9
            // 
            this.groupControl9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl9.Appearance.Options.UseFont = true;
            this.groupControl9.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl9.AppearanceCaption.Options.UseFont = true;
            this.groupControl9.Controls.Add(this.下料工位上料完成);
            this.groupControl9.Controls.Add(this.下料工位允许上料);
            this.groupControl9.Controls.Add(this.切割工位上料完成);
            this.groupControl9.Controls.Add(this.切割工位允许上料);
            this.groupControl9.Controls.Add(this.打码工位上料完成);
            this.groupControl9.Controls.Add(this.打码工位允许上料);
            this.groupControl9.Location = new System.Drawing.Point(728, 4);
            this.groupControl9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(174, 561);
            this.groupControl9.TabIndex = 169;
            this.groupControl9.Text = "打码机操作";
            // 
            // 下料工位上料完成
            // 
            this.下料工位上料完成.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.下料工位上料完成.Appearance.Options.UseFont = true;
            this.下料工位上料完成.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料工位上料完成.AppearanceDisabled.Options.UseFont = true;
            this.下料工位上料完成.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料工位上料完成.AppearanceHovered.Options.UseFont = true;
            this.下料工位上料完成.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料工位上料完成.AppearancePressed.Options.UseFont = true;
            this.下料工位上料完成.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.下料工位上料完成.Location = new System.Drawing.Point(22, 283);
            this.下料工位上料完成.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.下料工位上料完成.Name = "下料工位上料完成";
            this.下料工位上料完成.Size = new System.Drawing.Size(128, 29);
            this.下料工位上料完成.TabIndex = 187;
            this.下料工位上料完成.Text = "下料工位上料完成";
            this.下料工位上料完成.CheckedChanged += new System.EventHandler(this.下料工位上料完成_CheckedChanged);
            // 
            // 下料工位允许上料
            // 
            this.下料工位允许上料.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.下料工位允许上料.Appearance.Options.UseFont = true;
            this.下料工位允许上料.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料工位允许上料.AppearanceDisabled.Options.UseFont = true;
            this.下料工位允许上料.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料工位允许上料.AppearanceHovered.Options.UseFont = true;
            this.下料工位允许上料.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.下料工位允许上料.AppearancePressed.Options.UseFont = true;
            this.下料工位允许上料.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.下料工位允许上料.Location = new System.Drawing.Point(22, 247);
            this.下料工位允许上料.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.下料工位允许上料.Name = "下料工位允许上料";
            this.下料工位允许上料.Size = new System.Drawing.Size(128, 29);
            this.下料工位允许上料.TabIndex = 186;
            this.下料工位允许上料.Text = "下料工位允许上料";
            this.下料工位允许上料.CheckedChanged += new System.EventHandler(this.下料工位允许上料_CheckedChanged);
            // 
            // 切割工位上料完成
            // 
            this.切割工位上料完成.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.切割工位上料完成.Appearance.Options.UseFont = true;
            this.切割工位上料完成.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割工位上料完成.AppearanceDisabled.Options.UseFont = true;
            this.切割工位上料完成.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割工位上料完成.AppearanceHovered.Options.UseFont = true;
            this.切割工位上料完成.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割工位上料完成.AppearancePressed.Options.UseFont = true;
            this.切割工位上料完成.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.切割工位上料完成.Location = new System.Drawing.Point(22, 180);
            this.切割工位上料完成.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.切割工位上料完成.Name = "切割工位上料完成";
            this.切割工位上料完成.Size = new System.Drawing.Size(128, 29);
            this.切割工位上料完成.TabIndex = 185;
            this.切割工位上料完成.Text = "切割工位上料完成";
            this.切割工位上料完成.CheckedChanged += new System.EventHandler(this.切割工位上料完成_CheckedChanged);
            // 
            // 切割工位允许上料
            // 
            this.切割工位允许上料.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.切割工位允许上料.Appearance.Options.UseFont = true;
            this.切割工位允许上料.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割工位允许上料.AppearanceDisabled.Options.UseFont = true;
            this.切割工位允许上料.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割工位允许上料.AppearanceHovered.Options.UseFont = true;
            this.切割工位允许上料.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割工位允许上料.AppearancePressed.Options.UseFont = true;
            this.切割工位允许上料.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.切割工位允许上料.Location = new System.Drawing.Point(22, 143);
            this.切割工位允许上料.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.切割工位允许上料.Name = "切割工位允许上料";
            this.切割工位允许上料.Size = new System.Drawing.Size(128, 29);
            this.切割工位允许上料.TabIndex = 184;
            this.切割工位允许上料.Text = "切割工位允许上料";
            this.切割工位允许上料.CheckedChanged += new System.EventHandler(this.切割工位允许上料_CheckedChanged);
            // 
            // 打码工位上料完成
            // 
            this.打码工位上料完成.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.打码工位上料完成.Appearance.Options.UseFont = true;
            this.打码工位上料完成.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码工位上料完成.AppearanceDisabled.Options.UseFont = true;
            this.打码工位上料完成.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码工位上料完成.AppearanceHovered.Options.UseFont = true;
            this.打码工位上料完成.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码工位上料完成.AppearancePressed.Options.UseFont = true;
            this.打码工位上料完成.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.打码工位上料完成.Location = new System.Drawing.Point(22, 70);
            this.打码工位上料完成.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.打码工位上料完成.Name = "打码工位上料完成";
            this.打码工位上料完成.Size = new System.Drawing.Size(128, 29);
            this.打码工位上料完成.TabIndex = 183;
            this.打码工位上料完成.Text = "打码工位上料完成";
            this.打码工位上料完成.CheckedChanged += new System.EventHandler(this.打码工位上料完成_CheckedChanged);
            // 
            // 打码工位允许上料
            // 
            this.打码工位允许上料.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.打码工位允许上料.Appearance.Options.UseFont = true;
            this.打码工位允许上料.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码工位允许上料.AppearanceDisabled.Options.UseFont = true;
            this.打码工位允许上料.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码工位允许上料.AppearanceHovered.Options.UseFont = true;
            this.打码工位允许上料.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码工位允许上料.AppearancePressed.Options.UseFont = true;
            this.打码工位允许上料.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.打码工位允许上料.Location = new System.Drawing.Point(22, 34);
            this.打码工位允许上料.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.打码工位允许上料.Name = "打码工位允许上料";
            this.打码工位允许上料.Size = new System.Drawing.Size(128, 29);
            this.打码工位允许上料.TabIndex = 182;
            this.打码工位允许上料.Text = "打码工位允许上料";
            this.打码工位允许上料.CheckedChanged += new System.EventHandler(this.打码工位允许上料_CheckedChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.groupBox7);
            this.groupControl1.Controls.Add(this.groupBox6);
            this.groupControl1.Controls.Add(this.userLanternIO);
            this.groupControl1.Location = new System.Drawing.Point(908, 4);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(785, 831);
            this.groupControl1.TabIndex = 167;
            this.groupControl1.Text = "I/O 输入输出";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.label7);
            this.groupBox7.Controls.Add(this.label8);
            this.groupBox7.Controls.Add(this.LED_Out0);
            this.groupBox7.Controls.Add(this.LED_Out31);
            this.groupBox7.Controls.Add(this.LED_Out1);
            this.groupBox7.Controls.Add(this.LED_Out30);
            this.groupBox7.Controls.Add(this.LED_Out2);
            this.groupBox7.Controls.Add(this.LED_Out29);
            this.groupBox7.Controls.Add(this.LED_Out3);
            this.groupBox7.Controls.Add(this.LED_Out28);
            this.groupBox7.Controls.Add(this.LED_Out4);
            this.groupBox7.Controls.Add(this.LED_Out27);
            this.groupBox7.Controls.Add(this.LED_Out5);
            this.groupBox7.Controls.Add(this.LED_Out26);
            this.groupBox7.Controls.Add(this.LED_Out6);
            this.groupBox7.Controls.Add(this.LED_Out25);
            this.groupBox7.Controls.Add(this.LED_Out7);
            this.groupBox7.Controls.Add(this.LED_Out24);
            this.groupBox7.Controls.Add(this.LED_Out8);
            this.groupBox7.Controls.Add(this.LED_Out23);
            this.groupBox7.Controls.Add(this.LED_Out9);
            this.groupBox7.Controls.Add(this.LED_Out22);
            this.groupBox7.Controls.Add(this.LED_Out10);
            this.groupBox7.Controls.Add(this.LED_Out21);
            this.groupBox7.Controls.Add(this.LED_Out11);
            this.groupBox7.Controls.Add(this.LED_Out20);
            this.groupBox7.Controls.Add(this.LED_Out12);
            this.groupBox7.Controls.Add(this.LED_Out19);
            this.groupBox7.Controls.Add(this.LED_Out13);
            this.groupBox7.Controls.Add(this.LED_Out18);
            this.groupBox7.Controls.Add(this.LED_Out14);
            this.groupBox7.Controls.Add(this.LED_Out17);
            this.groupBox7.Controls.Add(this.LED_Out15);
            this.groupBox7.Controls.Add(this.LED_Out16);
            this.groupBox7.Location = new System.Drawing.Point(11, 325);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Size = new System.Drawing.Size(769, 499);
            this.groupBox7.TabIndex = 166;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " I/O 输出(Output)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(606, 443);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 173;
            this.label5.Tag = "0";
            this.label5.Text = "Out24 ~ Out31";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(415, 443);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 172;
            this.label6.Tag = "0";
            this.label6.Text = "Out16 ~ Out23";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(219, 443);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 17);
            this.label7.TabIndex = 171;
            this.label7.Tag = "0";
            this.label7.Text = "Out8 ~ Out15";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 443);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 17);
            this.label8.TabIndex = 170;
            this.label8.Tag = "0";
            this.label8.Text = "Out0 ~ Out7";
            // 
            // LED_Out0
            // 
            this.LED_Out0.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out0.Location = new System.Drawing.Point(15, 24);
            this.LED_Out0.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out0.Name = "LED_Out0";
            this.LED_Out0.Size = new System.Drawing.Size(155, 39);
            this.LED_Out0.TabIndex = 132;
            this.LED_Out0.Text = "0 待料阻档气缸电磁阀";
            this.LED_Out0.UseVisualStyleBackColor = true;
            this.LED_Out0.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out31
            // 
            this.LED_Out31.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out31.Location = new System.Drawing.Point(583, 364);
            this.LED_Out31.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out31.Name = "LED_Out31";
            this.LED_Out31.Size = new System.Drawing.Size(135, 39);
            this.LED_Out31.TabIndex = 163;
            this.LED_Out31.Text = "31 空";
            this.LED_Out31.UseVisualStyleBackColor = true;
            this.LED_Out31.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out1
            // 
            this.LED_Out1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out1.Location = new System.Drawing.Point(15, 73);
            this.LED_Out1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out1.Name = "LED_Out1";
            this.LED_Out1.Size = new System.Drawing.Size(155, 39);
            this.LED_Out1.TabIndex = 133;
            this.LED_Out1.Text = "1 打码阻档气缸电磁阀";
            this.LED_Out1.UseVisualStyleBackColor = true;
            this.LED_Out1.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out30
            // 
            this.LED_Out30.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out30.Location = new System.Drawing.Point(583, 316);
            this.LED_Out30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out30.Name = "LED_Out30";
            this.LED_Out30.Size = new System.Drawing.Size(135, 39);
            this.LED_Out30.TabIndex = 162;
            this.LED_Out30.Text = "30 空";
            this.LED_Out30.UseVisualStyleBackColor = true;
            this.LED_Out30.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out2
            // 
            this.LED_Out2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out2.Location = new System.Drawing.Point(15, 121);
            this.LED_Out2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out2.Name = "LED_Out2";
            this.LED_Out2.Size = new System.Drawing.Size(155, 39);
            this.LED_Out2.TabIndex = 134;
            this.LED_Out2.Text = "2 打码顶升气缸电磁阀";
            this.LED_Out2.UseVisualStyleBackColor = true;
            this.LED_Out2.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out29
            // 
            this.LED_Out29.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out29.Location = new System.Drawing.Point(583, 267);
            this.LED_Out29.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out29.Name = "LED_Out29";
            this.LED_Out29.Size = new System.Drawing.Size(135, 39);
            this.LED_Out29.TabIndex = 161;
            this.LED_Out29.Text = "29 空";
            this.LED_Out29.UseVisualStyleBackColor = true;
            this.LED_Out29.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out3
            // 
            this.LED_Out3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out3.Location = new System.Drawing.Point(15, 170);
            this.LED_Out3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out3.Name = "LED_Out3";
            this.LED_Out3.Size = new System.Drawing.Size(155, 39);
            this.LED_Out3.TabIndex = 135;
            this.LED_Out3.Text = "3 翻转阻挡气缸电磁阀";
            this.LED_Out3.UseVisualStyleBackColor = true;
            this.LED_Out3.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out28
            // 
            this.LED_Out28.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out28.Location = new System.Drawing.Point(583, 219);
            this.LED_Out28.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out28.Name = "LED_Out28";
            this.LED_Out28.Size = new System.Drawing.Size(135, 39);
            this.LED_Out28.TabIndex = 160;
            this.LED_Out28.Text = "28 空";
            this.LED_Out28.UseVisualStyleBackColor = true;
            this.LED_Out28.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out4
            // 
            this.LED_Out4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out4.Location = new System.Drawing.Point(15, 219);
            this.LED_Out4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out4.Name = "LED_Out4";
            this.LED_Out4.Size = new System.Drawing.Size(155, 39);
            this.LED_Out4.TabIndex = 136;
            this.LED_Out4.Text = "4 翻转顶升气缸电磁阀原位";
            this.LED_Out4.UseVisualStyleBackColor = true;
            this.LED_Out4.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out27
            // 
            this.LED_Out27.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out27.Location = new System.Drawing.Point(583, 170);
            this.LED_Out27.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out27.Name = "LED_Out27";
            this.LED_Out27.Size = new System.Drawing.Size(135, 39);
            this.LED_Out27.TabIndex = 159;
            this.LED_Out27.Text = "27 空";
            this.LED_Out27.UseVisualStyleBackColor = true;
            this.LED_Out27.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out5
            // 
            this.LED_Out5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out5.Location = new System.Drawing.Point(15, 267);
            this.LED_Out5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out5.Name = "LED_Out5";
            this.LED_Out5.Size = new System.Drawing.Size(155, 39);
            this.LED_Out5.TabIndex = 137;
            this.LED_Out5.Text = "5 翻转顶升气缸电磁阀";
            this.LED_Out5.UseVisualStyleBackColor = true;
            this.LED_Out5.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out26
            // 
            this.LED_Out26.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out26.Location = new System.Drawing.Point(583, 121);
            this.LED_Out26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out26.Name = "LED_Out26";
            this.LED_Out26.Size = new System.Drawing.Size(135, 39);
            this.LED_Out26.TabIndex = 158;
            this.LED_Out26.Text = "26 空";
            this.LED_Out26.UseVisualStyleBackColor = true;
            this.LED_Out26.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out6
            // 
            this.LED_Out6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out6.Location = new System.Drawing.Point(15, 316);
            this.LED_Out6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out6.Name = "LED_Out6";
            this.LED_Out6.Size = new System.Drawing.Size(155, 39);
            this.LED_Out6.TabIndex = 138;
            this.LED_Out6.Text = "6 翻转旋转气缸电磁阀原位";
            this.LED_Out6.UseVisualStyleBackColor = true;
            this.LED_Out6.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out25
            // 
            this.LED_Out25.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out25.Location = new System.Drawing.Point(583, 73);
            this.LED_Out25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out25.Name = "LED_Out25";
            this.LED_Out25.Size = new System.Drawing.Size(135, 39);
            this.LED_Out25.TabIndex = 157;
            this.LED_Out25.Text = "25 空";
            this.LED_Out25.UseVisualStyleBackColor = true;
            this.LED_Out25.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out7
            // 
            this.LED_Out7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out7.Location = new System.Drawing.Point(15, 364);
            this.LED_Out7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out7.Name = "LED_Out7";
            this.LED_Out7.Size = new System.Drawing.Size(155, 39);
            this.LED_Out7.TabIndex = 139;
            this.LED_Out7.Text = "7 翻转旋转气缸电磁阀";
            this.LED_Out7.UseVisualStyleBackColor = true;
            this.LED_Out7.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out24
            // 
            this.LED_Out24.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out24.Location = new System.Drawing.Point(583, 24);
            this.LED_Out24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out24.Name = "LED_Out24";
            this.LED_Out24.Size = new System.Drawing.Size(135, 39);
            this.LED_Out24.TabIndex = 156;
            this.LED_Out24.Text = "24 空";
            this.LED_Out24.UseVisualStyleBackColor = true;
            this.LED_Out24.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out8
            // 
            this.LED_Out8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out8.Location = new System.Drawing.Point(189, 27);
            this.LED_Out8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out8.Name = "LED_Out8";
            this.LED_Out8.Size = new System.Drawing.Size(156, 39);
            this.LED_Out8.TabIndex = 140;
            this.LED_Out8.Text = "8 翻转夹紧气缸电磁阀原位";
            this.LED_Out8.UseVisualStyleBackColor = true;
            this.LED_Out8.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out23
            // 
            this.LED_Out23.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out23.Location = new System.Drawing.Point(376, 367);
            this.LED_Out23.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out23.Name = "LED_Out23";
            this.LED_Out23.Size = new System.Drawing.Size(166, 39);
            this.LED_Out23.TabIndex = 155;
            this.LED_Out23.Text = "23 空";
            this.LED_Out23.UseVisualStyleBackColor = true;
            this.LED_Out23.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out9
            // 
            this.LED_Out9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out9.Location = new System.Drawing.Point(189, 75);
            this.LED_Out9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out9.Name = "LED_Out9";
            this.LED_Out9.Size = new System.Drawing.Size(156, 39);
            this.LED_Out9.TabIndex = 141;
            this.LED_Out9.Text = "9 翻转夹紧气缸电磁阀";
            this.LED_Out9.UseVisualStyleBackColor = true;
            this.LED_Out9.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out22
            // 
            this.LED_Out22.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out22.Location = new System.Drawing.Point(376, 318);
            this.LED_Out22.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out22.Name = "LED_Out22";
            this.LED_Out22.Size = new System.Drawing.Size(166, 39);
            this.LED_Out22.TabIndex = 154;
            this.LED_Out22.Text = "22 照明灯";
            this.LED_Out22.UseVisualStyleBackColor = true;
            this.LED_Out22.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out10
            // 
            this.LED_Out10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out10.Location = new System.Drawing.Point(189, 124);
            this.LED_Out10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out10.Name = "LED_Out10";
            this.LED_Out10.Size = new System.Drawing.Size(156, 39);
            this.LED_Out10.TabIndex = 142;
            this.LED_Out10.Text = "10 切割阻挡气缸电磁阀";
            this.LED_Out10.UseVisualStyleBackColor = true;
            this.LED_Out10.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out21
            // 
            this.LED_Out21.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out21.Location = new System.Drawing.Point(376, 270);
            this.LED_Out21.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out21.Name = "LED_Out21";
            this.LED_Out21.Size = new System.Drawing.Size(166, 39);
            this.LED_Out21.TabIndex = 153;
            this.LED_Out21.Text = "21 停止按扭(灯)";
            this.LED_Out21.UseVisualStyleBackColor = true;
            this.LED_Out21.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out11
            // 
            this.LED_Out11.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out11.Location = new System.Drawing.Point(189, 172);
            this.LED_Out11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out11.Name = "LED_Out11";
            this.LED_Out11.Size = new System.Drawing.Size(156, 39);
            this.LED_Out11.TabIndex = 143;
            this.LED_Out11.Text = "11 切割顶升气缸电磁阀";
            this.LED_Out11.UseVisualStyleBackColor = true;
            this.LED_Out11.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out20
            // 
            this.LED_Out20.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out20.Location = new System.Drawing.Point(376, 221);
            this.LED_Out20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out20.Name = "LED_Out20";
            this.LED_Out20.Size = new System.Drawing.Size(166, 39);
            this.LED_Out20.TabIndex = 152;
            this.LED_Out20.Text = "20 复位按扭(灯)";
            this.LED_Out20.UseVisualStyleBackColor = true;
            this.LED_Out20.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out12
            // 
            this.LED_Out12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out12.Location = new System.Drawing.Point(189, 221);
            this.LED_Out12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out12.Name = "LED_Out12";
            this.LED_Out12.Size = new System.Drawing.Size(156, 39);
            this.LED_Out12.TabIndex = 144;
            this.LED_Out12.Text = "12 蜂鸣器";
            this.LED_Out12.UseVisualStyleBackColor = true;
            this.LED_Out12.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out19
            // 
            this.LED_Out19.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out19.Location = new System.Drawing.Point(376, 172);
            this.LED_Out19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out19.Name = "LED_Out19";
            this.LED_Out19.Size = new System.Drawing.Size(166, 39);
            this.LED_Out19.TabIndex = 151;
            this.LED_Out19.Text = "19 启动按扭(灯)";
            this.LED_Out19.UseVisualStyleBackColor = true;
            this.LED_Out19.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out13
            // 
            this.LED_Out13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out13.Location = new System.Drawing.Point(189, 270);
            this.LED_Out13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out13.Name = "LED_Out13";
            this.LED_Out13.Size = new System.Drawing.Size(156, 39);
            this.LED_Out13.TabIndex = 145;
            this.LED_Out13.Text = "13 黄灯";
            this.LED_Out13.UseVisualStyleBackColor = true;
            this.LED_Out13.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out18
            // 
            this.LED_Out18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out18.Location = new System.Drawing.Point(376, 124);
            this.LED_Out18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out18.Name = "LED_Out18";
            this.LED_Out18.Size = new System.Drawing.Size(166, 39);
            this.LED_Out18.TabIndex = 150;
            this.LED_Out18.Text = "18 回流皮带线电机正转控制";
            this.LED_Out18.UseVisualStyleBackColor = true;
            this.LED_Out18.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out14
            // 
            this.LED_Out14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out14.Location = new System.Drawing.Point(189, 318);
            this.LED_Out14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out14.Name = "LED_Out14";
            this.LED_Out14.Size = new System.Drawing.Size(156, 39);
            this.LED_Out14.TabIndex = 146;
            this.LED_Out14.Text = "14 绿灯";
            this.LED_Out14.UseVisualStyleBackColor = true;
            this.LED_Out14.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out17
            // 
            this.LED_Out17.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out17.Location = new System.Drawing.Point(376, 75);
            this.LED_Out17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out17.Name = "LED_Out17";
            this.LED_Out17.Size = new System.Drawing.Size(166, 39);
            this.LED_Out17.TabIndex = 149;
            this.LED_Out17.Text = "17 切割皮带线电机正转控制";
            this.LED_Out17.UseVisualStyleBackColor = true;
            this.LED_Out17.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out15
            // 
            this.LED_Out15.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out15.Location = new System.Drawing.Point(189, 367);
            this.LED_Out15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out15.Name = "LED_Out15";
            this.LED_Out15.Size = new System.Drawing.Size(156, 39);
            this.LED_Out15.TabIndex = 147;
            this.LED_Out15.Text = "15 红灯";
            this.LED_Out15.UseVisualStyleBackColor = true;
            this.LED_Out15.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // LED_Out16
            // 
            this.LED_Out16.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LED_Out16.Location = new System.Drawing.Point(376, 27);
            this.LED_Out16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Out16.Name = "LED_Out16";
            this.LED_Out16.Size = new System.Drawing.Size(166, 39);
            this.LED_Out16.TabIndex = 148;
            this.LED_Out16.Text = "16 上料皮带线电机正转控制";
            this.LED_Out16.UseVisualStyleBackColor = true;
            this.LED_Out16.CheckedChanged += new System.EventHandler(this.Output_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.LED_In0);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label48);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label49);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label50);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label51);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.label52);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.LED_In1);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.LED_In2);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.LED_In3);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.LED_In4);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this.LED_In5);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Controls.Add(this.LED_In6);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this.LED_In7);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this.LED_In9);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.LED_In10);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.LED_In11);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.LED_In12);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.LED_In13);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.LED_In14);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.LED_In15);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.LED_In8);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.LED_In17);
            this.groupBox6.Controls.Add(this.LED_In24);
            this.groupBox6.Controls.Add(this.LED_In18);
            this.groupBox6.Controls.Add(this.LED_In31);
            this.groupBox6.Controls.Add(this.LED_In19);
            this.groupBox6.Controls.Add(this.LED_In30);
            this.groupBox6.Controls.Add(this.LED_In20);
            this.groupBox6.Controls.Add(this.LED_In29);
            this.groupBox6.Controls.Add(this.LED_In21);
            this.groupBox6.Controls.Add(this.LED_In28);
            this.groupBox6.Controls.Add(this.LED_In22);
            this.groupBox6.Controls.Add(this.LED_In27);
            this.groupBox6.Controls.Add(this.LED_In23);
            this.groupBox6.Controls.Add(this.LED_In26);
            this.groupBox6.Controls.Add(this.LED_In16);
            this.groupBox6.Controls.Add(this.LED_In25);
            this.groupBox6.Location = new System.Drawing.Point(11, 30);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(769, 288);
            this.groupBox6.TabIndex = 165;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "I/O 输入(Input)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(620, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 17);
            this.label4.TabIndex = 169;
            this.label4.Tag = "0";
            this.label4.Text = "In24 ~ In31";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(415, 257);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 17);
            this.label3.TabIndex = 168;
            this.label3.Tag = "0";
            this.label3.Text = "In16 ~ In23";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 257);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 167;
            this.label2.Tag = "0";
            this.label2.Text = "In8 ~ In15";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 257);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 166;
            this.label1.Tag = "0";
            this.label1.Text = "In0 ~ In7";
            // 
            // LED_In0
            // 
            this.LED_In0.BackColor = System.Drawing.Color.Transparent;
            this.LED_In0.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In0.Location = new System.Drawing.Point(15, 27);
            this.LED_In0.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In0.Name = "LED_In0";
            this.LED_In0.Size = new System.Drawing.Size(22, 28);
            this.LED_In0.TabIndex = 82;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(607, 227);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(38, 17);
            this.label45.TabIndex = 130;
            this.label45.Text = "31 空";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(39, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(127, 17);
            this.label11.TabIndex = 42;
            this.label11.Text = "1 待料阻挡气缸下感应";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(607, 199);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(38, 17);
            this.label46.TabIndex = 129;
            this.label46.Text = "30 空";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(39, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 17);
            this.label12.TabIndex = 43;
            this.label12.Text = "2 待料阻挡气缸上感应";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(607, 171);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(38, 17);
            this.label47.TabIndex = 128;
            this.label47.Text = "29 空";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(39, 113);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(127, 17);
            this.label14.TabIndex = 44;
            this.label14.Text = "3 打码位载板到位检测";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(607, 143);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(38, 17);
            this.label48.TabIndex = 127;
            this.label48.Text = "28 空";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 141);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 17);
            this.label13.TabIndex = 45;
            this.label13.Text = "4 打码阻挡气缸下感应";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(607, 115);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(50, 17);
            this.label49.TabIndex = 126;
            this.label49.Text = "27 急停";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(39, 169);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 17);
            this.label16.TabIndex = 46;
            this.label16.Text = "5 打码阻挡气缸上感应";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(607, 87);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(62, 17);
            this.label50.TabIndex = 125;
            this.label50.Text = "26 门开关";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(39, 197);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 17);
            this.label15.TabIndex = 47;
            this.label15.Text = "6 打码顶升气缸下感应";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(607, 60);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(50, 17);
            this.label51.TabIndex = 124;
            this.label51.Text = "25 复位";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(39, 225);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(127, 17);
            this.label18.TabIndex = 48;
            this.label18.Text = "7 打码顶升气缸上感应";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(607, 32);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(79, 17);
            this.label52.TabIndex = 123;
            this.label52.Text = "24 暂停/停止";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(39, 29);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(115, 17);
            this.label27.TabIndex = 59;
            this.label27.Tag = "0";
            this.label27.Text = "0 上料待料载板有无";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(399, 227);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(50, 17);
            this.label37.TabIndex = 122;
            this.label37.Text = "23 启动";
            // 
            // LED_In1
            // 
            this.LED_In1.BackColor = System.Drawing.Color.Transparent;
            this.LED_In1.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In1.Location = new System.Drawing.Point(15, 56);
            this.LED_In1.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In1.Name = "LED_In1";
            this.LED_In1.Size = new System.Drawing.Size(22, 28);
            this.LED_In1.TabIndex = 75;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(399, 199);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(134, 17);
            this.label38.TabIndex = 121;
            this.label38.Text = "22 切割顶升气缸上感应";
            // 
            // LED_In2
            // 
            this.LED_In2.BackColor = System.Drawing.Color.Transparent;
            this.LED_In2.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In2.Location = new System.Drawing.Point(15, 83);
            this.LED_In2.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In2.Name = "LED_In2";
            this.LED_In2.Size = new System.Drawing.Size(22, 28);
            this.LED_In2.TabIndex = 76;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(399, 171);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(134, 17);
            this.label39.TabIndex = 120;
            this.label39.Text = "21 切割顶升气缸下感应";
            // 
            // LED_In3
            // 
            this.LED_In3.BackColor = System.Drawing.Color.Transparent;
            this.LED_In3.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In3.Location = new System.Drawing.Point(15, 111);
            this.LED_In3.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In3.Name = "LED_In3";
            this.LED_In3.Size = new System.Drawing.Size(22, 28);
            this.LED_In3.TabIndex = 77;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(399, 143);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(134, 17);
            this.label40.TabIndex = 119;
            this.label40.Text = "20 切割阻挡气缸上感应";
            // 
            // LED_In4
            // 
            this.LED_In4.BackColor = System.Drawing.Color.Transparent;
            this.LED_In4.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In4.Location = new System.Drawing.Point(15, 138);
            this.LED_In4.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In4.Name = "LED_In4";
            this.LED_In4.Size = new System.Drawing.Size(22, 28);
            this.LED_In4.TabIndex = 78;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(399, 115);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(134, 17);
            this.label41.TabIndex = 118;
            this.label41.Text = "19 切割阻挡气缸下感应";
            // 
            // LED_In5
            // 
            this.LED_In5.BackColor = System.Drawing.Color.Transparent;
            this.LED_In5.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In5.Location = new System.Drawing.Point(15, 165);
            this.LED_In5.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In5.Name = "LED_In5";
            this.LED_In5.Size = new System.Drawing.Size(22, 28);
            this.LED_In5.TabIndex = 79;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(399, 87);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(161, 17);
            this.label42.TabIndex = 117;
            this.label42.Text = "18 XY平台皮带载板到位感应";
            // 
            // LED_In6
            // 
            this.LED_In6.BackColor = System.Drawing.Color.Transparent;
            this.LED_In6.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In6.Location = new System.Drawing.Point(15, 194);
            this.LED_In6.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In6.Name = "LED_In6";
            this.LED_In6.Size = new System.Drawing.Size(22, 28);
            this.LED_In6.TabIndex = 80;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(399, 60);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(161, 17);
            this.label43.TabIndex = 116;
            this.label43.Text = "17 XY平台皮带载板进料感应";
            // 
            // LED_In7
            // 
            this.LED_In7.BackColor = System.Drawing.Color.Transparent;
            this.LED_In7.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In7.Location = new System.Drawing.Point(15, 222);
            this.LED_In7.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In7.Name = "LED_In7";
            this.LED_In7.Size = new System.Drawing.Size(22, 28);
            this.LED_In7.TabIndex = 81;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(399, 32);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(146, 17);
            this.label44.TabIndex = 115;
            this.label44.Text = "16 翻转工位夹紧关闭感应";
            // 
            // LED_In9
            // 
            this.LED_In9.BackColor = System.Drawing.Color.Transparent;
            this.LED_In9.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In9.Location = new System.Drawing.Point(182, 58);
            this.LED_In9.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In9.Name = "LED_In9";
            this.LED_In9.Size = new System.Drawing.Size(22, 28);
            this.LED_In9.TabIndex = 83;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(205, 227);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(146, 17);
            this.label26.TabIndex = 114;
            this.label26.Text = "15 翻转工位夹紧打开感应";
            // 
            // LED_In10
            // 
            this.LED_In10.BackColor = System.Drawing.Color.Transparent;
            this.LED_In10.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In10.Location = new System.Drawing.Point(182, 85);
            this.LED_In10.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In10.Name = "LED_In10";
            this.LED_In10.Size = new System.Drawing.Size(22, 28);
            this.LED_In10.TabIndex = 84;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(205, 199);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(122, 17);
            this.label30.TabIndex = 113;
            this.label30.Text = "14 翻转工位旋转感应";
            // 
            // LED_In11
            // 
            this.LED_In11.BackColor = System.Drawing.Color.Transparent;
            this.LED_In11.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In11.Location = new System.Drawing.Point(182, 113);
            this.LED_In11.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In11.Name = "LED_In11";
            this.LED_In11.Size = new System.Drawing.Size(22, 28);
            this.LED_In11.TabIndex = 85;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(205, 171);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(146, 17);
            this.label31.TabIndex = 112;
            this.label31.Text = "13 翻转工位旋转原点感应";
            // 
            // LED_In12
            // 
            this.LED_In12.BackColor = System.Drawing.Color.Transparent;
            this.LED_In12.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In12.Location = new System.Drawing.Point(182, 141);
            this.LED_In12.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In12.Name = "LED_In12";
            this.LED_In12.Size = new System.Drawing.Size(22, 28);
            this.LED_In12.TabIndex = 86;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(205, 143);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(110, 17);
            this.label32.TabIndex = 111;
            this.label32.Text = "12 翻转工位下感应";
            // 
            // LED_In13
            // 
            this.LED_In13.BackColor = System.Drawing.Color.Transparent;
            this.LED_In13.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In13.Location = new System.Drawing.Point(182, 168);
            this.LED_In13.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In13.Name = "LED_In13";
            this.LED_In13.Size = new System.Drawing.Size(22, 28);
            this.LED_In13.TabIndex = 87;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(205, 115);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(110, 17);
            this.label33.TabIndex = 110;
            this.label33.Text = "11 翻转工位上感应";
            // 
            // LED_In14
            // 
            this.LED_In14.BackColor = System.Drawing.Color.Transparent;
            this.LED_In14.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In14.Location = new System.Drawing.Point(182, 197);
            this.LED_In14.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In14.Name = "LED_In14";
            this.LED_In14.Size = new System.Drawing.Size(22, 28);
            this.LED_In14.TabIndex = 88;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(205, 87);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(134, 17);
            this.label34.TabIndex = 109;
            this.label34.Text = "10 翻转阻挡气缸上感应";
            // 
            // LED_In15
            // 
            this.LED_In15.BackColor = System.Drawing.Color.Transparent;
            this.LED_In15.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In15.Location = new System.Drawing.Point(182, 225);
            this.LED_In15.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In15.Name = "LED_In15";
            this.LED_In15.Size = new System.Drawing.Size(22, 28);
            this.LED_In15.TabIndex = 89;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(205, 60);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(127, 17);
            this.label35.TabIndex = 108;
            this.label35.Text = "9 翻转阻挡气缸下感应";
            // 
            // LED_In8
            // 
            this.LED_In8.BackColor = System.Drawing.Color.Transparent;
            this.LED_In8.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In8.Location = new System.Drawing.Point(182, 29);
            this.LED_In8.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In8.Name = "LED_In8";
            this.LED_In8.Size = new System.Drawing.Size(22, 28);
            this.LED_In8.TabIndex = 90;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(205, 32);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(139, 17);
            this.label36.TabIndex = 107;
            this.label36.Text = "8 翻转工位载板有无检测";
            // 
            // LED_In17
            // 
            this.LED_In17.BackColor = System.Drawing.Color.Transparent;
            this.LED_In17.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In17.Location = new System.Drawing.Point(376, 58);
            this.LED_In17.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In17.Name = "LED_In17";
            this.LED_In17.Size = new System.Drawing.Size(22, 28);
            this.LED_In17.TabIndex = 91;
            // 
            // LED_In24
            // 
            this.LED_In24.BackColor = System.Drawing.Color.Transparent;
            this.LED_In24.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In24.Location = new System.Drawing.Point(583, 29);
            this.LED_In24.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In24.Name = "LED_In24";
            this.LED_In24.Size = new System.Drawing.Size(22, 28);
            this.LED_In24.TabIndex = 106;
            // 
            // LED_In18
            // 
            this.LED_In18.BackColor = System.Drawing.Color.Transparent;
            this.LED_In18.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In18.Location = new System.Drawing.Point(376, 85);
            this.LED_In18.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In18.Name = "LED_In18";
            this.LED_In18.Size = new System.Drawing.Size(22, 28);
            this.LED_In18.TabIndex = 92;
            // 
            // LED_In31
            // 
            this.LED_In31.BackColor = System.Drawing.Color.Transparent;
            this.LED_In31.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In31.Location = new System.Drawing.Point(583, 225);
            this.LED_In31.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In31.Name = "LED_In31";
            this.LED_In31.Size = new System.Drawing.Size(22, 28);
            this.LED_In31.TabIndex = 105;
            // 
            // LED_In19
            // 
            this.LED_In19.BackColor = System.Drawing.Color.Transparent;
            this.LED_In19.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In19.Location = new System.Drawing.Point(376, 113);
            this.LED_In19.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In19.Name = "LED_In19";
            this.LED_In19.Size = new System.Drawing.Size(22, 28);
            this.LED_In19.TabIndex = 93;
            // 
            // LED_In30
            // 
            this.LED_In30.BackColor = System.Drawing.Color.Transparent;
            this.LED_In30.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In30.Location = new System.Drawing.Point(583, 197);
            this.LED_In30.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In30.Name = "LED_In30";
            this.LED_In30.Size = new System.Drawing.Size(22, 28);
            this.LED_In30.TabIndex = 104;
            // 
            // LED_In20
            // 
            this.LED_In20.BackColor = System.Drawing.Color.Transparent;
            this.LED_In20.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In20.Location = new System.Drawing.Point(376, 141);
            this.LED_In20.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In20.Name = "LED_In20";
            this.LED_In20.Size = new System.Drawing.Size(22, 28);
            this.LED_In20.TabIndex = 94;
            // 
            // LED_In29
            // 
            this.LED_In29.BackColor = System.Drawing.Color.Transparent;
            this.LED_In29.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In29.Location = new System.Drawing.Point(583, 168);
            this.LED_In29.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In29.Name = "LED_In29";
            this.LED_In29.Size = new System.Drawing.Size(22, 28);
            this.LED_In29.TabIndex = 103;
            // 
            // LED_In21
            // 
            this.LED_In21.BackColor = System.Drawing.Color.Transparent;
            this.LED_In21.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In21.Location = new System.Drawing.Point(376, 168);
            this.LED_In21.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In21.Name = "LED_In21";
            this.LED_In21.Size = new System.Drawing.Size(22, 28);
            this.LED_In21.TabIndex = 95;
            // 
            // LED_In28
            // 
            this.LED_In28.BackColor = System.Drawing.Color.Transparent;
            this.LED_In28.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In28.Location = new System.Drawing.Point(583, 141);
            this.LED_In28.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In28.Name = "LED_In28";
            this.LED_In28.Size = new System.Drawing.Size(22, 28);
            this.LED_In28.TabIndex = 102;
            // 
            // LED_In22
            // 
            this.LED_In22.BackColor = System.Drawing.Color.Transparent;
            this.LED_In22.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In22.Location = new System.Drawing.Point(376, 197);
            this.LED_In22.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In22.Name = "LED_In22";
            this.LED_In22.Size = new System.Drawing.Size(22, 28);
            this.LED_In22.TabIndex = 96;
            // 
            // LED_In27
            // 
            this.LED_In27.BackColor = System.Drawing.Color.Transparent;
            this.LED_In27.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In27.Location = new System.Drawing.Point(583, 113);
            this.LED_In27.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In27.Name = "LED_In27";
            this.LED_In27.Size = new System.Drawing.Size(22, 28);
            this.LED_In27.TabIndex = 101;
            // 
            // LED_In23
            // 
            this.LED_In23.BackColor = System.Drawing.Color.Transparent;
            this.LED_In23.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In23.Location = new System.Drawing.Point(376, 225);
            this.LED_In23.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In23.Name = "LED_In23";
            this.LED_In23.Size = new System.Drawing.Size(22, 28);
            this.LED_In23.TabIndex = 97;
            // 
            // LED_In26
            // 
            this.LED_In26.BackColor = System.Drawing.Color.Transparent;
            this.LED_In26.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In26.Location = new System.Drawing.Point(583, 85);
            this.LED_In26.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In26.Name = "LED_In26";
            this.LED_In26.Size = new System.Drawing.Size(22, 28);
            this.LED_In26.TabIndex = 100;
            // 
            // LED_In16
            // 
            this.LED_In16.BackColor = System.Drawing.Color.Transparent;
            this.LED_In16.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In16.Location = new System.Drawing.Point(376, 29);
            this.LED_In16.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_In16.Name = "LED_In16";
            this.LED_In16.Size = new System.Drawing.Size(22, 28);
            this.LED_In16.TabIndex = 98;
            // 
            // LED_In25
            // 
            this.LED_In25.BackColor = System.Drawing.Color.Transparent;
            this.LED_In25.LanternBackground = System.Drawing.Color.Gray;
            this.LED_In25.Location = new System.Drawing.Point(583, 58);
            this.LED_In25.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_In25.Name = "LED_In25";
            this.LED_In25.Size = new System.Drawing.Size(22, 28);
            this.LED_In25.TabIndex = 99;
            // 
            // userLanternIO
            // 
            this.userLanternIO.BackColor = System.Drawing.Color.Transparent;
            this.userLanternIO.LanternBackground = System.Drawing.Color.Gray;
            this.userLanternIO.Location = new System.Drawing.Point(89, 0);
            this.userLanternIO.Margin = new System.Windows.Forms.Padding(3, 40, 3, 40);
            this.userLanternIO.Name = "userLanternIO";
            this.userLanternIO.Size = new System.Drawing.Size(22, 27);
            this.userLanternIO.TabIndex = 165;
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl6.AppearanceCaption.Options.UseFont = true;
            this.groupControl6.Location = new System.Drawing.Point(426, 584);
            this.groupControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(296, 249);
            this.groupControl6.TabIndex = 100;
            this.groupControl6.Text = "切割要机操作";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.Y轴停止);
            this.groupControl2.Controls.Add(this.label55);
            this.groupControl2.Controls.Add(this.X轴停止);
            this.groupControl2.Controls.Add(this.label54);
            this.groupControl2.Controls.Add(this.Y轴报警清除);
            this.groupControl2.Controls.Add(this.X轴报警清除);
            this.groupControl2.Controls.Add(this.txtDst_Y);
            this.groupControl2.Controls.Add(this.txtDst_X);
            this.groupControl2.Controls.Add(this.Y轴启用禁用);
            this.groupControl2.Controls.Add(this.X轴启用禁用);
            this.groupControl2.Controls.Add(this.userLanternAXIS);
            this.groupControl2.Controls.Add(this.label9);
            this.groupControl2.Controls.Add(this.label24);
            this.groupControl2.Controls.Add(this.LED_ALARM_Y);
            this.groupControl2.Controls.Add(this.LED_INP_Y);
            this.groupControl2.Controls.Add(this.label22);
            this.groupControl2.Controls.Add(this.label23);
            this.groupControl2.Controls.Add(this.LED_ALARM_X);
            this.groupControl2.Controls.Add(this.LED_INP_X);
            this.groupControl2.Controls.Add(this.label19);
            this.groupControl2.Controls.Add(this.label20);
            this.groupControl2.Controls.Add(this.label21);
            this.groupControl2.Controls.Add(this.LED_ORG_Y);
            this.groupControl2.Controls.Add(this.LED_ELN_Y);
            this.groupControl2.Controls.Add(this.LED_ELP_Y);
            this.groupControl2.Controls.Add(this.Y轴反转);
            this.groupControl2.Controls.Add(this.Y轴正转);
            this.groupControl2.Controls.Add(this.lable11);
            this.groupControl2.Controls.Add(this.label10);
            this.groupControl2.Controls.Add(this.label17);
            this.groupControl2.Controls.Add(this.LED_ORG_X);
            this.groupControl2.Controls.Add(this.LED_ELN_X);
            this.groupControl2.Controls.Add(this.LED_ELP_X);
            this.groupControl2.Controls.Add(this.X轴反转);
            this.groupControl2.Controls.Add(this.X轴正转);
            this.groupControl2.Location = new System.Drawing.Point(3, 323);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(413, 510);
            this.groupControl2.TabIndex = 168;
            this.groupControl2.Text = "轴运动控制";
            // 
            // Y轴停止
            // 
            this.Y轴停止.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y轴停止.Appearance.Options.UseFont = true;
            this.Y轴停止.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴停止.AppearanceDisabled.Options.UseFont = true;
            this.Y轴停止.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴停止.AppearanceHovered.Options.UseFont = true;
            this.Y轴停止.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴停止.AppearancePressed.Options.UseFont = true;
            this.Y轴停止.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Y轴停止.ImageOptions.Image")));
            this.Y轴停止.Location = new System.Drawing.Point(17, 415);
            this.Y轴停止.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Y轴停止.Name = "Y轴停止";
            this.Y轴停止.Size = new System.Drawing.Size(189, 68);
            this.Y轴停止.TabIndex = 175;
            this.Y轴停止.Text = "Y轴 停止";
            this.Y轴停止.Click += new System.EventHandler(this.Y轴停止_Click);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(212, 435);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(87, 17);
            this.label55.TabIndex = 179;
            this.label55.Tag = "0";
            this.label55.Text = "运动距离(Puls)";
            // 
            // X轴停止
            // 
            this.X轴停止.Appearance.BackColor = System.Drawing.Color.Red;
            this.X轴停止.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X轴停止.Appearance.Options.UseBackColor = true;
            this.X轴停止.Appearance.Options.UseFont = true;
            this.X轴停止.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴停止.AppearanceDisabled.Options.UseFont = true;
            this.X轴停止.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴停止.AppearanceHovered.Options.UseFont = true;
            this.X轴停止.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴停止.AppearancePressed.Options.UseFont = true;
            this.X轴停止.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("X轴停止.ImageOptions.Image")));
            this.X轴停止.Location = new System.Drawing.Point(17, 174);
            this.X轴停止.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.X轴停止.Name = "X轴停止";
            this.X轴停止.Size = new System.Drawing.Size(189, 72);
            this.X轴停止.TabIndex = 174;
            this.X轴停止.Text = "X轴 停止";
            this.X轴停止.Click += new System.EventHandler(this.X轴停止_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(212, 197);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(87, 17);
            this.label54.TabIndex = 178;
            this.label54.Tag = "0";
            this.label54.Text = "运动距离(Puls)";
            // 
            // Y轴报警清除
            // 
            this.Y轴报警清除.Appearance.BackColor = System.Drawing.Color.Red;
            this.Y轴报警清除.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y轴报警清除.Appearance.Options.UseBackColor = true;
            this.Y轴报警清除.Appearance.Options.UseFont = true;
            this.Y轴报警清除.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴报警清除.AppearanceDisabled.Options.UseFont = true;
            this.Y轴报警清除.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴报警清除.AppearanceHovered.Options.UseFont = true;
            this.Y轴报警清除.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴报警清除.AppearancePressed.Options.UseFont = true;
            this.Y轴报警清除.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Y轴报警清除.ImageOptions.Image")));
            this.Y轴报警清除.Location = new System.Drawing.Point(303, 288);
            this.Y轴报警清除.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Y轴报警清除.Name = "Y轴报警清除";
            this.Y轴报警清除.Size = new System.Drawing.Size(90, 185);
            this.Y轴报警清除.TabIndex = 177;
            this.Y轴报警清除.Text = "报警清除";
            this.Y轴报警清除.Click += new System.EventHandler(this.Y轴报警清除_Click);
            // 
            // X轴报警清除
            // 
            this.X轴报警清除.Appearance.BackColor = System.Drawing.Color.Red;
            this.X轴报警清除.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X轴报警清除.Appearance.Options.UseBackColor = true;
            this.X轴报警清除.Appearance.Options.UseFont = true;
            this.X轴报警清除.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴报警清除.AppearanceDisabled.Options.UseFont = true;
            this.X轴报警清除.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴报警清除.AppearanceHovered.Options.UseFont = true;
            this.X轴报警清除.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴报警清除.AppearancePressed.Options.UseFont = true;
            this.X轴报警清除.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("X轴报警清除.ImageOptions.Image")));
            this.X轴报警清除.Location = new System.Drawing.Point(303, 50);
            this.X轴报警清除.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.X轴报警清除.Name = "X轴报警清除";
            this.X轴报警清除.Size = new System.Drawing.Size(90, 185);
            this.X轴报警清除.TabIndex = 176;
            this.X轴报警清除.Text = "报警清除";
            this.X轴报警清除.Click += new System.EventHandler(this.X轴报警清除_Click);
            // 
            // txtDst_Y
            // 
            this.txtDst_Y.EditValue = "2000";
            this.txtDst_Y.Location = new System.Drawing.Point(215, 459);
            this.txtDst_Y.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDst_Y.Name = "txtDst_Y";
            this.txtDst_Y.Size = new System.Drawing.Size(79, 20);
            this.txtDst_Y.TabIndex = 173;
            // 
            // txtDst_X
            // 
            this.txtDst_X.EditValue = "2000";
            this.txtDst_X.Location = new System.Drawing.Point(215, 221);
            this.txtDst_X.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDst_X.Name = "txtDst_X";
            this.txtDst_X.Size = new System.Drawing.Size(82, 20);
            this.txtDst_X.TabIndex = 172;
            // 
            // Y轴启用禁用
            // 
            this.Y轴启用禁用.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y轴启用禁用.Appearance.Options.UseFont = true;
            this.Y轴启用禁用.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴启用禁用.AppearanceDisabled.Options.UseFont = true;
            this.Y轴启用禁用.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴启用禁用.AppearanceHovered.Options.UseFont = true;
            this.Y轴启用禁用.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴启用禁用.AppearancePressed.Options.UseFont = true;
            this.Y轴启用禁用.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.Y轴启用禁用.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Y轴启用禁用.ImageOptions.Image")));
            this.Y轴启用禁用.Location = new System.Drawing.Point(17, 352);
            this.Y轴启用禁用.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Y轴启用禁用.Name = "Y轴启用禁用";
            this.Y轴启用禁用.Size = new System.Drawing.Size(189, 56);
            this.Y轴启用禁用.TabIndex = 171;
            this.Y轴启用禁用.Text = "Y轴 启用/禁用";
            this.Y轴启用禁用.CheckedChanged += new System.EventHandler(this.Y轴启用禁用_CheckedChanged);
            // 
            // X轴启用禁用
            // 
            this.X轴启用禁用.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X轴启用禁用.Appearance.Options.UseFont = true;
            this.X轴启用禁用.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴启用禁用.AppearanceDisabled.Options.UseFont = true;
            this.X轴启用禁用.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴启用禁用.AppearanceHovered.Options.UseFont = true;
            this.X轴启用禁用.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴启用禁用.AppearancePressed.Options.UseFont = true;
            this.X轴启用禁用.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.X轴启用禁用.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("X轴启用禁用.ImageOptions.Image")));
            this.X轴启用禁用.Location = new System.Drawing.Point(17, 111);
            this.X轴启用禁用.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.X轴启用禁用.Name = "X轴启用禁用";
            this.X轴启用禁用.Size = new System.Drawing.Size(189, 56);
            this.X轴启用禁用.TabIndex = 170;
            this.X轴启用禁用.Text = "X轴 启用/禁用";
            this.X轴启用禁用.CheckedChanged += new System.EventHandler(this.X轴启用禁用_CheckedChanged);
            // 
            // userLanternAXIS
            // 
            this.userLanternAXIS.BackColor = System.Drawing.Color.Transparent;
            this.userLanternAXIS.LanternBackground = System.Drawing.Color.Gray;
            this.userLanternAXIS.Location = new System.Drawing.Point(87, 0);
            this.userLanternAXIS.Margin = new System.Windows.Forms.Padding(3, 40, 3, 40);
            this.userLanternAXIS.Name = "userLanternAXIS";
            this.userLanternAXIS.Size = new System.Drawing.Size(22, 27);
            this.userLanternAXIS.TabIndex = 169;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(244, 401);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 17);
            this.label9.TabIndex = 147;
            this.label9.Text = "报警";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(244, 373);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 17);
            this.label24.TabIndex = 146;
            this.label24.Text = "到位INP";
            // 
            // LED_ALARM_Y
            // 
            this.LED_ALARM_Y.BackColor = System.Drawing.Color.Transparent;
            this.LED_ALARM_Y.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ALARM_Y.Location = new System.Drawing.Point(220, 398);
            this.LED_ALARM_Y.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_ALARM_Y.Name = "LED_ALARM_Y";
            this.LED_ALARM_Y.Size = new System.Drawing.Size(22, 28);
            this.LED_ALARM_Y.TabIndex = 145;
            // 
            // LED_INP_Y
            // 
            this.LED_INP_Y.BackColor = System.Drawing.Color.Transparent;
            this.LED_INP_Y.LanternBackground = System.Drawing.Color.Gray;
            this.LED_INP_Y.Location = new System.Drawing.Point(220, 372);
            this.LED_INP_Y.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_INP_Y.Name = "LED_INP_Y";
            this.LED_INP_Y.Size = new System.Drawing.Size(22, 28);
            this.LED_INP_Y.TabIndex = 144;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(244, 165);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 17);
            this.label22.TabIndex = 143;
            this.label22.Text = "报警";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(244, 137);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 17);
            this.label23.TabIndex = 142;
            this.label23.Text = "到位INP";
            // 
            // LED_ALARM_X
            // 
            this.LED_ALARM_X.BackColor = System.Drawing.Color.Transparent;
            this.LED_ALARM_X.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ALARM_X.Location = new System.Drawing.Point(220, 163);
            this.LED_ALARM_X.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_ALARM_X.Name = "LED_ALARM_X";
            this.LED_ALARM_X.Size = new System.Drawing.Size(22, 28);
            this.LED_ALARM_X.TabIndex = 141;
            // 
            // LED_INP_X
            // 
            this.LED_INP_X.BackColor = System.Drawing.Color.Transparent;
            this.LED_INP_X.LanternBackground = System.Drawing.Color.Gray;
            this.LED_INP_X.Location = new System.Drawing.Point(220, 136);
            this.LED_INP_X.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_INP_X.Name = "LED_INP_X";
            this.LED_INP_X.Size = new System.Drawing.Size(22, 28);
            this.LED_INP_X.TabIndex = 140;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(244, 346);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 17);
            this.label19.TabIndex = 139;
            this.label19.Text = "负限位";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(244, 318);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 17);
            this.label20.TabIndex = 138;
            this.label20.Text = "正限位";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(244, 290);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 17);
            this.label21.TabIndex = 137;
            this.label21.Text = "原点";
            // 
            // LED_ORG_Y
            // 
            this.LED_ORG_Y.BackColor = System.Drawing.Color.Transparent;
            this.LED_ORG_Y.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ORG_Y.Location = new System.Drawing.Point(220, 288);
            this.LED_ORG_Y.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_ORG_Y.Name = "LED_ORG_Y";
            this.LED_ORG_Y.Size = new System.Drawing.Size(22, 28);
            this.LED_ORG_Y.TabIndex = 136;
            // 
            // LED_ELN_Y
            // 
            this.LED_ELN_Y.BackColor = System.Drawing.Color.Transparent;
            this.LED_ELN_Y.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ELN_Y.Location = new System.Drawing.Point(220, 344);
            this.LED_ELN_Y.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_ELN_Y.Name = "LED_ELN_Y";
            this.LED_ELN_Y.Size = new System.Drawing.Size(22, 28);
            this.LED_ELN_Y.TabIndex = 135;
            // 
            // LED_ELP_Y
            // 
            this.LED_ELP_Y.BackColor = System.Drawing.Color.Transparent;
            this.LED_ELP_Y.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ELP_Y.Location = new System.Drawing.Point(220, 317);
            this.LED_ELP_Y.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_ELP_Y.Name = "LED_ELP_Y";
            this.LED_ELP_Y.Size = new System.Drawing.Size(22, 28);
            this.LED_ELP_Y.TabIndex = 134;
            // 
            // Y轴反转
            // 
            this.Y轴反转.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y轴反转.Appearance.Options.UseFont = true;
            this.Y轴反转.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴反转.AppearanceDisabled.Options.UseFont = true;
            this.Y轴反转.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴反转.AppearanceHovered.Options.UseFont = true;
            this.Y轴反转.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴反转.AppearancePressed.Options.UseFont = true;
            this.Y轴反转.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Y轴反转.ImageOptions.Image")));
            this.Y轴反转.Location = new System.Drawing.Point(115, 287);
            this.Y轴反转.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Y轴反转.Name = "Y轴反转";
            this.Y轴反转.Size = new System.Drawing.Size(91, 58);
            this.Y轴反转.TabIndex = 133;
            this.Y轴反转.Text = "Y轴反转";
            this.Y轴反转.Click += new System.EventHandler(this.Y轴反转_Click);
            // 
            // Y轴正转
            // 
            this.Y轴正转.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Y轴正转.Appearance.Options.UseFont = true;
            this.Y轴正转.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴正转.AppearanceDisabled.Options.UseFont = true;
            this.Y轴正转.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴正转.AppearanceHovered.Options.UseFont = true;
            this.Y轴正转.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Y轴正转.AppearancePressed.Options.UseFont = true;
            this.Y轴正转.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Y轴正转.ImageOptions.Image")));
            this.Y轴正转.Location = new System.Drawing.Point(17, 288);
            this.Y轴正转.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Y轴正转.Name = "Y轴正转";
            this.Y轴正转.Size = new System.Drawing.Size(92, 57);
            this.Y轴正转.TabIndex = 132;
            this.Y轴正转.Text = "Y轴正转";
            this.Y轴正转.Click += new System.EventHandler(this.Y轴正转_Click);
            // 
            // lable11
            // 
            this.lable11.AutoSize = true;
            this.lable11.Location = new System.Drawing.Point(244, 108);
            this.lable11.Name = "lable11";
            this.lable11.Size = new System.Drawing.Size(44, 17);
            this.lable11.TabIndex = 131;
            this.lable11.Text = "负限位";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(244, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 130;
            this.label10.Text = "正限位";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(244, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 17);
            this.label17.TabIndex = 129;
            this.label17.Text = "原点";
            // 
            // LED_ORG_X
            // 
            this.LED_ORG_X.BackColor = System.Drawing.Color.Transparent;
            this.LED_ORG_X.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ORG_X.Location = new System.Drawing.Point(220, 50);
            this.LED_ORG_X.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_ORG_X.Name = "LED_ORG_X";
            this.LED_ORG_X.Size = new System.Drawing.Size(22, 28);
            this.LED_ORG_X.TabIndex = 128;
            // 
            // LED_ELN_X
            // 
            this.LED_ELN_X.BackColor = System.Drawing.Color.Transparent;
            this.LED_ELN_X.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ELN_X.Location = new System.Drawing.Point(220, 106);
            this.LED_ELN_X.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.LED_ELN_X.Name = "LED_ELN_X";
            this.LED_ELN_X.Size = new System.Drawing.Size(22, 28);
            this.LED_ELN_X.TabIndex = 127;
            // 
            // LED_ELP_X
            // 
            this.LED_ELP_X.BackColor = System.Drawing.Color.Transparent;
            this.LED_ELP_X.LanternBackground = System.Drawing.Color.Gray;
            this.LED_ELP_X.Location = new System.Drawing.Point(220, 79);
            this.LED_ELP_X.Margin = new System.Windows.Forms.Padding(3, 19, 3, 19);
            this.LED_ELP_X.Name = "LED_ELP_X";
            this.LED_ELP_X.Size = new System.Drawing.Size(22, 28);
            this.LED_ELP_X.TabIndex = 126;
            // 
            // X轴反转
            // 
            this.X轴反转.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X轴反转.Appearance.Options.UseFont = true;
            this.X轴反转.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴反转.AppearanceDisabled.Options.UseFont = true;
            this.X轴反转.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴反转.AppearanceHovered.Options.UseFont = true;
            this.X轴反转.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴反转.AppearancePressed.Options.UseFont = true;
            this.X轴反转.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("X轴反转.ImageOptions.Image")));
            this.X轴反转.Location = new System.Drawing.Point(115, 50);
            this.X轴反转.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.X轴反转.Name = "X轴反转";
            this.X轴反转.Size = new System.Drawing.Size(91, 58);
            this.X轴反转.TabIndex = 1;
            this.X轴反转.Text = "X轴反转";
            this.X轴反转.Click += new System.EventHandler(this.X轴反转_Click);
            // 
            // X轴正转
            // 
            this.X轴正转.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X轴正转.Appearance.Options.UseFont = true;
            this.X轴正转.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴正转.AppearanceDisabled.Options.UseFont = true;
            this.X轴正转.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴正转.AppearanceHovered.Options.UseFont = true;
            this.X轴正转.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.X轴正转.AppearancePressed.Options.UseFont = true;
            this.X轴正转.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("X轴正转.ImageOptions.Image")));
            this.X轴正转.Location = new System.Drawing.Point(17, 50);
            this.X轴正转.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.X轴正转.Name = "X轴正转";
            this.X轴正转.Size = new System.Drawing.Size(92, 58);
            this.X轴正转.TabIndex = 0;
            this.X轴正转.Text = "X轴正转";
            this.X轴正转.Click += new System.EventHandler(this.X轴正转_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Location = new System.Drawing.Point(426, 327);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(296, 238);
            this.groupControl4.TabIndex = 99;
            this.groupControl4.Text = "打码机操作";
            // 
            // groupControl5
            // 
            this.groupControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.Appearance.Options.UseFont = true;
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.Controls.Add(this.翻转工位联动);
            this.groupControl5.Controls.Add(this.回流皮带控制);
            this.groupControl5.Controls.Add(this.切割皮带控制);
            this.groupControl5.Controls.Add(this.上料皮带控制);
            this.groupControl5.Controls.Add(this.切割气缸顶升);
            this.groupControl5.Controls.Add(this.打码气缸顶升);
            this.groupControl5.Controls.Add(this.切割气缸阻挡);
            this.groupControl5.Controls.Add(this.翻转工位夹紧);
            this.groupControl5.Controls.Add(this.翻转工位旋转);
            this.groupControl5.Controls.Add(this.翻转气缸顶升);
            this.groupControl5.Controls.Add(this.翻转气缸阻挡);
            this.groupControl5.Controls.Add(this.打码气缸阻挡);
            this.groupControl5.Controls.Add(this.待料气缸阻挡);
            this.groupControl5.Location = new System.Drawing.Point(3, 4);
            this.groupControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(719, 312);
            this.groupControl5.TabIndex = 98;
            this.groupControl5.Text = "设备操作";
            this.groupControl5.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl5_Paint);
            // 
            // 翻转工位联动
            // 
            this.翻转工位联动.Appearance.BackColor = System.Drawing.Color.Red;
            this.翻转工位联动.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.翻转工位联动.Appearance.Options.UseBackColor = true;
            this.翻转工位联动.Appearance.Options.UseFont = true;
            this.翻转工位联动.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位联动.AppearanceDisabled.Options.UseFont = true;
            this.翻转工位联动.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位联动.AppearanceHovered.Options.UseFont = true;
            this.翻转工位联动.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位联动.AppearancePressed.Options.UseFont = true;
            this.翻转工位联动.Location = new System.Drawing.Point(559, 171);
            this.翻转工位联动.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.翻转工位联动.Name = "翻转工位联动";
            this.翻转工位联动.Size = new System.Drawing.Size(128, 58);
            this.翻转工位联动.TabIndex = 189;
            this.翻转工位联动.Text = "翻转工位联动";
            this.翻转工位联动.Click += new System.EventHandler(this.翻转工位联动_Click);
            // 
            // 回流皮带控制
            // 
            this.回流皮带控制.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.回流皮带控制.Appearance.Options.UseFont = true;
            this.回流皮带控制.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.回流皮带控制.AppearanceDisabled.Options.UseFont = true;
            this.回流皮带控制.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.回流皮带控制.AppearanceHovered.Options.UseFont = true;
            this.回流皮带控制.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.回流皮带控制.AppearancePressed.Options.UseFont = true;
            this.回流皮带控制.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.回流皮带控制.Location = new System.Drawing.Point(11, 170);
            this.回流皮带控制.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.回流皮带控制.Name = "回流皮带控制";
            this.回流皮带控制.Size = new System.Drawing.Size(128, 56);
            this.回流皮带控制.TabIndex = 187;
            this.回流皮带控制.Text = "回流皮带转动/停止";
            this.回流皮带控制.CheckedChanged += new System.EventHandler(this.回流皮带控制_CheckedChanged);
            // 
            // 切割皮带控制
            // 
            this.切割皮带控制.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.切割皮带控制.Appearance.Options.UseFont = true;
            this.切割皮带控制.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割皮带控制.AppearanceDisabled.Options.UseFont = true;
            this.切割皮带控制.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割皮带控制.AppearanceHovered.Options.UseFont = true;
            this.切割皮带控制.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割皮带控制.AppearancePressed.Options.UseFont = true;
            this.切割皮带控制.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.切割皮带控制.Location = new System.Drawing.Point(11, 102);
            this.切割皮带控制.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.切割皮带控制.Name = "切割皮带控制";
            this.切割皮带控制.Size = new System.Drawing.Size(128, 56);
            this.切割皮带控制.TabIndex = 186;
            this.切割皮带控制.Text = "切割皮带转动/停止";
            this.切割皮带控制.CheckedChanged += new System.EventHandler(this.切割皮带控制_CheckedChanged);
            // 
            // 上料皮带控制
            // 
            this.上料皮带控制.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.上料皮带控制.Appearance.Options.UseFont = true;
            this.上料皮带控制.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.上料皮带控制.AppearanceDisabled.Options.UseFont = true;
            this.上料皮带控制.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.上料皮带控制.AppearanceHovered.Options.UseFont = true;
            this.上料皮带控制.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.上料皮带控制.AppearancePressed.Options.UseFont = true;
            this.上料皮带控制.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.上料皮带控制.Location = new System.Drawing.Point(11, 34);
            this.上料皮带控制.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.上料皮带控制.Name = "上料皮带控制";
            this.上料皮带控制.Size = new System.Drawing.Size(128, 56);
            this.上料皮带控制.TabIndex = 185;
            this.上料皮带控制.Text = "上料皮带转动/停止";
            this.上料皮带控制.CheckedChanged += new System.EventHandler(this.上料皮带控制_CheckedChanged);
            // 
            // 切割气缸顶升
            // 
            this.切割气缸顶升.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.切割气缸顶升.Appearance.Options.UseFont = true;
            this.切割气缸顶升.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割气缸顶升.AppearanceDisabled.Options.UseFont = true;
            this.切割气缸顶升.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割气缸顶升.AppearanceHovered.Options.UseFont = true;
            this.切割气缸顶升.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割气缸顶升.AppearancePressed.Options.UseFont = true;
            this.切割气缸顶升.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.切割气缸顶升.Location = new System.Drawing.Point(559, 102);
            this.切割气缸顶升.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.切割气缸顶升.Name = "切割气缸顶升";
            this.切割气缸顶升.Size = new System.Drawing.Size(128, 56);
            this.切割气缸顶升.TabIndex = 184;
            this.切割气缸顶升.Text = "切割气缸顶升";
            this.切割气缸顶升.CheckedChanged += new System.EventHandler(this.切割气缸顶升_CheckedChanged);
            // 
            // 打码气缸顶升
            // 
            this.打码气缸顶升.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.打码气缸顶升.Appearance.Options.UseFont = true;
            this.打码气缸顶升.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码气缸顶升.AppearanceDisabled.Options.UseFont = true;
            this.打码气缸顶升.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码气缸顶升.AppearanceHovered.Options.UseFont = true;
            this.打码气缸顶升.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码气缸顶升.AppearancePressed.Options.UseFont = true;
            this.打码气缸顶升.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.打码气缸顶升.Location = new System.Drawing.Point(285, 102);
            this.打码气缸顶升.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.打码气缸顶升.Name = "打码气缸顶升";
            this.打码气缸顶升.Size = new System.Drawing.Size(128, 56);
            this.打码气缸顶升.TabIndex = 183;
            this.打码气缸顶升.Text = "打码气缸顶升";
            this.打码气缸顶升.CheckedChanged += new System.EventHandler(this.打码气缸顶升_CheckedChanged);
            // 
            // 切割气缸阻挡
            // 
            this.切割气缸阻挡.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.切割气缸阻挡.Appearance.Options.UseFont = true;
            this.切割气缸阻挡.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割气缸阻挡.AppearanceDisabled.Options.UseFont = true;
            this.切割气缸阻挡.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割气缸阻挡.AppearanceHovered.Options.UseFont = true;
            this.切割气缸阻挡.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.切割气缸阻挡.AppearancePressed.Options.UseFont = true;
            this.切割气缸阻挡.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.切割气缸阻挡.Checked = true;
            this.切割气缸阻挡.Location = new System.Drawing.Point(559, 34);
            this.切割气缸阻挡.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.切割气缸阻挡.Name = "切割气缸阻挡";
            this.切割气缸阻挡.Size = new System.Drawing.Size(128, 56);
            this.切割气缸阻挡.TabIndex = 182;
            this.切割气缸阻挡.Text = "切割气缸阻挡";
            this.切割气缸阻挡.CheckedChanged += new System.EventHandler(this.切割气缸阻挡_CheckedChanged);
            // 
            // 翻转工位夹紧
            // 
            this.翻转工位夹紧.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.翻转工位夹紧.Appearance.Options.UseFont = true;
            this.翻转工位夹紧.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位夹紧.AppearanceDisabled.Options.UseFont = true;
            this.翻转工位夹紧.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位夹紧.AppearanceHovered.Options.UseFont = true;
            this.翻转工位夹紧.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位夹紧.AppearancePressed.Options.UseFont = true;
            this.翻转工位夹紧.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.翻转工位夹紧.Location = new System.Drawing.Point(423, 237);
            this.翻转工位夹紧.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.翻转工位夹紧.Name = "翻转工位夹紧";
            this.翻转工位夹紧.Size = new System.Drawing.Size(128, 56);
            this.翻转工位夹紧.TabIndex = 181;
            this.翻转工位夹紧.Text = "翻转工位松开/夹紧";
            this.翻转工位夹紧.CheckedChanged += new System.EventHandler(this.翻转工位夹紧_CheckedChanged);
            // 
            // 翻转工位旋转
            // 
            this.翻转工位旋转.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.翻转工位旋转.Appearance.Options.UseFont = true;
            this.翻转工位旋转.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位旋转.AppearanceDisabled.Options.UseFont = true;
            this.翻转工位旋转.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位旋转.AppearanceHovered.Options.UseFont = true;
            this.翻转工位旋转.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转工位旋转.AppearancePressed.Options.UseFont = true;
            this.翻转工位旋转.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.翻转工位旋转.Location = new System.Drawing.Point(423, 170);
            this.翻转工位旋转.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.翻转工位旋转.Name = "翻转工位旋转";
            this.翻转工位旋转.Size = new System.Drawing.Size(128, 56);
            this.翻转工位旋转.TabIndex = 180;
            this.翻转工位旋转.Text = "翻转工位旋转";
            this.翻转工位旋转.CheckedChanged += new System.EventHandler(this.翻转工位旋转_CheckedChanged);
            // 
            // 翻转气缸顶升
            // 
            this.翻转气缸顶升.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.翻转气缸顶升.Appearance.Options.UseFont = true;
            this.翻转气缸顶升.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转气缸顶升.AppearanceDisabled.Options.UseFont = true;
            this.翻转气缸顶升.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转气缸顶升.AppearanceHovered.Options.UseFont = true;
            this.翻转气缸顶升.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转气缸顶升.AppearancePressed.Options.UseFont = true;
            this.翻转气缸顶升.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.翻转气缸顶升.Location = new System.Drawing.Point(423, 102);
            this.翻转气缸顶升.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.翻转气缸顶升.Name = "翻转气缸顶升";
            this.翻转气缸顶升.Size = new System.Drawing.Size(128, 56);
            this.翻转气缸顶升.TabIndex = 179;
            this.翻转气缸顶升.Text = "翻转气缸顶升";
            this.翻转气缸顶升.CheckedChanged += new System.EventHandler(this.翻转气缸顶升_CheckedChanged);
            // 
            // 翻转气缸阻挡
            // 
            this.翻转气缸阻挡.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.翻转气缸阻挡.Appearance.Options.UseFont = true;
            this.翻转气缸阻挡.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转气缸阻挡.AppearanceDisabled.Options.UseFont = true;
            this.翻转气缸阻挡.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转气缸阻挡.AppearanceHovered.Options.UseFont = true;
            this.翻转气缸阻挡.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.翻转气缸阻挡.AppearancePressed.Options.UseFont = true;
            this.翻转气缸阻挡.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.翻转气缸阻挡.Checked = true;
            this.翻转气缸阻挡.Location = new System.Drawing.Point(422, 34);
            this.翻转气缸阻挡.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.翻转气缸阻挡.Name = "翻转气缸阻挡";
            this.翻转气缸阻挡.Size = new System.Drawing.Size(128, 56);
            this.翻转气缸阻挡.TabIndex = 178;
            this.翻转气缸阻挡.Text = "翻转气缸阻挡";
            this.翻转气缸阻挡.CheckedChanged += new System.EventHandler(this.翻转气缸阻挡_CheckedChanged);
            // 
            // 打码气缸阻挡
            // 
            this.打码气缸阻挡.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.打码气缸阻挡.Appearance.Options.UseFont = true;
            this.打码气缸阻挡.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码气缸阻挡.AppearanceDisabled.Options.UseFont = true;
            this.打码气缸阻挡.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码气缸阻挡.AppearanceHovered.Options.UseFont = true;
            this.打码气缸阻挡.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.打码气缸阻挡.AppearancePressed.Options.UseFont = true;
            this.打码气缸阻挡.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.打码气缸阻挡.Checked = true;
            this.打码气缸阻挡.Location = new System.Drawing.Point(285, 34);
            this.打码气缸阻挡.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.打码气缸阻挡.Name = "打码气缸阻挡";
            this.打码气缸阻挡.Size = new System.Drawing.Size(128, 56);
            this.打码气缸阻挡.TabIndex = 177;
            this.打码气缸阻挡.Text = "打码气缸阻挡";
            this.打码气缸阻挡.CheckedChanged += new System.EventHandler(this.打码气缸阻挡_CheckedChanged);
            // 
            // 待料气缸阻挡
            // 
            this.待料气缸阻挡.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.待料气缸阻挡.Appearance.Options.UseFont = true;
            this.待料气缸阻挡.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.待料气缸阻挡.AppearanceDisabled.Options.UseFont = true;
            this.待料气缸阻挡.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.待料气缸阻挡.AppearanceHovered.Options.UseFont = true;
            this.待料气缸阻挡.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.待料气缸阻挡.AppearancePressed.Options.UseFont = true;
            this.待料气缸阻挡.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.待料气缸阻挡.Checked = true;
            this.待料气缸阻挡.Location = new System.Drawing.Point(148, 34);
            this.待料气缸阻挡.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.待料气缸阻挡.Name = "待料气缸阻挡";
            this.待料气缸阻挡.Size = new System.Drawing.Size(128, 56);
            this.待料气缸阻挡.TabIndex = 176;
            this.待料气缸阻挡.Text = "待料气缸阻挡";
            this.待料气缸阻挡.CheckedChanged += new System.EventHandler(this.待料气缸阻挡_CheckedChanged);
            // 
            // NP_生产数据查询
            // 
            this.NP_生产数据查询.Caption = " 数据查询";
            this.NP_生产数据查询.Image = ((System.Drawing.Image)(resources.GetObject("NP_生产数据查询.Image")));
            this.NP_生产数据查询.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_生产数据查询.Name = "NP_生产数据查询";
            this.NP_生产数据查询.Size = new System.Drawing.Size(1903, 1093);
            // 
            // NP_工艺参数
            // 
            this.NP_工艺参数.Caption = " 工艺参数";
            this.NP_工艺参数.Controls.Add(this.tabPane1);
            this.NP_工艺参数.Image = ((System.Drawing.Image)(resources.GetObject("NP_工艺参数.Image")));
            this.NP_工艺参数.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NP_工艺参数.Name = "NP_工艺参数";
            this.NP_工艺参数.Size = new System.Drawing.Size(1684, 647);
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNG_上料调度);
            this.tabPane1.Controls.Add(this.tabNG_烧录);
            this.tabPane1.Controls.Add(this.tabNG_打码切割);
            this.tabPane1.Controls.Add(this.tabNG_下料组盘);
            this.tabPane1.Controls.Add(this.tabNG_打标包装);
            this.tabPane1.Location = new System.Drawing.Point(3, 4);
            this.tabPane1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNG_上料调度,
            this.tabNG_烧录,
            this.tabNG_打码切割,
            this.tabNG_下料组盘,
            this.tabNG_打标包装});
            this.tabPane1.RegularSize = new System.Drawing.Size(791, 669);
            this.tabPane1.SelectedPage = this.tabNG_烧录;
            this.tabPane1.Size = new System.Drawing.Size(791, 669);
            this.tabPane1.TabIndex = 0;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabNG_上料调度
            // 
            this.tabNG_上料调度.Caption = "上料调度";
            this.tabNG_上料调度.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_上料调度.Image")));
            this.tabNG_上料调度.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_上料调度.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNG_上料调度.Name = "tabNG_上料调度";
            this.tabNG_上料调度.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_上料调度.Size = new System.Drawing.Size(773, 591);
            // 
            // tabNG_烧录
            // 
            this.tabNG_烧录.Caption = "烧录";
            this.tabNG_烧录.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_烧录.Image")));
            this.tabNG_烧录.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_烧录.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNG_烧录.Name = "tabNG_烧录";
            this.tabNG_烧录.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_烧录.Size = new System.Drawing.Size(773, 605);
            // 
            // tabNG_打码切割
            // 
            this.tabNG_打码切割.Caption = "打码切割";
            this.tabNG_打码切割.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_打码切割.Image")));
            this.tabNG_打码切割.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打码切割.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNG_打码切割.Name = "tabNG_打码切割";
            this.tabNG_打码切割.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打码切割.Size = new System.Drawing.Size(773, 591);
            // 
            // tabNG_下料组盘
            // 
            this.tabNG_下料组盘.Caption = "下料组盘";
            this.tabNG_下料组盘.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_下料组盘.Image")));
            this.tabNG_下料组盘.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_下料组盘.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNG_下料组盘.Name = "tabNG_下料组盘";
            this.tabNG_下料组盘.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_下料组盘.Size = new System.Drawing.Size(773, 591);
            // 
            // tabNG_打标包装
            // 
            this.tabNG_打标包装.Caption = "打标包装";
            this.tabNG_打标包装.Image = ((System.Drawing.Image)(resources.GetObject("tabNG_打标包装.Image")));
            this.tabNG_打标包装.ItemShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打标包装.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabNG_打标包装.Name = "tabNG_打标包装";
            this.tabNG_打标包装.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText;
            this.tabNG_打标包装.Size = new System.Drawing.Size(773, 591);
            // 
            // timer_IO
            // 
            this.timer_IO.Enabled = true;
            this.timer_IO.Interval = 150;
            this.timer_IO.Tick += new System.EventHandler(this.timer_IO_Tick);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // frm_LaserStationMain
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 706);
            this.Controls.Add(this.navigationPane1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_LaserStationMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "打码切割I/O 监控";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.NP_主页运行.ResumeLayout(false);
            this.NP_主页运行.PerformLayout();
            this.NP_设置.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.启用禁用翻转.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.NP_视觉.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.NP_手动监控.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDst_Y.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDst_X.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.NP_工艺参数.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_主页运行;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_StopMachine;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_StartMachine;
        private DevExpress.XtraEditors.SimpleButton btmInvoke_ResetMachine;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_设置;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_视觉;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_手动监控;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_生产数据查询;
        private DevExpress.XtraBars.Navigation.NavigationPage NP_工艺参数;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_上料调度;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_烧录;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_打码切割;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_下料组盘;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNG_打标包装;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox LED_Out0;
        private System.Windows.Forms.CheckBox LED_Out31;
        private System.Windows.Forms.CheckBox LED_Out1;
        private System.Windows.Forms.CheckBox LED_Out30;
        private System.Windows.Forms.CheckBox LED_Out2;
        private System.Windows.Forms.CheckBox LED_Out29;
        private System.Windows.Forms.CheckBox LED_Out3;
        private System.Windows.Forms.CheckBox LED_Out28;
        private System.Windows.Forms.CheckBox LED_Out4;
        private System.Windows.Forms.CheckBox LED_Out27;
        private System.Windows.Forms.CheckBox LED_Out5;
        private System.Windows.Forms.CheckBox LED_Out26;
        private System.Windows.Forms.CheckBox LED_Out6;
        private System.Windows.Forms.CheckBox LED_Out25;
        private System.Windows.Forms.CheckBox LED_Out7;
        private System.Windows.Forms.CheckBox LED_Out24;
        private System.Windows.Forms.CheckBox LED_Out8;
        private System.Windows.Forms.CheckBox LED_Out23;
        private System.Windows.Forms.CheckBox LED_Out9;
        private System.Windows.Forms.CheckBox LED_Out22;
        private System.Windows.Forms.CheckBox LED_Out10;
        private System.Windows.Forms.CheckBox LED_Out21;
        private System.Windows.Forms.CheckBox LED_Out11;
        private System.Windows.Forms.CheckBox LED_Out20;
        private System.Windows.Forms.CheckBox LED_Out12;
        private System.Windows.Forms.CheckBox LED_Out19;
        private System.Windows.Forms.CheckBox LED_Out13;
        private System.Windows.Forms.CheckBox LED_Out18;
        private System.Windows.Forms.CheckBox LED_Out14;
        private System.Windows.Forms.CheckBox LED_Out17;
        private System.Windows.Forms.CheckBox LED_Out15;
        private System.Windows.Forms.CheckBox LED_Out16;
        private System.Windows.Forms.GroupBox groupBox6;
        private HslCommunication.Controls.UserLantern userLanternIO;
        private HslCommunication.Controls.UserLantern LED_In0;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label37;
        private HslCommunication.Controls.UserLantern LED_In1;
        private System.Windows.Forms.Label label38;
        private HslCommunication.Controls.UserLantern LED_In2;
        private System.Windows.Forms.Label label39;
        private HslCommunication.Controls.UserLantern LED_In3;
        private System.Windows.Forms.Label label40;
        private HslCommunication.Controls.UserLantern LED_In4;
        private System.Windows.Forms.Label label41;
        private HslCommunication.Controls.UserLantern LED_In5;
        private System.Windows.Forms.Label label42;
        private HslCommunication.Controls.UserLantern LED_In6;
        private System.Windows.Forms.Label label43;
        private HslCommunication.Controls.UserLantern LED_In7;
        private System.Windows.Forms.Label label44;
        private HslCommunication.Controls.UserLantern LED_In9;
        private System.Windows.Forms.Label label26;
        private HslCommunication.Controls.UserLantern LED_In10;
        private System.Windows.Forms.Label label30;
        private HslCommunication.Controls.UserLantern LED_In11;
        private System.Windows.Forms.Label label31;
        private HslCommunication.Controls.UserLantern LED_In12;
        private System.Windows.Forms.Label label32;
        private HslCommunication.Controls.UserLantern LED_In13;
        private System.Windows.Forms.Label label33;
        private HslCommunication.Controls.UserLantern LED_In14;
        private System.Windows.Forms.Label label34;
        private HslCommunication.Controls.UserLantern LED_In15;
        private System.Windows.Forms.Label label35;
        private HslCommunication.Controls.UserLantern LED_In8;
        private System.Windows.Forms.Label label36;
        private HslCommunication.Controls.UserLantern LED_In17;
        private HslCommunication.Controls.UserLantern LED_In24;
        private HslCommunication.Controls.UserLantern LED_In18;
        private HslCommunication.Controls.UserLantern LED_In31;
        private HslCommunication.Controls.UserLantern LED_In19;
        private HslCommunication.Controls.UserLantern LED_In30;
        private HslCommunication.Controls.UserLantern LED_In20;
        private HslCommunication.Controls.UserLantern LED_In29;
        private HslCommunication.Controls.UserLantern LED_In21;
        private HslCommunication.Controls.UserLantern LED_In28;
        private HslCommunication.Controls.UserLantern LED_In22;
        private HslCommunication.Controls.UserLantern LED_In27;
        private HslCommunication.Controls.UserLantern LED_In23;
        private HslCommunication.Controls.UserLantern LED_In26;
        private HslCommunication.Controls.UserLantern LED_In16;
        private HslCommunication.Controls.UserLantern LED_In25;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timer_IO;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private HslCommunication.Controls.UserLantern LED_ORG_Y;
        private HslCommunication.Controls.UserLantern LED_ELN_Y;
        private HslCommunication.Controls.UserLantern LED_ELP_Y;
        private DevExpress.XtraEditors.SimpleButton Y轴反转;
        private DevExpress.XtraEditors.SimpleButton Y轴正转;
        private System.Windows.Forms.Label lable11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label17;
        private HslCommunication.Controls.UserLantern LED_ORG_X;
        private HslCommunication.Controls.UserLantern LED_ELN_X;
        private HslCommunication.Controls.UserLantern LED_ELP_X;
        private DevExpress.XtraEditors.SimpleButton X轴反转;
        private DevExpress.XtraEditors.SimpleButton X轴正转;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private HslCommunication.Controls.UserLantern LED_ALARM_X;
        private HslCommunication.Controls.UserLantern LED_INP_X;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label24;
        private HslCommunication.Controls.UserLantern LED_ALARM_Y;
        private HslCommunication.Controls.UserLantern LED_INP_Y;
        private HslCommunication.Controls.UserLantern userLanternAXIS;
        private DevExpress.XtraEditors.CheckButton Y轴启用禁用;
        private DevExpress.XtraEditors.CheckButton X轴启用禁用;
        private DevExpress.XtraEditors.TextEdit txtDst_Y;
        private DevExpress.XtraEditors.TextEdit txtDst_X;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.SimpleButton X轴停止;
        private DevExpress.XtraEditors.SimpleButton Y轴停止;
        private DevExpress.XtraEditors.SimpleButton Y轴报警清除;
        private DevExpress.XtraEditors.SimpleButton X轴报警清除;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.SimpleButton 单次采集;
        private DevExpress.XtraEditors.CheckButton 待料气缸阻挡;
        private DevExpress.XtraEditors.CheckButton 切割气缸顶升;
        private DevExpress.XtraEditors.CheckButton 打码气缸顶升;
        private DevExpress.XtraEditors.CheckButton 切割气缸阻挡;
        private DevExpress.XtraEditors.CheckButton 翻转工位夹紧;
        private DevExpress.XtraEditors.CheckButton 翻转工位旋转;
        private DevExpress.XtraEditors.CheckButton 翻转气缸顶升;
        private DevExpress.XtraEditors.CheckButton 翻转气缸阻挡;
        private DevExpress.XtraEditors.CheckButton 打码气缸阻挡;
        private DevExpress.XtraEditors.CheckButton 回流皮带控制;
        private DevExpress.XtraEditors.CheckButton 切割皮带控制;
        private DevExpress.XtraEditors.CheckButton 上料皮带控制;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.SimpleButton 连续采集;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.RichTextBox richTextLaserMarking;
        private System.Windows.Forms.RichTextBox richTextLoad;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.RichTextBox richTextLaseCutting;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.RichTextBox richTextRotate;
        private DevExpress.XtraEditors.CheckEdit 启用禁用翻转;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.CheckButton 切割工位上料完成;
        private DevExpress.XtraEditors.CheckButton 切割工位允许上料;
        private DevExpress.XtraEditors.CheckButton 打码工位上料完成;
        private DevExpress.XtraEditors.CheckButton 打码工位允许上料;
        private DevExpress.XtraEditors.CheckButton 下料工位上料完成;
        private DevExpress.XtraEditors.CheckButton 下料工位允许上料;
        private DevExpress.XtraEditors.SimpleButton 翻转工位联动;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private HslCommunication.Controls.UserLantern userLantern3;
        private HslCommunication.Controls.UserLantern userLantern4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private HslCommunication.Controls.UserLantern LEDModbusServer;
        private HslCommunication.Controls.UserLantern LEDUnloadPLCState;
        private System.Windows.Forms.ImageList imageList16;
        private DevExpress.XtraEditors.CheckButton checkButton1;
        private DevExpress.XtraEditors.CheckButton checkButton2;
    }
}