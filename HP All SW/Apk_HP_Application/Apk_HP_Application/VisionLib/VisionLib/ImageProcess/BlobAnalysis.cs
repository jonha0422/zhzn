﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib.ImageProcess
{
    public class BlobAnalysi : ImageProcessBase
    {

        public double MinGray { get; set; }//灰度值 ---HSV
        public double MaxGray { get; set; }//灰度值 ---HSV

        public int BlobNumber{ get; set; }
        public BlobAnalysi()
        {
            cName = "Blob分析";

        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            int suc = getTempColor();
            return 0;
        }
        public int getTempColor()
        {
            if (ModelRoi==null || ModelRoi.Count == 0)
            {
                return -1;
            }
            HObject ho_ImageReduced,ThresholdObj,Balls;
            HRegion hReg = ModelRoi[0].getRegion();
            double R = hReg.Row.D;

            HOperatorSet.ReduceDomain(TempImage, hReg, out  ho_ImageReduced);
            HTuple channels = 0;
            HOperatorSet.CountChannels(ho_ImageReduced, out channels);
            HTuple mean, deviation;
           
            if (channels == 3)
            {
               // HObject gObject=null,G,B;
                //HOperatorSet.Rgb1ToGray(ho_ImageReduced, out gObject);
               // HOperatorSet.Decompose3(ho_ImageReduced, out gObject,out G,out B);
               // HOperatorSet.Threshold(gObject, out ThresholdObj, MinGray, MaxGray);
                HObject Red, Green, Blue;
                HObject Hue, Saturation, Value;
                HOperatorSet.Decompose3(ho_ImageReduced, out Red, out Green, out Blue);
                HOperatorSet.TransFromRgb(Red, Green, Blue, out Hue, out Saturation, out Value, "hsv");
                HOperatorSet.Threshold(Hue, out ThresholdObj, MinGray, MaxGray);
            }
            else
            {
                HOperatorSet.Threshold(ho_ImageReduced, out ThresholdObj, MinGray, MaxGray);
            }
            HOperatorSet.Connection(ThresholdObj, out ThresholdObj);
            HOperatorSet.SelectShape(ThresholdObj, out ThresholdObj,"area","and", 1000, 9000000);
            if (ViewPort != null)
            {
                //HOperatorSet.cl
                HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                HOperatorSet.DispObj(ThresholdObj, ViewPort.HalconWindow);
            }
            HObject SingleBalls,IntermediateBalls;
            //HOperatorSet.OpeningCircle (ThresholdObj,out  Balls, 20);
            HOperatorSet.ClosingCircle(ThresholdObj, out  Balls, 20);

            if (ViewPort != null)
            {
                //HOperatorSet.cl
                HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                HOperatorSet.SetColor(ViewPort.HalconWindow, "blue");
                HOperatorSet.DispObj(Balls, ViewPort.HalconWindow);
            }
            HOperatorSet.Connection (Balls, out SingleBalls);
            HOperatorSet.SelectShape(SingleBalls, out IntermediateBalls, "circularity", "and", 0.55, 1.0);
            if (ViewPort != null)
            {
                HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                HOperatorSet.DispObj(IntermediateBalls, ViewPort.HalconWindow);
            }
            //   select_shape (SingleBalls, IntermediateBalls, 'circularity', 'and', 0.85, 1.0);
            return 0;
        }
        public override int ImageProcess()
        {
             if (SrcObject == null)
            {
                return 0;
            }

             if (SearchRoi == null || SearchRoi.Count == 0)
             {
                 return -1;
             }

            DesObject = null ;
            HObject SearchObj = SrcObject;
            HObject ho_ImageReduced,ThresholdObj,Balls;
            HRegion hReg = SearchRoi[0].getRegion();
            double R = hReg.Row.D;

            HOperatorSet.ReduceDomain(SearchObj, hReg, out  ho_ImageReduced);
            HTuple channels = 0;
            HOperatorSet.CountChannels(ho_ImageReduced, out channels);
           
            if (channels == 3)
            {
                // HObject gObject=null,G,B;
                //HOperatorSet.Rgb1ToGray(ho_ImageReduced, out gObject);
                // HOperatorSet.Decompose3(ho_ImageReduced, out gObject,out G,out B);
                // HOperatorSet.Threshold(gObject, out ThresholdObj, MinGray, MaxGray);
                HObject Red, Green, Blue;
                HObject Hue, Saturation, Value;
                HOperatorSet.Decompose3(ho_ImageReduced, out Red, out Green, out Blue);
                HOperatorSet.TransFromRgb(Red, Green, Blue, out Hue, out Saturation, out Value, "hsv");
                HOperatorSet.Threshold(Hue, out ThresholdObj, MinGray, MaxGray);
            }
            else
            {
                HOperatorSet.Threshold(ho_ImageReduced, out ThresholdObj, MinGray, MaxGray);
            }
            HOperatorSet.Connection(ThresholdObj, out ThresholdObj);
            HOperatorSet.SelectShape(ThresholdObj, out ThresholdObj, "area", "and", 1000, 9000000);
            if (ViewPort != null)
            {
                //HOperatorSet.cl
                HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                HOperatorSet.DispObj(ThresholdObj, ViewPort.HalconWindow);
            }
            
            HObject SingleBalls,IntermediateBalls;
           // HOperatorSet.OpeningCircle (ThresholdObj,out  Balls, 16.3) ;
            HOperatorSet.ClosingCircle(ThresholdObj, out  Balls, 20);
            if (ViewPort != null)
            {
                //HOperatorSet.cl
                HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                HOperatorSet.SetColor(ViewPort.HalconWindow, "blue");
                HOperatorSet.DispObj(Balls, ViewPort.HalconWindow);
            }
            HOperatorSet.Connection (Balls, out SingleBalls);
            HOperatorSet.SelectShape(SingleBalls, out IntermediateBalls, "circularity", "and", 0.55, 1.0);
 
            HTuple Row,Column,Radius;
            HOperatorSet.SmallestCircle(IntermediateBalls, out Row, out Column, out Radius);
            HTuple Number=0;
            HOperatorSet.CountObj(IntermediateBalls, out Number);
            Result aR = new Result();
            aR.cX = Number.I;
            if (Number.I >= BlobNumber)
            {
                if (ViewPort != null)
                {
                    HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                    HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                    HOperatorSet.DispObj(IntermediateBalls, ViewPort.HalconWindow);
                }
                aR.isOk = true;
                ResultSta = true;
            }
            else
            {
                if (ViewPort != null)
                {
                    HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                    HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                    HOperatorSet.DispObj(IntermediateBalls, ViewPort.HalconWindow);
                }
                aR.isOk = false;
                ResultSta = false;
            }
            mResult.Add(aR);
            
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            BlobAnalysi aFun = CoverLib.JsonCover.DeStrLSerialize<BlobAnalysi>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<BlobAnalysi>(this);
            return aFunPara;
        }
    }
}
