﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CamaeraLib
{
    public partial class CameraParaFrm : Form
    {
        public int CamID { get; set; }
        public CameraParaFrm()
        {
            InitializeComponent();
        }

        private void bClose_Click(object sender, EventArgs e)
        {
            CameraSystem.Instance.Writer();
            Close();
        }

        private void CameraParaFrm_Load(object sender, EventArgs e)
        {
            ucCameraPara1.init(CameraSystem.Instance.CameraList[CamID]);
        }
    }
}
