﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace LanguageLib
{
    /**
     * - Keeps to dictionary about the tag and related texts.
     * - Getting the text for the active language.
     * - Setting the text for the active language.
     */
    public class LanguageSet:LanguageDef
    {
        // Static setting for the text "Key".
        public static String KEY = "Key";

        /// <summary>
        /// Constructor of this class.
        /// </summary>
        public LanguageSet()
        {
            CreateDictionaries();
        }

        /// <summary>
        /// Sets and gets the property 'Key'.
        /// </summary>
        public String Key { get; set; }

        public static bool IsSame(LanguageSet ls1, LanguageSet ls2)
        {
            FieldInfo[] f1 = typeof(LanguageSet).GetFields();

            for (int i = 0; i < f1.Length; i++)
            {
                object o1 = f1[i].GetValue(ls1);
                object o2 = f1[i].GetValue(ls2);

                if ((o1 == null) && (o2 != null))
                {
                    return false;
                }

                if ((o2 == null) && (o1 != null))
                {
                    return false;
                }

                if ((o1 == null) && (o2 == null))
                {
                    continue;
                }

                if ((o1.GetType() == typeof(string)) && o2.GetType() == typeof(string))
                {
                    if (o1.ToString() != o2.ToString())
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Adds text or help text to the dedicated language. 
        /// </summary> 
        /// <param name="key">the key to the writing of readind dictionary</param>
        /// <param name="text">the text for the key</param>
        /// 
        /// <return>bool:status of setting text</return>
        public bool SetText(String key, String text)
        {
            SetLang sl;
            SetHelp sh;

            // Read the "pointer" to the text method from
            // the writing dictionary for texts. 
            if (langWrite.TryGetValue(key, out sl))
            {
                sl(text);
                return true;
            }

            // Read the "pointer" to the help method from
            // the writing dictionary for help texts.
            else if (langWriteHelp.TryGetValue(key, out sh))
            {
                sh(text);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Updates existing text.
        /// </summary> 
        /// <param name="key">the key of the writing and reading dictionary</param>
        /// <param name="text">the new text value</param>
        public bool UpdateText(String key, String text)
        {
            SetLang sl;
            SetHelp sh;

            // Read the "pointer" to the text method from
            // the writing dictionary for texts. 
            if (langWrite.TryGetValue(key, out sl))
            {
                sl(text);
                return true;
            }
            // Read the "pointer" to the help method from
            // the writing dictionary for help texts.
            else if (langWriteHelp.TryGetValue(key, out sh))
            {
                sh(text);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Reads a text from the dictionary
        /// </summary> 
        /// <param name="key">the key of the reading dictionary</param>
        /// 
        /// <return>String:the text in selected language or the key of the language set.</return>
        public String GetText(String key)
        {
            GetLang gl;
            GetHelp gh;

            // Read the "pointer" to the text method from
            // the reading dictionary for texts.
            if (langRead.TryGetValue(key, out gl))
                return gl();
            // Read the "pointer" to the help method from
            // the reading dictionary for help texts.
            else if (langReadHelp.TryGetValue(key, out gh))
                return gh();
            else
                return Key;
        }

        /// <summary>
        /// Sets the help text for the active language.
        /// </summary> 
        /// <param name="key">the key to the writing of readind dictionary or the tag text</param>
        /// <param name="text">the value for the key</param>
        public bool SetHelpText(String key, String text)
        {
            SetHelp sh;

            // Read the "pointer" to the right language method from
            // the writing dictionary. 
            if (langWriteHelp.TryGetValue(key, out sh))
            {
                sh(text);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Updates existing help text.
        /// </summary> 
        /// <param name="key">the key of the writing and reading dictionary</param>
        /// <param name="text">the new text value</param>
        public bool UpdateHelpText(String key, String text)
        {
            // Read the "pointer" to the right method from
            // the writing dictionary. 
            SetHelp sh;
            if (langWriteHelp.TryGetValue(key, out sh))
            {
                sh(text);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reads a help text from the dictionary
        /// </summary> 
        /// <param name="key">the key of the reading dictionary</param>
        ///
        /// <return>String:the text in selected language or the key of the language set.</return>
        public String GetHelpText(String key)
        {
            // Read the "pointer" to the right method from
            // the reading dictionary. 
            GetHelp gh;
            if (langReadHelp.TryGetValue(key, out gh))
                return gh();
            else
                return Key;
        }

        /// <summary>
        /// Returns the size of the enum.
        /// </summary>
        /// <return>int:the size of the enum (-1 if length error)</return>
        public int GetDictionarySize()
        {
            Array values = Enum.GetValues(typeof(lang));

            // Return the length if the length of the enum is equal to the count of dictionaries.
            // Otherwise, return -1 indicating the length error.
            if (values.Length == langRead.Count && values.Length == langWrite.Count)
                return values.Length;
            else
                return -1;
        }

        /// <summary>
        /// Checks if the active language selection is for the text.
        /// </summary>
        /// <return>bool:status</return>
        public bool IsText(String language)
        {
            try
            {
                SetLang lang;
                if (langWrite.TryGetValue(language, out lang))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }

        /// <summary>
        /// Checks if the active language selection is for the help text.
        /// </summary> 
        /// <return>bool:status</return>
        public bool IsHelpText(String language)
        {
            try
            {
                SetHelp lang;
                if (langWriteHelp.TryGetValue(language, out lang))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }
        public void ConvertToVersion1()
        {
            //if (English != null)
            //{
            //    English = English.Replace("_", "[NL]");
            //}
            if (EnglishHelp != null)
            {
                EnglishHelp = EnglishHelp.Replace("_", "[NL]");
            }
            if (French != null)
            {
                French = French.Replace("_", "[NL]");
            }
            if (FrenchHelp != null)
            {
                FrenchHelp = FrenchHelp.Replace("_", "[NL]");
            }
            if (Finnish != null)
            {
                Finnish = Finnish.Replace("_", "[NL]");
            }
            if (FinnishHelp != null)
            {
                FinnishHelp = FinnishHelp.Replace("_", "[NL]");
            }
            if (Estonian != null)
            {
                Estonian = Estonian.Replace("_", "[NL]");
            }
            if (EstonianHelp != null)
            {
                EstonianHelp = EstonianHelp.Replace("_", "[NL]");
            }
            if (Polish != null)
            {
                Polish = Polish.Replace("_", "[NL]");
            }
            if (PolishHelp != null)
            {
                PolishHelp = PolishHelp.Replace("_", "[NL]");
            }
            if (German != null)
            {
                German = German.Replace("_", "[NL]");
            }
            if (GermanHelp != null)
            {
                GermanHelp = GermanHelp.Replace("_", "[NL]");
            }
        }
    }
}
