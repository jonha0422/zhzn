﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.IO;
using HalconDotNet;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using static ZHToolBox.HalconVision.HDVisionTypes;
using static ZHToolBox.HalconVision.HalconGeneral;

namespace ZHToolBox.HalconVision
{
    public class ShapeModelInspector : MethodBase
    {
        bool TemplateIsCreated = false;
        private SHAPE_MODE_DES _ShapeModeDes = new SHAPE_MODE_DES();
        private MATCHINGRESULT_DES _MatchingResult = new MATCHINGRESULT_DES();

        #region>>>>>>>>> 配制参数  //Category("选项"), DefaultValue(3), DisplayName("测试")
        [Editor(typeof(PropertyGridTempleteUI), typeof(UITypeEditor)), Category("配置"), DefaultValue(""), DisplayName("模版图像文件")]
        public string tempFileName { get; set; }

        [Editor(typeof(PropertyGridTempleteUI), typeof(UITypeEditor)), Category("配置"), DefaultValue(""), DisplayName("测量图像文件")]
        public string ImageFileName { get; set; }
        #endregion

        #region>>>>>>>>> 建模参数
        [TypeConverter(typeof(ExpandableObjectConverter)), Category("建模参数"), DefaultValue(3), DisplayName("图片")]
        public HObject objImage { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter)), Category("建模参数"), DefaultValue(3), DisplayName("RIO 模版区域1")]
        public RIO_DES strRIO1 { get { return _ShapeModeDes.strRIO1; } set { _ShapeModeDes.strRIO1 = value; } }

        [TypeConverter(typeof(ExpandableObjectConverter)), Category("建模参数"), DefaultValue(3), DisplayName("RIO 模版区域2")]
        public RIO_DES strRIO2 { get { return _ShapeModeDes.strRIO2; } set { _ShapeModeDes.strRIO2 = value; } }

        [Category("建模参数"), DefaultValue(3), DisplayName("二值化最小")]
        public double fThresholdMin { get { return _ShapeModeDes.fThresholdMin; } set { _ShapeModeDes.fThresholdMin = value; } }

        [Category("建模参数"), DefaultValue(3), DisplayName("二值化最大")]
        public double fThresholdMax { get { return _ShapeModeDes.fThresholdMax; } set { _ShapeModeDes.fThresholdMax = value; } }

        [Category("建模参数"), DefaultValue(3), DisplayName("匹配类型")]
        public ShapeModeMatchType MatchingType { get { return _ShapeModeDes.MatchingType; } set { _ShapeModeDes.MatchingType = value; } }

        [Category("建模参数"), DefaultValue(-10), DisplayName("建模开始角度")]
        public double fAngleStart_T { get { return _ShapeModeDes.fAngleStart_T; } set { _ShapeModeDes.fAngleStart_T = value; } } // 模板匹配寻找的初始角度, 最大角度及角度间隔

        [Category("建模参数"), DefaultValue(20), DisplayName("建模角度范围")]
        public double fAngleExtent_T { get { return _ShapeModeDes.fAngleExtent_T; } set { _ShapeModeDes.fAngleExtent_T = value; } }

        [Category("建模参数"), DefaultValue(20), DisplayName("建模角度增量")]
        public double fAngleStep_T { get { return _ShapeModeDes.fAngleStep_T; } set { _ShapeModeDes.fAngleStep_T = value; } }

        [Category("建模参数"), DefaultValue(0.90), DisplayName("优化程度")]
        public string szOptimization { get { return _ShapeModeDes.szOptimization; } set { _ShapeModeDes.szOptimization = value; } }

        [Category("建模参数"), DefaultValue(0.90), DisplayName("szMetric")]
        public string szMetric { get { return _ShapeModeDes.szMetric; } set { _ShapeModeDes.szMetric = value; } }

        [Category("建模参数"), DefaultValue(0.90), DisplayName("szContrast")]
        public string szContrast { get { return _ShapeModeDes.szContrast; } set { _ShapeModeDes.szContrast = value; } }

        [Category("建模参数"), DefaultValue(0.90), DisplayName("szMinContrast")]
        public string szMinContrast { get { return _ShapeModeDes.szMinContrast; } set { _ShapeModeDes.szMinContrast = value; } }

        #endregion

        #region>>>>>>>>> 检测参数
        [Category("检测参数"), DefaultValue(5), DisplayName("金字塔级别")]
        public Int32 dwNumLevels { get { return _ShapeModeDes.dwNumLevels; } set { _ShapeModeDes.dwNumLevels = value; } }

        [Category("检测参数"), DefaultValue(-10), DisplayName("开始角度")]
        public double fAngleStart { get { return _ShapeModeDes.fAngleStart; } set { _ShapeModeDes.fAngleStart = value; } } // 模板匹配寻找的初始角度, 最大角度及角度间隔

        [Category("检测参数"), DefaultValue(20), DisplayName("角度范转")]
        public double fAngleExtent { get { return _ShapeModeDes.fAngleExtent; } set { _ShapeModeDes.fAngleExtent = value; } }

        [Category("检测参数"), DefaultValue(20), DisplayName("寻找角度增量")]
        public double fAngleStep { get { return _ShapeModeDes.fAngleStep; } set { _ShapeModeDes.fAngleStep = value; } }

        [Category("检测参数"), DefaultValue(0.90), DisplayName("最小匹配度")]
        public double fMinScore { get { return _ShapeModeDes.fMinScore; } set { _ShapeModeDes.fMinScore = value; } }

        [Category("检测参数"), DefaultValue(100), DisplayName("最大检验/匹配个数")]
        public Int32 dwNumMatches { get { return _ShapeModeDes.dwNumMatches; } set { _ShapeModeDes.dwNumMatches = value; } }

        [Category("检测参数"), DefaultValue(0.70), DisplayName("最大被覆盖范围比例")]
        public double fMaxOverlap { get { return _ShapeModeDes.fMaxOverlap; } set { _ShapeModeDes.fMaxOverlap = value; } }

        [Category("检测参数"), DefaultValue(0.90), DisplayName("优化程度")]
        public string szSubPixel { get { return _ShapeModeDes.szSubPixel; } set { _ShapeModeDes.szSubPixel = value; } }

        [Category("检测参数"), DefaultValue(0.4), DisplayName("搜索启发式值")]
        public double fGreediness { get { return _ShapeModeDes.fGreediness; } set { _ShapeModeDes.fGreediness = value; } }

        [Category("检测参数"), DefaultValue(1), DisplayName("最小比例")]
        public double fScaledMin { get { return _ShapeModeDes.fScaledMin; } set { _ShapeModeDes.fScaledMin = value; } }

        [Category("检测参数"), DefaultValue(1), DisplayName("最大比例")]
        public double fScaledMax { get { return _ShapeModeDes.fScaledMax; } set { _ShapeModeDes.fScaledMax = value; } }
        #endregion

        #region>>>>>>>>> 输出
        [Category("结果"), DefaultValue(3), DisplayName("最终结果")]
        public bool FinallyResult { get; set; }

        [Category("结果"), DefaultValue(3), DisplayName("匹配数量")]
        public int CountOfMatch { get; set; }

        [Editor(typeof(PropertyGridRIOCollectorUI), typeof(UITypeEditor)), Category("结果"), DefaultValue(3), DisplayName("检测区域及结果")]
        public CheckResultDef[] CheckResult { get; set; }

        #endregion

        #region>>>>>>>>>显示
        bool _bShowProcessWindow = false;
        [Category("性能/显示"), DefaultValue(false), DisplayName("显示图像处理窗口")]
        public bool bShowProcessWindow
        {
            get
            {
                return _bShowProcessWindow;
            }

            set
            {
                _bShowProcessWindow = value;
                if (_bShowProcessWindow)
                {
                    if (ImageTempWnd != null)
                        ImageTempWnd.Show();

                    if (ImageProcessWnd != null)
                        ImageProcessWnd.Show();
                }
                else
                {
                    if (ImageTempWnd != null)
                        ImageTempWnd.Hide();

                    if (ImageProcessWnd != null)
                        ImageProcessWnd.Hide();
                }
            }

        }

        [Category("性能/显示"), DefaultValue(false), DisplayName("启用多线程")]
        public bool bEnableMuitlThread { get; set; }
        #endregion

        HalconForm ImageTempWnd = new HalconForm("模版窗口");
        HalconForm ImageProcessWnd = new HalconForm("图像处理窗口");

        public ShapeModelInspector()
        {
            FinallyResult = false;

            DateTime dateTime = DateTime.Now;
            Name = dateTime.ToString("yyyyMMddHHmmss") + $".{dateTime.Millisecond.ToString("D03")}模版匹配";

            CheckResult = new CheckResultDef[1];
            CheckResult[0] = new CheckResultDef();
        }

        public void Dispose()
        {
            try
            {
                ImageTempWnd.Dispose();
                ImageProcessWnd.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        public bool Init(string tempImageFileName, bool reset = false)
        {
            try
            {
                HObject objImage;
                if (tempImageFileName == null || tempImageFileName.Length <= 0)
                {
                    if (!File.Exists(tempFileName))
                        throw new HalconException($"ShapeModelInspector ImageProcess()处理异常，当前文件不存在\r\n{tempFileName}");

                    HOperatorSet.ReadImage(out objImage, tempFileName);
                }
                else
                {
                    if (!File.Exists(tempImageFileName))
                        throw new HalconException($"ShapeModelInspector ImageProcess()处理异常，当前文件不存在\r\n{tempImageFileName}");

                    tempFileName = tempImageFileName;
                    HOperatorSet.ReadImage(out objImage, tempImageFileName);
                }

                return Init(new HImage(objImage), reset);

            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Init(HImage vTempImage, bool reset = false)
        {
            try
            {
                if (TemplateIsCreated && !reset)
                    return true;

                HOperatorSet.ClearWindow(ImageTempWnd.hWindow);

                //创建模版
                HObject imageObj = null;
                HOperatorSet.CopyImage(vTempImage, out imageObj);
                _ShapeModeDes.objImage = imageObj.Clone();
                if (!ShapeModel_CreateTemplaterFromImage(ref _ShapeModeDes, _ShapeModeDes.MatchingType))
                    return false;

                //如果创建成功再将图片拷回
                HOperatorSet.CopyImage(vTempImage, out imageObj);
                _ShapeModeDes.objImage = imageObj;

                HTuple width, height;
                HOperatorSet.GetImageSize(imageObj, out width, out height);
                ImageTempWnd.ShowImage(imageObj, width, height);
                TemplateIsCreated = true;

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ImageProcess(string tempImageFileName)
        {
            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"ShapeModelInspector 没有建立模版，请调用Init");

                if (!File.Exists(tempImageFileName))
                    throw new HalconException($"ShapeModelInspector ImageProcess()处理异常，当前文件不存在\r\n{tempImageFileName}");

                HObject objImage;
                HOperatorSet.ReadImage(out objImage, tempImageFileName);
                bool result = ImageProcess(new HImage(objImage));
                return result;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ImageProcess(HImage imageObj)
        {
            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"ShapeModelInspector 没有建立模版，请调用Init");

                if (!HalconGeneral.IsImageObj(imageObj))
                    throw new HalconException($"ShapeModelInspector ImageProcess 图像格式不对");

                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    CheckResult[nIndexAOI].Result = -999;
                    CheckResult[nIndexAOI].ResultStatus = false;
                }

                HOperatorSet.ClearWindow(ImageProcessWnd.hWindow);

                CountOfMatch = 0;
                HTuple width, height;
                HOperatorSet.GetImageSize(imageObj, out width, out height);
                ImageProcessWnd.ShowImage(imageObj, width, height);

                HObject ho_Image = null;
                HOperatorSet.CopyImage(imageObj, out ho_Image);
                HOperatorSet.DispObj(ho_Image, ImageProcessWnd.hWindow);

                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    if (CheckResult[nIndexAOI].CheckedRIO.fRow <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fCol <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fLen1 <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fLen2 <= 0.01)
                    {
                        CheckResult[nIndexAOI].CheckedRIO.fRow = (double)height / 2;
                        CheckResult[nIndexAOI].CheckedRIO.fCol = (double)width / 2;
                        CheckResult[nIndexAOI].CheckedRIO.fPhi = 0;
                        CheckResult[nIndexAOI].CheckedRIO.fLen1 = (double)width / 2 - 2;
                        CheckResult[nIndexAOI].CheckedRIO.fLen2 = (double)height / 2 - 2;
                    }

                    HObject Rectangle1, TemplateBottleRegion, ImageReduce;
                    HOperatorSet.GenRectangle2(out Rectangle1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol,
                                                                    (new HTuple(CheckResult[nIndexAOI].CheckedRIO.fPhi)).TupleRad(), CheckResult[nIndexAOI].CheckedRIO.fLen1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fLen2);

                    HOperatorSet.Union1(Rectangle1, out TemplateBottleRegion);
                    HOperatorSet.ReduceDomain(ho_Image, TemplateBottleRegion, out ImageReduce);
                    HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);


                    MATCHINGRESULT_DES resultOfMacht = new MATCHINGRESULT_DES();
                   
                    bool result = ShapeModel_GetMatchingResultFromImage(ImageReduce, _ShapeModeDes, ref resultOfMacht, _ShapeModeDes.MatchingType);
                    if (result)
                    {
                        HOperatorSet.SetColor(ImageProcessWnd.hWindow, "green");
                        HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                        HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, $"Part OK({((double)resultOfMacht.TupleScore).ToString("F2")})", "image", CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol, "forest green", "true");
                        HD_DisplayShapeMatchingResults(ImageProcessWnd.hWindow, _ShapeModeDes.TupleModelID, "green",
                                                                                resultOfMacht.TupleRow,
                                                                                resultOfMacht.TupleCol,
                                                                                resultOfMacht.TupleAngle,
                                                                                1,
                                                                                1,
                                                                                0);
                        CountOfMatch += _MatchingResult.TupleRow.Length;

                        CheckResult[nIndexAOI].ResultStatus = true;
                        CheckResult[nIndexAOI].Result = resultOfMacht.TupleRow.Length;
                        if (CheckResult[nIndexAOI].MatchingResult == null)
                            CheckResult[nIndexAOI].MatchingResult = new MATCHINGRESULT_DES();

                        CheckResult[nIndexAOI].MatchingResult.TupleRow = resultOfMacht.TupleRow.Clone();
                        CheckResult[nIndexAOI].MatchingResult.TupleCol = resultOfMacht.TupleCol.Clone();
                        CheckResult[nIndexAOI].MatchingResult.TupleAngle = resultOfMacht.TupleAngle.Clone();
                    }
                    else
                    {
                        FinallyResult = false;
                        CheckResult[nIndexAOI].Result = -998;
                        CheckResult[nIndexAOI].ResultStatus = false;

                        HOperatorSet.SetColor(ImageProcessWnd.hWindow, "red");
                        HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                        HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part NG", "image", CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol, "red", "true");
                    }
                }

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
