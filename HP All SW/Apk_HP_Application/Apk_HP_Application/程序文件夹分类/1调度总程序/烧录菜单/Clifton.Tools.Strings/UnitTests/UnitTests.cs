/*
Copyright (c) 2005, Marc Clifton
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list
  of conditions and the following disclaimer. 

* Redistributions in binary form must reproduce the above copyright notice, this 
  list of conditions and the following disclaimer in the documentation and/or other
  materials provided with the distribution. 
 
* Neither the name of MyXaml nor the names of its contributors may be
  used to endorse or promote products derived from this software without specific
  prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using Vts.UnitTest;

namespace Clifton.Tools.Strings
{
	[TestFixture]
	public class StringHelperUnitTests
	{
		[Test]
		public void LeftOfFound()
		{
			string ret=StringHelpers.LeftOf("abcdefg", 'd');
			Assertion.Assert(ret=="abc", "Unexpected return.");
		}

		[Test]
		public void LeftOfNotFound()
		{
			string ret=StringHelpers.LeftOf("abcdefg", 'h');
			Assertion.Assert(ret=="abcdefg", "Unexpected return.");
		}

		[Test]
		public void LeftOfNFound()
		{
			string ret=StringHelpers.LeftOf("abcdecfg", 'c', 2);
			Assertion.Assert(ret=="abcde", "Unexpected return.");
		}

		[Test]
		public void LeftOfNNotFound()
		{
			string ret=StringHelpers.LeftOf("abcdecfg", 'c', 3);
			Assertion.Assert(ret=="abcdecfg", "Unexpected return.");
		}

		[Test]
		public void RightOfFound()
		{
			string ret=StringHelpers.RightOf("abcdefg", 'd');
			Assertion.Assert(ret=="efg", "Unexpected return.");
		}

		[Test]
		public void RightOfNotFound()
		{
			string ret=StringHelpers.RightOf("abcdefg", 'h');
			Assertion.Assert(ret=="", "Unexpected return.");
		}

		[Test]
		public void RightOfNFound()
		{
			string ret=StringHelpers.RightOf("abcdecfg", 'c', 2);
			Assertion.Assert(ret=="fg", "Unexpected return.");
		}

		[Test]
		public void RightOfNNotFound()
		{
			string ret=StringHelpers.RightOf("abcdecfg", 'c', 3);
			Assertion.Assert(ret=="", "Unexpected return.");
		}

		[Test]
		public void LeftOfRightmostOfFound()
		{
			string ret=StringHelpers.LeftOfRightmostOf("abcdefg123d456", 'd');
			Assertion.Assert(ret=="abcdefg123", "Unexpected return.");
		}

		[Test]
		public void RightOfRightmostOfFound()
		{
			string ret=StringHelpers.RightOfRightmostOf("abcdefg123d456", 'd');
			Assertion.Assert(ret=="456", "Unexpected return.");
		}

		[Test]
		public void BetweenFound()
		{
			string ret=StringHelpers.Between("abcdefg", 'b', 'e');
			Assertion.Assert(ret=="cd", "Unexpected return.");
		}

		[Test]
		public void BetweenEndNotFound()
		{
			string ret=StringHelpers.Between("habcdefg", 'b', 'h');
			Assertion.Assert(ret=="", "Unexpected return.");
		}

		[Test]
		public void BetweenStartNotFound()
		{
			string ret=StringHelpers.Between("abcdefg", 'h', 'e');
			Assertion.Assert(ret=="", "Unexpected return.");
		}

		[Test]
		public void Count()
		{
			int n=StringHelpers.Count("abcdafgai", 'a');
			Assertion.Assert(n==3, "Unexpected count.");
		}

		[Test]
		public void CountZero()
		{
			int n=StringHelpers.Count("abcdafgai", 'z');
			Assertion.Assert(n==0, "Unexpected count.");
		}

		[Test]
		public void Rightmost()
		{
			char c=StringHelpers.Rightmost("abcdefg");
			Assertion.Assert(c=='g', "Unexepected return.");
		}

		[Test]
		public void RightmostOfNothing()
		{
			char c=StringHelpers.Rightmost("");
			Assertion.Assert(c=='\0', "Unexepected return.");
		}
	}
}
