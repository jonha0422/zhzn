﻿namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    partial class frm_FlashStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_FlashStation));
            this.gridFlashResult = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtFlashVer = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btmFalshCommand = new DevExpress.XtraEditors.SimpleButton();
            this.chkVerifyAfterFlash = new DevExpress.XtraEditors.CheckEdit();
            this.barFlashProgress = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lable8889 = new DevExpress.XtraEditors.LabelControl();
            this.LEDMachine = new HslCommunication.Controls.UserLantern();
            this.LEDPLC = new HslCommunication.Controls.UserLantern();
            this.btmSetFlashType = new DevExpress.XtraEditors.SimpleButton();
            this.cmbFlashTypeDesc = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btmVerifyFlash = new DevExpress.XtraEditors.SimpleButton();
            this.btmPLCTrigger = new DevExpress.XtraEditors.SimpleButton();
            this.txtD1000 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.richTextMsgOut = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.LED_FLSAHSTATE = new HslCommunication.Controls.UserLantern();
            this.txtReceivedData = new System.Windows.Forms.TextBox();
            this.btmReconnectFlashMachine = new DevExpress.XtraEditors.SimpleButton();
            this.txtTrayID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtPCBSN = new DevExpress.XtraEditors.TextEdit();
            this.cmbFlasedList = new System.Windows.Forms.ListBox();
            this.txtPCBSN2 = new DevExpress.XtraEditors.TextEdit();
            this.txtTrayID2 = new DevExpress.XtraEditors.TextEdit();
            this.复位条码数据 = new DevExpress.XtraEditors.SimpleButton();
            this.txtTypeMenue = new DevExpress.XtraEditors.TextEdit();
            this.txtFalshType = new System.Windows.Forms.TextBox();
            this.chk切换行例数据 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridFlashResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFlashVer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVerifyAfterFlash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barFlashProgress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFlashTypeDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1000.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeMenue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk切换行例数据.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridFlashResult
            // 
            this.gridFlashResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFlashResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridFlashResult.ColumnHeadersHeight = 24;
            this.gridFlashResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridFlashResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20});
            this.gridFlashResult.Location = new System.Drawing.Point(3, 176);
            this.gridFlashResult.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridFlashResult.Name = "gridFlashResult";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFlashResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridFlashResult.RowTemplate.Height = 23;
            this.gridFlashResult.Size = new System.Drawing.Size(720, 462);
            this.gridFlashResult.TabIndex = 0;
            // 
            // Column1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "1";
            this.Column1.Name = "Column1";
            this.Column1.Width = 80;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "2";
            this.Column2.Name = "Column2";
            this.Column2.Width = 80;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "3";
            this.Column3.Name = "Column3";
            this.Column3.Width = 80;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "4";
            this.Column4.Name = "Column4";
            this.Column4.Width = 80;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "5";
            this.Column5.Name = "Column5";
            this.Column5.Width = 80;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "6";
            this.Column6.Name = "Column6";
            this.Column6.Width = 80;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "7";
            this.Column7.Name = "Column7";
            this.Column7.Width = 80;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "8";
            this.Column8.Name = "Column8";
            this.Column8.Width = 80;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "9";
            this.Column9.Name = "Column9";
            this.Column9.Width = 80;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "10";
            this.Column10.Name = "Column10";
            this.Column10.Width = 80;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "11";
            this.Column11.Name = "Column11";
            this.Column11.Width = 80;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "12";
            this.Column12.Name = "Column12";
            this.Column12.Width = 80;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "13";
            this.Column13.Name = "Column13";
            this.Column13.Width = 80;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "14";
            this.Column14.Name = "Column14";
            this.Column14.Width = 80;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "15";
            this.Column15.Name = "Column15";
            this.Column15.Width = 80;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "16";
            this.Column16.Name = "Column16";
            this.Column16.Width = 80;
            // 
            // Column17
            // 
            this.Column17.HeaderText = "17";
            this.Column17.Name = "Column17";
            this.Column17.Width = 80;
            // 
            // Column18
            // 
            this.Column18.HeaderText = "18";
            this.Column18.Name = "Column18";
            this.Column18.Width = 80;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "19";
            this.Column19.Name = "Column19";
            this.Column19.Width = 80;
            // 
            // Column20
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column20.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column20.HeaderText = "20";
            this.Column20.Name = "Column20";
            this.Column20.Width = 80;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.AppearanceDisabled.Options.UseFont = true;
            this.labelControl20.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.AppearanceHovered.Options.UseFont = true;
            this.labelControl20.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.AppearancePressed.Options.UseFont = true;
            this.labelControl20.Location = new System.Drawing.Point(6, 87);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(72, 17);
            this.labelControl20.TabIndex = 58;
            this.labelControl20.Text = "当前烧录型号";
            // 
            // txtFlashVer
            // 
            this.txtFlashVer.EditValue = "";
            this.txtFlashVer.Location = new System.Drawing.Point(84, 113);
            this.txtFlashVer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFlashVer.Name = "txtFlashVer";
            this.txtFlashVer.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtFlashVer.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtFlashVer.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtFlashVer.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtFlashVer.Properties.Appearance.Options.UseBackColor = true;
            this.txtFlashVer.Properties.Appearance.Options.UseBorderColor = true;
            this.txtFlashVer.Properties.Appearance.Options.UseFont = true;
            this.txtFlashVer.Properties.Appearance.Options.UseForeColor = true;
            this.txtFlashVer.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFlashVer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFlashVer.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtFlashVer.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtFlashVer.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtFlashVer.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtFlashVer.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFlashVer.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtFlashVer.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtFlashVer.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtFlashVer.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtFlashVer.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFlashVer.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtFlashVer.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtFlashVer.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtFlashVer.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtFlashVer.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFlashVer.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtFlashVer.Size = new System.Drawing.Size(410, 28);
            this.txtFlashVer.TabIndex = 61;
            this.txtFlashVer.EditValueChanged += new System.EventHandler(this.txtFlashVer_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearanceDisabled.Options.UseFont = true;
            this.labelControl1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearanceHovered.Options.UseFont = true;
            this.labelControl1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl1.AppearancePressed.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(18, 118);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 17);
            this.labelControl1.TabIndex = 60;
            this.labelControl1.Text = "日期及版本";
            // 
            // btmFalshCommand
            // 
            this.btmFalshCommand.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmFalshCommand.Appearance.Options.UseFont = true;
            this.btmFalshCommand.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmFalshCommand.AppearanceDisabled.Options.UseFont = true;
            this.btmFalshCommand.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmFalshCommand.AppearanceHovered.Options.UseFont = true;
            this.btmFalshCommand.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmFalshCommand.AppearancePressed.Options.UseFont = true;
            this.btmFalshCommand.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btmFalshCommand.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmFalshCommand.ImageOptions.Image")));
            this.btmFalshCommand.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.TopCenter;
            this.btmFalshCommand.Location = new System.Drawing.Point(735, 5);
            this.btmFalshCommand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmFalshCommand.Name = "btmFalshCommand";
            this.btmFalshCommand.Size = new System.Drawing.Size(123, 108);
            this.btmFalshCommand.TabIndex = 62;
            this.btmFalshCommand.Text = "烧录";
            this.btmFalshCommand.Click += new System.EventHandler(this.btmFalshCommand_Click);
            // 
            // chkVerifyAfterFlash
            // 
            this.chkVerifyAfterFlash.Location = new System.Drawing.Point(869, 9);
            this.chkVerifyAfterFlash.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkVerifyAfterFlash.Name = "chkVerifyAfterFlash";
            this.chkVerifyAfterFlash.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVerifyAfterFlash.Properties.Appearance.Options.UseFont = true;
            this.chkVerifyAfterFlash.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkVerifyAfterFlash.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkVerifyAfterFlash.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkVerifyAfterFlash.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkVerifyAfterFlash.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkVerifyAfterFlash.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkVerifyAfterFlash.Properties.Caption = "烧录并验证";
            this.chkVerifyAfterFlash.Size = new System.Drawing.Size(87, 21);
            this.chkVerifyAfterFlash.TabIndex = 63;
            // 
            // barFlashProgress
            // 
            this.barFlashProgress.EditValue = "等待烧录";
            this.barFlashProgress.Location = new System.Drawing.Point(3, 144);
            this.barFlashProgress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barFlashProgress.Name = "barFlashProgress";
            this.barFlashProgress.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barFlashProgress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.barFlashProgress.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.barFlashProgress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.barFlashProgress.Properties.ProgressAnimationMode = DevExpress.Utils.Drawing.ProgressAnimationMode.Cycle;
            this.barFlashProgress.Properties.ShowTitle = true;
            this.barFlashProgress.Size = new System.Drawing.Size(1090, 28);
            this.barFlashProgress.TabIndex = 64;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearanceDisabled.Options.UseFont = true;
            this.labelControl2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearanceHovered.Options.UseFont = true;
            this.labelControl2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearancePressed.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(1033, 10);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 17);
            this.labelControl2.TabIndex = 66;
            this.labelControl2.Text = "烧录线程";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 250;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearanceDisabled.Options.UseFont = true;
            this.labelControl3.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearanceHovered.Options.UseFont = true;
            this.labelControl3.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearancePressed.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(1033, 67);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 17);
            this.labelControl3.TabIndex = 70;
            this.labelControl3.Text = "烧录机连接";
            // 
            // lable8889
            // 
            this.lable8889.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable8889.Appearance.Options.UseFont = true;
            this.lable8889.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lable8889.AppearanceDisabled.Options.UseFont = true;
            this.lable8889.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lable8889.AppearanceHovered.Options.UseFont = true;
            this.lable8889.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lable8889.AppearancePressed.Options.UseFont = true;
            this.lable8889.Location = new System.Drawing.Point(1033, 37);
            this.lable8889.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lable8889.Name = "lable8889";
            this.lable8889.Size = new System.Drawing.Size(49, 17);
            this.lable8889.TabIndex = 69;
            this.lable8889.Text = "PLC 连接";
            // 
            // LEDMachine
            // 
            this.LEDMachine.BackColor = System.Drawing.Color.Transparent;
            this.LEDMachine.Location = new System.Drawing.Point(1003, 63);
            this.LEDMachine.Name = "LEDMachine";
            this.LEDMachine.Size = new System.Drawing.Size(24, 24);
            this.LEDMachine.TabIndex = 195;
            // 
            // LEDPLC
            // 
            this.LEDPLC.BackColor = System.Drawing.Color.Transparent;
            this.LEDPLC.Location = new System.Drawing.Point(1003, 34);
            this.LEDPLC.Name = "LEDPLC";
            this.LEDPLC.Size = new System.Drawing.Size(24, 24);
            this.LEDPLC.TabIndex = 196;
            // 
            // btmSetFlashType
            // 
            this.btmSetFlashType.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmSetFlashType.Appearance.Options.UseFont = true;
            this.btmSetFlashType.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetFlashType.AppearanceDisabled.Options.UseFont = true;
            this.btmSetFlashType.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetFlashType.AppearanceHovered.Options.UseFont = true;
            this.btmSetFlashType.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmSetFlashType.AppearancePressed.Options.UseFont = true;
            this.btmSetFlashType.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmSetFlashType.ImageOptions.Image")));
            this.btmSetFlashType.Location = new System.Drawing.Point(615, 7);
            this.btmSetFlashType.LookAndFeel.SkinName = "Office 2013";
            this.btmSetFlashType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmSetFlashType.Name = "btmSetFlashType";
            this.btmSetFlashType.Size = new System.Drawing.Size(113, 51);
            this.btmSetFlashType.TabIndex = 71;
            this.btmSetFlashType.Text = "设置烧录型号";
            this.btmSetFlashType.Click += new System.EventHandler(this.btmSetFlashType_Click);
            // 
            // cmbFlashTypeDesc
            // 
            this.cmbFlashTypeDesc.Location = new System.Drawing.Point(84, 82);
            this.cmbFlashTypeDesc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbFlashTypeDesc.Name = "cmbFlashTypeDesc";
            this.cmbFlashTypeDesc.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmbFlashTypeDesc.Properties.Appearance.Options.UseFont = true;
            this.cmbFlashTypeDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbFlashTypeDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cmbFlashTypeDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbFlashTypeDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbFlashTypeDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbFlashTypeDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.cmbFlashTypeDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cmbFlashTypeDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cmbFlashTypeDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbFlashTypeDesc.Size = new System.Drawing.Size(410, 24);
            this.cmbFlashTypeDesc.TabIndex = 72;
            this.cmbFlashTypeDesc.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // btmVerifyFlash
            // 
            this.btmVerifyFlash.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmVerifyFlash.Appearance.Options.UseFont = true;
            this.btmVerifyFlash.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmVerifyFlash.AppearanceDisabled.Options.UseFont = true;
            this.btmVerifyFlash.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmVerifyFlash.AppearanceHovered.Options.UseFont = true;
            this.btmVerifyFlash.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmVerifyFlash.AppearancePressed.Options.UseFont = true;
            this.btmVerifyFlash.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmVerifyFlash.ImageOptions.Image")));
            this.btmVerifyFlash.Location = new System.Drawing.Point(616, 62);
            this.btmVerifyFlash.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmVerifyFlash.Name = "btmVerifyFlash";
            this.btmVerifyFlash.Size = new System.Drawing.Size(113, 51);
            this.btmVerifyFlash.TabIndex = 73;
            this.btmVerifyFlash.Text = "验证烧录数据";
            this.btmVerifyFlash.Click += new System.EventHandler(this.btmVerifyFlash_Click);
            // 
            // btmPLCTrigger
            // 
            this.btmPLCTrigger.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmPLCTrigger.Appearance.Options.UseFont = true;
            this.btmPLCTrigger.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmPLCTrigger.AppearanceDisabled.Options.UseFont = true;
            this.btmPLCTrigger.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmPLCTrigger.AppearanceHovered.Options.UseFont = true;
            this.btmPLCTrigger.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmPLCTrigger.AppearancePressed.Options.UseFont = true;
            this.btmPLCTrigger.Location = new System.Drawing.Point(861, 87);
            this.btmPLCTrigger.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmPLCTrigger.Name = "btmPLCTrigger";
            this.btmPLCTrigger.Size = new System.Drawing.Size(113, 31);
            this.btmPLCTrigger.TabIndex = 74;
            this.btmPLCTrigger.Text = "PLC手动触发烧录";
            this.btmPLCTrigger.Click += new System.EventHandler(this.btmPLCTrigger_Click);
            // 
            // txtD1000
            // 
            this.txtD1000.EditValue = "250";
            this.txtD1000.Location = new System.Drawing.Point(935, 32);
            this.txtD1000.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1000.Name = "txtD1000";
            this.txtD1000.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1000.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1000.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1000.Properties.Appearance.Options.UseFont = true;
            this.txtD1000.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1000.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.ReadOnly = true;
            this.txtD1000.Size = new System.Drawing.Size(47, 22);
            this.txtD1000.TabIndex = 127;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.Options.UseBackColor = true;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl18.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl18.Location = new System.Drawing.Point(870, 34);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(62, 17);
            this.labelControl18.TabIndex = 128;
            this.labelControl18.Text = "烧录超时(s)";
            // 
            // richTextMsgOut
            // 
            this.richTextMsgOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextMsgOut.Location = new System.Drawing.Point(27, 651);
            this.richTextMsgOut.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.richTextMsgOut.Name = "richTextMsgOut";
            this.richTextMsgOut.Size = new System.Drawing.Size(479, 257);
            this.richTextMsgOut.TabIndex = 193;
            this.richTextMsgOut.Text = "";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.LightSalmon;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(3, 651);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 254);
            this.label10.TabIndex = 194;
            this.label10.Text = "             运行信息输出";
            // 
            // LED_FLSAHSTATE
            // 
            this.LED_FLSAHSTATE.BackColor = System.Drawing.Color.Transparent;
            this.LED_FLSAHSTATE.Location = new System.Drawing.Point(1003, 7);
            this.LED_FLSAHSTATE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_FLSAHSTATE.Name = "LED_FLSAHSTATE";
            this.LED_FLSAHSTATE.Size = new System.Drawing.Size(24, 24);
            this.LED_FLSAHSTATE.TabIndex = 197;
            // 
            // txtReceivedData
            // 
            this.txtReceivedData.Location = new System.Drawing.Point(512, 722);
            this.txtReceivedData.Multiline = true;
            this.txtReceivedData.Name = "txtReceivedData";
            this.txtReceivedData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtReceivedData.Size = new System.Drawing.Size(581, 186);
            this.txtReceivedData.TabIndex = 198;
            // 
            // btmReconnectFlashMachine
            // 
            this.btmReconnectFlashMachine.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmReconnectFlashMachine.Appearance.Options.UseFont = true;
            this.btmReconnectFlashMachine.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReconnectFlashMachine.AppearanceDisabled.Options.UseFont = true;
            this.btmReconnectFlashMachine.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReconnectFlashMachine.AppearanceHovered.Options.UseFont = true;
            this.btmReconnectFlashMachine.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmReconnectFlashMachine.AppearancePressed.Options.UseFont = true;
            this.btmReconnectFlashMachine.Location = new System.Drawing.Point(980, 87);
            this.btmReconnectFlashMachine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmReconnectFlashMachine.Name = "btmReconnectFlashMachine";
            this.btmReconnectFlashMachine.Size = new System.Drawing.Size(113, 31);
            this.btmReconnectFlashMachine.TabIndex = 199;
            this.btmReconnectFlashMachine.Text = "重连烧录机";
            this.btmReconnectFlashMachine.Click += new System.EventHandler(this.btmReconnectFlashMachine_Click);
            // 
            // txtTrayID
            // 
            this.txtTrayID.EditValue = "";
            this.txtTrayID.Location = new System.Drawing.Point(84, 45);
            this.txtTrayID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTrayID.Name = "txtTrayID";
            this.txtTrayID.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTrayID.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTrayID.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTrayID.Properties.Appearance.Options.UseBackColor = true;
            this.txtTrayID.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTrayID.Properties.Appearance.Options.UseFont = true;
            this.txtTrayID.Properties.Appearance.Options.UseForeColor = true;
            this.txtTrayID.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTrayID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTrayID.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTrayID.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTrayID.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Size = new System.Drawing.Size(202, 28);
            this.txtTrayID.TabIndex = 202;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl4.AppearanceDisabled.Options.UseFont = true;
            this.labelControl4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl4.AppearanceHovered.Options.UseFont = true;
            this.labelControl4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl4.AppearancePressed.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(32, 49);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(48, 17);
            this.labelControl4.TabIndex = 201;
            this.labelControl4.Text = "载板条码";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceDisabled.Options.UseFont = true;
            this.labelControl5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceHovered.Options.UseFont = true;
            this.labelControl5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearancePressed.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(20, 15);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(60, 17);
            this.labelControl5.TabIndex = 200;
            this.labelControl5.Text = "产品序例号";
            // 
            // txtPCBSN
            // 
            this.txtPCBSN.EditValue = "";
            this.txtPCBSN.Location = new System.Drawing.Point(84, 10);
            this.txtPCBSN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBSN.Name = "txtPCBSN";
            this.txtPCBSN.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBSN.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBSN.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBSN.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseFont = true;
            this.txtPCBSN.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Size = new System.Drawing.Size(202, 28);
            this.txtPCBSN.TabIndex = 203;
            // 
            // cmbFlasedList
            // 
            this.cmbFlasedList.FormattingEnabled = true;
            this.cmbFlasedList.ItemHeight = 17;
            this.cmbFlasedList.Location = new System.Drawing.Point(729, 175);
            this.cmbFlasedList.Name = "cmbFlasedList";
            this.cmbFlasedList.Size = new System.Drawing.Size(364, 463);
            this.cmbFlasedList.TabIndex = 204;
            // 
            // txtPCBSN2
            // 
            this.txtPCBSN2.EditValue = "";
            this.txtPCBSN2.Location = new System.Drawing.Point(292, 10);
            this.txtPCBSN2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBSN2.Name = "txtPCBSN2";
            this.txtPCBSN2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBSN2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBSN2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBSN2.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseFont = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPCBSN2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Size = new System.Drawing.Size(202, 28);
            this.txtPCBSN2.TabIndex = 205;
            // 
            // txtTrayID2
            // 
            this.txtTrayID2.EditValue = "";
            this.txtTrayID2.Location = new System.Drawing.Point(292, 45);
            this.txtTrayID2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTrayID2.Name = "txtTrayID2";
            this.txtTrayID2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTrayID2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTrayID2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTrayID2.Properties.Appearance.Options.UseBackColor = true;
            this.txtTrayID2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTrayID2.Properties.Appearance.Options.UseFont = true;
            this.txtTrayID2.Properties.Appearance.Options.UseForeColor = true;
            this.txtTrayID2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTrayID2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Size = new System.Drawing.Size(202, 28);
            this.txtTrayID2.TabIndex = 206;
            // 
            // 复位条码数据
            // 
            this.复位条码数据.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.复位条码数据.Appearance.Options.UseFont = true;
            this.复位条码数据.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.复位条码数据.AppearanceDisabled.Options.UseFont = true;
            this.复位条码数据.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.复位条码数据.AppearanceHovered.Options.UseFont = true;
            this.复位条码数据.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.复位条码数据.AppearancePressed.Options.UseFont = true;
            this.复位条码数据.Location = new System.Drawing.Point(500, 10);
            this.复位条码数据.Name = "复位条码数据";
            this.复位条码数据.Size = new System.Drawing.Size(111, 63);
            this.复位条码数据.TabIndex = 207;
            this.复位条码数据.Text = "复位条码数据";
            this.复位条码数据.Click += new System.EventHandler(this.复位条码数据_Click);
            // 
            // txtTypeMenue
            // 
            this.txtTypeMenue.EditValue = "";
            this.txtTypeMenue.Location = new System.Drawing.Point(500, 82);
            this.txtTypeMenue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTypeMenue.Name = "txtTypeMenue";
            this.txtTypeMenue.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTypeMenue.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTypeMenue.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTypeMenue.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTypeMenue.Properties.Appearance.Options.UseBackColor = true;
            this.txtTypeMenue.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTypeMenue.Properties.Appearance.Options.UseFont = true;
            this.txtTypeMenue.Properties.Appearance.Options.UseForeColor = true;
            this.txtTypeMenue.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTypeMenue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTypeMenue.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTypeMenue.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTypeMenue.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTypeMenue.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTypeMenue.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTypeMenue.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTypeMenue.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTypeMenue.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTypeMenue.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTypeMenue.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTypeMenue.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTypeMenue.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTypeMenue.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTypeMenue.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTypeMenue.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTypeMenue.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTypeMenue.Size = new System.Drawing.Size(108, 24);
            this.txtTypeMenue.TabIndex = 208;
            // 
            // txtFalshType
            // 
            this.txtFalshType.Location = new System.Drawing.Point(512, 651);
            this.txtFalshType.Multiline = true;
            this.txtFalshType.Name = "txtFalshType";
            this.txtFalshType.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFalshType.Size = new System.Drawing.Size(581, 65);
            this.txtFalshType.TabIndex = 209;
            // 
            // chk切换行例数据
            // 
            this.chk切换行例数据.Location = new System.Drawing.Point(870, 61);
            this.chk切换行例数据.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chk切换行例数据.Name = "chk切换行例数据";
            this.chk切换行例数据.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.chk切换行例数据.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk切换行例数据.Properties.Appearance.Options.UseBackColor = true;
            this.chk切换行例数据.Properties.Appearance.Options.UseFont = true;
            this.chk切换行例数据.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chk切换行例数据.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chk切换行例数据.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chk切换行例数据.Properties.AppearanceFocused.Options.UseFont = true;
            this.chk切换行例数据.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chk切换行例数据.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chk切换行例数据.Properties.Caption = "切换行例数据";
            this.chk切换行例数据.Size = new System.Drawing.Size(112, 21);
            this.chk切换行例数据.TabIndex = 210;
            this.chk切换行例数据.CheckedChanged += new System.EventHandler(this.chk切换行例数据_CheckedChanged);
            // 
            // frm_FlashStation
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 911);
            this.Controls.Add(this.chk切换行例数据);
            this.Controls.Add(this.txtFalshType);
            this.Controls.Add(this.txtTypeMenue);
            this.Controls.Add(this.复位条码数据);
            this.Controls.Add(this.txtTrayID2);
            this.Controls.Add(this.txtPCBSN2);
            this.Controls.Add(this.cmbFlasedList);
            this.Controls.Add(this.txtPCBSN);
            this.Controls.Add(this.txtTrayID);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.btmReconnectFlashMachine);
            this.Controls.Add(this.txtReceivedData);
            this.Controls.Add(this.LED_FLSAHSTATE);
            this.Controls.Add(this.richTextMsgOut);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtD1000);
            this.Controls.Add(this.labelControl18);
            this.Controls.Add(this.btmPLCTrigger);
            this.Controls.Add(this.btmVerifyFlash);
            this.Controls.Add(this.cmbFlashTypeDesc);
            this.Controls.Add(this.btmSetFlashType);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.lable8889);
            this.Controls.Add(this.LEDMachine);
            this.Controls.Add(this.LEDPLC);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.barFlashProgress);
            this.Controls.Add(this.chkVerifyAfterFlash);
            this.Controls.Add(this.btmFalshCommand);
            this.Controls.Add(this.txtFlashVer);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl20);
            this.Controls.Add(this.gridFlashResult);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_FlashStation";
            this.Text = "烧录结果信息";
            this.Load += new System.EventHandler(this.frm_FlashResult_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridFlashResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFlashVer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkVerifyAfterFlash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barFlashProgress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFlashTypeDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1000.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeMenue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk切换行例数据.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridFlashResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtFlashVer;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btmFalshCommand;
        private DevExpress.XtraEditors.CheckEdit chkVerifyAfterFlash;
        private DevExpress.XtraEditors.MarqueeProgressBarControl barFlashProgress;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lable8889;
        private HslCommunication.Controls.UserLantern LEDMachine;
        private HslCommunication.Controls.UserLantern LEDPLC;
        private DevExpress.XtraEditors.SimpleButton btmSetFlashType;
        private DevExpress.XtraEditors.ComboBoxEdit cmbFlashTypeDesc;
        private DevExpress.XtraEditors.SimpleButton btmVerifyFlash;
        private DevExpress.XtraEditors.SimpleButton btmPLCTrigger;
        private DevExpress.XtraEditors.TextEdit txtD1000;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private System.Windows.Forms.RichTextBox richTextMsgOut;
        private System.Windows.Forms.Label label10;
        private HslCommunication.Controls.UserLantern LED_FLSAHSTATE;
        private System.Windows.Forms.TextBox txtReceivedData;
        private DevExpress.XtraEditors.SimpleButton btmReconnectFlashMachine;
        private DevExpress.XtraEditors.TextEdit txtTrayID;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtPCBSN;
        private System.Windows.Forms.ListBox cmbFlasedList;
        private DevExpress.XtraEditors.TextEdit txtPCBSN2;
        private DevExpress.XtraEditors.TextEdit txtTrayID2;
        private DevExpress.XtraEditors.SimpleButton 复位条码数据;
        private DevExpress.XtraEditors.TextEdit txtTypeMenue;
        private System.Windows.Forms.TextBox txtFalshType;
        private DevExpress.XtraEditors.CheckEdit chk切换行例数据;
    }
}