﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitMQLib
{
    public class RabbitMQConfigSection 
    {
        /// <summary>
        /// 获取或设置服务器IP

        public string ServerIp { get; set; }

        /// <summary>
        /// 获取或设置服务器端口号
        /// </summary>
        [ConfigurationProperty("ServerPort", DefaultValue = 5672, IsRequired = true)]
        public int ServerPort { get; set; }

        /// <summary>
        /// 获取或设置连接的帐号
        /// </summary>
        [ConfigurationProperty("UserName", DefaultValue = "guest", IsRequired = true)]
        public string UserName { get; set; }
        

        /// <summary>
        /// 获取或设置连接的密码
        /// </summary>
        [ConfigurationProperty("Password", DefaultValue = "guest", IsRequired = true)]
        public string Password { get; set; }


        /// <summary>
        /// 获取或设置连接池中的连接心跳检测间隔时间，默认10秒
        /// </summary>
        [ConfigurationProperty("HeartBeatSecond", DefaultValue = 10, IsRequired = false)]
        public int HeartBeatSecond { get; set; }



        /// <summary>
        /// 获取或设置连接命名
        /// </summary>
        [ConfigurationProperty("ConnectionName", DefaultValue = "", IsRequired = false)]
        public string ConnectionName { get; set; }

        public RabbitMQConfigSection()
        {
            ServerIp = "127.0.0.1";
            ServerPort = 5672;
            UserName = "guest";
            Password = "guest";
            HeartBeatSecond = 10;
            ConnectionName = "";
        }
    }
    public class RabbitMQPool : IDisposable
    {
        /// <summary>
        /// 根据服务器信息，存储所有的连接池，避免同一服务器创建多个连接
        /// </summary>
        private static readonly Dictionary<string, RabbitMQPool> _pools;

        /// <summary>
        /// 默认连接池名
        /// </summary>
        private static readonly string _defaultKey = "default";


        #region 静态构造函数 与 私有构造函数
        /// <summary>
        /// 创建默认的消息队列实例
        /// </summary>
        static RabbitMQPool()
        {
            WaitPrefetchCount = 5;
            _pools = new Dictionary<string, RabbitMQPool>(1);
        }
        /// <summary>
        /// 队列池构造函数
        /// </summary>
        /// <param name="config">服务器配置信息</param>
        private RabbitMQPool(RabbitMQConfigSection config)
        {
            WaitPrefetchCount = 5;
            Init(config);
        }

        #endregion

        #region 非公开方法


        /// <summary>
        ///  连接服务器所需参数
        /// </summary>
        private RabbitMQConfigSection _config;

        /// <summary>
        /// 当前连接使用的参数
        /// </summary>
        public RabbitMQConfigSection Config{
           get{
               return _config;
           }
            set{
                _config=value;
            }
        } 

        // private readonly Random _random = new Random();
        private IConnection _connection;

        /// <summary>
        /// 供重载构造函数初始化连接参数用
        /// </summary>
        /// <param name="config"></param>
        protected void Init(RabbitMQConfigSection config)
        {
            this._config = config;
            _connection = CreateNewConnection(_config);

            // 启动连接检测线程, AutomaticRecoveryEnabled 会自动恢复连接，所以不去检测了
            //ThreadPool.UnsafeQueueUserWorkItem(CheckConnectionAlive, null);
        }

        /// <summary>
        /// 检测当前连接是否存活的方法
        /// </summary>
        /// <param name="state"></param>
        protected void CheckConnectionAlive(object state)
        {
            var sleepSecond = _config.HeartBeatSecond;
            while (true)
            {
                try
                {
                    using (var session = _connection.CreateModel())
                    {
                        session.ExchangeDeclare("__Heart", ExchangeType.Fanout, true);
                    }
                }
                catch (Exception exp1)
                {
                    OutputMsg("RabbitMQ 旧连接已断开：" + exp1.Message);
                    try
                    {
                        _connection = CreateNewConnection(_config);
                    }
                    catch (Exception exp2)
                    {
                        OutputMsg("RabbitMQ 连接创建失败：" + exp2.Message);
                    }
                }
                Thread.Sleep(sleepSecond);
            }
            // ReSharper disable once FunctionNeverReturns
        }


        static RabbitMQConfigSection ParseConnectStr(string constr)
        {
            string serverIp = "127.0.0.1";
            int serverPort = 5672;
            string userName = "guest";
            string password = null;
            string connName = "";

            int idx = constr.IndexOf("//", StringComparison.OrdinalIgnoreCase);
            if (idx >= 0)
            {
                constr = constr.Substring(idx + 2);
            }
            var arr = constr.Split(':', '@');
            if (arr.Length == 4)
            {
                userName = arr[0];
                password = arr[1];
                serverIp = arr[2];
                var arrPortAndName = arr[3].Split('/');
                if (!int.TryParse(arrPortAndName[0], out serverPort))
                    serverPort = 5672;
                if (arrPortAndName.Length > 1)
                    connName = arrPortAndName[1];
            }
            else if (arr.Length == 2)
            {
                serverIp = arr[0];
                var arrPortAndName = arr[1].Split('/');
                if (!int.TryParse(arrPortAndName[0], out serverPort))
                    serverPort = 5672;
                if (arrPortAndName.Length > 1)
                    connName = arrPortAndName[1];
            }
            return new RabbitMQConfigSection
            {
                ServerIp = serverIp,
                ServerPort = serverPort,
                Password = password,
                UserName = userName,
                ConnectionName = connName
            };
        }

        /// <summary>
        /// 创建新连接，并放入池中
        /// </summary>
        /// <returns></returns>
        static IConnection CreateNewConnection(RabbitMQConfigSection config)
        {
            try
            {
                var heartSecond = Convert.ToUInt16(config.HeartBeatSecond);
                if (heartSecond < 5)
                {
                    heartSecond = 10;
                }
                ConnectionFactory connectionFactory = new ConnectionFactory
                {
                    HostName = config.ServerIp,
                    Port = config.ServerPort,
                    UserName = config.UserName,
                    Password = config.Password,
                    //Socket read timeout is twice the hearbeat
                    RequestedHeartbeat = heartSecond,
                    AutomaticRecoveryEnabled = true,
                };

                var name = string.IsNullOrEmpty(config.ConnectionName) ? "noName" : config.ConnectionName;
                var connection = connectionFactory.CreateConnection(name);
                return connection;
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 输出调试信息
        /// </summary>
        /// <param name="msg"></param>
        [Conditional("DEBUG")]
        protected static void OutputMsg(string msg)
        {
#if DEBUG
            Console.WriteLine(msg);
#endif
        }



        /// <summary>
        /// 把对象序列化为二进制
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        private static byte[] SerializToBytes<T>(T data, Encoding encoding = null)
        {
            var result = SerializToStr(data);
            if (string.IsNullOrEmpty(result))
            {
                return null;
            }
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            return encoding.GetBytes(result);
        }
        /// <summary>
        /// 把对象序列化为字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string SerializToStr<T>(T data)
        {
            if (data == null)
            {
                return string.Empty;
            }
            var type = typeof(T);
            if (type == typeof(string))
            {
                return data as string;
            }
            if (type == typeof(Guid))
            {
                return data.ToString();
            }
            if (type == typeof(byte[]))
            {
                // ReSharper disable once AssignNullToNotNullAttribute
                return Convert.ToBase64String(data as byte[]);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(data);
        }

        /// <summary>
        /// 从二进制反序列化为对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        static T DeSerializFromBytes<T>(byte[] data, Encoding encoding = null)
        {
            if (data == null || data.Length == 0)
            {
                return default(T);
            }
            //if (typeof(T) == typeof(byte[]))
            //{
            //    return (T)(object)data;
            //}
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            return DeSerializFromStr<T>(encoding.GetString(data));
        }

        /// <summary>
        /// 从字符串反序列化为对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        static T DeSerializFromStr<T>(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return default(T);
            }
            var type = typeof(T);
            if (type == typeof(string))
            {
                return (T)(object)str;
            }
            if (type == typeof(Guid))
            {
                Guid ret;
                Guid.TryParse(str, out  ret);
                return (T)(object)ret;
            }
            if (type == typeof(byte[]))
            {
                return (T)(object)Convert.FromBase64String(str);
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str);
        }

        #endregion


        #region 静态方法 ，暴露给外部用的获取连接池实例

        /// <summary>
        /// 根据Config配置，获取默认实例.
        /// 注：配置里的密码必须是明文，暂不支持加密
        /// </summary>
        public static RabbitMQPool Default
        {
            get
            {
                RabbitMQPool defaultPool;
                if (!_pools.TryGetValue(_defaultKey, out defaultPool))
                {
                    RabbitMQConfigSection config=(RabbitMQConfigSection)ConfigurationManager.GetSection("RabbitMQConfigSection");
                    // 按ConfigSection方式配置
                    if (config!=null)
                    {
                        defaultPool = new RabbitMQPool(config);
                        _pools[_defaultKey] = defaultPool;
                    }
                    else
                    {

                        var mqConnstr = ConfigurationManager.AppSettings["DefaultRabbitMqConn"];
                        if (!string.IsNullOrEmpty(mqConnstr))
                        {
                            config = ParseConnectStr(mqConnstr);
                            config.HeartBeatSecond = 10;

                            defaultPool = new RabbitMQPool(config);
                            _pools[_defaultKey] = defaultPool;
                        }
                    }
                    if (defaultPool == null)
                    {
                        throw new ConfigurationErrorsException("必添加DefaultRabbitMqConn配置或RabbitMQConfigSection子节点");
                    }
                }
                return defaultPool;
            }
        }

        /// <summary> 
        /// 传入一个连接串的队列池构造函数
        /// </summary>
        /// <param name="constr">格式参考：amqp://mike:mike123@10.2.0.174:5672/connectionName
        /// 注：连接串里的password必须是明文密码</param>
        /// <param name="heartBeatSecond">mq心跳时长，默认10秒</param>
        /// <param name="connectionName">mq连接的命名</param>
        /// <returns></returns>
        public static RabbitMQPool CreatePool(string constr, int heartBeatSecond = 10, string connectionName = "")
        {
            if (string.IsNullOrEmpty(constr))
            {
                throw new ArgumentException("参数不能为空");
            }
            var config = ParseConnectStr(constr);
            config.HeartBeatSecond = heartBeatSecond;
            if (!string.IsNullOrEmpty(connectionName))
                config.ConnectionName = connectionName;

            RabbitMQPool ret;
            string key = config.ServerIp + ":" + config.ServerPort.ToString() + ":" + config.UserName + ":" +
                         config.Password + ":" + heartBeatSecond.ToString();
            if (!_pools.TryGetValue(key, out ret))
            {
                lock (_pools)
                {
                    if (!_pools.TryGetValue(key, out ret))
                    {
                        ret = new RabbitMQPool(config);
                        _pools[key] = ret;
                    }
                }
            }
            return ret;
        }
        /// <summary> 
        /// 传入一个连接串的队列池构造函数
        /// </summary>
        /// <param name="constr">格式参考：amqp://mike:mike123@10.2.0.174:5672/connectionName
        /// 注：连接串里的password必须是明文密码</param>
        /// <param name="heartBeatSecond">mq心跳时长，默认10秒</param>
        /// <param name="connectionName">mq连接的命名</param>
        /// <returns></returns>
        public static RabbitMQPool CreatePool(RabbitMQConfigSection config, int heartBeatSecond = 10, string connectionName = "")
        {
            
            RabbitMQPool ret;
            string key = config.ServerIp + ":" + config.ServerPort.ToString() + ":" + config.UserName + ":" +
                         config.Password + ":" + heartBeatSecond.ToString();
            if (!_pools.TryGetValue(key, out ret))
            {
                lock (_pools)
                {
                    if (!_pools.TryGetValue(key, out ret))
                    {
                        ret = new RabbitMQPool(config);
                        _pools[key] = ret;
                    }
                }
            }
            return ret;
        }
        #endregion

        #region 实例方法，实例创建队列和发送接收消息的方法
        ///
        /// 
        public bool isOpen()
        {
            if (_connection == null || !_connection.IsOpen)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 获取一个可用连接，并返回一个可以出入队的通道
        /// </summary>
        /// <returns></returns>
        public IModel GetChannel()
        {
            try
            {
                if (_connection == null || !_connection.IsOpen)
                {
                    _connection = CreateNewConnection(_config);
                    // throw new ApplicationException("MQ连接异常");
                }
                return _connection.CreateModel();
            }
            catch
            {
                return null;
            }
        }


        #region 定义交换器和队列
        /// <summary>
        /// 定义Exchange(如果没有Exchange，消息无法发布入队)
        /// 可以在Application_Start中调用此方法，也可以直接通过RabbitMQ的WEB控制台预先定义好
        /// </summary>
        /// <param name="exchange">要定义的交换器名</param>
        /// <param name="exchangeType">fanout：广播消息给Exchage绑定的所有队列；
        /// direct：直接转发给RouteKey指定的队列；
        /// topic：转发给匹配主题的队列；
        /// headers：尚未研究</param>
        /// <param name="durable">是否持久化</param>
        /// <param name="autoDelete">是否自动删除.
        /// 注：设置为true时，当有Queue或其它Exchange绑定到这个Exchange上后，
        ///     在这个Exchange的所有绑定都取消后，会自动删除这个Exchange</param>
        /// <param name="arguments">其它参数</param>
        public int ExchangeDeclare(string exchange, string exchangeType = "fanout", bool durable = false,
            bool autoDelete = false, IDictionary<string, object> arguments = null)
        {
            try
            {
                if (string.IsNullOrEmpty(exchange))
                {
                    throw new ArgumentException("Exchange名不能为空", nameof(exchange));
                }

                using (IModel channel = GetChannel())
                {
                    if (channel != null)
                    {
                        channel.ExchangeDeclare(exchange, exchangeType, durable, autoDelete, arguments);
                        return 0;
                    }

                }
                return 1;
            }
            catch
            {
                return 2;
            }
        }


        /// <summary>
        /// 定义Queue，如果存在Exchange，则绑定到它。
        /// 可以在Application_Start中调用此方法，也可以直接通过RabbitMQ的WEB控制台预先定义好
        /// </summary>
        /// <param name="queue">要定义的队列名</param>
        /// <param name="exchange">要定义的交换器名，为空时不绑定</param>
        /// <param name="durable">是否持久化</param>
        /// <param name="messageTtl">消息生存时长，毫秒，默认值0，无限时长</param>
        /// <param name="messageMaxLen">消息队列最大长度，默认值0，无际长度</param>
        /// <param name="autoDeleteTime">自动删除时间(ms)，在该时间段内如果没有被重新声明，且没有调用过get命令，将会删除</param>
        /// <param name="routingKey">绑定到队列上的路由Key，可选。特殊业务中可能需要通过路由Key来归类不同的队列</param>
        /// <param name="deadExchange">设置了messageTtl时，过期的死信要转发到的exg</param>
        /// <param name="deadRoutingKey">设置了messageTtl时，过期的死信要转发的routing-key</param>
        public void QueueDeclareAndBind(string queue, string exchange = "", bool durable = false, 
            int messageTtl = 0, int messageMaxLen = 0, int autoDeleteTime = 0, string routingKey = null,
            string deadExchange = "", string deadRoutingKey = "")
        {
            if (string.IsNullOrEmpty(queue))
            {
                throw new ArgumentException("Queue名不能为空", "queue");
            }

            using (IModel channel = GetChannel())
            {
                QueueBind(channel, exchange, queue, durable, messageTtl, messageMaxLen,
                    autoDeleteTime, routingKey, deadExchange, deadRoutingKey);
            }
        }


        /// <summary>
        /// 定义Exchange(如果没有Exchange，消息无法入队)，
        /// 如果提供的Queue名，则定义并绑定Queue。
        /// 可以在Application_Start中调用此方法，也可以直接通过RabbitMQ的WEB控制台预先定义好
        /// </summary>
        /// <param name="exchange">要定义的交换器名</param>
        /// <param name="queue">要定义和绑定到交换器的队列名，为空时不处理</param>
        /// <param name="exchangeType">fanout：广播消息给Exchage绑定的所有队列；
        /// direct：直接转发给RouteKey指定的队列；
        /// topic：转发给匹配主题的队列；
        /// headers：尚未研究</param>
        /// <param name="durable">是否持久化</param>
        /// <param name="messageTtl">消息生存时长，毫秒，默认值0，无限时长</param>
        /// <param name="messageMaxLen">消息队列最大长度，默认值0，无际长度</param>
        /// <param name="autoDeleteTime">自动删除时间(ms)，在该时间段内如果没有被重新声明，且没有调用过get命令，将会删除</param>
        /// <param name="routingKey">绑定到队列上的路由Key，可选。特殊业务中可能需要通过路由Key来归类不同的队列</param>
        /// <param name="deadExchange">设置了messageTtl时，过期的死信要转发到的exg</param>
        /// <param name="deadRoutingKey">设置了messageTtl时，过期的死信要转发的routing-key</param>
        public void QueueDeclare(string exchange, string queue = "", string exchangeType = "fanout", bool durable = false,
            int messageTtl = 0, int messageMaxLen = 0, int autoDeleteTime = 0, string routingKey = null,
            string deadExchange = "", string deadRoutingKey = "")
        {
            if (string.IsNullOrEmpty(exchange))
            {
                throw new ArgumentException("Exchange名不能为空", "exchange");
            }

            using (IModel channel = GetChannel())
            {
                OperationInterruptedException declareExp = null;
                try
                {
                    channel.ExchangeDeclare(exchange, exchangeType, durable);
                }
                catch (OperationInterruptedException exp)
                {
                    // Exchange已经存在，重新定义参数不相同时，会报错，错误信息举例：
                    // "The AMQP operation was interrupted: AMQP close-reason, initiated by Peer, code=406, 
                    //  text =\"PRECONDITION_FAILED - inequivalent arg 'type' for exchange 'xxx' in vhost '/': received 'fanout' but current is 'topic'\", 
                    //  classId =40, methodId=10, cause="
                    declareExp = exp;
                }

                if (!string.IsNullOrEmpty(queue))
                {
                    QueueBind(channel, exchange, queue, durable, messageTtl, messageMaxLen,
                        autoDeleteTime, routingKey, deadExchange, deadRoutingKey);
                }
                if (declareExp != null)
                {
                    // Exchange存在时，允许绑定后，再抛出此异常
                    throw declareExp;
                }
            }
        }


        /// <summary>
        /// 定义延迟队列和交换器
        /// 可以在Application_Start中调用此方法，也可以直接通过RabbitMQ的WEB控制台预先定义好
        /// </summary>
        /// <param name="exchange">要定义的交换器名</param>
        /// <param name="queue">要定义和绑定到交换器的队列名，为空时不处理</param>
        /// <param name="exchangeType">fanout：广播消息给Exchage绑定的所有队列；
        /// direct：直接转发给RouteKey指定的队列；
        /// topic：转发给匹配主题的队列；
        /// headers：尚未研究</param>
        /// <param name="durable">是否持久化</param>
        /// <param name="messageTtl">消息生存时长，毫秒，默认值300000，表示5分钟</param>
        /// <param name="messageMaxLen">消息队列最大长度，默认值100000个</param>
        /// <param name="autoDeleteTime">自动删除时间(ms)，在该时间段内如果没有被重新声明，且没有调用过get命令，将会删除</param>
        /// <param name="routingKey">绑定到队列上的路由Key，可选。特殊业务中可能需要通过路由Key来归类不同的队列</param>
        public void DelayQueueDeclare(string exchange, string queue = "", string exchangeType = "fanout", bool durable = false,
            int messageTtl = 300000, int messageMaxLen = 100000, int autoDeleteTime = 0, string routingKey = null)
        {
            // 设置2个队列a和b，a队列设置过期，没有消费者，过期后路由到b队列，b队列有消费者
            var deadExchange = exchange + ".DeadLetter";
            var deadRoutingKey = "";
            var deadQueue = "";
            if (!string.IsNullOrEmpty(queue))
            {
                deadQueue = queue + ".DeadLetter";
                deadRoutingKey = queue + ".DeadRoute";
            }
            
            // 设置队列a
            QueueDeclare(exchange, queue, exchangeType, durable, messageTtl, messageMaxLen, autoDeleteTime, "", deadExchange, deadRoutingKey);
            // 设置队列b
            QueueDeclare(deadExchange, deadQueue, exchangeType, durable, 0, 0, 0, deadRoutingKey);
        }

        /// <summary>
        /// 定义队列，并绑定交换器和队列
        /// </summary>
        private void QueueBind(IModel channel, string exchange, string queue = "", bool durable = false,
            int messageTtl = 0, int messageMaxLen = 0, int autoDeleteTime = 0, string routingKey = null,
            string deadExchange = "", string deadRoutingKey = "")
        {
            if (messageTtl != 0 && messageTtl < 1000)
                throw new ArgumentException("ttl必须大于1000毫秒", "messageTtl");
            if (messageMaxLen < 0)
                throw new ArgumentException("消息队列最大长度必须大于0","messageMaxLen");
            Dictionary<string, object> arg = null;
            if (messageTtl >= 1000)
            {
                arg = new Dictionary<string, object>();
                arg.Add("x-message-ttl", messageTtl);
            }
            if (messageMaxLen > 0)
            {
                arg = arg ?? new Dictionary<string, object>();
                arg.Add("x-max-length", messageMaxLen);
            }
            if (autoDeleteTime > 0)
            {
                arg = arg ?? new Dictionary<string, object>();
                arg.Add("x-expires", autoDeleteTime);
            }
            if (!string.IsNullOrWhiteSpace(deadExchange))
            {
                arg = arg ?? new Dictionary<string, object>();
                arg.Add("x-dead-letter-exchange", deadExchange);
                if (!string.IsNullOrWhiteSpace(deadRoutingKey))
                {
                    arg.Add("x-dead-letter-routing-key", deadRoutingKey);
                }
            }
            channel.QueueDeclare(queue, durable, false, false, arg);
            if (!string.IsNullOrEmpty(exchange))
                channel.QueueBind(queue, exchange, routingKey ?? string.Empty);
        }

        #endregion



        #region 生产消息

        /// <summary>
        /// 提交消息到指定的Exchage；
        /// 当无可用连接或提交消息失败时，抛出异常
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exchange">交换器名</param>
        /// <param name="message">消息对象，内部序列化为JSON后，再转字节数组</param>
        /// <param name="headers">消息的头信息</param>
        /// <param name="deliveryMode">是否持久化 1:不持久化 2：持久化</param>
        /// <param name="routingKey">路由的Key，可选。特殊业务中可能需要通过路由Key来归类不同的队列</param>
        public int PublishMessage<T>(string exchange, T message,
            Dictionary<string, object> headers = null, byte deliveryMode = 1, string routingKey = null)
        {
            var byteMessage = SerializToBytes(message);
            int suc=PublishMessage(exchange, byteMessage, headers, deliveryMode, routingKey);
            return suc;
        }


        /// <summary>
        /// 提交消息到指定的Exchage；
        /// 当无可用连接或提交消息失败时，抛出异常
        /// </summary>
        /// <param name="exchange">交换器名</param>
        /// <param name="byteMessage">具体的消息内容</param>
        /// <param name="headers">消息的头信息</param>
        /// <param name="deliveryMode">是否持久化 1:不持久化 2：持久化</param>
        /// <param name="routingKey">路由的Key，可选。特殊业务中可能需要通过路由Key来归类不同的队列</param>
        /// <exception cref="Exception">当连接不可用时会上抛异常</exception>
        private int PublishMessage(string exchange, byte[] byteMessage,
            Dictionary<string, object> headers = null, byte deliveryMode = 1, string routingKey = null)
        {
            if (string.IsNullOrEmpty(exchange))
            {
                throw new ArgumentException("ExChange不能为空", "exchange");
            }
           
            // 发布消息重试1次
            try
            {
                int suc=DoPublish(exchange,byteMessage,headers,deliveryMode,routingKey);
                return suc;
            }
            catch
            {
                Thread.Sleep(50);
                try
                {
                    int suc = DoPublish(exchange, byteMessage, headers, deliveryMode, routingKey);
                    return suc;
                }
                catch
                {
                    return 2;
                }
            }
        }
        
        private  int DoPublish(string exchange, byte[] byteMessage,
            Dictionary<string, object> headers = null, byte deliveryMode = 1, string routingKey = null)
            {
                try
                {
                    using (IModel channel = GetChannel())
                    {
                        if (channel == null)
                        {
                            return 1;
                        }
                        IBasicProperties prop = channel.CreateBasicProperties();
                        prop.DeliveryMode = deliveryMode;
                        prop.Headers = headers;
                        channel.BasicPublish(exchange, routingKey ?? string.Empty, prop, byteMessage);
                        return 0;
                    }
                }
                catch
                {
                    return 1;
                }
                return 1;
            }

        /// <summary>
        /// 提交消息到延迟队列Exchage；
        /// 当无可用连接或提交消息失败时，抛出异常
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exchange">交换器名</param>
        /// <param name="message">消息对象，内部序列化为JSON后，再转字节数组</param>
        /// <param name="headers">消息的头信息</param>
        /// <param name="deliveryMode">是否持久化 1:不持久化 2：持久化</param>
        /// <param name="routingKey">路由的Key，可选。特殊业务中可能需要通过路由Key来归类不同的队列</param>
        public void DelayPublish<T>(string exchange, T message,
            Dictionary<string, object> headers = null, byte deliveryMode = 1, string routingKey = null)
        {
            var byteMessage = SerializToBytes(message);
            PublishMessage(exchange, byteMessage, headers, deliveryMode, routingKey);
        }

        #endregion


        #region 消费消息相关

        /// <summary>
        /// 获得队列数据时的回调函数委托
        /// </summary>
        /// <param name="queueData">消息体</param>
        /// <param name="headers">消息的头信息</param>
        public delegate void QueueCallback(byte[] queueData, IDictionary<string, object> headers);
        /// <summary>
        /// 获得队列数据时的回调函数委托
        /// </summary>
        /// <param name="queueData">消息对象</param>
        /// <param name="headers">消息的头信息</param>
        public delegate void QueueCallback<in T>(T queueData, IDictionary<string, object> headers);

        /// <summary>
        /// 获取并处理队列数据出现异常时的回调函数
        /// </summary>
        /// <param name="exception"></param>
        public delegate void QueueErrorCallback(Exception exception);

        /// <summary>
        /// 处理消息异常的回调
        /// </summary>
        public static QueueErrorCallback WaitErrorCallback { get; set; }

        /// <summary>
        /// Qos设置，最多预取的消息数量，未处理完不继续接收消息
        /// </summary>
        public static ushort WaitPrefetchCount { get; set; } 

        ///// <summary>
        ///// 监听队列，并使用回调函数处理
        ///// </summary>
        ///// <param name="queue">队列名</param>
        ///// <param name="callback">获得队列数据时的回调函数</param>
        ///// <param name="errorCallback">获取并处理队列数据出现异常时的回调函数</param>
        //[Obsolete("请改用WaitQueue<T>方法")]
        //public void WaitQueue(string queue,
        //    QueueCallback callback, QueueErrorCallback errorCallback = null)
        //{
        //    if (string.IsNullOrEmpty(queue))
        //    {
        //        throw new ArgumentException("队列名不能为空", nameof(queue));
        //    }
        //    errorCallback = errorCallback ?? WaitErrorCallback;

        //    ThreadPool.UnsafeQueueUserWorkItem(state =>
        //    {
        //        try
        //        {
        //            // 要监听队列，所以不能关闭channel通道
        //            var channel = GetChannel();
        //            var consumer = new EventingBasicConsumer(channel);
        //            consumer.Received += (sender, e) =>
        //            {
        //                try
        //                {
        //                    callback?.Invoke(e.Body, e.BasicProperties.Headers);
        //                }
        //                catch (Exception exp)
        //                {
        //                    if (errorCallback == null)
        //                        throw;
        //                    errorCallback(exp);
        //                    //Console.WriteLine("队列接收处理出错：" + exp.ToString());
        //                }
        //            };
        //            channel.BasicConsume(queue, true, consumer);
        //        }
        //        catch (Exception exp)
        //        {
        //            if (errorCallback == null)
        //                throw;
        //            errorCallback(exp);
        //            //Console.WriteLine("队列接收处理出错：" + exp.ToString());
        //        }
        //    }, callback);
        //}


        /// <summary>
        /// 监听队列，并使用回调函数处理
        /// </summary>
        /// <param name="queue">队列名</param>
        /// <param name="callback">获得队列数据时的回调函数</param>
        /// <param name="errorCallback">获取并处理队列数据出现异常时的回调函数</param>
        /// <param name="prefetchCount">消息预取数量，默认5条，超过5个未ack时，不接收消息</param>
        /// <param name="useAsync">异步处理消息还是同步</param>
        public void WaitQueue(string queue,
            QueueCallback<string> callback, QueueErrorCallback errorCallback = null,
            ushort prefetchCount = 0, bool useAsync = false)
        {
            WaitQueue<string>(queue, callback, errorCallback, prefetchCount, useAsync);
        }


        /// <summary>
        /// 监听队列，并使用回调函数处理
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queue">队列名</param>
        /// <param name="callback">获得队列数据时的回调函数</param>
        /// <param name="errorCallback">获取并处理队列数据出现异常时的回调函数</param>
        /// <param name="prefetchCount">消息预取数量，默认5条，超过5个未ack时，不接收消息</param>
        /// <param name="useAsync">异步处理消息还是同步</param>
        public void WaitQueue<T>(string queue,
            QueueCallback<T> callback, QueueErrorCallback errorCallback = null,
            ushort prefetchCount = 0, bool useAsync = false)
        {
            if (string.IsNullOrEmpty(queue))
            {
                throw new ArgumentException("队列名不能为空", "queue");
            }
            errorCallback = errorCallback ?? WaitErrorCallback;
            if (prefetchCount <= 0)
            {
                prefetchCount = WaitPrefetchCount;
            }
            try
            {
                // 要监听队列，所以不能关闭channel通道
                var channel = GetChannel();
                channel.BasicQos(0, prefetchCount, false);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (sender, e) =>
                {
                    if (useAsync)
                    {
                        // ThreadPool.UnsafeQueueUserWorkItem(msgCallback, sender, e, callback, errorCallback);
                        msgCallback<T>(sender, e, callback, errorCallback);
                    }

                    else
                    {
                        msgCallback<T>(sender, e, callback, errorCallback);
                    }
                        
                };
                // 开启acknowledge机制，在接收事件里ack，配合qos进行流控
                channel.BasicConsume(queue, false, consumer);
            }
            catch (Exception exp)
            {
                if (errorCallback == null)
                    throw;
                errorCallback(exp);
                //Console.WriteLine("队列接收处理出错：" + exp.ToString());
            }
        }

        public void msgCallback<T>(object sender, BasicDeliverEventArgs e, QueueCallback<T> callback, QueueErrorCallback errorCallback = null)
        {
            try
            {
                if (callback == null)
                    return;
                var obj = DeSerializFromBytes<T>(e.Body);
                callback(obj, e.BasicProperties.Headers);
            }
            catch (Exception exp)
            {
                if (errorCallback == null)
                    throw;
                errorCallback(exp);
                //Console.WriteLine("队列接收处理出错：" + exp.ToString());
            }
            finally
            {
                try
                {
                    // 手工ack 参数2：multiple：是否批量.true:将一次性ack所有小于deliveryTag的消息
                    ((EventingBasicConsumer)sender).Model.BasicAck(e.DeliveryTag, false);
                }
                catch (Exception ackExp)
                {
                    if (errorCallback == null)
                        throw;
                    errorCallback(ackExp);
                }
            }
        }

        /// <summary>
        /// 监听队列，并使用回调函数处理
        /// </summary>
        /// <param name="queue">队列名</param>
        /// <param name="consumerNum">启动几个消费者</param>
        /// <param name="callback">获得队列数据时的回调函数</param>
        /// <param name="errorCallback">获取并处理队列数据出现异常时的回调函数</param>
        /// <param name="prefetchCount">每个消费者的消息预取数量，默认5条，超过5个未ack时，不接收消息</param>
        public void WaitQueue(string queue, int consumerNum,
            QueueCallback<string> callback, QueueErrorCallback errorCallback = null,
            ushort prefetchCount = 0)
        {
            if (consumerNum < 1)
            {
                throw new ArgumentException("消费者数量不能小于1");
            }
            for (var i = 0; i < consumerNum; i++)
            {
                WaitQueue(queue, callback, errorCallback, prefetchCount);
            }
        }

        /// <summary>
        /// 监听队列，并使用回调函数处理
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queue">队列名</param>
        /// <param name="consumerNum">启动几个消费者</param>
        /// <param name="callback">获得队列数据时的回调函数</param>
        /// <param name="errorCallback">获取并处理队列数据出现异常时的回调函数</param>
        /// <param name="prefetchCount">每个消费者的消息预取数量，默认5条，超过5个未ack时，不接收消息</param>
        public void WaitQueue<T>(string queue, int consumerNum,
            QueueCallback<T> callback, QueueErrorCallback errorCallback = null,
            ushort prefetchCount = 0)
        {
            if (consumerNum < 1)
            {
                throw new ArgumentException("消费者数量不能小于1");
            }
            for (var i = 0; i < consumerNum; i++)
            {
                WaitQueue(queue, callback, errorCallback, prefetchCount);
            }
        }


        /// <summary>
        /// 监听延迟队列，并使用回调函数处理
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queue">队列名</param>
        /// <param name="callback">获得队列数据时的回调函数</param>
        /// <param name="errorCallback">获取并处理队列数据出现异常时的回调函数</param>
        public void DelayWaitQueue<T>(string queue,
            QueueCallback<T> callback, QueueErrorCallback errorCallback = null)
        {
            if (string.IsNullOrEmpty(queue))
            {
                throw new ArgumentException("队列名不能为空", "queue");
            }
            //var deadRoutingKey = queue + ".DeadRoute";
            var deadQueue = queue + ".DeadLetter";
            WaitQueue<T>(deadQueue, callback, errorCallback);
        }


        #endregion

        /// <summary>
        /// 删除指定的队列, 并返回抛弃的消息数
        /// </summary>
        /// <param name="queue"></param>
        /// <returns></returns>
        public uint DelQueue(string queue)
        {
            using (IModel channel = GetChannel())
            {
                return channel.QueueDelete(queue);
            }
        }

        #endregion

        public void Dispose()
        {
            if(_connection!=null && _connection.IsOpen){
                _connection.Dispose();
            }
        }
    }
}
