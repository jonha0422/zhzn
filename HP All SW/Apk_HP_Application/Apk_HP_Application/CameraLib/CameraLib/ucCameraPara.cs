﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GenericLib;

namespace CamaeraLib
{
    public partial class ucCameraPara : UserControl
    {
        public CameraBase mCamera;
        public ucCameraPara()
        {
            InitializeComponent();
        }
        public void init(CameraBase aCam)
        {
            if (aCam != null)
            {
                mCamera = aCam;
                SynchronizationContextProvider.CreateSynchronizationContext(WindowsFormsSynchronizationContext.Current);
                Dictionary<string, string> mPara;
                mPara = PropertiesDataBindings.GetProperties<CameraConfig>(mCamera._CameraConfig);
                PropertiesDataBindings.UIDataBindings(mCamera._CameraConfig, this, mPara);
            }
        }
        private void ucCameraPara_Load(object sender, EventArgs e)
        {
            
            
        }

        private void Trigger_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Trigger.Text == "Off")
            {
                mCamera.SetTrigger(false);
            }
            else
            {
                mCamera.SetTrigger(true);
            }
        }

        private void TriggerSource_TextChanged(object sender, EventArgs e)
        {
            mCamera.SetTriggerSource(StringFunc.ToInt(TriggerSource.Text));
        }

        private void ExposureTime_TextChanged(object sender, EventArgs e)
        {
            mCamera.SetExposureTime(StringFunc.ToInt(ExposureTime.Text));
        }

        private void GainVal_TextChanged(object sender, EventArgs e)
        {
            mCamera.SetGainVal(StringFunc.ToInt(GainVal.Text));
        }
    }
}
