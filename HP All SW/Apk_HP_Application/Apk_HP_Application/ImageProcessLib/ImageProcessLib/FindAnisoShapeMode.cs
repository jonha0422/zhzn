﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionLib;
using VisionLib.ImageProcess;

namespace ImageProcessLib
{
    public class MarkPoint : IComparable
    {
        public double srcX { get; set; }
        public double srcY { get; set; }
        public double srcPhi { get; set; }
        public double desX { get; set; }
        public double desY { get; set; }
        public double unitX { get; set; }
        public double unitY { get; set; }
        public int CompareTo(Object aMarkPoint)
        {
            double D = this.srcY - ((MarkPoint)aMarkPoint).srcY;//先按照年龄排序  
                                                                // int kk = this.srcY.CompareTo(((MarkPoint)aMarkPoint).srcY);
                                                                // return kk;
            if (Math.Abs(D) < 50)
            {
                return this.srcX.CompareTo(((MarkPoint)aMarkPoint).srcX);
            }
            else
            {

                return this.srcY.CompareTo(((MarkPoint)aMarkPoint).srcY);



            }
            if (Math.Abs(D) < 5)
            {
                if (this.desX > ((MarkPoint)aMarkPoint).desX)
                {
                    // return 1;
                }

            }
            else
            {
                return 0;
            }
            return 1;
        }
    }
    public class FindAnisoShapeMode : ImageProcessBase
    {
        private HTuple hv_ModelID = null;
        public int numLevels { get; set; }
        [Browsable(true)]
        [Category("角度")]
        [DisplayName("开始角度")]
        public int angleStart { get; set; }
        [Browsable(true)]
        [Category("角度")]
        [DisplayName("角度范围")]
        public int angleExtent { get; set; }
        //  public int angleStep { get; set; }
        [Browsable(true)]
        [Category("形变R系数")]
        [DisplayName("最小系数")]
        public double scaleRMin { get; set; }
        [Browsable(true)]
        [Category("形变R系数")]
        [DisplayName("最大系数")]
        public double scaleRMax { get; set; }
        [Browsable(true)]
        [Category("形变C系数")]
        [DisplayName("最小系数")]
        public double scaleCMin { get; set; }
        [Browsable(true)]
        [Category("形变C系数")]
        [DisplayName("最大系数")]
        public double scaleCMax { get; set; }
        //public int scaleStep { get; set; }
        [Browsable(true)]
        [Category("对比度")]
        [DisplayName("轮廓对比度")]
        public int contrast { get; set; }
        [Browsable(true)]
        [Category("对比度")]
        [DisplayName("最小对比度")]
        public int minContrast { get; set; }
        [Browsable(true)]
        [Category("匹配参数")]
        [DisplayName("最小相识度")]
        public double MinScore { get; set; }
        [Browsable(true)]
        [Category("匹配参数")]
        [DisplayName("匹配个数")]
        public int NumMatches { get; set; }
        [Browsable(true)]
        [Category("匹配参数")]
        [DisplayName("MaxOverlap")]
        public double MaxOverlap { get; set; }
        [Browsable(true)]
        [Category("匹配参数")]
        [DisplayName("Greediness")]
        public double Greediness { get; set; }

        [Browsable(true)]
        [Category("二值话处理")]
        [DisplayName("模板启用")]
        public bool IsThresholdTemp { get; set; }//模板二值化

        [Browsable(true)]
        [Category("二值话处理")]
        [DisplayName("匹配启用")]
        public bool IsThresholdEnable { get; set; }
        [Browsable(true)]
        [Category("二值话处理")]
        [DisplayName("最小灰度")]

        public double MinGray { get; set; }//灰度值 ---HSV
        [Category("二值话处理")]
        [DisplayName("最大灰度")]
        public double MaxGray { get; set; }//灰度值 ---HSV

        [Category("二值话处理")]
        [DisplayName("最小面积")]
        public double MinArea { get; set; }//
        [Category("二值话处理")]
        [DisplayName("最大面积")]
        public double MaxArea { get; set; }//
        [Category("二值话处理")]
        [DisplayName("显示结果")]
        public bool isShowThreshold { get; set; }//
        [Category("圆")]
        [DisplayName("开运算半径")]
        [DefaultValue(3)]
        public double OpenRadius { get; set; }//
        [Category("圆")]
        [DisplayName("圆度")]
        public double Circularity { get; set; }//
        [Category("机械手")]
        [DisplayName("拍照后移动到结果点")]
        public bool isEpsonMove { get; set; }//
        [Category("机械手")]
        [DisplayName("机械手ID")]
        public int EpsonID { get; set; }//
        [Category("机械手")]
        [DisplayName("1吸取位X")]
        public double pX { get; set; }//
        [Category("机械手")]
        [DisplayName("2吸取位Y")]
        public double pY { get; set; }//
        [Category("机械手")]
        [DisplayName("3吸取位Z")]
        public double pZ { get; set; }//
        [Category("机械手")]
        [DisplayName("4吸取位U")]
        public double pU { get; set; }//


        [Category("9点标定")]
        [DisplayName("Mark点半径")]
        public double cRadius { get; set; }//
        public List<MarkPoint> mMarkPoint { get; set; }

        private HObject ho_Model;
        private HTuple HomMat2D = null;
        private bool isCalibration = false;
        public FindAnisoShapeMode()
        {
            cName = "机器人引导定位";
            numLevels = 5;

            angleStart = -45;
            angleExtent = 90;
            scaleRMin = 0.8;
            scaleRMax = 1.1;
            scaleCMin = 0.8;
            scaleCMax = 1.1;
            contrast = 40;
            minContrast = 10;

            MinScore = 0.5;
            NumMatches = 1;
            MaxOverlap = 0.5;
            Greediness = 0.5;
            SearchRoiCount = 5;

            OpenRadius = 1;
            Circularity = 0.5;
            mMarkPoint = new List<MarkPoint>();
            isEpson = true;
        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            create_model();
            return 0;
        }
        public int BlobAnalysis(HObject srcObj, out HObject desObj)
        {
            int suc = 0;
            HOperatorSet.Threshold(srcObj, out desObj, MinGray, MaxGray);
            if (ViewPort != null && isShowThreshold == true)
            {
                HOperatorSet.SetDraw(ViewPort.HalconWindow, "margin");
                HOperatorSet.SetColor(ViewPort.HalconWindow, "blue");
                HOperatorSet.DispObj(desObj, ViewPort.HalconWindow);
            }

            HOperatorSet.OpeningCircle(desObj, out desObj, OpenRadius);
            HOperatorSet.Connection(desObj, out desObj);
            HOperatorSet.SelectShape(desObj, out desObj, "circularity", "and", Circularity, 1.0);
            HOperatorSet.SelectShape(desObj, out desObj, "area", "and", MinArea, MaxArea);
            if (ViewPort != null && isShowThreshold == true)
            {
                HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                HOperatorSet.DispObj(desObj, ViewPort.HalconWindow);
            }
            HTuple width, height;
            HOperatorSet.GetImageSize(srcObj, out width, out height);
            HOperatorSet.RegionToBin(desObj, out desObj, 255, 0, width, height);


            return suc;
        }
        public int GetPointAffinedPosF(double srcX, double srcY, double centerX, double centerY, double angle, out double dstX, out double dstY)
        {
            double x = srcX - centerX;
            double y = srcY - centerY;
            //double pi_angle = MathFunc.DegToRad(angle);
            double pi_angle = 2 * Math.PI * angle / 360.0;
            dstX = x * Math.Cos(pi_angle) + y * Math.Sin(pi_angle) + centerX;
            dstY = -x * Math.Sin(pi_angle) + y * Math.Cos(pi_angle) + centerY;
            return 0;
        }
        public override int ImageProcess()
        {
            if (SrcObject == null)
            {
                return 0;
            }
            if (hv_ModelID == null)
            {
                return 0;
            }
            DesObject = null;
            HObject SearchObj = SrcObject;
            HTuple channels = 0;
            HOperatorSet.CountChannels(SearchObj, out channels);
            if (channels == 3)
            {
                HOperatorSet.Rgb1ToGray(SearchObj, out SearchObj);
            }
            if (IsThresholdEnable)
            {
                BlobAnalysis(SearchObj, out SearchObj);

            }
            ResultSta = true;
            mResult.Clear();
            for (int i = 0; i < SearchRoi.Count; i++)
            {
                if (SearchRoi.Count > 0)
                {
                    HRegion hReg = SearchRoi[i].getRegion();
                    // HOperatorSet.GenRectangle2(out RecRoi, hReg.Row, hReg.Column, ModelRoi[0].Phi, ModelRoi[0].Length1, ModelRoi[0].Length2);
                    //  HOperatorSet.ReduceDomain(ho_Image, hReg, out ho_ImageReduced);
                    HOperatorSet.ReduceDomain(SearchObj, hReg, out SearchObj);

                }
                HTuple hv_Row = null;
                HTuple hv_Column = null, hv_Angle = null, hv_RScale = null, hv_CScale = null;
                HTuple hv_Score = null;

                HOperatorSet.FindAnisoShapeModel(SearchObj, hv_ModelID, angleStart, angleExtent, scaleRMin, scaleRMax, scaleCMin, scaleCMax, MinScore, NumMatches, MaxOverlap, "least_squares", numLevels, Greediness,
                    out hv_Row, out hv_Column, out hv_Angle, out hv_RScale, out hv_CScale, out hv_Score);

                HTuple hv_I = null, hv_HomMat2DIdentity = new HTuple();
                HTuple hv_HomMat2DTranslate = new HTuple(), hv_HomMat2DRotate = new HTuple();
                HTuple hv_HomMat2DScale = new HTuple();
                HObject ho_ModelTrans;
                int drawID = 0;
                HOperatorSet.GenEmptyObj(out ho_ModelTrans);
                for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
                {
                    HOperatorSet.HomMat2dIdentity(out hv_HomMat2DIdentity);
                    HOperatorSet.HomMat2dTranslate(hv_HomMat2DIdentity, hv_Row.TupleSelect(hv_I),
                        hv_Column.TupleSelect(hv_I), out hv_HomMat2DTranslate);
                    HOperatorSet.HomMat2dRotate(hv_HomMat2DTranslate, hv_Angle.TupleSelect(hv_I),
                        hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out hv_HomMat2DRotate);
                    HOperatorSet.HomMat2dScale(hv_HomMat2DRotate, hv_RScale.TupleSelect(hv_I), hv_CScale.TupleSelect(
                        hv_I), hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out hv_HomMat2DScale);
                    ho_ModelTrans.Dispose();
                    HOperatorSet.AffineTransContourXld(ho_Model, out ho_ModelTrans, hv_HomMat2DScale);
                    if (ViewPort != null)
                    {
                        HOperatorSet.SetDraw(ViewPort.HalconWindow, "margin");
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                        HOperatorSet.DispXld(ho_ModelTrans, ViewPort.HalconWindow);
                    }
                    else if (DesObject != null)
                    {
                        if (drawID == 0)
                        {
                            drawID++;
                            HOperatorSet.PaintXld(ho_ModelTrans, DesObject, out DesObject, (((new HTuple(100)).TupleConcat(255)).TupleConcat(0)));
                        }
                        else
                        {
                            HOperatorSet.PaintXld(ho_ModelTrans, DesObject, out DesObject, (((new HTuple(100)).TupleConcat(255)).TupleConcat(0)));
                        }
                    }



                    Result aR = new Result();
                    aR.cX = hv_Column.TupleSelect(hv_I);
                    aR.cY = hv_Row.TupleSelect(hv_I);
                    aR.Phi = hv_Angle.TupleSelect(hv_I);
                    aR.isOk = true;

                    if (HomMat2D != null && isCalibration == false)
                    {
                        HRegion hReg = ModelRoi[0].getRegion();
                        HTuple qx, qy;
                        // affine_trans_point_2d(HomMat2D, aR.cX, aR.cY, out aR.cX, &y);
                        HOperatorSet.AffineTransPoint2d(HomMat2D, aR.cX, aR.cY, out qx, out qy);
                        aR.cX = qx;
                        aR.cY = qy;
                        aR.Phi = aR.Phi;// - ModelRoi[0].Phi; ;
                        aR.Phi = aR.Phi / 3.1415926 * 180;

                        HOperatorSet.AffineTransPoint2d(HomMat2D, ModelRoi[0].MidC, ModelRoi[0].MidR, out qx, out qy);
                        //HTuple W, H;
                        //HTuple midX, midY;//旋转中心
                        //HOperatorSet.GetImageSize(SrcObject, out W, out H);
                        //HOperatorSet.AffineTransPoint2d(HomMat2D, W/2, H/2, out midX, out midY);

                        double desX = 0, desY = 0;
                        desX = pX + aR.cX - qx;
                        desY = pY + aR.cY - qy;
                        GetPointAffinedPosF(desX, desY, aR.cX, aR.cY, -aR.Phi, out desX, out desY);
                        aR.cX = desX;
                        aR.cY = desY;
                        if (isEpsonMove == true)
                        {


                            double X, Y, Z, U;
                            string Hand = "";

                            // EpsonLib.EpsonMotion.Instance.EpsonGetHearPos(EpsonID, out X, out Y, out Z, out U, out Hand);

                            EpsonLib.EpsonMotion.Instance.MovetoPos(EpsonID, desX, desY, pZ, pU + aR.Phi, "R", 1);
                            //  EpsonLib.EpsonMotion.Instance.MovetoPos(EpsonID, aR.cX, aR.cY, 0, 0, "R", 1);
                        }
                    }

                    mResult.Add(aR);

                    //if (HDevWindowStack.IsOpen())
                    //{
                    //  HOperatorSet.DispObj(ho_ModelTrans, HDevWindowStack.GetActive());
                    //}
                }
                if ((int)(new HTuple(hv_Score.TupleLength())) > 0)
                {
                    // ResultSta = true;
                }
                else
                {
                    ResultSta = false;
                    if (drawID == 0)
                    {
                        drawID++;
                        // HOperatorSet.PaintRegion(SearchRoi[i].getRegion(), SrcObject, out DesObject, (((new HTuple(255)).TupleConcat(0)).TupleConcat(0)), new HTuple("margin"));

                    }
                    else
                    {
                        //HOperatorSet.PaintRegion(SearchRoi[i].getRegion(), DesObject, out DesObject, (((new HTuple(255)).TupleConcat(0)).TupleConcat(0)), new HTuple("margin"));

                    }

                }
            }

            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            FindAnisoShapeMode aFun = CoverLib.JsonCover.DeStrLSerialize<FindAnisoShapeMode>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<FindAnisoShapeMode>(this);
            return aFunPara;
        }

        public int create_model()
        {
            if (hv_ModelID != null && hv_ModelID.Type != HTupleType.EMPTY)
            {
                HOperatorSet.ClearShapeModel(hv_ModelID);
            }
            if (ModelRoi == null || ModelRoi.Count == 0)
            {
                return -1;

            }
            HObject RecRoi, ho_ImageReduced;
            HRegion hReg = ModelRoi[0].getRegion();
            // HOperatorSet.GenRectangle2(out RecRoi, hReg.Row, hReg.Column, ModelRoi[0].Phi, ModelRoi[0].Length1, ModelRoi[0].Length2);
            //  HOperatorSet.ReduceDomain(ho_Image, hReg, out ho_ImageReduced);
            HOperatorSet.ReduceDomain(TempImage, hReg, out ho_ImageReduced);
            HTuple channels = 0;
            HOperatorSet.CountChannels(ho_ImageReduced, out channels);
            if (channels == 3)
            {
                HOperatorSet.Rgb1ToGray(ho_ImageReduced, out ho_ImageReduced);
            }
            if (IsThresholdTemp)
            {
                BlobAnalysis(ho_ImageReduced, out ho_ImageReduced);
                HOperatorSet.ReduceDomain(ho_ImageReduced, hReg, out ho_ImageReduced);
            }
            HOperatorSet.CreateAnisoShapeModel(ho_ImageReduced, numLevels, (new HTuple(angleStart)).TupleRad()
                        , (new HTuple(angleExtent)).TupleRad(), "auto", scaleRMin, scaleRMax, "auto", scaleCMin, scaleCMax, "auto", "none", "ignore_global_polarity",
                        contrast, minContrast, out hv_ModelID);
            if (hv_ModelID.Type == HTupleType.EMPTY)
            {
                return -1;
            }
            HObject ho_ModelTrans;
            HTuple hv_Area = null, hv_RowRef = null;
            HTuple hv_ColumnRef = null, hv_HomMat2D = null, hv_Row = null;
            HTuple hv_Column = null, hv_Angle = null, hv_Scale = null;
            HOperatorSet.GenEmptyObj(out ho_Model);
            HOperatorSet.GenEmptyObj(out ho_ModelTrans);
            HOperatorSet.GetShapeModelContours(out ho_Model, hv_ModelID, 1);
            HOperatorSet.AreaCenter(ho_ModelTrans, out hv_Area, out hv_RowRef, out hv_ColumnRef);
            //HOperatorSet.VectorAngleToRigid(0, 0, 0, hv_RowRef, hv_ColumnRef, 0, out hv_HomMat2D);
            HOperatorSet.VectorAngleToRigid(0, 0, 0, ModelRoi[0].MidR, ModelRoi[0].MidC, 0, out hv_HomMat2D);
            ho_ModelTrans.Dispose();
            HOperatorSet.AffineTransContourXld(ho_Model, out ho_ModelTrans, hv_HomMat2D);

            // HOperatorSet.PaintXld(ho_ModelTrans, TempImage, out TempDesImage, (((new HTuple(0)).TupleConcat(255)).TupleConcat(0)));
            if (ViewPort != null)
            {
                HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                HOperatorSet.DispXld(ho_ModelTrans, ViewPort.HalconWindow);
            }
            CreatHomMat2d();
            // HOperatorSet
            return 0;
        }
        private int CreatHomMat2d()
        {
            if (mMarkPoint.Count > 2)
            {
                HTuple Px = new HTuple();
                HTuple Py = new HTuple();
                HTuple Qx = new HTuple();
                HTuple Qy = new HTuple();

                for (int i = 0; i < mMarkPoint.Count; i++)
                {
                    Px[i] = mMarkPoint[i].srcX;
                    Py[i] = mMarkPoint[i].srcY;
                    Qx[i] = mMarkPoint[i].desX;
                    Qy[i] = mMarkPoint[i].desY;
                }
                HOperatorSet.VectorToHomMat2d(Px, Py, Qx, Qy, out HomMat2D);
                //HTuple qx =0, qy=0;
                //for (int i = 0; i < mMarkPoint.Count; i++)
                //{
                //    HOperatorSet.AffineTransPoint2d(HomMat2D, mMarkPoint[i].srcX, mMarkPoint[i].srcY, out qx, out qy);
                //}
            }
            return 0;
        }
        public int calibrationBy9Point()
        {
            HObject Circle, Image, ImageModel, aModelContours;
            HTuple aModelID;
            HTuple hv_Row, hv_Column, hv_Angle, hv_RScale, hv_CScale, hv_Score;
            HOperatorSet.GenCircle(out Circle, cRadius + 5, cRadius + 5, cRadius);
            HOperatorSet.GenImageConst(out Image, "byte", cRadius * 2 + 30, cRadius * 2 + 30);
            HOperatorSet.PaintRegion(Circle, Image, out ImageModel, 128, "fill");
            if (ViewPort != null)
            {
                HOperatorSet.DispImage(ImageModel, ViewPort.HalconWindow);
            }
            HOperatorSet.CreateAnisoShapeModel(ImageModel, "auto", (new HTuple(-10)).TupleRad()
                        , (new HTuple(45)).TupleRad(), "auto", 0.7, 1.3, "auto", 0.7, 1.3, "auto", "none", "use_polarity", 30, 20, out aModelID);
            HOperatorSet.GenEmptyObj(out aModelContours);
            HOperatorSet.GetShapeModelContours(out aModelContours, aModelID, 1);

            int suc = CamaeraLib.CameraSystem.Instance.CameraList[CamID].OneShot();
            if (suc == 0)
            {
                CamaeraLib.Image aImage = CamaeraLib.CameraSystem.Instance.CameraList[CamID].GetLatestImage();
                HImage hImage;
                suc = VisionLib.ImageConvert.ImageTohImage(aImage, out hImage);
                if (suc == 0)
                {
                    SrcObject = hImage;
                    if (ViewPort != null)
                    {
                        HOperatorSet.DispImage(SrcObject, ViewPort.HalconWindow);
                    }

                    HOperatorSet.FindAnisoShapeModel(SrcObject, aModelID, (new HTuple(0)).TupleRad()
                        , (new HTuple(100)).TupleRad(), 0.7, 1.3, 0.7, 1.3, 0.5, 9, 0.5, "least_squares", 0, 0.8,
                    out hv_Row, out hv_Column, out hv_Angle, out hv_RScale, out hv_CScale, out hv_Score);

                    HTuple hv_I = null, hv_HomMat2DIdentity = new HTuple();
                    HTuple hv_HomMat2DTranslate = new HTuple(), hv_HomMat2DRotate = new HTuple();
                    HTuple hv_HomMat2DScale = new HTuple();
                    HObject ho_ModelTrans;
                    HOperatorSet.GenEmptyObj(out ho_ModelTrans);

                    //HOperatorSet.GenCircle(out Circle, hv_Row[0], hv_Column[0], cRadius);
                    //HOperatorSet.SortRegion(Circle, out Circle, "upper_left", "true", "row");
                    int id = 0;
                    HalconWind nHW = new HalconWind();
                    for (hv_I = 0; (int)hv_I <= (int)((new HTuple(hv_Score.TupleLength())) - 1); hv_I = (int)hv_I + 1)
                    {
                        HOperatorSet.HomMat2dIdentity(out hv_HomMat2DIdentity);
                        HOperatorSet.HomMat2dTranslate(hv_HomMat2DIdentity, hv_Row.TupleSelect(hv_I),
                            hv_Column.TupleSelect(hv_I), out hv_HomMat2DTranslate);
                        HOperatorSet.HomMat2dRotate(hv_HomMat2DTranslate, hv_Angle.TupleSelect(hv_I),
                            hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out hv_HomMat2DRotate);
                        HOperatorSet.HomMat2dScale(hv_HomMat2DRotate, hv_RScale.TupleSelect(hv_I), hv_CScale.TupleSelect(
                            hv_I), hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), out hv_HomMat2DScale);
                        ho_ModelTrans.Dispose();
                        HOperatorSet.AffineTransContourXld(aModelContours, out ho_ModelTrans, hv_HomMat2DScale);
                        if (ViewPort != null)
                        {
                            HOperatorSet.SetDraw(ViewPort.HalconWindow, "margin");
                            HOperatorSet.SetColor(ViewPort.HalconWindow, "red");
                            HOperatorSet.DispXld(ho_ModelTrans, ViewPort.HalconWindow);

                            id++;
                            // nHW.disp_message(ViewPort.HalconWindow, id.ToString(), "image", hv_Row.TupleSelect(hv_I), hv_Column.TupleSelect(hv_I), "green", "false");
                        }

                        MarkPoint aM = new MarkPoint();
                        aM.srcX = hv_Column.TupleSelect(hv_I);
                        aM.srcY = hv_Row.TupleSelect(hv_I);

                        mMarkPoint.Add(aM);

                    }
                    id = 0;
                    mMarkPoint.Sort();
                    //mMarkPoint.Sort(
                    //      delegate (MarkPoint M1, MarkPoint M2)
                    //      {
                    //          if (M1.srcY > M2.srcY)
                    //          {
                    //              return 1;
                    //          }
                    //          else
                    //          {
                    //              return 0;
                    //          }
                    //      });
                    for (int i = 0; i < mMarkPoint.Count; i++)
                    {
                        if (ViewPort != null)
                        {
                            id++;
                            nHW.disp_message(ViewPort.HalconWindow, id.ToString(), "image", mMarkPoint[i].srcY, mMarkPoint[i].srcX, "green", "false");
                        }
                    }
                }
            }
            return 0;
        }
        public override int calibration()//九点标定法
        {
            double X, Y, Z, U;
            string Hand = "";
            isCalibration = true;
            mMarkPoint.Clear();
            // EpsonLib.EpsonMotion.Instance.EpsonGetHearPos(EpsonID, out X, out Y, out Z, out U, out Hand);
            //calibrationByPoint(X, Y);

            calibrationBy9Point();

            isCalibration = false;
            //CreatHomMat2d();

            return 0;
        }
        public override int MoveToStartPos()
        {
            EpsonLib.EpsonMotion.Instance.MovetoPoint(0, 11);
            string revMsg = "";
            int suc = EpsonLib.EpsonMotion.Instance.sentComm(0, "GotoPhotoPos;6X1520", out revMsg, 10000);
            return 0;
        }
    }
}
