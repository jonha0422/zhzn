﻿namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    partial class frm_StatinInfoDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_StatinInfoDetail));
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.txtD1013 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1012 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1011 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1000 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1010 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1001 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1009 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1002 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1003 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1004 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1005 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1006 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1007 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1008 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtMESResult = new DevExpress.XtraEditors.TextEdit();
            this.txtPCBVituralSN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtTrayID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.txtStatus = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btmRestStatic = new DevExpress.XtraEditors.SimpleButton();
            this.txtFailedUnit = new DevExpress.XtraEditors.TextEdit();
            this.txtPassedUnit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txtUPH = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtFailed = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txtPassed = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.richTxt_Msg_Out = new System.Windows.Forms.RichTextBox();
            this.timer_StatusMonitor = new System.Windows.Forms.Timer(this.components);
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.txtMESResult2 = new DevExpress.XtraEditors.TextEdit();
            this.txtPCBVituralSN2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.txtTrayID2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.启动 = new DevExpress.XtraEditors.SimpleButton();
            this.错误清除 = new DevExpress.XtraEditors.SimpleButton();
            this.停止 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.启用禁用 = new DevExpress.XtraEditors.CheckButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.btmTriggerBD2 = new DevExpress.XtraEditors.SimpleButton();
            this.btmTri1gerBD2 = new DevExpress.XtraEditors.SimpleButton();
            this.richTxt_Msg_Out2 = new System.Windows.Forms.RichTextBox();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtD1213 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1200 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1207 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1208 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1210 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1203 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1209 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1202 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1212 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1204 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1211 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1201 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1205 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1206 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.txtD1313 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1300 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1307 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1308 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1310 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1303 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1309 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1302 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1312 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1304 = new DevExpress.XtraEditors.TextEdit();
            this.txtD1311 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1301 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1305 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.txtD1306 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.LEDPLC = new HslCommunication.Controls.UserLantern();
            this.LEDScanner1 = new HslCommunication.Controls.UserLantern();
            this.LEDScanner2 = new HslCommunication.Controls.UserLantern();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1013.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1012.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1011.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1000.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1010.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1001.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1009.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1002.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1003.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1004.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1005.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1006.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1007.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1008.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMESResult.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBVituralSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailedUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassedUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMESResult2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBVituralSN2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1213.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1200.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1207.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1208.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1210.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1203.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1209.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1202.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1212.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1204.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1211.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1201.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1205.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1206.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1313.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1300.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1307.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1308.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1310.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1303.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1309.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1302.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1312.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1304.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1311.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1301.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1305.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1306.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearanceDisabled.Options.UseFont = true;
            this.labelControl12.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearanceHovered.Options.UseFont = true;
            this.labelControl12.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearancePressed.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(48, 27);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(123, 17);
            this.labelControl12.TabIndex = 12;
            this.labelControl12.Text = "PLC/PC-Modbus 连接";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearanceDisabled.Options.UseFont = true;
            this.labelControl2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearanceHovered.Options.UseFont = true;
            this.labelControl2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl2.AppearancePressed.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(49, 113);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(79, 17);
            this.labelControl2.TabIndex = 14;
            this.labelControl2.Text = "条码枪2连接状";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearanceDisabled.Options.UseFont = true;
            this.labelControl3.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearanceHovered.Options.UseFont = true;
            this.labelControl3.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl3.AppearancePressed.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(49, 70);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(67, 17);
            this.labelControl3.TabIndex = 15;
            this.labelControl3.Text = "条码枪1连接";
            // 
            // groupControl5
            // 
            this.groupControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.Appearance.Options.UseFont = true;
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.Location = new System.Drawing.Point(213, 103);
            this.groupControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(177, 493);
            this.groupControl5.TabIndex = 99;
            this.groupControl5.Text = "PLC IO Monitor";
            // 
            // txtD1013
            // 
            this.txtD1013.Location = new System.Drawing.Point(129, 418);
            this.txtD1013.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1013.Name = "txtD1013";
            this.txtD1013.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1013.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1013.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1013.Properties.Appearance.Options.UseFont = true;
            this.txtD1013.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1013.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1013.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1013.Properties.ReadOnly = true;
            this.txtD1013.Size = new System.Drawing.Size(37, 22);
            this.txtD1013.TabIndex = 128;
            // 
            // txtD1012
            // 
            this.txtD1012.Location = new System.Drawing.Point(129, 386);
            this.txtD1012.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1012.Name = "txtD1012";
            this.txtD1012.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1012.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1012.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1012.Properties.Appearance.Options.UseFont = true;
            this.txtD1012.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1012.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1012.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1012.Properties.ReadOnly = true;
            this.txtD1012.Size = new System.Drawing.Size(37, 22);
            this.txtD1012.TabIndex = 127;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.Options.UseBackColor = true;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl18.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl18.Location = new System.Drawing.Point(59, 10);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(65, 17);
            this.labelControl18.TabIndex = 126;
            this.labelControl18.Text = "D1000 心跳";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl19.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl19.Location = new System.Drawing.Point(4, 420);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(120, 17);
            this.labelControl19.TabIndex = 125;
            this.labelControl19.Text = "D1013 条码2请求结果";
            // 
            // txtD1011
            // 
            this.txtD1011.Location = new System.Drawing.Point(129, 355);
            this.txtD1011.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1011.Name = "txtD1011";
            this.txtD1011.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1011.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1011.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1011.Properties.Appearance.Options.UseFont = true;
            this.txtD1011.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1011.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1011.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1011.Properties.ReadOnly = true;
            this.txtD1011.Size = new System.Drawing.Size(37, 22);
            this.txtD1011.TabIndex = 124;
            // 
            // txtD1000
            // 
            this.txtD1000.EditValue = "3";
            this.txtD1000.Location = new System.Drawing.Point(129, 7);
            this.txtD1000.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1000.Name = "txtD1000";
            this.txtD1000.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1000.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1000.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1000.Properties.Appearance.Options.UseFont = true;
            this.txtD1000.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1000.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtD1000.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1000.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1000.Properties.ReadOnly = true;
            this.txtD1000.Size = new System.Drawing.Size(37, 22);
            this.txtD1000.TabIndex = 101;
            // 
            // txtD1010
            // 
            this.txtD1010.Location = new System.Drawing.Point(129, 323);
            this.txtD1010.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1010.Name = "txtD1010";
            this.txtD1010.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1010.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1010.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1010.Properties.Appearance.Options.UseFont = true;
            this.txtD1010.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1010.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1010.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1010.Properties.ReadOnly = true;
            this.txtD1010.Size = new System.Drawing.Size(37, 22);
            this.txtD1010.TabIndex = 123;
            // 
            // txtD1001
            // 
            this.txtD1001.Location = new System.Drawing.Point(129, 39);
            this.txtD1001.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1001.Name = "txtD1001";
            this.txtD1001.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1001.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1001.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1001.Properties.Appearance.Options.UseFont = true;
            this.txtD1001.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1001.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1001.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1001.Properties.ReadOnly = true;
            this.txtD1001.Size = new System.Drawing.Size(37, 22);
            this.txtD1001.TabIndex = 102;
            // 
            // txtD1009
            // 
            this.txtD1009.Location = new System.Drawing.Point(129, 291);
            this.txtD1009.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1009.Name = "txtD1009";
            this.txtD1009.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtD1009.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1009.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1009.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1009.Properties.Appearance.Options.UseFont = true;
            this.txtD1009.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1009.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1009.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1009.Properties.ReadOnly = true;
            this.txtD1009.Size = new System.Drawing.Size(37, 22);
            this.txtD1009.TabIndex = 122;
            // 
            // txtD1002
            // 
            this.txtD1002.Location = new System.Drawing.Point(129, 70);
            this.txtD1002.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1002.Name = "txtD1002";
            this.txtD1002.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1002.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1002.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1002.Properties.Appearance.Options.UseFont = true;
            this.txtD1002.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1002.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1002.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1002.Properties.ReadOnly = true;
            this.txtD1002.Size = new System.Drawing.Size(37, 22);
            this.txtD1002.TabIndex = 103;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl11.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl11.Location = new System.Drawing.Point(4, 389);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(120, 17);
            this.labelControl11.TabIndex = 121;
            this.labelControl11.Text = "D1012 条码2请求完成";
            // 
            // txtD1003
            // 
            this.txtD1003.Location = new System.Drawing.Point(129, 102);
            this.txtD1003.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1003.Name = "txtD1003";
            this.txtD1003.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1003.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1003.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1003.Properties.Appearance.Options.UseFont = true;
            this.txtD1003.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1003.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1003.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1003.Properties.ReadOnly = true;
            this.txtD1003.Size = new System.Drawing.Size(37, 22);
            this.txtD1003.TabIndex = 104;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl5.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl5.Location = new System.Drawing.Point(4, 357);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(120, 17);
            this.labelControl5.TabIndex = 120;
            this.labelControl5.Text = "D1011 条码2请求触发";
            // 
            // txtD1004
            // 
            this.txtD1004.Location = new System.Drawing.Point(129, 134);
            this.txtD1004.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1004.Name = "txtD1004";
            this.txtD1004.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1004.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1004.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1004.Properties.Appearance.Options.UseFont = true;
            this.txtD1004.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1004.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1004.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1004.Properties.ReadOnly = true;
            this.txtD1004.Size = new System.Drawing.Size(37, 22);
            this.txtD1004.TabIndex = 105;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl13.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl13.Location = new System.Drawing.Point(4, 325);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(120, 17);
            this.labelControl13.TabIndex = 119;
            this.labelControl13.Text = "D1010 条码1请求结果";
            // 
            // txtD1005
            // 
            this.txtD1005.Location = new System.Drawing.Point(129, 165);
            this.txtD1005.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1005.Name = "txtD1005";
            this.txtD1005.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1005.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1005.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1005.Properties.Appearance.Options.UseFont = true;
            this.txtD1005.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1005.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1005.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1005.Properties.ReadOnly = true;
            this.txtD1005.Size = new System.Drawing.Size(37, 22);
            this.txtD1005.TabIndex = 106;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl14.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(4, 294);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(120, 17);
            this.labelControl14.TabIndex = 118;
            this.labelControl14.Text = "D1009 条码1请求完成";
            // 
            // txtD1006
            // 
            this.txtD1006.Location = new System.Drawing.Point(129, 197);
            this.txtD1006.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1006.Name = "txtD1006";
            this.txtD1006.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1006.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1006.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1006.Properties.Appearance.Options.UseFont = true;
            this.txtD1006.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1006.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1006.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1006.Properties.ReadOnly = true;
            this.txtD1006.Size = new System.Drawing.Size(37, 22);
            this.txtD1006.TabIndex = 107;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl7.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl7.Location = new System.Drawing.Point(4, 262);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(120, 17);
            this.labelControl7.TabIndex = 117;
            this.labelControl7.Text = "D1008 条码1请求触发";
            // 
            // txtD1007
            // 
            this.txtD1007.Location = new System.Drawing.Point(129, 228);
            this.txtD1007.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1007.Name = "txtD1007";
            this.txtD1007.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1007.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1007.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1007.Properties.Appearance.Options.UseFont = true;
            this.txtD1007.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1007.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1007.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1007.Properties.ReadOnly = true;
            this.txtD1007.Size = new System.Drawing.Size(37, 22);
            this.txtD1007.TabIndex = 108;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl8.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(30, 231);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(94, 17);
            this.labelControl8.TabIndex = 116;
            this.labelControl8.Text = "D1007 异常/错误";
            // 
            // txtD1008
            // 
            this.txtD1008.Location = new System.Drawing.Point(129, 260);
            this.txtD1008.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1008.Name = "txtD1008";
            this.txtD1008.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1008.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1008.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1008.Properties.Appearance.Options.UseFont = true;
            this.txtD1008.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1008.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1008.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1008.Properties.ReadOnly = true;
            this.txtD1008.Size = new System.Drawing.Size(37, 22);
            this.txtD1008.TabIndex = 109;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl9.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(35, 199);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(89, 17);
            this.labelControl9.TabIndex = 115;
            this.labelControl9.Text = "D1006 工作状态";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.Options.UseBackColor = true;
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl17.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl17.Location = new System.Drawing.Point(6, 41);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(118, 17);
            this.labelControl17.TabIndex = 110;
            this.labelControl17.Text = "D1001 禁用/启用工位";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseBackColor = true;
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl10.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl10.Location = new System.Drawing.Point(59, 168);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(65, 17);
            this.labelControl10.TabIndex = 114;
            this.labelControl10.Text = "D1005 停止";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl16.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl16.Location = new System.Drawing.Point(35, 73);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(89, 17);
            this.labelControl16.TabIndex = 111;
            this.labelControl16.Text = "D1002 准备信号";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl6.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl6.Location = new System.Drawing.Point(59, 136);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(65, 17);
            this.labelControl6.TabIndex = 113;
            this.labelControl6.Text = "D1004 复位";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl15.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl15.Location = new System.Drawing.Point(59, 104);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(65, 17);
            this.labelControl15.TabIndex = 112;
            this.labelControl15.Text = "D1003 启动";
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Location = new System.Drawing.Point(396, 103);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(223, 211);
            this.groupControl1.TabIndex = 100;
            this.groupControl1.Text = "PLC && PC && 条码枪 IP Address 连接设置";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.txtMESResult);
            this.groupControl2.Controls.Add(this.txtPCBVituralSN);
            this.groupControl2.Controls.Add(this.labelControl21);
            this.groupControl2.Controls.Add(this.txtTrayID);
            this.groupControl2.Controls.Add(this.labelControl22);
            this.groupControl2.Controls.Add(this.labelControl23);
            this.groupControl2.Location = new System.Drawing.Point(183, 111);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(250, 135);
            this.groupControl2.TabIndex = 101;
            this.groupControl2.Text = "条码1请求基本信息";
            // 
            // txtMESResult
            // 
            this.txtMESResult.EditValue = "NG:25468";
            this.txtMESResult.Location = new System.Drawing.Point(71, 94);
            this.txtMESResult.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtMESResult.Name = "txtMESResult";
            this.txtMESResult.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtMESResult.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtMESResult.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMESResult.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtMESResult.Properties.Appearance.Options.UseBackColor = true;
            this.txtMESResult.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMESResult.Properties.Appearance.Options.UseFont = true;
            this.txtMESResult.Properties.Appearance.Options.UseForeColor = true;
            this.txtMESResult.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMESResult.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMESResult.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtMESResult.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtMESResult.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMESResult.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtMESResult.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtMESResult.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMESResult.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtMESResult.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtMESResult.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult.Properties.ReadOnly = true;
            this.txtMESResult.Size = new System.Drawing.Size(174, 24);
            this.txtMESResult.TabIndex = 118;
            // 
            // txtPCBVituralSN
            // 
            this.txtPCBVituralSN.EditValue = "345672";
            this.txtPCBVituralSN.Location = new System.Drawing.Point(71, 63);
            this.txtPCBVituralSN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPCBVituralSN.Name = "txtPCBVituralSN";
            this.txtPCBVituralSN.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBVituralSN.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBVituralSN.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPCBVituralSN.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBVituralSN.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBVituralSN.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBVituralSN.Properties.Appearance.Options.UseFont = true;
            this.txtPCBVituralSN.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBVituralSN.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBVituralSN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBVituralSN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBVituralSN.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBVituralSN.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBVituralSN.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBVituralSN.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBVituralSN.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBVituralSN.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBVituralSN.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBVituralSN.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN.Properties.ReadOnly = true;
            this.txtPCBVituralSN.Size = new System.Drawing.Size(174, 24);
            this.txtPCBVituralSN.TabIndex = 117;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl21.AppearanceDisabled.Options.UseFont = true;
            this.labelControl21.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl21.AppearanceHovered.Options.UseFont = true;
            this.labelControl21.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl21.AppearancePressed.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(14, 98);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(51, 17);
            this.labelControl21.TabIndex = 116;
            this.labelControl21.Text = "校验结果:";
            // 
            // txtTrayID
            // 
            this.txtTrayID.EditValue = "1";
            this.txtTrayID.Location = new System.Drawing.Point(71, 33);
            this.txtTrayID.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTrayID.Name = "txtTrayID";
            this.txtTrayID.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTrayID.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTrayID.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrayID.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTrayID.Properties.Appearance.Options.UseBackColor = true;
            this.txtTrayID.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTrayID.Properties.Appearance.Options.UseFont = true;
            this.txtTrayID.Properties.Appearance.Options.UseForeColor = true;
            this.txtTrayID.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTrayID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTrayID.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTrayID.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTrayID.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTrayID.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID.Properties.ReadOnly = true;
            this.txtTrayID.Size = new System.Drawing.Size(174, 24);
            this.txtTrayID.TabIndex = 115;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl22.AppearanceDisabled.Options.UseFont = true;
            this.labelControl22.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl22.AppearanceHovered.Options.UseFont = true;
            this.labelControl22.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl22.AppearancePressed.Options.UseFont = true;
            this.labelControl22.Location = new System.Drawing.Point(14, 67);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(50, 17);
            this.labelControl22.TabIndex = 114;
            this.labelControl22.Text = "PCB编号:";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearanceDisabled.Options.UseFont = true;
            this.labelControl23.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearanceHovered.Options.UseFont = true;
            this.labelControl23.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearancePressed.Options.UseFont = true;
            this.labelControl23.Location = new System.Drawing.Point(14, 36);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(51, 17);
            this.labelControl23.TabIndex = 113;
            this.labelControl23.Text = "载板编号:";
            // 
            // txtStatus
            // 
            this.txtStatus.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.txtStatus.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtStatus.Appearance.Options.UseBackColor = true;
            this.txtStatus.Appearance.Options.UseFont = true;
            this.txtStatus.Appearance.Options.UseTextOptions = true;
            this.txtStatus.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtStatus.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtStatus.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatus.AppearanceDisabled.Options.UseFont = true;
            this.txtStatus.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatus.AppearanceHovered.Options.UseFont = true;
            this.txtStatus.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatus.AppearancePressed.Options.UseFont = true;
            this.txtStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.txtStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.txtStatus.Location = new System.Drawing.Point(183, 19);
            this.txtStatus.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(250, 81);
            this.txtStatus.TabIndex = 119;
            this.txtStatus.Text = "Ready.....";
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.btmRestStatic);
            this.groupControl3.Controls.Add(this.txtFailedUnit);
            this.groupControl3.Controls.Add(this.txtPassedUnit);
            this.groupControl3.Controls.Add(this.labelControl27);
            this.groupControl3.Controls.Add(this.txtUPH);
            this.groupControl3.Controls.Add(this.labelControl24);
            this.groupControl3.Controls.Add(this.txtFailed);
            this.groupControl3.Controls.Add(this.labelControl25);
            this.groupControl3.Controls.Add(this.txtPassed);
            this.groupControl3.Controls.Add(this.labelControl26);
            this.groupControl3.Location = new System.Drawing.Point(183, 398);
            this.groupControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(250, 189);
            this.groupControl3.TabIndex = 102;
            this.groupControl3.Text = "生产数据统计";
            // 
            // btmRestStatic
            // 
            this.btmRestStatic.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmRestStatic.Appearance.Options.UseFont = true;
            this.btmRestStatic.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRestStatic.AppearanceDisabled.Options.UseFont = true;
            this.btmRestStatic.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRestStatic.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("btmRestStatic.AppearanceHovered.Image")));
            this.btmRestStatic.AppearanceHovered.Options.UseFont = true;
            this.btmRestStatic.AppearanceHovered.Options.UseImage = true;
            this.btmRestStatic.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmRestStatic.AppearancePressed.Options.UseFont = true;
            this.btmRestStatic.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmRestStatic.ImageOptions.Image")));
            this.btmRestStatic.Location = new System.Drawing.Point(6, 141);
            this.btmRestStatic.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmRestStatic.Name = "btmRestStatic";
            this.btmRestStatic.Size = new System.Drawing.Size(79, 36);
            this.btmRestStatic.TabIndex = 17;
            this.btmRestStatic.Text = "清零";
            this.btmRestStatic.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // txtFailedUnit
            // 
            this.txtFailedUnit.Location = new System.Drawing.Point(160, 68);
            this.txtFailedUnit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFailedUnit.Name = "txtFailedUnit";
            this.txtFailedUnit.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedUnit.Properties.Appearance.Options.UseFont = true;
            this.txtFailedUnit.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedUnit.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtFailedUnit.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedUnit.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtFailedUnit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailedUnit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtFailedUnit.Size = new System.Drawing.Size(85, 24);
            this.txtFailedUnit.TabIndex = 16;
            // 
            // txtPassedUnit
            // 
            this.txtPassedUnit.Location = new System.Drawing.Point(160, 32);
            this.txtPassedUnit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPassedUnit.Name = "txtPassedUnit";
            this.txtPassedUnit.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedUnit.Properties.Appearance.Options.UseFont = true;
            this.txtPassedUnit.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedUnit.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPassedUnit.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedUnit.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPassedUnit.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassedUnit.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPassedUnit.Size = new System.Drawing.Size(85, 24);
            this.txtPassedUnit.TabIndex = 15;
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.BackColor = System.Drawing.Color.Aquamarine;
            this.labelControl27.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl27.Appearance.Options.UseBackColor = true;
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.Appearance.Options.UseTextOptions = true;
            this.labelControl27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl27.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl27.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl27.AppearanceDisabled.Options.UseFont = true;
            this.labelControl27.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl27.AppearanceHovered.Options.UseFont = true;
            this.labelControl27.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl27.AppearancePressed.Options.UseFont = true;
            this.labelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl27.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.labelControl27.Location = new System.Drawing.Point(113, 159);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(137, 29);
            this.labelControl27.TabIndex = 120;
            this.labelControl27.Text = "运行输出信息";
            // 
            // txtUPH
            // 
            this.txtUPH.Location = new System.Drawing.Point(99, 104);
            this.txtUPH.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUPH.Name = "txtUPH";
            this.txtUPH.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPH.Properties.Appearance.Options.UseFont = true;
            this.txtUPH.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPH.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtUPH.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPH.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtUPH.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtUPH.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtUPH.Size = new System.Drawing.Size(146, 24);
            this.txtUPH.TabIndex = 14;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl24.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.Appearance.Options.UseBackColor = true;
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearanceDisabled.Options.UseFont = true;
            this.labelControl24.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearanceHovered.Options.UseFont = true;
            this.labelControl24.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearancePressed.Options.UseFont = true;
            this.labelControl24.Location = new System.Drawing.Point(68, 109);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(25, 17);
            this.labelControl24.TabIndex = 13;
            this.labelControl24.Text = "UPH";
            // 
            // txtFailed
            // 
            this.txtFailed.Location = new System.Drawing.Point(99, 68);
            this.txtFailed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFailed.Name = "txtFailed";
            this.txtFailed.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailed.Properties.Appearance.Options.UseFont = true;
            this.txtFailed.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailed.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtFailed.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailed.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtFailed.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtFailed.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtFailed.Size = new System.Drawing.Size(55, 24);
            this.txtFailed.TabIndex = 12;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.Appearance.Options.UseBackColor = true;
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.AppearanceDisabled.Options.UseFont = true;
            this.labelControl25.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.AppearanceHovered.Options.UseFont = true;
            this.labelControl25.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.AppearancePressed.Options.UseFont = true;
            this.labelControl25.Location = new System.Drawing.Point(6, 72);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(87, 17);
            this.labelControl25.TabIndex = 11;
            this.labelControl25.Text = "坏品(整个Panel)";
            // 
            // txtPassed
            // 
            this.txtPassed.Location = new System.Drawing.Point(99, 32);
            this.txtPassed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPassed.Name = "txtPassed";
            this.txtPassed.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassed.Properties.Appearance.Options.UseFont = true;
            this.txtPassed.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassed.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPassed.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassed.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPassed.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPassed.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPassed.Size = new System.Drawing.Size(55, 24);
            this.txtPassed.TabIndex = 10;
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl26.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Appearance.Options.UseFont = true;
            this.labelControl26.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl26.AppearanceDisabled.Options.UseFont = true;
            this.labelControl26.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl26.AppearanceHovered.Options.UseFont = true;
            this.labelControl26.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl26.AppearancePressed.Options.UseFont = true;
            this.labelControl26.Location = new System.Drawing.Point(6, 35);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(87, 17);
            this.labelControl26.TabIndex = 9;
            this.labelControl26.Text = "好品(整个Panel)";
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelControl1.Appearance.Options.UseFont = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.groupControl5);
            this.panelControl1.Location = new System.Drawing.Point(439, 10);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1113, 911);
            this.panelControl1.TabIndex = 103;
            // 
            // richTxt_Msg_Out
            // 
            this.richTxt_Msg_Out.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTxt_Msg_Out.Location = new System.Drawing.Point(183, 591);
            this.richTxt_Msg_Out.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTxt_Msg_Out.Name = "richTxt_Msg_Out";
            this.richTxt_Msg_Out.Size = new System.Drawing.Size(250, 162);
            this.richTxt_Msg_Out.TabIndex = 104;
            this.richTxt_Msg_Out.Text = "";
            this.richTxt_Msg_Out.WordWrap = false;
            // 
            // timer_StatusMonitor
            // 
            this.timer_StatusMonitor.Interval = 250;
            this.timer_StatusMonitor.Tick += new System.EventHandler(this.timer_StatusMonitor_Tick);
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.txtMESResult2);
            this.groupControl4.Controls.Add(this.txtPCBVituralSN2);
            this.groupControl4.Controls.Add(this.labelControl28);
            this.groupControl4.Controls.Add(this.txtTrayID2);
            this.groupControl4.Controls.Add(this.labelControl29);
            this.groupControl4.Controls.Add(this.labelControl30);
            this.groupControl4.Location = new System.Drawing.Point(183, 254);
            this.groupControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(250, 135);
            this.groupControl4.TabIndex = 121;
            this.groupControl4.Text = "条码2请求基本信息";
            // 
            // txtMESResult2
            // 
            this.txtMESResult2.EditValue = "NG:25468";
            this.txtMESResult2.Location = new System.Drawing.Point(71, 94);
            this.txtMESResult2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtMESResult2.Name = "txtMESResult2";
            this.txtMESResult2.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtMESResult2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtMESResult2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMESResult2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtMESResult2.Properties.Appearance.Options.UseBackColor = true;
            this.txtMESResult2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMESResult2.Properties.Appearance.Options.UseFont = true;
            this.txtMESResult2.Properties.Appearance.Options.UseForeColor = true;
            this.txtMESResult2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMESResult2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMESResult2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtMESResult2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtMESResult2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMESResult2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtMESResult2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtMESResult2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtMESResult2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtMESResult2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtMESResult2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMESResult2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMESResult2.Properties.ReadOnly = true;
            this.txtMESResult2.Size = new System.Drawing.Size(174, 24);
            this.txtMESResult2.TabIndex = 118;
            // 
            // txtPCBVituralSN2
            // 
            this.txtPCBVituralSN2.EditValue = "345672";
            this.txtPCBVituralSN2.Location = new System.Drawing.Point(71, 63);
            this.txtPCBVituralSN2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPCBVituralSN2.Name = "txtPCBVituralSN2";
            this.txtPCBVituralSN2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBVituralSN2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBVituralSN2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPCBVituralSN2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBVituralSN2.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBVituralSN2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBVituralSN2.Properties.Appearance.Options.UseFont = true;
            this.txtPCBVituralSN2.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBVituralSN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBVituralSN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBVituralSN2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBVituralSN2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBVituralSN2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBVituralSN2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBVituralSN2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBVituralSN2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBVituralSN2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBVituralSN2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBVituralSN2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBVituralSN2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBVituralSN2.Properties.ReadOnly = true;
            this.txtPCBVituralSN2.Size = new System.Drawing.Size(174, 24);
            this.txtPCBVituralSN2.TabIndex = 117;
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl28.AppearanceDisabled.Options.UseFont = true;
            this.labelControl28.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl28.AppearanceHovered.Options.UseFont = true;
            this.labelControl28.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl28.AppearancePressed.Options.UseFont = true;
            this.labelControl28.Location = new System.Drawing.Point(14, 98);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(51, 17);
            this.labelControl28.TabIndex = 116;
            this.labelControl28.Text = "校验结果:";
            // 
            // txtTrayID2
            // 
            this.txtTrayID2.EditValue = "1";
            this.txtTrayID2.Location = new System.Drawing.Point(71, 33);
            this.txtTrayID2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTrayID2.Name = "txtTrayID2";
            this.txtTrayID2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTrayID2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTrayID2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrayID2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTrayID2.Properties.Appearance.Options.UseBackColor = true;
            this.txtTrayID2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTrayID2.Properties.Appearance.Options.UseFont = true;
            this.txtTrayID2.Properties.Appearance.Options.UseForeColor = true;
            this.txtTrayID2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTrayID2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTrayID2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTrayID2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTrayID2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTrayID2.Properties.ReadOnly = true;
            this.txtTrayID2.Size = new System.Drawing.Size(174, 24);
            this.txtTrayID2.TabIndex = 115;
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl29.AppearanceDisabled.Options.UseFont = true;
            this.labelControl29.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl29.AppearanceHovered.Options.UseFont = true;
            this.labelControl29.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl29.AppearancePressed.Options.UseFont = true;
            this.labelControl29.Location = new System.Drawing.Point(14, 67);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(50, 17);
            this.labelControl29.TabIndex = 114;
            this.labelControl29.Text = "PCB编号:";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.Options.UseFont = true;
            this.labelControl30.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl30.AppearanceDisabled.Options.UseFont = true;
            this.labelControl30.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl30.AppearanceHovered.Options.UseFont = true;
            this.labelControl30.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl30.AppearancePressed.Options.UseFont = true;
            this.labelControl30.Location = new System.Drawing.Point(14, 36);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(51, 17);
            this.labelControl30.TabIndex = 113;
            this.labelControl30.Text = "载板编号:";
            // 
            // 启动
            // 
            this.启动.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.启动.Appearance.Options.UseFont = true;
            this.启动.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearanceDisabled.Options.UseFont = true;
            this.启动.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearanceHovered.Options.UseFont = true;
            this.启动.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearancePressed.Options.UseFont = true;
            this.启动.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("启动.ImageOptions.Image")));
            this.启动.Location = new System.Drawing.Point(7, 87);
            this.启动.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.启动.Name = "启动";
            this.启动.Size = new System.Drawing.Size(76, 55);
            this.启动.TabIndex = 130;
            this.启动.Text = "启动";
            this.启动.Click += new System.EventHandler(this.启动_Click);
            // 
            // 错误清除
            // 
            this.错误清除.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.错误清除.Appearance.Options.UseFont = true;
            this.错误清除.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearanceDisabled.Options.UseFont = true;
            this.错误清除.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearanceHovered.Options.UseFont = true;
            this.错误清除.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearancePressed.Options.UseFont = true;
            this.错误清除.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("错误清除.ImageOptions.Image")));
            this.错误清除.Location = new System.Drawing.Point(4, 146);
            this.错误清除.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.错误清除.Name = "错误清除";
            this.错误清除.Size = new System.Drawing.Size(79, 55);
            this.错误清除.TabIndex = 131;
            this.错误清除.Text = "错误清除";
            this.错误清除.Click += new System.EventHandler(this.错误清除_Click);
            // 
            // 停止
            // 
            this.停止.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.停止.Appearance.Options.UseFont = true;
            this.停止.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearanceDisabled.Options.UseFont = true;
            this.停止.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearanceHovered.Options.UseFont = true;
            this.停止.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearancePressed.Options.UseFont = true;
            this.停止.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("停止.ImageOptions.Image")));
            this.停止.Location = new System.Drawing.Point(89, 30);
            this.停止.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.停止.Name = "停止";
            this.停止.Size = new System.Drawing.Size(84, 55);
            this.停止.TabIndex = 132;
            this.停止.Text = "停止";
            this.停止.Click += new System.EventHandler(this.停止_Click);
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl6.AppearanceCaption.Options.UseFont = true;
            this.groupControl6.Controls.Add(this.启用禁用);
            this.groupControl6.Controls.Add(this.simpleButton4);
            this.groupControl6.Controls.Add(this.btmTriggerBD2);
            this.groupControl6.Controls.Add(this.btmTri1gerBD2);
            this.groupControl6.Controls.Add(this.停止);
            this.groupControl6.Controls.Add(this.错误清除);
            this.groupControl6.Controls.Add(this.启动);
            this.groupControl6.Location = new System.Drawing.Point(0, 650);
            this.groupControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(177, 265);
            this.groupControl6.TabIndex = 122;
            this.groupControl6.Text = "操作(限于PLC)";
            // 
            // 启用禁用
            // 
            this.启用禁用.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.启用禁用.Appearance.Options.UseFont = true;
            this.启用禁用.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearanceDisabled.Options.UseFont = true;
            this.启用禁用.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearanceHovered.Options.UseFont = true;
            this.启用禁用.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearancePressed.Options.UseFont = true;
            this.启用禁用.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.启用禁用.Checked = true;
            this.启用禁用.Location = new System.Drawing.Point(7, 33);
            this.启用禁用.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.启用禁用.Name = "启用禁用";
            this.启用禁用.Size = new System.Drawing.Size(76, 51);
            this.启用禁用.TabIndex = 173;
            this.启用禁用.Text = "启用/禁用";
            this.启用禁用.CheckedChanged += new System.EventHandler(this.启用禁用_CheckedChanged);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceHovered.Options.UseFont = true;
            this.simpleButton4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearancePressed.Options.UseFont = true;
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(4, 208);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(167, 49);
            this.simpleButton4.TabIndex = 135;
            this.simpleButton4.Text = "复位";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_2);
            // 
            // btmTriggerBD2
            // 
            this.btmTriggerBD2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmTriggerBD2.Appearance.Options.UseFont = true;
            this.btmTriggerBD2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmTriggerBD2.AppearanceDisabled.Options.UseFont = true;
            this.btmTriggerBD2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmTriggerBD2.AppearanceHovered.Options.UseFont = true;
            this.btmTriggerBD2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmTriggerBD2.AppearancePressed.Options.UseFont = true;
            this.btmTriggerBD2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmTriggerBD2.ImageOptions.Image")));
            this.btmTriggerBD2.Location = new System.Drawing.Point(87, 146);
            this.btmTriggerBD2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmTriggerBD2.Name = "btmTriggerBD2";
            this.btmTriggerBD2.Size = new System.Drawing.Size(86, 55);
            this.btmTriggerBD2.TabIndex = 134;
            this.btmTriggerBD2.Text = "触发条码2";
            this.btmTriggerBD2.Click += new System.EventHandler(this.btmTriggerBD2_Click);
            // 
            // btmTri1gerBD2
            // 
            this.btmTri1gerBD2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmTri1gerBD2.Appearance.Options.UseFont = true;
            this.btmTri1gerBD2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmTri1gerBD2.AppearanceDisabled.Options.UseFont = true;
            this.btmTri1gerBD2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmTri1gerBD2.AppearanceHovered.Options.UseFont = true;
            this.btmTri1gerBD2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmTri1gerBD2.AppearancePressed.Options.UseFont = true;
            this.btmTri1gerBD2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btmTri1gerBD2.ImageOptions.Image")));
            this.btmTri1gerBD2.Location = new System.Drawing.Point(87, 87);
            this.btmTri1gerBD2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmTri1gerBD2.Name = "btmTri1gerBD2";
            this.btmTri1gerBD2.Size = new System.Drawing.Size(86, 55);
            this.btmTri1gerBD2.TabIndex = 133;
            this.btmTri1gerBD2.Text = "触发条码1";
            this.btmTri1gerBD2.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // richTxt_Msg_Out2
            // 
            this.richTxt_Msg_Out2.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTxt_Msg_Out2.Location = new System.Drawing.Point(183, 758);
            this.richTxt_Msg_Out2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTxt_Msg_Out2.Name = "richTxt_Msg_Out2";
            this.richTxt_Msg_Out2.Size = new System.Drawing.Size(250, 162);
            this.richTxt_Msg_Out2.TabIndex = 105;
            this.richTxt_Msg_Out2.Text = "";
            this.richTxt_Msg_Out2.WordWrap = false;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.Appearance.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.PageClient.Options.UseFont = true;
            this.xtraTabControl1.Location = new System.Drawing.Point(1, 151);
            this.xtraTabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(177, 498);
            this.xtraTabControl1.TabIndex = 101;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderDisabled.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabPage1.Appearance.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.PageClient.Options.UseFont = true;
            this.xtraTabPage1.Controls.Add(this.panelControl4);
            this.xtraTabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(171, 466);
            this.xtraTabPage1.Text = "PLC D1000";
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.txtD1013);
            this.panelControl4.Controls.Add(this.txtD1000);
            this.panelControl4.Controls.Add(this.labelControl18);
            this.panelControl4.Controls.Add(this.labelControl14);
            this.panelControl4.Controls.Add(this.txtD1007);
            this.panelControl4.Controls.Add(this.txtD1008);
            this.panelControl4.Controls.Add(this.txtD1010);
            this.panelControl4.Controls.Add(this.labelControl19);
            this.panelControl4.Controls.Add(this.labelControl13);
            this.panelControl4.Controls.Add(this.labelControl6);
            this.panelControl4.Controls.Add(this.txtD1003);
            this.panelControl4.Controls.Add(this.labelControl17);
            this.panelControl4.Controls.Add(this.txtD1009);
            this.panelControl4.Controls.Add(this.txtD1002);
            this.panelControl4.Controls.Add(this.labelControl10);
            this.panelControl4.Controls.Add(this.labelControl5);
            this.panelControl4.Controls.Add(this.txtD1012);
            this.panelControl4.Controls.Add(this.txtD1004);
            this.panelControl4.Controls.Add(this.txtD1011);
            this.panelControl4.Controls.Add(this.labelControl11);
            this.panelControl4.Controls.Add(this.txtD1001);
            this.panelControl4.Controls.Add(this.labelControl9);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl16);
            this.panelControl4.Controls.Add(this.txtD1005);
            this.panelControl4.Controls.Add(this.labelControl15);
            this.panelControl4.Controls.Add(this.txtD1006);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(0, 0);
            this.panelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(171, 459);
            this.panelControl4.TabIndex = 1;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.panelControl2);
            this.xtraTabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(171, 466);
            this.xtraTabPage2.Text = "PLC D1200";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.txtD1213);
            this.panelControl2.Controls.Add(this.txtD1200);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.txtD1207);
            this.panelControl2.Controls.Add(this.txtD1208);
            this.panelControl2.Controls.Add(this.txtD1210);
            this.panelControl2.Controls.Add(this.labelControl20);
            this.panelControl2.Controls.Add(this.labelControl31);
            this.panelControl2.Controls.Add(this.labelControl32);
            this.panelControl2.Controls.Add(this.txtD1203);
            this.panelControl2.Controls.Add(this.labelControl33);
            this.panelControl2.Controls.Add(this.txtD1209);
            this.panelControl2.Controls.Add(this.txtD1202);
            this.panelControl2.Controls.Add(this.labelControl34);
            this.panelControl2.Controls.Add(this.labelControl35);
            this.panelControl2.Controls.Add(this.txtD1212);
            this.panelControl2.Controls.Add(this.txtD1204);
            this.panelControl2.Controls.Add(this.txtD1211);
            this.panelControl2.Controls.Add(this.labelControl36);
            this.panelControl2.Controls.Add(this.txtD1201);
            this.panelControl2.Controls.Add(this.labelControl37);
            this.panelControl2.Controls.Add(this.labelControl38);
            this.panelControl2.Controls.Add(this.labelControl39);
            this.panelControl2.Controls.Add(this.txtD1205);
            this.panelControl2.Controls.Add(this.labelControl40);
            this.panelControl2.Controls.Add(this.txtD1206);
            this.panelControl2.Controls.Add(this.labelControl41);
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(171, 459);
            this.panelControl2.TabIndex = 0;
            // 
            // txtD1213
            // 
            this.txtD1213.Location = new System.Drawing.Point(77, 425);
            this.txtD1213.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1213.Name = "txtD1213";
            this.txtD1213.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1213.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1213.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1213.Properties.Appearance.Options.UseFont = true;
            this.txtD1213.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1213.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1213.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1213.Properties.ReadOnly = true;
            this.txtD1213.Size = new System.Drawing.Size(37, 22);
            this.txtD1213.TabIndex = 156;
            // 
            // txtD1200
            // 
            this.txtD1200.EditValue = "";
            this.txtD1200.Location = new System.Drawing.Point(77, 15);
            this.txtD1200.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1200.Name = "txtD1200";
            this.txtD1200.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1200.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1200.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1200.Properties.Appearance.Options.UseFont = true;
            this.txtD1200.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1200.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1200.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1200.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtD1200.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1200.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1200.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtD1200.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1200.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1200.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtD1200.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1200.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1200.Properties.ReadOnly = true;
            this.txtD1200.Size = new System.Drawing.Size(37, 22);
            this.txtD1200.TabIndex = 129;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl1.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl1.Location = new System.Drawing.Point(58, 18);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(14, 17);
            this.labelControl1.TabIndex = 154;
            this.labelControl1.Text = "00";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl4.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl4.Location = new System.Drawing.Point(58, 302);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(14, 17);
            this.labelControl4.TabIndex = 146;
            this.labelControl4.Text = "09";
            // 
            // txtD1207
            // 
            this.txtD1207.Location = new System.Drawing.Point(77, 236);
            this.txtD1207.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1207.Name = "txtD1207";
            this.txtD1207.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1207.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1207.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1207.Properties.Appearance.Options.UseFont = true;
            this.txtD1207.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1207.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1207.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1207.Properties.ReadOnly = true;
            this.txtD1207.Size = new System.Drawing.Size(37, 22);
            this.txtD1207.TabIndex = 136;
            // 
            // txtD1208
            // 
            this.txtD1208.Location = new System.Drawing.Point(77, 267);
            this.txtD1208.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1208.Name = "txtD1208";
            this.txtD1208.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1208.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1208.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1208.Properties.Appearance.Options.UseFont = true;
            this.txtD1208.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1208.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1208.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1208.Properties.ReadOnly = true;
            this.txtD1208.Size = new System.Drawing.Size(37, 22);
            this.txtD1208.TabIndex = 137;
            // 
            // txtD1210
            // 
            this.txtD1210.Location = new System.Drawing.Point(77, 330);
            this.txtD1210.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1210.Name = "txtD1210";
            this.txtD1210.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1210.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1210.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1210.Properties.Appearance.Options.UseFont = true;
            this.txtD1210.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1210.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1210.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1210.Properties.ReadOnly = true;
            this.txtD1210.Size = new System.Drawing.Size(37, 22);
            this.txtD1210.TabIndex = 151;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl20.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl20.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl20.Location = new System.Drawing.Point(58, 429);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(14, 17);
            this.labelControl20.TabIndex = 153;
            this.labelControl20.Text = "13";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl31.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Appearance.Options.UseBackColor = true;
            this.labelControl31.Appearance.Options.UseFont = true;
            this.labelControl31.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl31.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl31.Location = new System.Drawing.Point(58, 334);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(14, 17);
            this.labelControl31.TabIndex = 147;
            this.labelControl31.Text = "10";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl32.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Appearance.Options.UseBackColor = true;
            this.labelControl32.Appearance.Options.UseFont = true;
            this.labelControl32.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl32.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl32.Location = new System.Drawing.Point(58, 145);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(14, 17);
            this.labelControl32.TabIndex = 141;
            this.labelControl32.Text = "04";
            // 
            // txtD1203
            // 
            this.txtD1203.Location = new System.Drawing.Point(77, 109);
            this.txtD1203.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1203.Name = "txtD1203";
            this.txtD1203.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1203.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1203.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1203.Properties.Appearance.Options.UseFont = true;
            this.txtD1203.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1203.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1203.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1203.Properties.ReadOnly = true;
            this.txtD1203.Size = new System.Drawing.Size(37, 22);
            this.txtD1203.TabIndex = 132;
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl33.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Appearance.Options.UseBackColor = true;
            this.labelControl33.Appearance.Options.UseFont = true;
            this.labelControl33.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl33.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl33.Location = new System.Drawing.Point(58, 50);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(14, 17);
            this.labelControl33.TabIndex = 138;
            this.labelControl33.Text = "01";
            // 
            // txtD1209
            // 
            this.txtD1209.Location = new System.Drawing.Point(77, 299);
            this.txtD1209.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1209.Name = "txtD1209";
            this.txtD1209.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtD1209.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1209.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1209.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1209.Properties.Appearance.Options.UseFont = true;
            this.txtD1209.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1209.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1209.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1209.Properties.ReadOnly = true;
            this.txtD1209.Size = new System.Drawing.Size(37, 22);
            this.txtD1209.TabIndex = 150;
            // 
            // txtD1202
            // 
            this.txtD1202.Location = new System.Drawing.Point(77, 78);
            this.txtD1202.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1202.Name = "txtD1202";
            this.txtD1202.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1202.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1202.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1202.Properties.Appearance.Options.UseFont = true;
            this.txtD1202.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1202.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1202.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1202.Properties.ReadOnly = true;
            this.txtD1202.Size = new System.Drawing.Size(37, 22);
            this.txtD1202.TabIndex = 131;
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl34.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Appearance.Options.UseBackColor = true;
            this.labelControl34.Appearance.Options.UseFont = true;
            this.labelControl34.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl34.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl34.Location = new System.Drawing.Point(58, 176);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(14, 17);
            this.labelControl34.TabIndex = 142;
            this.labelControl34.Text = "05";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl35.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Appearance.Options.UseBackColor = true;
            this.labelControl35.Appearance.Options.UseFont = true;
            this.labelControl35.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl35.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl35.Location = new System.Drawing.Point(58, 366);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(14, 17);
            this.labelControl35.TabIndex = 148;
            this.labelControl35.Text = "11";
            // 
            // txtD1212
            // 
            this.txtD1212.Location = new System.Drawing.Point(77, 393);
            this.txtD1212.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1212.Name = "txtD1212";
            this.txtD1212.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1212.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1212.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1212.Properties.Appearance.Options.UseFont = true;
            this.txtD1212.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1212.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1212.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1212.Properties.ReadOnly = true;
            this.txtD1212.Size = new System.Drawing.Size(37, 22);
            this.txtD1212.TabIndex = 155;
            // 
            // txtD1204
            // 
            this.txtD1204.Location = new System.Drawing.Point(77, 141);
            this.txtD1204.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1204.Name = "txtD1204";
            this.txtD1204.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1204.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1204.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1204.Properties.Appearance.Options.UseFont = true;
            this.txtD1204.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1204.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1204.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1204.Properties.ReadOnly = true;
            this.txtD1204.Size = new System.Drawing.Size(37, 22);
            this.txtD1204.TabIndex = 133;
            // 
            // txtD1211
            // 
            this.txtD1211.Location = new System.Drawing.Point(77, 362);
            this.txtD1211.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1211.Name = "txtD1211";
            this.txtD1211.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1211.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1211.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1211.Properties.Appearance.Options.UseFont = true;
            this.txtD1211.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1211.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1211.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1211.Properties.ReadOnly = true;
            this.txtD1211.Size = new System.Drawing.Size(37, 22);
            this.txtD1211.TabIndex = 152;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl36.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Appearance.Options.UseBackColor = true;
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl36.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl36.Location = new System.Drawing.Point(58, 397);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(14, 17);
            this.labelControl36.TabIndex = 149;
            this.labelControl36.Text = "12";
            // 
            // txtD1201
            // 
            this.txtD1201.Location = new System.Drawing.Point(77, 46);
            this.txtD1201.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1201.Name = "txtD1201";
            this.txtD1201.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1201.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1201.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1201.Properties.Appearance.Options.UseFont = true;
            this.txtD1201.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1201.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1201.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1201.Properties.ReadOnly = true;
            this.txtD1201.Size = new System.Drawing.Size(37, 22);
            this.txtD1201.TabIndex = 130;
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl37.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.Options.UseBackColor = true;
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl37.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl37.Location = new System.Drawing.Point(58, 208);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(14, 17);
            this.labelControl37.TabIndex = 143;
            this.labelControl37.Text = "06";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl38.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Appearance.Options.UseBackColor = true;
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl38.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl38.Location = new System.Drawing.Point(58, 271);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(14, 17);
            this.labelControl38.TabIndex = 145;
            this.labelControl38.Text = "08";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl39.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Appearance.Options.UseBackColor = true;
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl39.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl39.Location = new System.Drawing.Point(58, 81);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(14, 17);
            this.labelControl39.TabIndex = 139;
            this.labelControl39.Text = "02";
            // 
            // txtD1205
            // 
            this.txtD1205.Location = new System.Drawing.Point(77, 172);
            this.txtD1205.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1205.Name = "txtD1205";
            this.txtD1205.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1205.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1205.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1205.Properties.Appearance.Options.UseFont = true;
            this.txtD1205.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1205.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1205.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1205.Properties.ReadOnly = true;
            this.txtD1205.Size = new System.Drawing.Size(37, 22);
            this.txtD1205.TabIndex = 134;
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl40.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Appearance.Options.UseBackColor = true;
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl40.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl40.Location = new System.Drawing.Point(58, 113);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(14, 17);
            this.labelControl40.TabIndex = 140;
            this.labelControl40.Text = "03";
            // 
            // txtD1206
            // 
            this.txtD1206.Location = new System.Drawing.Point(77, 204);
            this.txtD1206.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1206.Name = "txtD1206";
            this.txtD1206.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1206.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1206.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1206.Properties.Appearance.Options.UseFont = true;
            this.txtD1206.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1206.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1206.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1206.Properties.ReadOnly = true;
            this.txtD1206.Size = new System.Drawing.Size(37, 22);
            this.txtD1206.TabIndex = 135;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl41.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.Options.UseBackColor = true;
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl41.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl41.Location = new System.Drawing.Point(58, 239);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(14, 17);
            this.labelControl41.TabIndex = 144;
            this.labelControl41.Text = "07";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.panelControl3);
            this.xtraTabPage3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(171, 466);
            this.xtraTabPage3.Text = "PLC D1300";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.txtD1313);
            this.panelControl3.Controls.Add(this.txtD1300);
            this.panelControl3.Controls.Add(this.labelControl42);
            this.panelControl3.Controls.Add(this.labelControl43);
            this.panelControl3.Controls.Add(this.txtD1307);
            this.panelControl3.Controls.Add(this.txtD1308);
            this.panelControl3.Controls.Add(this.txtD1310);
            this.panelControl3.Controls.Add(this.labelControl44);
            this.panelControl3.Controls.Add(this.labelControl45);
            this.panelControl3.Controls.Add(this.labelControl46);
            this.panelControl3.Controls.Add(this.txtD1303);
            this.panelControl3.Controls.Add(this.labelControl47);
            this.panelControl3.Controls.Add(this.txtD1309);
            this.panelControl3.Controls.Add(this.txtD1302);
            this.panelControl3.Controls.Add(this.labelControl48);
            this.panelControl3.Controls.Add(this.labelControl49);
            this.panelControl3.Controls.Add(this.txtD1312);
            this.panelControl3.Controls.Add(this.txtD1304);
            this.panelControl3.Controls.Add(this.txtD1311);
            this.panelControl3.Controls.Add(this.labelControl50);
            this.panelControl3.Controls.Add(this.txtD1301);
            this.panelControl3.Controls.Add(this.labelControl51);
            this.panelControl3.Controls.Add(this.labelControl52);
            this.panelControl3.Controls.Add(this.labelControl53);
            this.panelControl3.Controls.Add(this.txtD1305);
            this.panelControl3.Controls.Add(this.labelControl54);
            this.panelControl3.Controls.Add(this.txtD1306);
            this.panelControl3.Controls.Add(this.labelControl55);
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(171, 459);
            this.panelControl3.TabIndex = 1;
            // 
            // txtD1313
            // 
            this.txtD1313.Location = new System.Drawing.Point(76, 421);
            this.txtD1313.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1313.Name = "txtD1313";
            this.txtD1313.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1313.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1313.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1313.Properties.Appearance.Options.UseFont = true;
            this.txtD1313.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1313.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1313.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1313.Properties.ReadOnly = true;
            this.txtD1313.Size = new System.Drawing.Size(37, 22);
            this.txtD1313.TabIndex = 184;
            // 
            // txtD1300
            // 
            this.txtD1300.EditValue = "";
            this.txtD1300.Location = new System.Drawing.Point(76, 11);
            this.txtD1300.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1300.Name = "txtD1300";
            this.txtD1300.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1300.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1300.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1300.Properties.Appearance.Options.UseFont = true;
            this.txtD1300.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1300.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1300.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1300.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtD1300.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1300.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1300.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtD1300.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1300.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1300.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtD1300.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1300.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1300.Properties.ReadOnly = true;
            this.txtD1300.Size = new System.Drawing.Size(37, 22);
            this.txtD1300.TabIndex = 157;
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl42.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.Options.UseBackColor = true;
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl42.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl42.Location = new System.Drawing.Point(57, 15);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(14, 17);
            this.labelControl42.TabIndex = 182;
            this.labelControl42.Text = "00";
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl43.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Appearance.Options.UseBackColor = true;
            this.labelControl43.Appearance.Options.UseFont = true;
            this.labelControl43.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl43.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl43.Location = new System.Drawing.Point(57, 299);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(14, 17);
            this.labelControl43.TabIndex = 174;
            this.labelControl43.Text = "09";
            // 
            // txtD1307
            // 
            this.txtD1307.Location = new System.Drawing.Point(76, 232);
            this.txtD1307.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1307.Name = "txtD1307";
            this.txtD1307.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1307.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1307.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1307.Properties.Appearance.Options.UseFont = true;
            this.txtD1307.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1307.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1307.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1307.Properties.ReadOnly = true;
            this.txtD1307.Size = new System.Drawing.Size(37, 22);
            this.txtD1307.TabIndex = 164;
            // 
            // txtD1308
            // 
            this.txtD1308.Location = new System.Drawing.Point(76, 264);
            this.txtD1308.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1308.Name = "txtD1308";
            this.txtD1308.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1308.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1308.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1308.Properties.Appearance.Options.UseFont = true;
            this.txtD1308.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1308.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1308.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1308.Properties.ReadOnly = true;
            this.txtD1308.Size = new System.Drawing.Size(37, 22);
            this.txtD1308.TabIndex = 165;
            // 
            // txtD1310
            // 
            this.txtD1310.Location = new System.Drawing.Point(76, 327);
            this.txtD1310.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1310.Name = "txtD1310";
            this.txtD1310.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1310.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1310.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1310.Properties.Appearance.Options.UseFont = true;
            this.txtD1310.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1310.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1310.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1310.Properties.ReadOnly = true;
            this.txtD1310.Size = new System.Drawing.Size(37, 22);
            this.txtD1310.TabIndex = 179;
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl44.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Appearance.Options.UseBackColor = true;
            this.labelControl44.Appearance.Options.UseFont = true;
            this.labelControl44.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl44.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl44.Location = new System.Drawing.Point(57, 425);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(14, 17);
            this.labelControl44.TabIndex = 181;
            this.labelControl44.Text = "13";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl45.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Appearance.Options.UseBackColor = true;
            this.labelControl45.Appearance.Options.UseFont = true;
            this.labelControl45.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl45.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl45.Location = new System.Drawing.Point(57, 330);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(14, 17);
            this.labelControl45.TabIndex = 175;
            this.labelControl45.Text = "10";
            // 
            // labelControl46
            // 
            this.labelControl46.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl46.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl46.Appearance.Options.UseBackColor = true;
            this.labelControl46.Appearance.Options.UseFont = true;
            this.labelControl46.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl46.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl46.Location = new System.Drawing.Point(57, 141);
            this.labelControl46.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(14, 17);
            this.labelControl46.TabIndex = 169;
            this.labelControl46.Text = "04";
            // 
            // txtD1303
            // 
            this.txtD1303.Location = new System.Drawing.Point(76, 106);
            this.txtD1303.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1303.Name = "txtD1303";
            this.txtD1303.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1303.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1303.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1303.Properties.Appearance.Options.UseFont = true;
            this.txtD1303.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1303.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1303.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1303.Properties.ReadOnly = true;
            this.txtD1303.Size = new System.Drawing.Size(37, 22);
            this.txtD1303.TabIndex = 160;
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl47.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Appearance.Options.UseBackColor = true;
            this.labelControl47.Appearance.Options.UseFont = true;
            this.labelControl47.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl47.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl47.Location = new System.Drawing.Point(57, 46);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(14, 17);
            this.labelControl47.TabIndex = 166;
            this.labelControl47.Text = "01";
            // 
            // txtD1309
            // 
            this.txtD1309.Location = new System.Drawing.Point(76, 295);
            this.txtD1309.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1309.Name = "txtD1309";
            this.txtD1309.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.txtD1309.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1309.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1309.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1309.Properties.Appearance.Options.UseFont = true;
            this.txtD1309.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1309.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1309.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1309.Properties.ReadOnly = true;
            this.txtD1309.Size = new System.Drawing.Size(37, 22);
            this.txtD1309.TabIndex = 178;
            // 
            // txtD1302
            // 
            this.txtD1302.Location = new System.Drawing.Point(76, 74);
            this.txtD1302.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1302.Name = "txtD1302";
            this.txtD1302.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1302.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1302.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1302.Properties.Appearance.Options.UseFont = true;
            this.txtD1302.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1302.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1302.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1302.Properties.ReadOnly = true;
            this.txtD1302.Size = new System.Drawing.Size(37, 22);
            this.txtD1302.TabIndex = 159;
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl48.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Appearance.Options.UseBackColor = true;
            this.labelControl48.Appearance.Options.UseFont = true;
            this.labelControl48.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl48.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl48.Location = new System.Drawing.Point(57, 172);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(14, 17);
            this.labelControl48.TabIndex = 170;
            this.labelControl48.Text = "05";
            // 
            // labelControl49
            // 
            this.labelControl49.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl49.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl49.Appearance.Options.UseBackColor = true;
            this.labelControl49.Appearance.Options.UseFont = true;
            this.labelControl49.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl49.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl49.Location = new System.Drawing.Point(57, 362);
            this.labelControl49.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(14, 17);
            this.labelControl49.TabIndex = 176;
            this.labelControl49.Text = "11";
            // 
            // txtD1312
            // 
            this.txtD1312.Location = new System.Drawing.Point(76, 390);
            this.txtD1312.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1312.Name = "txtD1312";
            this.txtD1312.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1312.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1312.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1312.Properties.Appearance.Options.UseFont = true;
            this.txtD1312.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1312.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1312.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1312.Properties.ReadOnly = true;
            this.txtD1312.Size = new System.Drawing.Size(37, 22);
            this.txtD1312.TabIndex = 183;
            // 
            // txtD1304
            // 
            this.txtD1304.Location = new System.Drawing.Point(76, 137);
            this.txtD1304.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1304.Name = "txtD1304";
            this.txtD1304.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1304.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1304.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1304.Properties.Appearance.Options.UseFont = true;
            this.txtD1304.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1304.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1304.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1304.Properties.ReadOnly = true;
            this.txtD1304.Size = new System.Drawing.Size(37, 22);
            this.txtD1304.TabIndex = 161;
            // 
            // txtD1311
            // 
            this.txtD1311.Location = new System.Drawing.Point(76, 358);
            this.txtD1311.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1311.Name = "txtD1311";
            this.txtD1311.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1311.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1311.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1311.Properties.Appearance.Options.UseFont = true;
            this.txtD1311.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1311.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1311.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1311.Properties.ReadOnly = true;
            this.txtD1311.Size = new System.Drawing.Size(37, 22);
            this.txtD1311.TabIndex = 180;
            // 
            // labelControl50
            // 
            this.labelControl50.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl50.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl50.Appearance.Options.UseBackColor = true;
            this.labelControl50.Appearance.Options.UseFont = true;
            this.labelControl50.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl50.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl50.Location = new System.Drawing.Point(57, 393);
            this.labelControl50.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(14, 17);
            this.labelControl50.TabIndex = 177;
            this.labelControl50.Text = "12";
            // 
            // txtD1301
            // 
            this.txtD1301.Location = new System.Drawing.Point(76, 43);
            this.txtD1301.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1301.Name = "txtD1301";
            this.txtD1301.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1301.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1301.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1301.Properties.Appearance.Options.UseFont = true;
            this.txtD1301.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1301.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1301.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1301.Properties.ReadOnly = true;
            this.txtD1301.Size = new System.Drawing.Size(37, 22);
            this.txtD1301.TabIndex = 158;
            // 
            // labelControl51
            // 
            this.labelControl51.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl51.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl51.Appearance.Options.UseBackColor = true;
            this.labelControl51.Appearance.Options.UseFont = true;
            this.labelControl51.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl51.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl51.Location = new System.Drawing.Point(57, 204);
            this.labelControl51.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(14, 17);
            this.labelControl51.TabIndex = 171;
            this.labelControl51.Text = "06";
            // 
            // labelControl52
            // 
            this.labelControl52.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl52.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl52.Appearance.Options.UseBackColor = true;
            this.labelControl52.Appearance.Options.UseFont = true;
            this.labelControl52.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl52.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl52.Location = new System.Drawing.Point(57, 267);
            this.labelControl52.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(14, 17);
            this.labelControl52.TabIndex = 173;
            this.labelControl52.Text = "08";
            // 
            // labelControl53
            // 
            this.labelControl53.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl53.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl53.Appearance.Options.UseBackColor = true;
            this.labelControl53.Appearance.Options.UseFont = true;
            this.labelControl53.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl53.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl53.Location = new System.Drawing.Point(57, 78);
            this.labelControl53.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(14, 17);
            this.labelControl53.TabIndex = 167;
            this.labelControl53.Text = "02";
            // 
            // txtD1305
            // 
            this.txtD1305.Location = new System.Drawing.Point(76, 169);
            this.txtD1305.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1305.Name = "txtD1305";
            this.txtD1305.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1305.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1305.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1305.Properties.Appearance.Options.UseFont = true;
            this.txtD1305.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1305.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1305.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1305.Properties.ReadOnly = true;
            this.txtD1305.Size = new System.Drawing.Size(37, 22);
            this.txtD1305.TabIndex = 162;
            // 
            // labelControl54
            // 
            this.labelControl54.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl54.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl54.Appearance.Options.UseBackColor = true;
            this.labelControl54.Appearance.Options.UseFont = true;
            this.labelControl54.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl54.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl54.Location = new System.Drawing.Point(57, 109);
            this.labelControl54.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(14, 17);
            this.labelControl54.TabIndex = 168;
            this.labelControl54.Text = "03";
            // 
            // txtD1306
            // 
            this.txtD1306.Location = new System.Drawing.Point(76, 200);
            this.txtD1306.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtD1306.Name = "txtD1306";
            this.txtD1306.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtD1306.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtD1306.Properties.Appearance.Options.UseBackColor = true;
            this.txtD1306.Properties.Appearance.Options.UseFont = true;
            this.txtD1306.Properties.Appearance.Options.UseTextOptions = true;
            this.txtD1306.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtD1306.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtD1306.Properties.ReadOnly = true;
            this.txtD1306.Size = new System.Drawing.Size(37, 22);
            this.txtD1306.TabIndex = 163;
            // 
            // labelControl55
            // 
            this.labelControl55.Appearance.BackColor = System.Drawing.Color.Aqua;
            this.labelControl55.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl55.Appearance.Options.UseBackColor = true;
            this.labelControl55.Appearance.Options.UseFont = true;
            this.labelControl55.AppearanceDisabled.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl55.AppearanceDisabled.Options.UseBackColor = true;
            this.labelControl55.Location = new System.Drawing.Point(57, 236);
            this.labelControl55.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(14, 17);
            this.labelControl55.TabIndex = 172;
            this.labelControl55.Text = "07";
            // 
            // LEDPLC
            // 
            this.LEDPLC.BackColor = System.Drawing.Color.Transparent;
            this.LEDPLC.LanternBackground = System.Drawing.Color.Gray;
            this.LEDPLC.Location = new System.Drawing.Point(10, 19);
            this.LEDPLC.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LEDPLC.Name = "LEDPLC";
            this.LEDPLC.Size = new System.Drawing.Size(31, 33);
            this.LEDPLC.TabIndex = 101;
            // 
            // LEDScanner1
            // 
            this.LEDScanner1.BackColor = System.Drawing.Color.Transparent;
            this.LEDScanner1.LanternBackground = System.Drawing.Color.Gray;
            this.LEDScanner1.Location = new System.Drawing.Point(10, 62);
            this.LEDScanner1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.LEDScanner1.Name = "LEDScanner1";
            this.LEDScanner1.Size = new System.Drawing.Size(31, 33);
            this.LEDScanner1.TabIndex = 102;
            // 
            // LEDScanner2
            // 
            this.LEDScanner2.BackColor = System.Drawing.Color.Transparent;
            this.LEDScanner2.LanternBackground = System.Drawing.Color.Gray;
            this.LEDScanner2.Location = new System.Drawing.Point(10, 107);
            this.LEDScanner2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.LEDScanner2.Name = "LEDScanner2";
            this.LEDScanner2.Size = new System.Drawing.Size(31, 33);
            this.LEDScanner2.TabIndex = 103;
            // 
            // frm_StatinInfoDetail
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1558, 924);
            this.Controls.Add(this.LEDScanner2);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.LEDScanner1);
            this.Controls.Add(this.richTxt_Msg_Out2);
            this.Controls.Add(this.LEDPLC);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.groupControl6);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.richTxt_Msg_Out);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl12);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_StatinInfoDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_StatinInfoDetail";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.form_Closing);
            this.Load += new System.EventHandler(this.frm_StatinInfoDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1013.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1012.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1011.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1000.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1010.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1001.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1009.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1002.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1003.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1004.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1005.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1006.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1007.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1008.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMESResult.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBVituralSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailedUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassedUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUPH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFailed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMESResult2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBVituralSN2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrayID2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1213.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1200.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1207.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1208.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1210.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1203.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1209.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1202.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1212.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1204.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1211.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1201.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1205.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1206.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1313.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1300.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1307.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1308.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1310.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1303.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1309.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1302.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1312.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1304.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1311.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1301.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1305.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtD1306.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtD1013;
        private DevExpress.XtraEditors.TextEdit txtD1012;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit txtD1011;
        private DevExpress.XtraEditors.TextEdit txtD1000;
        private DevExpress.XtraEditors.TextEdit txtD1010;
        private DevExpress.XtraEditors.TextEdit txtD1001;
        private DevExpress.XtraEditors.TextEdit txtD1009;
        private DevExpress.XtraEditors.TextEdit txtD1002;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtD1003;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtD1004;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtD1005;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtD1006;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtD1007;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtD1008;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl txtStatus;
        private DevExpress.XtraEditors.TextEdit txtMESResult;
        private DevExpress.XtraEditors.TextEdit txtPCBVituralSN;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtTrayID;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton btmRestStatic;
        private DevExpress.XtraEditors.TextEdit txtFailedUnit;
        private DevExpress.XtraEditors.TextEdit txtPassedUnit;
        private DevExpress.XtraEditors.TextEdit txtUPH;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtFailed;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txtPassed;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.RichTextBox richTxt_Msg_Out;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private System.Windows.Forms.Timer timer_StatusMonitor;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.TextEdit txtMESResult2;
        private DevExpress.XtraEditors.TextEdit txtPCBVituralSN2;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit txtTrayID2;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.SimpleButton 停止;
        private DevExpress.XtraEditors.SimpleButton 错误清除;
        private DevExpress.XtraEditors.SimpleButton 启动;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.SimpleButton btmTriggerBD2;
        private DevExpress.XtraEditors.SimpleButton btmTri1gerBD2;
        private System.Windows.Forms.RichTextBox richTxt_Msg_Out2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.TextEdit txtD1213;
        private DevExpress.XtraEditors.TextEdit txtD1200;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtD1207;
        private DevExpress.XtraEditors.TextEdit txtD1208;
        private DevExpress.XtraEditors.TextEdit txtD1210;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.TextEdit txtD1203;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.TextEdit txtD1209;
        private DevExpress.XtraEditors.TextEdit txtD1202;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtD1212;
        private DevExpress.XtraEditors.TextEdit txtD1204;
        private DevExpress.XtraEditors.TextEdit txtD1211;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit txtD1201;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit txtD1205;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit txtD1206;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.TextEdit txtD1313;
        private DevExpress.XtraEditors.TextEdit txtD1300;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit txtD1307;
        private DevExpress.XtraEditors.TextEdit txtD1308;
        private DevExpress.XtraEditors.TextEdit txtD1310;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.TextEdit txtD1303;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.TextEdit txtD1309;
        private DevExpress.XtraEditors.TextEdit txtD1302;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.TextEdit txtD1312;
        private DevExpress.XtraEditors.TextEdit txtD1304;
        private DevExpress.XtraEditors.TextEdit txtD1311;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.TextEdit txtD1301;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.TextEdit txtD1305;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.TextEdit txtD1306;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.CheckButton 启用禁用;
        private HslCommunication.Controls.UserLantern LEDPLC;
        private HslCommunication.Controls.UserLantern LEDScanner1;
        private HslCommunication.Controls.UserLantern LEDScanner2;
    }
}