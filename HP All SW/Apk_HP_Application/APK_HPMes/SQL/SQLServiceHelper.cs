﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace APK_HP.MES.HPSql
{
    public class SQLServiceHelper
    {
        //Data Source  dll 所在的文件夹位置
        string Connection = "Data Source=127.0.0.1;Database=database;User ID=User;Password=123";      
        public SqlConnection getConnection()
        {
            return new SqlConnection(Connection);
            //return new SQLiteConnection("Data Source=" + Application.StartupPath + @"\database\" + Globalspace.database_Name + ".sqlite;Version=3;");
        }
        SqlConnection con = null;
        /// 执行SQL语句
        /// <param name="sql">SQL语句</param>
        /// <returns>返回一个具体值</returns>
        public object QueryScalar(string sql)// 没有调用
        {
            Open();//打开数据连接
            object result = null;
            try
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    result = cmd.ExecuteScalar();
                    return result;
                }
            }
            catch
            {
                return null;
            }

        }
        /// 执行SQL语句
        /// <param name="sql">要执行的SQL语句</param>
        /// <param name="prams">参数</param>
        public object QueryScalar(string sql, SqlParameter[] prams)//调用了
        {
            Open();
            object result = null;
            try
            {
                using (SqlCommand cmd = CreateCommandSql(sql, prams))
                {
                    result = cmd.ExecuteScalar();
                    return result;
                }
            }
            catch
            {
                return null;
            }
        }
        /// 创建一个Sqlcommand对象，用来构建SQL语句
        /// <param name="sql">sql语句</param>
        /// <param name="prams">sql所需要的参数</param>
        public SqlCommand CreateCommandSql(string sql, SqlParameter[] prams)//调用了
        {
            Open();
            SqlCommand cmd = new SqlCommand(sql, con);
            if (prams != null)
            {
                foreach (SqlParameter parameter in prams)
                {
                    cmd.Parameters.Add(parameter);
                }
            }
            return cmd;
        }
        private void Open()
        {
            if (con == null)
            {
                con = new SqlConnection(Connection);
            }
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }
        /// 要执行SQL语句，该方法返回一个DataTable
        /// <param name="sql">执行SQL语句</param>
        public DataTable Query(string sql)//外部 统计调用了
        {
            Open();
            using (SqlDataAdapter sqlda = new SqlDataAdapter(sql, con))
            {
                using (DataTable dt = new DataTable())
                {
                    sqlda.Fill(dt);
                    return dt;
                }
            }
        }
        /// 执行SQL语句，返回DataTable
        /// <param name="sql">要执行的SQL语句</param>
        /// <param name="prams">构建SQL语句所需要的参数</param>
        public DataTable Query(string sql, SqlParameter[] prams)
        {
            SqlCommand cmd = CreateCommandSql(sql, prams);
            using (SqlDataAdapter sqldata = new SqlDataAdapter(cmd))
            {
                using (DataTable dt = new DataTable())
                {
                    sqldata.Fill(dt);
                    return dt;
                }
            }
        }
        /// 执行SQL语句，返回影响的记录行数
        /// <param name="sql">要执行的SQL语句</param>
        public int RunSql(string sql)//外部调用
        {
            int result = -1;
            try
            {
                Open();
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    result = cmd.ExecuteNonQuery();//ExecuteNonQuery
                    con.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Run过程(string sql1, string sql2)//外部调用
        {
            int result = -1;
            try
            {
                Open();
                using (SqlCommand cmd = new SqlCommand(sql1, con))
                {
                    result = cmd.ExecuteNonQuery();
                    cmd.CommandText = sql2;
                    result = cmd.ExecuteNonQuery();
                    con.Close();
                    return result;
                }
            }
            catch
            {
                return 0;
            }
        }

        /// 执行SQL语句，返回影响的记录行数
        /// <param name="sql">要执行的SQL语句</param>
        /// <param name="prams">SQL语句所需要的参数</param>
        public int RunSql(string sql, SqlParameter[] prams)
        {
            try
            {
                int result = -1;
                SqlCommand cmd = CreateCommandSql(sql, prams);
                result = cmd.ExecuteNonQuery();
                this.Close();
                return result;
            }
            catch
            {
                return 0;
            }
        }
        public void Close()
        {
            if (con != null)
                con.Close();
        }

        public void RunSql(string sql, SqlParameter[] prams, out SqlDataReader dataReader, string str_data_open)
        {
            SqlCommand cmd = CreateCommandSql(sql, prams);
            dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }

        /// 要执行SQL语句，该方法返回一个DataTable
        public DataTable QTable(string sql1, string sql2)//外部 统计调用了
        {
            Open();
            using (DataSet dt = new DataSet())
            {
                using (SqlDataAdapter sqlda = new SqlDataAdapter(sql1, con))
                {
                    sqlda.Fill(dt, sql2);
                    DataTable da = dt.Tables[sql2];
                    return da;
                }
            }
        }
    }
}
