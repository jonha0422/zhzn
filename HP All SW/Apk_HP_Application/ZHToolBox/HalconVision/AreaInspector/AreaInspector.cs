﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.IO;
using HalconDotNet;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using static ZHToolBox.HalconVision.HDVisionTypes;
using static ZHToolBox.HalconVision.HalconGeneral;

namespace ZHToolBox.HalconVision
{
    public class AreaInspector : MethodBase
    {
        bool TemplateIsCreated = false;

        #region>>>>>>>>> 配制参数  //Category("选项"), DefaultValue(3), DisplayName("测试")
        [Editor(typeof(PropertyGridTempleteUI), typeof(UITypeEditor)), Category("配置"), DefaultValue(""), DisplayName("模版图像文件")]
        public string tempFileName { get; set; }

        [Editor(typeof(PropertyGridTempleteUI), typeof(UITypeEditor)), Category("配置"), DefaultValue(""), DisplayName("测量图像文件")]
        public string ImageFileName { get; set; }
        #endregion

        #region>>>>>>>>> 检测参数
        [Category("检测参数"), DefaultValue(0.90), DisplayName("单个最小面积")]
        public double fMinArea { get; set; }

        [Category("检测参数"), DefaultValue(0.90), DisplayName("单个最大面积")]
        public double fMaxArea { get; set; }

        [Category("检测参数"), DefaultValue(0.90), DisplayName("总计最小面积")]
        public double fMeasureAreaMin { get; set; }

        [Category("检测参数"), DefaultValue(0.90), DisplayName("最小个数")]
        public int CountMin { get; set; }

        [Category("检测参数"), DefaultValue(true), DisplayName("空洞填补")]
        public bool EnableFillUp { get; set; }

        [Category("检测参数"), DefaultValue(false), DisplayName("是否进行二值化")]
        public bool EnableThreashold { get; set; }

        [Category("检测参数"), DefaultValue(128), DisplayName("最小二值化")]
        public short ThresholdMin { get; set; }

        [Category("检测参数"), DefaultValue(255), DisplayName("最大二值化")]
        public short ThresholdMax { get; set; }

        [Category("检测参数"), DefaultValue(false), DisplayName("结果取反")]
        public bool SwapResult { get; set; }

        #endregion

        #region>>>>>>>>> 输出
        [Category("结果"), DefaultValue(3), DisplayName("最终结果")]
        public bool FinallyResult { get; set; }

        [Category("结果"), DefaultValue(3), DisplayName("匹配数量")]
        public int CountOfMatch { get; set; }

        [Editor(typeof(PropertyGridRIOCollectorUI), typeof(UITypeEditor)), Category("结果"), DefaultValue(3), DisplayName("检测区域及结果")]
        public CheckResultDef[] CheckResult { get; set; }

        #endregion

        #region>>>>>>>>>显示
        bool _bShowProcessWindow = false;
        [Category("性能/显示"), DefaultValue(false), DisplayName("显示图像处理窗口")]
        public bool bShowProcessWindow
        {
            get
            {
                return _bShowProcessWindow;
            }

            set
            {
                _bShowProcessWindow = value;
                if (_bShowProcessWindow)
                {
                    if (ImageTempWnd != null)
                        ImageTempWnd.Show();

                    if (ImageProcessWnd != null)
                        ImageProcessWnd.Show();
                }
                else
                {
                    if (ImageTempWnd != null)
                        ImageTempWnd.Hide();

                    if (ImageProcessWnd != null)
                        ImageProcessWnd.Hide();
                }
            }

        }

        [Category("性能/显示"), DefaultValue(false), DisplayName("启用多线程")]
        public bool bEnableMuitlThread { get; set; }
        #endregion

        HalconForm ImageTempWnd = new HalconForm("模版窗口");
        HalconForm ImageProcessWnd = new HalconForm("图像处理窗口");

        public AreaInspector()
        {
            FinallyResult = false;

            fMinArea = 50;
            fMaxArea = 9999;

            fMeasureAreaMin = 100000;
            CheckResult = new CheckResultDef[1];
            CheckResult[0] = new CheckResultDef();

            EnableFillUp = true;
            EnableThreashold = true;

            ThresholdMin = 128;
            ThresholdMax = 255;

            SwapResult = false;

            CountMin = 1;

            DateTime dateTime = DateTime.Now;
            Name = dateTime.ToString("yyyyMMddHHmmss") + $".{dateTime.Millisecond.ToString("D03")}面积检测方法";
        }

        ~AreaInspector()
        {

        }

        public void Dispose()
        {
            try
            {
                ImageTempWnd.Dispose();
                ImageProcessWnd.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        public bool Init(string tempImageFileName, bool reset = false)
        {
            try
            {
                HObject objImage = new HObject() ;
                return Init(new HImage(objImage), reset);

            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Init(HImage vTempImage, bool reset = false)
        {
            try
            {
                TemplateIsCreated = true;

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
               throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ImageProcess(string tempImageFileName)
        {
            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"AreaInspector 没有建立模版，请调用Init");

                if (!File.Exists(tempImageFileName))
                    throw new HalconException($"AreaInspector ImageProcess()处理异常，当前文件不存在\r\n{tempImageFileName}");

                HObject objImage;
                HOperatorSet.ReadImage(out objImage, tempImageFileName);
                bool result = ImageProcess(new HImage(objImage));
                return result;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ImageProcess(HImage imageObj)
        {
            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"AreaInspector 没有初始化，请调用Init");

                if (!HalconGeneral.IsImageObj(imageObj))
                    throw new HalconException($"AreaInspector ImageProcess 图像格式不对");

                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    CheckResult[nIndexAOI].Result = -999;
                    CheckResult[nIndexAOI].ResultStatus = false;
                }

                if(ThresholdMin <= 0)
                    ThresholdMin = 128;

                if (ThresholdMax > 255)
                    ThresholdMax = 255;

                CountOfMatch = 0;
                HTuple width, height;
                HOperatorSet.GetImageSize(imageObj, out width, out height);
                ImageProcessWnd.ShowImage(imageObj, width, height);

                HObject ho_Image = null;
                HOperatorSet.CopyImage(imageObj, out ho_Image);
                HOperatorSet.DispObj(ho_Image, ImageProcessWnd.hWindow);

                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    if (CheckResult[nIndexAOI].CheckedRIO.fRow <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fCol <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fLen1 <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fLen2 <= 0.01)
                    {
                        CheckResult[nIndexAOI].CheckedRIO.fRow = (double)height / 2;
                        CheckResult[nIndexAOI].CheckedRIO.fCol = (double)width / 2;
                        CheckResult[nIndexAOI].CheckedRIO.fPhi = 0;
                        CheckResult[nIndexAOI].CheckedRIO.fLen1 = (double)width / 2 - 2;
                        CheckResult[nIndexAOI].CheckedRIO.fLen2 = (double)height / 2 - 2;
                    }

                    HObject Rectangle1, TemplateBottleRegion, ImageReduce;
                    HOperatorSet.GenRectangle2(out Rectangle1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol,
                                                                    (new HTuple(CheckResult[nIndexAOI].CheckedRIO.fPhi)).TupleRad(), CheckResult[nIndexAOI].CheckedRIO.fLen1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fLen2);

                    HOperatorSet.Union1(Rectangle1, out TemplateBottleRegion);
                    HOperatorSet.ReduceDomain(ho_Image, TemplateBottleRegion, out ImageReduce);
                    HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);

                    HObject ProcessImage = new HObject();
                    HOperatorSet.CopyImage(ImageReduce, out ProcessImage);

                    if (EnableThreashold)
                        HOperatorSet.Threshold(ImageReduce, out ProcessImage, ThresholdMin, ThresholdMax);
                    else
                        HOperatorSet.AutoThreshold(ImageReduce, out ProcessImage, 2);

                    HOperatorSet.Connection(ProcessImage, out ProcessImage);

                    if (EnableFillUp)
                        HOperatorSet.FillUp(ProcessImage, out ProcessImage);

                    HOperatorSet.SelectShape(ProcessImage, out ProcessImage, "area", "and", fMinArea, fMaxArea);

                    if(ProcessImage.CountObj() <= 0 || ProcessImage.CountObj() < CountMin)
                    {
                        FinallyResult = SwapResult == false ? false : true;
                        CheckResult[nIndexAOI].Result = -998;
                        CheckResult[nIndexAOI].ResultStatus = SwapResult == false ? false : true;

                        HOperatorSet.SetColor(ImageProcessWnd.hWindow, SwapResult == false ? "red" : "forest green");
                        HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                        HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, SwapResult == false ? "Part NG" : "Part OK", "image", CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol, SwapResult == false ? "red" : "forest green", "true");
                        continue;
                    }

                    HOperatorSet.SetDraw(ImageProcessWnd.hWindow, "margin");
                    HOperatorSet.SetColor(ImageProcessWnd.hWindow, "blue");
                    HOperatorSet.DispObj(ProcessImage, ImageProcessWnd.hWindow);

                    HTuple area, row, column;
                    double totalArea = 0;
                    HOperatorSet.AreaCenter(ProcessImage, out area, out row, out column);
                    for (int nIndex = 0; nIndex < area.Length; nIndex++)
                        totalArea += area[nIndex];

                    CheckResult[nIndexAOI].Result = (int)totalArea;
                    if (totalArea > fMeasureAreaMin)
                    {
                        HOperatorSet.SetColor(ImageProcessWnd.hWindow, SwapResult == false ? "green" : "red");
                        HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                        HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, SwapResult == false ? $"Part OK({((double)totalArea).ToString("F2")})" : $"Part NG({((double)totalArea).ToString("F2")})", "image", CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol, SwapResult == false ? "forest green" : "red", "true");
                        CountOfMatch++;

                        CheckResult[nIndexAOI].ResultStatus = SwapResult == false ? true : false;
                        if (CheckResult[nIndexAOI].MatchingResult == null)
                            CheckResult[nIndexAOI].MatchingResult = new MATCHINGRESULT_DES();

                        CheckResult[nIndexAOI].MatchingResult.TupleRow = row.Clone();
                        CheckResult[nIndexAOI].MatchingResult.TupleCol = column.Clone();
                    }
                    else
                    {
                        FinallyResult =  false;
                        CheckResult[nIndexAOI].ResultStatus = SwapResult == false ? false : true;

                        HOperatorSet.SetColor(ImageProcessWnd.hWindow, SwapResult == false ? "red" : "green");
                        HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                        HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, SwapResult == false ? $"Part NG({((double)totalArea).ToString("F2")})" : $"Part OK({((double)totalArea).ToString("F2")})", "image", CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol, SwapResult == false ? "red" : "forest green", "true");
                        continue;
                    }
                }

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
