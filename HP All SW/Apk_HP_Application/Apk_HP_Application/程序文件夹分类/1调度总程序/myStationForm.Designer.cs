﻿namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    partial class myStationForm
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtStatus = new DevExpress.XtraEditors.LabelControl();
            this.txtMES1 = new DevExpress.XtraEditors.TextEdit();
            this.txtPCBSN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtTRAYID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtMES2 = new DevExpress.XtraEditors.TextEdit();
            this.txtTRAYID2 = new DevExpress.XtraEditors.TextEdit();
            this.txtPCBSN2 = new DevExpress.XtraEditors.TextEdit();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.LED_Status = new HslCommunication.Controls.UserLantern();
            ((System.ComponentModel.ISupportInitialize)(this.txtMES1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTRAYID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMES2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTRAYID2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtStatus
            // 
            this.txtStatus.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.txtStatus.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtStatus.Appearance.Options.UseBackColor = true;
            this.txtStatus.Appearance.Options.UseFont = true;
            this.txtStatus.Appearance.Options.UseTextOptions = true;
            this.txtStatus.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtStatus.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtStatus.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatus.AppearanceDisabled.Options.UseFont = true;
            this.txtStatus.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatus.AppearanceHovered.Options.UseFont = true;
            this.txtStatus.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtStatus.AppearancePressed.Options.UseFont = true;
            this.txtStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.txtStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.txtStatus.Location = new System.Drawing.Point(58, 79);
            this.txtStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(164, 24);
            this.txtStatus.TabIndex = 112;
            this.txtStatus.Text = "Ready.....";
            this.txtStatus.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.txtStatus.MouseHover += new System.EventHandler(this.frm_MouseHover);
            // 
            // txtMES1
            // 
            this.txtMES1.EditValue = "N/A";
            this.txtMES1.Location = new System.Drawing.Point(58, 53);
            this.txtMES1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMES1.Name = "txtMES1";
            this.txtMES1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtMES1.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtMES1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtMES1.Properties.Appearance.Options.UseBackColor = true;
            this.txtMES1.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMES1.Properties.Appearance.Options.UseFont = true;
            this.txtMES1.Properties.Appearance.Options.UseForeColor = true;
            this.txtMES1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMES1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES1.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtMES1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtMES1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES1.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES1.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtMES1.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtMES1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES1.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtMES1.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtMES1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES1.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES1.Properties.ReadOnly = true;
            this.txtMES1.Size = new System.Drawing.Size(81, 20);
            this.txtMES1.TabIndex = 110;
            this.txtMES1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.txtMES1.MouseHover += new System.EventHandler(this.frm_MouseHover);
            // 
            // txtPCBSN
            // 
            this.txtPCBSN.EditValue = "N/A";
            this.txtPCBSN.Location = new System.Drawing.Point(58, 28);
            this.txtPCBSN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBSN.Name = "txtPCBSN";
            this.txtPCBSN.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBSN.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBSN.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBSN.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseFont = true;
            this.txtPCBSN.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.ReadOnly = true;
            this.txtPCBSN.Size = new System.Drawing.Size(81, 20);
            this.txtPCBSN.TabIndex = 109;
            this.txtPCBSN.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.txtPCBSN.MouseHover += new System.EventHandler(this.frm_MouseHover);
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl15.AppearanceDisabled.Options.UseFont = true;
            this.labelControl15.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl15.AppearanceHovered.Options.UseFont = true;
            this.labelControl15.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl15.AppearancePressed.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(4, 57);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(51, 17);
            this.labelControl15.TabIndex = 108;
            this.labelControl15.Text = "校验结果:";
            this.labelControl15.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.labelControl15.MouseHover += new System.EventHandler(this.frm_MouseHover);
            // 
            // txtTRAYID
            // 
            this.txtTRAYID.EditValue = "N/A";
            this.txtTRAYID.Location = new System.Drawing.Point(58, 3);
            this.txtTRAYID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTRAYID.Name = "txtTRAYID";
            this.txtTRAYID.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTRAYID.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTRAYID.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTRAYID.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTRAYID.Properties.Appearance.Options.UseBackColor = true;
            this.txtTRAYID.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTRAYID.Properties.Appearance.Options.UseFont = true;
            this.txtTRAYID.Properties.Appearance.Options.UseForeColor = true;
            this.txtTRAYID.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtTRAYID.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTRAYID.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtTRAYID.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTRAYID.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtTRAYID.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTRAYID.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.ReadOnly = true;
            this.txtTRAYID.Size = new System.Drawing.Size(81, 20);
            this.txtTRAYID.TabIndex = 107;
            this.txtTRAYID.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.txtTRAYID.MouseHover += new System.EventHandler(this.frm_MouseHover);
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearanceDisabled.Options.UseFont = true;
            this.labelControl14.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearanceHovered.Options.UseFont = true;
            this.labelControl14.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearancePressed.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(5, 31);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(50, 17);
            this.labelControl14.TabIndex = 106;
            this.labelControl14.Text = "PCB编号:";
            this.labelControl14.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.labelControl14.MouseHover += new System.EventHandler(this.frm_MouseHover);
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearanceDisabled.Options.UseFont = true;
            this.labelControl13.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearanceHovered.Options.UseFont = true;
            this.labelControl13.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearancePressed.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(4, 6);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(51, 17);
            this.labelControl13.TabIndex = 105;
            this.labelControl13.Text = "载板编号:";
            this.labelControl13.Click += new System.EventHandler(this.labelControl13_Click);
            this.labelControl13.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.labelControl13.MouseHover += new System.EventHandler(this.frm_MouseHover);
            // 
            // txtMES2
            // 
            this.txtMES2.EditValue = "N/A";
            this.txtMES2.Location = new System.Drawing.Point(141, 53);
            this.txtMES2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMES2.Name = "txtMES2";
            this.txtMES2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtMES2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtMES2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtMES2.Properties.Appearance.Options.UseBackColor = true;
            this.txtMES2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtMES2.Properties.Appearance.Options.UseFont = true;
            this.txtMES2.Properties.Appearance.Options.UseForeColor = true;
            this.txtMES2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMES2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtMES2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtMES2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtMES2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtMES2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtMES2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtMES2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtMES2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtMES2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtMES2.Properties.ReadOnly = true;
            this.txtMES2.Size = new System.Drawing.Size(81, 20);
            this.txtMES2.TabIndex = 113;
            // 
            // txtTRAYID2
            // 
            this.txtTRAYID2.EditValue = "N/A";
            this.txtTRAYID2.Location = new System.Drawing.Point(141, 3);
            this.txtTRAYID2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTRAYID2.Name = "txtTRAYID2";
            this.txtTRAYID2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTRAYID2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTRAYID2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtTRAYID2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTRAYID2.Properties.Appearance.Options.UseBackColor = true;
            this.txtTRAYID2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTRAYID2.Properties.Appearance.Options.UseFont = true;
            this.txtTRAYID2.Properties.Appearance.Options.UseForeColor = true;
            this.txtTRAYID2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTRAYID2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtTRAYID2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTRAYID2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTRAYID2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtTRAYID2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTRAYID2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTRAYID2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtTRAYID2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTRAYID2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTRAYID2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID2.Properties.ReadOnly = true;
            this.txtTRAYID2.Size = new System.Drawing.Size(81, 20);
            this.txtTRAYID2.TabIndex = 114;
            // 
            // txtPCBSN2
            // 
            this.txtPCBSN2.EditValue = "N/A";
            this.txtPCBSN2.Location = new System.Drawing.Point(141, 28);
            this.txtPCBSN2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPCBSN2.Name = "txtPCBSN2";
            this.txtPCBSN2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBSN2.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBSN2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBSN2.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseFont = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBSN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN2.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 6.75F);
            this.txtPCBSN2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBSN2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBSN2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN2.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN2.Properties.ReadOnly = true;
            this.txtPCBSN2.Size = new System.Drawing.Size(81, 20);
            this.txtPCBSN2.TabIndex = 115;
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // LED_Status
            // 
            this.LED_Status.BackColor = System.Drawing.Color.Transparent;
            this.LED_Status.Location = new System.Drawing.Point(7, 79);
            this.LED_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LED_Status.Name = "LED_Status";
            this.LED_Status.Size = new System.Drawing.Size(24, 24);
            this.LED_Status.TabIndex = 117;
            // 
            // myStationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LED_Status);
            this.Controls.Add(this.txtPCBSN2);
            this.Controls.Add(this.txtTRAYID2);
            this.Controls.Add(this.txtMES2);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.txtMES1);
            this.Controls.Add(this.txtPCBSN);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.txtTRAYID);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.labelControl13);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "myStationForm";
            this.Size = new System.Drawing.Size(226, 108);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.frm_mouseDoubleClick);
            this.MouseLeave += new System.EventHandler(this.frm_MouseLeave);
            this.MouseHover += new System.EventHandler(this.frm_MouseHover);
            ((System.ComponentModel.ISupportInitialize)(this.txtMES1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTRAYID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMES2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTRAYID2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN2.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl txtStatus;
        private DevExpress.XtraEditors.TextEdit txtMES1;
        private DevExpress.XtraEditors.TextEdit txtPCBSN;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtTRAYID;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit txtMES2;
        private DevExpress.XtraEditors.TextEdit txtTRAYID2;
        private DevExpress.XtraEditors.TextEdit txtPCBSN2;
        private System.Windows.Forms.Timer timer1;
        private HslCommunication.Controls.UserLantern LED_Status;
    }
}
