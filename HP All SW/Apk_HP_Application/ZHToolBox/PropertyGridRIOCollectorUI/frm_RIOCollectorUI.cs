﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZHToolBox.HalconVision;
using HalconDotNet;

namespace ZHToolBox.PropertyGridRIOCollectorUI
{
    public partial class frm_RIOCollectorUI : Form
    {
        public class DataProperty
        {
            [TypeConverter(typeof(ExpandableObjectConverter)), Category("RIO Setting"), DisplayName("CheckResultDef")]
            public CheckResultDef[] CheckResult { get; set; }

            public DataProperty()
            {
                CheckResult = null;
            }
        }

        public DataProperty rapteClass = new DataProperty();

        public HalconForm Camera = new HalconForm("图像窗口");

        public frm_RIOCollectorUI()
        {
            InitializeComponent();
            Size size = panel1.Size;
            Camera.TopLevel = false;
            Camera.FormBorderStyle = FormBorderStyle.None;
            panel1.Controls.Add(Camera);
            Camera.Size = size;
            Camera.Show();
            Camera.SendToBack();
            Camera.BringToFront();
            Camera.hAcqDriverName = "HuaRay";
            Camera.cameraName = "cam0";
            Camera.nPort = 0;

            comboBox1.SelectedIndex = 0;

            Text = "请先打开一张图片，再先技相应的RIO";
        }

        private void UpdateValues()
        {
            if (rapteClass.CheckResult != null)
            {
                listView1.Items.Clear();
                Application.DoEvents();

                for (int nIndex = 0; nIndex < rapteClass.CheckResult.Length; nIndex++)
                {
                    string name = rapteClass.CheckResult[nIndex].CheckedRIO.szItemRemark;
                    string ValueName = rapteClass.CheckResult[nIndex].CheckedRIO.fRow.ToString("F2") + ", " + rapteClass.CheckResult[nIndex].CheckedRIO.fCol.ToString("F2");
                    listView1.Items.Add(name.Length <= 0 ? ValueName : name);
                }

                propertyGrid1.SelectedObject = rapteClass;
            }
        }

        ~frm_RIOCollectorUI()
        {
            Camera.Close();
        }

        private void AddRIO_Click(object sender, EventArgs e)
        {
            HDVisionTypes.RIO_DES rio = Camera.GetCurrentRIO(0);

            if(rapteClass.CheckResult != null)
            {
                CheckResultDef[] newRIOCheck = new CheckResultDef[1];
                newRIOCheck[0] = new CheckResultDef();
                newRIOCheck[0].CheckedRIO = rio;

                rapteClass.CheckResult = rapteClass.CheckResult.Concat(newRIOCheck).ToArray();

                string name = newRIOCheck[0].CheckedRIO.szItemRemark;
                listView1.Items.Add(name.Length <= 0 ? newRIOCheck[0].CheckedRIO.fRow.ToString("F2") + ", " + newRIOCheck[0].CheckedRIO.fCol.ToString("F2") : name);

                UpdateValues();
            }
        }

        private void DeleteRIO_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count <= 0)
                return;

            if (listView1.Items.Count <= 1)
                MessageBox.Show(this, "至少保留一个项目", "错误", MessageBoxButtons.OK, MessageBoxIcon.Information);

            int nCur = listView1.SelectedIndices[0];
            listView1.Items.RemoveAt(nCur);

            List<CheckResultDef> objectValue = rapteClass.CheckResult.ToList();
            objectValue.RemoveAt(nCur);

            rapteClass.CheckResult = objectValue.ToArray();

            UpdateValues();
        }

        private void UpdateRIO_Click(object sender, EventArgs e)
        {
            HDVisionTypes.RIO_DES rio = Camera.GetCurrentRIO(0);

            if (listView1.SelectedItems.Count <= 0)
                return;

            int nCur = listView1.SelectedIndices[0];
            rapteClass.CheckResult[nCur].CheckedRIO = rio;

            UpdateValues();

        }

        private void frm_RIOCollectorUI_Load(object sender, EventArgs e)
        {
            UpdateValues();
        }

        private void btmAutoRowCol_Click(object sender, EventArgs e)
        {
            try
            {
                int rowCount = int.Parse(txtRow.Text.Trim());
                int colCount = int.Parse(txtCol.Text.Trim());

                int nCur = comboBox1.SelectedIndex;

                if (rowCount <= 0 || colCount <= 0)
                    return;

                HDVisionTypes.RIO_DES[] rioArry = new HDVisionTypes.RIO_DES[rowCount * colCount];
                HDVisionTypes.RIO_DES rio = Camera.GetCurrentRIO(0);

                HObject rec;

                HOperatorSet.ClearWindow(Camera.hWindow);
                HOperatorSet.DispObj(Camera.hImage, Camera.hWindow);

                HOperatorSet.SetColor(Camera.hWindow, "green");
                HOperatorSet.GenRectangle2(out rec,
                                                                     rio.fRow,
                                                                     rio.fCol,
                                                                     (new HTuple(rio.fPhi)).TupleRad(),
                                                                     rio.fLen1,
                                                                     rio.fLen2);
                HOperatorSet.DispObj(rec, Camera.hWindow);
                HOperatorSet.GenCrossContourXld(out rec, rio.fRow, rio.fCol, 40, 0);
                HOperatorSet.DispObj(rec, Camera.hWindow);

                double colStart = rio.fCol - rio.fLen1 <= 0 ? 0 : rio.fCol - rio.fLen1;
                double rowStart = rio.fRow - rio.fLen2 <= 0 ? 0 : rio.fRow - rio.fLen2;

                double fLenWithd = rio.fLen1 * 2;
                double fLenHeight = rio.fLen2 * 2;

                double diffWithd = fLenWithd / colCount;
                double diffHeight = fLenHeight / rowCount;

                int nStep = 0;

                #region>>>>>>>> 左上右下，往右 0
                if(comboBox1.SelectedIndex == 0)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexRow = 0; nIndexRow < rowCount; nIndexRow++)
                    {
                        for (int nIndexCol = 0; nIndexCol < colCount; nIndexCol++)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }


                #endregion

                #region>>>>>>>> 左上右下，往下 1
                else if (comboBox1.SelectedIndex == 1)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexCol = 0; nIndexCol < colCount; nIndexCol++)
                    {
                        for (int nIndexRow = 0; nIndexRow < rowCount; nIndexRow++)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }

                #endregion

                #region>>>>>>>> 右上左下，往左 2
                else if (comboBox1.SelectedIndex == 2)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexRow = 0; nIndexRow < rowCount; nIndexRow++)
                    {
                        for (int nIndexCol = colCount - 1; nIndexCol >= 0; nIndexCol--)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }

                #endregion

                #region>>>>>>>>右上左下，往下 3
                else if (comboBox1.SelectedIndex == 3)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexCol = colCount - 1; nIndexCol >= 0; nIndexCol--)
                    {
                        for (int nIndexRow = 0; nIndexRow < rowCount; nIndexRow++)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }


                #endregion

                #region>>>>>>>> 左下右上，往右 4
                else if (comboBox1.SelectedIndex == 4)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexRow = rowCount - 1; nIndexRow >= 0; nIndexRow--)
                    {
                        for (int nIndexCol = 0; nIndexCol < colCount; nIndexCol++)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }

                #endregion

                #region>>>>>>>> 左下右上，往上5
                else if (comboBox1.SelectedIndex == 5)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexCol = 0; nIndexCol < colCount; nIndexCol++)
                    {
                        for (int nIndexRow = rowCount - 1; nIndexRow >= 0; nIndexRow--)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }

                #endregion

                #region>>>>>>>> 右下左上, 往左 6
                else if (comboBox1.SelectedIndex == 6)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexRow = rowCount - 1; nIndexRow >= 0; nIndexRow--)
                    {
                        for (int nIndexCol = colCount - 1; nIndexCol >=0 ; nIndexCol--)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }


                #endregion

                #region>>>>>>>> 右下左上, 往上 7
                else if (comboBox1.SelectedIndex == 7)
                {
                    HOperatorSet.SetColor(Camera.hWindow, "blue");
                    for (int nIndexCol = colCount - 1; nIndexCol >= 0; nIndexCol--)
                    {
                        for (int nIndexRow = rowCount - 1; nIndexRow >= 0; nIndexRow--)
                        {
                            rioArry[nStep] = new HDVisionTypes.RIO_DES();
                            rioArry[nStep].fRow = rowStart + (diffHeight * nIndexRow) + diffHeight / 2;
                            rioArry[nStep].fCol = colStart + (diffWithd * nIndexCol) + diffWithd / 2;
                            rioArry[nStep].fLen1 = diffWithd / 2;
                            rioArry[nStep].fLen2 = diffHeight / 2;
                            rioArry[nStep].fPhi = 0.0;

                            HalconGeneral.HD_DisplayMessage(Camera.hWindow, (nStep + 1).ToString(), "image",
                                                                                       rioArry[nStep].fRow + 20,
                                                                                        rioArry[nStep].fCol + 20,
                                                                                        "red", "true");

                            HOperatorSet.GenRectangle2(out rec,
                                                                        rioArry[nStep].fRow,
                                                                        rioArry[nStep].fCol,
                                                                        (new HTuple(rioArry[nStep].fPhi)).TupleRad(),
                                                                        rioArry[nStep].fLen1,
                                                                        rioArry[nStep].fLen2);
                            HOperatorSet.DispObj(rec, Camera.hWindow);
                            HOperatorSet.GenCrossContourXld(out rec, rioArry[nStep].fRow, rioArry[nStep].fCol, 40, 0);
                            HOperatorSet.DispObj(rec, Camera.hWindow);

                            nStep++;
                        }
                    }
                }


                #endregion

                rapteClass.CheckResult = new CheckResultDef[rioArry.Length];
                for(int nIndex = 0; nIndex < rioArry.Length; nIndex++)
                {
                    rapteClass.CheckResult[nIndex] = new CheckResultDef();
                    rapteClass.CheckResult[nIndex].CheckedRIO = rioArry[nIndex];
                }

                UpdateValues();

            }
            catch (Exception ex)
            {

            }
        }

        private void panel1_Resize(object sender, EventArgs e)
        {

        }

        private void frm_RIOCollectorUI_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void frm_RIOCollectorUI_SizeChanged(object sender, EventArgs e)
        {
            panel1.Location = new Point(0, 0);
            Camera.Location = new Point(0, 0);

            panel3.Location = new Point(this.ClientSize.Width - panel3.Width, 0);

            panel1.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
            Camera.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
        }

        private void frm_RIOCollectorUI_ResizeEnd(object sender, EventArgs e)
        {
            Size size = panel1.Size;
            Size size2 = this.Size;

            panel1.Location = new Point(0, 0);
            Camera.Location = new Point(0, 0);

            panel3.Location = new Point(this.ClientSize.Width - panel3.Width, 0);

            panel1.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
            Camera.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
        }

        private void frm_RIOCollectorUI_Resize(object sender, EventArgs e)
        {

        }

        private void frm_RIOCollectorUI_MaximumSizeChanged(object sender, EventArgs e)
        {
            panel1.Location = new Point(0, 0);
            Camera.Location = new Point(0, 0);

            panel3.Location = new Point(this.ClientSize.Width - panel3.Width, 0);

            panel1.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
            Camera.Size = new Size(this.ClientSize.Width - panel3.Size.Width, this.Height);
        }
    }
}
