﻿namespace Apk_HP_Application.通讯及接口
{
    partial class HalconForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HalconForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Rectangle1 = new System.Windows.Forms.ToolStripButton();
            this.Rectangle2 = new System.Windows.Forms.ToolStripButton();
            this.Circle = new System.Windows.Forms.ToolStripButton();
            this.Clear = new System.Windows.Forms.ToolStripButton();
            this.ZoonOut = new System.Windows.Forms.ToolStripButton();
            this.ZoonIn = new System.Windows.Forms.ToolStripButton();
            this.Zoom100 = new System.Windows.Forms.ToolStripButton();
            this.Open = new System.Windows.Forms.ToolStripButton();
            this.Save = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.S1 = new System.Windows.Forms.ToolStripButton();
            this.S2 = new System.Windows.Forms.ToolStripButton();
            this.RotateD = new System.Windows.Forms.ToolStripButton();
            this.RotateI = new System.Windows.Forms.ToolStripButton();
            this.ZoonPanel = new System.Windows.Forms.ToolStripButton();
            this.Move = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.Setting = new System.Windows.Forms.ToolStripButton();
            this.MainPanel = new System.Windows.Forms.ToolStripButton();
            this.Grab = new System.Windows.Forms.ToolStripButton();
            this.Snap = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Rectangle1,
            this.Rectangle2,
            this.Circle,
            this.toolStripSeparator1,
            this.Clear,
            this.toolStripSeparator2,
            this.ZoonOut,
            this.ZoonIn,
            this.Zoom100,
            this.ZoonPanel,
            this.RotateD,
            this.RotateI,
            this.toolStripSeparator3,
            this.Move,
            this.toolStripSeparator4,
            this.Open,
            this.Save,
            this.toolStripSeparator5,
            this.S1,
            this.S2,
            this.toolStripSeparator6,
            this.MainPanel,
            this.toolStripSeparator7,
            this.Snap,
            this.Grab,
            this.toolStripSeparator8,
            this.Setting});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(683, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Rectangle1
            // 
            this.Rectangle1.CheckOnClick = true;
            this.Rectangle1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Rectangle1.Image = ((System.Drawing.Image)(resources.GetObject("Rectangle1.Image")));
            this.Rectangle1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Rectangle1.Name = "Rectangle1";
            this.Rectangle1.Size = new System.Drawing.Size(23, 22);
            this.Rectangle1.Text = "toolStripButton1";
            this.Rectangle1.ToolTipText = "在图上画一个正矩形框";
            this.Rectangle1.Click += new System.EventHandler(this.Rectangle1_Click);
            // 
            // Rectangle2
            // 
            this.Rectangle2.CheckOnClick = true;
            this.Rectangle2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Rectangle2.Image = ((System.Drawing.Image)(resources.GetObject("Rectangle2.Image")));
            this.Rectangle2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Rectangle2.Name = "Rectangle2";
            this.Rectangle2.Size = new System.Drawing.Size(23, 22);
            this.Rectangle2.Text = "toolStripButton2";
            this.Rectangle2.ToolTipText = "在图上画一个任一角度的矩形框";
            this.Rectangle2.Click += new System.EventHandler(this.Rectangle2_Click);
            // 
            // Circle
            // 
            this.Circle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Circle.Image = ((System.Drawing.Image)(resources.GetObject("Circle.Image")));
            this.Circle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Circle.Name = "Circle";
            this.Circle.Size = new System.Drawing.Size(23, 22);
            this.Circle.Text = "toolStripButton3";
            this.Circle.ToolTipText = "在图上画一个圆";
            this.Circle.Click += new System.EventHandler(this.Circle_Click);
            // 
            // Clear
            // 
            this.Clear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Clear.Image = ((System.Drawing.Image)(resources.GetObject("Clear.Image")));
            this.Clear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(23, 22);
            this.Clear.Text = "toolStripButton4";
            this.Clear.ToolTipText = "清除所有框只保留图片";
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // ZoonOut
            // 
            this.ZoonOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoonOut.Image = ((System.Drawing.Image)(resources.GetObject("ZoonOut.Image")));
            this.ZoonOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoonOut.Name = "ZoonOut";
            this.ZoonOut.Size = new System.Drawing.Size(23, 22);
            this.ZoonOut.Text = "toolStripButton5";
            this.ZoonOut.ToolTipText = "放大图片";
            this.ZoonOut.Click += new System.EventHandler(this.ZoonOut_Click);
            // 
            // ZoonIn
            // 
            this.ZoonIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoonIn.Image = ((System.Drawing.Image)(resources.GetObject("ZoonIn.Image")));
            this.ZoonIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoonIn.Name = "ZoonIn";
            this.ZoonIn.Size = new System.Drawing.Size(23, 22);
            this.ZoonIn.Text = "toolStripButton6";
            this.ZoonIn.ToolTipText = "缩小图片";
            this.ZoonIn.Click += new System.EventHandler(this.ZoonIn_Click);
            // 
            // Zoom100
            // 
            this.Zoom100.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Zoom100.Image = ((System.Drawing.Image)(resources.GetObject("Zoom100.Image")));
            this.Zoom100.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Zoom100.Name = "Zoom100";
            this.Zoom100.Size = new System.Drawing.Size(23, 22);
            this.Zoom100.Text = "toolStripButton7";
            this.Zoom100.ToolTipText = "适合图片大小";
            this.Zoom100.Click += new System.EventHandler(this.Zoom100_Click);
            // 
            // Open
            // 
            this.Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Open.Image = ((System.Drawing.Image)(resources.GetObject("Open.Image")));
            this.Open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Open.Name = "Open";
            this.Open.Size = new System.Drawing.Size(23, 22);
            this.Open.Text = "toolStripButton8";
            this.Open.ToolTipText = "从文件中打开图片";
            this.Open.Click += new System.EventHandler(this.Open_Click);
            // 
            // Save
            // 
            this.Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Save.Image = ((System.Drawing.Image)(resources.GetObject("Save.Image")));
            this.Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(23, 22);
            this.Save.Text = "toolStripButton9";
            this.Save.ToolTipText = "保存图片到文件";
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(683, 519);
            this.panel1.TabIndex = 1;
            this.panel1.SizeChanged += new System.EventHandler(this.panel_SizeChanged);
            this.panel1.Resize += new System.EventHandler(this.panel_Size);
            // 
            // S1
            // 
            this.S1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.S1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.S1.Name = "S1";
            this.S1.Size = new System.Drawing.Size(45, 22);
            this.S1.Text = "RIO 1";
            this.S1.ToolTipText = "当前RIO选择1号";
            this.S1.Click += new System.EventHandler(this.S1_Click);
            // 
            // S2
            // 
            this.S2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.S2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.S2.Name = "S2";
            this.S2.Size = new System.Drawing.Size(45, 22);
            this.S2.Text = "RIO 2";
            this.S2.ToolTipText = "当前RIO选择2号";
            this.S2.Click += new System.EventHandler(this.S2_Click);
            // 
            // RotateD
            // 
            this.RotateD.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RotateD.Image = ((System.Drawing.Image)(resources.GetObject("RotateD.Image")));
            this.RotateD.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RotateD.Name = "RotateD";
            this.RotateD.Size = new System.Drawing.Size(23, 22);
            this.RotateD.Text = "toolStripButton1";
            this.RotateD.ToolTipText = "图片逆时针旋转90度";
            // 
            // RotateI
            // 
            this.RotateI.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RotateI.Image = ((System.Drawing.Image)(resources.GetObject("RotateI.Image")));
            this.RotateI.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RotateI.Name = "RotateI";
            this.RotateI.Size = new System.Drawing.Size(23, 22);
            this.RotateI.Text = "toolStripButton2";
            this.RotateI.ToolTipText = "图片顺时针旋转90度";
            // 
            // ZoonPanel
            // 
            this.ZoonPanel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoonPanel.Image = ((System.Drawing.Image)(resources.GetObject("ZoonPanel.Image")));
            this.ZoonPanel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoonPanel.Name = "ZoonPanel";
            this.ZoonPanel.Size = new System.Drawing.Size(23, 22);
            this.ZoonPanel.Text = "toolStripButton1";
            this.ZoonPanel.ToolTipText = "适合窗口大小";
            // 
            // Move
            // 
            this.Move.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Move.Image = ((System.Drawing.Image)(resources.GetObject("Move.Image")));
            this.Move.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Move.Name = "Move";
            this.Move.Size = new System.Drawing.Size(23, 22);
            this.Move.Text = "Move";
            this.Move.ToolTipText = "移动图形";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // Setting
            // 
            this.Setting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Setting.Image = ((System.Drawing.Image)(resources.GetObject("Setting.Image")));
            this.Setting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Setting.Name = "Setting";
            this.Setting.Size = new System.Drawing.Size(23, 22);
            this.Setting.Text = "toolStripButton1";
            this.Setting.ToolTipText = "设置与参数";
            // 
            // MainPanel
            // 
            this.MainPanel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MainPanel.Image = ((System.Drawing.Image)(resources.GetObject("MainPanel.Image")));
            this.MainPanel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(23, 22);
            this.MainPanel.Text = "toolStripButton1";
            this.MainPanel.ToolTipText = "回到/离开主窗口";
            this.MainPanel.Click += new System.EventHandler(this.MainPanel_Click);
            // 
            // Grab
            // 
            this.Grab.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Grab.Image = ((System.Drawing.Image)(resources.GetObject("Grab.Image")));
            this.Grab.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Grab.Name = "Grab";
            this.Grab.Size = new System.Drawing.Size(23, 22);
            this.Grab.Text = "连续采集";
            this.Grab.Click += new System.EventHandler(this.Grab_Click);
            // 
            // Snap
            // 
            this.Snap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Snap.Image = ((System.Drawing.Image)(resources.GetObject("Snap.Image")));
            this.Snap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Snap.Name = "Snap";
            this.Snap.Size = new System.Drawing.Size(23, 22);
            this.Snap.Text = "toolStripButton2";
            this.Snap.ToolTipText = "单次采集";
            this.Snap.Click += new System.EventHandler(this.Snap_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // HalconForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 545);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "HalconForm";
            this.Text = "HalconForm";
            this.ResizeEnd += new System.EventHandler(this.form_ReSizeEnd);
            this.Resize += new System.EventHandler(this.form_Size);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton Rectangle1;
        private System.Windows.Forms.ToolStripButton Rectangle2;
        private System.Windows.Forms.ToolStripButton Circle;
        private System.Windows.Forms.ToolStripButton Clear;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripButton ZoonOut;
        private System.Windows.Forms.ToolStripButton ZoonIn;
        private System.Windows.Forms.ToolStripButton Zoom100;
        private System.Windows.Forms.ToolStripButton Open;
        private System.Windows.Forms.ToolStripButton Save;
        private System.Windows.Forms.ToolStripButton S1;
        private System.Windows.Forms.ToolStripButton S2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton ZoonPanel;
        private System.Windows.Forms.ToolStripButton RotateD;
        private System.Windows.Forms.ToolStripButton RotateI;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton Move;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton Setting;
        private System.Windows.Forms.ToolStripButton MainPanel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton Snap;
        private System.Windows.Forms.ToolStripButton Grab;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    }
}