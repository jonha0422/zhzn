﻿namespace CamaeraLib
{
    partial class CameraParaFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraParaFrm));
            this.ucCameraPara1 = new CamaeraLib.ucCameraPara();
            this.bClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ucCameraPara1
            // 
            this.ucCameraPara1.Location = new System.Drawing.Point(9, 17);
            this.ucCameraPara1.Name = "ucCameraPara1";
            this.ucCameraPara1.Size = new System.Drawing.Size(294, 287);
            this.ucCameraPara1.TabIndex = 0;
            // 
            // bClose
            // 
            this.bClose.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bClose.Location = new System.Drawing.Point(141, 247);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(134, 56);
            this.bClose.TabIndex = 1;
            this.bClose.Text = "退出";
            this.bClose.UseVisualStyleBackColor = true;
            this.bClose.Click += new System.EventHandler(this.bClose_Click);
            // 
            // CameraParaFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 348);
            this.Controls.Add(this.bClose);
            this.Controls.Add(this.ucCameraPara1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CameraParaFrm";
            this.Text = "CameraParaFrm";
            this.Load += new System.EventHandler(this.CameraParaFrm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ucCameraPara ucCameraPara1;
        private System.Windows.Forms.Button bClose;
    }
}