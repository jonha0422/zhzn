﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application.程序文件夹分类._3打码功割控制程序
{
    public partial class frm_LaserStationMain : DevExpress.XtraEditors.XtraForm
    {
        public frm_LaserStationMain()
        {
            InitializeComponent();

            //连接正运动控制卡和IO卡
            if(!HPLaserFun.InitializeHardware())
            {
                MessageBox.Show("初始化打码切割位硬件失败", "错误", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public void thread_foLoading(object wsarg)
        {
            DateTime timer1 = DateTime.Now;
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(200);
                try
                {
                    #region>>>>>升起阻挡气缸
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLoad, $"{DateTime.Now.ToLongTimeString()}>>>升起阻挡气缸", Color.Blue, 9);
                    HPLaserFun.Action_Load_CylinderStop(true);
                    Thread.Sleep(300);
                    HPLaserFun.Action_LoadLineControl(true);
                    #endregion

                    #region>>>>>等待有料
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLoad, $"{DateTime.Now.ToLongTimeString()}>>>等待有料", Color.Blue, 9);
                    while (true)
                    {
                        if (HPglobal.m_bQuitApp)
                            return;

                        if(HPLaserFun.IsLoadPosHasTray())
                        {
                            Thread.Sleep(500);
                            HPLaserFun.Action_LoadLineControl(false);
                            break;
                        }

                    }
                    #endregion

                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLoad, "clear", Color.Blue, 9);

                    #region>>>>>等待送板
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLoad, $"{DateTime.Now.ToLongTimeString()}>>>等待送板", Color.Blue, 9);
                    while (true)
                    {
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.bAllowAcceptPCB_LaserMark)
                            break;
                    }

                    HPLaserFun.Action_LoadLineControl(true);
                    HPLaserFun.Action_Load_CylinderStop(false);
                    #endregion

                    #region>>>>>等待送板完成与下个工位接料完成
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLoad, $"{DateTime.Now.ToLongTimeString()}>>>等待送板完成与下个工位接料完成", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(50);
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.bAcceptPCBCompleted_LaserMark && 
                            !HPLaserFun.IsLoadPosHasTray())
                            break;
                    }
                    #endregion

                    DateTime timer2 = DateTime.Now;
                    TimeSpan timeSpan = timer2 - timer1;
                    double timeUsed = timeSpan.TotalMilliseconds;


                }
                catch (Exception ex)
                {
                    HPglobal.RecordeAllMessage("thread_forDataSwap_A 调用异常", true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                }
            }

            //退出线程

        }

        public void thread_forLaserMarking(object wsarg)
        {
            DateTime timer1 = DateTime.Now;
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(200);

                try
                {
                    //复位标志
                    HPLaserFun.bAllowAcceptPCB_LaserMark = true;
                    HPLaserFun.bAcceptPCBCompleted_LaserMark = false;

                    #region>>>>>升起阻挡气缸
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserMark, $"{DateTime.Now.ToLongTimeString()}>>>升起阻挡气缸", Color.Blue, 9);
                    HPLaserFun.Action_LaserMark_CylinderStop(true);
                    Thread.Sleep(300);
                    HPLaserFun.Action_LoadLineControl(true);
                    #endregion

                    #region>>>>>等待有料
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserMark, $"{DateTime.Now.ToLongTimeString()}>>>等待有料", Color.Blue, 9);
                    while (true)
                    {
                        if (HPglobal.m_bQuitApp)
                            return;

                        Thread.Sleep(50);
                        HPLaserFun.Action_LoadLineControl(true);
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.IsLaserMarkingPosHasTray())
                        {
                            Thread.Sleep(500);
                            HPLaserFun.Action_LoadLineControl(false);
                            break;
                        }
                    }

                    HPLaserFun.bAllowAcceptPCB_LaserMark = false;
                    HPLaserFun.bAcceptPCBCompleted_LaserMark = true;
                    #endregion

                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserMark, "clear", Color.Blue, 9);
                    #region>>>>>开始进行打码工作

                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserMark, $"{DateTime.Now.ToLongTimeString()}>>>有料进入, 开始打码", Color.Blue, 9);

                    #endregion

                    #region>>>>>等待送板
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserMark, $"{DateTime.Now.ToLongTimeString()}>>>等待送板", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(50);
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.bAllowAcceptPCB_LaserCuuting)
                            break;
                    }

                    HPLaserFun.Action_LoadLineControl(true);
                    HPLaserFun.Action_LaserMark_CylinderStop(false);
                    #endregion

                    #region>>>>>等待送板完成与下个工位接料完成
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserMark, $"{DateTime.Now.ToLongTimeString()}>>>等待送板完成与下个工位接料完成", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(50);
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.bAcceptPCBCompleted_LaserCuuting &&
                            !HPLaserFun.IsLaserMarkingPosHasTray())
                            break;
                    }
                    #endregion

                    DateTime timer2 = DateTime.Now;
                    TimeSpan timeSpan = timer2 - timer1;
                    double timeUsed = timeSpan.TotalMilliseconds;


                }
                catch (Exception ex)
                {
                    HPglobal.RecordeAllMessage("thread_forDataSwap_A 调用异常", true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                }
            }

            //退出线程

        }

        public void thread_forRotate(object wsarg)
        {
            DateTime timer1 = DateTime.Now;
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(200);
                try
                {
                    DateTime timer2 = DateTime.Now;
                    TimeSpan timeSpan = timer2 - timer1;
                    double timeUsed = timeSpan.TotalMilliseconds;
                }
                catch (Exception ex)
                {
                    HPglobal.RecordeAllMessage("thread_forDataSwap_A 调用异常", true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                }
            }

            //退出线程

        }

        public void thread_forLaserCutting(object wsarg)
        {
            DateTime timer1 = DateTime.Now;
            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(200);
                try
                {
                    //复位标志
                    HPLaserFun.bAllowAcceptPCB_LaserCuuting = true;
                    HPLaserFun.bAcceptPCBCompleted_LaserCuuting = false;

                    #region>>>>>升起阻挡气缸
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>升起阻挡气缸", Color.Blue, 9);
                    if (HPLaserFun.EnableRotate)
                    {
                        HPLaserFun.Action_Rotate_CylinderStop(true);
                    }
                    else
                        HPLaserFun.Action_Rotate_CylinderStop(false);

                    HPLaserFun.Action_LaserCutting_CylinderStop(true);
                    Thread.Sleep(300);
                    HPLaserFun.Action_LaserCuttingLineControl(true);

                    #endregion

                    #region>>>>>确认是否启用翻转
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>确认是否启用翻转", Color.Blue, 9);
                    if (HPLaserFun.EnableRotate)
                    {
                        #region>>>>>等待翻转有料
                        HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>等待翻转有料", Color.Blue, 9);
                        while (true)
                        {
                            if (HPglobal.m_bQuitApp)
                                return;

                            Thread.Sleep(50);
                            HPLaserFun.Action_LoadLineControl(true);
                            if (HPglobal.m_bQuitApp)
                                return;

                            if (HPLaserFun.IsRotatePosHasTray())
                            {
                                Thread.Sleep(1000);
                                HPLaserFun.Action_LaserMark_CylinderStop(true);
                                HPLaserFun.Action_LoadLineControl(false);
                                break;
                            }

                        }
                        #endregion

                        HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, "clear", Color.Blue, 9);

                        #region>>>>>翻转PCB
                        HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>翻转PCB", Color.Blue, 9);
                        HPLaserFun.Action_RotatePCB(HPLaserFun.AutoRotate);
                        #endregion
                    }
                    #endregion

                    #region>>>>>从翻转位送板到切割位
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>从翻转位送板到切割位", Color.Blue, 9);
                    HPLaserFun.Action_Rotate_CylinderStop(false);
                    HPLaserFun.Action_LoadLineControl(true);
                    #endregion

                    #region>>>>>等待切割位有料
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>等待切割位有料", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(50);
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.IsXYPosHasTray())
                        {
                            Thread.Sleep(500);
                            HPLaserFun.Action_RotateLineControl(false);
                            HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>切割位有料", Color.Blue, 9);
                            break;
                        }
                    }

                    HPLaserFun.bAllowAcceptPCB_LaserCuuting = false;
                    HPLaserFun.bAcceptPCBCompleted_LaserCuuting = true;
                    #endregion

                    #region>>>>>开始进行切割工作
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>开始进行切割工作", Color.Blue, 9);
                    HPLaserFun.Action_LaserCuttingLineControl(false);
                    Task RotateTask = Task.Factory.StartNew(() => HPLaserFun.Action_Rotate_CylinderRotate(false));
                    Thread.Sleep(3000);

                    //等待翻转工位完成
                    RotateTask.Wait();
                    #endregion

                    #region>>>>>等待送板
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>切割完成，等待送板", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(50);
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.bAllowAcceptPCB_Unload)
                            break;
                    }
                    HPLaserFun.Action_LaserCutting_CylinderStop(false);
                    Thread.Sleep(500);
                    HPLaserFun.Action_LaserCuttingLineControl(true);

                    #endregion

                    #region>>>>>等待送板完成与下个工位接料完成
                    HPglobal.WorkingMessageOutput(ref HPLaserFun.m_queLaserCutting, $"{DateTime.Now.ToLongTimeString()}>>>等待送板完成与下个工位接料完成", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(50);
                        if (HPglobal.m_bQuitApp)
                            return;

                        if (HPLaserFun.bAcceptPCBCompleted_Unload &&
                            !HPLaserFun.IsXYPosHasTray())
                            break;
                    }
                    #endregion

                    DateTime timer2 = DateTime.Now;
                    TimeSpan timeSpan = timer2 - timer1;
                    double timeUsed = timeSpan.TotalMilliseconds;


                }
                catch (Exception ex)
                {
                    HPglobal.RecordeAllMessage("thread_forDataSwap_A 调用异常", true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                }
            }

            //退出线程

        }

        public void DisplayObjData()
        {
            try
            {
                #region>>>>>>ECI0064连机状态
                if (HPLaserFun.hHandleECI0064 == null)
                    userLanternIO.LanternBackground = Color.Gray;
                else
                {
                    userLanternIO.LanternBackground = (userLanternIO.LanternBackground == Color.Green ? Color.Yellow : Color.Green);
                    #region>>>>>>IO 输入的状态

                    HslCommunication.Controls.UserLantern[] arryLEDs = { LED_In0, LED_In1, LED_In2, LED_In3, LED_In4, LED_In5, LED_In6, LED_In7,
                                                                                                                 LED_In8, LED_In9, LED_In10, LED_In11, LED_In12, LED_In13, LED_In14, LED_In15,
                                                                                                                LED_In16, LED_In17, LED_In18, LED_In19, LED_In20, LED_In21, LED_In22, LED_In23,
                                                                                                                LED_In24, LED_In25, LED_In26, LED_In27, LED_In28, LED_In29, LED_In30, LED_In31};
                    for (int nIndex = 0; nIndex < 32; nIndex++)
                    {
                        uint nValue = 0;
                        zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI0064, nIndex, ref nValue);
                        arryLEDs[nIndex].LanternBackground = (nValue != 0 ? Color.Red : Color.Green);
                    }


                    #endregion
                }
                #endregion

                #region>>>>>>ECI2406连机状态
                if (HPLaserFun.hHandleECI2406 == null)
                    userLanternAXIS.LanternBackground = Color.Gray;
                else
                {
                    userLanternAXIS.LanternBackground = (userLanternAXIS.LanternBackground == Color.Green ? Color.Yellow : Color.Green);
                    uint nValue = 0;

                    //X==================
                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 0, ref nValue);
                    LED_ORG_X.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 1, ref nValue);
                    LED_ELP_X.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 2, ref nValue);
                    LED_ELN_X.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 6, ref nValue);
                    LED_INP_X.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 24, ref nValue);
                    LED_ALARM_X.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    //X==================
                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 3, ref nValue);
                    LED_ORG_Y.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 4, ref nValue);
                    LED_ELP_Y.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 5, ref nValue);
                    LED_ELN_Y.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 7, ref nValue);
                    LED_INP_Y.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);

                    zmcaux.ZAux_Direct_GetIn(HPLaserFun.hHandleECI2406, 25, ref nValue);
                    LED_ALARM_Y.LanternBackground = (nValue != 0 ? Color.Red : Color.Green);
                }
                #endregion

                #region>>>>>>Unload PLC
                if(!HPLaserFun.bIsPLCUnloadConnected)
                    LEDUnloadPLCState.LanternBackground = Color.Gray;
                else
                    LEDUnloadPLCState.LanternBackground = (LEDUnloadPLCState.LanternBackground == Color.Green ? Color.Yellow : Color.Green);
                #endregion

                #region>>>>>>ModbusTCP Server
                if(HPLaserFun.modebusTCPServer.OnlineCount <= 0)
                    LEDModbusServer.LanternBackground = Color.Gray;
                else
                    LEDModbusServer.LanternBackground = (LEDModbusServer.LanternBackground == Color.Green ? Color.Yellow : Color.Green);

                #endregion

                #region>>>>>>取出接受与发出的命
                HPglobal.outMessage(richTextLoad, HPLaserFun.m_queLoad);
                HPglobal.outMessage(richTextLaserMarking, HPLaserFun.m_queLaserMark);
                HPglobal.outMessage(richTextRotate, HPLaserFun.m_queRotate);
                HPglobal.outMessage(richTextLaseCutting, HPLaserFun.m_queLaserCutting);
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format(" DisplayObjData() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void timer_IO_Tick(object sender, EventArgs e)
        {
            try
            {
                DisplayObjData();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("GTSBox_ProcessReq_LockOK 异常\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void Output_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.CheckBox[] arryLEDs = { LED_Out0, LED_Out1, LED_Out2, LED_Out3, LED_Out4, LED_Out5, LED_Out6, LED_Out7,
                                                                                           LED_Out8, LED_Out9, LED_Out10, LED_Out11, LED_Out12, LED_Out13, LED_Out14, LED_Out15,
                                                                                           LED_Out16, LED_Out17, LED_Out18, LED_Out19, LED_Out20, LED_Out21, LED_Out22, LED_Out23,
                                                                                           LED_Out24, LED_Out25, LED_Out26, LED_Out27, LED_Out28, LED_Out29, LED_Out30, LED_Out31};

            for (int nIndex = 0; nIndex < 32; nIndex++)
            {
                if (sender == arryLEDs[nIndex])
                {
                    zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI0064, nIndex, arryLEDs[nIndex].Checked  ? (UInt32)1: (UInt32)0);
                    break;
                }
            }

            for (int nIndex = 0; nIndex < 32; nIndex++)
            {
                UInt32 value = 0;
                zmcaux.ZAux_Direct_GetOp(HPLaserFun.hHandleECI0064, nIndex, ref value);
                arryLEDs[nIndex].Checked = (value == 0 ? false : true);
            }
        }

        private void form_Closing(object sender, FormClosingEventArgs e)
        {
            zmcaux.ZAux_Close(HPLaserFun.hHandleECI0064);
            zmcaux.ZAux_Close(HPLaserFun.hHandleECI2406);
            timer_IO.Enabled = false;
        }

        public bool moveAxis(int nAxis, bool bDir)
        {
            try
            {
                float dst = (nAxis == 0 ? float.Parse(txtDst_X.Text.Trim()) : float.Parse(txtDst_Y.Text.Trim()));
                double fStartPos = 0.00;
                float fPosCurr = -99999999.99f;
                float fSpeed = 10000;
                if (!bDir)
                    dst = -dst;

                lock (HPLaserFun.ObjMove)
                {
                    Int32 nResult = zmcaux.ZAux_Direct_GetDpos(HPLaserFun.hHandleECI2406, nAxis, ref fPosCurr);
                    nResult = zmcaux.ZAux_Direct_SetLspeed(HPLaserFun.hHandleECI2406, nAxis, 1000);
                    nResult = zmcaux.ZAux_Direct_SetSpeed(HPLaserFun.hHandleECI2406, nAxis, fSpeed);
                    nResult = zmcaux.ZAux_Direct_SetAccel(HPLaserFun.hHandleECI2406, nAxis, (float)fSpeed * 5);
                    nResult = zmcaux.ZAux_Direct_SetDecel(HPLaserFun.hHandleECI2406, nAxis, (float)fSpeed * 5);
                    nResult = zmcaux.ZAux_Direct_Singl_Move(HPLaserFun.hHandleECI2406, nAxis, dst);
                    string msg = string.Format("Move_TrapMoving_Relative(卡号: %d, 轴号: %d, 相对移动距离: %ld", 0, nAxis, dst);
                }

            }
            catch (Exception ex)
            {

            }

            return true;
        }

        private void X轴正转_Click(object sender, EventArgs e)
        {
            moveAxis(0, true);
        }

        private void X轴反转_Click(object sender, EventArgs e)
        {
            moveAxis(0, false);
        }

        private void X轴启用禁用_CheckedChanged(object sender, EventArgs e)
        {
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 8, X轴启用禁用.Checked  ? (UInt32)1 : (UInt32)0);
        }

        private void Y轴正转_Click(object sender, EventArgs e)
        {
            moveAxis(1, true);
        }

        private void X轴停止_Click(object sender, EventArgs e)
        {
            zmcaux.ZAux_Direct_Singl_Cancel(HPLaserFun.hHandleECI2406, 0, 2);
        }

        private void X轴报警清除_Click(object sender, EventArgs e)
        {
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 0, 1);
            Thread.Sleep(100);
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 0, 0);
            Thread.Sleep(100);
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 0, 1);
        }


        private void Y轴反转_Click(object sender, EventArgs e)
        {
            moveAxis(1, false);
        }

        private void Y轴启用禁用_CheckedChanged(object sender, EventArgs e)
        {
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 9, Y轴启用禁用.Checked  ? (UInt32)1 : (UInt32)0);
        }



        private void Y轴停止_Click(object sender, EventArgs e)
        {
            zmcaux.ZAux_Direct_Singl_Cancel(HPLaserFun.hHandleECI2406, 1, 2);
        }

        private void Y轴报警清除_Click(object sender, EventArgs e)
        {
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 1, 1);
            Thread.Sleep(100);
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 1, 0);
            Thread.Sleep(100);
            zmcaux.ZAux_Direct_SetOp(HPLaserFun.hHandleECI2406, 1, 1);
        }

        private void groupControl5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void 上料皮带控制_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_LoadLineControl(上料皮带控制.Checked));
        }

        private void 切割皮带控制_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_LaserCuttingLineControl(切割皮带控制.Checked));
            
        }

        private void 回流皮带控制_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_RotateLineControl(回流皮带控制.Checked));
           
        }

        private void 待料气缸阻挡_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_Load_CylinderStop(待料气缸阻挡.Checked));
           
        }

        private void 打码气缸阻挡_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_LaserMark_CylinderStop(打码气缸阻挡.Checked));
            
        }

        private void 打码气缸顶升_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_LaserMark_CylinderUpDown(打码气缸顶升.Checked));
            
        }

        private void 翻转气缸阻挡_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_Rotate_CylinderStop(翻转气缸阻挡.Checked));
            
        }

        private void 翻转气缸顶升_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_Rotate_CylinderUpDown(翻转气缸顶升.Checked));
            
        }

        private void 翻转工位旋转_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_Rotate_CylinderRotate(翻转工位旋转.Checked));
            
        }

        private void 翻转工位夹紧_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_Rotate_CylinderOpenClose(翻转工位夹紧.Checked));
            
        }

        private void 切割气缸阻挡_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_LaserCutting_CylinderStop(切割气缸阻挡.Checked));
            
        }

        private void 切割气缸顶升_CheckedChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_LaserCutting_CylinderUpDown(切割气缸顶升.Checked));
            
        }

        private void btmInvoke_ResetMachine_Click(object sender, EventArgs e)
        {

        }

        private void btmInvoke_StartMachine_Click(object sender, EventArgs e)
        {
            if(!HPLaserFun.bLaserMachineIsStarted)
            {
                //启动工作线程
                Thread ThreadLoading = new Thread(new ParameterizedThreadStart(thread_foLoading));
                ThreadLoading.Start(this);

                Thread ThreadLaserMarking = new Thread(new ParameterizedThreadStart(thread_forLaserMarking));
                ThreadLaserMarking.Start(this);

                Thread ThreadRotate = new Thread(new ParameterizedThreadStart(thread_forRotate));
                ThreadRotate.Start(this);

                Thread ThreadLaserCutting = new Thread(new ParameterizedThreadStart(thread_forLaserCutting));
                ThreadLaserCutting.Start(this);
            }
        }

        private void btmInvoke_StopMachine_Click(object sender, EventArgs e)
        {

        }

        private void 打码工位允许上料_CheckedChanged(object sender, EventArgs e)
        {
            HPLaserFun.bAllowAcceptPCB_LaserMark = 打码工位允许上料.Checked;
        }

        private void 打码工位上料完成_CheckedChanged(object sender, EventArgs e)
        {
            HPLaserFun.bAcceptPCBCompleted_LaserMark = 打码工位上料完成.Checked;
        }

        private void 切割工位允许上料_CheckedChanged(object sender, EventArgs e)
        {
            HPLaserFun.bAllowAcceptPCB_LaserCuuting = 切割工位允许上料.Checked;
        }

        private void 切割工位上料完成_CheckedChanged(object sender, EventArgs e)
        {
            HPLaserFun.bAcceptPCBCompleted_LaserCuuting = 切割工位上料完成.Checked;
        }

        private void 下料工位允许上料_CheckedChanged(object sender, EventArgs e)
        {
            HPLaserFun.bAllowAcceptPCB_Unload = 下料工位允许上料.Checked;
        }

        private void 下料工位上料完成_CheckedChanged(object sender, EventArgs e)
        {
            HPLaserFun.bAcceptPCBCompleted_Unload = 下料工位上料完成.Checked;
        }

        private void 启用禁用翻转_CheckedChanged(object sender, EventArgs e)
        {
            HPLaserFun.EnableRotate = 启用禁用翻转.Checked;
        }

        private void 翻转工位联动_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew<bool>(() => HPLaserFun.Action_RotatePCB(true));
            
        }
    }
}