﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Xml;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Drawing;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;
using APK_HP.MES.HPSql;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public class HPStationBase
    {
        public object PLCorPCDevice = null;
        public PLC_Panasonic PLCServer = null;
        public PC_ModeBusNet PCModbusServer = null;
        public TcpClient Scanner1Server = null;
        public TcpClient Scanner2Server = null;
        public bool bDispose = false;

        object objDeviceWrite = new object();
        object objDeviceRead = new object();

        public string strStationName { get; } = "未定义工位名字";
        public string strIPAddress;
        public int nPort;

        public bool bEnabled = true;

        public bool usePLCOrPCDevice = true;
        public bool IsDeviceConnected = false;
        public bool isScanner1Connected = false;
        public bool isScanner2Connected = false;

        public bool bEnbalScanner1 = true;
        public bool bEnbalScanner2 = true;

        public short nMes1VerifyResult = 2;
        public short nMes2VerifyResult = 2;
        public int bIsMESChecking1 = 0;
        public int bIsMESChecking2 = 0;
        public string strTrayIDName;
        public string strTrayIDName2;
        public string strVirtrualPCBSN;
        public string strVirtrualPCBSN2;

        public UInt32 CountOfPassed = 0;
        public UInt32 CountOfFailed = 0;
        public Int16 CountOfUnit = 0;
        public double fUPH = 0;

        public int ErrorCode;
        public string ErrorMsg;
        public Queue<HPglobal.MessageOut> m_queOutMsg = new Queue<HPglobal.MessageOut>();
        public Queue<HPglobal.MessageOut> m_queOutMsg2 = new Queue<HPglobal.MessageOut>();

        public Int16[] D1000Values = new Int16[40];
        public Int16[] D1200Values = new Int16[40];
        public Int16[] D1300Values = new Int16[40];

        public HPglobal.emMachineWorkingState emState = HPglobal.emMachineWorkingState.emMachineWorkingState_NotReset;


        public HPStationBase(string stationName)
        {
            if (stationName != null && stationName.Length > 0)
                strStationName = stationName;
        }

        ~HPStationBase()
        {
            Dispose();
        }

        public bool ConnectPLC(string ip, int port)
        {
            try
            {
                PLCorPCDevice = null;
                strIPAddress = ip;
                nPort = port;
                usePLCOrPCDevice = true;

                if (PCModbusServer != null && PCModbusServer.isConnected)
                    PCModbusServer.ConnectClose();

                if (PLCServer != null && PLCServer.isConnected)
                    PLCServer.ConnectClose();

                PCModbusServer = null;
                PLCServer = null;

                PLCServer = new PLC_Panasonic();
                PLCorPCDevice = PLCServer;
                IsDeviceConnected = false;

                PLCServer.ConnectTimeOut = 2000;
                PLCServer.ReceiveTimeOut = 2000;
                OperateResult result = PLCServer.ConnectPLC(strIPAddress, nPort);
                if (!result.IsSuccess)
                    HPglobal.RecordeAllMessage($"ConnectPLC 连接失败(工位名称: {strStationName} 连接端口: {strIPAddress}:{nPort})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                else
                {
                    HPglobal.RecordeAllMessage($"ConnectPLC 连接成功(工位名称: {strStationName} 连接端口: {strIPAddress}:{nPort})", true, true, "");
                    IsDeviceConnected = true;
                }

                return result.IsSuccess;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format($"ConnectPLC  异常(工位名称: { strStationName}连接端口: { strIPAddress}:{ nPort})\r\n{0}", ex.ToString());

            }

            return false;
        }

        public bool ConnectPCModbus(string ip, int port, byte station)
        {
            try
            {
                PLCorPCDevice = null;
                strIPAddress = ip;
                nPort = port;
                usePLCOrPCDevice = false;

                if (PCModbusServer != null && PCModbusServer.isConnected)
                    PCModbusServer.ConnectClose();

                if (PLCServer != null && PLCServer.isConnected)
                    PLCServer.ConnectClose();

                PCModbusServer = null;
                PLCServer = null;

                PCModbusServer = new PC_ModeBusNet();
                PLCorPCDevice = PCModbusServer;
                IsDeviceConnected = false;

                PCModbusServer.ConnectTimeOut = 2000;
                PCModbusServer.ReceiveTimeOut = 2000;
                OperateResult result = PCModbusServer.ConnectModebusTCPServer(strIPAddress, nPort, station);
                if (!result.IsSuccess)
                    HPglobal.RecordeAllMessage($"ConnectPCModbus 连接失败(工位名称: {strStationName} 连接端口: {strIPAddress}:{nPort})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                else
                {
                    HPglobal.RecordeAllMessage($"ConnectPCModbus 连接成功(工位名称: {strStationName} 连接端口: {strIPAddress}:{nPort})", true, true, "");
                    IsDeviceConnected = true;
                }
                return result.IsSuccess;
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format($"ConnectPCModbus  异常(工位名称: { strStationName} 连接端口: { strIPAddress}:{ nPort})\r\n{ 0}", ex.ToString());

            }

            return false;

        }

        public bool ConnectScanner1(string ip, int port, int nTimeout = 2000)
        {
            try
            {
                isScanner1Connected = false;
                Scanner1Server = TimeOutSocket.Connect(new IPEndPoint(IPAddress.Parse(ip), port), nTimeout);
                if (Scanner1Server == null)
                    HPglobal.RecordeAllMessage($"ConnectScanner1 连接条码枪失败(工位名称: {strStationName} 连接端口: {ip}:{port})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                else
                {
                    HPglobal.RecordeAllMessage($"ConnectScanner1 连接条码枪成功(工位名称: {strStationName} 连接端口: {ip}:{port})", true, true, "");
                    isScanner1Connected = true;
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format($"Scanner1Server 连接条码枪失败(工位名称: {strStationName} 连接端口: {ip}:{port})\r\n{0}", ex.ToString());

            }

            return isScanner1Connected;
        }

        public bool ConnectScanner2(string ip, int port, int nTimeout = 2000)
        {
            try
            {
                isScanner2Connected = false;
                Scanner2Server = TimeOutSocket.Connect(new IPEndPoint(IPAddress.Parse(ip), port), nTimeout);
                if (Scanner2Server == null)
                    HPglobal.RecordeAllMessage($"ConnectScanner2 连接条码枪失败(工位名称: {strStationName} 连接端口: {ip}:{port})", true, true, "检查设备电源是否开启或线路是否正常及IP配制");
                else
                {
                    HPglobal.RecordeAllMessage($"ConnectScanner2 连接条码枪成功(工位名称: {strStationName} 连接端口: {ip}:{port})", true, true, "");
                    isScanner2Connected = true;
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format($"ConnectScanner2 连接条码枪失败(工位名称: {strStationName} 连接端口: {ip}:{port})\r\n{0}", ex.ToString());

            }

            return isScanner2Connected;
        }

        public bool Intialize()
        {
            if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                return true;

            try
            {
                bDispose = false;
                #region>>>>>>>>>>> 开启心跳线程
                Thread threadOfHeart = new Thread(new ParameterizedThreadStart(thread_TestHeartBettwenPLC));
                threadOfHeart.Start(null);
                #endregion

                #region>>>>>>>>>>> 开启I/O信号及所有信号线程
                Thread threadOfIO = new Thread(new ParameterizedThreadStart(thread_For_IOMonitor));
                threadOfIO.Start(null);
                #endregion

                #region>>>>>>>>>>> 开启条码枪验证线程
                Thread threadOfScanner1 = new Thread(new ParameterizedThreadStart(thread_ForMesVerify_BarcodeEvent));
                threadOfScanner1.Start("1");

                Thread threadOfScanner2 = new Thread(new ParameterizedThreadStart(thread_ForMesVerify_BarcodeEvent));
                threadOfScanner2.Start("2");
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase Start() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }


            return false;
        }

        public bool Start()
        {
            if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                return true;

            try
            {
                emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Starting;
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"clear", System.Drawing.Color.Blue);
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>开始启动，清除报警", System.Drawing.Color.Blue);
                ClearError();
                Thread.Sleep(200);

                //切换设备到自动模式
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>切换设备到自动模式", System.Drawing.Color.Blue);
                SetManualOrAuto(true);

                //发送停止信号
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>发送启动信号D1003(1->0)", System.Drawing.Color.Blue);
                WriteInt16("D1003", 1);
                Thread.Sleep(350);
                WriteInt16("D1003", 0);

                //等待设备启动
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>等待设备启动(D1006 要为3).......", System.Drawing.Color.Blue);
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(5);
                    Application.DoEvents();
                    if (HPglobal.m_bQuitApp)
                    {
                        emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                        return false;
                    }

                    Int16 nState = ReadInt16("D1006");
                    if (nState == 3)
                    {
                        HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备启动成功", System.Drawing.Color.Blue);
                        emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Working;
                        return true;
                    }

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 60)
                    {
                        HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备启动超时", System.Drawing.Color.Red);
                        emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase Start() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }


            return false;
        }

        public bool Stop()
        {
            Int16 nState;
            if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                return true;

            try
            {
                emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Stopping;
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"clear", System.Drawing.Color.Blue);
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>开始停止，清除报警", System.Drawing.Color.Blue);
                ClearError();
                Thread.Sleep(200);

                //发送停止信号
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>发送停止信号D1005(1->0)", System.Drawing.Color.Blue);
                WriteInt16("D1005", 1);
                Thread.Sleep(350);
                WriteInt16("D1005", 0);

                //等待设备停止
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>等待设备停止(D1006 要为0或2).......", System.Drawing.Color.Blue);
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(200);
                    if (HPglobal.m_bQuitApp)
                    {
                        emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                        return false;
                    }

                    nState = ReadInt16("D1006");
                    if (nState == 0 || nState == 2)
                    {
                        HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备停止成功", System.Drawing.Color.Blue);
                        emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Idle;
                        return true;
                    }

                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 60)
                    {
                        HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备停止超时", System.Drawing.Color.Red);
                        emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                        return false;
                    }
                }
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase Start() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;
        }

        public bool Reset()
        {
            if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                return true;

            try
            {
                emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Reseting;

                //清除报警
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"clear", System.Drawing.Color.Blue);
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>开始复位，清除报警", System.Drawing.Color.Blue);
                ClearError();
                Thread.Sleep(200);

                //得到设备是否处于工作状态
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>得到设备是否处于工作状态", System.Drawing.Color.Blue);
                Int16 nState = ReadInt16("D1006");
                if (nState == 3)
                {
                    emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                    HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备正在工作中({nState}),正确要为0", System.Drawing.Color.Blue);
                    return false;
                }

                //切换设备到手动模式
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>切换设备到手动模式", System.Drawing.Color.Blue);
                SetManualOrAuto(false);

                //复位
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>复位D1002", System.Drawing.Color.Blue);
                WriteInt16("D1002", 0);
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>读取D1002({ReadInt16("D1002")})", System.Drawing.Color.Blue);

                //发送复位信号
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>发送复位信号D1004(1->0)", System.Drawing.Color.Blue);
                WriteInt16("D1004", 1);
                Thread.Sleep(350);
                WriteInt16("D1004", 0);

                //等待完成复位
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>等待复位完成(D1002 要为1).......", System.Drawing.Color.Blue);
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (true)
                {
                    Thread.Sleep(200);
                    if (HPglobal.m_bQuitApp)
                    {
                        return false;
                    }

                    nState = ReadInt16("D1002");
                    if (nState == 1)
                        break;

                    time2 = DateTime.Now;
                    if((time2 - time1).TotalSeconds > 120)
                    {
                        HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备复位超时", System.Drawing.Color.Blue);
                        emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                        return false;
                    }
                }

                //切换设备到自动模式
                HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>切换设备到自动模式", System.Drawing.Color.Blue);
                SetManualOrAuto(true);

                //得到是否准备好或有错
                Int16 nError = ReadInt16("D1007");
                if (nError != 0)
                {
                    HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备有错({nError}), 复位失败", System.Drawing.Color.Red);
                    emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm;
                    return false;
                }
                else
                {
                    HPglobal.WorkingMessageOutput(ref m_queOutMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备复位成功", System.Drawing.Color.Blue);
                    emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Idle;
                    return true;
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase Reset() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;
        }

        public void Dispose()
        {
            try
            {
                bDispose = true;

                if (PLCorPCDevice != null)
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        PLC.ConnectClose();
                    }
                    else
                    {
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        PLC.ConnectClose();
                    }
                }

                if (Scanner1Server != null && Scanner1Server.Client != null)
                    Scanner1Server.Client.Close();

                if (Scanner2Server != null && Scanner2Server.Client != null)
                    Scanner2Server.Client.Close();
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase Dispose() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void EnableStation(bool enable)
        {
            bEnabled = enable;
            if(bEnabled)
                emState = HPglobal.emMachineWorkingState.emMachineWorkingState_NotReset;
            else
                emState = HPglobal.emMachineWorkingState.emMachineWorkingState_Disable;
        }

        public bool ClearError()
        {
            if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                return false;

            try
            {
                bool bResult = WriteInt16("D1004", 2);
                Thread.Sleep(350);
                bResult = WriteInt16("D1004", 0);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase ClearError() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;
        }

        public bool SetManualOrAuto(bool auto)
        {
            try
            {
                if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                    return true;

                bool bResult = WriteInt16("D1001", (short)(auto ? 1 : 0));
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase ClearError() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;
        }

        public void PutOutMsg(string strMsg)
        {

        }

        private void WorkingMessageOutput(string Message, System.Drawing.Color loColor, int fontSize = 9, int nFlag = 1)
        {
            try
            {
                HPglobal.MessageOut message = new HPglobal.MessageOut();

                if (Message.Contains("\r\n"))
                    message.strMesg = Message;
                else
                    message.strMesg = Message + "\r\n";
                message.nColor = loColor;
                message.nFontSize = fontSize;

                RichTextBox textBox = null;

                //============================================
                if (nFlag == 1)
                    m_queOutMsg.Enqueue(message);
                else
                    m_queOutMsg2.Enqueue(message);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStationBase WorkingMessageOutput() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
            }
        }

        public void thread_TestHeartBettwenPLC(object wsarg)
        {
            string trigAddress = "";

            while (!HPglobal.m_bQuitApp || !bDispose)
            {
                if (HPglobal.m_bQuitApp)
                    return;

                Thread.Sleep(2000);
                if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                    continue;

                try
                {
                    if (WriteInt16("D1000", 1))
                        IsDeviceConnected = true;
                    else
                    {
                        IsDeviceConnected = false;
                        if (usePLCOrPCDevice)
                        {
                            PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                            PLC.ConnectServer();
                            PLC.RemoteReset();
                            OperateResult result1 = PLC.ConnectServer();
                            PLC.SetPersistentConnection(); //重连
                        }
                        else
                        {
                            PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                            PLC.ConnectServer();
                            OperateResult result1 = PLC.ConnectServer();
                            PLC.SetPersistentConnection(); //重连
                        }                     
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("thread_TestHeartBettwenPLC  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        public void thread_For_IOMonitor(object wsarg)
        {
            string trigAddress = "";
            Int16[] valuesD1000 = null;
            Int16[] valuesD1200 = null;
            Int16[] valuesD1300 = null;
            while (!HPglobal.m_bQuitApp && !bDispose)
            {
                if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                {
                    Thread.Sleep(1000);
                    continue;
                }

                if(IsDeviceConnected == false)
                {
                    Thread.Sleep(1500);
                    HPglobal.m_strLastException = $"thread_For_IOMonitor{strStationName}  失败：当前设备还没有连接，等待连接";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                    while(IsDeviceConnected == false)
                    {
                        Thread.Sleep(1000);
                        if (HPglobal.m_bQuitApp)
                            return;
                    }
                }

                Thread.Sleep(50);
                if (HPglobal.m_bQuitApp)
                    return;

                try
                {
                    valuesD1000 = ReadInt16Arry("D1000", 40);
                    valuesD1200 = ReadInt16Arry("D1200", 40);
                    valuesD1300 = ReadInt16Arry("D1300", 40);
                    if (valuesD1000 != null && valuesD1200 != null && valuesD1300 != null)
                    {
                        Array.Copy(valuesD1000, D1000Values, 40);
                        Array.Copy(valuesD1200, D1200Values, 40);
                        Array.Copy(valuesD1300, D1300Values, 40);
                    }
                    else
                        throw new Exception("读取D1000的数据为空null");
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = $"thread_For_IOMonitor{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        public void RecordeInfo_LoadInfo(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeInfo_LoadInfo{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void RecordeInfo_UnbindInfo(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeInfo_UnbindInfo{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void RecordeInfo_FlashInfo(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeInfo_FlashInfo{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void RecordeLaserMark_LoadInfo(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeLaserMark_LoadInfo{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void RecordeLaserCut_LoadInfo(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeLaserCut_LoadInfo{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void RecordeInfo_Vistion1(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeInfo_Vistion1{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void RecordeInfo_Vision2(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeInfo_Vision2{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void RecordeInfo_Packing(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            try
            {

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"RecordeInfo_Packing{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        public void ResetMESCheck_Data(string str1or2)
        {
            try
            {
                Int16[] resetString = new Int16[16];
                for (int nIndex = 0; nIndex < 16; nIndex++)
                    resetString[nIndex] = 0x00;

                #region>>>>>>复位数据
                if(str1or2 == "1")
                {
                    strTrayIDName = "";
                    strVirtrualPCBSN = "";
                    nMes1VerifyResult = 2;
                    WriteInt16(HPglobal.PCBID1_Address, resetString);
                    WriteInt16(HPglobal.TrayID1_Address, resetString);
                }
                else
                {
                    strTrayIDName2 = "";
                    strVirtrualPCBSN2 = "";
                    nMes2VerifyResult = 2;
                    WriteInt16(HPglobal.PCBID2_Address, resetString);
                    WriteInt16(HPglobal.TrayID2_Address, resetString);
                }

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void WriteMESCheck_Completed(string str1or2)
        {
            string trigAddress = "D1008";
            string completedAddress = "D1009";
            string resultAddress = "D1010";

            try
            {
                #region>>>>>>写入PLC结果及完成信号
                if (str1or2 == "2")
                {
                    trigAddress = "D1011";
                    completedAddress = "D1012";
                    resultAddress = "D1013";
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>写入PLC结果({nMes2VerifyResult})及完成信号", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    WriteInt16(resultAddress, nMes2VerifyResult);
                }
                else
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>写入PLC结果({nMes1VerifyResult})及完成信号", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    WriteInt16(resultAddress, nMes1VerifyResult);
                }

                Thread.Sleep(50);
                WriteInt16(completedAddress, (short)1);

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public short Function_MESCheck_Load1()
        {
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "1";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                #region>>>>>>读取TrayQrID
                nMes1VerifyResult = 0;
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName);
                #endregion

                #region>>>>>>托盘解绑
                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>托盘解绑", Color.Magenta, 9, int.Parse(str1or2));
                bool bResult = HPInvoke.dataBaseSQL.SQL_TrayUnBind(strTrayIDName);
                if (!bResult)
                {
                    errorMsg = $"{DateTime.Now.ToLongTimeString()}>>>托盘解绑 失败!\r\n";
                    WorkingMessageOutput(errorMsg, Color.Red, 9, int.Parse(str1or2));
                }

                nMes1VerifyResult = 1;

                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_Load1{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_Load2()
        {
            TcpClient objScanner = Scanner2Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "2";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                #region>>>>>>得到条码和生成虚拟条码
                string TrayQrCode = "";
                string PCBQrCode = "";
                SNReadResult = HPInvoke.panel_Load.GetSN(ref PCBQrCode, ref TrayQrCode);
                if(!SNReadResult)
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>得到条码和生成虚拟条码失败!({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));

                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>GetSN成功({TrayQrCode}, {PCBQrCode})", Color.Blue, 9, int.Parse(str1or2));
                #endregion

                #region>>>>>>绑盘
                string PCBID = DateTime.Now.ToString("yyyyMMddHHmmss");
                strVirtrualPCBSN2 = PCBID;
                strTrayIDName2 = TrayQrCode;
                SNReadResult = HPInvoke.dataBaseSQL.SQL_TrayBind(TrayQrCode, HPInvoke.ObjAllParameters.workOrder, PCBQrCode, PCBID);
                #endregion

                #region>>>>>>设置上料结果
                if (SNReadResult)
                {
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>托盘绑定成功{TrayQrCode}", Color.Blue, 9, int.Parse(str1or2));


                    if (!(SNReadResult = HPInvoke.dataBaseSQL.SQL_SetStationResultStatus(PCBID, APK_HP.MES.HPSql.WorkStationNameEnum.上料工站, true)))
                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResultStatus 失败{PCBID}({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));
                    else
                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResultStatus 成功{PCBID}", Color.Blue, 9, int.Parse(str1or2));

                    ResultInformation result = new ResultInformation(HPInvoke.dataBaseSQL.SQL_GetResultFiledFromID(APK_HP.MES.HPSql.WorkStationNameEnum.上料工站));
                    result.Results[0].SetResult($"BindOK({TrayQrCode};{PCBQrCode};{PCBID})");
                    result.ResultStatus = true;
                    if (!(SNReadResult = HPInvoke.dataBaseSQL.SQL_SetStationResult(PCBID, APK_HP.MES.HPSql.WorkStationNameEnum.上料工站, result)))
                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 失败{PCBID}({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));
                    else
                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 成功{PCBID}", Color.Blue, 9, int.Parse(str1or2));
                }
                else
                {
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>托盘绑定失败!({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));
                }
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_UnLoad2{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_Flash1_1()
        {
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "1";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                REREADBARCODE:
                #region>>>>>>读取TrayQrID
                nMes1VerifyResult = 0;
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName);

                if (!SNReadResult)
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName}", Color.Red, 9, int.Parse(str1or2));
                else
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 成功{strTrayIDName}", Color.Blue, 9, int.Parse(str1or2));

                #endregion

                #region>>>>>>得到PCBID
                string PCBID, WrokOrde, PCBQrCode;
                SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName, out PCBID, out WrokOrde, out PCBQrCode);
                strVirtrualPCBSN = PCBID;
                #endregion

                #region>>>>>>如果条码枪读取结果失败，提示
                if (!SNReadResult)
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>读取条码或得到PCBID失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                    errorMsg = $"读取条码(或图像采集条码)失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        goto REREADBARCODE;
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        nMes1VerifyResult = 2;
                    }
                }
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>设置烧录机的条码信息
                if (nMes1VerifyResult == 1)
                {
                    while (HPInvoke.panel_Flash1.strPCBSN.Length > 0 || HPInvoke.panel_Flash1.strTrayID.Length > 0)
                    {
                        Thread.Sleep(200);

                        if (HPInvoke.panel_Flash1.strPCBSN.Contains(strVirtrualPCBSN))
                            break;

                        if (HPglobal.m_bQuitApp)
                            return 1;
                    }
                }

                HPInvoke.panel_Flash1.strPCBSN = strVirtrualPCBSN;
                HPInvoke.panel_Flash1.strTrayID = strTrayIDName;
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_Flash1_1{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_Flash2_1()
        {
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "1";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                REREADBARCODE:
                #region>>>>>>读取TrayQrID
                nMes1VerifyResult = 0;
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName);

                if (!SNReadResult)
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName}", Color.Red, 9, int.Parse(str1or2));
                else
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 成功{strTrayIDName}", Color.Blue, 9, int.Parse(str1or2));

                #endregion

                #region>>>>>>得到PCBID
                string PCBID, WrokOrde, PCBQrCode;
                SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName, out PCBID, out WrokOrde, out PCBQrCode);
                strVirtrualPCBSN = PCBID;
                #endregion

                #region>>>>>>如果条码枪读取结果失败，提示
                if (!SNReadResult)
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>读取条码或得到PCBID失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                    errorMsg = $"读取条码(或图像采集条码)失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        goto REREADBARCODE;
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        nMes1VerifyResult = 2;
                    }
                }
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>设置烧录机的条码信息
                if (nMes1VerifyResult == 1)
                {
                    while (HPInvoke.panel_Flash2.strPCBSN.Length > 0 || HPInvoke.panel_Flash2.strTrayID.Length > 0)
                    {
                        Thread.Sleep(200);

                        if (HPInvoke.panel_Flash2.strPCBSN.Contains(strVirtrualPCBSN))
                            break;

                        if (HPglobal.m_bQuitApp)
                            return 1;
                    }

                    HPInvoke.panel_Flash2.strPCBSN = strVirtrualPCBSN;
                    HPInvoke.panel_Flash2.strTrayID = strTrayIDName;

                }
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_Flash2_1{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_LaserMark1()
        {
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "1";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                REREADBARCODE:
                #region>>>>>>读取TrayQrID
                nMes1VerifyResult = 0;
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName);

                if (!SNReadResult)
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName}", Color.Red, 9, int.Parse(str1or2));
                else
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 成功{strTrayIDName}", Color.Blue, 9, int.Parse(str1or2));

                #endregion

                #region>>>>>>得到PCBID
                string PCBID, WrokOrde, PCBQrCode;
                SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName, out PCBID, out WrokOrde, out PCBQrCode);
                strVirtrualPCBSN = PCBID;
                #endregion

                #region>>>>>>如果条码枪读取结果失败，提示
                if (!SNReadResult)
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>读取条码或得到PCBID失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                    errorMsg = $"读取条码(或图像采集条码)失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        goto REREADBARCODE;
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        nMes1VerifyResult = 2;
                    }
                }
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                 #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_LaserMark1{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }


        public short Function_MESCheck_LaserCut2()
        {
            TcpClient objScanner = Scanner2Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "2";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                REREADBARCODE:
                #region>>>>>>读取TrayQrID
                nMes2VerifyResult = 0;
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName2);

                if (!SNReadResult)
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName2}", Color.Red, 9, int.Parse(str1or2));
                else
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 成功{strTrayIDName2}", Color.Blue, 9, int.Parse(str1or2));

                #endregion

                #region>>>>>>得到PCBID
                string PCBID, WrokOrde, PCBQrCode;
                SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName2, out PCBID, out WrokOrde, out PCBQrCode);
                strVirtrualPCBSN2 = PCBID;
                #endregion

                #region>>>>>>如果条码枪读取结果失败，提示
                if (!SNReadResult)
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>读取条码或得到PCBID失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                    errorMsg = $"读取条码(或图像采集条码)失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        goto REREADBARCODE;
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        nMes2VerifyResult = 2;
                    }
                }
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_LaserCut2{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_VS1()
        {
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "1";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                REREADBARCODE:
                #region>>>>>>读取TrayQrID
                nMes1VerifyResult = 0;
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName);

                if (!SNReadResult)
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName}", Color.Red, 9, int.Parse(str1or2));
                else
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 成功{strTrayIDName}", Color.Blue, 9, int.Parse(str1or2));

                #endregion

                #region>>>>>>得到PCBID
                string PCBID, WrokOrde, PCBQrCode;
                SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName, out PCBID, out WrokOrde, out PCBQrCode);
                strVirtrualPCBSN = PCBID;
                #endregion

                #region>>>>>>如果条码枪读取结果失败，提示
                if (!SNReadResult)
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>读取条码或得到PCBID失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                    errorMsg = $"读取条码(或图像采集条码)失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        goto REREADBARCODE;
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        nMes1VerifyResult = 2;
                    }
                }
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_VS1{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_VS2()
        {
            TcpClient objScanner = Scanner2Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "2";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                REREADBARCODE:
                #region>>>>>>读取TrayQrID
                nMes2VerifyResult = 0;
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName2);

                if (!SNReadResult)
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName2}", Color.Red, 9, int.Parse(str1or2));
                else
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 成功{strTrayIDName2}", Color.Blue, 9, int.Parse(str1or2));

                #endregion

                #region>>>>>>得到PCBID
                string PCBID, WrokOrde, PCBQrCode;
                SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName2, out PCBID, out WrokOrde, out PCBQrCode);
                strVirtrualPCBSN2 = PCBID;
                #endregion

                #region>>>>>>如果条码枪读取结果失败，提示
                if (!SNReadResult)
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>读取条码或得到PCBID失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                    errorMsg = $"读取条码(或图像采集条码)失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        goto REREADBARCODE;
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        nMes2VerifyResult = 2;
                    }
                }
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                 #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_VS2{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_Packing1(int trigger1or2)
        {
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "1";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                START_PRINT:
                if(trigger1or2 == 1)
                    HPInvoke.PackingCount = HPInvoke.PackingStation.stationBase.ReadInt16("D1061");
                else
                    HPInvoke.PackingCount = HPInvoke.PackingStation.stationBase.ReadInt16("D1062");

                #region>>>>>>确认标签打印数据
                if (HPInvoke.PackingCount == 0)
                {
                    errorMsg = $"!!!错误!!!\r\n标签打印的时候传入的标签数据为 0 \r\n是否继续???";
                    HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        goto START_PRINT;
                    else
                    { return 2; }
                }
                #endregion

                #region>>>>>>如果条码枪为第1个条码枪, 工位为包装机，且打印标签之后直接给PLC信号
                REPRINTELABLE:
                strVirtrualPCBSN = "NA";
                strTrayIDName = "NA";
                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>开始打标, 发送打标指令{HPInvoke.ObjAllParameters.workOrder}", Color.Blue, 9, int.Parse(str1or2));
                if (!HPInvoke.PrintWrokOrderLable(HPInvoke.ObjAllParameters.workOrder, HPInvoke.PackingCount))
                {
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>包装机打标 失败{HPInvoke.ObjAllParameters.workOrder}", Color.Red, 9, int.Parse(str1or2));
                    errorMsg = $"包装机打标 失败\r\n工单编号:{HPInvoke.ObjAllParameters.workOrder}";
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                    if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                    { goto REPRINTELABLE; }
                    else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                    {
                        nMes1VerifyResult = 2;
                    }
                }
                else
                {
                    nMes1VerifyResult = 1;
                }
                #endregion

                //复位包装数量
                if (trigger1or2 == 2)
                    HPInvoke.PackingStation.stationBase.WriteInt16("D1062", 0);

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_Packing1{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }

        public short Function_MESCheck_Packing2()
        {
            TcpClient objScanner = Scanner2Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;
            bool SNReadResult = false;
            string str1or2 = "2";

            try
            {
                #region>>>>>>复位数据
                ResetMESCheck_Data(str1or2);
                #endregion

                #region>>>>>>读取TrayQrID
                SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName2);
                #endregion

                nMes2VerifyResult = 1;

                #region>>>>>>读取所有已下料盘信息
                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>读取并记录包装条码到数据库", Color.Blue, 9, int.Parse(str1or2));
                HPInvoke.ObjAllParameters.strBarCodePrint = strTrayIDName2;
                string[]  arryUnloadPCBIDs = HPInvoke.ReadUnloaedPCBID_FromRobotPLC();
                string strWritePCBIDs = "";
                if (arryUnloadPCBIDs != null && arryUnloadPCBIDs.Length > 0)
                {
                    for (int nIndex = 0; nIndex < arryUnloadPCBIDs.Length; nIndex++)
                    {
                        if (nIndex == arryUnloadPCBIDs.Length - 1)
                            strWritePCBIDs += arryUnloadPCBIDs[nIndex];
                        else
                            strWritePCBIDs += (arryUnloadPCBIDs[nIndex] + ";");
                    }
                }
                #endregion

                #region>>>>>>绑定所有已下料盘信息到包装条码
                if (!HPInvoke.dataBaseSQL.SQL_RecordePackingInfo(HPInvoke.ObjAllParameters.workOrder, HPInvoke.ObjAllParameters.strBarCodePrint, strWritePCBIDs))
                {
                    nMes2VerifyResult = 2;
                    errorMsg = $"录入包装信息失败！\r\n请检查是否正确打标并扫描或数据库异常\r\n工位: {strStationName}\r\n条码枪号: {str1or2}";
                    actionHandle_result = HPglobal.HandlerShow(errorMsg);
                }
                #endregion

                #region>>>>>>清除已下料的PCBs
                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>清除已下料的PCBs", Color.Blue, 9, int.Parse(str1or2));
                HPInvoke.ResetAllUnloadPCBs();
                #endregion

                #region>>>>>>写入PLC结果及完成信号
                WriteMESCheck_Completed(str1or2);
                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = $"Function_MESCheck_Packing2{strStationName}  执行其间发生错误\r\n{ex.ToString()}";
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return 2;
        }
        public void thread_ForMesVerify_BarcodeEventV2(object wsarg)
        {
            //HPInvoke.BarcodeRequireEvent requirementObj = (HPInvoke.BarcodeRequireEvent)wsarg;
            string trigAddress = "D1008";
            string completedAddress = "D1009";
            string resultAddress = "D1010";
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;

            string str1or2 = (string)wsarg;
            Int16[] resetString = new Int16[16];
            for (int nIndex = 0; nIndex < 16; nIndex++)
                resetString[nIndex] = 0x00;

            while (!HPglobal.m_bQuitApp && !bDispose)
            {
                if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                {
                    Thread.Sleep(200);
                    continue;
                }

                Thread.Sleep(50);
                if (HPglobal.m_bQuitApp)
                    return;

                try
                {
                    #region>>>>>初始化数据
                    if (str1or2 == "2")
                    {
                        trigAddress = "D1011";
                        completedAddress = "D1012";
                        resultAddress = "D1013";
                        objScanner = Scanner2Server;
                    }
                    #endregion

                    TRIGSTART:
                    #region>>>>>>等待条码事件触发
                    Int16 nTrigger = ReadInt16(trigAddress);
                    if (nTrigger == 0)
                        continue;

                    if (objScanner == null && strStationName != "上料工位")
                        continue;

                    //置位触发信号
                    WorkingMessageOutput($"clear", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    WriteInt16(completedAddress, 0);
                    WriteInt16(trigAddress, 0);
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>收到PLC条码信号 D1008 -> {nTrigger}", System.Drawing.Color.Blue, 9, int.Parse(str1or2));

                    #endregion

                    if (str1or2 == "1" && strStationName == "上料工位")
                    {
                        Function_MESCheck_Load1();
                    }
                    else if (str1or2 == "2" && strStationName == "上料工位")
                    {
                        Function_MESCheck_Load2();
                    }
                    else if (str1or2 == "1" && strStationName == "烧录1工位")
                    {
                        Function_MESCheck_Flash1_1();
                    }
                    else if (str1or2 == "1" && strStationName == "烧录2工位")
                    {
                        Function_MESCheck_Flash2_1();
                    }
                    else if (str1or2 == "1" && strStationName == "打码切割工位")
                    {
                        Function_MESCheck_LaserMark1();
                    }
                    else if (str1or2 == "2" && strStationName == "打码切割工位")
                    {
                        Function_MESCheck_LaserCut2();
                    }
                    else if (str1or2 == "1" && strStationName == "下料组盘工位")
                    {
                        Function_MESCheck_VS1();
                    }
                    else if (str1or2 == "2" && strStationName == "下料组盘工位")
                    {
                        Function_MESCheck_VS2();
                    }
                    else if (str1or2 == "1" && strStationName == "包装工位")
                    {
                        Function_MESCheck_Packing1(nTrigger);
                    }
                    else if (str1or2 == "2" && strStationName == "包装工位")
                    {
                        Function_MESCheck_Packing2();
                    }
                }
                catch (Exception ex)
                {
                    #region>>>>>>>异常处理
                    if (str1or2 == "2")
                        nMes2VerifyResult = 0;
                    else
                        nMes1VerifyResult = 0;

                    HPglobal.m_strLastException = string.Format("thread_ForMesVerify_BarcodeEvent  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>thread_ForMesVerify_BarcodeEvent 异常", System.Drawing.Color.Blue, 9, int.Parse(str1or2));

                    WriteInt16(resultAddress, (short)4);
                    Thread.Sleep(50);
                    WriteInt16(completedAddress, (short)1);
                    #endregion

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wsarg"></param>
        public void thread_ForMesVerify_BarcodeEvent(object wsarg)
        {
            //HPInvoke.BarcodeRequireEvent requirementObj = (HPInvoke.BarcodeRequireEvent)wsarg;
            string trigAddress = "D1008";
            string completedAddress = "D1009";
            string resultAddress = "D1010";
            TcpClient objScanner = Scanner1Server;
            string errorMsg = "";
            emActionHandle actionHandle_result;

            string str1or2 = (string)wsarg;
            Int16[] resetString = new Int16[16];
            for (int nIndex = 0; nIndex < 16; nIndex++)
                resetString[nIndex] = 0x00;

            while (!HPglobal.m_bQuitApp && !bDispose)
            {
                if (emState == HPglobal.emMachineWorkingState.emMachineWorkingState_Disable)
                {
                    Thread.Sleep(200);
                    continue;
                }

                Thread.Sleep(50);
                if (HPglobal.m_bQuitApp)
                    return;

                try
                {
                    #region>>>>>初始化数据
                    if (str1or2 == "2")
                    {
                        trigAddress = "D1011";
                        completedAddress = "D1012";
                        resultAddress = "D1013";
                        objScanner = Scanner2Server;
                    }
                    #endregion

                    TRIGSTART:

                    if (HPglobal.m_bQuitApp)
                        return;

                    #region>>>>>>如果上料2工位，并且工单量已满就提示操作作员
                    if (strStationName == "上料工位" && str1or2 == "2" && HPInvoke.CurrentPlaneProductCompleted)
                    {
                        HPInvoke.dataBaseSQL.SQL_SetWorkSheetStartEndTime(HPInvoke.ObjAllParameters.workOrder, false);
                        errorMsg = $"\r\n!!!!恭喜!!!!!\r\n当前工单量已完成，请切换工单或更改工单数据并清除之前的数据\r\n如果当前板还有余料，请手工拿出并保存好，可下次继续做!"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        goto TRIGSTART;
                    }
                    #endregion

                    #region>>>>>>如果上料2工位，但没操作员
                    if (HPInvoke.bEnableRFID && Program.bAppIsStared && strStationName == "上料工位" && str1or2 == "2" && (HPglobal.m_strRFID == null || HPglobal.m_strRFID.Length <= 0))
                    {
                        errorMsg = $"\r\n!!!!注意!!!!!\r\n操作员离开，没有操作员RFID，无法操作!"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                        HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short) 1);
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        goto TRIGSTART;
                    }
                    #endregion

                    #region>>>>>>等待条码事件触发
                    Int16 nTrigger = ReadInt16(trigAddress);
                    if (nTrigger == 0)
                        continue;

                    if (objScanner == null && strStationName != "上料工位")
                        continue;

                    #endregion

                    int PlanTotalCount = (((int)HPInvoke.ProductInfoData_Unload[15] & 0x00FFFF) << 16) | ((int)HPInvoke.ProductInfoData_Unload[14] & 0x00FFFF);
                    int ActuralProductedTotalCount = HPInvoke.ActuralProductedCount = (((int)HPInvoke.ProductInfoData_Unload[21] & 0x00FFFF) << 16) | ((int)HPInvoke.ProductInfoData_Unload[20] & 0x00FFFF);
                    int TrayCount = HPInvoke.ProductInfoData_Unload[1] * HPInvoke.ProductInfoData_Unload[2];

                    #region>>>>>>置位触发信号
                    WorkingMessageOutput($"clear", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    WriteInt16(completedAddress,0);
                    WriteInt16(trigAddress, 0);
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>收到PLC条码信号 D1008 -> {nTrigger}", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    #endregion

                    #region>>>>>>读取条码
                    REREADBARCODE:
                    bool SNReadResult = false;
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>开始读取条码", System.Drawing.Color.Blue, 9, int.Parse(str1or2));

                    #region>>>>>>如果条码枪为第2个条码枪
                    if (str1or2 == "2")
                    {
                        #region>>>>>>复位数据
                        strTrayIDName2 = "";
                        strVirtrualPCBSN2 = "";
                        nMes2VerifyResult = 2;
                        WriteInt16(HPglobal.PCBID2_Address, resetString);
                        WriteInt16(HPglobal.TrayID2_Address, resetString);
                        #endregion

                        #region>>>>>>如果当前为第2个条码枪并且为上料工位，那就绑盘并设置上料结果
                        if (strStationName == "上料工位")
                        {
                            //==================得到条码和生成虚拟条码==================
                            string TrayQrCode = "";
                            string PCBQrCode = "";
                            SNReadResult = HPInvoke.panel_Load.GetSN(ref PCBQrCode , ref TrayQrCode);

                            //得到好品总数量与计划总量相比
                            PlanTotalCount = (((int)HPInvoke.ProductInfoData_Unload[15] & 0x00FFFF) << 16) | ((int)HPInvoke.ProductInfoData_Unload[14] & 0x00FFFF);
                            ActuralProductedTotalCount = HPInvoke.ActuralProductedCount = (((int)HPInvoke.ProductInfoData_Unload[21] & 0x00FFFF) << 16) | ((int)HPInvoke.ProductInfoData_Unload[20] & 0x00FFFF);
                            TrayCount = HPInvoke.ProductInfoData_Unload[1] * HPInvoke.ProductInfoData_Unload[2];

                            if (SNReadResult)
                            {
                                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>GetSN成功({TrayQrCode}, {PCBQrCode})", Color.Blue, 9, int.Parse(str1or2));
                                short intCurrentLoaded = HPInvoke.UnloadStation.stationBase.ReadInt16("D1065");
                                HPInvoke.ActureTrayCountOfLoaded = intCurrentLoaded;

                                #region>>>>>>如果是尾料生产，判断当前的SN是不是尾料 料盘
                                if (HPInvoke.bEnableLastModeInvode && HPInvoke.bLastProductMode) //如果是尾料生产，判断当前的SN是不是尾料 料盘
                                {
                                    #region>>>>>>当前尾料模式，但前一块板已做完，但还是最后一块板，提示请放入新的板
                                    if (HPInvoke.countRemaind_ForLastTray <= 0 && HPInvoke.strLastTryaID_ForLastProductMode == TrayQrCode)
                                    {
                                        errorMsg = $"\r\n当前尾料模式，但前一块板已做完，请放入新的板"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                                        goto REREADBARCODE;
                                    }
                                    #endregion

                                    #region>>>>>>当前尾料模式，但前一块板已做完，已经放入新板
                                    else if (HPInvoke.countRemaind_ForLastTray <= 0 && HPInvoke.strLastTryaID_ForLastProductMode != TrayQrCode)
                                    {
                                        HPInvoke.strLastTryaID_ForLastProductMode = TrayQrCode;
                                        HPInvoke.countRemaind_ForLastTray = HPInvoke.ProductInfoData_Unload[1] * HPInvoke.ProductInfoData_Unload[2];
                                    }
                                    #endregion

                                    #region>>>>>>当前尾料模式，前一块还没有做完，要一直等这块板
                                    else
                                    {
                                        while (HPInvoke.strLastTryaID_ForLastProductMode != TrayQrCode)
                                        {
                                            #region>>>>>>>> 如果当前的量已做满了，重新回到触发
                                            if (HPInvoke.CurrentPlaneProductCompleted)
                                            {
                                                HPInvoke.bLastProductMode = false;
                                                HPInvoke.strLastTryaID_ForLastProductMode = "";
                                                goto TRIGSTART;
                                            }
                                            #endregion

                                            #region>>>>>>>> 如果需要重新投入新的托盘，直接放行，并复位当前尾料模式
                                            if (HPInvoke.ActureTrayCountOfLoaded <= HPInvoke.PlanTrayCount_AutoCal - 2)
                                            {
                                                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>检测到坏品增加，需要增加投料，退出尾盘模式.......", Color.Blue, 9, int.Parse(str1or2));
                                                HPInvoke.bLastProductMode = false;
                                                HPInvoke.strLastTryaID_ForLastProductMode = "";
                                                break;
                                            }
                                            #endregion

                                            #region>>>>>>>> 当前的最后的托盘还没有做完，提示
                                            else
                                            {
                                                errorMsg = $"\r\n当前尾料模式，只接受如下托盘流过产线\r\n{HPInvoke.strLastTryaID_ForLastProductMode}\r\n当前托盘号: {TrayQrCode}如果当前的板已经切完，请放入一下新的托盘\r\n"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                                                errorMsg += $"点击 继续 使用当前盘，否则\r\n重新读盘"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()

                                                actionHandle_result = HPglobal.HandlerShow(errorMsg);
                                                if(actionHandle_result == emActionHandle.ActionHandle_Continue)
                                                {
                                                    HPInvoke.strLastTryaID_ForLastProductMode = TrayQrCode;
                                                }
                                                else
                                                    goto REREADBARCODE;
                                            }
                                            #endregion
                                           
                                        }
                                    }
                                    #endregion
                                }

                                #endregion

                                #region>>>>>> else 如果果当前为尾料的最后一盘, 等所有盘做完了再放盘(中间坏品增加则退出尾料生产模式)
                                else if (HPInvoke.bEnableLastModeInvode && PlanTotalCount > 0 && 
                                    (HPInvoke.PlanTrayCount_AutoCal - HPInvoke.ActureTrayCountOfLoaded == 1 || 
                                    PlanTotalCount <= TrayCount || 
                                    Math.Abs(PlanTotalCount - ActuralProductedTotalCount) <= TrayCount)) //如果果当前为尾料的最后一盘, 等所有盘做完了再放盘
                                {
                                    HPInvoke.bLastProductMode = true;
                                    HPInvoke.countRemaind_ForLastTray = HPInvoke.ProductInfoData_Unload[1] * HPInvoke.ProductInfoData_Unload[2];
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>开始尾盘模式，等待烧录完成所有并且下料进行下料模式.......", Color.Blue, 9, int.Parse(str1or2));
                                    Thread.Sleep(500);
                                    short idle_1 = HPInvoke.Flash1Station.stationBase.ReadInt16("D1210");
                                    short idle_2 = HPInvoke.Flash2Station.stationBase.ReadInt16("D1210");
                                    short lastMode = HPInvoke.Flash2Station.stationBase.ReadInt16("D1056");
                                    HPInvoke.strLastTryaID_ForLastProductMode = TrayQrCode;
                                    while (idle_1 == 0 || idle_2 == 0 || lastMode == 0)
                                    {
                                        Application.DoEvents();
                                        idle_1 = HPInvoke.Flash1Station.stationBase.ReadInt16("D1210");
                                        idle_2 = HPInvoke.Flash2Station.stationBase.ReadInt16("D1210");
                                        lastMode = HPInvoke.UnloadStation.stationBase.ReadInt16("D1056");
                                        if (HPInvoke.PlanTrayCount_AutoCal  - HPInvoke.ActureTrayCountOfLoaded == 2)
                                        {
                                            WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>检测到坏品增加，需要增加投料，退出尾盘模式.......", Color.Blue, 9, int.Parse(str1or2));
                                            HPInvoke.bLastProductMode = false;
                                            HPInvoke.strLastTryaID_ForLastProductMode = "";
                                            break;
                                        }

                                        if(HPInvoke.bEnableLastModeInvode == false)
                                        {
                                            HPInvoke.bLastProductMode = false;
                                            HPInvoke.strLastTryaID_ForLastProductMode = "";
                                            break;
                                        }
                                    }

                                    HPInvoke.Flash1Station.stationBase.WriteInt16("D1210", (short) 0);
                                    HPInvoke.Flash2Station.stationBase.WriteInt16("D1210", (short) 0);
                                }
                                #endregion

                                #region>>>>>>如果不是尾料生产或没有启用尾料生产
                                else
                                {
                                    HPInvoke.bLastProductMode = false;
                                    Thread.Sleep(500);
                                }
                                #endregion

                                #region>>>>>>绑盘
                                string PCBID = DateTime.Now.ToString("yyyyMMddHHmmss");
                                strVirtrualPCBSN2 = PCBID;
                                strTrayIDName2 = TrayQrCode;
                                SNReadResult = HPInvoke.dataBaseSQL.SQL_TrayBind(TrayQrCode, HPInvoke.ObjAllParameters.workOrder, PCBQrCode, PCBID);
                                HPInvoke.strLastTryaID_ForLastProductMode = TrayQrCode;
                                #endregion

                                #region>>>>>>设置上料结果
                                if (SNReadResult)
                                {
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>托盘绑定成功{TrayQrCode}", Color.Blue, 9, int.Parse(str1or2));


                                    if (!(SNReadResult = HPInvoke.dataBaseSQL.SQL_SetStationResultStatus(PCBID, APK_HP.MES.HPSql.WorkStationNameEnum.上料工站, true)))
                                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResultStatus 失败{PCBID}({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));
                                    else
                                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResultStatus 成功{PCBID}", Color.Blue, 9, int.Parse(str1or2));

                                    ResultInformation result = new ResultInformation(HPInvoke.dataBaseSQL.SQL_GetResultFiledFromID(APK_HP.MES.HPSql.WorkStationNameEnum.上料工站));
                                    result.Results[0].SetResult($"BindOK({TrayQrCode};{PCBQrCode};{PCBID})");
                                    result.ResultStatus = true;
                                    if (!(SNReadResult = HPInvoke.dataBaseSQL.SQL_SetStationResult(PCBID, APK_HP.MES.HPSql.WorkStationNameEnum.上料工站, result)))
                                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 失败{PCBID}({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));
                                    else
                                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_SetStationResult 成功{PCBID}", Color.Blue, 9, int.Parse(str1or2));

                                    //========================================================
                                    intCurrentLoaded = HPInvoke.UnloadStation.stationBase.ReadInt16("D1065");
                                    HPInvoke.ActureTrayCountOfLoaded = intCurrentLoaded + 1;
                                    HPInvoke.UnloadStation.stationBase.WriteInt16("D1065", (short) HPInvoke.ActureTrayCountOfLoaded);
                                }
                                else
                                {
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>托盘绑定失败!({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));
                                }

                                #endregion

                            }
                            else
                                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>GetSN失败({TrayQrCode}, {PCBQrCode})", Color.Blue, 9, int.Parse(str1or2));

                        }
                        #endregion

                        #region>>>>>>如果当前为第2个条码枪并且为其它工位就，就读取托盘条码并根据工位分析
                        else
                        {
                            #region>>>>>>读取TrayQrID
                            SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName2);
                            #endregion

                            if(HPInvoke.bForceOK_PackingSN && strStationName == "包装工位" && str1or2 == "2")
                            {
                                SNReadResult = true;
                                strTrayIDName2 = DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss");
                            }

                            #region>>>>>>如果条码枪为第2个条码枪, 工位为包装机,就记录条码打印记录
                            if (SNReadResult && strStationName == "包装工位")
                            {
                                if(strTrayIDName2 != null && strTrayIDName2.Length > 0)
                               {
                                    nMes2VerifyResult = 1;

                                    if (nTrigger == 1 || nTrigger == 2)
                                    {
                                        #region>>>>>>记录条码到数据库
                                        WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>读取并记录包装条码到数据库", Color.Blue, 9, int.Parse(str1or2));
                                        HPInvoke.ObjAllParameters.strBarCodePrint = strTrayIDName2;

                                        string strPackingPCBIDs = "";
                                        if (nTrigger == 1)
                                        {
                                            if (HPInvoke.m_queUnloadPCBs.Count > 0)
                                                strPackingPCBIDs = HPInvoke.m_queUnloadPCBs.ToArray()[0];
                                        }
                                        else
                                        {
                                            if (HPInvoke.m_queUnloadPCBs.Count > 1)
                                                strPackingPCBIDs = HPInvoke.m_queUnloadPCBs.ToArray()[1];
                                            else if (HPInvoke.m_queUnloadPCBs.Count > 0)
                                                strPackingPCBIDs = HPInvoke.m_queUnloadPCBs.ToArray()[0];
                                        }

                                        if (strPackingPCBIDs.Length <= 0 ||
                                            !HPInvoke.dataBaseSQL.SQL_RecordePackingInfo(HPInvoke.ObjAllParameters.workOrder, HPInvoke.ObjAllParameters.strBarCodePrint, strPackingPCBIDs))
                                        {
                                            nMes2VerifyResult = 2;
                                            errorMsg = $"录入包装信息失败！\r\n打包托盘个数: {strPackingPCBIDs.Length}\r\n请检查是否正确打标并扫描或数据库异常\r\n工位: {strStationName}\r\n条码枪号: {str1or2}";
                                            WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, int.Parse(str1or2));
                                            HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                                            HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                                            actionHandle_result = HPglobal.HandlerShow(errorMsg);
                                            if (actionHandle_result == emActionHandle.ActionHandle_Continue)
                                                nMes2VerifyResult = 1;
                                        }
                                        #endregion
                                    }
                                    else if (nTrigger == 3)
                                    {
                                        if (HPInvoke.m_queUnloadPCBs.Count > 0)
                                            HPInvoke.m_queUnloadPCBs.Dequeue();
                                    }

                                    //结束
                                    goto PROCESS_FINISHED_EVENT;

                                }
                                else
                                {
                                    nMes1VerifyResult = 2;
                                    goto READ_ERROR_PROMPT;
                                }
                            }
                            #endregion

                            #region>>>>>>如果条码枪为第2个条码枪, 工位为其它工位，就得到当前PCBID并显示
                            else if (SNReadResult)
                            {
                                #region>>>>>>得到PCBID
                                string PCBID, WrokOrde, PCBQrCode;
                                if( !(SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName2, out PCBID, out WrokOrde, out PCBQrCode)))
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_GetTrayInfor 失败{strTrayIDName2}{PCBID}({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9, int.Parse(str1or2));
                                else
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>SQL_GetTrayInfor 成功{strTrayIDName2}{PCBID}", Color.Blue, 9, int.Parse(str1or2));

                                strVirtrualPCBSN2 = PCBID;

                                #endregion
                            }
                            #endregion

                            else
                                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName2}", Color.Red, 9, int.Parse(str1or2));
                        }
                        #endregion

                    }
                    #endregion

                    #region>>>>>>如果条码枪为第1个条码枪
                    else
                    {
                        #region>>>>>>复位数据
                        strTrayIDName = "";
                        strVirtrualPCBSN = "";
                        nMes1VerifyResult = 2;
                        WriteInt16(HPglobal.PCBID1_Address, resetString);
                        WriteInt16(HPglobal.TrayID1_Address, resetString);
                        #endregion

                        #region>>>>>>如果条码枪为第1个条码枪, 工位为包装机，且打印标签之后直接给PLC信号
                        if (strStationName == "包装工位")
                        {
                            REPRINTELABLE:
                            strVirtrualPCBSN = "NA";
                            strTrayIDName = "NA";

                            Thread.Sleep(500);
                            HPInvoke.PackingCount = HPInvoke.PackingStation.stationBase.ReadInt16("D1068"); //ReadInt16("D1061"); nTrigger == 1
                            //HPInvoke.PackingCount = HPInvoke.PackingStation.stationBase.ReadInt16("D1068"); //ReadInt16("D1062"); nTrigger == 2

                            #region>>>>>>确认标签打印数据
                            if (HPInvoke.PackingCount == 0)
                            {
                                errorMsg = $"!!!错误!!!\r\n \r\n是否继续???";
                                HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                                HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                                actionHandle_result = HPglobal.HandlerShow(errorMsg);
                                if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                                    goto REPRINTELABLE;
                                else
                                {  }
                            }
                            #endregion

                            #region>>>>>>开始打标，如果打标有误提示错误处理
                            WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>开始打标, 发送打标指令{HPInvoke.ObjAllParameters.workOrder}", Color.Blue, 9, int.Parse(str1or2));
                            if (HPInvoke.PackingCount <= 0 || !HPInvoke.PrintWrokOrderLable(HPInvoke.ObjAllParameters.workOrder, HPInvoke.PackingCount))
                            {
                                errorMsg = $"标签打印的时候传入的标签数据为 0 或\r\n包装机打标 失败\r\n工单编号:{HPInvoke.ObjAllParameters.workOrder}\r\n请确包装数量是否正确或打印机工作正常或\r\n处于正常连机状态";
                                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, int.Parse(str1or2));
                                HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Red, 9, 1);
                                HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                                actionHandle_result = HPglobal.HandlerShow(errorMsg);
                                if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                                { goto REPRINTELABLE; }
                                else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                                {
                                    nMes1VerifyResult = 2;
                                }
                            }
                            else
                            {
                                nMes1VerifyResult = 1;
                            }
                            #endregion

                            #region>>>>>>如果当前为打标第1次触发，就将数量传给第2次打标用
                            if (nTrigger == 1)
                            {
                                HPInvoke.PackingStation.stationBase.WriteInt16("D1062", (short)HPInvoke.PackingCount);
                            }
                            #endregion

                            goto PROCESS_FINISHED_EVENT;
                        }
                        #endregion

                        #region>>>>>>如果条码枪为第1个条码枪, 工位为其它站别就读取trayID 再处理
                        else
                        {
                            #region>>>>>>读取TrayQrID
                            nMes1VerifyResult = 0;
                            SNReadResult = HPInvoke.Function_ReadBarcode(objScanner, ref strTrayIDName);
                            #endregion

                            #region>>>>>>如果为上料工位第一个条码枪就需要解绑，反之就得到PCIBID
                            if (strStationName == "上料工位")
                            {
                                #region>>>>>>托盘解绑
                                WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>托盘解绑", Color.Magenta, 9, int.Parse(str1or2));
                                bool bResult = HPInvoke.dataBaseSQL.SQL_TrayUnBind(strTrayIDName);
                                if (!bResult)
                                {
                                    errorMsg = $"{DateTime.Now.ToLongTimeString()}>>>托盘解绑 失败!\r\n";
                                    WorkingMessageOutput(errorMsg, Color.Red, 9, int.Parse(str1or2));
                                }
                                #endregion

                                #region>>>>>>如果为尾料模式并且当前板还有余料，写PLC 为 3
                                if (HPInvoke.bEnableLastModeInvode && HPInvoke.bLastProductMode&&
                                    HPInvoke.countRemaind_ForLastTray > 0 &&
                                    HPInvoke.strLastTryaID_ForLastProductMode == strTrayIDName)
                                {
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>当前尾料模式，当前板{strTrayIDName}为最后一块并且还有余料, PLC 结果写 3，直接取放", Color.Blue, 9, int.Parse(str1or2));
                                    nMes1VerifyResult = 3;
                                }
                                else
                                {
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>非尾料模式，当前板{strTrayIDName}正常收料！", Color.Magenta, 9, int.Parse(str1or2));
                                    nMes1VerifyResult = 1;
                                }
                                #endregion

                                goto PROCESS_FINISHED_EVENT;

                            }
                            else
                            {
                                #region>>>>>>得到PCBID
                                if (SNReadResult)
                                {
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 成功{strTrayIDName}", Color.Blue, 9, int.Parse(str1or2));

                                    string PCBID, WrokOrde, PCBQrCode;
                                    SNReadResult = HPInvoke.dataBaseSQL.SQL_GetTrayInfor(strTrayIDName, out PCBID, out WrokOrde, out PCBQrCode);
                                    strVirtrualPCBSN = PCBID;
                                }
                                else
                                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>Function_ReadBarcode 失败{strTrayIDName}", Color.Red, 9, int.Parse(str1or2));

                                #endregion
                            }
                            #endregion
                        }
                        #endregion

                    }
                    #endregion

                    #endregion

                    #region>>>>>>如果条码枪读取结果失败，提示
                    READ_ERROR_PROMPT:
                    if (!SNReadResult)
                    {
                        WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>读取条码失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                        errorMsg = $"读取条码(或图像采集条码)失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                        HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                            goto REREADBARCODE;
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        {
                            if (str1or2 == "2")
                                nMes2VerifyResult = 2;
                            else
                                nMes1VerifyResult = 2;

                                goto PROCESS_FINISHED_EVENT;
                        }
                    }
                    #endregion

                    #region>>>>>>MES校验
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>开始验证条码及MES校验", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    if (str1or2 == "2")
                    {
                        bIsMESChecking2 = 1;
                        nMes2VerifyResult = HPInvoke.Function_MESCheckLastResult(strStationName, 2, strTrayIDName2, strVirtrualPCBSN2);
                        bIsMESChecking2 = 2;

                        // MES校验完成之后，得到PCB虚拟的序列号，再将载板条码一起写入对应触发条码扫描的PLC内部
                        WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>开始写入条码到PLC({str1or2}, {strTrayIDName2}, {strVirtrualPCBSN2})", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                        WriteInt16(HPglobal.PCBID2_Address, resetString);
                        WriteInt16(HPglobal.TrayID2_Address, resetString);

                        WriteString(HPglobal.PCBID2_Address, strVirtrualPCBSN2);
                        WriteString(HPglobal.TrayID2_Address, strTrayIDName2);
                    }
                    else
                    {
                        bIsMESChecking1 = 1;
                        nMes1VerifyResult = HPInvoke.Function_MESCheckLastResult(strStationName, 1, strTrayIDName, strVirtrualPCBSN);
                        bIsMESChecking1 = 2;

                        // MES校验完成之后，得到PCB虚拟的序列号，再将载板条码一起写入对应触发条码扫描的PLC内部
                        WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>开始写入条码到PLC({str1or2}, {strTrayIDName}, {strVirtrualPCBSN})", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                        WriteInt16(HPglobal.PCBID1_Address, resetString);
                        WriteInt16(HPglobal.TrayID1_Address, resetString);

                        WriteString(HPglobal.PCBID1_Address, strVirtrualPCBSN);
                        WriteString(HPglobal.TrayID1_Address, strTrayIDName);
                    }
                    #endregion

                    #region>>>>>>如果当前为打码工位但发现已到最后一盘, 强行进入尾料模式
                    if (strStationName == "打码切割工位" && str1or2 == "1" && HPInvoke.bEnableLastModeInvode && PlanTotalCount > 0 &&
                        (HPInvoke.PlanTrayCount_AutoCal - HPInvoke.ActureTrayCountOfLoaded == 1 || 
                        PlanTotalCount <= TrayCount ||
                         HPInvoke.LaserRemaind_ForLaserStation <= TrayCount ||
                        Math.Abs(PlanTotalCount - ActuralProductedTotalCount) <= TrayCount)) //如果果当前为尾料的最后一盘, 等所有盘做完了再放盘
                    {
                        HPInvoke.bLastProductMode = true; //强行进入尾料模式
                        HPInvoke.strLastTryaID_ForLastProductMode = strTrayIDName;
                    }
                    #endregion

                    #region>>>>>>如果如果当前为打码工位, 已经进入尾料模式且当前为最后一块上料盘
                    if (HPInvoke.bLastProductMode &&    strStationName == "打码切割工位" && str1or2 == "1") // &&HPInvoke.ActureTrayCountOfLoaded >= HPInvoke.PlanTrayCount_AutoCal

                    {
                        #region>>>>>>如果当前剩余的料没有了，全部不切
                        if (HPInvoke.LaserRemaind_ForLaserStation <= 0)
                        {
                            for (int nIndex = 0; nIndex < 100; nIndex++)
                                HPInvoke.lastModeFlag[nIndex] = 0x00;
                        }
                        #endregion

                        #region>>>>>>如果剩余的切割数据大于一整盘，设定整盘都切
                        else if (HPInvoke.LaserRemaind_ForLaserStation >= HPInvoke.ProductInfoData_Unload[1] * HPInvoke.ProductInfoData_Unload[2])
                        {
                            for (int nIndex = 0; nIndex < 100; nIndex++)
                                HPInvoke.lastModeFlag[nIndex] = 0x01;
                        }
                        #endregion

                        #region>>>>>>开始设定哪些要切割
                        else
                        {
                            //清零
                            for (int nIndex = 0; nIndex < 100; nIndex++)
                                HPInvoke.lastModeFlag[nIndex] = 0x00;

                            int nCountForLaser = 0;
                            int nIndexStart = 0;
                            int nRowNum = HPInvoke.ProductInfoData_Unload[1];
                            int nColNum = HPInvoke.ProductInfoData_Unload[2];
                            int nCountOfTray = nRowNum * nColNum;
                            if (HPInvoke.countRemaind_ForLastTray >= HPInvoke.LaserRemaind_ForLaserStation)
                            {
                                nCountForLaser = HPInvoke.LaserRemaind_ForLaserStation;
                                nIndexStart = nCountOfTray - HPInvoke.countRemaind_ForLastTray;
                                HPInvoke.countRemaind_ForLastTray -= HPInvoke.LaserRemaind_ForLaserStation;
                            }
                            else
                            {
                                nCountForLaser = HPInvoke.countRemaind_ForLastTray;
                                nIndexStart = nCountOfTray - HPInvoke.countRemaind_ForLastTray;
                                HPInvoke.countRemaind_ForLastTray = 0;
                            }

                            HPInvoke.UnloadStation.stationBase.WriteInt16("D1066", (short) HPInvoke.countRemaind_ForLastTray);

                            //设置哪里要切割或打码的标志
                            int nCountRow = nIndexStart / nColNum;
                            int nCountIndex = nIndexStart % nColNum;
                            Int16[] nActureArry = new Int16[nRowNum * nColNum];
                            Int16[,] nActureArry2 = new Int16[nRowNum, nColNum];
                            //得到料盘最后需要打码标志并转成二维数组
                            int nStep = 0;

                            for (int nIndex = 0; nIndex < nRowNum * nColNum; nIndex++)
                            {
                                if (nIndex >= nIndexStart)
                                {
                                    nActureArry[nIndex] = 0x01;
                                    nStep++;
                                }

                                else
                                    nActureArry[nIndex] = 0x00;

                                if (nStep <= nCountForLaser)
                                    nActureArry2[nIndex / nColNum, nIndex % nColNum] = nActureArry[nIndex];
                                else
                                    nActureArry2[nIndex / nColNum, nIndex % nColNum] = 0x00;

                            }

                            for (int nRow = 0; nRow < 10; nRow++)
                            {
                                for (int nCol = 0; nCol < 10; nCol++)
                                {
                                    if (nRow < nRowNum && nCol < nColNum)
                                    {
                                        HPInvoke.lastModeFlag[nRow * 10 + nCol] = nActureArry2[nRow, nCol];
                                    }
                                    else
                                        HPInvoke.lastModeFlag[nRow * 10 + nCol] = 0x00;
                                }
                            }
                        }
                        #endregion

                        #region>>>>>>设定切割并写入数据库
                        if (HPInvoke.frm_MainPanel != null)
                            HPInvoke.frm_MainPanel.updataLastMark();

                        if(!HPInvoke.dataBaseSQL.SQL_SetLastModeFlagAllProcess(strTrayIDName, HPInvoke.lastModeFlag))
                        {
                            WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>写入尾料标志失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                            errorMsg = $"写入尾料标志失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                            HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                            actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        }
                        #endregion

                    }
                    else if((!HPInvoke.bLastProductMode && strStationName == "上料工位" &&  str1or2 == "2" ) || //没有启用尾料模式但工位为上料工位
                               (!HPInvoke.bLastProductMode && strStationName == "打码切割工位" && str1or2 == "1") || 
                               (HPInvoke.bLastProductMode && strStationName == "打码切割工位" && str1or2 == "1" && PlanTotalCount > 0 && HPInvoke.LaserRemaind_ForLaserStation >= TrayCount)) //启用了尾料模式但当前的尾数大于一整盘
                    {
                        for (int nIndex = 0; nIndex < 100; nIndex++)
                            HPInvoke.lastModeFlag[nIndex] = 0x01;

                        if (HPInvoke.frm_MainPanel != null)
                            HPInvoke.frm_MainPanel.updataLastMark();

                        if(!HPInvoke.dataBaseSQL.SQL_SetLastModeFlagAllProcess(str1or2 == "1" ? strTrayIDName : strTrayIDName2, HPInvoke.lastModeFlag))
                        {
                            WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>写入尾料标志失败", System.Drawing.Color.Red, 9, int.Parse(str1or2));
                            errorMsg = $"写入尾料标志失败！\r\n工位: {strStationName}\r\n条码枪号: {str1or2}"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                            HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                            actionHandle_result = HPglobal.HandlerShow(errorMsg);

                        }
                    }


                    #endregion
                }
                catch (Exception ex)
                {
                    #region>>>>>>>异常处理
                    if (str1or2 == "2")
                        nMes2VerifyResult = 0;
                    else
                        nMes1VerifyResult = 0;

                    HPglobal.m_strLastException = string.Format("thread_ForMesVerify_BarcodeEvent  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");

                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>thread_ForMesVerify_BarcodeEvent 异常", System.Drawing.Color.Blue, 9, int.Parse(str1or2));

                    WriteInt16(resultAddress, (short) 4);
                    Thread.Sleep(50);
                    WriteInt16(completedAddress, (short) 1);
                    #endregion

                }

                #region>>>>>>写入PLC结果及完成信号
                PROCESS_FINISHED_EVENT:
                if (str1or2 == "2")
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>写入PLC结果({nMes2VerifyResult})及完成信号", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    WriteInt16(resultAddress, nMes2VerifyResult);
                }
                else
                {
                    WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>写入PLC结果({nMes1VerifyResult})及完成信号", System.Drawing.Color.Blue, 9, int.Parse(str1or2));
                    WriteInt16(resultAddress, nMes1VerifyResult);

                    #region>>>>>>设置烧录机的条码信息
                    WorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>等待烧录触发以设置新的条码{strTrayIDName}", Color.Blue, 9, int.Parse(str1or2));
                    if (strStationName == "烧录1工位" && nMes1VerifyResult == 1)
                    {
                        while (HPInvoke.panel_Flash1.strPCBSN.Length > 0 || HPInvoke.panel_Flash1.strTrayID.Length > 0)
                        {
                            Thread.Sleep(200);

                            if (strVirtrualPCBSN != null && HPInvoke.panel_Flash1.strPCBSN.Contains(strVirtrualPCBSN))
                                break;

                            if (HPglobal.m_bQuitApp)
                                return;
                        }

                        HPInvoke.panel_Flash1.strPCBSN = strVirtrualPCBSN;
                        HPInvoke.panel_Flash1.strTrayID = strTrayIDName;
                    }
                    else if (strStationName == "烧录2工位" && nMes1VerifyResult == 1)
                    {
                        while (HPInvoke.panel_Flash2.strPCBSN.Length > 0 || HPInvoke.panel_Flash2.strTrayID.Length > 0)
                        {
                            Thread.Sleep(200);

                            if (strVirtrualPCBSN != null && HPInvoke.panel_Flash2.strPCBSN.Contains(strVirtrualPCBSN))
                                break;

                            if (HPglobal.m_bQuitApp)
                                return;
                        }

                        HPInvoke.panel_Flash2.strPCBSN = strVirtrualPCBSN;
                        HPInvoke.panel_Flash2.strTrayID = strTrayIDName;
                    }
                    #endregion
                }

                Thread.Sleep(50);
                WriteInt16(completedAddress, (short) 1);

                #endregion

                #region>>>>>>保存本地记录
                WorkingMessageOutput($"{ DateTime.Now.ToLongTimeString()}>>>保存本地记录", System.Drawing.Color.Blue, 9, int.Parse(str1or2));

                #endregion
            }
        }

        public void SetPersistentConnection()
        {
            try
            {
                if (usePLCOrPCDevice)
                {
                    PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                    PLC.SetPersistentConnection();
                }
                else
                {
                    PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                    PLC.SetPersistentConnection();
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPStation SetPersistentConnection  执行其间发生错误\r\n{0}", ex.ToString());
            }
        }

        public bool Write(string PLCAddr, bool state)
        {
            lock(objDeviceWrite)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        PLC.Write(PLCAddr, state);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        PLC.Write(PLCAddr, state);
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation Write  执行其间发生错误\r\n{0}", ex.ToString());
                }

                return false;
            }

        }

        public bool WriteInt16(string PLCAddr, Int16[] value)
        {
            OperateResult result;
            lock (objDeviceWrite)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.Write(PLCAddr, value);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.Write(PLCAddr, value);
                    }

                    return result.IsSuccess;
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation Write  执行其间发生错误\r\n{0}", ex.ToString());
                }

                return false;
            }

        }

        public bool WriteInt16(string PLCAddr, Int16 value)
        {
            OperateResult result;
            lock(objDeviceWrite)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.Write(PLCAddr, value);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.Write(PLCAddr, value);
                    }

                    return result.IsSuccess;
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation Write  执行其间发生错误\r\n{0}", ex.ToString());
                }

                return false;
            }

        }

        public bool WriteInt32(string PLCAddr, Int32 value)
        {
            OperateResult result;
            lock (objDeviceWrite)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.Write(PLCAddr, value);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.Write(PLCAddr, value);
                    }

                    if (!result.IsSuccess)
                    {
                        int n = 0;
                    }
                    return result.IsSuccess;
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation Write  执行其间发生错误\r\n{0}", ex.ToString());
                }

                return false;
            }

        }

        public bool WriteBool(string PLCAddr, bool[] value)
        {
            OperateResult result;
            lock (objDeviceWrite)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.Write(PLCAddr, value);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.Write(PLCAddr, value);
                    }

                    return result.IsSuccess;
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation Write  执行其间发生错误\r\n{0}", ex.ToString());
                }

                return false;
            }

        }

        public bool ReadBool(string PLCAddr)
        {
            OperateResult<bool> result = null;
            lock(objDeviceRead)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.ReadBool(PLCAddr);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.ReadBool(PLCAddr);
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation ReadBool  执行其间发生错误\r\n{0}", ex.ToString());
                }

                if (result == null || !result.IsSuccess)
                    return false;
                else
                    return result.Content;
            }

        }

        public Int16 ReadInt16(string PLCAddr)
        {
            OperateResult<Int16> result = null;
            lock(objDeviceRead)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.ReadInt16(PLCAddr);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.ReadInt16(PLCAddr);
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation ReadBool  执行其间发生错误\r\n{0}", ex.ToString());
                }

                if (result == null || !result.IsSuccess)
                    return 0;
                else
                    return result.Content;
            }

        }

        public Int16[] ReadInt16Arry(string PLCAddr, ushort nLen)
        {
            OperateResult<Int16[]> result = null;
            lock(objDeviceRead)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.ReadInt16(PLCAddr, nLen);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.ReadInt16(PLCAddr, nLen);
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation ReadBool  执行其间发生错误\r\n{0}", ex.ToString());
                }

                if (result == null || !result.IsSuccess)
                    return null;
                else
                    return result.Content;
            }

        }

        public byte[] ReadBytes(string PLCAddr, ushort length)
        {
            OperateResult < byte[] > result = null;
            lock(objDeviceRead)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.Read(PLCAddr, length);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.Read(PLCAddr, length);
                    }
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation ReadBytes  执行其间发生错误\r\n{0}", ex.ToString());
                }

                if (result == null || !result.IsSuccess)
                    return null;
                else
                    return result.Content;
            }
       
        }

        public string ReadString(string PLCAddr, ushort len)
        {
            OperateResult <string>result;
            lock (objDeviceWrite)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.ReadString(PLCAddr, len);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.ReadString(PLCAddr, len);
                    }

                    if (!result.IsSuccess)
                        return null;

                    return result.Content ;
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation Write  执行其间发生错误\r\n{0}", ex.ToString());
                }

                return null;
            }

        }

        public bool WriteString(string PLCAddr, string value)
        {
            OperateResult result;
            lock (objDeviceWrite)
            {
                try
                {
                    if (usePLCOrPCDevice)
                    {
                        PLC_Panasonic PLC = PLCorPCDevice as PLC_Panasonic;
                        result = PLC.Write(PLCAddr, value);
                    }
                    else
                    {
                        PLCAddr = PLCAddr.Replace("D", "");
                        PC_ModeBusNet PLC = PLCorPCDevice as PC_ModeBusNet;
                        result = PLC.Write(PLCAddr, value);
                    }

                    return result.IsSuccess;
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = string.Format("HPStation Write  执行其间发生错误\r\n{0}", ex.ToString());
                }

                return false;
            }

        }
    }

}

