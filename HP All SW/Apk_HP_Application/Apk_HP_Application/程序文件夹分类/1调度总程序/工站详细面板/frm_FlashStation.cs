﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Threading;
using CommunicationZH;
using HslCommunication;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;
using APK_HP.MES.HPSql;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public partial class frm_FlashStation : DevExpress.XtraEditors.XtraForm
    {
        public int nMachineID = 0;
        bool bStopFlag = false;
        public PLC_Panasonic PLCServer = null;
        public APK_FlashVHP1 flashServer = new APK_FlashVHP1();
        public bool isEventStarted = false;
        public bool isFlashing = false;
        public System.Diagnostics.Stopwatch FlashTimeWatch = new System.Diagnostics.Stopwatch();
        public Queue<HPglobal.MessageOut> m_queFlashMsg = new Queue<HPglobal.MessageOut>();
        string rs232Comport = "";
        Int16[] FlashResultStatus = new Int16[100];

        public string strPCBSN = "";
        public string strTrayID = "" ;

        string strPCBSN_Temp = "";
        string strTrayID_Temp = "";

        public short passedCurr = 0;
        public short failedCurr = 0;

        public frm_FlashStation( string com, int nID = 1) 
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.ResizeRedraw |
                               ControlStyles.OptimizedDoubleBuffer |
                               ControlStyles.AllPaintingInWmPaint, true);
            this.UpdateStyles();

            rs232Comport = com;
            nMachineID = nID;

            #region>>>>>初始化表格
            gridFlashResult.Rows.Clear();
            int nRow = gridFlashResult.Rows.Add(20);
            for(int nIndexRow = 0; nIndexRow < 20; nIndexRow++)
            {
                gridFlashResult.Rows[nIndexRow].HeaderCell.Value = (nIndexRow + 1).ToString();
                gridFlashResult.Rows[nIndexRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                gridFlashResult.Rows[nIndexRow].Height = 30;
/*                gridFlashResult.Rows[nIndexRow].DefaultHeaderCellType.*/
            }

            int nCountOfClomn = gridFlashResult.Columns.Count;
            for(int nIndexCol = 0; nIndexCol < nCountOfClomn; nIndexCol++)
            {
                gridFlashResult.Columns[nIndexCol].Width = 50;
                gridFlashResult.Columns[nIndexCol].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            
            gridFlashResult.TopLeftHeaderCell.Value = "行/例";
            gridFlashResult.RowHeadersWidth = 60;

            gridFlashResult.Refresh();
            #endregion

            #region>>>>>>>>>>>连结串口
            if (!flashServer.ConnectRS232(rs232Comport, 115200))
            {
                  HPglobal.RecordeAllMessage($"连接烧录机{nMachineID}失败,  端口号：{rs232Comport}", true, false, "");
            }
            #endregion

            #region>>>>>>>>>>> 开启烧录线程
            Thread threadOfFlash = new Thread(new ParameterizedThreadStart(thread_ForFlashEvent));
            threadOfFlash.Start($"{nMachineID.ToString()}");
            #endregion

        }

        ~frm_FlashStation()
        {

        }

        public void thread_ForFlashEvent(object wsarg)
        {
            //HPInvoke.BarcodeRequireEvent requirementObj = (HPInvoke.BarcodeRequireEvent)wsarg;
            string trigAddress = "D1200";
            string completedAddress = "D1201";
            string resultAddress = "D1202";
            string errorMsg = "";
            emActionHandle actionHandle_result;
            string str1or2 = (string)wsarg;
            isEventStarted = true;

            while (!HPglobal.m_bQuitApp)
            {
                Thread.Sleep(250);

                try
                {
                    if (PLCServer == null || !flashServer.isServerConnected)
                        continue;

                    #region>>>>>初始化数据

                    #endregion

                    TRIGSTART:
                    #region>>>>>>等待烧录事件触发
                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>等待PLC触发烧录D1008", Color.Blue, 9);
                    while (true)
                    {
                        Thread.Sleep(200);
                        if (HPglobal.m_bQuitApp || bStopFlag)
                            return;

                        OperateResult<short> nTrigger = PLCServer.ReadInt16(trigAddress);
                        if (nTrigger.IsSuccess && nTrigger.Content == 1)
                            break;
                    }

                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"clear", Color.Blue, 9);
                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>受到PLC触发拍照D1008->1", Color.Blue, 9);
                    #endregion

                    if (HPglobal.m_bQuitApp || bStopFlag)
                        return;

                    #region>>>>>>>> 读取条码
                    strPCBSN_Temp = $"{strPCBSN}";
                    strTrayID_Temp = $"{strTrayID}";

                    strPCBSN = "";
                    strTrayID = "";

                    #endregion

                    #region>>>>>>置位触发信号
                    PLCServer.Write(completedAddress, (short)0);
                    PLCServer.Write(trigAddress, (short)0);

                    #endregion

                    //复位烧录结果
                    bool bFlashTotalResult = false;
                    for (int nIndex = 0; nIndex < 100; nIndex++)
                        FlashResultStatus[nIndex] = 0xFF;

                    GET_FLASH_TYPE:
                    #region>>>>>>获取烧录型号
                    string refCustomerName = "";
                    string TwoRefName = "";
                    string ThreeRefName = "";
                    string strFlashMenu = HPglobal.frm_MainPanel.GetFalshMenuFromWorkOrder(ref refCustomerName, ref TwoRefName, ref ThreeRefName);
                    byte[] bytesType = GetFlashTypeFromMenu(strFlashMenu, ThreeRefName);
                    #endregion

                    #region>>>>>>如果烧录型号没有，提示
                    if (strFlashMenu == null || strFlashMenu.Length < 0 || bytesType == null || bytesType.Length < 36)
                    {
                        HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>获取烧录型号失败，请确认工单号是否正确", Color.Red, 9);
                        errorMsg = $"\r\n获取烧录型号失败，请确认工单号是否正确！\r\n继续 或 重试 将重新获取工单及烧录型号\r\n停止 或 终止 将终止当前流程(结果全部Fail)！"; //(objScanner.Client.LocalEndPoint as IPEndPoint).ToString()
                        HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                            goto GET_FLASH_TYPE;
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        {
                            goto END_FLASH;
                        }
                    }
                    #endregion

                    this.Invoke(new Action(() =>
                    {
                        txtFlashVer.Text = "";
                        string strTemp = "";
                        for (int nIndex = 0; nIndex < bytesType.Length; nIndex++)
                        {
                            strTemp += $"{bytesType[nIndex].ToString("D02")} ";
                        }

                        txtFalshType.Text = "";
                        txtFalshType.Text = strTemp;
                        txtTypeMenue.Text = strFlashMenu;
                    }));

                    RE_FLASH:
                    #region>>>>>>写入产品型号，烧录，及验证
                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>写入产品型号，烧录，及验证", Color.Blue, 9);
                    if(!Flash_Auto(out bFlashTotalResult, bytesType))
                    {
                        errorMsg = $"烧录机{nMachineID}出错，请检查烧录机是否工作正常！\r\n";
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>{errorMsg}", Color.Blue, 9, 1);
                        HPInvoke.LoadStation.stationBase.WriteInt16("D1314", (short)1);
                        actionHandle_result = HPglobal.HandlerShow(errorMsg);
                        if (actionHandle_result == emActionHandle.ActionHandle_Redo || actionHandle_result == emActionHandle.ActionHandle_Continue)
                        { goto RE_FLASH; }
                        else if (actionHandle_result == emActionHandle.ActionHandle_Stop || actionHandle_result == emActionHandle.ActionHandle_Abort)
                        { goto EndFalsh; }
                    }
                    #endregion

                    #region>>>>>>保存烧录结果，写入MES
                    END_FLASH:
                    if(strTrayID_Temp != null && strTrayID_Temp.Length > 0)
                    {
                        HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>保存烧录结果，写入PLC({strTrayID_Temp}, {strPCBSN_Temp})", Color.Blue, 9);

                        if (!HPInvoke.dataBaseSQL.SQL_SetFlashResultStationResultFromTray(strTrayID_Temp, FlashResultStatus))
                            HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备烧录结果到MES: ({strTrayID_Temp}, {strPCBSN_Temp}) 错误({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9);
                        else
                            HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备烧录结果到MES({strTrayID_Temp}, {strPCBSN_Temp}) 成功)", Color.Magenta, 9);

                        if (!HPInvoke.dataBaseSQL.SQL_SetStationResultStatusFromTray(strTrayID_Temp, WorkStationNameEnum.烧录工站, true))
                            HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备烧录状态到MES 错误({APK_HP.MES.HPSql.HPSQLService.ErrorMessageLast})", Color.Red, 9);
                        else
                            HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>设备烧录状态到MES 成功)", Color.Magenta, 9);

                        //HPInvoke.WriteFalshResultToRobotPLC(strTrayID_Temp, FlashResultStatus);

                        HPglobal.frmMainWindow.Invoke(new Action(()=>
                        {
                            cmbFlasedList.Items.Add($"{DateTime.Now.ToLongTimeString()}>>>{strPCBSN_Temp}, {strTrayID_Temp}");
                        }));
                    }
                    else
                    {
                        HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>当前产品无条码编号，请重新扫描", Color.Red, 9);
                    }

                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>保存烧录结果，写入MES", Color.Blue, 9);
                    #endregion

                    #region>>>>>>写入统计数据到PLC
                    HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>当前烧录好品({passedCurr}), 坏品({failedCurr})", Color.Blue, 9, 1);
                    short currentPassed = (short)(PLCServer.ReadInt16("D1038").Content + passedCurr);
                    short currentFailed = (short)(PLCServer.ReadInt16("D1039").Content + failedCurr);

                    PLCServer.Write("D1038", currentPassed);
                    PLCServer.Write("D1039", currentFailed);
                    #endregion

                    EndFalsh:
                    #region>>>>>>写入PLC完成信息
                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>写入PLC完成信息", Color.Blue, 9);
                    PLCServer.Write(completedAddress, (short)1);
                    Thread.Sleep(20);
                    PLCServer.Write(resultAddress, bFlashTotalResult ? (short)1 : (short)2 );
                    #endregion

                    #region>>>>>>保存本地记录

                    #endregion

                }
                catch (Exception ex)
                {
                    PLCServer.Write(completedAddress, (short)1);
                    PLCServer.Write(resultAddress,  (short)2);
                    HPglobal.m_strLastException = string.Format("thread_ForFlashEvent  执行其间发生错误\r\n{0}", ex.ToString());
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                }
            }
        }

        private void frm_FlashResult_Load(object sender, EventArgs e)
        {

        }

        public void UpdateStatus()
        {
            try
            {
                txtPCBSN.Text = $"{strPCBSN_Temp}";
                txtTrayID.Text = $"{strTrayID_Temp}";

                txtPCBSN2.Text = $"{strPCBSN}";
                txtTrayID2.Text = $"{strTrayID}";

                cmbFlashTypeDesc.Text = HPInvoke.ObjAllParameters.APKWorkOrderInfo.DescRiption;

                #region>>>>显示好板，坏板统计数据
                //                 txtPassed.Text = m_statinBase.CountOfPassed.ToString();
                //                 txtPassed.Text = m_statinBase.CountOfPassed.ToString();
                // 
                //                 txtPassedUnit.Text = (m_statinBase.CountOfPassed * m_statinBase.CountOfUnit).ToString();
                //                 txtFailedUnit.Text = (m_statinBase.CountOfFailed * m_statinBase.CountOfUnit).ToString();
                // 
                //                 m_statinBase.fUPH = (double)m_statinBase.CountOfPassed / (double)(m_statinBase.CountOfPassed + m_statinBase.CountOfFailed) * 100.00f;
                //                 txtUPH.Text = m_statinBase.fUPH.ToString("F2") + "%";
                #endregion

                #region>>>>显示设备的连接状态
                //=================================
                if (isEventStarted)
                    LED_FLSAHSTATE.LanternBackground = (LED_FLSAHSTATE.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                else
                    LED_FLSAHSTATE.LanternBackground = (LED_FLSAHSTATE.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);
                //=================================
                if (PLCServer != null && PLCServer.isConnected)
                    LEDPLC.LanternBackground = (LEDPLC.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                else
                    LEDPLC.LanternBackground = (LEDPLC.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);
                //=================================
                if (flashServer.isServerConnected)
                    LEDMachine.LanternBackground = (LEDMachine.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);
                else
                    LEDMachine.LanternBackground = (LEDMachine.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);
                //=================================
                #endregion

                #region>>>>>显示烧录状态
                HPglobal.outMessage(richTextMsgOut, m_queFlashMsg);
                if (isFlashing)
                {
                    double timeUsed = FlashTimeWatch.Elapsed.TotalSeconds;
                    barFlashProgress.Text = $"正在烧录({timeUsed.ToString("F2")} sec, 请等待烧录完成........)";
                    barFlashProgress.BackColor = Color.Yellow;
                    barFlashProgress.Properties.Stopped = false;
                }
                else
                {
                    double timeUsed = FlashTimeWatch.Elapsed.TotalSeconds;
                    barFlashProgress.Text = $"烧录完成({timeUsed.ToString("F2")} sec, 等待下次烧录)";
                    barFlashProgress.BackColor = Color.Yellow;
                    barFlashProgress.Properties.Stopped = true;
                }
                #endregion

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_FlashStation UpdateStatus异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                UpdateStatus();
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_FlashStation 异常\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }
        }

        private void btmSetFlashType_Click(object sender, EventArgs e)
        {
            try
            {
                txtFlashVer.Text = "";
                string dateTime = "";
                string version = "";
                bool compare = false;
                //byte[] type = { 0x01, 0x02, 0x03, 0x04, 0x05 };
                //                 byte[] type = { 0x31, 0x31, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30 };
                //                 byte[] type2 = { 0x30, 0x33 };
                //                 byte[] type = new byte[36]{ 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x00,
                //                                             0x00, 0x00, 0x01, 0x00, 0x00, 0x00 , 0x00 , 0x00 , 0x00 ,
                //                                              0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 ,
                //                                              0x00 , 0x00 , 0x00 , 0x00 , 0x07 , 0x00 , 0x00 , 0x00 , 0x08 };
                string refCustomerName = "";
                string TwoRefName = "";
                string ThreeRefName = "";
                string menu = HPglobal.frm_MainPanel.GetFalshMenuFromWorkOrder(ref refCustomerName, ref TwoRefName, ref ThreeRefName);
                if (menu != null && menu.Length > 0)
                {
                    byte[] bytesType = GetFlashTypeFromMenu(menu, ThreeRefName);
                    if(bytesType != null && bytesType.Length == 36)
                    {
                        txtTypeMenue.Text = $"{menu}";
                        txtFlashVer.Text = "";
                        flashServer.Flash_SetType(bytesType, ref dateTime, ref version, ref compare);
                        txtFlashVer.Text = $"{dateTime} Ver: {version}";
                        string strTemp = "";
                        for(int nIndex = 0; nIndex < bytesType.Length; nIndex++)
                        {
                            strTemp += $"{bytesType[nIndex].ToString("D02")} ";
                        }

                        txtFalshType.Text = "";
                        txtFalshType.Text = strTemp;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public Int32 GetFalshSerialNumberFromFile(string strSearch, string fileName = "MenuTree_Canon.c")
        {
            try
            {
                //保存调用记录信息
                if (!File.Exists(fileName))
                    throw new Exception($"找不到烧录序号定义文件({fileName})");

                FileStream openFile = new FileStream(fileName, FileMode.Open);
                openFile.Seek(0, SeekOrigin.Begin);

                byte[] reads = new byte[openFile.Length];
                openFile.Read(reads, 0, (int)openFile.Length);
                openFile.Close();
                string strReads = System.Text.ASCIIEncoding.ASCII.GetString(reads);
                string[] arrySN_Description = strReads.Split("\n".ToCharArray());

                foreach (string strType in arrySN_Description)
                {
                    if(strType.Contains(strSearch))
                    {
                        string strTemp = strType.Trim("\r".ToCharArray());
                        if (strTemp.Contains("//"))
                        {
                            string strSN = strTemp.Substring(strTemp.IndexOf("//") + 2, strTemp.Length - strTemp.IndexOf("//") - 2);
                            Int32 nSN = Int32.Parse(strSN);
                            return nSN;
                        }
                        else
                            return -1;
                    }
                }

                    return -1;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public byte[] GetFlashTypeFromMenu(string menu, string snType)
        {
            try
            {
                byte[] bytesType = new byte[36];

                if (menu == null || menu.Length <= 0 || snType == null || snType.Length <= 0)
                    throw new Exception($"烧录型号字符串输入错误({menu}, {snType})，为空");

                for (int nIndex = 0; nIndex < bytesType.Length; nIndex++)
                    bytesType[nIndex] = 0x00;

                string[] arryStr = menu.Split("|".ToArray());
                if (arryStr.Length < 3)
                    throw new Exception("烧录型号字符串输入错误，参数如: 4|2|1");

                Int32 nSerialNumber = GetFalshSerialNumberFromFile(snType, HPInvoke.ObjAllParameters.FlashCFilename);
                if(nSerialNumber <= 0)
                    throw new Exception("无法得到烧录序号，请确认文件是否正确或型号是否正确({}, {})");

                UInt32 Dword0 = (UInt32)nSerialNumber; //1个字（32bit）的序号+8个字（32bit）的菜单节点信息（私有数据）
                UInt32 Dword1 = 0x00;
                UInt32 Dword2 = 0x00;
                UInt32 Dword3 = 0x00;
                UInt32 Dword4 = 0x00;
                UInt32 Dword5 = 0x00;
                UInt32 Dword6 = 0x00;
                UInt32 Dword7 = 0x00;

                int StepIndex = 0x00;

                bytesType[StepIndex++] = (byte)((Dword0 & 0xFF000000) >> 24);
                bytesType[StepIndex++] = (byte)((Dword0 & 0x00FF0000) >> 16);
                bytesType[StepIndex++] = (byte)((Dword0 & 0x0000FF00) >> 8);
                bytesType[StepIndex++] = (byte)((Dword0 & 0x000000FF) >> 0);

                if (arryStr.Length >= 1)
                {
                    Dword1 = UInt32.Parse(arryStr[0]);
                    bytesType[StepIndex++] = (byte)((Dword1 & 0xFF000000) >> 24);
                    bytesType[StepIndex++] = (byte)((Dword1 & 0x00FF0000) >> 16);
                    bytesType[StepIndex++] = (byte)((Dword1 & 0x0000FF00) >> 8);
                    bytesType[StepIndex++] = (byte)((Dword1 & 0x000000FF) >> 0);
                }

                if (arryStr.Length >= 2)
                {
                    Dword2 = UInt32.Parse(arryStr[1]);
                    bytesType[StepIndex++] = (byte)((Dword2 & 0xFF000000) >> 24);
                    bytesType[StepIndex++] = (byte)((Dword2 & 0x00FF0000) >> 16);
                    bytesType[StepIndex++] = (byte)((Dword2 & 0x0000FF00) >> 8);
                    bytesType[StepIndex++] = (byte)((Dword2 & 0x000000FF) >> 0);
                }

                if (arryStr.Length >= 3)
                {
                    Dword3 = UInt32.Parse(arryStr[2]);
                    bytesType[StepIndex++] = (byte)((Dword3 & 0xFF000000) >> 24);
                    bytesType[StepIndex++] = (byte)((Dword3 & 0x00FF0000) >> 16);
                    bytesType[StepIndex++] = (byte)((Dword3 & 0x0000FF00) >> 8);
                    bytesType[StepIndex++] = (byte)((Dword3 & 0x000000FF) >> 0);
                }

                if (arryStr.Length >= 4)
                {
                    Dword4 = UInt32.Parse(arryStr[3]);
                    bytesType[StepIndex++] = (byte)((Dword4 & 0xFF000000) >> 24);
                    bytesType[StepIndex++] = (byte)((Dword4 & 0x00FF0000) >> 16);
                    bytesType[StepIndex++] = (byte)((Dword4 & 0x0000FF00) >> 8);
                    bytesType[StepIndex++] = (byte)((Dword4 & 0x000000FF) >> 0);
                }

                if (arryStr.Length >= 5)
                {
                    Dword5 = UInt32.Parse(arryStr[4]);
                    bytesType[StepIndex++] = (byte)((Dword5 & 0xFF000000) >> 24);
                    bytesType[StepIndex++] = (byte)((Dword5 & 0x00FF0000) >> 16);
                    bytesType[StepIndex++] = (byte)((Dword5 & 0x0000FF00) >> 8);
                    bytesType[StepIndex++] = (byte)((Dword5 & 0x000000FF) >> 0);
                }

                if (arryStr.Length >= 6)
                {
                    Dword6 = UInt32.Parse(arryStr[5]);
                    bytesType[StepIndex++] = (byte)((Dword6 & 0xFF000000) >> 24);
                    bytesType[StepIndex++] = (byte)((Dword6 & 0x00FF0000) >> 16);
                    bytesType[StepIndex++] = (byte)((Dword6 & 0x0000FF00) >> 8);
                    bytesType[StepIndex++] = (byte)((Dword6 & 0x000000FF) >> 0);
                }

                return bytesType;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public bool Flash_Auto(out bool bFlashTotalResult)
        {
            bFlashTotalResult = false;
            //bytesType = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05 };
            //                 bytesType = new byte[36]{ 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x00,
            //                                             0x00, 0x00, 0x01, 0x00, 0x00, 0x00 , 0x00 , 0x00 , 0x00 ,
            //                                              0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 ,
            //                                              0x00 , 0x00 , 0x00 , 0x00 , 0x07 , 0x00 , 0x00 , 0x00 , 0x08 };

            /*            bytesType = System.Text.Encoding.ASCII.GetBytes(type);*/
            string refCustomerName = "";
            string TwoRefName = "";
            string ThreeRefName = "";
            string menu = HPglobal.frm_MainPanel.GetFalshMenuFromWorkOrder(ref refCustomerName, ref TwoRefName, ref ThreeRefName);

            byte[] bytesType = GetFlashTypeFromMenu(menu, ThreeRefName);

            try
            {
                return Flash_Auto(out bFlashTotalResult, bytesType);
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Flash_Auto  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }


        private bool Flash_Auto(out bool bFlashTotalResult, byte[] type)
        {
            bFlashTotalResult = false;
            int rowCount = 10;
            int colCount = 10;

            passedCurr = 0;
            failedCurr = 0;

            try
            {
                if (type.Length <= 0)
                    return false;

                FlashTimeWatch.Reset();
                FlashTimeWatch.Start();
                isFlashing = true;
                this.Invoke(new Action(()=>  //更新托盘数据
                {
                    txtReceivedData.Text = "";
                }));
               
                Application.DoEvents();

                //复位表格
                HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>复位表格", Color.Blue, 9);
                this.Invoke(new Action(()=>  //更新托盘数据
                {
                    int nIndex = 0;
                    for (int row = 0; row < rowCount; row++)
                    {
                        for (int col = 0; col < colCount; col++, nIndex++)
                        {
                            gridFlashResult.Rows[row].Cells[col].Value = $"Go";
                            gridFlashResult.Rows[row].Cells[col].Style.BackColor = Color.Yellow;
                        }
                    }
                }));

                //启动烧录
                HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>启动烧录", Color.Blue, 9);
                if (!flashServer.Flash_Start(type, true, false)) // System.Text.Encoding.ASCII.GetBytes(type) System.Text.Encoding.ASCII.GetString(type)
                {
                    isFlashing = false;
                    FlashTimeWatch.Stop();
                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>Flash_Start 出错", Color.Red,  9);
                    this.Invoke(new Action(()=>  //更新托盘数据
                    {
                        int nIndex = 0;
                        for (int row = 0; row < rowCount; row++)
                        {
                            for (int col = 0; col < colCount; col++, nIndex++)
                            {
                                gridFlashResult.Rows[row].Cells[col].Value = $"S_Error";
                                gridFlashResult.Rows[row].Cells[col].Style.BackColor = Color.Red;
                            }
                        }
                    }));
                    return false;
                }

                this.Invoke(new Action(() => txtFlashVer.Text = $"DataTime: {flashServer.CurrentDataTime} Ver: {flashServer.CurrentVersion}" ));

                //得到烧录结果
                HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>等待烧录结果", Color.Blue, 9);
                Thread.Sleep(HPInvoke.ObjAllParameters.flashGetDataSleep);
                string strReturn = "";
                FlashResult[] results = flashServer.Flash_GetResult_OutList(ref strReturn, 10000);
                if (results == null)
                {
                    isFlashing = false;
                    FlashTimeWatch.Stop();
                    HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>Flash_GetResult_OutList 出错(无法得到正确返回结果)", Color.Red, 9);
                    this.Invoke(new Action(()=>  //更新托盘数据
                    {
                        txtReceivedData.Text = "Error, 烧录没有任何结果返回";
                        int nIndex = 0;
                        for (int row = 0; row < rowCount; row++)
                        {
                            for (int col = 0; col < colCount; col++, nIndex++)
                            {
                                gridFlashResult.Rows[row].Cells[col].Value = $"R_Error";
                                gridFlashResult.Rows[row].Cells[col].Style.BackColor = Color.Red;
                            }
                        }
                    }));
                    return false;
                }

                //更新表格
                HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>烧录成功，更新结果及表格", Color.Magenta, 9);
                bool totalFlashResult = true;
                passedCurr = 0;
                failedCurr = 0;
                this.Invoke(new Action(()=>  //更新托盘数据
                {
                    txtReceivedData.Text = $"烧录返回数据: {strReturn}";
                    int nIndex = 0;
                    for (int row = 0; row < rowCount; row++)
                    {
                        for (int col = 0; col < colCount; col++, nIndex++)
                        {
                            gridFlashResult.Rows[row].Cells[col].Value = $"0x{results[nIndex].byteError.ToString("X02")}";
                            gridFlashResult.Rows[row].Cells[col].Style.BackColor = results[nIndex].bResult ? Color.LightGreen : Color.Red;

                            FlashResultStatus[nIndex] = results[nIndex].byteError;

                            if (!results[nIndex].bResult || FlashResultStatus[nIndex] != 0x00)
                                totalFlashResult = false;

                            //计算烧录数量
                            if(row < flashServer.nRow && col < flashServer.nCol)
                            {
                                if (FlashResultStatus[nIndex] == 0x00)
                                    passedCurr++;
                                else
                                    failedCurr++;
                            }
                        }
                    }
                }));

                bFlashTotalResult = totalFlashResult;

                isFlashing = false;
                FlashTimeWatch.Stop();
                return true;
            }
            catch(Exception ex)
            {
                isFlashing = false;
                FlashTimeWatch.Stop();

                HPglobal.WorkingMessageOutput(ref m_queFlashMsg, $"{DateTime.Now.ToLongTimeString()}>>>烧录出现异常({ex.Message})", Color.Red, 9);
                this.Invoke(new Action(()=>  //更新托盘数据
                {
                    int nIndex = 0;
                    for (int row = 0; row < rowCount; row++)
                    {
                        for (int col = 0; col < colCount; col++, nIndex++)
                        {
                            gridFlashResult.Rows[row].Cells[col].Value = $"Ex_Error";
                            gridFlashResult.Rows[row].Cells[col].Style.BackColor = Color.Yellow;
                        }
                    }
                }));

                HPglobal.m_strLastException = string.Format("Flash_Auto  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return false;
            }
        }

        private void btmVerifyFlash_Click(object sender, EventArgs e)
        {
            string strReturn = "";
            FlashResult[] results = flashServer.Flash_GetResult_OutList(ref strReturn, 240000);
            int nIndex = 0;
            for(int row = 0; row < 10; row++)
            {
                for(int col = 0; col < 10; col++, nIndex++)
                {
                    gridFlashResult.Rows[row].Cells[col].Value = $"0x{results[nIndex].byteError.ToString("X02")}";
                    gridFlashResult.Rows[row].Cells[col].Style.BackColor = results[nIndex].bResult ? Color.LightGreen : Color.Red;

                }
            }
        }

        private void btmFalshCommand_Click(object sender, EventArgs e)
        {
            if (isFlashing)
            {
                isFlashing = false;
                FlashTimeWatch.Stop();
            }
            else
            {
                FlashTimeWatch.Reset();
                FlashTimeWatch.Start();
                isFlashing = true;

                //byte[] type = { 0x01, 0x02, 0x03, 0x04, 0x05 };
                byte[] type = new byte[36]{ 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x00,
                                            0x00, 0x00, 0x01, 0x00, 0x00, 0x00 , 0x00 , 0x00 , 0x00 ,
                                             0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 ,
                                             0x00 , 0x00 , 0x00 , 0x00 , 0x07 , 0x00 , 0x00 , 0x00 , 0x08 };
                flashServer.Flash_Start(type, true, false); //System.Text.Encoding.ASCII.GetString(type)
            }
        }

        private void btmPLCTrigger_Click(object sender, EventArgs e)
        {
            if (PLCServer == null)
                return;

            PLCServer.Write("D1200", (short) 1);
        }

        private void btmReconnectFlashMachine_Click(object sender, EventArgs e)
        {
            try
            {
                if (flashServer.ConnectRS232(rs232Comport, 115200))
                {
                    HPglobal.RecordeAllMessage($"连接烧录机失败{nMachineID},  端口号：{rs232Comport}", true, false, "");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, $"连接烧录机失败{nMachineID},  端口号：{rs232Comport}\r\n{ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void 复位条码数据_Click(object sender, EventArgs e)
        {
            strPCBSN = "";
            strTrayID = "";
        }

        private void txtFlashVer_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void FlashResultSwitchDataMode(bool switchRowCol = false)
        {
            try
            {
                this.Invoke(new Action(() =>
                {
                    chk切换行例数据.Checked = switchRowCol;

                    if (switchRowCol)
                        flashServer.emDataMode = APK_FlashVHP1.emFlashData_Mode.emFlashData_Mode_RowToCol;
                    else
                        flashServer.emDataMode = APK_FlashVHP1.emFlashData_Mode.emFlashData_Mode_Normal;
                }));
            }
            catch(Exception ex)
            {
                HPglobal.m_strLastException = string.Format("FlashResultSwitchDataMode  执行其间发生错误\r\n{0}", ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

        }

        private void chk切换行例数据_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                FlashResultSwitchDataMode(chk切换行例数据.Checked);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    }
}