﻿namespace CamaeraLib
{
    partial class ucCameraPara
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lbName = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbTriggerMode = new System.Windows.Forms.Label();
            this.Trigger = new System.Windows.Forms.ComboBox();
            this.lbTriggerSource = new System.Windows.Forms.Label();
            this.TriggerSource = new UserControlLib.ucNumberText();
            this.ExposureTime = new UserControlLib.ucNumberText();
            this.lb = new System.Windows.Forms.Label();
            this.GainVal = new UserControlLib.ucNumberText();
            this.lbGainVal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(59, 31);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(29, 12);
            this.lbName.TabIndex = 0;
            this.lbName.Text = "Name";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(99, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(144, 21);
            this.textBox1.TabIndex = 1;
            // 
            // lbTriggerMode
            // 
            this.lbTriggerMode.AutoSize = true;
            this.lbTriggerMode.Location = new System.Drawing.Point(17, 63);
            this.lbTriggerMode.Name = "lbTriggerMode";
            this.lbTriggerMode.Size = new System.Drawing.Size(71, 12);
            this.lbTriggerMode.TabIndex = 2;
            this.lbTriggerMode.Text = "TriggerMode";
            // 
            // Trigger
            // 
            this.Trigger.FormattingEnabled = true;
            this.Trigger.Items.AddRange(new object[] {
            "Off",
            "ON"});
            this.Trigger.Location = new System.Drawing.Point(98, 60);
            this.Trigger.Name = "Trigger";
            this.Trigger.Size = new System.Drawing.Size(146, 20);
            this.Trigger.TabIndex = 3;
            this.Trigger.SelectedIndexChanged += new System.EventHandler(this.Trigger_SelectedIndexChanged);
            // 
            // lbTriggerSource
            // 
            this.lbTriggerSource.AutoSize = true;
            this.lbTriggerSource.Location = new System.Drawing.Point(5, 101);
            this.lbTriggerSource.Name = "lbTriggerSource";
            this.lbTriggerSource.Size = new System.Drawing.Size(83, 12);
            this.lbTriggerSource.TabIndex = 4;
            this.lbTriggerSource.Text = "TriggerSource";
            // 
            // TriggerSource
            // 
            this.TriggerSource.Location = new System.Drawing.Point(99, 96);
            this.TriggerSource.MaxValue = 0D;
            this.TriggerSource.MinValue = 0D;
            this.TriggerSource.Name = "TriggerSource";
            this.TriggerSource.Size = new System.Drawing.Size(144, 21);
            this.TriggerSource.TabIndex = 5;
            this.TriggerSource.textBoxType = UserControlLib.TextBoxType.AlphaNumeric;
            this.TriggerSource.TextChanged += new System.EventHandler(this.TriggerSource_TextChanged);
            // 
            // ExposureTime
            // 
            this.ExposureTime.Location = new System.Drawing.Point(98, 134);
            this.ExposureTime.MaxValue = 0D;
            this.ExposureTime.MinValue = 0D;
            this.ExposureTime.Name = "ExposureTime";
            this.ExposureTime.Size = new System.Drawing.Size(146, 21);
            this.ExposureTime.TabIndex = 7;
            this.ExposureTime.textBoxType = UserControlLib.TextBoxType.AlphaNumeric;
            this.ExposureTime.TextChanged += new System.EventHandler(this.ExposureTime_TextChanged);
            // 
            // lb
            // 
            this.lb.AutoSize = true;
            this.lb.Location = new System.Drawing.Point(11, 137);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(77, 12);
            this.lb.TabIndex = 6;
            this.lb.Text = "ExposureTime";
            // 
            // GainVal
            // 
            this.GainVal.Location = new System.Drawing.Point(98, 164);
            this.GainVal.MaxValue = 0D;
            this.GainVal.MinValue = 0D;
            this.GainVal.Name = "GainVal";
            this.GainVal.Size = new System.Drawing.Size(146, 21);
            this.GainVal.TabIndex = 9;
            this.GainVal.textBoxType = UserControlLib.TextBoxType.AlphaNumeric;
            this.GainVal.TextChanged += new System.EventHandler(this.GainVal_TextChanged);
            // 
            // lbGainVal
            // 
            this.lbGainVal.AutoSize = true;
            this.lbGainVal.Location = new System.Drawing.Point(41, 167);
            this.lbGainVal.Name = "lbGainVal";
            this.lbGainVal.Size = new System.Drawing.Size(47, 12);
            this.lbGainVal.TabIndex = 8;
            this.lbGainVal.Text = "GainVal";
            // 
            // ucCameraPara
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GainVal);
            this.Controls.Add(this.lbGainVal);
            this.Controls.Add(this.ExposureTime);
            this.Controls.Add(this.lb);
            this.Controls.Add(this.TriggerSource);
            this.Controls.Add(this.lbTriggerSource);
            this.Controls.Add(this.Trigger);
            this.Controls.Add(this.lbTriggerMode);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbName);
            this.Name = "ucCameraPara";
            this.Size = new System.Drawing.Size(264, 215);
            this.Load += new System.EventHandler(this.ucCameraPara_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbTriggerMode;
        private System.Windows.Forms.ComboBox Trigger;
        private System.Windows.Forms.Label lbTriggerSource;
        private UserControlLib.ucNumberText TriggerSource;
        private UserControlLib.ucNumberText ExposureTime;
        private System.Windows.Forms.Label lb;
        private UserControlLib.ucNumberText GainVal;
        private System.Windows.Forms.Label lbGainVal;
    }
}
