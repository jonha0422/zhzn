using System;
using HalconDotNet;

namespace InteractiveROI
{
	/// <summary>
	/// This class demonstrates one of the possible implementations for a 
	/// linear ROI. ROILine inherits from the base class ROI and 
	/// implements (besides other auxiliary methods) all virtual methods 
	/// defined in ROI.cs.
	/// </summary>
	public class ROILine : ROI
	{

		private double row1, col1;   // first end point of line
		private double row2, col2;   // second end point of line
		private double midR, midC;   // midPoint of line

		private HXLDCont arrowHandleXLD;

        public double Row1
        {
            get
            {
                return row1;
            }

            set
            {
                row1 = value;
            }
        }

        public double Col1
        {
            get
            {
                return col1;
            }

            set
            {
                col1 = value;
            }
        }

        public double Row2
        {
            get
            {
                return row2;
            }

            set
            {
                row2 = value;
            }
        }

        public double Col2
        {
            get
            {
                return col2;
            }

            set
            {
                col2 = value;
            }
        }

        public double MidR
        {
            get
            {
                return midR;
            }

            set
            {
                midR = value;
            }
        }

        public double MidC
        {
            get
            {
                return midC;
            }

            set
            {
                midC = value;
            }
        }

        public ROILine()
		{
			NumHandles = 3;        // two end points of line
			activeHandleIdx = 2;
			arrowHandleXLD = new HXLDCont();
			arrowHandleXLD.GenEmptyObj();
		}


		/// <summary>Creates a new ROI instance at the mouse position.</summary>
		public override void createROI(double midX, double midY)
		{
			MidR = midY;
			MidC = midX;

			Row1 = MidR;
			Col1 = MidC - 50;
			Row2 = MidR;
			Col2 = MidC + 50;

			updateArrowHandle();
		}

		/// <summary>Paints the ROI into the supplied window.</summary>
		public override void draw(HalconDotNet.HWindow window)
		{

			window.DispLine(Row1, Col1, Row2, Col2);

			window.DispRectangle2(Row1, Col1, 0, 5, 5);
			window.DispObj(arrowHandleXLD);  //window.DispRectangle2( row2, col2, 0, 5, 5);
			window.DispRectangle2(MidR, MidC, 0, 5, 5);
		}

		/// <summary> 
		/// Returns the distance of the ROI handle being
		/// closest to the image point(x,y).
		/// </summary>
		public override double distToClosestHandle(double x, double y)
		{

			double max = 10000;
			double [] val = new double[NumHandles];

			val[0] = HMisc.DistancePp(y, x, Row1, Col1); // upper left 
			val[1] = HMisc.DistancePp(y, x, Row2, Col2); // upper right 
			val[2] = HMisc.DistancePp(y, x, MidR, MidC); // midpoint 

			for (int i=0; i < NumHandles; i++)
			{
				if (val[i] < max)
				{
					max = val[i];
					activeHandleIdx = i;
				}
			}// end of for 

			return val[activeHandleIdx];
		}

		/// <summary> 
		/// Paints the active handle of the ROI object into the supplied window. 
		/// </summary>
		public override void displayActive(HalconDotNet.HWindow window)
		{

			switch (activeHandleIdx)
			{
				case 0:
					window.DispRectangle2(Row1, Col1, 0, 5, 5);
					break;
				case 1:
					window.DispObj(arrowHandleXLD); //window.DispRectangle2(row2, col2, 0, 5, 5);
					break;
				case 2:
					window.DispRectangle2(MidR, MidC, 0, 5, 5);
					break;
			}
		}

		/// <summary>Gets the HALCON region described by the ROI.</summary>
		public override HRegion getRegion()
		{
			HRegion region = new HRegion();
			region.GenRegionLine(Row1, Col1, Row2, Col2);
			return region;
		}

		public override double getDistanceFromStartPoint(double row, double col)
		{
			double distance = HMisc.DistancePp(row, col, Row1, Col1);
			return distance;
		}
		/// <summary>
		/// Gets the model information described by 
		/// the ROI.
		/// </summary> 
		public override HTuple getModelData()
		{
			return new HTuple(new double[] { Row1, Col1, Row2, Col2 });
		}

		/// <summary> 
		/// Recalculates the shape of the ROI. Translation is 
		/// performed at the active handle of the ROI object 
		/// for the image coordinate (x,y).
		/// </summary>
		public override void moveByHandle(double newX, double newY)
		{
			double lenR, lenC;

			switch (activeHandleIdx)
			{
				case 0: // first end point
					Row1 = newY;
					Col1 = newX;

					MidR = (Row1 + Row2) / 2;
					MidC = (Col1 + Col2) / 2;
					break;
				case 1: // last end point
					Row2 = newY;
					Col2 = newX;

					MidR = (Row1 + Row2) / 2;
					MidC = (Col1 + Col2) / 2;
					break;
				case 2: // midpoint 
					lenR = Row1 - MidR;
					lenC = Col1 - MidC;

					MidR = newY;
					MidC = newX;

					Row1 = MidR + lenR;
					Col1 = MidC + lenC;
					Row2 = MidR - lenR;
					Col2 = MidC - lenC;
					break;
			}
			updateArrowHandle();
		}


		/// <summary> Auxiliary method </summary>
		private void updateArrowHandle()
		{
			double length,dr,dc, halfHW;
			double rrow1, ccol1,rowP1, colP1, rowP2, colP2;

			double headLength = 15;
			double headWidth  = 15;


			arrowHandleXLD.Dispose();
			arrowHandleXLD.GenEmptyObj();

			rrow1 = Row1 + (Row2 - Row1) * 0.8;
			ccol1 = Col1 + (Col2 - Col1) * 0.8;

			length = HMisc.DistancePp(rrow1, ccol1, Row2, Col2);
			if (length == 0)
				length = -1;

			dr = (Row2 - rrow1) / length;
			dc = (Col2 - ccol1) / length;

			halfHW = headWidth / 2.0;
			rowP1 = rrow1 + (length - headLength) * dr + halfHW * dc;
			rowP2 = rrow1 + (length - headLength) * dr - halfHW * dc;
			colP1 = ccol1 + (length - headLength) * dc - halfHW * dr;
			colP2 = ccol1 + (length - headLength) * dc + halfHW * dr;

			if (length == -1)
				arrowHandleXLD.GenContourPolygonXld(rrow1, ccol1);
			else
				arrowHandleXLD.GenContourPolygonXld(new HTuple(new double[] { rrow1, Row2, rowP1, Row2, rowP2, Row2 }),
													new HTuple(new double[] { ccol1, Col2, colP1, Col2, colP2, Col2 }));
		}

	}//end of class
}//end of namespace
