﻿using GenericLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using ProductData;


namespace RabbitMQLib
{
    //发送消息结构体
    public class MsgData
    {
        public string SentStation { get; set; }//发送工位
        public string ReceiveStation { get; set; }//接收工位
        public string MsgType { get; set; }//消息类型
        public string Msg { get; set; }//消息内容
        public MsgData()
        {
            SentStation = "";
            ReceiveStation = "";
            MsgType = "";
            Msg = "";
        }


    }
    public class MqttSystem :CustomThread
    {
        private static MqttSystem instance;
        private static object theLock = new object();

        public string CurrentStation { get; set; }//当前工位
        public string exchange { get; set; }//交换机
        public string exchangeType { get; set; }//类型
        public string queue { get; set; }//主题
        public RabbitMQConfigSection MQConfig { get; set; }
        public RabbitMQConfigSection MQConfigBak { get; set; }
        private RabbitMQPool MQPool;
        private RabbitMQPool MQPoolBak;

        public delegate void MsgCallback(string queueData);
        [XmlIgnoreAttribute]
        public MsgCallback onMsgCallBack;
        public static MqttSystem Instance
        {
            get
            {
                lock (theLock)
                {
                    if (instance == null)
                    {

                        instance = new MqttSystem();
                        instance.Read();
                        Instance.ReceiveMsg();
                    }
                    return instance;
                }
            }
        }
        public MqttSystem()
        : base("MqttSystem")
        {
            exchange = "ChipIntelligentProductionLine1";
            exchangeType = "topic";
            queue = "queue";
            CurrentStation = "AP01";
            MQConfig = new RabbitMQConfigSection();
            MQConfigBak = new RabbitMQConfigSection();
        }
		

        protected override void MainThread()
        {
            while (this.shutdown == false)
            {
                if (MQPool.isOpen() == false)
                {
                    MQPool = RabbitMQPool.CreatePool(MQConfig);
                }
				Sleep(66);
            }
        }

		public void Stop()
		{
			shutdown = true;
		}

        public bool Read()
        {
            try
            {
                using (TextReader reader1 = new StreamReader(Application.StartupPath + "\\Config\\MqttSystem.xml"))
                //using (TextReader reader1 = new StreamReader("MotionCardConfig.xml"))
                {
                    XmlSerializer serializer1 = new XmlSerializer(typeof(MqttSystem));
                    instance = (MqttSystem)serializer1.Deserialize(reader1);
                    reader1.Close();
                }
            }
            catch
            {
                instance.Writer();
            }

            instance.Init();
            instance.Run();
            return true;
        }
        public bool Writer()
        {
            StreamWriter writer = new StreamWriter(Application.StartupPath + "\\Config\\MqttSystem.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(MqttSystem));
            serializer.Serialize(writer, instance);
            writer.Close();

            return true;
        }
        public int Init()
        {
            try
            {
                MQPool = RabbitMQPool.CreatePool(MQConfig);
                if (MQPool.isOpen() == false)
                {
                    LogLib.Log.ShowWarning("主RabbitMQPool打开失败，请检查主控是否打开", true);
                }
                else
                {
                    int suc = MQPool.ExchangeDeclare(exchange, exchangeType, true, autoDelete: false);
                    if (suc != 0)
                    {
                        LogLib.Log.ShowWarning("创建ExchangeDeclare失败，请检查主控是否打开", true);
                    }
                }


                MQPoolBak = RabbitMQPool.CreatePool(MQConfigBak);
                if (MQPoolBak.isOpen() == false)
                {
                    LogLib.Log.ShowWarning("备用RabbitMQPool打开失败，请检查AP01是否打开", true);
                }
                else
                {
                    int suc = MQPoolBak.ExchangeDeclare(exchange, exchangeType, true, autoDelete: false);
                    if (suc != 0)
                    {
                        LogLib.Log.ShowWarning("创建备用ExchangeDeclare失败，请检查主控是否打开", true);
                    }
                }
                if (MQPool.isOpen() == false && MQPoolBak.isOpen() == false)
                {
                    return -1;
                }
                return 0;
            }
            catch(Exception ex)
            {
                return -2;
            }

            
        }
        /// <summary>
        /// 发送消息到另一工位位
        /// </summary>
        /// <param name="ReceiveStation">目标工位：MasterControl(总控) ALL(所有工位) AP01（上料机）  AP02.1（烧录1） AP02.2 AP02.3  AP03(转盘) AP04(补料) AP05（辅线）   </param>
        /// <param name="MsgType">消息类型</param>
        /// <param name="msgData">消息内容</param>
        /// <returns></returns>
        public int SentMsg(string ReceiveStation, string MsgType, string msgData)
        {
            if (MQPool.isOpen() == false && MQPoolBak.isOpen() == false)
            {
                return -1;
            }
            if (MQPool.isOpen() == true)
            {
                int suc = MQPool.ExchangeDeclare(exchange, exchangeType, true, autoDelete: false);
                if (suc != 0)
                {
                    suc = SentMsgBak(ReceiveStation, MsgType, msgData);
                    return suc;
                }
                MsgData aMsg = new MsgData();
                aMsg.SentStation = CurrentStation;
                aMsg.ReceiveStation = ReceiveStation;
                aMsg.MsgType = MsgType;
                aMsg.Msg = msgData;
                string strMsg = CoverLib.JsonCover.StrSerialize(aMsg);
                LogLib.Log.Info("发送消息：" + strMsg);
                suc =MQPool.PublishMessage(exchange, strMsg, routingKey: "Station." + ReceiveStation);
                if (suc != 0)
                {
                    suc = SentMsgBak(ReceiveStation, MsgType, msgData);
                    return suc;
                }
            }

            return 0;
        }
        private int SentMsgBak(string ReceiveStation, string MsgType, string msgData)
        {
            if ( MQPoolBak.isOpen() == false)
            {
                return -1;
            }
            if (MQPoolBak.isOpen() == false)
            {
                int suc = MQPoolBak.ExchangeDeclare(exchange, exchangeType, true, autoDelete: false);
                if (suc != 0)
                {
                    return 1;
                }
                MsgData aMsg = new MsgData();
                aMsg.SentStation = CurrentStation;
                aMsg.ReceiveStation = ReceiveStation;
                aMsg.MsgType = MsgType;
                aMsg.Msg = msgData;
                string strMsg = CoverLib.JsonCover.StrSerialize(aMsg);
                suc = MQPoolBak.PublishMessage(exchange, strMsg, routingKey: "Station." + ReceiveStation);
                if (suc != 0)
                {
                    suc = SentMsgBak(ReceiveStation, MsgType, msgData);
                    return 2;
                }
            }
            return 0;
        }
        public int ReceiveMsg()
        {
            if (MQPool.isOpen() == false)
            {
                return -1;
            }
            //var pool = RabbitMQPool.Default;
            MQPool.QueueDeclareAndBind(CurrentStation, exchange, autoDeleteTime: 1800000, routingKey: "Station." + CurrentStation);
            MQPool.QueueDeclareAndBind(CurrentStation, exchange, autoDeleteTime: 1800000, routingKey: "Station.ALL");
            queue = CurrentStation;
            MQPool.WaitQueue(queue, (data, headers) =>
            {
                try
                {
                    if (onMsgCallBack != null)
                    {
                        onMsgCallBack(data);
                    }
                    LogLib.Log.Info("收到消息：" + data);
                    MsgData aMsgData = CoverLib.JsonCover.DeStrLSerialize<MsgData>(data);
                    //根据收到不同的消息类型，进行相应的处理---各工位不同

                    if (aMsgData.MsgType == "RequestPCBData")//请求PCB的信息
                    {
                       
                    }
                    if (aMsgData.MsgType == "RequestPCBData_Bak")//接受到上工位的PCB信息 主要是转盘工位用
                    {
                       
                    }
                    
                }
                catch
                {
                    LogLib.Log.Warning(data);
                }
                
                //Thread.Sleep(100); // 休眠再退出，测试qos情况
            });
            return 0;
        }
        public int sentProductDataTo(int ProductID, string SentStation, string msgTpe)
        {

            ProductData.ProductSystem.Instance.ProductDataList[ProductID].AddProductLogToDb("发送数据给：" + SentStation);
            string msgdata = CoverLib.JsonCover.StrSerialize(ProductData.ProductSystem.Instance.ProductDataList[ProductID]);//把一个PCB的信息格式化成JSON
           // string SentStation = "AP02." + (i + 1).ToString();
            //int suc = RabbitMQLib.MqttSystem.Instance.SentMsg(SentStation, "BurnPCBData", msgdata);
            int suc = RabbitMQLib.MqttSystem.Instance.SentMsg(SentStation, msgTpe, msgdata);
            if (suc != 0)//发送不成功
            {
                LogLib.Log.ShowWarning("发送消息不成功消息类型:[RequestPCBData_Bak]" + " 目标工位：" + SentStation, true, "通信警告", "RequestPCBData");
                return -1; ;
            }
            return 0;
        }
           
    }
}
