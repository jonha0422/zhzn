﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HalconDotNet;

namespace HalconFormDonet
{
    public partial class Parameters : Form
    {
        HalconForm camera = null;

        public Parameters(HalconForm cameraForm)
        {
            InitializeComponent();
            camera = cameraForm;
            comboBox2.SelectedIndex = 4;
        }

        private void btmReference_Click(object sender, EventArgs e)
        {
            try
            {
                HTuple information = new HTuple();
                HTuple valueList = new HTuple();
                HOperatorSet.InfoFramegrabber(comboBox2.Text.Trim(), "info_boards", out information, out valueList);
                string[] deviceList = valueList;
                comboBox1.Items.Clear();
                txtPort.Items.Clear();
                if (deviceList.Length > 0)
                {
                    for(int nIndex = 0; nIndex < deviceList.Length; nIndex++)
                    {
                        //device: DahuaTechnology: 5F0123CGAK00030 port:0;
                        string strName = deviceList[nIndex].Replace("device: ", "");
                        strName = strName.Replace("device:", "");
                        strName = strName.Replace(" port:", ";");

                        string[] nameList = strName.Split(";".ToCharArray());
                        string deviceName = nameList[0].Replace(" ", "");
                        comboBox1.Items.Add(deviceName);

                        if (nameList.Length >= 2)
                            txtPort.Items.Add(nameList[1]);
                    }

                    comboBox1.SelectedIndex = 0;
                    txtPort.SelectedIndex = 0;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show($"btmReference_Click 错误\n{ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void form_Closing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        public void updataParameters()
        {

        }

        private void btmSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (camera != null)
                {
                    camera.hAcqDriverName = comboBox2.Text.Trim();
                    camera.cameraName = comboBox1.Text.Trim() ;
                    camera.nPort = int.Parse(txtPort.Text.Trim());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show($"btmSave_Click 错误\n{ex.Message}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
          
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtPort.SelectedIndex = comboBox1.SelectedIndex;
            }
            catch(Exception ex)
            {

            }
        }
    }
}
