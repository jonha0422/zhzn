﻿namespace LanguageLib
{
    partial class ucSelectLanguage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pEnglishUSA = new System.Windows.Forms.PictureBox();
            this.pJapanese = new System.Windows.Forms.PictureBox();
            this.pBulgarian = new System.Windows.Forms.PictureBox();
            this.pHungarian = new System.Windows.Forms.PictureBox();
            this.pPolish = new System.Windows.Forms.PictureBox();
            this.pEstonian = new System.Windows.Forms.PictureBox();
            this.pPortuguese = new System.Windows.Forms.PictureBox();
            this.pSpanish = new System.Windows.Forms.PictureBox();
            this.pChinese = new System.Windows.Forms.PictureBox();
            this.pDanish = new System.Windows.Forms.PictureBox();
            this.pSwedish = new System.Windows.Forms.PictureBox();
            this.pRussian = new System.Windows.Forms.PictureBox();
            this.pFrensh = new System.Windows.Forms.PictureBox();
            this.pFinnish = new System.Windows.Forms.PictureBox();
            this.pGerman = new System.Windows.Forms.PictureBox();
            this.pEnglish = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pEnglishUSA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJapanese)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBulgarian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pHungarian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pPolish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEstonian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pPortuguese)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pSpanish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pChinese)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pDanish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pSwedish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRussian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pFrensh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pFinnish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pGerman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEnglish)).BeginInit();
            this.SuspendLayout();
            // 
            // pEnglishUSA
            // 
            this.pEnglishUSA.Image = global::LanguageLib.Properties.Resources.USA;
            this.pEnglishUSA.Location = new System.Drawing.Point(570, 0);
            this.pEnglishUSA.Name = "pEnglishUSA";
            this.pEnglishUSA.Size = new System.Drawing.Size(38, 32);
            this.pEnglishUSA.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pEnglishUSA.TabIndex = 15;
            this.pEnglishUSA.TabStop = false;
            this.pEnglishUSA.Click += new System.EventHandler(this.pEnglishUSA_Click);
            // 
            // pJapanese
            // 
            this.pJapanese.Image = global::LanguageLib.Properties.Resources.Japan;
            this.pJapanese.Location = new System.Drawing.Point(532, 0);
            this.pJapanese.Name = "pJapanese";
            this.pJapanese.Size = new System.Drawing.Size(38, 32);
            this.pJapanese.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pJapanese.TabIndex = 14;
            this.pJapanese.TabStop = false;
            this.pJapanese.Click += new System.EventHandler(this.pJapanese_Click);
            // 
            // pBulgarian
            // 
            this.pBulgarian.Image = global::LanguageLib.Properties.Resources.Bulgaria_Flag;
            this.pBulgarian.Location = new System.Drawing.Point(494, 0);
            this.pBulgarian.Name = "pBulgarian";
            this.pBulgarian.Size = new System.Drawing.Size(38, 32);
            this.pBulgarian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pBulgarian.TabIndex = 13;
            this.pBulgarian.TabStop = false;
            this.pBulgarian.Click += new System.EventHandler(this.pBulgarian_Click);
            // 
            // pHungarian
            // 
            this.pHungarian.Image = global::LanguageLib.Properties.Resources.Hungary_Flag;
            this.pHungarian.Location = new System.Drawing.Point(456, 0);
            this.pHungarian.Name = "pHungarian";
            this.pHungarian.Size = new System.Drawing.Size(38, 32);
            this.pHungarian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pHungarian.TabIndex = 12;
            this.pHungarian.TabStop = false;
            this.pHungarian.Click += new System.EventHandler(this.pHungarian_Click);
            // 
            // pPolish
            // 
            this.pPolish.Image = global::LanguageLib.Properties.Resources.Poland;
            this.pPolish.Location = new System.Drawing.Point(418, 0);
            this.pPolish.Name = "pPolish";
            this.pPolish.Size = new System.Drawing.Size(38, 32);
            this.pPolish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pPolish.TabIndex = 11;
            this.pPolish.TabStop = false;
            this.pPolish.Click += new System.EventHandler(this.pPolish_Click);
            // 
            // pEstonian
            // 
            this.pEstonian.Image = global::LanguageLib.Properties.Resources.Estonia;
            this.pEstonian.Location = new System.Drawing.Point(380, 0);
            this.pEstonian.Name = "pEstonian";
            this.pEstonian.Size = new System.Drawing.Size(38, 32);
            this.pEstonian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pEstonian.TabIndex = 10;
            this.pEstonian.TabStop = false;
            this.pEstonian.Visible = false;
            this.pEstonian.Click += new System.EventHandler(this.pEstonian_Click);
            // 
            // pPortuguese
            // 
            this.pPortuguese.Image = global::LanguageLib.Properties.Resources.Portugal;
            this.pPortuguese.Location = new System.Drawing.Point(342, 0);
            this.pPortuguese.Name = "pPortuguese";
            this.pPortuguese.Size = new System.Drawing.Size(38, 32);
            this.pPortuguese.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pPortuguese.TabIndex = 9;
            this.pPortuguese.TabStop = false;
            this.pPortuguese.Visible = false;
            this.pPortuguese.Click += new System.EventHandler(this.pPortuguese_Click);
            // 
            // pSpanish
            // 
            this.pSpanish.Image = global::LanguageLib.Properties.Resources.Spain;
            this.pSpanish.Location = new System.Drawing.Point(304, 0);
            this.pSpanish.Name = "pSpanish";
            this.pSpanish.Size = new System.Drawing.Size(38, 32);
            this.pSpanish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pSpanish.TabIndex = 8;
            this.pSpanish.TabStop = false;
            this.pSpanish.Visible = false;
            this.pSpanish.Click += new System.EventHandler(this.pSpanish_Click);
            // 
            // pChinese
            // 
            this.pChinese.Image = global::LanguageLib.Properties.Resources.China;
            this.pChinese.Location = new System.Drawing.Point(266, 0);
            this.pChinese.Name = "pChinese";
            this.pChinese.Size = new System.Drawing.Size(38, 32);
            this.pChinese.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pChinese.TabIndex = 7;
            this.pChinese.TabStop = false;
            this.pChinese.Visible = false;
            this.pChinese.Click += new System.EventHandler(this.pChinese_Click);
            // 
            // pDanish
            // 
            this.pDanish.Image = global::LanguageLib.Properties.Resources.Denmark;
            this.pDanish.Location = new System.Drawing.Point(228, 0);
            this.pDanish.Name = "pDanish";
            this.pDanish.Size = new System.Drawing.Size(38, 32);
            this.pDanish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pDanish.TabIndex = 6;
            this.pDanish.TabStop = false;
            this.pDanish.Visible = false;
            this.pDanish.Click += new System.EventHandler(this.pDanish_Click);
            // 
            // pSwedish
            // 
            this.pSwedish.Image = global::LanguageLib.Properties.Resources.Sweden;
            this.pSwedish.Location = new System.Drawing.Point(190, 0);
            this.pSwedish.Name = "pSwedish";
            this.pSwedish.Size = new System.Drawing.Size(38, 32);
            this.pSwedish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pSwedish.TabIndex = 5;
            this.pSwedish.TabStop = false;
            this.pSwedish.Visible = false;
            this.pSwedish.Click += new System.EventHandler(this.pSwedish_Click);
            // 
            // pRussian
            // 
            this.pRussian.Image = global::LanguageLib.Properties.Resources.Russian;
            this.pRussian.Location = new System.Drawing.Point(152, 0);
            this.pRussian.Name = "pRussian";
            this.pRussian.Size = new System.Drawing.Size(38, 32);
            this.pRussian.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pRussian.TabIndex = 4;
            this.pRussian.TabStop = false;
            this.pRussian.Visible = false;
            this.pRussian.Click += new System.EventHandler(this.pRussian_Click);
            // 
            // pFrensh
            // 
            this.pFrensh.Image = global::LanguageLib.Properties.Resources.France;
            this.pFrensh.Location = new System.Drawing.Point(114, 0);
            this.pFrensh.Name = "pFrensh";
            this.pFrensh.Size = new System.Drawing.Size(38, 32);
            this.pFrensh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pFrensh.TabIndex = 3;
            this.pFrensh.TabStop = false;
            this.pFrensh.Visible = false;
            this.pFrensh.Click += new System.EventHandler(this.pFrensh_Click);
            // 
            // pFinnish
            // 
            this.pFinnish.Image = global::LanguageLib.Properties.Resources.Finland;
            this.pFinnish.Location = new System.Drawing.Point(76, 0);
            this.pFinnish.Name = "pFinnish";
            this.pFinnish.Size = new System.Drawing.Size(38, 32);
            this.pFinnish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pFinnish.TabIndex = 2;
            this.pFinnish.TabStop = false;
            this.pFinnish.Visible = false;
            this.pFinnish.Click += new System.EventHandler(this.pFinnish_Click);
            // 
            // pGerman
            // 
            this.pGerman.Image = global::LanguageLib.Properties.Resources.Germany;
            this.pGerman.Location = new System.Drawing.Point(38, 0);
            this.pGerman.Name = "pGerman";
            this.pGerman.Size = new System.Drawing.Size(38, 32);
            this.pGerman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pGerman.TabIndex = 1;
            this.pGerman.TabStop = false;
            this.pGerman.Visible = false;
            this.pGerman.Click += new System.EventHandler(this.pGerman_Click);
            // 
            // pEnglish
            // 
            this.pEnglish.Image = global::LanguageLib.Properties.Resources.UK;
            this.pEnglish.InitialImage = null;
            this.pEnglish.Location = new System.Drawing.Point(0, 0);
            this.pEnglish.Name = "pEnglish";
            this.pEnglish.Size = new System.Drawing.Size(38, 32);
            this.pEnglish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pEnglish.TabIndex = 0;
            this.pEnglish.TabStop = false;
            this.pEnglish.Visible = false;
            this.pEnglish.Click += new System.EventHandler(this.pEnglish_Click);
            // 
            // ucSelectLanguage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.pEnglishUSA);
            this.Controls.Add(this.pJapanese);
            this.Controls.Add(this.pBulgarian);
            this.Controls.Add(this.pHungarian);
            this.Controls.Add(this.pPolish);
            this.Controls.Add(this.pEstonian);
            this.Controls.Add(this.pPortuguese);
            this.Controls.Add(this.pSpanish);
            this.Controls.Add(this.pChinese);
            this.Controls.Add(this.pDanish);
            this.Controls.Add(this.pSwedish);
            this.Controls.Add(this.pRussian);
            this.Controls.Add(this.pFrensh);
            this.Controls.Add(this.pFinnish);
            this.Controls.Add(this.pGerman);
            this.Controls.Add(this.pEnglish);
            this.Name = "ucSelectLanguage";
            this.Size = new System.Drawing.Size(609, 32);
            this.Load += new System.EventHandler(this.ucSelectLanguage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pEnglishUSA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pJapanese)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBulgarian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pHungarian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pPolish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEstonian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pPortuguese)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pSpanish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pChinese)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pDanish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pSwedish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRussian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pFrensh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pFinnish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pGerman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEnglish)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pEnglish;
        private System.Windows.Forms.PictureBox pGerman;
        private System.Windows.Forms.PictureBox pFinnish;
        private System.Windows.Forms.PictureBox pFrensh;
        private System.Windows.Forms.PictureBox pRussian;
        private System.Windows.Forms.PictureBox pSwedish;
        private System.Windows.Forms.PictureBox pDanish;
        private System.Windows.Forms.PictureBox pChinese;
        private System.Windows.Forms.PictureBox pSpanish;
        private System.Windows.Forms.PictureBox pPortuguese;
        private System.Windows.Forms.PictureBox pEstonian;
        private System.Windows.Forms.PictureBox pPolish;
        private System.Windows.Forms.PictureBox pHungarian;
        private System.Windows.Forms.PictureBox pBulgarian;
        private System.Windows.Forms.PictureBox pJapanese;
        private System.Windows.Forms.PictureBox pEnglishUSA;
    }
}
