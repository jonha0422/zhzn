using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace daitou
{
	public class TableControllerFour : XtreeNodeController
	{
		protected TableDefFour tableDefFour;
		protected TableDefThree tableDef;

		public override string Name
		{
			get { return tableDefFour.Name; }
			set {tableDefFour.Name=value;}
		}
        public override string Pridata
        {
            get { return tableDefFour.Pridata; }
            set { tableDefFour.Pridata = value; }
        }
        public TableDefFour TableDefFour
		{
			get { return tableDefFour; }
		}

		public override object Item
		{
			get { return tableDefFour; }
		}

		public TableControllerFour()
		{
		}

		public TableControllerFour(TableDefFour tableDefFour)
		{
			this.tableDefFour = tableDefFour;
		}

		public override int Index(object item)
		{
            return tableDefFour.Fields.IndexOf(((TableControllerFive)item).TableDefFive);
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
            tableDef = ((TableControllerThree)parentInstance).TableDefThree;
			tableDefFour = new TableDefFour();
            tableDef.Fields.Add(tableDefFour);
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableControllerThree)parentInstance).TableDefThree;
            tableDef.Fields.Remove(tableDefFour);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableControllerThree)parentInstance).TableDefThree;
            tableDef.Fields.Remove(tableDefFour);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            tableDef = ((TableControllerThree)parentInstance).TableDefThree;
            tableDef.Fields.Insert(idx, tableDefFour);
		}

		public override void Select(TreeNode tn)
		{
			//Program.Properties.SelectedObject = tableDefFour;
		}
	}
}
