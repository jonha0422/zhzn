﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APK_HP.HPSql
{
    public class SQLServiceConnect
    {
        private static SQLServiceConnect _instance = null;
        private static object SqlLock = new object();
        private static   SqlCommand m_dbCommand = new SqlCommand();
        public static SQLServiceConnect Instance
        {
            get
            {
                lock (SqlLock)
                {
                    if (_instance == null)
                    {
                        _instance = new SQLServiceConnect();
                    }
                    return _instance;
                }
            }
        }
        public static SqlConnection conn = null;
        public static string connSql = " Data Source=192.168.1.100;Database=HPDatabase;User ID =sa; Password=123456;";
        static SqlConnection connn = null;

        //"dataBaseSQL.m_strMySQL_IP = "192.168.1.100";
        //            dataBaseSQL.m_strMySQL_Port = "1433";
        //            dataBaseSQL.m_strMySQL_User = "sa";
        //            dataBaseSQL.m_strMySQL_PWS = "123456";";

        /// <summary>
        ///   连接数据库
        /// </summary>
        /// <param name="Service">连接地址</param>
        /// <param name="Database">数据库名称</param>
        /// <param name="User">用户名</param>
        /// <param name="Pwd">用户密码</param>
        /// <returns></returns>
        public static bool Conn(/*string Service,string Database,string User,string Pwd*/)
        {
            try
            {
                //获取或设置用于打开 SQL Server 数据库的字符串
                conn.ConnectionString = " Data Source=192.168.1.100;Database=HPDatabase;User ID =sa; Password=123456;";
                try
                {
                    //打开数据库
                    conn.Open();
                    if (conn.State.ToString() != "Open")
                    {
                        return false;
                    }
                }
                catch (SqlException ex)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region 绑盘解绑
        /// <summary>
        /// 通过查询PCBID是否存在，绑定到托盘并更新到PCB板表
        /// </summary>
        /// <param name="QrCode">载盘二维码</param>
        /// <param name="PCBID"></param>
        /// <returns></returns>
        public static bool SQL_BindQrCode(string QrCode, string PCBID,string CurrentState,string WorkOrderID,string PCBQrCode)
        {
            try
            {
                string Sql = string.Format("SELECT * FROM LoadPlateTable WHERE QrCode='{0}'", QrCode);
                var res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    //如果没有添加新的 QrCode 状态是没有绑定的
                    Sql = string.Format("Insert into LoadPlateTable(QrCode,PCBID,BindFlag,Unbind,BindTime,CurrentState)values('{0}','{1}','{2}','{3}','{4}','{5}')", QrCode, PCBID, 0, 1,DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "空闲");//BindFlag绑定必须是1，UnBind必须是0
                    var EXres = RunSql(Sql);
                    if (EXres<=0)
                        return false;
                    Sql = string.Format("UPDATE  LoadPlateTable Set PCBID='{1}' ,BindFlag='{2}',UnBind='{3}',BindTime='{4}'where QrCode='{0}'", QrCode, PCBID, 1, 0, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                else
                {
                    //如果有查询是否绑定，无绑定则绑定，有则返回
                    //查询虚拟ID是否存在，不存在就更新，存在则报错
                    //if (res.HasRows)
                    //{
                        Sql = string.Format("UPDATE  LoadPlateTable Set PCBID='{1}' ,BindFlag='{2}',UnBind='{3}',BindTime='{4}'where QrCode='{0}'", QrCode, PCBID, 1, 0, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    //}
                    //虚拟PCBID不存在，根据托盘二维码更新数据，并绑定,
                }
                //更新PCB板表
                Sql= string.Format("SELECT * FROM PCBIDTable WHERE QrCode='{0}'", QrCode);
                 res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    Sql = string.Format("Insert into PCBIDTable(PCBID,WorkOrderID,PCBQrCode,QrCode)values('{0}','{1}','{2}','{3}')", PCBID, WorkOrderID, PCBQrCode, QrCode);//BindFlag绑定必须是1，UnBind必须是0
                    var EXres = RunSql(Sql);
                    if (EXres <= 0)
                        return false;
                }
                else
                {
                    Sql = string.Format("UPDATE  PCBIDTable Set PCBID='{0}' ,WorkOrderID='{1}',PCBQrCode='{2}',QrCode='{3}'where QrCode='{4}'", PCBID, WorkOrderID, PCBQrCode, QrCode);
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //生成一个历史数据表格，后续每个工站，更新该数据唯一
                Sql = string.Format("insert into HistoryTable(QrCode,PCBID,BindTime,WorkOrderID,PCBQrCode,WorkStationName1,ExecutionResult1,ExecutionTime1,WorkStationName2,ExecutionResult2,ExecutionTime2,WorkStationName3,ExecutionResult3,ExecutionTime3,WorkStationName4,ExecutionResult4,ExecutionTime4,WorkStationName5,ExecutionResult5,ExecutionTime5,WorkStationName6,ExecutionResult6,ExecutionTime6,WorkStationName7,ExecutionResult7,ExecutionTime7)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}')"
                    , QrCode ,PCBID,dt, WorkOrderID, PCBQrCode,"等待上料工序",0,dt,"","","","","","","","","","","","","","","","","","");
              var rest=   RunSql(Sql);
                if (rest != 1)
                    return false;              
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 通过查询PCBID是否存在，解绑托盘，删除PCB虚拟ID
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <returns></returns>
        public static bool SQL_UnBindQrCode(string QrCode, string PCBID)
        {
            try
            {
                var Sql = string.Format("SELECT * FROM LoadPlateTable WHERE QrCode='{0}'", QrCode);
                var res = ExecuteReader(Sql);
                if (res.HasRows)
                {
                    Sql = string.Format("SELECT * FROM LoadPlateTable WHERE PCBID='{0}'", PCBID);
                     res = ExecuteReader(Sql);
                    if (res.HasRows)
                    {
                        //如果存在，更新绑定，清空虚拟PCBID，托盘状态变更为可用状态
                        Sql = string.Format("UPDATE LoadPlateTable SET PCBID='{1}',BindFlag='{2}',UnBind='{3}',CurrentState='{4}',BindTime='{5}'where QrCode='{0}'", QrCode, PCBID, 0, 1,"空闲","");
                        var N = RunSql(Sql);
                        if (N != 1)
                            return false;
                    }
                }
                //else if (N == 0)
                //{
                //    string a = "提示解绑OK,但不执行操作,没有该数据";
                //}
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion


        #region 工站数据更新
        /// <summary>
        /// 上料数据更新
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="itemparam"></param>
        /// <returns></returns>
        public static bool FeddingStationDate(string QrCode, string PCBID, ItemParameter itemparam)
        {
            try
            {
                string iPCBID;
                string iQrCode;
                //先绑盘在有更新上料数据
                //查询PCBID QrCode是否同时存在，如果不存在则为没有绑盘
                var Sql = string.Format("SELECT * FROM FeedingCheckTable WHERE PCBID='{0}'", PCBID);
                var res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    Sql = string.Format("Insert into FeedingCheckTable(PCBID,WorkStationName,DataNumber,ExecutionResult,ItemName1,ItemCriteria1,ItemMin1,ItemMax1,ItemName2,ItemCriteria2,ItemMin2,ItemMax2,ItemName3,ItemCriteria3,ItemMin3,ItemMax3,ItemName4,ItemCriteria4,ItemMin4,ItemMax4,ItemName5,ItemCriteria5,ItemMin5,ItemMax5,ItemName6,ItemCriteria6,ItemMin6,ItemMax6,ItemName7,ItemCriteria7,ItemMin7,ItemMax7,ItemName8,ItemCriteria8,ItemMin8,ItemMax8,ItemName9,ItemCriteria9,ItemMin9,ItemMax9,ItemName10,ItemCriteria10,ItemMin10,ItemMax10)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}')"
                       , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult
                      , itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                       );
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                else
                {
                    Sql = string.Format("UPDATE FeedingCheckTable SET PCBID='{0}',WorkStationName='{1}',DataNumber='{2}',ExecutionResult='{3}',ItemName1='{4}',ItemCriteria1='{5}',ItemMin1='{6}',ItemMax1='{7}',ItemName2='{8}',ItemCriteria2='{9}',ItemMin2='{10}',ItemMax2='{11}',ItemName3='{12}',ItemCriteria3='{13}',ItemMin3='{14}',ItemMax3='{15}',ItemName4='{16}',ItemCriteria4='{17}',ItemMin4='{18}',ItemMax4='{19}' ,ItemName5='{20}',ItemCriteria5='{21}',ItemMin5='{22}',ItemMax5='{23}',ItemName6='{24}',ItemCriteria6='{25}',ItemMin6='{26}',ItemMax6='{27}',ItemName7='{28}',ItemCriteria7='{29}',ItemMin7='{30}',ItemMax7='{31}',ItemName8='{32}',ItemCriteria8='{33}',ItemMin8='{34}',ItemMax8='{35}',ItemName9='{36}',ItemCriteria9='{37}',ItemMin9='{38}',ItemMax9='{39}',ItemName10='{40}',ItemCriteria10='{41}',ItemMin10='{42}',ItemMax10='{43}' where PCBID='{0}'"
                      , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult, itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                        );
                    var intRes = RunSql(Sql);
                    if (intRes != 1)
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 烧录数据更新
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="itemparam"></param>
        /// <returns></returns>
        public static bool BurnStationDate(string QrCode, string PCBID, ItemParameter itemparam)
        {
            try
            {
                string iPCBID;
                string iQrCode;
                //先绑盘在有更新上料数据
                //查询PCBID QrCode是否同时存在，如果不存在则为没有绑盘
                var Sql = string.Format("SELECT * FROM BurnTable WHERE PCBID='{0}'", PCBID);
                var res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    Sql = string.Format("Insert into BurnTable(PCBID,WorkStationName,DataNumber,ExecutionResult,ItemName1,ItemCriteria1,ItemMin1,ItemMax1,ItemName2,ItemCriteria2,ItemMin2,ItemMax2,ItemName3,ItemCriteria3,ItemMin3,ItemMax3,ItemName4,ItemCriteria4,ItemMin4,ItemMax4,ItemName5,ItemCriteria5,ItemMin5,ItemMax5,ItemName6,ItemCriteria6,ItemMin6,ItemMax6,ItemName7,ItemCriteria7,ItemMin7,ItemMax7,ItemName8,ItemCriteria8,ItemMin8,ItemMax8,ItemName9,ItemCriteria9,ItemMin9,ItemMax9,ItemName10,ItemCriteria10,ItemMin10,ItemMax10)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}')"
                       , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult
                      , itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                       );
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                else
                {
                    Sql = string.Format("UPDATE BurnTable SET PCBID='{0}',WorkStationName='{1}',DataNumber='{2}',ExecutionResult='{3}',ItemName1='{4}',ItemCriteria1='{5}',ItemMin1='{6}',ItemMax1='{7}',ItemName2='{8}',ItemCriteria2='{9}',ItemMin2='{10}',ItemMax2='{11}',ItemName3='{12}',ItemCriteria3='{13}',ItemMin3='{14}',ItemMax3='{15}',ItemName4='{16}',ItemCriteria4='{17}',ItemMin4='{18}',ItemMax4='{19}' ,ItemName5='{20}',ItemCriteria5='{21}',ItemMin5='{22}',ItemMax5='{23}',ItemName6='{24}',ItemCriteria6='{25}',ItemMin6='{26}',ItemMax6='{27}',ItemName7='{28}',ItemCriteria7='{29}',ItemMin7='{30}',ItemMax7='{31}',ItemName8='{32}',ItemCriteria8='{33}',ItemMin8='{34}',ItemMax8='{35}',ItemName9='{36}',ItemCriteria9='{37}',ItemMin9='{38}',ItemMax9='{39}',ItemName10='{40}',ItemCriteria10='{41}',ItemMin10='{42}',ItemMax10='{43}' where PCBID='{0}'"
                      , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult, itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                        );
                    var intRes = RunSql(Sql);
                    if (intRes != 1)
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 打码数据更新
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="itemparam"></param>
        /// <returns></returns>
        public static bool HitCodeStationDate(string QrCode, string PCBID, ItemParameter itemparam)
        {
            try
            {
                string iPCBID;
                string iQrCode;
                //先绑盘在有更新上料数据
                //查询PCBID QrCode是否同时存在，如果不存在则为没有绑盘
                var Sql = string.Format("SELECT * FROM HisCodeTable WHERE PCBID='{0}'", PCBID);
                var res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    Sql = string.Format("Insert into HisCodeTable(PCBID,WorkStationName,DataNumber,ExecutionResult,ItemName1,ItemCriteria1,ItemMin1,ItemMax1,ItemName2,ItemCriteria2,ItemMin2,ItemMax2,ItemName3,ItemCriteria3,ItemMin3,ItemMax3,ItemName4,ItemCriteria4,ItemMin4,ItemMax4,ItemName5,ItemCriteria5,ItemMin5,ItemMax5,ItemName6,ItemCriteria6,ItemMin6,ItemMax6,ItemName7,ItemCriteria7,ItemMin7,ItemMax7,ItemName8,ItemCriteria8,ItemMin8,ItemMax8,ItemName9,ItemCriteria9,ItemMin9,ItemMax9,ItemName10,ItemCriteria10,ItemMin10,ItemMax10)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}')"
                       , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult
                      , itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                       );
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                else
                {
                    Sql = string.Format("UPDATE HisCodeTable SET PCBID='{0}',WorkStationName='{1}',DataNumber='{2}',ExecutionResult='{3}',ItemName1='{4}',ItemCriteria1='{5}',ItemMin1='{6}',ItemMax1='{7}',ItemName2='{8}',ItemCriteria2='{9}',ItemMin2='{10}',ItemMax2='{11}',ItemName3='{12}',ItemCriteria3='{13}',ItemMin3='{14}',ItemMax3='{15}',ItemName4='{16}',ItemCriteria4='{17}',ItemMin4='{18}',ItemMax4='{19}' ,ItemName5='{20}',ItemCriteria5='{21}',ItemMin5='{22}',ItemMax5='{23}',ItemName6='{24}',ItemCriteria6='{25}',ItemMin6='{26}',ItemMax6='{27}',ItemName7='{28}',ItemCriteria7='{29}',ItemMin7='{30}',ItemMax7='{31}',ItemName8='{32}',ItemCriteria8='{33}',ItemMin8='{34}',ItemMax8='{35}',ItemName9='{36}',ItemCriteria9='{37}',ItemMin9='{38}',ItemMax9='{39}',ItemName10='{40}',ItemCriteria10='{41}',ItemMin10='{42}',ItemMax10='{43}' where PCBID='{0}'"
                      , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult, itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                        );
                    var intRes = RunSql(Sql);
                    if (intRes != 1)
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 切割数据更新
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="itemparam"></param>
        /// <returns></returns>
        public static bool CuttingDataStationDate(string QrCode, string PCBID, ItemParameter itemparam)
        {
            try
            {
                string iPCBID;
                string iQrCode;
                //先绑盘在有更新上料数据
                //查询PCBID QrCode是否同时存在，如果不存在则为没有绑盘
                var Sql = string.Format("SELECT * FROM CuttingTable WHERE PCBID='{0}'", PCBID);
                var res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    Sql = string.Format("Insert into CuttingTable(PCBID,WorkStationName,DataNumber,ExecutionResult,ItemName1,ItemCriteria1,ItemMin1,ItemMax1,ItemName2,ItemCriteria2,ItemMin2,ItemMax2,ItemName3,ItemCriteria3,ItemMin3,ItemMax3,ItemName4,ItemCriteria4,ItemMin4,ItemMax4,ItemName5,ItemCriteria5,ItemMin5,ItemMax5,ItemName6,ItemCriteria6,ItemMin6,ItemMax6,ItemName7,ItemCriteria7,ItemMin7,ItemMax7,ItemName8,ItemCriteria8,ItemMin8,ItemMax8,ItemName9,ItemCriteria9,ItemMin9,ItemMax9,ItemName10,ItemCriteria10,ItemMin10,ItemMax10)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}')"
                       , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult
                      , itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                       );
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                else
                {
                    Sql = string.Format("UPDATE CuttingTable SET PCBID='{0}',WorkStationName='{1}',DataNumber='{2}',ExecutionResult='{3}',ItemName1='{4}',ItemCriteria1='{5}',ItemMin1='{6}',ItemMax1='{7}',ItemName2='{8}',ItemCriteria2='{9}',ItemMin2='{10}',ItemMax2='{11}',ItemName3='{12}',ItemCriteria3='{13}',ItemMin3='{14}',ItemMax3='{15}',ItemName4='{16}',ItemCriteria4='{17}',ItemMin4='{18}',ItemMax4='{19}' ,ItemName5='{20}',ItemCriteria5='{21}',ItemMin5='{22}',ItemMax5='{23}',ItemName6='{24}',ItemCriteria6='{25}',ItemMin6='{26}',ItemMax6='{27}',ItemName7='{28}',ItemCriteria7='{29}',ItemMin7='{30}',ItemMax7='{31}',ItemName8='{32}',ItemCriteria8='{33}',ItemMin8='{34}',ItemMax8='{35}',ItemName9='{36}',ItemCriteria9='{37}',ItemMin9='{38}',ItemMax9='{39}',ItemName10='{40}',ItemCriteria10='{41}',ItemMin10='{42}',ItemMax10='{43}' where PCBID='{0}'"
                      , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult, itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                        );
                    var intRes = RunSql(Sql);
                    if (intRes != 1)
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        ///  包装数据更新
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="itemparam"></param>
        /// <returns></returns>
        public static bool PackagingStationData(string QrCode, string PCBID, ItemParameter itemparam)
        {
            try
            {
                string iPCBID;
                string iQrCode;
                //先绑盘在有更新上料数据
                //查询PCBID QrCode是否同时存在，如果不存在则为没有绑盘
                var Sql = string.Format("SELECT * FROM PackagingTable WHERE PCBID='{0}'", PCBID);
                var res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    Sql = string.Format("Insert into PackagingTable(PCBID,WorkStationName,DataNumber,ExecutionResult,ItemName1,ItemCriteria1,ItemMin1,ItemMax1,ItemName2,ItemCriteria2,ItemMin2,ItemMax2,ItemName3,ItemCriteria3,ItemMin3,ItemMax3,ItemName4,ItemCriteria4,ItemMin4,ItemMax4,ItemName5,ItemCriteria5,ItemMin5,ItemMax5,ItemName6,ItemCriteria6,ItemMin6,ItemMax6,ItemName7,ItemCriteria7,ItemMin7,ItemMax7,ItemName8,ItemCriteria8,ItemMin8,ItemMax8,ItemName9,ItemCriteria9,ItemMin9,ItemMax9,ItemName10,ItemCriteria10,ItemMin10,ItemMax10)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}','{40}','{41}','{42}','{43}')"
                       , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult
                      , itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                       );
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                else
                {
                    Sql = string.Format("UPDATE PackagingTable SET PCBID='{0}',WorkStationName='{1}',DataNumber='{2}',ExecutionResult='{3}',ItemName1='{4}',ItemCriteria1='{5}',ItemMin1='{6}',ItemMax1='{7}',ItemName2='{8}',ItemCriteria2='{9}',ItemMin2='{10}',ItemMax2='{11}',ItemName3='{12}',ItemCriteria3='{13}',ItemMin3='{14}',ItemMax3='{15}',ItemName4='{16}',ItemCriteria4='{17}',ItemMin4='{18}',ItemMax4='{19}' ,ItemName5='{20}',ItemCriteria5='{21}',ItemMin5='{22}',ItemMax5='{23}',ItemName6='{24}',ItemCriteria6='{25}',ItemMin6='{26}',ItemMax6='{27}',ItemName7='{28}',ItemCriteria7='{29}',ItemMin7='{30}',ItemMax7='{31}',ItemName8='{32}',ItemCriteria8='{33}',ItemMin8='{34}',ItemMax8='{35}',ItemName9='{36}',ItemCriteria9='{37}',ItemMin9='{38}',ItemMax9='{39}',ItemName10='{40}',ItemCriteria10='{41}',ItemMin10='{42}',ItemMax10='{43}' where PCBID='{0}'"
                      , PCBID, itemparam.WorkStationName, itemparam.DataNumber, itemparam.ExecutionResult, itemparam.ItemName1, itemparam.ItemCriteria1, itemparam.ItemMin1, itemparam.ItemMax1
                       , itemparam.ItemName2, itemparam.ItemCriteria2, itemparam.ItemMin2, itemparam.ItemMax2
                        , itemparam.ItemName3, itemparam.ItemCriteria3, itemparam.ItemMin3, itemparam.ItemMax3
                         , itemparam.ItemName4, itemparam.ItemCriteria4, itemparam.ItemMin4, itemparam.ItemMax4
                          , itemparam.ItemName5, itemparam.ItemCriteria5, itemparam.ItemMin5, itemparam.ItemMax5
                           , itemparam.ItemName6, itemparam.ItemCriteria6, itemparam.ItemMin6, itemparam.ItemMax6
                            , itemparam.ItemName7, itemparam.ItemCriteria7, itemparam.ItemMin7, itemparam.ItemMax7
                             , itemparam.ItemName8, itemparam.ItemCriteria8, itemparam.ItemMin8, itemparam.ItemMax8
                              , itemparam.ItemName9, itemparam.ItemCriteria9, itemparam.ItemMin9, itemparam.ItemMax9
                               , itemparam.ItemName10, itemparam.ItemCriteria10, itemparam.ItemMin10, itemparam.ItemMax10
                        );
                    var intRes = RunSql(Sql);
                    if (intRes != 1)
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region 更新历史数据
        /// <summary>
        /// 上料工站更新工站记录
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="ExecuteResults">工站执行结果，OK或NG</param>
        /// <returns></returns>
        public static bool FeddingAddHistoryData(string QrCode, string PCBID, bool ExecuteResults=true)
        {
            try
            {
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var Sql = string.Format("SELECT * FROM HistoryTable WHERE PCBID='{0}'", PCBID);
                var Execute = ExecuteReader(Sql);
                if (Execute.HasRows)
                {
                    Sql = string.Format("UPDATE  HistoryTable Set WorkStationName1='{1}' ,ExecutionResult1='{2}',ExecutionTime1='{3}'where PCBID='{0}'",PCBID,"上料工序绑定完成", ExecuteResults, dt);
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 烧录工站更新工站记录
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="ExecuteResults">工站执行结果，OK或NG</param>
        /// <returns></returns>
        public static bool BurnAddHistoryData(string QrCode, string PCBID, bool ExecuteResults = true)
        {
            try
            {
                bool FeddingResult = false;
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var Sql = string.Format("SELECT * FROM HistoryTable WHERE PCBID='{0}'", PCBID);
                var dtquery = DtQuery(Sql);
                if (dtquery.Rows.Count == 1)
                {
                    DataRow dr= dtquery.Rows[0];
                    FeddingResult = Convert.ToBoolean(dr[7]);
                    if (FeddingResult)
                    {
                        Sql = string.Format("UPDATE  HistoryTable Set WorkStationName2='{1}' ,ExecutionResult2='{2}',ExecutionTime2='{3}'where PCBID='{0}'", PCBID, "烧录工序执行完成", ExecuteResults, dt);
                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 打码工站更新工站记录
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="ExecuteResults">工站执行结果，OK或NG</param>
        /// <returns></returns>
        public static bool HitCodeAddHistoryData(string QrCode, string PCBID, bool ExecuteResults=true)
        {
            try
            {
                bool FeddingResult = false;
                bool BurnCodeResult = false;
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var Sql = string.Format("SELECT * FROM HistoryTable WHERE PCBID='{0}'", PCBID);
                var dtquery = DtQuery(Sql);
                if (dtquery.Rows.Count == 1)
                {
                    DataRow dr = dtquery.Rows[0];
                    FeddingResult = Convert.ToBoolean(dr[7]);
                    BurnCodeResult = Convert.ToBoolean(dr[10]);
                    if (FeddingResult&&BurnCodeResult)
                    {
                        Sql = string.Format("UPDATE  HistoryTable Set WorkStationName3='{1}' ,ExecutionResult3='{2}',ExecutionTime3='{3}'where PCBID='{0}'", PCBID, "打码工序执行完成", ExecuteResults, dt);
                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 切割工站更新工站记录
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="ExecuteResults">工站执行结果，OK或NG</param>
        /// <returns></returns>
        public static bool CuttingAddHistoryData(string QrCode, string PCBID, bool ExecuteResults=true)
        {
            try
            {
                bool FeddingResult = false;
                bool BurnCodeResult = false;
                bool HitCodeResult = false;
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var Sql = string.Format("SELECT * FROM HistoryTable WHERE PCBID='{0}'", PCBID);
                var dtquery = DtQuery(Sql);
                if (dtquery.Rows.Count == 1)
                {
                    DataRow dr = dtquery.Rows[0];
                    FeddingResult = Convert.ToBoolean(dr[7]);
                    BurnCodeResult = Convert.ToBoolean(dr[10]);
                    HitCodeResult = Convert.ToBoolean(dr[13]);
                    if (FeddingResult&& BurnCodeResult&& HitCodeResult)
                    {
                        Sql = string.Format("UPDATE  HistoryTable Set WorkStationName4='{1}' ,ExecutionResult4='{2}',ExecutionTime4='{3}'where PCBID='{0}'", PCBID, "切割测试工序执行完成", ExecuteResults, dt);
                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 顶部测试工站更新工站记录
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="ExecuteResults">工站执行结果，OK或NG</param>
        /// <returns></returns>
        public static bool TopTestAddHistoryData(string QrCode, string PCBID, bool ExecuteResults=true)
        {
            try
            {
                bool FeddingResult = false;
                bool BurnCodeResult = false;
                bool HitCodeResult = false;
                bool CuttingResult = false;
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var Sql = string.Format("SELECT * FROM HistoryTable WHERE PCBID='{0}'", PCBID);
                var dtquery = DtQuery(Sql);
                if (dtquery.Rows.Count == 1)
                {
                    DataRow dr = dtquery.Rows[0];
                    FeddingResult = Convert.ToBoolean(dr[7]);
                    BurnCodeResult = Convert.ToBoolean(dr[10]);
                    HitCodeResult = Convert.ToBoolean(dr[13]);
                    CuttingResult= Convert.ToBoolean(dr[16]);
                    if (FeddingResult && BurnCodeResult && HitCodeResult&& CuttingResult)
                    {
                        Sql = string.Format("UPDATE  HistoryTable Set WorkStationName5='{1}' ,ExecutionResult5='{2}',ExecutionTime5='{3}'where PCBID='{0}'", PCBID, "顶部测试工序执行完成", ExecuteResults, dt);
                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 底部测试工站更新工站记录
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="ExecuteResults">工站执行结果，OK或NG</param>
        /// <returns></returns>
        public static bool BottomAddHistoryData(string QrCode, string PCBID, bool ExecuteResults=true)
        {
            try
            {
                bool FeddingResult = false;
                bool BurnCodeResult = false;
                bool HitCodeResult = false;
                bool CuttingResult = false;
                bool TopTestResult = false;
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var Sql = string.Format("SELECT * FROM HistoryTable WHERE PCBID='{0}'", PCBID);
                var dtquery = DtQuery(Sql);
                if (dtquery.Rows.Count == 1)
                {
                    DataRow dr = dtquery.Rows[0];
                    FeddingResult = Convert.ToBoolean(dr[7]);
                    BurnCodeResult = Convert.ToBoolean(dr[10]);
                    HitCodeResult = Convert.ToBoolean(dr[13]);
                    CuttingResult = Convert.ToBoolean(dr[16]);
                    TopTestResult= Convert.ToBoolean(dr[19]);
                    if (FeddingResult && BurnCodeResult && HitCodeResult && CuttingResult&&TopTestResult)
                    {
                        Sql = string.Format("UPDATE  HistoryTable Set WorkStationName6='{1}' ,ExecutionResult6='{2}',ExecutionTime6='{3}'where PCBID='{0}'", PCBID, "底部测试工序执行完成", ExecuteResults, dt);
                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 包装测试工站更新工站记录
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="ExecuteResults">工站执行结果，OK或NG</param>
        /// <returns></returns>
        public static bool PackagingAddHistoryData(string QrCode, string PCBID, bool ExecuteResults=true)
        {
            try
            {
                bool FeddingResult = false;
                bool BurnCodeResult = false;
                bool HitCodeResult = false;
                bool CuttingResult = false;
                bool TopTestResult = false;
                bool BottomResult = false;
                var dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var Sql = string.Format("SELECT * FROM HistoryTable WHERE PCBID='{0}'", PCBID);
                var dtquery = DtQuery(Sql);
                if (dtquery.Rows.Count == 1)
                {
                    DataRow dr = dtquery.Rows[0];
                    FeddingResult = Convert.ToBoolean(dr[7]);
                    BurnCodeResult = Convert.ToBoolean(dr[10]);
                    HitCodeResult = Convert.ToBoolean(dr[13]);
                    CuttingResult = Convert.ToBoolean(dr[16]);
                    TopTestResult = Convert.ToBoolean(dr[19]);
                    BottomResult = Convert.ToBoolean(dr[22]);
                    if (FeddingResult && BurnCodeResult && HitCodeResult && CuttingResult && TopTestResult&&BottomResult)
                    {
                        Sql = string.Format("UPDATE  HistoryTable Set WorkStationName7='{1}' ,ExecutionResult7='{2}',ExecutionTime7='{3}'where PCBID='{0}'", PCBID, "包装工序执行完成", ExecuteResults, dt);
                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        } 

        #endregion

        /// <summary>
        /// 执行查询语句，返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>SqlDataReader</returns>
        public static  SqlDataReader ExecuteReader(string strSQL)
        {
            SqlConnection connection = new SqlConnection(connSql);
            SqlCommand cmd = new SqlCommand(strSQL, connection);
            try
            {
                connection.Open();
                SqlDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return myReader;
            }
            catch (SqlException e)
            {
                throw;
            }
        }

        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString)
        {
            using (SqlConnection connection = new SqlConnection(connSql))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter command = new SqlDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (SqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }

        public  static void Open()
        {
            try
            {
                if (connn == null)
                {
                    conn = new SqlConnection(connSql);
                }
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State.ToString() != "Open")
                {
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// 要执行SQL语句，该方法返回一个DataTable
        /// <param name="sql">执行SQL语句</param>
        public static DataTable DtQuery(string sql)//外部 统计调用了
        {
            Open();
            using (SqlDataAdapter sqlda = new SqlDataAdapter(sql, conn))
            {
                using (DataTable dt = new DataTable())
                {
                    sqlda.Fill(dt);
                    return dt;
                }
            }
        }

        /// 执行SQL语句，返回影响的记录行数
        /// <param name="sql">要执行的SQL语句</param>
        public static int RunSql(string sql)//外部调用
        {
            int result = -1;
            try
            {
                Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    result = cmd.ExecuteNonQuery();
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
