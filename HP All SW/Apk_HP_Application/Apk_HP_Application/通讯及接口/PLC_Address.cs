﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslCommunication.Profinet.Omron;
using HslCommunication;
using System.Threading;
using System.Windows.Forms;

namespace CommunicationZH
{
    class PLCDataProcess : object
    {
        public static int PLC_WordTransferToInt(byte PLC_FirstByte, byte PLC_SencByte)
        {
            return (((Int16)PLC_FirstByte) << 8 | PLC_SencByte);
        }

        //         public static Int16 PLC_WordTransferToInt16(byte PLC_FirstByte, byte PLC_SencByte)
        //         {
        //             return (((Int16)PLC_FirstByte) << 8 | (Int16)PLC_SencByte);
        //         }

        public static UInt16 PLC_WordTransferUInt16(byte PLC_FirstByte, byte PLC_SencByte)
        {
            return (UInt16)(((UInt16)PLC_FirstByte) << 8 | PLC_SencByte);
        }

        public static Int32 PLC_WordTransferToLong(byte PLC_FirstByte, byte PLC_SencByte, byte PLC_ThrtByte, byte PLC_FourByte)
        {
            return (((Int32)PLC_ThrtByte) << 24 | ((Int32)PLC_FourByte) << 16 | ((Int32)PLC_FirstByte) << 8 | PLC_SencByte);
        }

        public static UInt32 PLC_WordTransferToDWORD(byte PLC_FirstByte, byte PLC_SencByte, byte PLC_ThrtByte, byte PLC_FourByte)
        {
            return (((UInt32)PLC_ThrtByte) << 24 | ((UInt32)PLC_FourByte) << 16 | ((UInt32)PLC_FirstByte) << 8 | PLC_SencByte);
        }

        public static double PLC_WordTransferToUInt32(byte PLC_FirstByte, byte PLC_SencByte, byte PLC_ThrtByte, byte PLC_FourByte)
        {
            return (double)((((UInt32)PLC_ThrtByte) << 24 | ((UInt32)PLC_FourByte) << 16 | ((UInt32)PLC_FirstByte) << 8 | PLC_SencByte));
        }

        int PLC_WordTransferToshort(byte PLC_Hight, byte PLC_Low)
        {
            return ((int)PLC_Hight << 8 | PLC_Low);
        }

        public class Std_IOMonitor
        {
            private ushort m_addr;

            public bool _1号上料位有无料;
            public bool _2号上料位有无料;
            public bool _3号上料位有无料;
            public bool _配对台1号有无料;
            public bool _配对台2号有无料;
            public bool _配对台3号有无料;
            public bool _配对台4号有无料;
            public bool _NG满料;
            public bool _1号夹手有无料;
            public bool _2号夹手有无料;
            public bool _3号夹手有无料;
            public bool _4号夹手有无料;
            public bool _1号提升台有无料;
            public bool _1号提升台位置上;
            public bool _1号提升台托盘上下料位;
            public bool _1号提升台托盘航吊取放料位;
            public bool _2号提升台有无料;
            public bool _2号提升台位置上;
            public bool _2号提升台托盘上下料位;
            public bool _2号提升台托盘航吊取放料位;
            public bool _工位自动手动标志;
            public Int64 _报警异常;

            enum emIO_Station
            {
                emIO_Station_NoDefine = 0,
                emIO_StationA_L,
                emIO_StationA_UL,
                emIO_StationB_L,
                emIO_StationB_UL,
            }
        }
    }
}
