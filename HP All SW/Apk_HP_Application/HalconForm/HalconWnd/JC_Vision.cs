﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using HalconDotNet;
using System.Windows;
using System.Windows.Forms;

namespace HalconFormDonet
{
    public class JC_Vision
    {
        //=======================Vision Comm=======================
        //相机品牌
        enum CameraBrand
        {
            CAMERA_BRAND_NO_DEFINE = -1,
            CAMERA_BRAND_BASLER = 0,
            CAMERA_BRAND_AVT = 1,
            CAMERA_BRAND_BAUMER = 2,
            CAMERA_BRAND_AVTGigE = 3,
        }

//相机型号 
        enum CameraType
        {
            CAMERA_TYPE_NO_DEFINE = -1,
            CAMERA_TYPE_Gige = 0,
            CAMERA_TYPE_Usb = 1,
        }

//Trigger Mode 
        enum TriggerMode
        {
            TRIGGER_MODEL_NOT_DEFINE = -1,
            TRIGGER_MODEL_OFF = 0,
            TRIGGER_MODEL_ON = 1,
        }

//TringSource
        enum TriggerSource
        {
            TRIGGER_SOURCE_NOT_DEFINE = -1,
            TRIGGER_SOURCE_SOFTWARE = 0,
            TRIGGER_SOURCE_EXTERNER_LINE1 = 1,
            TRIGGER_SOURCE_EXTERNER_LINE2 = 2,
            TRIGGER_SOURCE_FREERUN = 3,
        }

        enum LineInput
        {
            LineInput_Line1,
            LineInput_Line2,
            LineInput_Line3,
            LineInput_Line4,

        }

        enum LineOutput
        {
            LineOutput_Out1,
            LineOutput_Out2,
            LineOutput_Out3,
            LineOutput_Out4,
        }

        public const int  CAMERA_TIEMOUT = 5;

        //         struct tg_Multi_Trigger_Image
        //         {
        //             void* pCamera;
        //             TriggerMode emTriggerMode;
        //             TriggerSource emTriggerSource;
        //             HBITMAP* parryBitmap;
        //             int nCountOfImage;
        //             DWORD nTimeout;
        //         }
        // 
        // typedef int ( CALLBACK* CAPTURE_FRAME_PROC)();

//         public delegate int CAPTURE_FRAME_PROC(const unsigned char* pImageBuffer, Int32 nWidth, Int32 nHeight, Int32 nChannel);
//         struct CameraTrigger
//         {
//             public static extern Int16 bTriggerMode;
//             int nTriggerSource;
//         }

        enum CameraColorFormat
        {
            ColorFormatNotDefine = -1,
            ColorFormatMono8 = 0x01,
            ColorFormatRGB8 = 0x02,
            ColorFormatBGR8 = 0x03,
            ColorFormatBayerGR8 = 0x04,
            ColorFormatBayerRG8 = 0x05,
            ColorFormatBayerGB8 = 0x06,
            ColorFormatBayerBG8 = 0x07,
        }

        //         enum { NUM_COLORS = 3, };
        //         enum { BIT_DEPTH = 8, };
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         public static extern int HBITMAP_To_RGB(IntPtr hBitmapImage, ref IntPtr ppImageBuffRed, ref IntPtr ppImageBuffGreen, ref IntPtr ppImageBuffBlue, ref Int32 pdwWidth, ref Int32 pdwHeight);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         public static extern Int16  ColorPointToRGB(BYTE* pImageBuff, ref IntPtr ppImageBuffRed, ref IntPtr ppImageBuffGreen, ref IntPtr ppImageBuffBlue, long dwWidth, long dwHeight);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         public static extern Int16  GrayPointToRGB(BYTE* pImageBuff, ref IntPtr ppImageBuffRed, ref IntPtr ppImageBuffGreen, ref IntPtr ppImageBuffBlue, long dwWidth, long dwHeight);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         public static extern Int16  ColorPointToBGR(BYTE* pImageBuff, ref IntPtr ppImageBuffRed, ref IntPtr ppImageBuffGreen, ref IntPtr ppImageBuffBlue, long dwWidth, long dwHeight);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         void  FreePoint(ref IntPtr p);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         HBITMAP  RGBToHBITMAP(BYTE* pPointerRed, BYTE* pPointerGreen, BYTE* pPointerBlue, long dwWidth, long dwHeight, UINT nChannel);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         HBITMAP  GrayPointToHBITMAP(BYTE* pPointerGray, long dwWidth, long dwHeight);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         HBITMAP  ImagePointToHBITMAP(BYTE* pImageBuff, long dwWidth, long dwHeight, CameraColorFormat emColorFormat);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         void  FreeHBITMAP(HBITMAP hBitmap);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         public static extern Int16  BGRImageBuffToCImage(BYTE* pImageBuffer, UINT nImageWidth, UINT nImageHeight, CImage& OutImage );
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         public static extern Int16  RGBToCImage(BYTE* pPointerRed, BYTE* pPointerGreen, BYTE* pPointerBlue, long dwWidth, long dwHeight, UINT nChannel, CImage& ObjImage);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         BYTE* MonoToBGR(BYTE* pInBuffer, BYTE** ppOutBuffer, ULONG nInSize);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         void  RGBToBGR(BYTE* pRGBBuffer, BYTE* pBGRBuffer, ULONG nInSize);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         HBITMAP  CopyBitmap(HBITMAP hSourceHbitmap);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         HBITMAP  LoadBitmap_Ex(LPCTSTR lpszFile);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         void  SetBitmap_Ex(HBITMAP hBitmap, HWND hWnd, int nDisplayModel);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         public static extern Int16  HBITMAPToCImage(HBITMAP hBitmap, CImage& ObjeImage);
// 
//         [DllImport("JC_Generial.dll", EntryPoint = "HBITMAP_To_RGB", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
//         HGLOBAL  CopyHandle(HGLOBAL h);
    }

    public class JcHalconDialog
    {
        // 定义一个线段的结构体
        public struct LINE_DES
        {
            public double fRowStart1;
            public double fColStart1;
            public double fRowEnd1;
            public double fColEnd1;
            public double fLength;
            public double fCenternRow;
            public double fCenternCol;
            public double fAngle;
        }

        // 定义RIO结构体
        public struct RIO_DES
        {
            public double fRow;
            public double fCol;
            public double fPhi;
            public double fLen1;
            public double fLen2;
            string szItemRemark;

            public double fRadix;
            public double fRowStart;
            public double fColStart;
            public double fRowEnd;
            public double fColEnd;

            public void InitRIO() { fRow = -1.00; fCol = -1.00; fPhi = -1.00; fLen1 = -1.00; fLen2 = -1.00; szItemRemark = ""; fRadix = -1.00; fRowStart = -1.00; fColStart = -1.00; fRowEnd = -1.00; fColEnd = -1.00; }

        }

        public struct CIRCLE_DES
        {
            public HObject CircleImage;
            public Int32 fArea;
            public double fRow;
            public double fCol;
            public double fRadius;
        }

        public struct Rectangle_DES
        {
            public HObject RectangleImage;
            public Int32 fArea;
            public double fRow;
            public double fCol;
            public double fPhi;
            public double fLen1;
            public double fLen2;
        }

        const int ShapeModeType_ShapeBase = 0;				//基础外形匹配 (不可缩放比例）
        const int ShapeModeType_ShapeBase_XLD = 1;				//基础外形XLD匹配 (不可缩放比例）
        const int ShapeModeType_Scaled = 2;				//基础外形匹配 (可缩放比例）
        const int ShapeModeType_Scaled_XLD = 3;				//基础外形XLD匹配 (可缩放比例）
        const int ShapeModeType_AnisoScaled = 4;			//基础外形匹配 (X, Y 方向可缩放比例）
        const int ShapeModeType_AnisoScaled_XLD = 5;				//基础外形XLD匹配 (X, Y 方向可缩放比例）

        const int RIO_TYPE_RECTANGLE1 = 0;
        const int RIO_TYPE_RECTANGLE2 = 1;

        //寻找模板输出的相应的信息,包括所含模板的个数, 每个对像所对应的坐标, 角度及匹配度
        public struct MATCHINGRESULT_DES
        {
            public HTuple TupleRow;
            public HTuple TupleCol;
            public HTuple TupleAngle;
            public HTuple TupleScore;
            public HTuple TupleModel;
            public HTuple TupleRScaled;
            public HTuple TupleCScaled;
        }

        //定义模板匹配输入输出参数据结构
        public struct SHAPE_MODE_DES
        {
            public string szDescription;

            // 输入参数
            public HObject objImage;

            public RIO_DES strRIO1;  //一个图像包含两个RIO的联合
            public RIO_DES strRIO2;

            public double fThresholdMin;
            public double fThresholdMax;

            public int MatchingType;

            public Int32 dwNumLevels;
            public double fAngleStart;  // 模板匹配寻找的初始角度, 最大角度及角度间隔
            public double fAngleExtent;
            public double fAngleStep;
            public double fMinScore;
            public string szOptimization;
            public string szMetric;
            public string szContrast;
            public string szMinContrast;
            public Int32 dwNumMatches;
            public double fMaxOverlap;
            public string szSubPixel;
            public double fGreediness;

            // Scaled config
            public double fScaledMin;
            public double fScaledMax;

            // 输出参数, 输出模板对像的原始信息,包括对象句柄, 对像坐标, 角度及匹配度
            public CIRCLE_DES MarkCircleDes;
            public double fRadiuxMinForFind;
            public double fRadiuxMaxForFind;
            public HTuple TupleModelID;
            public HTuple TupleModelID_1;
            public HTuple TupleModelID_1_Min;
            public HTuple TupleModelID_1_Max;
            public HTuple TupleModelID_2;
            public HTuple TupleModelID_2_Min;
            public HTuple TupleModelID_2_Max;
            public HTuple TupleModelID_3;
            public HTuple TupleModelID_3_Min;
            public HTuple TupleModelID_3_Max;
            public HTuple TupleModelID_4;
            public HTuple TupleModelID_4_Min;
            public HTuple TupleModelID_4_Max;
            public MATCHINGRESULT_DES MatchingResultOrg;

            public void InitShapemode() { MatchingType = ShapeModeType_ShapeBase; }
            public void InitShapemode_Scaled() { MatchingType = ShapeModeType_Scaled; fScaledMin = 0.9; fScaledMax = 1.1; }

        }

        //Create and destroy dialog
        [DllImport("HalconDlg.dll", EntryPoint = "CreateHalconDialog", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr _CreateHalconDialog(bool bShow = false);

        public static Form CreateHalconDialog(bool bShow = false)
        {
            System.Windows.Forms.
            Form dialog = new Form();
            return dialog;
        }

        [DllImport("HalconDlg.dll", EntryPoint = "CloseHalconDialog", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void _CloseHalconDialog(IntPtr hDialog);

        [DllImport("HalconDlg.dll", EntryPoint = "SetParentWindow", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void _SetParentWindow(IntPtr hDlg, IntPtr pWnd);

        [DllImport("HalconDlg.dll", EntryPoint = "GetWindow", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr _GetWindow(IntPtr hDlg);

        [DllImport("HalconDlg.dll", EntryPoint = "GetParentWindow", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr _GetParentWindow(IntPtr hDlg);

        //Operation functions
        [DllImport("HalconDlg.dll", EntryPoint = "SetNewSize", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _SetNewSize(IntPtr hDlg, int nNewWidth, int nNewHeight, bool bReference = true);

        [DllImport("HalconDlg.dll", EntryPoint = "LoadImage", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _LoadImage(IntPtr hDlg, string szImage, int nNewWidth = -1, int nNewHeight = -1, int nDisplayModel = 0);

        [DllImport("HalconDlg.dll", EntryPoint = "LoadImageW", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _LoadImageW(IntPtr hDlg, string szImage, int nNewWidth = -1, int nNewHeight = -1, int nDisplayModel = 0);

        [DllImport("HalconDlg.dll", EntryPoint = "ShowImage", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _ShowImage(IntPtr hDlg, HObject HalconImage, int nNewWidth = -1, int nNewHeight = -1, int nDisplayModel = 0);

        [DllImport("HalconDlg.dll", EntryPoint = "ShowRIO", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _ShowRIO(IntPtr hDlg, RIO_DES Rio, int nRIOType = RIO_TYPE_RECTANGLE2, bool bClear = true);

        [DllImport("HalconDlg.dll", EntryPoint = "GetRio_Des", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern RIO_DES _GetRio_Des(IntPtr hDlg, int nIndexRIO);

        [DllImport("HalconDlg.dll", EntryPoint = "IsContainImage", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _IsContainImage(IntPtr hDlg);

        [DllImport("HalconDlg.dll", EntryPoint = "GetImage", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern HObject _GetImage(IntPtr hDlg);

        [DllImport("HalconDlg.dll", EntryPoint = "GetRIOImage", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern HObject _GetRIOImage(IntPtr hDlg, int nIndexRIO, bool bKeepRotate = true);

        [DllImport("HalconDlg.dll", EntryPoint = "CopyHalconImage", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _CopyHalconImage(IntPtr hDlg, ref HObject objImage);

        [DllImport("HalconDlg.dll", EntryPoint = "SaveImageToFile", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int16 _SaveImageToFile(IntPtr hDlg, ref string szFileName);

        [DllImport("HalconDlg.dll", EntryPoint = "ConvertToBitmap", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr _ConvertToBitmap(IntPtr hDlg);

        [DllImport("HalconDlg.dll", EntryPoint = "GetHalconWindow", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern HTuple _GetHalconWindow(IntPtr hDlg);
    }
}
