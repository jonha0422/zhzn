﻿namespace RabbitMQLib
{
    partial class RabbitMQTestFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtReceiveData = new System.Windows.Forms.TextBox();
            this.ReceiveType = new System.Windows.Forms.TextBox();
            this.cbReceiveFrom = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtMsgData = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMsgType = new System.Windows.Forms.TextBox();
            this.cbTargetStation = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTargetStation = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bExit = new System.Windows.Forms.Button();
            this.bSent = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtReceiveData
            // 
            this.txtReceiveData.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtReceiveData.Location = new System.Drawing.Point(19, 172);
            this.txtReceiveData.Multiline = true;
            this.txtReceiveData.Name = "txtReceiveData";
            this.txtReceiveData.Size = new System.Drawing.Size(502, 340);
            this.txtReceiveData.TabIndex = 4;
            // 
            // ReceiveType
            // 
            this.ReceiveType.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ReceiveType.Location = new System.Drawing.Point(106, 97);
            this.ReceiveType.Name = "ReceiveType";
            this.ReceiveType.Size = new System.Drawing.Size(265, 29);
            this.ReceiveType.TabIndex = 2;
            // 
            // cbReceiveFrom
            // 
            this.cbReceiveFrom.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbReceiveFrom.FormattingEnabled = true;
            this.cbReceiveFrom.Items.AddRange(new object[] {
            "MasterControl",
            "ALL",
            "AP01",
            "AP02.1",
            "AP02.2",
            "AP02.3",
            "AP03",
            "AP04",
            "AP05"});
            this.cbReceiveFrom.Location = new System.Drawing.Point(106, 20);
            this.cbReceiveFrom.Name = "cbReceiveFrom";
            this.cbReceiveFrom.Size = new System.Drawing.Size(264, 27);
            this.cbReceiveFrom.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(117, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(422, 31);
            this.label4.TabIndex = 5;
            this.label4.Text = "MasterContro(总控) ALL(所有工位) AP01（上料机）  AP.02.1（烧录1） AP.02.2 AP.02.3  AP03(转盘) AP04" +
    "(补料) AP05（辅线）";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(15, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 19);
            this.label5.TabIndex = 3;
            this.label5.Text = "消息内容";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(15, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 19);
            this.label6.TabIndex = 0;
            this.label6.Text = "发送工位";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(15, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 19);
            this.label7.TabIndex = 0;
            this.label7.Text = "消息类型";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtReceiveData);
            this.groupBox2.Controls.Add(this.ReceiveType);
            this.groupBox2.Controls.Add(this.cbReceiveFrom);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(0, 35);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(560, 521);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "收到消息";
            // 
            // txtMsgData
            // 
            this.txtMsgData.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtMsgData.Location = new System.Drawing.Point(28, 172);
            this.txtMsgData.Multiline = true;
            this.txtMsgData.Name = "txtMsgData";
            this.txtMsgData.Size = new System.Drawing.Size(556, 340);
            this.txtMsgData.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMsgData);
            this.groupBox1.Controls.Add(this.txtMsgType);
            this.groupBox1.Controls.Add(this.cbTargetStation);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbTargetStation);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(566, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(602, 521);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "发送消息";
            // 
            // txtMsgType
            // 
            this.txtMsgType.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtMsgType.Location = new System.Drawing.Point(107, 87);
            this.txtMsgType.Name = "txtMsgType";
            this.txtMsgType.Size = new System.Drawing.Size(265, 29);
            this.txtMsgType.TabIndex = 2;
            // 
            // cbTargetStation
            // 
            this.cbTargetStation.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbTargetStation.FormattingEnabled = true;
            this.cbTargetStation.Items.AddRange(new object[] {
            "MasterControl",
            "ALL",
            "AP01",
            "AP02.1",
            "AP02.2",
            "AP02.3",
            "AP03",
            "AP04",
            "AP05"});
            this.cbTargetStation.Location = new System.Drawing.Point(106, 20);
            this.cbTargetStation.Name = "cbTargetStation";
            this.cbTargetStation.Size = new System.Drawing.Size(264, 27);
            this.cbTargetStation.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(117, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(493, 31);
            this.label3.TabIndex = 5;
            this.label3.Text = "MasterControl(总控) ALL(所有工位) AP01（上料机）  AP.02.1（烧录1） AP.02.2 AP.02.3  AP03(转盘) AP0" +
    "4(补料) AP05（辅线）";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(24, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "消息内容";
            // 
            // lbTargetStation
            // 
            this.lbTargetStation.AutoSize = true;
            this.lbTargetStation.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbTargetStation.Location = new System.Drawing.Point(15, 28);
            this.lbTargetStation.Name = "lbTargetStation";
            this.lbTargetStation.Size = new System.Drawing.Size(85, 19);
            this.lbTargetStation.TabIndex = 0;
            this.lbTargetStation.Text = "目标工位";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(15, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "消息类型";
            // 
            // bExit
            // 
            this.bExit.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bExit.Location = new System.Drawing.Point(1002, 580);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(139, 62);
            this.bExit.TabIndex = 9;
            this.bExit.Text = "退出";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // bSent
            // 
            this.bSent.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bSent.Location = new System.Drawing.Point(797, 580);
            this.bSent.Name = "bSent";
            this.bSent.Size = new System.Drawing.Size(139, 62);
            this.bSent.TabIndex = 10;
            this.bSent.Text = "发送";
            this.bSent.UseVisualStyleBackColor = true;
            this.bSent.Click += new System.EventHandler(this.bSent_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // RabbitMQTestFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(222)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(1195, 676);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.bSent);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RabbitMQTestFrm";
            this.Text = "RabbitMQTestFrm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RabbitMQTestFrm_FormClosing);
            this.Load += new System.EventHandler(this.RabbitMQTestFrm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtReceiveData;
        private System.Windows.Forms.TextBox ReceiveType;
        private System.Windows.Forms.ComboBox cbReceiveFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtMsgData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMsgType;
        private System.Windows.Forms.ComboBox cbTargetStation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbTargetStation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.Button bSent;
        private System.Windows.Forms.Timer timer1;
    }
}