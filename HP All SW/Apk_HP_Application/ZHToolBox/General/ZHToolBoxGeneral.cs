﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace ZHToolBox.General
{
    public class ZHToolBoxGeneral
    {
        public static string SerializeObjToString<T>(T obj)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                StringWriter sw = new StringWriter();
                xs.Serialize(sw, obj);
                string strxml = sw.ToString();
                return strxml;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void SerializeObjToFile<T>(T obj, string strPathName)
        {
            try
            {
                string strPath = System.IO.Path.GetDirectoryName(strPathName);
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);


                XmlSerializer xs = new XmlSerializer(typeof(T));//typeof(ProductSettings)
                StringWriter sw = new StringWriter();
                xs.Serialize(sw, obj);
                string strxml = sw.ToString();


                FileStream fs = new FileStream(strPathName, FileMode.Create, FileAccess.Write);
                StreamWriter stream = new StreamWriter(fs, Encoding.UTF8);
                stream.WriteLine(strxml);
                stream.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object DeserializeObjFromFile<T>(string strPathName)
        {
            try
            {
                string strPath = System.IO.Path.GetDirectoryName(strPathName);
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);


                StreamReader sr = new StreamReader(strPathName, Encoding.UTF8);
                string strTemp;
                string strXml = "";
                while ((strTemp = sr.ReadLine()) != null)
                {
                    strXml += strTemp;
                }
                sr.Close();


                XmlSerializer xs = new XmlSerializer(typeof(T));
                StringReader strReader = new StringReader(strXml);
                object obj = xs.Deserialize(strReader);
                sr.Close();
                return obj;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
    }
}
