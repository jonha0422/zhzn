﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO.Ports;

namespace CommunicationZH
{
    public class FlashResult
    {
        public bool bResult { get; set; }
        public byte byteError { get; set; }

        public FlashResult()
        {
            bResult = false;
            byteError = 0xFF;
        }
    }

    public class APK_FlashVHP1
    {
        //private static APK_FlashVHP1 _instance = null;
        //public static APK_FlashVHP1 Instance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //        {
        //            _instance = new APK_FlashVHP1();
        //        }
        //        return _instance;
        //    }
        //}

        public enum emFlashData_Mode
        {
            emFlashData_Mode_Normal = 0,
            emFlashData_Mode_RowToCol
        }

        public bool isServerConnected = false;
        private TcpClient socketCommunication = null;
        private SerialPort serialCommunication = null;
        public byte nRow = 10;
        public byte nCol = 10;

        public string CurrentDataTime = "";
        public string CurrentVersion = "";

        public emFlashData_Mode emDataMode = emFlashData_Mode.emFlashData_Mode_Normal;

        #region>>>>>>>>>> CRC table
        private UInt32[] V2_Crc32Table = new UInt32[]{
            0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b, 0x1a864db2, 0x1e475005,
            0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61, 0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd,
            0x4c11db70, 0x48d0c6c7, 0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
            0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3, 0x709f7b7a, 0x745e66cd,
            0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039, 0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5,
            0xbe2b5b58, 0xbaea46ef, 0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
            0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb, 0xceb42022, 0xca753d95,
            0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1, 0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d,
            0x34867077, 0x30476dc0, 0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
            0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4, 0x0808d07d, 0x0cc9cdca,
            0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde, 0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02,
            0x5e9f46bf, 0x5a5e5b08, 0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
            0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc, 0xb6238b25, 0xb2e29692,
            0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6, 0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a,
            0xe0b41de7, 0xe4750050, 0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
            0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34, 0xdc3abded, 0xd8fba05a,
            0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637, 0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb,
            0x4f040d56, 0x4bc510e1, 0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
            0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5, 0x3f9b762c, 0x3b5a6b9b,
            0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff, 0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623,
            0xf12f560e, 0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
            0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd, 0xcda1f604, 0xc960ebb3,
            0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7, 0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b,
            0x9b3660c6, 0x9ff77d71, 0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
            0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b, 0x43cdc09c,
            0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8, 0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24,
            0x119b4be9, 0x155a565e, 0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
            0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a, 0x2d15ebe3, 0x29d4f654,
            0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0, 0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c,
            0xe3a1cbc1, 0xe760d676, 0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
            0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662, 0x933eb0bb, 0x97ffad0c,
            0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668, 0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
        };
        #endregion

        public APK_FlashVHP1(byte row = 10, byte col = 10)
        {
            nRow = row;
            nCol = col;
        }

        public void SetRowColNumber(byte row, byte col)
        {
            nRow = row;
            nCol = col;
        }
        /// <summary>
        /// 绑定通讯
        /// </summary>
        /// <param name="obj"></param>
        /// 
        public int GetIndexOf(byte[] src, byte[] dst)
        {
            if (src == null || dst == null || src.Length == 0 || dst.Length == 0)
                return -1;

            int i, j;
            for (i = 0; i < src.Length; i++)
            {
                if (src[i] == dst[0])
                {
                    for (j = 1; j < dst.Length; j++)
                    {
                        if (src[i + j] != dst[j])
                            break;
                    }
                    if (j == dst.Length)
                        return i;
                }
            }
            return -1;
        }
        public void BindCommunicationObj(TcpClient obj)
        {
            serialCommunication = null;
            socketCommunication = obj;
        }

        public void BindCommunicationObj(SerialPort obj)
        {
            serialCommunication = obj;
            socketCommunication = null;
        }

        public bool ConnectSocket(string ip, int port)
        {
            try
            {
                isServerConnected = false;
                if (serialCommunication != null)
                    serialCommunication.Close();

                if (socketCommunication == null)
                    socketCommunication = new TcpClient();

                socketCommunication.Connect(System.Net.IPAddress.Parse(ip), port);
                if (socketCommunication.Connected)
                {
                    isServerConnected = true;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ConnectRS232(string comport, int baudrate = 115200)
        {
            try
            {
                isServerConnected = false;
                if (socketCommunication != null)
                    socketCommunication.Close();

                if (serialCommunication == null)
                    serialCommunication = new SerialPort();
                else if(serialCommunication.IsOpen)
                    serialCommunication.Close();

                serialCommunication.ReadTimeout = 5000;
                serialCommunication.WriteTimeout = 5000;
                serialCommunication.PortName = comport;
                serialCommunication.BaudRate = baudrate;
                serialCommunication.Parity = Parity.None;
                serialCommunication.StopBits = StopBits.One;
                serialCommunication.DataBits = 8;
                serialCommunication.Open();
                isServerConnected = true;

                return true;
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        private bool Write(Byte[] bytes)
        {
            try
            {
                if (socketCommunication != null)
                    return socketCommunication.Client.Send(bytes) > 0 ? true : false;
                else if (serialCommunication != null)
                {
                    serialCommunication.Write(bytes, 0, bytes.Length);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form Write: \r\n{ex.Message}");
            }
        }

        private byte[] ReadLine(int timeout = 240000, string endLine = "\r\n\r\n")
        {
            DateTime time1, time2;
            byte[] endBytes = System.Text.Encoding.ASCII.GetBytes(endLine);
            try
            {
                #region>>>>>>TCP/IP
                if (socketCommunication != null)
                {
                    time1 = time2 = DateTime.Now;
                    while (true)
                    {
                        Thread.Sleep(20);
                        time2 = DateTime.Now;
                        if ((time2 - time1).TotalMilliseconds > timeout)
                            throw new Exception("ReadLine timeout!, 等待返回结果超时");

                        int nLen = 0;
                        if ((nLen = socketCommunication.Client.Available) <= endLine.Length)
                            continue;

                        byte[] bytesBuff = new byte[nLen];
                        socketCommunication.Client.Receive(bytesBuff, nLen, SocketFlags.Peek);
                        if (bytesBuff != null && bytesBuff.Length >= endBytes.Length)
                        {
                            int nIndex = -1;
                            if ((nIndex = GetIndexOf(bytesBuff, endBytes)) >= 0)
                            {
                                bytesBuff = new byte[nIndex + 4];
                                socketCommunication.Client.Receive(bytesBuff, nIndex + 4, SocketFlags.None);
                                return bytesBuff;
                            }
                        }
                    }
                }
                #endregion

                #region>>>>>>TCP/IP
                else if (serialCommunication != null)
                {
                    byte[] reads = null;
                    time1 = time2 = DateTime.Now;
                    while (true)
                    {
                        Thread.Sleep(20);
                        time2 = DateTime.Now;
                        if ((time2 - time1).TotalMilliseconds > timeout)
                            throw new Exception("ReadLine timeout!, 等待返回结果超时");

                        int nLen = serialCommunication.BytesToRead;
                        if (nLen <= 0)
                            continue;

                        if(reads == null)
                        {
                            reads = new byte[nLen];
                            serialCommunication.Read(reads, 0, nLen);
                        }
                        else
                        {
                            byte[] buffer = new byte[nLen];
                            reads = reads.Concat(buffer).ToArray();
                        }

                       if(reads != null && reads.Length >= endBytes.Length)
                        {
                            int nIndex = -1;
                            if((nIndex = GetIndexOf(reads, endBytes)) >= 0)
                                return reads;
                        }
                    }
                }
                #endregion

                else
                    return null;

            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form ReadLine: \r\n{ex.Message}");
            }
        }

        private bool Read(ref byte[] bytes, int nLen)
        {
            try
            {
                if (socketCommunication != null)
                    return socketCommunication.Client.Receive(bytes, nLen, SocketFlags.None) > 0 ? true : false;
                else if (serialCommunication != null)
                {
                    serialCommunication.Read(bytes, 0, nLen);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form Read: \r\n{ex.Message}");
            }
        }

        public byte[] FlashMachineCRC32(byte[] pData, ref UInt32 CRCResult, bool add0x0D0A = true)
        {
            try
            {
                int length_cnt, byte_cnt;
                CRCResult = 0x00;

                CRCResult = 0xFFFFFFFF;
                int Length = pData.Length;
                for (length_cnt = 0; length_cnt < Length; length_cnt++)
                {
                    for (byte_cnt = 0; byte_cnt < 3; byte_cnt++)
                    {
                        CRCResult = (CRCResult << 8) ^ V2_Crc32Table[((CRCResult >> 24) & 0xFF) ^ 0x00];
                    }
                    CRCResult = (CRCResult << 8) ^ V2_Crc32Table[((CRCResult >> 24) & 0xFF) ^ pData.ElementAt(length_cnt)];
                }

                byte[] end0D0A = new byte[4] { 0x0D, 0x0A, 0x0D, 0x0A };
                byte[] CRCBytes = new byte[4];
                CRCBytes[0] = (byte)((CRCResult & 0xFF000000) >> 24);
                CRCBytes[1] = (byte)((CRCResult & 0x00FF0000) >> 16);
                CRCBytes[2] = (byte)((CRCResult & 0x0000FF00) >> 8);
                CRCBytes[3] = (byte)(CRCResult & 0x000000FF);

                byte[] newData = pData.Concat(CRCBytes).ToArray();

                if (add0x0D0A)
                    newData = newData.Concat(end0D0A).ToArray();

                return newData;
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form FlashMachineCRC32: \r\n{ex.Message}");
            }

        }

        //==================================================================================
        // 函数名:
        // 描叙:
        // 形参说明：
        //0x99 0x66为报头
        //0x01为指令
        //0x05为要发送的数据长度，“WRITE”的字节个数为5
        //0x57 0x52 0x49 0x54 0x45为“WRITE”的ASSIC码
        //CRC ：数据为0x99 0x66 0x01 0x05 0x57 0x52 0x49 0x54 0x45 ，前9个字节数据的32位CRC校验字节
        //填充：四字节填充数据，0x0D,0x0A,0x0D,0x0A，用于增加通讯稳定性
        //0x66 	0x99	0x01	0x05	0x57	0x52	0x49 	0x54	0x45	0xXX	0xXX	0xXX	0xXX	0x0D	0x0A	0x0D	0x0A
        //==================================================================================
        public bool Flash_Start(byte[] type, bool bSetType = true, bool bVerify = true, int timeOut = 360000)
        {
            byte[] szCommand = new byte[] { 0x66, 0x99, 0x01, 0x05, 0x57, 0x52, 0x49, 0x54, 0x45 };
            byte[] szCompareStr = new byte[] { 0x99, 0x66, 0x01, 0x05, 0x57, 0x52, 0x49, 0x54, 0x45 };
            byte[] szResponed = new byte[1024];

            byte[] szMessageOuput = new byte[1024];
            byte[] szReadOutRS232 = new byte[5000];

            try
            {
                string dateTime = "";
                string version = "";
                bool compare = false;

                if (bSetType && !Flash_SetType(type, ref dateTime, ref version, ref compare))
                    return false;

                Thread.Sleep(1000);

                UInt32 CRCResult = 0x00000000;
                bool add0x0D0A = true;
                byte[] bytesToWrite = FlashMachineCRC32(szCommand, ref CRCResult, add0x0D0A);

                Write(bytesToWrite);
                Thread.Sleep(2000);

                byte[] bytesRead = ReadLine();
                if (bytesRead == null)
                    return false;

                if (GetIndexOf(bytesRead, szCompareStr) < 0)
                    return false;

                if (!bVerify)
                    return true;

                return Flash_Verify(timeOut);

            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form Flash_Start: \r\n{ex.Message}");
            }
        }

        public FlashResult[] Flash_Start(string type, int timeOut = 360000)
        {
            try
            {
                if (!this.Flash_Start(System.Text.Encoding.ASCII.GetBytes(type), true, false, timeOut))
                    return null;

                string strReturn = "";
                return Flash_GetResult_OutList(ref strReturn, timeOut);
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form FlashResult[] Flash_Start: \r\n{ex.Message}");
            }
        }

        //==================================================================================
        // 函数名:验证
        // 描叙:
        // 形参说明：
        // 0x66 0x99为报头
        // 0x02为指令
        // 0x06为要发送的数据长度，“VERIFY”的字节个数为6
        // 0x56 0x45 0x52 0x49 0x46 0x59为“VERIFY”的ASSIC码
        // CRC：数据为0x66 0x99 0x02 0x06 0x56 0x45 0x52 0x49 0x46 0x59 ，前10个字节数据的32位CRC校验字节
        // 填充：四字节填充数据，0x0D,0x0A,0x0D,0x0A，用于增加通讯稳定性
        //0x66 	0x99	0x02	0x06	0x56	0x45	0x52 	0x49	0x46	0x59	0xXX	0xXX	0xXX	0xXX	0x0D	0x0A	0x0D	0x0A
        //==================================================================================
        public bool Flash_Verify(int timeOut = 360000)
        {
            byte[] szCommand = new byte[] { 0x66, 0x99, 0x02, 0x06, 0x56, 0x45, 0x52, 0x49, 0x46, 0x59 };
            byte[] szCompareStr = new byte[] { 0x99, 0x66, 0x02, 0x06, 0x56, 0x45, 0x52, 0x49, 0x46, 0x59 };
            byte[] szResponed = new byte[1024];

            byte[] szMessageOuput = new byte[1024]; ;
            byte[] szReadOutRS232 = new byte[5000];

            try
            {
                UInt32 CRCResult = 0x00000000;
                bool add0x0D0A = true;
                byte[] bytesToWrite = FlashMachineCRC32(szCommand, ref CRCResult, add0x0D0A);

                Write(bytesToWrite);
                Thread.Sleep(500);

                byte[] bytesRead = ReadLine();
                if (bytesRead == null)
                    return false;

                if (GetIndexOf(bytesRead, szCompareStr) < 0)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form Flash_Verify: \r\n{ex.Message}");
            }


        }

        //==================================================================================
        // 函数名:
        // 描叙:
        // 形参说明：
        // 0x99 0x66为报头
        // 0x03为指令
        // N为要发送的数据长度
        // 数据：3个字节日期+4个字节版本+N字节芯片目录。举例：从机要发送日期2019年08月21日+版本10.13-3.12;
        // 日期：0x13, 0x08, 0x15 为日期2019年08月21日；
        // 版本：0x10, 0x13, 0x03, 0x12为10.13-3.12，主机接收到版本要在电脑显示为M10.13-S3.12
        // 芯片目录: 形式未定
        // 注意：主机接收到日期，版本号和芯片目录三者要与电脑一一比较，日期，版本号，或芯片目录其中一个对不上就报错；
        // CRC：数据为前面4+N个字节数据的32位CRC校验字节
        // 填充：四字节填充数据，0x0D,0x0A,0x0D,0x0A，用于增加通讯稳定性

        // 0x66 0x99为报头
        // 0x03为指令
        // 0x26为芯片目录的数据长度
        // 芯片目录: 1个字（32bit）的序号+8个字（32bit）的菜单节点信息（私有数据）
        // 行/列：产品的拼版方式 取值只能是1-10。
        // CRC：数据为前面4+N个字节数据的32位CRC校验字节
        // 填充：四字节填充数据，0x0D,0x0A,0x0D,0x0A，用于增加通讯稳定性

        //返回：
        // 0x99 0x66为报头
        // 0x03为指令
        // 0x2d为要发送的数据长度
        // 数据：3个字节日期+4个字节版本+36字节芯片目录。举例：从机要发送日期2019年08月21日+版本10.13-3.12;
        // 日期：0x13, 0x08, 0x15 为日期2019年08月21日；
        // 版本：0x10, 0x13, 0x03, 0x12为10.13-3.12，主机接收到版本要在电脑显示为M10.13-S3.12
        // 芯片目录: 
        // 正常：1个字（32bit）的序号+8个字（32bit）的菜单节点信息（私有数据）
        // 错误码：36个0xE0表示所选型号超过菜单的范围。
        //               36个0xE1表示所选型号没有关联相关操作函数。
        // 行/列：产品的拼版方式 取值只能是1-10。如超出范围，返回0xE0
        // CRC：数据为前面4+N个字节数据的32位CRC校验字节
        // 填充：四字节填充数据，0x0D,0x0A,0x0D,0x0A，用于增加通讯稳定性
        // 注意：主机接收到日期，版本号和芯片目录和行/列，四者要与电脑一一比较，其中一个对不上就报错；
        // 在现在的带头项目中，设备的列只能1-4取值，但付工写的上位机程序，还是能1-10取值


        //0x99	0x66 	0x03	N	 0x13	 0x08	0x15 	0x10 	0x13	 0x03	0x12	形式未定　	0xXX	0xXX	0xXX	0xXX	0x0D	0x0A	0x0D	0x0A
        //==================================================================================
        public bool Flash_SetType(string type, ref string dateTime, ref string version, ref bool compare, int timeOut = 10000)
        {
            try
            {
                byte[] bytesType = System.Text.Encoding.ASCII.GetBytes(type);
                return Flash_SetType(bytesType, ref  dateTime, ref version, ref compare, timeOut);
            }
            catch(Exception ex)
            {
                throw new Exception($"Exception form Flash_SetType: \r\n{ex.Message}");
            }
        }

        public bool Flash_SetType(byte[] type, ref string dateTime, ref string version, ref bool compare, int timeOut = 10000)
        {
            byte[] szCommand = new byte[] { 0x66, 0x99, 0x03, 0x26, 0x00, 0x00 };
            byte[] szCompareStr = new byte[] { 0x99, 0x66, 0x03 };
            byte[] szCompareStrV2 = new byte[] { 0x99, 0x66, 0x03, 0x2D};

            byte[] szMessageOuput = new byte[1024]; ;
            byte[] szReadOutRS232 = new byte[5000];

            try
            {
                if (nRow <= 0  || nCol <= 0 || nRow > 10 || nCol > 10)
                    throw new Exception($"Exception form Flash_SetType: \r\n{"row, col paramters is error"}");

                if (type.Length < 36)
                    throw new Exception($"Exception form Flash_SetType: \r\n{"type paramters is error, length mus be > 36"}");

                //设置行和例
                if(emDataMode == emFlashData_Mode.emFlashData_Mode_Normal)
                {
                    szCommand[4] = this.nRow;
                    szCommand[5] = this.nCol;
                }
                else
                {
                    szCommand[4] = this.nCol;
                    szCommand[5] = this.nRow;
                }

                //设置命令
                byte[] bytesType = null;
                if (type.Length > 36)
                    bytesType = (byte[]) ((byte[]) type.Clone()).Take(36);
                else
                    bytesType = (byte[]) type.Clone();

                //加上命令
                bytesType = szCommand.Concat(bytesType).ToArray();

                //CRC32 校验
                UInt32 CRCResult = 0x00000000;
                bool add0x0D0A = true;
                byte[] bytesToWrite = FlashMachineCRC32(bytesType, ref CRCResult, add0x0D0A);

                //发送命令
                Write(bytesToWrite);
                Thread.Sleep(2000);

                //读取数据
                byte[] bytesRead = ReadLine(timeOut);
                if (bytesRead == null || bytesRead.Length < 57)
                    return false;

                //确认返回的数据没有问题
                if (GetIndexOf(bytesRead, szCompareStrV2) < 0)
                    return false;
                

                for(int nIndex = 0; nIndex < 38; nIndex++)
                {
                    if (bytesToWrite[4 + nIndex] != bytesRead[11 + nIndex])
                        return false;
                }

                //拼日期
                UInt32 dwData = 0;
                UInt32 year = (UInt32)bytesRead[4] + 2000;
                UInt32 month = (UInt32)bytesRead[5];
                UInt32 day = (UInt32)bytesRead[6];

                string szData = $"{ year}-{ month.ToString("D02")}-{ day.ToString("D02")}";

                CurrentDataTime = dateTime = szData;

                //拼版本
                string ver1 = bytesRead[7].ToString("X02");
                string ver2 = bytesRead[8].ToString("X02");
                string ver3 = bytesRead[9].ToString("X02");
                string ver4 = bytesRead[10].ToString("X02");
                szData = $"{ ver1}.{ ver2}-{ ver3}.{ ver4}";

                CurrentVersion = version = szData;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form Flash_SetType: \r\n{ex.Message}");
            }
        }

        //==================================================================================
        // 函数名:
        // 描叙:
        // 形参说明：
        //0x66 0x99为报头
        //0x04为指令
        //0x06为要发送的数据长度，“RESULT”的字节个数为6
        //0x52 0x45 0x53 0x55 0x4C 0x54为“RESULT”的ASSIC码
        //CRC：数据为0x66 0x99 0x02 0x06 0x52 0x45 0x53 0x55 0x4C 0x54 ，前10个字节数据的32位CRC校验字节
        //填充：四字节填充数据，0x0D,0x0A,0x0D,0x0A，用于增加通讯稳定性
        //0x66 	0x99	0x04	0x06	0x52	0x45	0x53 	0x55	0x4C	0x54	0xXX	0xXX	0xXX	0xXX	0x0D	0x0A	0x0D	0x0A
        //==================================================================================
        public FlashResult[] Flash_GetResult_OutList(ref string retrData, int timeOut = 240000)
        {
            FlashResult[] arryResult = new FlashResult[100];
            byte[] bytesResult = new byte[100];
            byte[] szCommand = new byte[] { 0x66, 0x99, 0x04, 0x06, 0x52, 0x45, 0x53, 0x55, 0x4C, 0x54 };
            byte[] szCompareStr = new byte[] { 0x99, 0x66, 0x04, 0x64 };
            byte[] szResponed = new byte[1024];
            byte[] szMessageOuput = new byte[1024];
            byte[] szReadOutRS232 = new byte[5000];
            UInt32 CRCResult = 0x00000000;
            bool add0x0D0A = true;

            try
            {
                retrData = "";
                byte[] bytesToWrite = FlashMachineCRC32(szCommand, ref CRCResult, add0x0D0A);

                Write(bytesToWrite);
                Thread.Sleep(500);

                byte[] bytesRead = ReadLine(timeOut);
                if (bytesRead == null)
                    return null;

                if (GetIndexOf(bytesRead, szCompareStr) < 0)
                    return null;

                byte[] bytesData = bytesRead.Skip(4).Take(100).ToArray();

                #region>>>>>> 进行数据转换
                if (emDataMode != emFlashData_Mode.emFlashData_Mode_Normal)
                    bytesData = SwapData(bytesData, emDataMode);
                #endregion

                //======================================================
                #region>>>>>> 如下因为客户烧录机处于工程模式，所有数据需要进行转换
                bool bEngMode = false;
                if(bEngMode)
                {
                    for (int nRow = 0; nRow < 10; nRow++)
                    {
                        for (int nCol = 0; nCol < 10; nCol++)
                            if (nCol == 2 || nCol == 3)
                            {
                                bytesData[nRow * 10 + nCol] = bytesData[nRow * 10 + nCol + 3];
                                bytesData[nRow * 10 + nCol + 3] = 0xFF;
                            }
                    }
                }
                #endregion

                //=====================================================
                #region>>>>>> 得到烧录机返回的原始数据
                for (int nIndex = 0; nIndex < 100; nIndex++)
                {
                    arryResult[nIndex] = new FlashResult();
                    arryResult[nIndex].bResult = (bytesData[nIndex] == 0x00 ? true : false);
                    arryResult[nIndex].byteError = bytesData[nIndex];

                    if(nIndex == 100 -1)
                        retrData += $"0x{bytesData[nIndex].ToString("X02")}";
                    else
                        retrData += $"0x{bytesData[nIndex].ToString("X02")}, ";
                }
                #endregion

                return arryResult;
            }
            catch(Exception ex)
            {
                throw new Exception($"Exception form FlashResult[] Flash_GetResult_OutList: \r\n{ex.Message}");
            }

        }

        public byte[] SwapData(byte[] inputArry, emFlashData_Mode emDataMode)
        {
            try
            {
                if (inputArry == null || inputArry.Length < 100)
                    return null;

                byte[,] newArry2D = new byte[10, 10];

                #region>>>>>> 不转换数据
                if (emDataMode == emFlashData_Mode.emFlashData_Mode_Normal)
                {
                    return inputArry;
                }
                #endregion

                #region>>>>>> 行例转换
                else if (emDataMode == emFlashData_Mode.emFlashData_Mode_RowToCol)
                {
                    byte[] newArry1D = new byte[100];
                    int Index = 0;
                    for (int nCol = 0; nCol < 10; nCol++)
                    {
                        for (int nRow = 0; nRow < 10; nRow++)
                            newArry2D[nRow, nCol] = inputArry[Index++];
                    }

                    Index = 0;
                    for (int nRow = 0; nRow < 10; nRow++)
                    {
                        for (int nCol = 0; nCol < 10; nCol++)
                            newArry1D[Index++] = newArry2D[nRow, nCol];
                    }

                    return newArry1D;
                }
                #endregion

                return null;

            }
            catch (Exception ex)
            {
                throw new Exception($"Exception form SwapData: \r\n{ex.Message}");
            }
        }

        public bool[] Flash_GetResult_List(int timeOut = 240000)
        {
            try
            {
                bool[] IsOkOrNg = null;
                byte[] szCommand = new byte[] { 0x66, 0x99, 0x04, 0x06, 0x52, 0x45, 0x53, 0x55, 0x4C, 0x54 };
                byte[] szCompareStr = new byte[] { 0x99, 0x66, 0x04, 0x64 };
                byte[] szResponed = new byte[1024];

                byte[] szMessageOuput = new byte[1024]; ;
                byte[] szReadOutRS232 = new byte[5000];

                UInt32 CRCResult = 0x00000000;
                bool add0x0D0A = true;
                byte[] bytesToWrite = FlashMachineCRC32(szCommand, ref CRCResult, add0x0D0A);

                Write(bytesToWrite);
                Thread.Sleep(500);

                byte[] bytesRead = ReadLine(timeOut);
                if (bytesRead == null)
                    return null;

                if (GetIndexOf(bytesRead, szCompareStr) < 0)
                    return null;

                IsOkOrNg = new bool[100];
                bytesRead = bytesRead.Skip(5).Take(100).ToArray();
                for (int i = 0; i < bytesRead.Length; i++)
                {
                    IsOkOrNg[i] = bytesRead[i] != 0 ? false : true;
                }

                return IsOkOrNg;
            }
            catch(Exception ex)
            {
                throw new Exception($"Exception form bool[] Flash_GetResult_List: \r\n{ex.Message}");
            }
        }
    }
}
