﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;


namespace CommunicationZH
{
    public class RobotBase : Object
    {
        public object commandLock = new object();
        public bool m_bIsRobotConnected;
        public string m_strConnectString;
        protected TcpClient m_TCP_RobotClient = new TcpClient();
        protected string strRobotLastMsg;

        public bool bMonitorIOStatus = false;
        public UInt32 IOInputState;
        public UInt32 IOOutputState;

        public string IpAddress;
        public int Port;
        public string PSW;


        protected bool bIsQuit = false;
        protected bool bAutoConnect = false;

        public struct strRobotPosDef
        {
            public double x;
            public double y;
            public double z;
            public double ru;
            public double v;
            public double w;

            void Initialize() { x = 999.99; y = 999.99; z = 999.99; ru = 999.99; v = 999.99; w = 999.99; }
        }

        public enum emRobotMoveType
        {
            MoveType_Jump = 0,
            MoveType_Move,
            MoveType_Arc,
            MoveType_Jump3,
            MoveType_Arc3,
            MoveType_Go,
        };

        public enum emRobotAxisIndex
        {
            RobotAxis_X = 0,
            RobotAxis_Y,
            RobotAxis_Z,
            RobotAxis_RU,
            RobotAxis_V,
            RobotAxis_W,
        };

        public enum emRobotHandType
        {
            HandType_Left = 0,
            HandType_Right,
            HandType_Auto,
            HandType_Nodefine,
        };

        public class RobotPosition
        {
            public double x;
            public double y;
            public double z;
            public double r;
            public bool bMoveOrTeach;
            public int nSmart;
            public emRobotMoveType emMoveType;
            public emRobotHandType emHandType;
            public double fLimZ;
            public string AsyncCommand;

            public RobotPosition()
            {
                x = -9999.99;
                y = -9999.99;
                z = -9999.99;
                r = -9999.99;
                emMoveType = emRobotMoveType.MoveType_Jump;
                emHandType = emRobotHandType.HandType_Auto;
                fLimZ = 0.00;
                AsyncCommand = "";
            }

            public override string ToString()
            {
                string Retun = $"{ this.x.ToString("F2")}, {y.ToString("F2")}, {z.ToString("F2")}, {r.ToString("F2")}";
                if (emHandType == emRobotHandType.HandType_Left)
                    Retun += ", /L";
                else if (emHandType == emRobotHandType.HandType_Right)
                    Retun += ", /R";

                return Retun;
            }

            public static RobotPosition Parse(string pos)
            {
                RobotPosition structPos = new RobotPosition() ;

                string[] values = pos.Split(",".ToCharArray());

                if (values.Length < 5)
                    return structPos;

                structPos.x = double.Parse(values[0]);
                structPos.y = double.Parse(values[1]);
                structPos.z = double.Parse(values[2]);
                structPos.r = double.Parse(values[3]);

                //确定手臂姿势
                if (pos.Contains("/L"))
                    structPos.emHandType = emRobotHandType.HandType_Left;
                else if (pos.Contains("/R"))
                    structPos.emHandType = emRobotHandType.HandType_Right;

                //确定移动类型
                if (pos.Contains("Jump"))
                    structPos.emMoveType = emRobotMoveType.MoveType_Jump;
                else if (pos.Contains("Move"))
                    structPos.emMoveType = emRobotMoveType.MoveType_Move;
                else if (pos.Contains("Go"))
                    structPos.emMoveType = emRobotMoveType.MoveType_Go;
                else if (pos.Contains("Arc"))
                    structPos.emMoveType = emRobotMoveType.MoveType_Arc;
                else if (pos.Contains("Jump3"))
                    structPos.emMoveType = emRobotMoveType.MoveType_Jump3;
                else if (pos.Contains("Arc3"))
                    structPos.emMoveType = emRobotMoveType.MoveType_Arc3;

                //如果为Jump, 确定Zlimit 
                if (values.Length >= 7)
                    structPos.fLimZ = double.Parse(values[7]);

                return structPos;
            }
        }

        public RobotBase()
        {

        }

        ~RobotBase()
        {
            bIsQuit = true;
        }

        public virtual int Connect(string strIPAndPort, int nPort, string strPassword = "", bool bMotorOn = true)
        {
            return 0;
        }
        public virtual void DisConnect()
        {
            return;
        }
        public virtual bool Command(ref string szCommand, ref string szResponed, ref string szWaitComplete, bool bWaitForCompleted = true, int SleepTime = 200, Int32 dwTimeOut = 60000)
        {
            return false;
        }
        public virtual int Stop()
        {
            return 0;
        }
        public virtual int StartMainProgram(int nProgram = 0)
        {
            return 0;
        }
        public virtual int Home(int nIndexRobot = 0, Int32 dwTimeOut = 30000)
        {
            return 0;
        }
        public virtual int ServoOn(bool bOnOff, int nIndexRobot = 0)
        {
            return 0;
        }
        public virtual int Suspend()
        {
            return 0;
        }
        public virtual int EMG()
        {
            return 0;
        }
        public virtual int SetCoordinateSystem(bool bLocalorTool = true, int nIndex = 0)
        {
            return 0;
        }

        public virtual bool TestPosition(double x, double y, double z, double r, double resolution = 0.05)
        {
            return false;
        }
        public virtual int Move(double x, double y, double z, double r, bool bMoveOrTeach = true, int nSmart = 1, 
                                                 emRobotMoveType emMoveType = emRobotMoveType.MoveType_Jump, 
                                                 emRobotHandType emHandType = emRobotHandType.HandType_Right, 
                                                 double fLimZ = 0.00, string AsynCommand = "")
        {
            return 0;
        }

        public virtual int Move(RobotPosition endPos, RobotPosition passPos)
        {
            return 0;
        }
        public virtual bool IsReady()
        {
            return false;
        }
        public virtual int GetCurrentPosition(ref double x, ref double y, ref double z, ref double r, ref emRobotHandType handType)
        {
            return 0;
        }
        public virtual int Reset()
        {
            return 0;
        }
        public virtual int SetSpeed(int nSpeed = 2, bool bSetAutoManual = true)
        {
            return 0;
        }
        public virtual int GetIOLine(int nIndex)
        {
            return 0;
        }
        public virtual int GetIOPort8(int nIndex)
        {
            return 0;
        }
        public virtual int GetIOPort16(int nIndex)
        {
            return 0;
        }
        public virtual int SetIOLine(int nIndex, bool state)
        {
            return 0;
        }
        public virtual int SetIOPort8(int nIndex, byte value)
        {
            return 0;
        }
        public virtual int SetIOPort16(int nIndex, UInt16 value)
        {
            return 0;
        }
        public virtual int MoveWorkingPos()
        {
            return 0;
        }
        public virtual string GetRobotPos_Manual(int ID, bool bFroceRAxisToZero = false)
        {
            return null;
        }
        public void ClearReceiveBuffer()
        {

        }

        public int Send(string data, int dataLen = -1, int flags = 0)
        {
            int nLenActraul = 0;

            nLenActraul = m_TCP_RobotClient.Client.Send(System.Text.Encoding.Default.GetBytes(data));

            return nLenActraul;

        }

        public int WaitForReceive(long msecs = 20000, long uSecs = 0)
        {
            int nLenActraul = 0;
            DateTime time1, time2;

            time1 = time2 = DateTime.Now;
            while (true)
            {
                Thread.Sleep(5);
                nLenActraul = m_TCP_RobotClient.Client.Available;
                if (nLenActraul > 0)
                    break;

                time2 = DateTime.Now;
                if ((time2 - time1).TotalMilliseconds >= msecs)
                    return 0;
            }

            return nLenActraul;
        }

        public int Receive(ref string data, int maxLen, int timeOut = 0)
        {
            int nLenActraul = m_TCP_RobotClient.Client.Available;
            if (nLenActraul > 0)
            {
                byte[] rev = new byte[nLenActraul];
                nLenActraul = m_TCP_RobotClient.Client.Receive(rev);

                data = System.Text.Encoding.Default.GetString(rev).Trim();
                return nLenActraul;
            }
            return nLenActraul;
        }

        public int GetMaxReceive()
        {
            int nLenActraul = 0;

            return nLenActraul;
        }

    }

    class EpsonTCPNet : RobotBase
    {
        private Thread threadOfHeart = null;

        ~EpsonTCPNet()
        {
            if(m_TCP_RobotClient != null && m_bIsRobotConnected)
                DisConnect();
        }

        public override bool Command(ref string szCommand, ref string szResponed, ref string szWaitComplete, bool bWaitForCompleted = true, int SleepTime = 200, Int32 dwTimeOut = 10000)
        {
            if (commandLock == null)
                commandLock = new object();

            lock (commandLock)
            {
                try
                {
                    DateTime Time1, Time2;
                    Time1 = Time2 = DateTime.Now;
                    string szErrorMsg = "";



                    #region>>>>>>确认是否有效, 如果机器人断开，重连
                    if (m_TCP_RobotClient == null ||
                        m_TCP_RobotClient.Client == null ||
                        !m_TCP_RobotClient.Client.Connected)
                    {
                        m_bIsRobotConnected = false;
                        if (bAutoConnect)
                            m_bIsRobotConnected = (Connect(IpAddress, Port, PSW, true) == 0 ? true : false);

                        if (!m_bIsRobotConnected)
                            return false;
                    }
                    #endregion

                    string szDisplayMsg = "";
                    szDisplayMsg = string.Format("发送机器人命令{0}", szCommand);

                    //发送命令
                    ClearReceiveBuffer(); //清除原来的接受缓存

                    if (Send(szCommand) <= 0)
                        return false;

                    //如果不需要等待则直接返回
                    if (!bWaitForCompleted)
                        return true;

                    //等待网口数据返回
                    string szData = "";
                    if (WaitForReceive(dwTimeOut) <= 0)
                        return false;

                    //读取返回的数据
                    Thread.Sleep(10);
                    int nLen = GetMaxReceive();
                    if (Receive(ref szData, nLen, dwTimeOut) <= 0) //20s Timeout
                        return false;

                    //比较返回的字符是否正确
                    szResponed = szData;
                    if (szData.Contains(szWaitComplete))
                    {
                        return true;
                    }
                    else
                    {
                        szErrorMsg = string.Format("Command 发送命令出错：命令{0} 应收到{1} 实际收到要{2} ", szCommand, szWaitComplete, szResponed);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    strRobotLastMsg = $"Command excute Error:\r\n{ex.Message}";
                    throw new Exception(strRobotLastMsg);
                }
            }
        }

        public override void DisConnect()
        {
            try
            {
                string szCommand = "$Logout\r\n";
                string szResponed = "";
                string szWaitComplete = "#Logout";
                bIsQuit = true;
                if(threadOfHeart != null)
                {
                    threadOfHeart.Join(500);
                    if (threadOfHeart.IsAlive)
                        threadOfHeart.Abort();

                    threadOfHeart = null;
                }
               
                Command(ref szCommand, ref szResponed, ref szWaitComplete);
                Thread.Sleep(200);
                m_TCP_RobotClient.Close();
                m_TCP_RobotClient = null;
                return;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"DisConnect excute Error:\r\n{ex.Message}";
                return;
            }

        }

        public override int Connect(string strIPAndPort, int nPort, string strPassword, bool bMotorOn)
        {
            try
            {
                string szErrMsg = "";
                long lError = 0;
                string[] vetRS232String = null;

                string szCommand = "$Login\r\n";
                string szResponed = "";
                string szWaitComplete = "#Login,0";

                //停止所有程序
                m_bIsRobotConnected = false;

                //连接机器人
                DisConnect();

                if (m_TCP_RobotClient == null)
                    m_TCP_RobotClient = new TcpClient();

                m_TCP_RobotClient.Connect(strIPAndPort, nPort);
                if (!m_TCP_RobotClient.Connected)
                    return -1;

                //发送Login命令
                szCommand = "$Login,123456\r\n";
                szResponed = "";
                szWaitComplete = "#Login,0";

                szCommand = string.Format("$Login,{0}\r\n", strPassword);
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 5000))
                    return -2;

                //连接成功
                m_bIsRobotConnected = true;
                IpAddress = strIPAndPort;
                Port = nPort;
                PSW = strPassword;

                //启用机器人
                if (bMotorOn)
                    ServoOn(true);

                //设为低速
                SetSpeed(5);

                //建立心跳
                if(threadOfHeart == null)
                {
                    bIsQuit = false;
                    threadOfHeart = new Thread(new ParameterizedThreadStart(thread_TestHeartBettwenRobot));
                    threadOfHeart.Start(null);
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"Connect excute Error:\r\n{ex.Message}";
                return -2;
            }
        }

        private void thread_TestHeartBettwenRobot(object wsarg)
        {
            while (!bIsQuit)
            {
                Thread.Sleep(500);
                try
                {
                    if (m_TCP_RobotClient == null ||
                        m_TCP_RobotClient.Client == null || 
                        !m_TCP_RobotClient.Client.Connected ||
                        !m_bIsRobotConnected)
                    {
                        m_bIsRobotConnected = false;
                        continue;
                    }

                    m_bIsRobotConnected = true;
                    if (bMonitorIOStatus)
                    {
                        int oneTo16 = GetIOPort16(0);
                        int Start16To32 = GetIOPort16(1);
                        IOInputState = (((UInt32)Start16To32) << 16) | ((UInt32)oneTo16);
                    }
                    else
                    {
                        string szResponed = "";
                        string szWaitComplete = "#GetStatus";
                        string szCommand = string.Format("$GetStatus\r\n");
                        if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                        {
                            m_bIsRobotConnected = false;
                            strRobotLastMsg = string.Format("thread_TestHeartBettwenRobot  执行其间发生错误\r\n");
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_bIsRobotConnected = false;
                    strRobotLastMsg = string.Format("thread_TestHeartBettwenRobot  执行其间发生异常\r\n{0}", ex.ToString());
                }
            }
        }

        public override int ServoOn(bool bOnOff, int nIndexRobot = 0)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#SetMotorsOn";
                string szErrorMsg = "";

                //清除报警
                if (Reset() != 0)
                    return -1;

                //启用伺服
                Thread.Sleep(200);
                if (bOnOff)
                {
                    szCommand = string.Format("$SetMotorsOn,{0}\r\n", nIndexRobot);
                    szWaitComplete = $"#SetMotorsOn,0";
                }
                else
                {
                    szCommand = string.Format("$SetMotorsOff,{0}\r\n", nIndexRobot);
                    szWaitComplete = $"#SetMotorsOff,0";
                }

                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"ServoOn 发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -2;
                }

                //得到状态
                DateTime Time1, Time2;
                Time1 = Time2 = DateTime.Now;
                if (bOnOff)
                {
                    while (!IsReady())
                    {
                        Time2 = DateTime.Now;
                        if ((Time2 - Time1).TotalMilliseconds >= 3000)
                        {
                            strRobotLastMsg = $"ServoOn 启用机器机人，机器人无法设定在Ready状态!";
                            return -3;
                        }
                    }
                }
                else
                {
                    while (IsReady())
                    {
                        Time2 = DateTime.Now;
                        if ((Time2 - Time1).TotalMilliseconds >= 3000)
                        {
                            strRobotLastMsg = $"ServoOn 启用机器机人，机器人无法禁用!";
                            return -4;
                        }
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"ServoOn excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int StartMainProgram(int nProgram)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Start,0";
                string szErrorMsg = "";

                szCommand = string.Format("$Start,{0}\r\n", nProgram);
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"StartMainProgram excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int Home(int nIndexRobot = 0, Int32 dwTimeOut = 30000)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Home,0";
                string szErrorMsg = "";

                if (ServoOn(true) != 0)
                {
                    return -1;
                }

                Thread.Sleep(200);

                //设置速度
                SetSpeed(5);

                szCommand = string.Format("$Home,{0}\r\n", nIndexRobot);
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -2;
                }

                //等待复位完成
                DateTime Time1, Time2;
                Time1 = Time2 = DateTime.Now;

                while (!IsReady())
                {
                    Time2 = DateTime.Now;
                    if ((Time2 - Time1).TotalMilliseconds >= dwTimeOut)
                    {
                        strRobotLastMsg = $"Home 错误：回原点超时！";
                        return -3;
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"Home excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int Suspend()
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Pause,0";
                string szErrorMsg = "";

                szCommand = string.Format("$Pause\r\n");
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"Suspend excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int EMG()
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Stop,0";
                string szErrorMsg = "";

                szCommand = string.Format("$Stop\r\n");
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"EMG excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int Reset()
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Reset,0";
                string szErrorMsg = "";



                szCommand = string.Format("$Reset\r\n");
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }



                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"Reset excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int Stop()
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Stop,0";
                string szErrorMsg = "";

                szCommand = string.Format("$Stop\r\n");
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"Stop excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int SetCoordinateSystem(bool bLocalorTool, int nIndex)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Local";
                string szErrorMsg = "";

                if (bLocalorTool)
                {
                    szCommand = string.Format("$Local,{0}\r\n", nIndex);
                    szWaitComplete = $"#Local\r\n";
                }
                else
                {
                    szCommand = string.Format("$Tool,{0}\r\n", nIndex);
                    szWaitComplete = $"#$Tool\r\n";
                }

                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"SetCoordinateSystem excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override bool TestPosition(double x, double y, double z, double r, double resolution = 0.05)
        {
            try
            {
                double x_c = -999.99, y_c = -999.99, z_c = -999.99, r_c = -999.99;
                RobotBase.emRobotHandType handType = RobotBase.emRobotHandType.HandType_Auto;
                GetCurrentPosition(ref x_c, ref y_c, ref z_c, ref r_c, ref handType);
                {
                    if (Math.Abs(x_c - x) <= resolution &&
                       Math.Abs(y_c - y) <= resolution &&
                       Math.Abs(z_c - z) <= resolution &&
                       Math.Abs(r_c - r) <= resolution)
                        return true;
                    else
                        return false;
                }

            }
            catch(Exception ex)
            {
                strRobotLastMsg = $"TestPosition excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }
        }

        public override int Move(double x, double y, double z, double r, bool bMoveOrTeach, int nSmart, emRobotMoveType emMoveType, emRobotHandType emHandType, double fLimZ = -999, string AsynCommand = "")
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Execute";
                string szErrorMsg = "";
                emRobotHandType armType = emHandType;

                //测试一下当前的位置是否为和移动位置相同
                if (TestPosition(x, y, z, r, 0.05))
                    return 0;

                //确定手臂移动的姿势
                if (emHandType == emRobotHandType.HandType_Auto)
                {
                    armType = GetHandType();
                    if (armType == emRobotHandType.HandType_Nodefine)
                        return -5;
                }
                //设定移动的点
                szCommand = string.Format("$Execute,\"P98=XY({0},{1},{2},{3}) {4}\"\r\n", x.ToString("F3"), y.ToString("F3"), z.ToString("F3"), r.ToString("F3"), armType == emRobotHandType.HandType_Right ? "/R" : "/L");
                szWaitComplete = $"#Execute,0";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"设定点位到P98出现异常, (X: {x.ToString("F3")}, Y: {y.ToString("F3")}, Z: {z.ToString("F3")}, R: {r.ToString("F3")})";
                    return -1;
                }

                //移动
                // ====================Jump=====================
                if (emMoveType == emRobotMoveType.MoveType_Jump)
                {
                    if(AsynCommand != null && AsynCommand.Length > 0)
                        szCommand = $"$Execute,\"Jump P98\"\r\n";
                    else
                        szCommand = $"$Execute,\"Jump P98\"\r\n";
                    //	szCommand = string.Format("$Execute,\"Jump P98 LimZ %.2lf\"\r\n", fLimZ);
                }
                // ====================Move=====================
                else if (emMoveType == emRobotMoveType.MoveType_Move)
                {
                    double x_c = -999.99, y_c = -999.99, z_c = -999.99, r_c = -999.99;
                    RobotBase.emRobotHandType handType = RobotBase.emRobotHandType.HandType_Auto;
                    if (GetCurrentPosition(ref x_c, ref y_c, ref z_c, ref r_c, ref handType) != 0)
                    {
                        strRobotLastMsg = $"Move得到当前的坐标出错";
                        return -2;
                    }

                    if (Math.Abs(x_c - x) > 0.01 || Math.Abs(y_c - y) > 0.01 || Math.Abs(z_c - z) > 0.01)
                        szCommand = $"$Execute,\"Move P98\"\r\n";
                    else
                        szCommand = $"$Execute,\"Move P98 ROT\"\r\n";
                }
                else
                {
                    strRobotLastMsg = $"Move移动类型出现错误（类型为：Jump, MoveL)";
                    return -3;
                }

                //设定近回命令
                szWaitComplete = $"#Execute,0";

                //发送远程命令
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -4;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"Move excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int Move(RobotPosition endPos, RobotPosition passPos = null)
        {
            try
            {
                if (endPos == null)
                {
                    strRobotLastMsg = $"设定点无效";
                    return -1;
                }

                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Execute";
                string szErrorMsg = "";
                bool bMoveOrTeach = true;
                int nSmart = 1;

                double x = endPos.x;
                double y = endPos.y;
                double z = endPos.z;
                double r = endPos.r;
                emRobotMoveType emMoveType = endPos.emMoveType;
                emRobotHandType emHandType = endPos.emHandType;
                double fLimZ = endPos.fLimZ;
                string AsynCommand = endPos.AsyncCommand;

                //设定移动的点
                szCommand = string.Format("$Execute,\"P98=XY({0},{1},{2},{3}) {4}\"\r\n", x.ToString("F3"), y.ToString("F3"), z.ToString("F3"), r.ToString("F3"), emHandType == emRobotHandType.HandType_Right ? "/R" : "/L");
                szWaitComplete = $"#Execute,0";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"设定点位到P98出现异常, (X: {x.ToString("F3")}, Y: {y.ToString("F3")}, Z: {z.ToString("F3")}, R: {r.ToString("F3")})";
                    return -1;
                }

                //设定过渡点
                if (passPos != null)
                {
                    double x_Pass = passPos.x;
                    double y_Pass = passPos.y;
                    double z_Pass = passPos.z;
                    double r_Pass = passPos.r;
                    emRobotMoveType emMoveType_Pass = passPos.emMoveType;
                    emRobotHandType emHandType_Pass = passPos.emHandType;
                    double fLimZ_Pass = passPos.fLimZ;
                    string AsynCommand_Pass = passPos.AsyncCommand;

                    szCommand = string.Format("$Execute,\"P97=XY({0},{1},{2},{3}) {4}\"\r\n", x_Pass.ToString("F3"), y_Pass.ToString("F3"), z_Pass.ToString("F3"), r_Pass.ToString("F3"), emHandType_Pass == emRobotHandType.HandType_Right ? "/R" : "/L");
                    szWaitComplete = $"#Execute,0";
                    if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                    {
                        strRobotLastMsg = $"设定点位到P97出现异常, (X: {x.ToString("F3")}, Y: {y.ToString("F3")}, Z: {z.ToString("F3")}, R: {r.ToString("F3")})";
                        return -1;
                    }
                }

                //移动
                // ====================Jump=====================
                if (emMoveType == emRobotMoveType.MoveType_Jump)
                {
                    if (AsynCommand != null && AsynCommand.Length > 0)
                        szCommand = $"$Execute,\"{(passPos == null ? "" : "Pass P97;")}Jump P98\"\r\n";
                    else
                        szCommand = $"$Execute,\"{(passPos == null ? "" : "Pass P97;")}Jump P98\"\r\n";
                    //	szCommand = string.Format("$Execute,\"Jump P98 LimZ %.2lf\"\r\n", fLimZ);
                }
                // ====================Move=====================
                else if (emMoveType == emRobotMoveType.MoveType_Move)
                {
                    double x_c = -999.99, y_c = -999.99, z_c = -999.99, r_c = -999.99;
                    RobotBase.emRobotHandType handType = RobotBase.emRobotHandType.HandType_Auto;
                    if (GetCurrentPosition(ref x_c, ref y_c, ref z_c, ref r_c, ref handType) != 0)
                    {
                        strRobotLastMsg = $"Move得到当前的坐标出错";
                        return -2;
                    }

                    if (Math.Abs(x_c - x) > 0.01 || Math.Abs(y_c - y) > 0.01 || Math.Abs(z_c - z) > 0.01)
                        szCommand = $"$Execute,\"{(passPos == null ? "" : "Pass P97;")}Move P98\"\r\n";
                    else
                        szCommand = $"$Execute,\"{(passPos == null ? "" : "Pass P97;")}Move P98 ROT\"\r\n";
                }
                else
                {
                    strRobotLastMsg = $"Move移动类型出现错误（类型为：Jump, MoveL)";
                    return -3;
                }

                //设定近回命令
                szWaitComplete = $"#Execute,0";

                //发送远程命令
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -4;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"Move excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public emRobotHandType GetHandType()
        {
            try
            {
                string szCommand = string.Format("$Execute,\"Print Here\"\r\n");
                string szResponed = "";
                string szWaitComplete = "#Execute,";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    throw new Exception(strRobotLastMsg);
                }

                //==================================自动确定手臂姿势==================================
                if (szResponed.Contains("/L"))
                {
                    return emRobotHandType.HandType_Left;
                }
                else
                {
                    return emRobotHandType.HandType_Right;
                }
            }
            catch(Exception ex)
            {
                strRobotLastMsg = $"MoveRelative excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public int MoveRelative(emRobotAxisIndex axis, double dst, emRobotHandType emHandType = emRobotHandType.HandType_Auto)
        {
            try
            {
                string szTemp = "";
                string szErrorMsg = "";

                double x = -999, y = -999, z = -999, r = -999;
                RobotBase.emRobotHandType handType = RobotBase.emRobotHandType.HandType_Auto;
                GetCurrentPosition(ref x, ref y, ref z, ref r, ref handType);
                if (x < -998.000 || y < -998.000 || z < -998.000 || r < -998.000)
                    return -1;

                switch (axis)
                {
                    case emRobotAxisIndex.RobotAxis_X:
                        x += dst;
                        break;

                    case emRobotAxisIndex.RobotAxis_Y:
                        y += dst;
                        break;

                    case emRobotAxisIndex.RobotAxis_Z:
                        z += dst;
                        break;

                    case emRobotAxisIndex.RobotAxis_RU:
                        r += dst;
                        break;

                    default:
                        return -3;
                }

                string szCommand = string.Format("$Execute,\"Print Here\"\r\n");
                string szResponed = "";
                string szWaitComplete = "#Execute,";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -4;
                }

                //==================================自动确定手臂姿势==================================
                if (emHandType == emRobotHandType.HandType_Auto)
                {
                    if (szResponed.Contains("/L"))
                    {
                        if (Move(x, y, z, r, true, 0, emRobotMoveType.MoveType_Move, emRobotHandType.HandType_Left) != 0)
                            return -4;
                    }
                    else
                    {
                        if (Move(x, y, z, r, true, 0, emRobotMoveType.MoveType_Move, emRobotHandType.HandType_Right) != 0)
                            return -4;
                    }
                }
                //==================================左手臂姿势==================================
                else if (emHandType == emRobotHandType.HandType_Left)
                {
                    if (Move(x, y, z, r, true, 0, emRobotMoveType.MoveType_Move, emRobotHandType.HandType_Left) != 0)
                        return -4;
                }

                //==================================右手臂姿势==================================
                else if (emHandType == emRobotHandType.HandType_Right)
                {
                    if (Move(x, y, z, r, true, 0, emRobotMoveType.MoveType_Move, emRobotHandType.HandType_Right) != 0)
                        return -4;
                }


               return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"MoveRelative excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override bool IsReady()
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#GetStatus";
                string szErrorMsg = "";

                szCommand = string.Format("$GetStatus\r\n");
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                    return false;

                //分析状态
                szResponed = szResponed.Replace("\r\n", "");
                string[] strRetrn = szResponed.Split(",".ToCharArray());
                if (strRetrn.Length < 3 || strRetrn[1].Length < 10)
                    return false;

                int nReadyIndex = strRetrn.ElementAt(1).Length - 1;
                char cReady = strRetrn.ElementAt(1).ElementAt(nReadyIndex);
                string szError = strRetrn.ElementAt(2);

                //得到Motor On/Off的信息
                szCommand = string.Format("$Execute,\"Print Motor\"\r\n");
                szWaitComplete = "#Execute";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 3000))
                    return false;

                strRetrn = szResponed.Split(",".ToCharArray());
                if (strRetrn.Length < 2)
                {
                    strRobotLastMsg = $"IsReady 返回字符串有误，请确认机器人命令是否正确或已连机Login。";
                    return false;
                }

                bool bIsMotorn = false;
                if (strRetrn.ElementAt(1).Contains("1\r\n"))
                    bIsMotorn = true;

                //返回状态
                if (cReady != '1' || szError != "0000" || !bIsMotorn)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"IsReady excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int GetCurrentPosition(ref double x, ref double y, ref double z, ref double r, ref emRobotHandType handType)
        {
            try
            {
                DateTime Time1, Time2;
                Time1 = Time2 = DateTime.Now;

                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Execute,\" X:";
                string szErrorMsg = "";

                szCommand = string.Format("$Execute,\"Print Here\"\r\n");
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                if (!szResponed.Contains("X:") ||
                   !szResponed.Contains("Y:") ||
                    !szResponed.Contains("Z:") ||
                    !szResponed.Contains("U:"))
                {
                    strRobotLastMsg = $"GetCurrentPosition 返回数据出现异常";
                    return -1;
                }

                int p = szResponed.IndexOf("X:");
                x = double.Parse(szResponed.Substring(p + 3, 9).Replace(" ", ""));

                p = szResponed.IndexOf("Y:");
                y = double.Parse(szResponed.Substring(p + 3, 9).Replace(" ", ""));

                p = szResponed.IndexOf("Z:");
                z = double.Parse(szResponed.Substring(p + 3, 9).Replace(" ", ""));

                p = szResponed.IndexOf("U:");
                r = double.Parse(szResponed.Substring(p + 3, 9).Replace(" ", ""));

                if (szResponed.Contains("/L"))
                {
                    handType = emRobotHandType.HandType_Left;
                }
                else if (szResponed.Contains("/R"))
                {
                    handType = emRobotHandType.HandType_Right;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"GetCurrentPosition excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int SetSpeed(int nSpeed = 2, bool bSetAutoManual = true)
        {
            try
            {
                DateTime Time1, Time2;
                Time1 = Time2 = DateTime.Now;

                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#Execute,0";
                string szErrorMsg = "";

                if (nSpeed <= 0)
                    nSpeed = 2;

//                 if (nSpeed >= 100)
//                     nSpeed = 100;

                if (nSpeed <= 20)
                {
                    szCommand = string.Format("$Execute,\"Power Low\"\r\n");
                    if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                    {
                        strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                        return -1;
                    }

                }
                else
                {
                    szCommand = string.Format("$Execute,\"Power High\"\r\n");
                    if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                    {
                        strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                        return -2;
                    }

                    szCommand = string.Format("$Execute,\"SpeedR 800\"\r\n");
                    if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                    {
                        strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                        return -3;
                    }

                    szCommand = string.Format("$Execute,\"SpeedS 1800\"\r\n");
                    if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                    {
                        strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                        return -4;
                    }

                    szCommand = string.Format("$Execute,\"AccelR 3500\"\r\n");
                    if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                    {
                        strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                        return -5;
                    }

                    szCommand = string.Format("$Execute,\"AccelS 5000\"\r\n");
                    if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                    {
                        strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                        return -6;
                    }
                }

                //设定速度
                Thread.Sleep(100);
                int robotSpeed = nSpeed;
                if (robotSpeed >= 100)
                    robotSpeed = 100;

                szCommand = string.Format("$Execute,\"Speed {0},{1},{2}\"\r\n", robotSpeed.ToString(), robotSpeed.ToString(), robotSpeed.ToString());
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -7;
                }

                //设定加减速
                Thread.Sleep(100);
                int robotAccel = nSpeed;
                if (robotAccel >= 120)
                    robotAccel = 120;
                szCommand = string.Format("$Execute,\"Accel {0},{1}\"\r\n", robotAccel.ToString(), robotAccel.ToString());
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -8;
                }

                //设定SpeedR

                //设定SpeedS

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"SetSpeed excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int GetIOLine(int nIndex)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#GetIO,";

                szCommand = $"$GetIO,{nIndex}\r\n";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                int nSeach = szResponed.IndexOf(szWaitComplete);
                if (nSeach >= 0)
                    return int.Parse(szResponed.Substring(szWaitComplete.Length));
                else
                    return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"GetIOLine excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int GetIOPort8(int nIndex)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#GetIOByte,";

                szCommand = $"$GetIOByte,{nIndex}\r\n";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                int nSeach = szResponed.IndexOf(szWaitComplete);
                if (nSeach >= 0)
                    return int.Parse(szResponed.Substring(szWaitComplete.Length), System.Globalization.NumberStyles.HexNumber);
                else
                    return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"GetIOPort8 excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int GetIOPort16(int nIndex)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#GetIOWord,";

                szCommand = $"$GetIOWord,{nIndex}\r\n";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                int nSeach = szResponed.IndexOf(szWaitComplete);
                if (nSeach >= 0)
                {
                    string a = szResponed.Substring(szWaitComplete.Length);
                    return int.Parse(a, System.Globalization.NumberStyles.HexNumber);
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"GetIOPort16 excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int SetIOLine(int nIndex, bool state)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#SetIO,0";

                szCommand = $"$SetIO,{nIndex},{(state ? "1" : "0")}\r\n";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                int nSeach = szResponed.IndexOf(szWaitComplete);
                if (nSeach > 0)
                    return int.Parse(szResponed.Substring(szWaitComplete.Length + 1));
                else
                    return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"SetIOLine excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int SetIOPort8(int nIndex, byte value)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#SetIOByte,0";

                szCommand = $"$SetIOByte,{nIndex},{value}\r\n";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"SetIOPort8 excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }

        }

        public override int SetIOPort16(int nIndex, UInt16 value)
        {
            try
            {
                string szCommand = "";
                string szResponed = "";
                string szWaitComplete = "#SetIOWord,0";

                szCommand = $"$SetIOWord,{nIndex},{value}\r\n";
                if (!Command(ref szCommand, ref szResponed, ref szWaitComplete, true, 200, 2000))
                {
                    strRobotLastMsg = $"发送 {szCommand} 出现异常，请确认机器人命令是否正确或已连机Login。";
                    return -1;
                }

                return 0;

            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"SetIOPort16 excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }
        }

         public override int MoveWorkingPos()
        {
            try
            {

            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"MoveWorkingPos excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }
            return 0;
        }

        public string GetRobotPosToString( bool bFroceRAxisToZero = false)
        {
            try
            {
                double x = -999, y = -999, z = -999, r = -999;
                RobotBase.emRobotHandType handType = RobotBase.emRobotHandType.HandType_Auto;
                GetCurrentPosition(ref x, ref y, ref z, ref r, ref handType);
                if (x < -998.000 || y < -998.000 || z < -998.000 || r < -998.000)
                {
                    strRobotLastMsg = $"得到机器人坐标位置出现错误。";
                    return null;
                }
                else
                {
                    if (bFroceRAxisToZero)
                        r = 0.00;

                    string Retun = $"{ x.ToString("F2")}, {y.ToString("F2")}, {z.ToString("F2")}, {r.ToString("F2")}";
                    if (handType == emRobotHandType.HandType_Left)
                        Retun += ", /L";
                    else
                        Retun += ", /R";
                    return Retun;
                }

            }
            catch (Exception ex)
            {
                strRobotLastMsg = $"GetRobotPos_Manual excute Error:\r\n{ex.Message}";
                throw new Exception(strRobotLastMsg);
            }
        }

        public int MoveFromPosString(string strPosString, string StrPassPos = "")
        {
            try
            {
                string szErrorMsg = "";
                double x = -999, y = -999, z = -999, r = -999;

                string pos = strPosString.Replace(" ", "");
                string[] values = pos.Split(",".ToCharArray());

                if (values.Length < 5)
                    return -1;

                x = double.Parse(values[0]);
                y = double.Parse(values[1]);
                z = double.Parse(values[2]);
                r = double.Parse(values[3]);
                if (x < -998 || y < -998 || z < -998 || r < -998)
                    return -2;

                //确定手臂姿势
                emRobotHandType emHandType = emRobotHandType.HandType_Auto;
                if (strPosString.Contains("/L"))
                    emHandType = emRobotHandType.HandType_Left;
                else if (strPosString.Contains("/R"))
                    emHandType = emRobotHandType.HandType_Right;

                //确定移动类型
                emRobotMoveType moveType = emRobotMoveType.MoveType_Jump;

                //如果为Jump, 确定Zlimit 
                double zLimit = 0.00;
                if (values.Length >= 7)
                    zLimit = double.Parse(values[7]);

                //开始移动
                if (StrPassPos == null || StrPassPos.Length <= 0)
                {
                    if (Move(x, y, z, r, true, 1, moveType, emHandType, zLimit) != 0)
                    {
                        strRobotLastMsg = $"移动机器人坐标位置出现错误(MoveManual) 当前位置: {""}, 目标位置: {""}";
                        return -3;
                    }
                }
                else
                {
                    RobotPosition endPos = RobotPosition.Parse(strPosString);
                    RobotPosition passPos = RobotPosition.Parse(StrPassPos);
                    if (Move(endPos, passPos) != 0)
                    {
                        strRobotLastMsg = $"移动机器人坐标位置出现错误(MoveManual) 当前位置: {""}, 目标位置: {""}";
                        return -3;
                    }
                }

                return 0;
            }
            catch(Exception ex)
            {
                throw ex;
            }
          
        }
    }
}
