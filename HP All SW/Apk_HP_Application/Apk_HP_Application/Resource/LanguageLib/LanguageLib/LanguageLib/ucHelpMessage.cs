﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LanguageLib
{
    public partial class ucHelpMessage : UserControl
    {
        public ucHelpMessage()
        {
            InitializeComponent();
        }

        public string Message {
            set
            {
                lMessage.Text = value;
            }
        }
    }
}
