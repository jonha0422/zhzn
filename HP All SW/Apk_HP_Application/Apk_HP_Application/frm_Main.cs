﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Apk_HP_Application.程序文件夹分类._1调度总程序;
using Apk_HP_Application.程序文件夹分类._3打码功割控制程序;
using Apk_HP_Application.程序文件夹分类._4装盘及机器人控制;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application
{
    public partial class frm_Main : DevExpress.XtraEditors.XtraForm
    {
/*        frm_AutomaticSmartLine frm_LaserPanle = new frm_AutomaticSmartLine();*/
        public frm_Main()
        {
            InitializeComponent();

            string exeFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            HPglobal.strExeFolderName = System.IO.Path.GetDirectoryName(exeFileName);
            HPglobal.frmMainWindow = this;
            labelControl4.Text = $"产品型号: {HPglobal.m_strModelName}";

            if (HPglobal.nAppFlage == 0) //调度系统
            {
                //添加主面板
                HPglobal.frm_MainPanel = new frm_AutomaticSmartLine();
                HPglobal.frm_MainPanel.TopLevel = false;
                HPglobal.frm_MainPanel.Location = new Point(0, 0);
                panelControl4.Controls.Add(HPglobal.frm_MainPanel); 
                HPglobal.frm_MainPanel.Show();
                HPglobal.frm_MainPanel.SendToBack();
/*                panelContainer1.BringToFront();*/
            }
            else if (HPglobal.nAppFlage == 1) //上料视觉软件
            {

            }
            else if (HPglobal.nAppFlage ==2 ) //打码切割软件
            {
                //添打码切割
                HPglobal.frm_LaserPanel = new frm_LaserStationMain();
                HPglobal.frm_LaserPanel.TopLevel = false;
                HPglobal.frm_LaserPanel.Location = new Point(0, 0);
                panelControl4.Controls.Add(HPglobal.frm_LaserPanel); 
                HPglobal.frm_LaserPanel.Show();
                HPglobal.frm_LaserPanel.SendToBack();

            }
            else if (HPglobal.nAppFlage ==3) //机器人下料控制软件
            {
                HPglobal.frm_RobotPanel = new frm_RobotControl();
                HPglobal.frm_RobotPanel.TopLevel = false;
                HPglobal.frm_RobotPanel.Location = new Point(0, 0);
                panelControl4.Controls.Add(HPglobal.frm_RobotPanel); 
                HPglobal.frm_RobotPanel.Show();
                HPglobal.frm_RobotPanel.SendToBack();

            }

            timeExeStatus.Enabled = true;
            timeMonitorMessage.Enabled = true;

        }

        private void panel_Mainboard_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timeExeStatus_Tick(object sender, EventArgs e)
        {
            HPglobal.ShowColorStatus(labMachineStatus, HPglobal.emMachineState);
        }

        public void UpdataInformation(object wsarg)
        {
            try
            {
                System.String strSpliter = "#";
                lableRFID.Text = $"RFID: {HPglobal.m_strRFID}";
                lableRFID.BackColor = (HPglobal.m_strRFID.Length > 0? Color.LightGreen : Color.Red);

                //监控异常说明
                if (HPglobal.m_strLastException.Length > 1)
                {
                    txtExceptionMes.Text = HPglobal.m_strLastException;
                    txtExceptionMes.BackColor = Color.FromArgb(255, 0, 0);
                }

                #region>>>>>>>>>>>>取出收到的信息
                //                 while (m_queTCPRev.Count > 0)
                //                 {
                //                     string str = m_queTCPRev.Dequeue();
                //                     if (str != null)
                //                     {
                //                         string[] strArryCommand = str.Split(strSpliter.ToCharArray());
                //                         if (strArryCommand.Count() >= 4)
                //                         {
                //                             if (dataGridReceive.Rows.Count > 2000)
                //                             {
                //                                 dataGridReceive.Rows.Clear();
                //                                 dataGridReceive.Invalidate();
                //                             }
                //                             int nRow = this.dataGridReceive.Rows.Add();
                //                             dataGridReceive.Rows[nRow].HeaderCell.Value = (nRow + 1).ToString();
                //                             this.dataGridReceive.Rows[nRow].Cells[0].Value = strArryCommand[0];
                //                             this.dataGridReceive.Rows[nRow].Cells[1].Value = strArryCommand[1];
                //                             this.dataGridReceive.Rows[nRow].Cells[2].Value = strArryCommand[2];
                //                             this.dataGridReceive.Rows[nRow].Cells[3].Value = GetInterfaceDescriptionFromID(strArryCommand[3]);
                // 
                //                             if (chkAutoScollReceive.Checked)
                //                                 dataGridReceive.CurrentCell = dataGridReceive.Rows[nRow].Cells[0];
                //                         }
                //                     }
                //                 }
                #endregion

                #region>>>>>>>>>>>>取出发出的信息
                //                 while (m_queTCPSend.Count > 0)
                //                 {
                //                     string str = m_queTCPSend.Dequeue();
                //                     if (str != null)
                //                     {
                //                         string[] strArryCommand = str.Split(strSpliter.ToCharArray());
                //                         if (strArryCommand.Count() >= 4)
                //                         {
                //                             if (dataGridSend.Rows.Count > 2000)
                //                             {
                //                                 dataGridSend.Rows.Clear();
                //                                 dataGridSend.Invalidate();
                //                             }
                // 
                //                             int nRow = this.dataGridSend.Rows.Add();
                //                             dataGridSend.Rows[nRow].HeaderCell.Value = (nRow + 1).ToString();
                //                             this.dataGridSend.Rows[nRow].Cells[0].Value = strArryCommand[0];
                //                             this.dataGridSend.Rows[nRow].Cells[1].Value = strArryCommand[1];
                //                             this.dataGridSend.Rows[nRow].Cells[2].Value = strArryCommand[2];
                //                             if (strArryCommand[2].Contains("NG"))
                //                                 this.dataGridSend.Rows[nRow].Cells[2].Style.BackColor = Color.FromArgb(255, 0, 0);
                //                             this.dataGridSend.Rows[nRow].Cells[3].Value = GetInterfaceDescriptionFromID(strArryCommand[3]);
                //                             if (chkAutoScollSend.Checked)
                //                                 dataGridSend.CurrentCell = dataGridSend.Rows[nRow].Cells[0];
                //                         }
                //                     }
                //                 }
                #endregion

                #region>>>>>>>>>>>>取出输出的信息
                while (HPglobal.m_queMessageOut.Count > 0)
                {
                    int nCount = HPglobal.m_queMessageOut.Count;
                    string str = HPglobal.m_queMessageOut.Dequeue();
                    if (str == null)
                        continue;

                    string[] strArryCommand = str.Split(strSpliter.ToCharArray());
                    if (strArryCommand.Count() < 4)
                        continue;

                    System.Windows.Forms.DataGridView gridViewOut = null;
                    if (strArryCommand[2].Contains("错误") || strArryCommand[2].Contains("失败") || strArryCommand[2].Contains("出错") || strArryCommand[2].Contains("异常"))
                        gridViewOut = dataMsgError;
                    else
                        gridViewOut = dataMsgRun;

                    if (gridViewOut.Rows.Count > 8000)
                    {
                        gridViewOut.Rows.Clear();
                        gridViewOut.Invalidate();
                    }

                    int nRow = gridViewOut.Rows.Add();
                    gridViewOut.Rows[nRow].HeaderCell.Value = (nRow + 1).ToString();
                    gridViewOut.Rows[nRow].Cells[0].Value = strArryCommand[0];
                    gridViewOut.Rows[nRow].Cells[1].Value = strArryCommand[1];
                    gridViewOut.Rows[nRow].Cells[2].Value = strArryCommand[2];

                    if (strArryCommand[2].Contains("错误") || strArryCommand[2].Contains("失败") || strArryCommand[2].Contains("出错") || strArryCommand[2].Contains("异常")) //strArryCommand[2].Contains("NG") ||
                        gridViewOut.Rows[nRow].Cells[2].Style.BackColor = Color.FromArgb(255, 0, 0);
                    else if (strArryCommand[2].Contains("注意"))
                        gridViewOut.Rows[nRow].Cells[2].Style.BackColor = Color.FromArgb(255, 255, 0);
                    else if (strArryCommand[2].Contains("成功"))
                        gridViewOut.Rows[nRow].Cells[2].Style.BackColor = Color.LightGreen;

                    /*                            this.gridViewOut.Rows[nRow].Cells[3].Value = GetInterfaceDescriptionFromID(strArryCommand[3]);*/
                    gridViewOut.CurrentCell = gridViewOut.Rows[nRow].Cells[0];
                }
                #endregion

                #region>>>>>>>>>>>>取出TcpClient
                //                 if (m_quePLC.Count > 0)
                //                 {
                //                     OmronPLC_TCP PLC = m_quePLC.Dequeue();
                //                     if (PLC != null)
                //                     {
                //                         int nRow = grild_Client.Rows.Add();
                //                         grild_Client.Rows[nRow].HeaderCell.Value = (nRow + 1).ToString();
                //                         grild_Client.Rows[nRow].Cells[0].Value = PLC.IpAddress;
                //                         grild_Client.Rows[nRow].Cells[1].Value = "已连接";
                //                         grild_Client.Rows[nRow].Cells[1].Style.BackColor = Color.FromArgb(0, 255, 0);
                //                         grild_Client.Rows[nRow].Cells[0].Tag = PLC;
                //                         grild_Client.CurrentCell = grild_Client.Rows[nRow].Cells[0];
                //                     }
                //                 }
                #endregion

                #region>>>>>>>>>>>>监控客户端的连接
                //                 bool bState = false;
                //                 bool bPreState = bState;
                //                 for (int nIndex = 0; nIndex < grild_Client.RowCount; nIndex++)
                //                 {
                //                     try
                //                     {
                //                         OmronPLC_TCP PLC = (OmronPLC_TCP)grild_Client.Rows[nIndex].Cells[0].Tag;
                //                         if (PLC != null)
                //                         {
                //                             if (!PLC.isConnected)
                //                             {
                //                                 grild_Client.Rows[nIndex].Cells[1].Style.BackColor = Color.FromArgb(255, 0, 0);
                //                                 grild_Client.Rows[nIndex].Cells[1].Value = "已断开";
                //                             }
                //                             else
                //                             {
                //                                 if ((string)grild_Client.Rows[nIndex].Cells[1].Value != "已连接")
                //                                 {
                //                                     grild_Client.Rows[nIndex].Cells[1].Style.BackColor = Color.LightGreen;
                //                                     grild_Client.Rows[nIndex].Cells[1].Value = "已连接";
                //                                 }
                //                             }
                //                         }
                //                     }
                //                     catch (Exception ex)
                //                     {
                //                         grild_Client.Rows[nIndex].Cells[1].Style.BackColor = Color.FromArgb(255, 0, 0);
                //                         grild_Client.Rows[nIndex].Cells[1].Value = "异常";
                //                         grild_Client.Rows[nIndex].Cells[0].Tag = null;
                //                     }
                // 
                //                 }

                #endregion

            }
            catch (Exception e)
            {
                HPglobal.m_strLastException = e.Message;
                txtExceptionMes.Text = HPglobal.m_strLastException;
            }
        }

        private void timeMonitorMessage_Tick(object sender, EventArgs e)
        {
            UpdataInformation(null);
        }

        private void girdControl2_StyleChanged(object sender, EventArgs e)
        {

        }

        private void frm_Main_Load(object sender, EventArgs e)
        {

        }

        private void frm_Main_Closing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("！！！确认要退出程序吗？退出程序将影响下位机的运行并且数据会丢失，后果非常严重！！！慎重！！！\n\nYes: 退出程序\nNo: 继续运行\n\n请确保下位机已经没有运行的情况下停止运行程序。",
                                                "!!!确认!!!(安全退出程序)",
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question,
                                                MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            e.Cancel = false;

            //退出程序
            HPglobal.QuitApplicationAndClear();
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            //             HPglobal.RecordeAllMessage("fdsafdsafdsafdsa", true, true, "fdsafdsafdsafdsaf");
            //             HPglobal.WorkingMessageOutput("Start Testing....\r\n", System.Drawing.Color.Red, 9, 2);

                       Apk_HP_Application.程序文件夹分类._3打码功割控制程序.frm_LaserStationMain panel = new 程序文件夹分类._3打码功割控制程序.frm_LaserStationMain();
                        panel.Show();
            //frm_MainPanel.Hide();

            //             frm_LaserPanle.Show();
            //             frm_LaserPanle.SendToBack();
            //             frm_LaserPanle.BringToFront();
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            Apk_HP_Application.程序文件夹分类._4装盘及机器人控制.frm_RobotControl panel = new 程序文件夹分类._4装盘及机器人控制.frm_RobotControl();
            panel.Show();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frm_Operator panel = new frm_Operator();
            panel.SetFlagDialog(HPglobal.nAppFlage);
            panel.ShowDialog();
        }

        private void btmNewProduct_Click(object sender, EventArgs e)
        {
            try
            {
                frm_NewProduct panel = new frm_NewProduct();
                panel.ShowDialog();
            }
            catch(Exception ex)
            {

            }
        }
    }
}
