﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application
{
    public enum emActionHandle
    {
        ActionHandle_NoDef = 0x00,
        ActionHandle_Continue = 0x01,
        ActionHandle_Redo = 0x02,
        ActionHandle_Stop = 0x03,
        ActionHandle_Abort = 0x04,
    }
    public partial class frm_ActionErrorHandle : Form
    {
        private emActionHandle nResult = 0x00;
        private object lockDisplay = new object();
        string m_Message = "";
        frm_Main  m_MainWnd;
        bool bMessagebox = false;

        public frm_ActionErrorHandle(frm_Main formPara)
        {
            try
            {
                InitializeComponent();
                m_MainWnd = formPara;

                SetStyle(ControlStyles.UserPaint, true);
                SetStyle(ControlStyles.AllPaintingInWmPaint, true); // 禁止擦除背景. 
                SetStyle(ControlStyles.DoubleBuffer, true); // 双缓冲
                SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public emActionHandle DisplayMessage(string strDispaly)
        {
            try
            {
                确定.Visible = false;
                m_Message = strDispaly;
                btmHanleSkip.Focus();
                btmHanleSkip.Select();
                nResult = 0x00;
                Show(m_MainWnd);
                BringToFront();
                while (nResult == 0x00 && !HPglobal.m_bQuitApp)
                {
                    Application.DoEvents();
                    if (HPglobal.m_bQuitApp)
                        return nResult;
                }

                return nResult;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
        public emActionHandle MessageBoxShow(string strDispaly)
        {
            try
            {
                bMessagebox = true;
                btmHanleContinue.Visible = false;
                btmHanleAbort.Visible = false;
                btmHanleSkip.Visible = false;
                btmHanleAbort.Visible = false;

                m_Message = strDispaly;
                btmHanleSkip.Focus();
                btmHanleSkip.Select();
                nResult = 0x00;
                Show(m_MainWnd);
                BringToFront();
                while (nResult == 0x00 && !HPglobal.m_bQuitApp)
                {
                    Application.DoEvents();
                    if (HPglobal.m_bQuitApp)
                        return nResult;
                }

                return nResult;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void btmHanleContinue_Click(object sender, EventArgs e)
        {
            try
            {
                nResult = emActionHandle.ActionHandle_Continue;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void btmHanleSkip_Click(object sender, EventArgs e)
        {
            try
            {
                nResult = emActionHandle.ActionHandle_Redo;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void btmHanleStop_Click(object sender, EventArgs e)
        {
            try
            {
                nResult = emActionHandle.ActionHandle_Stop;
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void btmHanleAbort_Click(object sender, EventArgs e)
        {
            try
            {
                nResult = emActionHandle.ActionHandle_Abort;
                this.Close();
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        private void frm_Closing(object sender, FormClosingEventArgs e)
        {
            try
            {
                time_display.Enabled = false;

                if (nResult == 0x00 && !HPglobal.m_bQuitApp)
                {
                    e.Cancel = true;
                    return;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void frm_Closed(object sender, FormClosedEventArgs e)
        {

        }

        private void time_display_Tick(object sender, EventArgs e)
        {
            try
            {
                if (bMessagebox)
                {
                    textBox1.Text = m_Message;
                    textBox1.BackColor = (textBox1.BackColor == Color.Red ? Color.White : Color.Red);
                }
                else
                {
                    textBox1.Text = m_Message;
                    textBox1.BackColor = (textBox1.BackColor == Color.Yellow ? Color.White : Color.Yellow);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void 确定_Click(object sender, EventArgs e)
        {
            try
            {
                nResult = emActionHandle.ActionHandle_Continue;
                this.Close();
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
    }
}
