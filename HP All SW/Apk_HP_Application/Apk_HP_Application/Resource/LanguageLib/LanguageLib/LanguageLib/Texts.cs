﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageLib
{
    public enum Texts
    {
        Select_option_below_and_then_click_Ready_button,
        Option_details,
        Ready,
        Details,
        Close_Details,
        Abort_production,
        Select_this_to_abort_production,
        Select_this_to_retry,
        Select_this_to_Ignore,
        Select_this_to_continue,
        Yes,
        No,
        OK,
        Cancel,
        Retry,
        Ignore,
        Continue,
        Waiting_for_enable,
        Waiting_for_homing,
        Waiting_for_enter_production,
        Entering_Production,
        Homing,
        Home,
        Stopped,
        Waiting_for_start,
        Yes_tooltip,
        No_tooltip,
        Ok_tooltip,
        Cancel_tooltip,
        Disable,
        Disabled,
        Enable,
        Enabled,
        Max_allowed_step_10,
        Error_executing_frames,
        Command_Execution_Failed,
        Signal_waiting_timeout,
        Signal_not_defined,
        Offset,
        Repeat_X_Count,
        Repeat_Y_Count,
        Repeat_X_Offset,
        Repeat_Y_Offset,
        Add_or_modify_users,
        Add_or_modify_users_Admin_login,
        User_logging_error,
        Password_not_match,
        No_user_level_selected,
        Empty_passwords_not_allowed,
        Admin_cannot_be_deleted,
        Operator_cannot_be_deleted,
        notNumberUseSeparator,
        notIntegerNumber,
        notDecimalNumber,
        valueOutOfLimits,
        textIsTooShort,
        textIsTooLong,
        MachineNotIdle,
        ErrorExecutingCommand,
        OkToRunProgramUsingCamera,
        StartFromBeginOfProgram,
        StartFromCurrentCommand,
        UseCameraQ,
        MoveToCameraPositionQ,
        MoveToPositionQ,
        MoveZDownQ,
        ExecuteFramesQ,
        OkToDeleteSelectedCommandQ,
        EStopPressed,
        EStopReleased,
        TeachingMode,
        EnterProduction,
        WaitingForStart,
        ExitProduction,
        Running,
        Stopping,
        PowerKeyIsOff,
        OkToHomeMachineQ,
        CloseDoorsTurnKeysToAuto,
        OKToEnterProductionQ,
        SaveprogramQ,
        ErrorExecutingVisionTool,
        PixelSize,
        GiveProgramName,
        ProgramAlreadyExists,
        DoorsMustBeClosed,
        OpeningFailed,
        ProductNameAlreadyExistOwerwriteQ,
        SaveWithNewName,
        SavingFailed,
        CanNotDeleteActiveProduct,
        Open,
        Create,
        Delete,
        ProductNameAlreadyInUse,
        MustBeOn,
        MustBeOff,
        Program,
        has_been_modified,
        Error_deleting_program,
        Ok_to_delete_all_marked_commandsQ,
        DoorsMode,
        DoorsModeRequested,
        ReleaseDoorsModeButtonToContinue,
        Old_Offset_equals,
        New_Offset_equals,
        Save_new_values_Q,
        Update_camera_cross_hair_diameter_for_current_bit_Q,
        Old_Upper_Robot_Offset_equals,
        New_Upper_Robot_Offset_equals,
        Teach_drill_position,
        Press_next_to_teach_position_where_router_can_drill_a_hole,
        Drill_hole,
        Press_next_to_drill_a_hole,
        Teach_camera_position,
        Press_next_to_teach_drilled_hole_with_camera,
        Teach_upper_robot_camera_position,
        Press_next_to_teach_drilled_hole_with_upper_robot_camera,
        Continue_to_drill_a_hole_at_current_position_Q,
        Spindle_does_not_start,
        Move_Z_Q,
        Disable_Gripper_Q,
        Move_failed,
        Move_to_safe_Z_failed,
        Collision_detected,
        Homing_failed,
        Vision_failed,
        Sensor_failed,
        Transfer_failed,
        Feeder_not_ready,
        Move_break_failed,
        Parameter_error,
        Pin_adjust_failed,
        Gripper_move_failed,
        Select_ok_for_next_step,
        No_component_detected,
        Close_teach_dialog_before_continuing,
        Statistics,
        Offline_programming,
        Log,
        Functions,
        Cell,
        Product,
        Main,
        Diagnostics,
        Tools,
        Upper_camera,
        Lower_camera,
        BackupRestore,
        Feeder,
        SaveAs,
        Doors_mode_active
    }

    public partial class Language
    {
        public void SetDefaultTexts()
        {
            Language l = Instance;
            l.SetText(Texts.Select_option_below_and_then_click_Ready_button.ToString(), "Select option below and then click Ready button");
            l.SetText(Texts.Option_details.ToString(), "Option details");
            l.SetText(Texts.Ready.ToString(), "Ready");
            l.SetText(Texts.Details.ToString(), "Details");
            l.SetText(Texts.Close_Details.ToString(), "Close Details");
            l.SetText(Texts.Abort_production.ToString(), "Abort production");
            l.SetText(Texts.Select_this_to_abort_production.ToString(), "Select this to abort production");
            l.SetText(Texts.Select_this_to_retry.ToString(), "Select this to retry");
            l.SetText(Texts.Select_this_to_Ignore.ToString(), "Select this to Ignore");
            l.SetText(Texts.Select_this_to_continue.ToString(), "Select this to continue");
            l.SetText(Texts.Cancel.ToString(), "Cancel");
            l.SetText(Texts.Yes.ToString(), "Yes");
            l.SetText(Texts.No.ToString(), "No");
            l.SetText(Texts.OK.ToString(), "OK");
            l.SetText(Texts.Retry.ToString(), "Retry");
            l.SetText(Texts.Ignore.ToString(), "Ignore");
            l.SetText(Texts.Continue.ToString(), "Continue");
            l.SetText(Texts.Waiting_for_enable.ToString(), "Waiting for enable");
            l.SetText(Texts.Waiting_for_homing.ToString(), "Waiting for homing");
            l.SetText(Texts.Waiting_for_enter_production.ToString(), "Waiting for enter production");
            l.SetText(Texts.Entering_Production.ToString(), "Entering Production");
            l.SetText(Texts.Homing.ToString(), "Homing");
            l.SetText(Texts.Home.ToString(), "Home");
            l.SetText(Texts.Stopped.ToString(), "Stopped");
            l.SetText(Texts.Waiting_for_start.ToString(), "Waiting for start");
            l.SetText(Texts.Yes_tooltip.ToString(), "Yes Selected");
            l.SetText(Texts.No_tooltip.ToString(), "No Selected");
            l.SetText(Texts.Ok_tooltip.ToString(), "Ok Selected");
            l.SetText(Texts.Cancel_tooltip.ToString(), "Cancel Selected");
            l.SetText(Texts.Enable.ToString(), "Enable");
            l.SetText(Texts.Enabled.ToString(), "Enabled");
            l.SetText(Texts.Disable.ToString(), "Disable");
            l.SetText(Texts.Disabled.ToString(), "Disabled");
            l.SetText(Texts.Max_allowed_step_10.ToString(), "Max allowed step = 10");
            l.SetText(Texts.Error_executing_frames.ToString(), "Error executing frames");
            l.SetText(Texts.Command_Execution_Failed.ToString(), "Command Execution Failed");
            l.SetText(Texts.Signal_waiting_timeout.ToString(), "Signal waiting timeout ");
            l.SetText(Texts.Signal_not_defined.ToString(), "Signal not defined");
            l.SetText(Texts.Offset.ToString(), "Offset");
            l.SetText(Texts.Repeat_X_Count.ToString(), "Repeat X Count");
            l.SetText(Texts.Repeat_Y_Count.ToString(), "Repeat Y Count");
            l.SetText(Texts.Repeat_X_Offset.ToString(), "Repeat X Offset");
            l.SetText(Texts.Repeat_Y_Offset.ToString(), "Repeat Y Offset");
            l.SetText(Texts.Add_or_modify_users.ToString(),"Add or modify users");
            l.SetText(Texts.Add_or_modify_users_Admin_login.ToString(),"Add or modify users (first Login as admin)");
            l.SetText(Texts.User_logging_error.ToString(),"Error Logging In User");
            l.SetText(Texts.Password_not_match.ToString(),"Passwords are not matching");
            l.SetText(Texts.No_user_level_selected.ToString(),"No user level selected");
            l.SetText(Texts.Empty_passwords_not_allowed.ToString(),"No empty password for admin allowed");
            l.SetText(Texts.Admin_cannot_be_deleted.ToString(),"admin cannot be deleted");
            l.SetText(Texts.Operator_cannot_be_deleted.ToString(),"Operator cannot be deleted");
            l.SetText(Texts.notNumberUseSeparator.ToString(),"Not a number, Use dot (.) as decimal separator");
            l.SetText(Texts.notIntegerNumber.ToString(),"Not an integer number");
            l.SetText(Texts.notDecimalNumber.ToString(),"Not a decimal number");
            l.SetText(Texts.valueOutOfLimits.ToString(),"Value out of limits");
            l.SetText(Texts.textIsTooShort.ToString(), "Text is too short");
            l.SetText(Texts.textIsTooLong.ToString(), "Text is too long");
            l.SetText(Texts.MachineNotIdle.ToString(), "Machine not idle");
            l.SetText(Texts.ErrorExecutingCommand.ToString(), "Error executing command");
            l.SetText(Texts.OkToRunProgramUsingCamera.ToString(), "Ok to run program using camera?");
            l.SetText(Texts.StartFromBeginOfProgram.ToString(), "Start From Begin Of Program");
            l.SetText(Texts.StartFromCurrentCommand.ToString(), "Start From Current Command");
            l.SetText(Texts.UseCameraQ.ToString(), "Use Camera?");
            l.SetText(Texts.MoveToCameraPositionQ.ToString(), "Move to camera position?");
            l.SetText(Texts.MoveToPositionQ.ToString(), "Move to position?");
            l.SetText(Texts.MoveZDownQ.ToString(), "Move Z Down?");
            l.SetText(Texts.ExecuteFramesQ.ToString(), "Execute Frames?");
            l.SetText(Texts.OkToDeleteSelectedCommandQ.ToString(), "Ok to delete selected command?");
            l.SetText(Texts.EStopPressed.ToString(), "E-Stop Pressed");
            l.SetText(Texts.EStopReleased.ToString(), "E-Stop Released");
            l.SetText(Texts.TeachingMode.ToString(), "Teaching mode");
            l.SetText(Texts.EnterProduction.ToString(), "Enter Production");
            l.SetText(Texts.WaitingForStart.ToString(), "Waiting For Start");
            l.SetText(Texts.ExitProduction.ToString(), "Exit Production");
            l.SetText(Texts.Running.ToString(), "Running");
            l.SetText(Texts.Stopping.ToString(), "Stopping");
            l.SetText(Texts.PowerKeyIsOff.ToString(), "POWER key is OFF");
            l.SetText(Texts.OkToHomeMachineQ.ToString(), "OK To Home Machine?");
            l.SetText(Texts.CloseDoorsTurnKeysToAuto.ToString(), "Close doors, turn key to auto and select continue");
            l.SetText(Texts.OKToEnterProductionQ.ToString(), "OK To Enter Production?");
            l.SetText(Texts.SaveprogramQ.ToString(), "Save program?");
            l.SetText(Texts.ErrorExecutingVisionTool.ToString(), "Error executing vision tool");
            l.SetText(Texts.PixelSize.ToString(), "Pixel size");
            l.SetText(Texts.GiveProgramName.ToString(), "Give program name");
            l.SetText(Texts.ProgramAlreadyExists.ToString(), "Program already exists");
            l.SetText(Texts.DoorsMustBeClosed.ToString(), "Doors must be closed");
            l.SetText(Texts.OpeningFailed.ToString(), "Opening failed");
            l.SetText(Texts.ProductNameAlreadyExistOwerwriteQ.ToString(), "Product name already exist. Owerwrite?");
            l.SetText(Texts.SaveWithNewName.ToString(), "Save with new name");
            l.SetText(Texts.SavingFailed.ToString(), "Saving failed");
            l.SetText(Texts.CanNotDeleteActiveProduct.ToString(), "Can not delete active product");
            l.SetText(Texts.Open.ToString(), "Open");
            l.SetText(Texts.Create.ToString(), "Create");
            l.SetText(Texts.Delete.ToString(), "Delete");
            l.SetText(Texts.ProductNameAlreadyInUse.ToString(), "Product name already in use");
            l.SetText(Texts.MustBeOn.ToString(), "Must Be On");
            l.SetText(Texts.MustBeOff.ToString(), "Must Be Off");
            l.SetText(Texts.Program.ToString(), "Program");
            l.SetText(Texts.has_been_modified.ToString(), "has been modified");
            l.SetText(Texts.Error_deleting_program.ToString(), "Error deleting program");
            l.SetText(Texts.Ok_to_delete_all_marked_commandsQ.ToString(), "Ok to delete all marked commands?");
            l.SetText(Texts.DoorsMode.ToString(), "Doors Mode");
            l.SetText(Texts.DoorsModeRequested.ToString(), "Doors mode requested");
            l.SetText(Texts.ReleaseDoorsModeButtonToContinue.ToString(), "Release doors mode button to continue");
            l.SetText(Texts.Old_Offset_equals.ToString(), "Old Offset = ");
            l.SetText(Texts.New_Offset_equals.ToString(), "New Offset = ");
            l.SetText(Texts.Save_new_values_Q.ToString(), "Save new values?");
            l.SetText(Texts.Update_camera_cross_hair_diameter_for_current_bit_Q.ToString(), "Update camera cross hair diameter for current bit?");
            l.SetText(Texts.Old_Upper_Robot_Offset_equals.ToString(), "Old Upper Robot Offset = ");
            l.SetText(Texts.New_Upper_Robot_Offset_equals.ToString(), "New Upper Robot Offset = ");
            l.SetText(Texts.Teach_drill_position.ToString(), "Teach drill position");
            l.SetText(Texts.Press_next_to_teach_position_where_router_can_drill_a_hole.ToString(), "Press next to teach position where router can drill a hole");
            l.SetText(Texts.Drill_hole.ToString(), "Drill hole");
            l.SetText(Texts.Press_next_to_drill_a_hole.ToString(), "Press next to drill a hole");
            l.SetText(Texts.Teach_camera_position.ToString(), "Teach camera position");
            l.SetText(Texts.Press_next_to_teach_drilled_hole_with_camera.ToString(), "Press next to teach drilled hole with camera");
            l.SetText(Texts.Teach_upper_robot_camera_position.ToString(), "Teach upper robot camera position");
            l.SetText(Texts.Press_next_to_teach_drilled_hole_with_upper_robot_camera.ToString(), "Press next to teach drilled hole with upper robot camera");
            l.SetText(Texts.Continue_to_drill_a_hole_at_current_position_Q.ToString(), "Continue to drill a hole at current position?");
            l.SetText(Texts.Spindle_does_not_start.ToString(), "Spindle does not start");
            l.SetText(Texts.Move_Z_Q.ToString(), "Move Z?");
            l.SetText(Texts.Disable_Gripper_Q.ToString(), "Disable Gripper?");
            l.SetText(Texts.Move_failed.ToString(), "Move failed");
            l.SetText(Texts.Move_to_safe_Z_failed.ToString(), "Move to safe Z failed");
            l.SetText(Texts.Collision_detected.ToString(), "Collision detected");
            l.SetText(Texts.Homing_failed.ToString(), "Homing failed");
            l.SetText(Texts.Vision_failed.ToString(), "Vision failed");
            l.SetText(Texts.Sensor_failed.ToString(), "Sensor failed");
            l.SetText(Texts.Transfer_failed.ToString(), "Transfer failed");
            l.SetText(Texts.Feeder_not_ready.ToString(), "Feeder not ready");
            l.SetText(Texts.Move_break_failed.ToString(), "Move break failed");
            l.SetText(Texts.Parameter_error.ToString(), "Parameter error");
            l.SetText(Texts.Pin_adjust_failed.ToString(), "Pin adjust failed");
            l.SetText(Texts.Gripper_move_failed.ToString(), "Gripper move failed");
            l.SetText(Texts.Select_ok_for_next_step.ToString(), "Select ok for next step");
            l.SetText(Texts.No_component_detected.ToString(), "No component detected");
            l.SetText(Texts.Close_teach_dialog_before_continuing.ToString(), "Close teach dialog before continuing");
            l.SetText(Texts.Statistics.ToString(), "Statistics");
            l.SetText(Texts.Offline_programming.ToString(), "Offline programming");
            l.SetText(Texts.Log.ToString(), "Log");
            l.SetText(Texts.Functions.ToString(), "Functions");
            l.SetText(Texts.Cell.ToString(), "Cell");
            l.SetText(Texts.Product.ToString(), "Product");
            l.SetText(Texts.Main.ToString(), "Main");
            l.SetText(Texts.Diagnostics.ToString(), "Diagnostics");
            l.SetText(Texts.Tools.ToString(), "Tools");
            l.SetText(Texts.Upper_camera.ToString(), "Upper camera");
            l.SetText(Texts.Lower_camera.ToString(), "Lower camera");
            l.SetText(Texts.BackupRestore.ToString(), "Backup / Restore");
            l.SetText(Texts.Feeder.ToString(), "Feeder");
            l.SetText(Texts.SaveAs.ToString(), "Save as");
            l.SetText(Texts.Doors_mode_active.ToString(), "Doors mode active");
        }
    }
}
