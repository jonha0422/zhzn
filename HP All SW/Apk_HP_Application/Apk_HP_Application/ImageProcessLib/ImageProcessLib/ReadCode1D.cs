﻿using HalconDotNet;
using InteractiveROI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionLib;
using VisionLib.ImageProcess;

namespace ImageProcessLib
{
    public class ReadCode1D : ImageProcessBase
    {
        private HTuple BarCodeHandle = null;
        [Category("识别参数")]
        [DisplayName("条码类型")]
        public string symbolType { get; set; }

        [Category("识别参数")]
        [DisplayName("超时时间")]
        public int timeout { get; set; }


        public ReadCode1D()
        {
            cName = "二维码读取";
            symbolType = "";
            timeout = 500;

        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            if (ModelRoi == null || ModelRoi.Count == 0)
            {
                return -1;
            }
            ROIRectangle2 MROI = ModelRoi[0].Copy();
            if (BarCodeHandle != null)
            {
                HOperatorSet.ClearBarCodeModel(BarCodeHandle);
            }
            HOperatorSet.CreateBarCodeModel(new HTuple(), new HTuple(), out BarCodeHandle);
            int suc = ReadCode(TempImage, MROI, false);
            return 0;
        }
        public int ReadCode(HObject aImage,ROIRectangle2 MROI,bool isTemp=true)
        {
            try
            {
                if (ModelRoi == null || ModelRoi.Count == 0)
                {
                    return -1;
                }
                HTuple channels = 0;
                HOperatorSet.CountChannels(aImage, out channels);
                HObject grayObject = null;
                if (channels == 3)
                {

                    HOperatorSet.Rgb1ToGray(aImage, out grayObject);
                }
                else
                {
                    grayObject = aImage;
                }

                HRegion hReg = MROI.getRegion();
                HOperatorSet.ReduceDomain(grayObject, hReg, out grayObject);

                HObject SymbolRegions;
                HTuple  DecodedDataStrings;
                HOperatorSet.FindBarCode(grayObject, out SymbolRegions, BarCodeHandle, "auto", out DecodedDataStrings);
                Result aRs = new Result();
                if ( DecodedDataStrings.S != "")
                {
                    aRs.Str = DecodedDataStrings[0].S;
                    aRs.isOk = true;
                    ResultSta = true;
                    if (ViewPort != null)
                    {
                        HalconWind nHW = new HalconWind();
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                        nHW.disp_message(ViewPort.HalconWindow, DecodedDataStrings.S, "image", MROI.MidR - MROI.Length1, MROI.MidC - MROI.Length2, "green", "false");
                    }

                }
                else
                {
                    aRs.Str = "";
                    aRs.isOk = false;
                    ResultSta = false;
                }
                mResult.Add(aRs);
                return 0;
            }
            catch
            {
                return -1;
            }
            
        }
        public override int ImageProcess()
        {
            try
            {
                ResultSta = true;
                mResult.Clear();
                if (SrcObject == null)
                {
                    return 0;
                }

                if (ModelRoi == null || ModelRoi.Count == 0)
                {
                    return -1;
                }
                ROIRectangle2 MROI = ModelRoi[0].Copy();
                if (VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult.Count == 1)
                {
                    MROI.MidC += VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].cX;
                    MROI.MidR += VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].cY;
                    MROI.Phi -= VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].Phi;
                }
                else
                {
                    return -1;
                }
                int suc = ReadCode(SrcObject, MROI, false);
                return suc;
            }
            catch (Exception e)
            {
                LogLib.Log.ShowWarning("图像处理出错：" + e.Message, true, "Warning", "ReadCode2D");
                return -1;
            }
            
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            ReadCode1D aFun = CoverLib.JsonCover.DeStrLSerialize<ReadCode1D>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<ReadCode1D>(this);
            return aFunPara;
        }
    }
}
