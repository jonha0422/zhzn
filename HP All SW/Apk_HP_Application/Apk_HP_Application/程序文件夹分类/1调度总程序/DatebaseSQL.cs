﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using Apk_HP_Application.程序文件夹分类._0全局全量;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public class DatebaseSQL
    {
        #region >>>>>MySQL 设定
        public SqlConnection mySQLConn = null;
        public string m_strMySQL_IP = "";
        public string m_strMySQL_Port = "";
        public string m_strMySQL_User = "";
        public string m_strMySQL_PWS = "";
        #endregion

        //连接数据库
        public bool MySqlBaking_Connect(string strHost, string strPort, string strUser, string strPSW)
        {
            try
            {
                if (mySQLConn != null && mySQLConn.State.ToString() == "Open")
                    return true;

                mySQLConn = null;
                mySQLConn = new SqlConnection();
                int nTimerout = mySQLConn.ConnectionTimeout;
                //MySqlSslMode.None
                //Provider=SQLNCLI11.1;Persist Security Info=False;User ID=sa;Initial Catalog=HPDatabase;Data Source=192.168.1.100;Initial File Name="";Server SPN=""
                //MySQL: "Data Source={0};Database={1};User ID={2};Password={3};SslMode=None"
                mySQLConn.ConnectionString = string.Format("Data Source={0};Database={1};User ID={2};Password={3}",
                                                      strHost, "HPDatabase", strUser, strPSW);
                mySQLConn.Open();

                if (mySQLConn.State.ToString() != "Open")
                {
                    mySQLConn = null;
                    return false;
                }
                else
                {
                    //得到数据数量，如果超过80000行，就清除前面40000行
/*                    SqlCommand m_dbCommand = mySQLConn.CreateCommand(); */

                    //建立SFC绑定数据表
//                     string createSfctable = "CREATE TABLE IF NOT EXISTS sfc_table(Number INTEGER,Sfc VARCHAR(32),TrayID VARCHAR(16),Position INTEGER,BindTime VARCHAR(32),UnBindTime VARCHAR(32),PRIMARY KEY(Sfc))";
//                     FileStream existFile = new FileStream("createDatabase.txt", FileMode.Open);
//                     existFile.Seek(0, SeekOrigin.Begin);
//                     byte[] data = new byte[12000];
//                     existFile.Read(data, 0, 12000 - 1 );
//                     m_dbCommand.CommandText = System.Text.Encoding.ASCII.GetString(data);
//                     m_dbCommand.CommandText = m_dbCommand.CommandText.Replace("go", "");
//                     int Cols = m_dbCommand.ExecuteNonQuery();

//                     //建立托盘绑定数据表
//                     createSfctable = "CREATE TABLE IF NOT EXISTS TrayID_table(Number INTEGER,Resource VARCHAR(16)," +
//                         "SfcSample VARCHAR(32),ProcessLot1 VARCHAR(16),ProcessLot2 VARCHAR(16),ProcessLot3 VARCHAR(16)," +
//                         "BindTime VARCHAR(32), UnBindTime VARCHAR(32),IsBind INTEGER,IsUnBind INTEGER,CompletedTime VARCHAR(32),VacuuFlag VARCHAR(5),TempFlag VARCHAR(5)," +
//                         "BKVACM VARCHAR(20),BKTMP VARCHAR(20),BKMINTMPVACM VARCHAR(20),BKMAXTMPVACM VARCHAR(20),BKSTARTTIME VARCHAR(20),BKTIME VARCHAR(20)," +
//                         "PRIMARY KEY(`Number`, `Resource`))";
//                     m_dbCommand.CommandText = createSfctable;
//                     Cols = m_dbCommand.ExecuteNonQuery();
// 
//                     createSfctable = "ALTER TABLE `baking`.`trayid_table` CHANGE COLUMN `Number` `Number` INT(11) NOT NULL AUTO_INCREMENT";
//                     m_dbCommand.CommandText = createSfctable;
//                     Cols = m_dbCommand.ExecuteNonQuery();
// 
//                     //建立历史托盘绑定数据标(包含水含量的值)
//                     //建立托盘绑定数据表
//                     createSfctable = "CREATE TABLE IF NOT EXISTS TrayID_table_History(Number INTEGER,Resource VARCHAR(16)," +
//                         "SfcSample VARCHAR(32),ProcessLot1 VARCHAR(16),ProcessLot2 VARCHAR(16),ProcessLot3 VARCHAR(16)," +
//                         "BindTime VARCHAR(32), UnBindTime VARCHAR(32),IsBind INTEGER,IsUnBind INTEGER,CompletedTime VARCHAR(32),VacuuFlag VARCHAR(5),TempFlag VARCHAR(5)," +
//                         "BKVACM VARCHAR(20),BKTMP VARCHAR(20),BKMINTMPVACM VARCHAR(20),BKMAXTMPVACM VARCHAR(20),BKSTARTTIME VARCHAR(20),BKTIME VARCHAR(20)," +
//                         "WaterTestValue VARCHAR(20),TestInstrument VARCHAR(20)," +
//                         "PRIMARY KEY(`Number`))";
//                     m_dbCommand.CommandText = createSfctable;
//                     Cols = m_dbCommand.ExecuteNonQuery();
// 
//                     createSfctable = "ALTER TABLE `baking`.`trayid_table` CHANGE COLUMN `Number` `Number` INT(11) NOT NULL AUTO_INCREMENT";
//                     m_dbCommand.CommandText = createSfctable;
//                     Cols = m_dbCommand.ExecuteNonQuery();

                    return true;
                }

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("MySqlBaking_Connect() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "确认MySQL数据库是否配制正常！");
                return false;
            }
        }

        //关闭数据库
        public void MySqlBaking_Close()
        {
            try
            {
                if (mySQLConn != null && mySQLConn.State.ToString() == "Open")
                    mySQLConn.Close();

                mySQLConn = null;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("btnTest_Click() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "确认MySQL数据库是否配制正常！");
            }
        }
    }
}
