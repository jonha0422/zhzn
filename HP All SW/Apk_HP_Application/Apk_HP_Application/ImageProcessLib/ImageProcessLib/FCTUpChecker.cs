﻿using HalconDotNet;
using InteractiveROI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionLib;
using VisionLib.ImageProcess;

namespace ImageProcessLib
{
    public class FCTUpChecker : ImageProcessBase
    {
        [Browsable(true)]
        [Category("灰度")]
        [DisplayName("最小灰度")]
       // [ReadOnly(false)]
        //[DefaultValue(false)]

        public double MinGray { get; set; }//灰度值 ---HSV
        [Category("灰度")]
        [DisplayName("最大灰度")]
        public double MaxGray { get; set; }//灰度值 ---HSV

        [Category("面积")]
        [DisplayName("最小面积")]
        public double MinArea { get; set; }//
        [Category("面积")]
        [DisplayName("最大面积")]
        public double MaxArea { get; set; }//

        [Category("圆")]
        [DisplayName("开运算半径")]
        [DefaultValue(3)]
        public double OpenRadius { get; set; }//
        [Category("圆")]
        [DisplayName("圆度")]
        public double Circularity { get; set; }//

        [Category("显示图像")]
        [DisplayName("二值化结果")]
        public bool isShowThreshold { get; set; }//


        [Category("分割方式")]
        [DisplayName("行数")]
        public int SplitRowNum { get; set; }//
        [Category("分割方式")]
        [DisplayName("列数")]
        public int SplitCowNum { get; set; }//
        [Category("分割方式")]
        [DisplayName("开始角")]
        public int StartFrom { get; set; }//

        public double ColInterval { get; set; }//
        public int BlobNumber{ get; set; }
        public FCTUpChecker()
        {
            cName = "FCT上相机检查算法";
            OpenRadius = 1;
            Circularity = 0.5;
            SplitCowNum = 10;
            SplitRowNum = 10;

        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            if (ModelRoi == null || ModelRoi.Count == 0)
            {
                return -1;
            }
            ROIRectangle2 MROI = ModelRoi[0].Copy();
            int suc = getSplitColor(TempImage,MROI);
            return 0;
        }
        public int getSplitColor(HObject aImage,ROIRectangle2 MROI,bool isTemp=true)
        {
            if (ModelRoi==null || ModelRoi.Count == 0)
            {
                return -1;
            }
            HTuple channels = 0;
            HOperatorSet.CountChannels(aImage, out channels);
            HObject grayObject = null;
            if (channels == 3)
            {

                HOperatorSet.Rgb1ToGray(aImage, out grayObject);
            }
            else
            {
                grayObject = aImage;
            }

            HRegion hReg = MROI.getRegion();
            double []Rows=MROI.Rows;
            double[] Cols = MROI.Cols;
            double MinC = MROI.MidC;
            double MidR = MROI.MidR;
            
            double RLength = MROI.Length2 * 2;
            double CLength = MROI.Length1 * 2;
            double Phi = MROI.Phi;


            double RLength1 = Rows[3] - Rows[0];
            double CLength1 = Cols[1] - Cols[0];
            int colSpit = SplitCowNum;
            int rowSpit = SplitRowNum;
            double RowLength = RLength / rowSpit/2;
            double ColLength = CLength / colSpit/2;

            double RowStart = MidR - (RLength) / 2 + RowLength;
            double ColStart = MinC - (CLength) / 2 + ColLength;
            //double RowStart = Rows[0] + RowLength;// MidR - (Length2 - RowLength);
            //double ColStart = Cols[0] + ColLength;// MinC - (Length1 - ColLength);
            HTuple hv_HomMat2D, hRowStart, hColStart;
            HTuple hv_HomMat2DIdentity = new HTuple();
            HOperatorSet.HomMat2dIdentity(out hv_HomMat2DIdentity);
            //HOperatorSet.VectorAngleToRigid(MidR, MinC, 0, MidR, MinC, -Phi, out hv_HomMat2D);
            HOperatorSet.HomMat2dRotate(hv_HomMat2DIdentity, Phi, MinC, MidR, out hv_HomMat2D);
            int id = 0;
            HalconWind nHW = new HalconWind();
            for (int row = 0; row < rowSpit; row++)
            {
                //for (int col = colSpit - 1; col >= 0; col--)
                for (int col = 0; col < colSpit; col++)
                {
                    int r = row;
                    if (StartFrom == 3 || StartFrom == 4)//从左下角或右下角开始
                    {
                        r = rowSpit - row - 1;
                    }
                    int c = col;
                    if (StartFrom == 2 || StartFrom == 4)
                    {
                        c = colSpit - col - 1;
                    }
                    HRegion aReg = new HRegion();

                    double nRow=RowStart + RowLength * r*2;
                    double nCol=ColStart + ColLength * c*2;
                    HOperatorSet.AffineTransPoint2d(hv_HomMat2D, nCol, nRow, out hColStart, out hRowStart);
                    aReg.GenRectangle2(hRowStart.D, hColStart.D, -Phi, ColLength, RowLength);
                    if (ViewPort != null)
                    {
                        HOperatorSet.SetDraw(ViewPort.HalconWindow, "margin");
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                        HOperatorSet.DispRegion(aReg, ViewPort.HalconWindow);
                    }

                    HObject ho_ImageReduced, ThresholdObj, Balls, SingleBalls, IntermediateBalls;
                    HOperatorSet.ReduceDomain(grayObject, aReg, out  ho_ImageReduced);
                    HOperatorSet.Threshold(ho_ImageReduced, out ThresholdObj, MinGray, MaxGray);
                    HOperatorSet.OpeningCircle(ThresholdObj, out  Balls, OpenRadius);
                   // HOperatorSet.OpeningCircle(Balls, out  Balls, OpenRadius);
                    if (ViewPort != null && isShowThreshold==true)
                    {
                        //HOperatorSet.cl
                       // HOperatorSet.SetDraw(ViewPort.HalconWindow, "fill");
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "blue");
                        HOperatorSet.DispObj(ThresholdObj, ViewPort.HalconWindow);
                    }
                    HOperatorSet.Connection(Balls, out SingleBalls);
                    HOperatorSet.SelectShape(SingleBalls, out IntermediateBalls, "circularity", "and", Circularity, 1.0);
                    HOperatorSet.SelectShape(IntermediateBalls, out IntermediateBalls, "area", "and", MinArea, MaxArea);
                    HTuple BallNum = 0;
                    HOperatorSet.CountObj(IntermediateBalls, out BallNum);
                    if (isTemp == false)
                    {
                        Result aR = new Result();
                        if (BallNum == 0)
                        {
                            aR.isOk = false;
                        }
                        else
                        {
                            aR.isOk = true;
                        }
                        mResult.Add(aR);

                    }
                    id++;
                    if (ViewPort != null)
                    {
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                        HOperatorSet.DispObj(IntermediateBalls, ViewPort.HalconWindow);
                        if (BallNum == 0)
                        {
                            nHW.disp_message(ViewPort.HalconWindow, "NG", "image", nRow, nCol, "red", "false");
                        }
                        else
                        {
                            nHW.disp_message(ViewPort.HalconWindow, id.ToString(), "image", nRow, nCol, "green", "false");
                        }
                        

                    }
                    else
                    {
                      //  nHW.disp_message(ViewPort.HalconWindow, id.ToString(), "image", nRow, nCol, "red", "false");
                    }
                }
            }
            double R = hReg.Row.D;

            
            
            //   select_shape (SingleBalls, IntermediateBalls, 'circularity', 'and', 0.85, 1.0);
            return 0;
        }
        public override int ImageProcess()
        {
            try
            {
                ResultSta = true;
                mResult.Clear();
                if (SrcObject == null)
                {
                    return 0;
                }

                if (ModelRoi == null || ModelRoi.Count == 0)
                {
                    return -1;
                }
                ROIRectangle2 MROI = ModelRoi[0].Copy();
                if (VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult.Count == 1)
                {
                    MROI.MidC += VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].cX;
                    MROI.MidR += VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].cY;
                    MROI.Phi -= VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].Phi;
                }
                else
                {
                    return -1;
                }
                int suc = getSplitColor(SrcObject, MROI, false);
                return suc;
            }
            catch (Exception e)
            {
                LogLib.Log.ShowWarning("图像处理出错：" + e.Message, true, "Warning", "BlobSplitAnalysis");
                return -1;
            }
            
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            FCTUpChecker aFun = CoverLib.JsonCover.DeStrLSerialize<FCTUpChecker>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<FCTUpChecker>(this);
            return aFunPara;
        }
    }
}
