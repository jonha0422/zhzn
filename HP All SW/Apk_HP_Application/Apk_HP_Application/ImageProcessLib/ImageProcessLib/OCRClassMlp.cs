﻿using HalconDotNet;
using InteractiveROI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionLib;
using VisionLib.ImageProcess;

namespace ImageProcessLib
{
    public class OCRClassMlp : ImageProcessBase
    {
        [Browsable(true)]
        [Category("OCR参数")]
        [DisplayName("字体模板")]
        public string OcrFileName { get; set; }
        [Browsable(true)]
        [Category("OCR参数")]
        [DisplayName("最小字符高度")]
        public int min_char_height { get; set; }
        [Category("OCR参数")]
        [DisplayName("最大行数")]
        public int max_line_num { get; set; }
        [Browsable(true)]
        [Category("OCR参数")]
        [DisplayName("字符内容")]
        public string OCRString { get; set; }
        [Browsable(true)]
        [Category("OCR参数")]
        [DisplayName("字符格式")]
        public string Expression { get; set; }

        [Category("OCR参数")]
        [DisplayName("字符宽度")]
        public int char_width { get; set; }
        [Category("OCR参数")]
        [DisplayName("字符高度")]
        public int char_height { get; set; }
        [Category("OCR参数")]
        [DisplayName("is_dotprint")]
        public bool is_dotprint { get; set; }


        private HTuple OCRHandle = null;
        private HTuple TextModel = null;
        public OCRClassMlp()
        {
            cName = "OCR识别";
            OcrFileName = "Industrial_Rej";
            min_char_height = 20;
            OCRString = "";
            char_width = 0;
            char_height = 0;
            is_dotprint = false;
            max_line_num = 0;
            Expression = "(FLEXID[0-9][A-Z][0-9]{3}[A-F0-9]{4})";
        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            if (TempImage == null)
            {
                return 0;
            }
            if (ModelRoi == null || ModelRoi.Count == 0)
            {
                return -1;
            }
            if (OCRHandle != null)
            {
                HOperatorSet.ClearOcrClassMlp(OCRHandle); 
            }
            // read_ocr_class_mlp
            HOperatorSet.ReadOcrClassMlp(OcrFileName, out OCRHandle);
            // HOperatorSet.ReadOcrClassMlp("DotPrint", out OCRHandle);
            if (TextModel != null)
            {
                HOperatorSet.ClearTextModel(TextModel);
            }
            //HOperatorSet.CreateTextModelReader("manual", new HTuple(), out TextModel);
            HOperatorSet.CreateTextModelReader("auto", OCRHandle, out TextModel);
            HOperatorSet.SetTextModelParam(TextModel, "polarity", "light_on_dark");
            HOperatorSet.SetTextModelParam(TextModel, "min_stroke_width", 5);
            HOperatorSet.SetTextModelParam(TextModel, "polarity", "light_on_dark");
            if (min_char_height > 0)
            {
                HOperatorSet.SetTextModelParam(TextModel, "min_char_height", min_char_height);
            }
            if (max_line_num > 0)
            {
               // HOperatorSet.SetTextModelParam(TextModel, "max_line_num", max_line_num);
            }
            
           // HOperatorSet.SetTextModelParam(TextModel, "text_line_structure", "7");


           ROIRectangle2 MROI = ModelRoi[0].Copy();
            int suc = getOCRText(TempImage,MROI);
            return 0;
        }
        public int getOCRText(HObject aImage,ROIRectangle2 MROI,bool isTemp=true)
        {
            try
            {
                if (ModelRoi == null || ModelRoi.Count == 0)
                {
                    return -1;
                }
                HTuple channels = 0;
                HOperatorSet.CountChannels(aImage, out channels);
                HObject grayObject = null;

                HOperatorSet.AccessChannel(aImage, out grayObject, 1);
                //HOperatorSet.Threshold(grayObject, out grayObject, 50, 255);
                //HOperatorSet.Connection(grayObject, out grayObject);

                HRegion hReg = MROI.getRegion();
                HOperatorSet.ReduceDomain(grayObject, hReg, out grayObject);

                HObject ho_TmpObj_Domain, ho_TmpObj_DomainExpanded, ho_TmpObj_DomainTransformedRaw, ho_TmpObj_DomainTransformed;
                HObject ho_TmpObj_ImageTransformed, ho_TmpObj_ImageTransformedExpanded, ho_TmpObj_ImageTransformedReduced, ho_TmpObj_MonoReduced_OCR_01_0;
                HTuple hv_TmpCtrl_MatrixIdentity = null, hv_TmpCtrl_ClipRegion = null;
                HTuple hv_TmpCtrl_Row1 = null, hv_TmpCtrl_Col1 = null;
                HTuple hv_TmpCtrl_Row2 = null, hv_TmpCtrl_Col2 = null;
                HTuple hv_TmpCtrl_MatrixTranslation = null, hv_TmpCtrl_MatrixComposite = null;
                HTuple hv_TmpCtrl_ResultHandle_OCR_01_0 = null, hv_TmpCtrl_Expressions = null;
                HTuple hv_TmpCtrl_NumLines = null, hv_TmpCtrl_LineIndex = null;
                HTuple hv_SymbolNames_OCR_01_0 = new HTuple(), hv_Confidences_OCR_01_0 = new HTuple();
                HTuple hv_Words_OCR_01_0 = new HTuple(), hv_Scores_OCR_01_0 = new HTuple();
                HOperatorSet.GenEmptyObj(out ho_TmpObj_Domain);
                HOperatorSet.GenEmptyObj(out ho_TmpObj_DomainExpanded);
                HOperatorSet.GenEmptyObj(out ho_TmpObj_DomainTransformedRaw);
                HOperatorSet.GenEmptyObj(out ho_TmpObj_DomainTransformed);
                HOperatorSet.GenEmptyObj(out ho_TmpObj_ImageTransformed);
                HOperatorSet.GenEmptyObj(out ho_TmpObj_ImageTransformedExpanded);
                HOperatorSet.GenEmptyObj(out ho_TmpObj_ImageTransformedReduced);
                HOperatorSet.GenEmptyObj(out ho_TmpObj_MonoReduced_OCR_01_0);

                HOperatorSet.HomMat2dIdentity(out hv_TmpCtrl_MatrixIdentity);
                //OCR 01: Apply transformation to image and domain
                HOperatorSet.GetDomain(grayObject, out ho_TmpObj_Domain);
                HOperatorSet.GetSystem("clip_region", out hv_TmpCtrl_ClipRegion);
                HOperatorSet.SetSystem("clip_region", "false");
                ho_TmpObj_DomainExpanded.Dispose();
                HOperatorSet.DilationCircle(ho_TmpObj_Domain, out ho_TmpObj_DomainExpanded, 42);
                ho_TmpObj_DomainTransformedRaw.Dispose();
                HOperatorSet.AffineTransRegion(ho_TmpObj_DomainExpanded, out ho_TmpObj_DomainTransformedRaw, hv_TmpCtrl_MatrixIdentity, "true");

                HOperatorSet.SmallestRectangle1(ho_TmpObj_DomainTransformedRaw, out hv_TmpCtrl_Row1, out hv_TmpCtrl_Col1, out hv_TmpCtrl_Row2, out hv_TmpCtrl_Col2);
                HOperatorSet.HomMat2dTranslate(hv_TmpCtrl_MatrixIdentity, -hv_TmpCtrl_Row1, -hv_TmpCtrl_Col1, out hv_TmpCtrl_MatrixTranslation);
                HOperatorSet.HomMat2dCompose(hv_TmpCtrl_MatrixTranslation, hv_TmpCtrl_MatrixIdentity, out hv_TmpCtrl_MatrixComposite);
                ho_TmpObj_DomainTransformed.Dispose();
                HOperatorSet.AffineTransRegion(ho_TmpObj_Domain, out ho_TmpObj_DomainTransformed, hv_TmpCtrl_MatrixComposite, "true");
                ho_TmpObj_ImageTransformed.Dispose();
                HOperatorSet.AffineTransImage(grayObject, out ho_TmpObj_ImageTransformed, hv_TmpCtrl_MatrixComposite, "constant", "true");
                ho_TmpObj_DomainExpanded.Dispose();
                HOperatorSet.DilationCircle(ho_TmpObj_Domain, out ho_TmpObj_DomainExpanded, 42);
                ho_TmpObj_ImageTransformedExpanded.Dispose();
                HOperatorSet.ExpandDomainGray(ho_TmpObj_ImageTransformed, out ho_TmpObj_ImageTransformedExpanded, 42);
                ho_TmpObj_ImageTransformedReduced.Dispose();
                HOperatorSet.ReduceDomain(ho_TmpObj_ImageTransformed, ho_TmpObj_DomainTransformed, out ho_TmpObj_ImageTransformedReduced);
                ho_TmpObj_MonoReduced_OCR_01_0.Dispose();
                HOperatorSet.CropPart(ho_TmpObj_ImageTransformedReduced, out ho_TmpObj_MonoReduced_OCR_01_0,
                    0, 0, (hv_TmpCtrl_Col2 - hv_TmpCtrl_Col1) + 1, (hv_TmpCtrl_Row2 - hv_TmpCtrl_Row1) + 1);
                HOperatorSet.SetSystem("clip_region", hv_TmpCtrl_ClipRegion);


                HTuple TextResult, NumLines, Class = new HTuple(), Confidence = new HTuple();
                HObject Line;
                //HOperatorSet.FindText(grayObject, TextModel, out TextResult);
                HOperatorSet.FindText(ho_TmpObj_MonoReduced_OCR_01_0, TextModel, out TextResult);
                HOperatorSet.GetTextResult(TextResult, "num_lines", out NumLines);

                HTuple SingleCharacters, TextLineCharacters;
                HOperatorSet.GetTextResult(TextResult, "class", out SingleCharacters);
                HOperatorSet.TupleSum(SingleCharacters, out TextLineCharacters);
                String OcrStr = TextLineCharacters.S;
                Result aRs = new Result();
                if (OcrStr != "")
                {
                    aRs.isOk = true;
                    aRs.Str = OcrStr;
                    if (OCRString != "")
                    {
                        if (OCRString != OcrStr)
                        {
                            aRs.isOk = false;
                        }
                    }
                    if (ViewPort != null)
                    {
                        HalconWind nHW = new HalconWind();
                        HOperatorSet.SetColor(ViewPort.HalconWindow, "green");
                        nHW.disp_message(ViewPort.HalconWindow, OcrStr, "image", MROI.MidR - MROI.Length1, MROI.MidC - MROI.Length2, "green", "false");
                    }
                }
                else
                {
                    aRs.isOk = false;
                    aRs.Str = OcrStr;
                }
                mResult.Add(aRs);
                HOperatorSet.ClearTextResult(TextResult);
                return 0;
            }
            catch
            {
                return -1;
            }
            
        }
        public override int ImageProcess()
        {
            try
            {
                ResultSta = true;
                mResult.Clear();
                if (SrcObject == null)
                {
                    return 0;
                }

                if (ModelRoi == null || ModelRoi.Count == 0)
                {
                    return -1;
                }
                ROIRectangle2 MROI = ModelRoi[0].Copy();
                if (VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult.Count == 1)
                {
                    MROI.MidC += VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].cX;
                    MROI.MidR += VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].cY;
                    MROI.Phi -= VisionSystem.Instance.WordSpaceList[WordSpaceID].CamImageFunList[CamID].ImageProcessList[0].mResult[0].Phi;
                }
                else
                {
                    return -1;
                }
                int suc = getOCRText(SrcObject, MROI, false);
                return suc;
            }
            catch (Exception e)
            {
                LogLib.Log.ShowWarning("图像处理出错：" + e.Message, true, "Warning", "BlobSplitAnalysis");
                return -1;
            }
            
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            OCRClassMlp aFun = CoverLib.JsonCover.DeStrLSerialize<OCRClassMlp>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string aFunPara = CoverLib.JsonCover.StrSerialize<OCRClassMlp>(this);
            return aFunPara;
        }
    }
}
