﻿namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    partial class frm_LoadStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btmMQQT_Write = new DevExpress.XtraEditors.SimpleButton();
            this.btmMQQT_Read = new DevExpress.XtraEditors.SimpleButton();
            this.客户端连接状态 = new DevExpress.XtraEditors.LabelControl();
            this.txtPCBSN = new DevExpress.XtraEditors.TextEdit();
            this.txtTRAYID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LEDClient = new HslCommunication.Controls.UserLantern();
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTRAYID.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btmMQQT_Write
            // 
            this.btmMQQT_Write.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmMQQT_Write.Appearance.Options.UseFont = true;
            this.btmMQQT_Write.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmMQQT_Write.AppearanceDisabled.Options.UseFont = true;
            this.btmMQQT_Write.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmMQQT_Write.AppearanceHovered.Options.UseFont = true;
            this.btmMQQT_Write.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmMQQT_Write.AppearancePressed.Options.UseFont = true;
            this.btmMQQT_Write.Location = new System.Drawing.Point(22, 189);
            this.btmMQQT_Write.LookAndFeel.SkinName = "Office 2013";
            this.btmMQQT_Write.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmMQQT_Write.Name = "btmMQQT_Write";
            this.btmMQQT_Write.Size = new System.Drawing.Size(209, 90);
            this.btmMQQT_Write.TabIndex = 72;
            this.btmMQQT_Write.Text = "得到条码及载板编号";
            this.btmMQQT_Write.Click += new System.EventHandler(this.btmMQQT_Write_Click);
            // 
            // btmMQQT_Read
            // 
            this.btmMQQT_Read.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmMQQT_Read.Appearance.Options.UseFont = true;
            this.btmMQQT_Read.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmMQQT_Read.AppearanceDisabled.Options.UseFont = true;
            this.btmMQQT_Read.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmMQQT_Read.AppearanceHovered.Options.UseFont = true;
            this.btmMQQT_Read.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btmMQQT_Read.AppearancePressed.Options.UseFont = true;
            this.btmMQQT_Read.Location = new System.Drawing.Point(246, 189);
            this.btmMQQT_Read.LookAndFeel.SkinName = "Office 2013";
            this.btmMQQT_Read.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmMQQT_Read.Name = "btmMQQT_Read";
            this.btmMQQT_Read.Size = new System.Drawing.Size(182, 90);
            this.btmMQQT_Read.TabIndex = 73;
            this.btmMQQT_Read.Text = "MQQT_Read";
            this.btmMQQT_Read.Click += new System.EventHandler(this.btmMQQT_Read_Click);
            // 
            // 客户端连接状态
            // 
            this.客户端连接状态.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.客户端连接状态.Appearance.Options.UseFont = true;
            this.客户端连接状态.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.客户端连接状态.AppearanceDisabled.Options.UseFont = true;
            this.客户端连接状态.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.客户端连接状态.AppearanceHovered.Options.UseFont = true;
            this.客户端连接状态.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.客户端连接状态.AppearancePressed.Options.UseFont = true;
            this.客户端连接状态.Location = new System.Drawing.Point(11, 79);
            this.客户端连接状态.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.客户端连接状态.Name = "客户端连接状态";
            this.客户端连接状态.Size = new System.Drawing.Size(132, 17);
            this.客户端连接状态.TabIndex = 75;
            this.客户端连接状态.Text = "上料工位客户端连接状态";
            // 
            // txtPCBSN
            // 
            this.txtPCBSN.EditValue = "N/A";
            this.txtPCBSN.Location = new System.Drawing.Point(76, 98);
            this.txtPCBSN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPCBSN.Name = "txtPCBSN";
            this.txtPCBSN.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtPCBSN.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtPCBSN.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPCBSN.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtPCBSN.Properties.Appearance.Options.UseBackColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseBorderColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseFont = true;
            this.txtPCBSN.Properties.Appearance.Options.UseForeColor = true;
            this.txtPCBSN.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPCBSN.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtPCBSN.Properties.ReadOnly = true;
            this.txtPCBSN.Size = new System.Drawing.Size(226, 24);
            this.txtPCBSN.TabIndex = 113;
            // 
            // txtTRAYID
            // 
            this.txtTRAYID.EditValue = "N/A";
            this.txtTRAYID.Location = new System.Drawing.Point(76, 68);
            this.txtTRAYID.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTRAYID.Name = "txtTRAYID";
            this.txtTRAYID.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txtTRAYID.Properties.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.txtTRAYID.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTRAYID.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txtTRAYID.Properties.Appearance.Options.UseBackColor = true;
            this.txtTRAYID.Properties.Appearance.Options.UseBorderColor = true;
            this.txtTRAYID.Properties.Appearance.Options.UseFont = true;
            this.txtTRAYID.Properties.Appearance.Options.UseForeColor = true;
            this.txtTRAYID.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTRAYID.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtTRAYID.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTRAYID.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtTRAYID.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txtTRAYID.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.txtTRAYID.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.txtTRAYID.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTRAYID.Properties.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtTRAYID.Properties.ReadOnly = true;
            this.txtTRAYID.Size = new System.Drawing.Size(226, 24);
            this.txtTRAYID.TabIndex = 112;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearanceDisabled.Options.UseFont = true;
            this.labelControl14.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearanceHovered.Options.UseFont = true;
            this.labelControl14.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearancePressed.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(23, 102);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(50, 17);
            this.labelControl14.TabIndex = 111;
            this.labelControl14.Text = "PCB编号:";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearanceDisabled.Options.UseFont = true;
            this.labelControl13.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearanceHovered.Options.UseFont = true;
            this.labelControl13.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearancePressed.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(22, 72);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(51, 17);
            this.labelControl13.TabIndex = 110;
            this.labelControl13.Text = "载板编号:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPCBSN);
            this.groupBox1.Controls.Add(this.labelControl13);
            this.groupBox1.Controls.Add(this.txtTRAYID);
            this.groupBox1.Controls.Add(this.btmMQQT_Read);
            this.groupBox1.Controls.Add(this.labelControl14);
            this.groupBox1.Controls.Add(this.btmMQQT_Write);
            this.groupBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(179, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(455, 318);
            this.groupBox1.TabIndex = 114;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "上料工位条码及PCB 板号触发操作";
            // 
            // LEDClient
            // 
            this.LEDClient.BackColor = System.Drawing.Color.Transparent;
            this.LEDClient.LanternBackground = System.Drawing.Color.Gray;
            this.LEDClient.Location = new System.Drawing.Point(49, 21);
            this.LEDClient.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.LEDClient.Name = "LEDClient";
            this.LEDClient.Size = new System.Drawing.Size(48, 48);
            this.LEDClient.TabIndex = 115;
            // 
            // frm_LoadStation
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 911);
            this.Controls.Add(this.LEDClient);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.客户端连接状态);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_LoadStation";
            this.Text = "frm_LoadStation";
            this.Load += new System.EventHandler(this.frm_LoadStation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtPCBSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTRAYID.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btmMQQT_Write;
        private DevExpress.XtraEditors.SimpleButton btmMQQT_Read;
        private DevExpress.XtraEditors.LabelControl 客户端连接状态;
        private DevExpress.XtraEditors.TextEdit txtPCBSN;
        private DevExpress.XtraEditors.TextEdit txtTRAYID;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private System.Windows.Forms.GroupBox groupBox1;
        private HslCommunication.Controls.UserLantern LEDClient;
    }
}