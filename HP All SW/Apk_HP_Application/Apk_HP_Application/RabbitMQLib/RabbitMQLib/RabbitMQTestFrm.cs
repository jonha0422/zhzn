﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RabbitMQLib
{
    public partial class RabbitMQTestFrm : Form
    {
        private string MsgFrom;
        private string MsgType;
        private string MsgData;
        public RabbitMQTestFrm()
        {
            InitializeComponent();
        }

        private void RabbitMQTestFrm_Load(object sender, EventArgs e)
        {
            MqttSystem.Instance.onMsgCallBack += OnMsgCallBack;
            timer1.Enabled = true;
        }

        private void bExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bSent_Click(object sender, EventArgs e)
        {
            MqttSystem.Instance.SentMsg(cbTargetStation.Text, txtMsgType.Text, txtMsgData.Text);
        }
        private void OnMsgCallBack(string msgdata)
        {
            try
            {
                MsgData aMsgData = CoverLib.JsonCover.DeStrLSerialize<MsgData>(msgdata);
                MsgFrom = aMsgData.SentStation;
                MsgType= aMsgData.MsgType;
                MsgData = aMsgData.Msg;
            }
            catch
            {
                MsgFrom = "";
                MsgType = "";
                MsgData = "错误的结构体：" + msgdata;
            }
            
        }

        private void RabbitMQTestFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MqttSystem.Instance.onMsgCallBack -= OnMsgCallBack;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            cbReceiveFrom.Text = MsgFrom;
            ReceiveType.Text = MsgType;
            txtReceiveData.Text =MsgData;
        }
    }
}
