﻿using CamaeraLib;
using GenericLib;
using HalconDotNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using VisionLib.ImageProcess;

namespace VisionLib
{
    public class ImageFunctonConfig
    {
        public string ClassName { get; set; }
        public string Para { get; set; }

    }
    public class ImageProcessFunction
    {
        public HImage TempImage;//模板文件
        public HImage SrcImage;//待处理文件
        public List<ImageProcessBase> ImageProcessList;
        public List<ImageFunctonConfig> ImageFunctonConfigList;
        public int CamID;
        public int WordSpaceID;
        public bool ResultSta;
        public ImageProcessFunction()
        {
            ImageProcessList = new List<ImageProcessBase>();
            ImageFunctonConfigList = new List<ImageFunctonConfig>();
        }

        public bool Read(string mWordSpace)
        {
            string path = Application.StartupPath + "\\Config\\Vision\\" + VisionSystem.Instance.ProductName + "\\" + mWordSpace;
            DirectoryHandler.CreateFolder(path);
            if (File.Exists(@path + "\\Cam" + CamID.ToString() + ".bmp"))
            {
                TempImage = new HImage(path + "\\Cam" + CamID.ToString() + ".bmp");
            }
            

            if (File.Exists(path + "\\Cam" + CamID.ToString() + ".xml"))
            {
                using (TextReader reader1 = new StreamReader(path + "\\Cam" + CamID.ToString() + ".xml"))
                {
                    XmlSerializer serializer1 = new XmlSerializer(typeof(List<ImageFunctonConfig>));
                    ImageFunctonConfigList = (List<ImageFunctonConfig>)serializer1.Deserialize(reader1);
                    reader1.Close();
                }
            }
            
            if (ImageFunctonConfigList.Count == 0)
            {
                ImageProcessList.Add(new ImageProcessTest());
                ImageFunctonConfig aFun = new ImageFunctonConfig();
                aFun.ClassName = "VisionLib.ImageProcess.ImageProcessTest";
                aFun.Para = CoverLib.JsonCover.StrSerialize<ImageProcessTest>((ImageProcessTest)ImageProcessList[0]);
                ImageFunctonConfigList.Add(aFun);
                Writer(mWordSpace);
            }
            else
            {
                for (int i = 0; i < ImageFunctonConfigList.Count; i++)
                {
                    Object Class=CoverLib.XMLCover.getClassByType(ImageFunctonConfigList[i].ClassName);
                    //ImageProcessTest obj = (Class)1;
                    ImageProcessBase nFunClass=((ImageProcessBase)Class).CreatClass(ImageFunctonConfigList[i].Para);
                    if (nFunClass != null)
                    {
                        ImageProcessList.Add(nFunClass);
                    }
                    else
                    {
                        ImageProcessList.Add((ImageProcessBase)Class);
                    }
                    ImageProcessList[i].CamID = CamID;
                    ImageProcessList[i].WordSpaceID = WordSpaceID;
                    if (TempImage != null)
                    {
                        ImageProcessList[i].Init(TempImage);
                    }

                }
            }
            
            
            return true;
        }
        public bool Writer(string mWordSpace)
        {
            string path = Application.StartupPath + "\\Config\\Vision\\" + VisionSystem.Instance.ProductName + "\\" + mWordSpace;
            ImageFunctonConfigList.Clear();
            for (int i = 0; i < ImageProcessList.Count; i++)
            {
                ImageFunctonConfig aFunConfig=new ImageFunctonConfig();
                aFunConfig.ClassName = ImageProcessList[i].GetType().ToString();
                aFunConfig.Para = ImageProcessList[i].GetClassPara();
                ImageFunctonConfigList.Add(aFunConfig);
            }
            DirectoryHandler.CreateFolder(path);
            StreamWriter writer = new StreamWriter(path + "\\Cam" + CamID.ToString() + ".xml");
            XmlSerializer serializer = new XmlSerializer(typeof(List<ImageFunctonConfig>));
            serializer.Serialize(writer, ImageFunctonConfigList);
            writer.Close();
            if(TempImage!=null){
                HOperatorSet.WriteImage(TempImage, "bmp",0, path + "\\Cam" + CamID.ToString() + ".bmp");
            }
            
            return true;
        }
        public int init()
        {
            if (CamaeraLib.CameraSystem.Instance.CameraList.Count <= CamID) {
                return -1;
            }
            CamaeraLib.CameraSystem.Instance.CameraList[CamID].ImageReadyEvent += new CameraBase.ImageReadyEventHandler(OnImageReadyEventCallback);
            return 0;
        }
        private void OnImageReadyEventCallback()
        {
            if (VisionLib.VisionSystem.Instance.AutoRun == false)
            {
                return ;
            }
            ImageProcess();
        }
        public int ImageProcess()
        {
            CamaeraLib.Image aImage = CamaeraLib.CameraSystem.Instance.CameraList[CamID].GetLatestImage();
            int suc = ImageConvert.ImageTohImage(aImage, out SrcImage);
            if (true)
            {
                DateTime _ts = System.DateTime.Now;
                string strDt = _ts.ToString("yyyy-MM-dd hh mm ss");
                HOperatorSet.WriteImage(SrcImage, "bmp", 0, "C:\\cam"+ CamID.ToString()+"\\" + strDt + ".bmp");
            }
            VisionSystem.Instance.ImageProcessBeforeEvent(WordSpaceID, CamID);
            ResultSta = true;
            for (int i = 0; i < ImageProcessList.Count; i++)
            {
                ImageProcessList[i].SrcObject = SrcImage;
                suc+=ImageProcessList[i].ImageProcess();
                if (ImageProcessList[i].ResultSta == false)
                {
                    ResultSta = false;
                }
            }
            VisionSystem.Instance.ImageProcessAfterEvent(WordSpaceID, CamID);

            return suc;
        }
    }
}
