﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslCommunication.Profinet.Omron;
using HslCommunication;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace CommunicationZH
{
    public  class OmronPLC_TCP : HslCommunication.Profinet.Omron.OmronFinsNet
    {
        public bool isConnected = false;       
        public OmronPLC_TCP() : base()
        {
        }
        //
        // 摘要:
        //     从欧姆龙PLC中读取想要的数据，返回读取结果，读取单位为字
        //
        // 参数:
        //   address:
        //     读取地址，格式为"D100","C100","W100","H100","A100"
        //
        //   length:
        //     读取的数据长度
        //
        // 返回结果:
        //     带成功标志的结果数据对象
        public override OperateResult<byte[]> Read(string address, ushort length)
        {
            return base.Read(address, length);
        }
        //
        // 摘要:
        //     从欧姆龙PLC中批量读取位软元件，返回读取结果
        //
        // 参数:
        //   address:
        //     读取地址，格式为"D100.0","C100.15","W100.7","H100.4","A100.9"
        //
        // 返回结果:
        //     带成功标志的结果数据对象
        //
        // 备注:
        //     地址的格式请参照HslCommunication.Profinet.Omron.OmronFinsNet.ReadBool(System.String,System.UInt16)方法
        //
        // 摘要:
        //     返回表示当前对象的字符串
        //
        // 返回结果:
        //     字符串
        public override string ToString()
        {
            return base.ToString();
        }
        //
        // 摘要:
        //     向PLC写入数据，数据格式为原始的字节类型
        //
        // 参数:
        //   address:
        //     初始地址
        //
        //   value:
        //     原始的字节数据
        //
        // 返回结果:
        //     结果
        public override OperateResult Write(string address, byte[] value)
        {
            return base.Write(address, value);
        }
    }

    public class OmronPLC_UDP : HslCommunication.Profinet.Omron.OmronFinsUdp
    {
        public IPEndPoint IpEndPointAddr;
        public OmronPLC_UDP(IPEndPoint ip) : base(ip.Address.ToString(), ip.Port)
        {
            IpEndPointAddr = ip;
        }

        public ushort[] ReadDataDef(ushort startAddress, ushort count)
        {
            string strAdd = "D" + startAddress.ToString();
            OperateResult<ushort[]> reads = base.ReadUInt16(strAdd, count);
            if (reads.IsSuccess)
                return reads.Content;
            else
                return null;
        }

        public void WriteDataDef(ushort startAddress, ushort[] data)
        {
            string strAdd = "D" + startAddress.ToString();
            OperateResult result = base.Write(strAdd, data);
            return;
        }
    }

}
