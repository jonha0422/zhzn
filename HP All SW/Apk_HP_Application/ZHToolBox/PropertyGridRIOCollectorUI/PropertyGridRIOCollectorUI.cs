﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using ZHToolBox.PropertyGridRIOCollectorUI;

namespace ZHToolBox.HalconVision
{
    class PropertyGridRIOCollectorUI : UITypeEditor
    {
        System.Windows.Forms.PropertyGrid propertyGrid = null;
        ~PropertyGridRIOCollectorUI()
        {
        }


        public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
        {

            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

            propertyGrid = edSvc as System.Windows.Forms.PropertyGrid;
            if (edSvc != null)
            {

                // 可以打开任何特定的对话框
                frm_RIOCollectorUI formUI = new frm_RIOCollectorUI();
                formUI.rapteClass.CheckResult = value as CheckResultDef[];
                formUI.ShowDialog();

                return formUI.rapteClass.CheckResult;
            }
            return value;
        }
    }

    class PropertyGridTempleteUI : UITypeEditor
    {
        System.Windows.Forms.PropertyGrid propertyGrid = null;
        ~PropertyGridTempleteUI()
        {
        }


        public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
        {

            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

            propertyGrid = edSvc as System.Windows.Forms.PropertyGrid;
            if (edSvc != null)
            {

                // 可以打开任何特定的对话框
                frm_TempleteResourceEditor formUI = new frm_TempleteResourceEditor();
                formUI.rapteClass.ResourceName = value as string;
                formUI.ShowDialog();

                return formUI.rapteClass.ResourceName;
            }
            return value;
        }
    }
}

