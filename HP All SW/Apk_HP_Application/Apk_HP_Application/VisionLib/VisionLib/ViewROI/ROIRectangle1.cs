using System;
using HalconDotNet;


namespace InteractiveROI
{
	/// <summary>
	/// This class demonstrates one of the possible implementations for a 
	/// (simple) rectangularly shaped ROI. ROIRectangle1 inherits 
	/// from the base class ROI and implements (besides other auxiliary
	/// methods) all virtual methods defined in ROI.cs.
	/// Since a simple rectangle is defined by two data points, by the upper 
	/// left corner and the lower right corner, we use four values (row1/col1) 
	/// and (row2/col2) as class members to hold these positions at 
	/// any time of the program. The four corners of the rectangle can be taken
	/// as handles, which the user can use to manipulate the size of the ROI. 
	/// Furthermore, we define a midpoint as an additional handle, with which
	/// the user can grab and drag the ROI. Therefore, we declare NumHandles
	/// to be 5 and set the activeHandle to be 0, which will be the upper left
	/// corner of our ROI.
	/// </summary>
	public class ROIRectangle1 : ROI
	{

        private double row1, col1;   // upper left
        private double row2, col2;   // lower right 
        private double midR, midC;   // midpoint 
        private double m_x, m_y;

        public double Row1
        {
            get
            {
                return row1;
            }

            set
            {
                row1 = value;
            }
        }

        public double Col1
        {
            get
            {
                return col1;
            }

            set
            {
                col1 = value;
            }
        }

        public double Row2
        {
            get
            {
                return row2;
            }

            set
            {
                row2 = value;
            }
        }

        public double Col2
        {
            get
            {
                return col2;
            }

            set
            {
                col2 = value;
            }
        }

        public double MidR
        {
            get
            {
                return midR;
            }

            set
            {
                midR = value;
            }
        }

        public double MidC
        {
            get
            {
                return midC;
            }

            set
            {
                midC = value;
            }
        }

        public double X
        {
            get
            {
                return m_x;
            }

            set
            {
                m_x = value;
            }
        }

        public double Y
        {
            get
            {
                return m_y;
            }

            set
            {
                m_y = value;
            }
        }

        /// <summary>Constructor</summary>
        public ROIRectangle1()
		{

			NumHandles = 5; // 4 corner points + midpoint
			activeHandleIdx = 4;
		}

		/// <summary>Creates a new ROI instance at the mouse position</summary>
		/// <param name="midX">
		/// x (=column) coordinate for interactive ROI
		/// </param>
		/// <param name="midY">
		/// y (=row) coordinate for interactive ROI
		/// </param>
		public override void createROI(double midX, double midY)
		{
			MidR = midY;
			MidC = midX;

			Row1 = MidR - 50;
			Col1 = MidC - 50;
			Row2 = MidR + 50;
			Col2 = MidC + 50;
		}

		/// <summary>Paints the ROI into the supplied window</summary>
		/// <param name="window">HALCON window</param>
		public override void draw(HalconDotNet.HWindow window)
		{
			window.DispRectangle1(Row1, Col1, Row2, Col2);
            
			window.DispRectangle2(Row1, Col1, 0, 5, 5);
			window.DispRectangle2(Row1, Col2, 0, 5, 5);
			window.DispRectangle2(Row2, Col2, 0, 5, 5);
			window.DispRectangle2(Row2, Col1, 0, 5, 5);
			window.DispRectangle2(MidR, MidC, 0, 5, 5);
		}

		/// <summary> 
		/// Returns the distance of the ROI handle being
		/// closest to the image point(x,y)
		/// </summary>
		/// <param name="x">x (=column) coordinate</param>
		/// <param name="y">y (=row) coordinate</param>
		/// <returns> 
		/// Distance of the closest ROI handle.
		/// </returns>
		public override double distToClosestHandle(double x, double y)
		{

			double max = 10000;
			double [] val = new double[NumHandles];

			MidR = ((Row2 - Row1) / 2) + Row1;
			MidC = ((Col2 - Col1) / 2) + Col1;

			val[0] = HMisc.DistancePp(y, x, Row1, Col1); // upper left 
			val[1] = HMisc.DistancePp(y, x, Row1, Col2); // upper right 
			val[2] = HMisc.DistancePp(y, x, Row2, Col2); // lower right 
			val[3] = HMisc.DistancePp(y, x, Row2, Col1); // lower left 
			val[4] = HMisc.DistancePp(y, x, MidR, MidC); // midpoint 

			for (int i=0; i < NumHandles; i++)
			{
				if (val[i] < max)
				{
					max = val[i];
					activeHandleIdx = i;
				}
			}// end of for 

			return val[activeHandleIdx];
		}

		/// <summary> 
		/// Paints the active handle of the ROI object into the supplied window
		/// </summary>
		/// <param name="window">HALCON window</param>
		public override void displayActive(HalconDotNet.HWindow window)
		{
			switch (activeHandleIdx)
			{
				case 0:
					window.DispRectangle2(Row1, Col1, 0, 5, 5);
					break;
				case 1:
					window.DispRectangle2(Row1, Col2, 0, 5, 5);
					break;
				case 2:
					window.DispRectangle2(Row2, Col2, 0, 5, 5);
					break;
				case 3:
					window.DispRectangle2(Row2, Col1, 0, 5, 5);
					break;
				case 4:
					window.DispRectangle2(MidR, MidC, 0, 5, 5);
                    X = MidR;
                    Y = MidC;
					break;
			}
		}

		/// <summary>Gets the HALCON region described by the ROI</summary>
		public override HRegion getRegion()
		{
			HRegion region = new HRegion();
			region.GenRectangle1(Row1, Col1, Row2, Col2);
			return region;
		}

		/// <summary>
		/// Gets the model information described by 
		/// the interactive ROI
		/// </summary> 
		public override HTuple getModelData()
		{
			return new HTuple(new double[] { Row1, Col1, Row2, Col2 });
		}


		/// <summary> 
		/// Recalculates the shape of the ROI instance. Translation is 
		/// performed at the active handle of the ROI object 
		/// for the image coordinate (x,y)
		/// </summary>
		/// <param name="newX">x mouse coordinate</param>
		/// <param name="newY">y mouse coordinate</param>
		public override void moveByHandle(double newX, double newY)
		{
			double len1, len2;
			double tmp;

			switch (activeHandleIdx)
			{
				case 0: // upper left 
					Row1 = newY;
					Col1 = newX;
					break;
				case 1: // upper right 
					Row1 = newY;
					Col2 = newX;
					break;
				case 2: // lower right 
					Row2 = newY;
					Col2 = newX;
					break;
				case 3: // lower left
					Row2 = newY;
					Col1 = newX;
					break;
				case 4: // midpoint 
					len1 = ((Row2 - Row1) / 2);
					len2 = ((Col2 - Col1) / 2);

					Row1 = newY - len1;
					Row2 = newY + len1;

					Col1 = newX - len2;
					Col2 = newX + len2;

					break;
			}

			if (Row2 <= Row1)
			{
				tmp = Row1;
				Row1 = Row2;
				Row2 = tmp;
			}

			if (Col2 <= Col1)
			{
				tmp = Col1;
				Col1 = Col2;
				Col2 = tmp;
			}

			MidR = ((Row2 - Row1) / 2) + Row1;
			MidC = ((Col2 - Col1) / 2) + Col1;

		}//end of method
	}//end of class
}//end of namespace
