﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO.Ports;

namespace CommunicationZH
{
    class APK_FlashV952
    {
        public TcpClient socketCommunication = null;
        public SerialPort serialCommunication = null;

        #region>>>>>>>>>> CRC table
        UInt32[] V2_Crc32Table = new UInt32[]{
            0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b, 0x1a864db2, 0x1e475005,
            0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61, 0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd,
            0x4c11db70, 0x48d0c6c7, 0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
            0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3, 0x709f7b7a, 0x745e66cd,
            0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039, 0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5,
            0xbe2b5b58, 0xbaea46ef, 0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
            0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb, 0xceb42022, 0xca753d95,
            0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1, 0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d,
            0x34867077, 0x30476dc0, 0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
            0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4, 0x0808d07d, 0x0cc9cdca,
            0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde, 0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02,
            0x5e9f46bf, 0x5a5e5b08, 0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
            0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc, 0xb6238b25, 0xb2e29692,
            0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6, 0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a,
            0xe0b41de7, 0xe4750050, 0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
            0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34, 0xdc3abded, 0xd8fba05a,
            0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637, 0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb,
            0x4f040d56, 0x4bc510e1, 0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
            0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5, 0x3f9b762c, 0x3b5a6b9b,
            0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff, 0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623,
            0xf12f560e, 0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
            0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd, 0xcda1f604, 0xc960ebb3,
            0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7, 0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b,
            0x9b3660c6, 0x9ff77d71, 0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
            0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b, 0x43cdc09c,
            0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8, 0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24,
            0x119b4be9, 0x155a565e, 0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
            0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a, 0x2d15ebe3, 0x29d4f654,
            0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0, 0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c,
            0xe3a1cbc1, 0xe760d676, 0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
            0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662, 0x933eb0bb, 0x97ffad0c,
            0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668, 0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
        };
        #endregion

        public bool ConnectSocket(string ip, int port)
        {
            try
            {
                if (serialCommunication != null)
                    serialCommunication.Close();

                socketCommunication.Connect(System.Net.IPAddress.Parse(ip), port);
                if (socketCommunication.Connected)
                    return true;
                else
                    return false;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ConnectRS232(string comport, int baudrate = 115200)
        {
            try
            {
                if (socketCommunication != null)
                    socketCommunication.Close();

                serialCommunication.BaudRate = baudrate;
                serialCommunication.Parity = Parity.None;
                serialCommunication.StopBits = StopBits.One;
                serialCommunication.DataBits = 8;
                serialCommunication.Open();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        byte[] FlashMachineCRC32(byte[] pData, ref UInt32 CRCResult)
        {
            int length_cnt, byte_cnt;
            CRCResult = 0x00;

            CRCResult = 0xFFFFFFFF;
            int Length = pData.Length;
            for (length_cnt = 0; length_cnt < Length; length_cnt++)
            {
                for (byte_cnt = 0; byte_cnt < 3; byte_cnt++)
                {
                    CRCResult = (CRCResult << 8) ^ V2_Crc32Table[((CRCResult >> 24) & 0xFF) ^ 0x00];
                }
                CRCResult = (CRCResult << 8) ^ V2_Crc32Table[((CRCResult >> 24) & 0xFF) ^ pData.ElementAt(length_cnt)];
            }

            byte[] newData = new byte[pData.Length + 4];
            Array.Copy(pData, newData, pData.Length);
            newData[pData.Length + 0] = (byte)((CRCResult & 0xFF000000) >> 24);
            newData[pData.Length + 1] = (byte)((CRCResult & 0x00FF0000) >> 16);
            newData[pData.Length + 2] = (byte)((CRCResult & 0x0000FF00) >> 8);
            newData[pData.Length + 3] = (byte)(CRCResult & 0x000000FF);

            return newData;
        }

        public bool sendCommandToFlashMachine(byte[] command, ref byte[] szResponed, byte[] waitCompletePrefix, bool bWaitForCompleted = true, int nSleepTime = 200, UInt32 dwTimeOutSec = 250, bool addCRCEnd = true)
        {
            UInt32 CRCResult = 0x00;
            FlashMachineCRC32(command, ref CRCResult);


            return false;
        }

        public int GetInQueueLength()
        {
            return 0;
        }

        public byte[] ReadResponed(int nLen)
        {
            return null;
        }

        bool Flash_Start(ref int nDataByteSize, ref int nCountOfFailed, ref byte[] valuesReturn, bool bWaitCompleted)
        {
            bool bResult = false;
            byte[] szCommand = new byte[100];
            byte[] szCompareStr = new byte[4]{ 0x99, 0x66, 0x01, 0x04};
            byte[] szResponed = new byte[1024];

            byte[] totalReadRespond = null;
            byte[] szMessageOuput = new byte[1024]; ;
            byte[] szReadOutRS232 = new byte[5000];

            int nRetray = 0;
            int nReadLen = 0;

            nCountOfFailed = 0;

        RE_TEST:
            nRetray++;
            szCommand[0] = 0x66;
            szCommand[1] = 0x99;
            szCommand[2] = 0x01;
            szCommand[3] = 0x05;
            szCommand[4] = 0x57;
            szCommand[5] = 0x52;
            szCommand[6] = 0x49;
            szCommand[7] = 0x54;
            szCommand[8] = 0x45;

            //初始化数据
            //sendCommandToFlashMachine(szCommand, true);

            //延时读取结果
            DateTime time1, time2;
            time1 = time2 = DateTime.Now;

            int nActualLen = 0;
            byte[] szResultPrefix = new byte[]{ 0x99, 0x66, 0x04};
            bool bReadResult = false;
            byte[] newReadData;
            while (true)
            {
                Thread.Sleep(200);
                time2 = DateTime.Now;
                TimeSpan timeSpan = time2 - time1;
                if (timeSpan.TotalSeconds >= 120)
                    return false;

                nReadLen = GetInQueueLength();
                if (nReadLen <= 0)
                    continue;

                newReadData = ReadResponed(nReadLen);
                if (newReadData == null || newReadData.Length <= 0)
                    continue;

                if ( totalReadRespond == null || totalReadRespond.Length <= 0)
                {
                    totalReadRespond = new byte[newReadData.Length];
                    Array.Copy(newReadData, totalReadRespond, newReadData.Length);
                }
                else
                {
                    totalReadRespond.Concat<byte>(newReadData);
                }

                //如果找到数据报头就停下
                if (Array.IndexOf(totalReadRespond, szResultPrefix) >= 0)
                {
                    Thread.Sleep(3000);
                    break;
                }
            }

            //再读取一次
           newReadData = ReadResponed(nReadLen);
            if (totalReadRespond == null || totalReadRespond.Length <= 0)
            {
                totalReadRespond = new byte[newReadData.Length];
                Array.Copy(newReadData, totalReadRespond, newReadData.Length);
            }
            else
            {
                totalReadRespond.Concat<byte>(newReadData);
            }

            //以16进制显示数据

            //从数据在提取结果
            Array.Clear(valuesReturn, 0, valuesReturn.Length);
            valuesReturn = new byte[totalReadRespond.Length - 5];
            Array.Copy(totalReadRespond, 5, valuesReturn, 0, totalReadRespond.Length - 5);
            nCountOfFailed = totalReadRespond[4];
            nDataByteSize = totalReadRespond[3];
            return true;    
        }

        //==================================================================================
        // 函数名:
        // 描叙:
        // 形参说明：
        // 主机发“CF401A / U.A      ”
        // 0x66 0x99 0x03 0x10 'C' 'F' '4' '0' '1' 'A' '/' 'U' '.' 'A' 0x20 0x20 0x20 0x20 0x20 0x20 0x校验 0x校验 0x校验 0x校验
        // 从机回应（从机正确接收到主机命令则回应否则不回应）
        // 0x99 0x66 0x03 0x04 0x55 0x55 0x55 0x55 0x校验 0x校验 0x校验 0x校验
        //==================================================================================
        bool Flash_SetType(string szTypeName, ref bool bCompareResult, bool bWaitCompleted)
        {
            bool bResult = false;
            byte[] szCommand = new byte[100];
            byte[] szCompareStr = new byte[3]{ 0x99, 0x66, 0x03 };
            byte[] szResponed = new byte[1024];

            if (szTypeName == null || szTypeName.Length <= 0)
                return false;

            szCommand[0] = 0x66;
            szCommand[1] = 0x99;
            szCommand[2] = 0x03;
            szCommand[3] = 0x10;

            byte[] byteTypeName = System.Text.Encoding.ASCII.GetBytes(szTypeName); 
            byte[] szName = new byte[16];
            for (int nIndex = 0; nIndex < 16; nIndex++)
                szName[nIndex] = 0x20;

            if (byteTypeName.Length >= 16)
                Array.Copy(byteTypeName, 0, szName, 0, 16);
            else
                Array.Copy(byteTypeName, 0, szName, 0, byteTypeName.Length);

            Array.Copy(byteTypeName, 0, szCommand, 4, byteTypeName.Length);

            //CRC32 校验
            uint crcResult = 0x00;
            byte[] writeCommand = FlashMachineCRC32(szCommand, ref crcResult);
            if (writeCommand == null || writeCommand.Length <= 0)
                return false;

            //发送命令
//             if (!sendCommandToFlashMachine(writeCommand, byte[] responed, true))
//                 return false;

            Thread.Sleep(50);

            //拼日期
            UInt32 dwData = 0;
            UInt32 dwData1 = (UInt32)szResponed[4] << 24 & 0xFF000000;
            UInt32 dwData2 = (UInt32)szResponed[5] << 16 & 0x00FF0000;
            UInt32 dwData3 = (UInt32)szResponed[6] << 8 & 0x0000FF00;
            UInt32 dwData4 = (UInt32)szResponed[7] << 0 & 0x000000FF;
            dwData = dwData1 | dwData2 | dwData3 | dwData4;
            string szData = string.Format("%08ld", dwData);

            //拼时间
            UInt32 dwTime = 0;
            UInt32 dwTime1 = (UInt32)szResponed[8] << 16 & 0x00FF0000;
            UInt32 dwTime2 = (UInt32)szResponed[9] << 8 & 0x0000FF00;
            UInt32 dwTime3 = (UInt32)szResponed[10] << 0 & 0x000000FF;
            dwTime = dwTime1 | dwTime2 | dwTime3;
            string szTime = string.Format("%06ld", dwTime);

            //拼版本
            string szVer = string.Format("M%d.%d-S%d.%d SD%d.%d",
                                                            szResponed[11], szResponed[12],
                                                            szResponed[13], szResponed[14],
                                                            szResponed[15], szResponed[16]);

            //显示烧录机内部程序信息
            string strDisp = "当前烧录型号信息: ";
            strDisp += szData;
            strDisp += " ";
            strDisp += szTime;
            strDisp += ":";
            strDisp += szVer;

            //开始比较烧录日期与当前日期
//             char szError[100] = { 0 };
//             int nYear = atoi(string(szData).substr(0, 4).c_str());
//             int nMonth = atoi(string(szData).substr(4, 2).c_str());
//             int nDay = atoi(string(szData).substr(6, 2).c_str());
//             int nHour = atoi(string(szTime).substr(0, 2).c_str());
//             int nMin = atoi(string(szTime).substr(2, 2).c_str());
//             int nSec = atoi(string(szTime).substr(4, 2).c_str());

//             m_pDlgMainUI->AddString(_T("烧录验证: 得到烧录软件时间"), COLOR_BLUE, 10);
//             if (nYear < 1900 ||
//                 nMonth < 1 ||
//                 nMonth > 12 ||
//                 nDay < 1 ||
//                 nDay > 31 ||
//                 nHour < 0 ||
//                 nHour > 23 ||
//                 nMin < 0 ||
//                 nMin > 59 ||
//                 nSec < 0 ||
//                 nSec > 59)
//             {
//                 sprintf_s(szError, "得到烧录机内部日期时间有误\r\n");
//                 m_pDlgMainUI->AddString(StringA_To_StringW(szError).GetBuffer(), COLOR_RED, 12);
//                 PutMessage(szError, 0, MESSAGE_TYPE_ERROR);
//                 return FALSE;
//             }
//             CTime Time1Ojb(nYear, nMonth, nDay, nHour, nMin, nSec);
// 
//             //得到当前系统时间
//             m_pDlgMainUI->AddString(_T("烧录验证: 得到当前系统时间\r\n"), COLOR_BLUE, 10);
//             SYSTEMTIME sysTime;
//             GetLocalTime(&sysTime);
//             CTime Time2Ojb(sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
// 
//             //比较时间间隔
//             m_pDlgMainUI->AddString(_T("烧录验证: 比较时间间隔\r\n"), COLOR_BLUE, 10);
//             CTimeSpan timeSpn = Time2Ojb - Time1Ojb;
//             int nTotalHours = timeSpn.GetTotalHours();
// 
//             //比较结果
//             sprintf_s(szError, "烧录机内部程序时间与当前时间相差%d小时\r\n", nTotalHours);
//             PutMessage(szError, 0, MESSAGE_TYPE_DEBUG);
// 
//             if (nTotalHours >= 24 || nTotalHours <= -24)
//             {
//                 sprintf_s(szError, "烧录机内部程序时间与当前时间不匹配(超过%d小时)\r\n", nTotalHours);
//                 m_pDlgMainUI->AddString(StringA_To_StringW(szError).GetBuffer(), COLOR_RED, 12);
//                 PutMessage(szError, 0, MESSAGE_TYPE_ERROR);
//                 return FALSE;
//             }
//             else
//             {
//                 m_pDlgMainUI->AddString(StringA_To_StringW(szError).GetBuffer(), COLOR_GREEN, 10);
//             }

            return bResult;
        }
    }
}
