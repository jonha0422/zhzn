﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib.ImageProcess
{
    public class Threshold : ImageProcessBase
    {
        public int MinGray { get; set; }
        public int MaxGray { get; set; }
         public Threshold()
        {
            cName = "二值化";
            MinGray = 0;
            MaxGray = 100;
        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            return 0;
        }
        public override int ImageProcess()
        {
            if (SrcObject == null)
            {
                return 0;
            }
            HObject SearchObj = SrcObject;
            if (SearchRoi.Count > 0)
            {
                HRegion hReg = SearchRoi[0].getRegion();
                HOperatorSet.ReduceDomain(SrcObject, hReg, out  SearchObj);

            }
            HOperatorSet.Threshold(SearchObj, out DesObject, MinGray, MaxGray);
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            Threshold aFun = CoverLib.JsonCover.DeStrLSerialize<Threshold>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string  aFunPara = CoverLib.JsonCover.StrSerialize<Threshold>(this);
            return aFunPara;
        }
    }
}
