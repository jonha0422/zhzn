using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

using Clifton.Tools.Strings;
using Clifton.Windows.Forms.XmlTree;

namespace daitou
{
	public class TableControllerTwo : XtreeNodeController
	{
		protected TableDefTwo tableDefTwo;
		protected TableDef tableDef;

		public override string Name
		{
			get { return tableDefTwo.Name; }
			set {tableDefTwo.Name=value;}
		}
        public override string Pridata
        {
            get { return tableDefTwo.Pridata; }
            set { tableDefTwo.Pridata = value; }
        }
        public TableDefTwo TableDefTwo
		{
			get { return tableDefTwo; }
		}

		public override object Item
		{
			get { return tableDefTwo; }
		}

		public TableControllerTwo()
		{
		}

		public TableControllerTwo(TableDefTwo tableDefTwo)
		{
			this.tableDefTwo = tableDefTwo;
		}

		public override int Index(object item)
		{
			return tableDefTwo.Fields.IndexOf(((TableControllerThree)item).TableDefThree);
		}

		public override bool AddNode(IXtreeNode parentInstance, string tag)
		{
            tableDef = ((TableController)parentInstance).TableDef;
			tableDefTwo = new TableDefTwo();
            tableDef.Fields.Add(tableDefTwo);
			return true;
		}

		public override bool DeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableController)parentInstance).TableDef;
            tableDef.Fields.Remove(tableDefTwo);
			return true;
		}

		public override void AutoDeleteNode(IXtreeNode parentInstance)
		{
            tableDef = ((TableController)parentInstance).TableDef;
            tableDef.Fields.Remove(tableDefTwo);
		}

		public override void InsertNode(IXtreeNode parentInstance, int idx)
		{
            tableDef = ((TableController)parentInstance).TableDef;
            tableDef.Fields.Insert(idx, tableDefTwo);
		}

		public override void Select(TreeNode tn)
		{
			//Program.Properties.SelectedObject = tableDefTwo;
		}
	}
}
