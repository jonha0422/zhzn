﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationZH
{

    class CAxisBase
    {

        public AXIS_DES m_AxisInfo;
        public bool m_bHomeStatusOK;

        protected bool m_bAlarmActiveSignal;
        protected bool m_bLimitPositiveActiveSignal;
        protected bool m_bLimitNagativeActiveSignal;
        protected bool m_bIsInpEnabled;

        protected bool m_IsMovingHome;
/*        protected HANDLE m_hThreadMoveHome;*/
        protected UInt16 m_dwThreadMoveHome;

        protected double m_SpeedHome;
        protected double m_SpeedHomeTacc;
        protected long m_SpeedHomeOffset;
        protected long m_HomePos;
        protected int m_nNumAxisPerCard;

        public struct AXIS_DES
        {
            string szDescription;
            uint nCardNumber;
            uint nIndexOfAxis;
            bool bIsEnable;
            double fMove_SpeedStart;                //当前移动轴的启动速度
            double fMove_Speed;                     //当前移动轴的运动速度
            double fMove_Speed_HighSpeed;   //当前移动轴的运动速度
            double fMove_Speed_Tacc;                //当前移动轴的加速
            double fMove_Speed_Tdec;            //当前移动轴的减速时间
            long dwWorkPosition;                    //当前移动轴的工作原点
            double fHelicalPitch;                       //轴丝杠导程 mm//当前移动轴位置 （脉冲数量）
            UInt32 dwNumberPulsePerCircle;   //轴电机每圈所需要的脉冲数量
            double fDistancePerPulse;               //丝杠位移精度 mm
        }

        public CAxisBase()
        {
        }
        ~CAxisBase()
        {

        }

        public virtual bool IntializeAxis(AXIS_DES Axis_CfgInfo)
        {
/*            memcpy(&m_AxisInfo, &Axis_CfgInfo, sizeof(strAXIS_Description));*/
            return true;
        }

        public virtual double GetAxisResolutionPerPulse()
        {
            return 0.00f;
        }
	    public virtual double GetAxisSpeedPerSecond(long dnAxisPPS)
        {
            return 0.00f;
        }
        public virtual double PostionToDistance(long dPulsPostion)
        {
            return 0.00f;
        }
        public virtual long DistanceToPostion(double fDistance)
        {
            return 0;
        }

        public virtual bool EnableAxis(bool bEnable = true)
        {
            return false;
        }
        public virtual bool EnableAlarm(bool bEnable = true)
        {
            return false;
        }
        public virtual bool EnableLimit(bool bEnable = true)
        {
            return false;
        }
        public virtual bool EnableInp(bool bEnable = true)
        {
            return false;
        }

        public bool SetSoftLimit(long dwPositive, long dwNegative)
        {
            return false;
        }

        public virtual bool StatusGet_LimitPositive()
        {
            return false;
        }
        public virtual bool StatusGet_LimitNagative()
        {
            return false;
        }
        public virtual bool StatusGet_Alarm()
        {
            return false;
        }
        public virtual bool StatusGet_AxisEnalbeFlag()
        {
            return false;
        }
        public virtual bool StatusGet_PulseStatus()
        {
            return false;
        }
        public virtual bool StatusGet_Home()
        {
            return false;
        }
        public virtual bool StatusGet_InPosition()
        {
            return false;
        }
        public virtual bool IsAxisCanWorkd()
        {
            return false;
        }

        //状态设置
        public virtual bool Alarm_Clear(bool bIOStatus)
        {
            return false;
        }

        //属性设置
        public virtual bool SetActiveSignal(bool bAlarm = false, bool bLimitPositive = false, bool bLimitNagative = false)
        {
            return false;
        }
        public virtual bool SetSpeed(double fNormal, double fTacc = 0.5, double fTdec = 0.5)/* Hz */
        {
            return false;
        }

        //运动控制
        public virtual bool MoveHome(bool bInit = false, bool bDirection = false, bool bIndex = false, double fSpeed = 10, double fAccSpeed = 0.5, long dwOffset = 0,
                                                                bool bWaitCompleted = true, int nWaitTimeOut = 0)
        {
            return false;
        }
        public virtual bool MoveHomeStop()
        {
            return false;
        }
        public virtual bool IsMoveHomeCompleted()
        {
            return false;
        }

        public virtual bool  IsMoveCompleted()
        {
            return false;
        }
        public virtual bool StopMoving()
        {
            return false;
        }
        public virtual bool GetCurrentPosition(ref double pPos)
        {
            return false;
        }
        public virtual bool TestPosition(long dwPositionForTest)
        {
            return false;
        }

        //角度运动
        public virtual double PostionToAngle(long dPulsPostion)
        {
            return 0.00f;
        }
        public virtual long AngleToPostionNumber(double fAngle)
        {
            return 0;
        }
        public virtual bool SetCurrentAngle(double dAngle, double fSpeed, double fTacc, double fTdec, bool bZeroPosition = true, bool bWaitCompleted = true)
        {
            return false;
        }

        //点运动
        public virtual bool Move_TrapMoving_Relative(long dwPosition, double fSpeed = 10000, double fTacc = 0.3, double fTdec = 0.3, bool bWaitCompleted = true)
        {
            return false;
        }
        public virtual bool Move_TrapMoving_Absolute(long dwPosition, double fSpeed = 10000, double fTacc = 0.3, double fTdec = 0.3, bool bWaitCompleted = true)
        {
            return false;
        }

        //Jog运动
        public virtual bool Move_JogMoving(double fSpeed = 1000, double fTacc = 0.0625, double fTdec = 0.0625)
        {
            return false;
        }
    };
    class MotionCardBase
    {
        public enum emPulseDir
        {
            emPulseDir = 0,
            emCCW_CW = 1
        }

        public	IntPtr hIntPtr;
        public MotionCardBase()
        {

        }
        ~MotionCardBase()
        {

        }

        public virtual IntPtr Initialize()
        {
            return Initialize("");
        }
        public virtual IntPtr Initialize(string szIP)
        {
            return new IntPtr();
        }
        public virtual void Release()
        {

        }

        //属性设置
        public virtual void SetPulseMode(emPulseDir emType)
        {

        }
        public virtual void SetPulseMode(emPulseDir emType, int nAxis)
        {

        }

        // 状态读取
        public virtual int IO_PortIn(int nCard)
        {
            return 0;
        }
        public virtual bool IO_LineIn(int nCard, int nLine)
        {
            return false;
        }

        public virtual void IO_LineOut(int nCard, int nLine, bool bStatus)
        {

        }
        public virtual bool IO_GetLineOut(int nCard, int nLine)
        {
            return false;
        }
    }
}
