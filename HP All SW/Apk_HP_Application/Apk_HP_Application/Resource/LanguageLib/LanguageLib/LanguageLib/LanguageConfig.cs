﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using GenericLib;
using System.Windows.Forms;

namespace LanguageLib
{
    public class LanguageConfig
    {
        public enum EnglishFlagType
        {
            England,
            USA
        }
        private static LanguageConfig instance;
        private static object threadLock = new object();
        public List<Language.lang> Languages;
        public Language.lang ActiveLanguage;
        public EnglishFlagType EnglishFlag;
        [XmlIgnoreAttribute]
        public string Path { get; set; }

        // Version 0 -> 1, "_" -> "[NL]"
        public int Version = 0;

        private LanguageConfig()
        {
            Languages = new List<LanguageDef.lang>();
            ActiveLanguage = LanguageDef.lang.English;
            Path = Application.StartupPath + "\\Config\\Language\\";
        }

        [XmlIgnoreAttribute]
        public static LanguageConfig Instance
        {
            get
            {
                lock (threadLock)
                {
                    if (instance == null)
                    {
                        instance = new LanguageConfig();
                    }
                    return instance;
                }
            }
        }
        public bool Write()
        {
            return ReadWriteObject.Write(instance, Path + "Config.xml");
        }
        public bool Read()
        {
            lock (threadLock)
            {
                instance = (LanguageConfig)ReadWriteObject.Read(typeof(LanguageConfig), Path + "Config.xml");
                return (instance != null);
            }
        }
    }
}
