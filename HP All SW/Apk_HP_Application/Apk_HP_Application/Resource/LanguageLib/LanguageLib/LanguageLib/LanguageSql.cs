﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageLib
{
    class LanguageSql
    {
        /// <summary>
        /// Creates the SQL query to read all data from excel table.
        /// </summary> 
        /// <param name="sheetname">the name of the excel sheet</param>
        ///  
        /// <return>String:the SQL query string</return>
        public String ReadAllString(String sheetname)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Append("Select * From[");
            sqlQuery.Append(sheetname);
            sqlQuery.Append("$]");
            return sqlQuery.ToString();
        }

        /// <summary>
        /// Creates the SQL query to read a dedicated language from the excel table.
        /// It has to be noticed that tag and english text is always read.
        /// </summary> 
        /// <param name="sheetname">the name of the excel sheet</param>
        /// <param name="language">the selected language</param>
        /// 
        /// <return>the SQL query string</return>
        public String ReadLanguageString(String sheetname, Language.lang language)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Append("Select Tag, English, EnglishHelp, ");
            sqlQuery.Append(language.ToString());
            sqlQuery.Append(", ");
            sqlQuery.Append(language.ToString()+LanguageSet.HELP);
            sqlQuery.Append(" From [");
            sqlQuery.Append(sheetname);
            sqlQuery.Append("$]");
            return sqlQuery.ToString();
        }

        /// <summary>
        /// Creates the SQL query to update the data in the dedicated row
        /// of the excel table.
        /// </summary> 
        /// <param name="sheetname">the name of the excel sheet</param>
        /// <param name="column">the name of the column</param>
        /// <param name="newText">the text replacing the old one</param>
        /// <param name="tag">the tag indicating the row</param>
        /// 
        /// <return>String:the SQL update string</return>
        public String UpdateString(String sheetname, String column, String newText, String tag)
        {
            StringBuilder sqlUpdate = new StringBuilder();
            sqlUpdate.Append("Update [");
            sqlUpdate.Append(sheetname);
            sqlUpdate.Append("$] Set ");
            sqlUpdate.Append(column);
            sqlUpdate.Append("='");
            sqlUpdate.Append(newText);
            sqlUpdate.Append("' Where Tag='");
            sqlUpdate.Append(tag);
            sqlUpdate.Append("'");
            return sqlUpdate.ToString();
        }

        /// <summary>
        /// Creates the SQL query to insert a new data row into the excel table.
        /// </summary>
        /// <param name="sheetname">the name of the excel sheet</param>     
        /// <param name="set">the language set object</param>           
        /// 
        /// <return>the SQL insert string</return>  
        public String InsertString(String sheetname, LanguageSet set)
        {
            StringBuilder sqlInsert = new StringBuilder();
            sqlInsert.Append("Insert Into [");
            sqlInsert.Append(sheetname);
            sqlInsert.Append("$] (Tag,English");

            // Add needed additional language definitions.
            Language.lang language = Language.lang.Finnish;
            for (int i = (int)language; i < set.GetDictionarySize(); i++)
            {
                String txt = set.GetText(language.ToString());
                if ( txt != null && txt != "")
                    sqlInsert.Append("," + language.ToString());
                language++;
            }

            sqlInsert.Append(") Values ('");
            sqlInsert.Append(set.GetText("Key"));
            sqlInsert.Append("','");
            sqlInsert.Append(set.GetText("English"));

            // Add needed additional language texts.
            language = Language.lang.Finnish;
            for (int i = (int)language; i < set.GetDictionarySize(); i++)
            {
                String text = set.GetText(language.ToString());
                if ( text != null && text != "")
                {
                    sqlInsert.Append("','");
                    sqlInsert.Append(text);
                }
                language++;
            }
            sqlInsert.Append("')");
            return sqlInsert.ToString();
        }

        /// <summary>
        /// Creates the SQL string to delete all data in the excel-table.
        /// </summary>
        /// <param name="sheetname">the name of the excel sheet</param>          
        /// 
        /// <return>String:the SQL insert string</return>   
        private String sqlDeleteAll(String sheetname)
        {
            StringBuilder sqlDeleteAll = new StringBuilder();
            sqlDeleteAll.Append("Delete /// FROM [");
            sqlDeleteAll.Append(sheetname);
            sqlDeleteAll.Append("$]");
            return sqlDeleteAll.ToString();
        }
    }
}
