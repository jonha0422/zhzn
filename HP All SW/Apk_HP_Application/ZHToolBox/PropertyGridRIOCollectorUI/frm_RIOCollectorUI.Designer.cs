﻿namespace ZHToolBox.PropertyGridRIOCollectorUI
{
    partial class frm_RIOCollectorUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpdateRIO = new System.Windows.Forms.Button();
            this.DeleteRIO = new System.Windows.Forms.Button();
            this.AddRIO = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.行 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txtCol = new System.Windows.Forms.TextBox();
            this.txtRow = new System.Windows.Forms.TextBox();
            this.btmAutoRowCol = new System.Windows.Forms.Button();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // UpdateRIO
            // 
            this.UpdateRIO.Location = new System.Drawing.Point(194, 453);
            this.UpdateRIO.Name = "UpdateRIO";
            this.UpdateRIO.Size = new System.Drawing.Size(138, 37);
            this.UpdateRIO.TabIndex = 0;
            this.UpdateRIO.Text = "Update RIO";
            this.UpdateRIO.UseVisualStyleBackColor = true;
            this.UpdateRIO.Click += new System.EventHandler(this.UpdateRIO_Click);
            // 
            // DeleteRIO
            // 
            this.DeleteRIO.Location = new System.Drawing.Point(194, 496);
            this.DeleteRIO.Name = "DeleteRIO";
            this.DeleteRIO.Size = new System.Drawing.Size(138, 37);
            this.DeleteRIO.TabIndex = 1;
            this.DeleteRIO.Text = "Delete RIO";
            this.DeleteRIO.UseVisualStyleBackColor = true;
            this.DeleteRIO.Click += new System.EventHandler(this.DeleteRIO_Click);
            // 
            // AddRIO
            // 
            this.AddRIO.Location = new System.Drawing.Point(194, 412);
            this.AddRIO.Name = "AddRIO";
            this.AddRIO.Size = new System.Drawing.Size(138, 35);
            this.AddRIO.TabIndex = 2;
            this.AddRIO.Text = "Add RIO";
            this.AddRIO.UseVisualStyleBackColor = true;
            this.AddRIO.Click += new System.EventHandler(this.AddRIO_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1031, 728);
            this.panel1.TabIndex = 3;
            this.panel1.Resize += new System.EventHandler(this.panel1_Resize);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(3, 411);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(185, 281);
            this.listView1.TabIndex = 4;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.行);
            this.panel3.Controls.Add(this.comboBox1);
            this.panel3.Controls.Add(this.txtCol);
            this.panel3.Controls.Add(this.txtRow);
            this.panel3.Controls.Add(this.btmAutoRowCol);
            this.panel3.Controls.Add(this.propertyGrid1);
            this.panel3.Controls.Add(this.listView1);
            this.panel3.Controls.Add(this.DeleteRIO);
            this.panel3.Controls.Add(this.UpdateRIO);
            this.panel3.Controls.Add(this.AddRIO);
            this.panel3.Location = new System.Drawing.Point(1037, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(335, 725);
            this.panel3.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 702);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "方向";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(240, 675);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "例";
            // 
            // 行
            // 
            this.行.AutoSize = true;
            this.行.Location = new System.Drawing.Point(240, 647);
            this.行.Name = "行";
            this.行.Size = new System.Drawing.Size(20, 17);
            this.行.TabIndex = 10;
            this.行.Text = "行";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "左上右下, 往右 0",
            "左上右下, 往下 1",
            "右上左下, 往左 2",
            "右上左下, 往下 3",
            "左下右上, 往右 4",
            "左下右上, 往上 5",
            "右下左上, 往左 6",
            "右下左上, 往上 7"});
            this.comboBox1.Location = new System.Drawing.Point(194, 697);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(126, 25);
            this.comboBox1.TabIndex = 9;
            // 
            // txtCol
            // 
            this.txtCol.Location = new System.Drawing.Point(266, 672);
            this.txtCol.Name = "txtCol";
            this.txtCol.Size = new System.Drawing.Size(54, 23);
            this.txtCol.TabIndex = 8;
            this.txtCol.Text = "4";
            // 
            // txtRow
            // 
            this.txtRow.Location = new System.Drawing.Point(266, 644);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(54, 23);
            this.txtRow.TabIndex = 7;
            this.txtRow.Text = "4";
            // 
            // btmAutoRowCol
            // 
            this.btmAutoRowCol.Location = new System.Drawing.Point(194, 586);
            this.btmAutoRowCol.Name = "btmAutoRowCol";
            this.btmAutoRowCol.Size = new System.Drawing.Size(138, 52);
            this.btmAutoRowCol.TabIndex = 6;
            this.btmAutoRowCol.Text = "将当前区域自动分行例";
            this.btmAutoRowCol.UseVisualStyleBackColor = true;
            this.btmAutoRowCol.Click += new System.EventHandler(this.btmAutoRowCol_Click);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Location = new System.Drawing.Point(3, 3);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(329, 402);
            this.propertyGrid1.TabIndex = 5;
            // 
            // frm_RIOCollectorUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 861);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_RIOCollectorUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_RIOCollectorUI";
            this.MaximumSizeChanged += new System.EventHandler(this.frm_RIOCollectorUI_MaximumSizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_RIOCollectorUI_FormClosing);
            this.Load += new System.EventHandler(this.frm_RIOCollectorUI_Load);
            this.ResizeEnd += new System.EventHandler(this.frm_RIOCollectorUI_ResizeEnd);
            this.SizeChanged += new System.EventHandler(this.frm_RIOCollectorUI_SizeChanged);
            this.Resize += new System.EventHandler(this.frm_RIOCollectorUI_Resize);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button UpdateRIO;
        private System.Windows.Forms.Button DeleteRIO;
        private System.Windows.Forms.Button AddRIO;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox txtCol;
        private System.Windows.Forms.TextBox txtRow;
        private System.Windows.Forms.Button btmAutoRowCol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label 行;
        private System.Windows.Forms.Label label2;
    }
}