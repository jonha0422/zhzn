﻿namespace Apk_HP_Application
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.运行选项 = new DevExpress.XtraEditors.GroupControl();
            this.机器人下料控制软件 = new System.Windows.Forms.RadioButton();
            this.打码切割软件 = new System.Windows.Forms.RadioButton();
            this.上料视觉软件 = new System.Windows.Forms.RadioButton();
            this.调度系统 = new System.Windows.Forms.RadioButton();
            this.chkEnableRemoteDebug = new DevExpress.XtraEditors.CheckEdit();
            this.chkDisableAllPLCEvent = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtUser = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPsw = new System.Windows.Forms.TextBox();
            this.btmNext = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.ComboBox();
            this.btmLoad = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.运行选项)).BeginInit();
            this.运行选项.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableRemoteDebug.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisableAllPLCEvent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelControl1.Appearance.Options.UseFont = true;
            this.panelControl1.Controls.Add(this.memoEdit1);
            this.panelControl1.Controls.Add(this.progressBarControl1);
            this.panelControl1.Controls.Add(this.groupControl2);
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.btmNext);
            this.panelControl1.Controls.Add(this.button1);
            this.panelControl1.Controls.Add(this.textBox2);
            this.panelControl1.Controls.Add(this.btmLoad);
            this.panelControl1.Controls.Add(this.label3);
            this.panelControl1.Location = new System.Drawing.Point(-6, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(807, 415);
            this.panelControl1.TabIndex = 0;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(439, 202);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ReadOnly = true;
            this.memoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.memoEdit1.Size = new System.Drawing.Size(352, 208);
            this.memoEdit1.TabIndex = 33;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(8, 377);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Size = new System.Drawing.Size(425, 33);
            this.progressBarControl1.TabIndex = 31;
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.运行选项);
            this.groupControl2.Controls.Add(this.chkEnableRemoteDebug);
            this.groupControl2.Controls.Add(this.chkDisableAllPLCEvent);
            this.groupControl2.Location = new System.Drawing.Point(10, 181);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(423, 190);
            this.groupControl2.TabIndex = 30;
            this.groupControl2.Text = "调度软件运行选项";
            // 
            // 运行选项
            // 
            this.运行选项.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.运行选项.Appearance.Options.UseFont = true;
            this.运行选项.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.运行选项.AppearanceCaption.Options.UseFont = true;
            this.运行选项.Controls.Add(this.机器人下料控制软件);
            this.运行选项.Controls.Add(this.打码切割软件);
            this.运行选项.Controls.Add(this.上料视觉软件);
            this.运行选项.Controls.Add(this.调度系统);
            this.运行选项.Location = new System.Drawing.Point(8, 25);
            this.运行选项.Name = "运行选项";
            this.运行选项.Size = new System.Drawing.Size(211, 160);
            this.运行选项.TabIndex = 31;
            this.运行选项.Text = "运行选项";
            // 
            // 机器人下料控制软件
            // 
            this.机器人下料控制软件.AutoSize = true;
            this.机器人下料控制软件.Location = new System.Drawing.Point(10, 118);
            this.机器人下料控制软件.Name = "机器人下料控制软件";
            this.机器人下料控制软件.Size = new System.Drawing.Size(134, 21);
            this.机器人下料控制软件.TabIndex = 3;
            this.机器人下料控制软件.TabStop = true;
            this.机器人下料控制软件.Text = "机器人下料控制软件";
            this.机器人下料控制软件.UseVisualStyleBackColor = true;
            this.机器人下料控制软件.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            this.机器人下料控制软件.Click += new System.EventHandler(this.btmSelectProgram);
            // 
            // 打码切割软件
            // 
            this.打码切割软件.AutoSize = true;
            this.打码切割软件.Location = new System.Drawing.Point(10, 91);
            this.打码切割软件.Name = "打码切割软件";
            this.打码切割软件.Size = new System.Drawing.Size(98, 21);
            this.打码切割软件.TabIndex = 2;
            this.打码切割软件.TabStop = true;
            this.打码切割软件.Text = "打码切割软件";
            this.打码切割软件.UseVisualStyleBackColor = true;
            this.打码切割软件.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            this.打码切割软件.Click += new System.EventHandler(this.btmSelectProgram);
            // 
            // 上料视觉软件
            // 
            this.上料视觉软件.AutoSize = true;
            this.上料视觉软件.Location = new System.Drawing.Point(10, 64);
            this.上料视觉软件.Name = "上料视觉软件";
            this.上料视觉软件.Size = new System.Drawing.Size(98, 21);
            this.上料视觉软件.TabIndex = 1;
            this.上料视觉软件.TabStop = true;
            this.上料视觉软件.Text = "上料视觉软件";
            this.上料视觉软件.UseVisualStyleBackColor = true;
            this.上料视觉软件.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            this.上料视觉软件.Click += new System.EventHandler(this.btmSelectProgram);
            // 
            // 调度系统
            // 
            this.调度系统.AutoSize = true;
            this.调度系统.Location = new System.Drawing.Point(10, 37);
            this.调度系统.Name = "调度系统";
            this.调度系统.Size = new System.Drawing.Size(74, 21);
            this.调度系统.TabIndex = 0;
            this.调度系统.TabStop = true;
            this.调度系统.Text = "调度系统";
            this.调度系统.UseVisualStyleBackColor = true;
            this.调度系统.Click += new System.EventHandler(this.btmSelectProgram);
            // 
            // chkEnableRemoteDebug
            // 
            this.chkEnableRemoteDebug.Location = new System.Drawing.Point(225, 60);
            this.chkEnableRemoteDebug.Name = "chkEnableRemoteDebug";
            this.chkEnableRemoteDebug.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEnableRemoteDebug.Properties.Appearance.Options.UseFont = true;
            this.chkEnableRemoteDebug.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableRemoteDebug.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkEnableRemoteDebug.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableRemoteDebug.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkEnableRemoteDebug.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkEnableRemoteDebug.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkEnableRemoteDebug.Properties.Caption = "启动远程调试模试";
            this.chkEnableRemoteDebug.Size = new System.Drawing.Size(133, 21);
            this.chkEnableRemoteDebug.TabIndex = 7;
            this.chkEnableRemoteDebug.CheckedChanged += new System.EventHandler(this.chkEnableRemoteDebug_CheckedChanged);
            // 
            // chkDisableAllPLCEvent
            // 
            this.chkDisableAllPLCEvent.Location = new System.Drawing.Point(225, 38);
            this.chkDisableAllPLCEvent.Name = "chkDisableAllPLCEvent";
            this.chkDisableAllPLCEvent.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDisableAllPLCEvent.Properties.Appearance.Options.UseFont = true;
            this.chkDisableAllPLCEvent.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkDisableAllPLCEvent.Properties.AppearanceDisabled.Options.UseFont = true;
            this.chkDisableAllPLCEvent.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkDisableAllPLCEvent.Properties.AppearanceFocused.Options.UseFont = true;
            this.chkDisableAllPLCEvent.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.chkDisableAllPLCEvent.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.chkDisableAllPLCEvent.Properties.Caption = "禁用所有PLC调度事件";
            this.chkDisableAllPLCEvent.Size = new System.Drawing.Size(153, 21);
            this.chkDisableAllPLCEvent.TabIndex = 30;
            this.chkDisableAllPLCEvent.CheckedChanged += new System.EventHandler(this.chkDisableAllPLCEvent_CheckedChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.txtUser);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.txtPsw);
            this.groupControl1.Location = new System.Drawing.Point(10, 68);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(423, 107);
            this.groupControl1.TabIndex = 29;
            this.groupControl1.Text = "登录信息";
            // 
            // txtUser
            // 
            this.txtUser.FormattingEnabled = true;
            this.txtUser.Items.AddRange(new object[] {
            "Operator",
            "Administrator"});
            this.txtUser.Location = new System.Drawing.Point(57, 36);
            this.txtUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(171, 25);
            this.txtUser.TabIndex = 6;
            this.txtUser.Text = "Administrator";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "用户名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "密码";
            // 
            // txtPsw
            // 
            this.txtPsw.Location = new System.Drawing.Point(57, 67);
            this.txtPsw.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPsw.Name = "txtPsw";
            this.txtPsw.Size = new System.Drawing.Size(171, 23);
            this.txtPsw.TabIndex = 4;
            this.txtPsw.UseSystemPasswordChar = true;
            // 
            // btmNext
            // 
            this.btmNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btmNext.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmNext.Location = new System.Drawing.Point(454, 68);
            this.btmNext.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.btmNext.Name = "btmNext";
            this.btmNext.Size = new System.Drawing.Size(163, 125);
            this.btmNext.TabIndex = 22;
            this.btmNext.Text = "Next(继续)";
            this.btmNext.UseVisualStyleBackColor = false;
            this.btmNext.Click += new System.EventHandler(this.btmNext_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button1.Location = new System.Drawing.Point(623, 68);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 61);
            this.button1.TabIndex = 23;
            this.button1.Text = "Quit(退出)";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // textBox2
            // 
            this.textBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.textBox2.FormattingEnabled = true;
            this.textBox2.Location = new System.Drawing.Point(10, 36);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(702, 25);
            this.textBox2.TabIndex = 27;
            this.textBox2.SelectedIndexChanged += new System.EventHandler(this.textBox2_SelectedIndexChanged);
            // 
            // btmLoad
            // 
            this.btmLoad.BackColor = System.Drawing.Color.Moccasin;
            this.btmLoad.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btmLoad.Location = new System.Drawing.Point(713, 34);
            this.btmLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btmLoad.Name = "btmLoad";
            this.btmLoad.Size = new System.Drawing.Size(74, 29);
            this.btmLoad.TabIndex = 26;
            this.btmLoad.Text = "打开(&O)";
            this.btmLoad.UseVisualStyleBackColor = false;
            this.btmLoad.Click += new System.EventHandler(this.btmLoad_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "请选择调度参数配制及通讯配制文件";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 410);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "请输入用户和密码登录系统";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.运行选项)).EndInit();
            this.运行选项.ResumeLayout(false);
            this.运行选项.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkEnableRemoteDebug.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisableAllPLCEvent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.Button btmNext;
        private System.Windows.Forms.ComboBox txtUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPsw;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox textBox2;
        private System.Windows.Forms.Button btmLoad;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.CheckEdit chkDisableAllPLCEvent;
        private DevExpress.XtraEditors.CheckEdit chkEnableRemoteDebug;
        private DevExpress.XtraEditors.GroupControl 运行选项;
        private System.Windows.Forms.RadioButton 机器人下料控制软件;
        private System.Windows.Forms.RadioButton 打码切割软件;
        private System.Windows.Forms.RadioButton 上料视觉软件;
        private System.Windows.Forms.RadioButton 调度系统;
    }
}