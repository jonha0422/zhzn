﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LanguageLib
{
    public partial class ucSelectLanguage : UserControl
    {
        public ucSelectLanguage()
        {
            InitializeComponent();
        }

        public event EventHandler<SelectLanguageEventArgs> LanguageSelected;

        private void ucSelectLanguage_Load(object sender, EventArgs e)
        {
            BorderStyle = BorderStyle.None;

            LanguageConfig.Instance.Read();
            List<LanguageDef.lang> conf = LanguageConfig.Instance.Languages;

            for (int i = 0; i < conf.Count; i++)
			{
                PictureBox pBox = null;
                switch (conf[i])
                {
                    case LanguageDef.lang.English:
                        if (LanguageConfig.Instance.EnglishFlag == LanguageConfig.EnglishFlagType.USA)
                        {
                            pBox = pEnglishUSA;
                        }
                        else
                        {
                            pBox = pEnglish;
                        }
                        break;
                    case LanguageDef.lang.Finnish:
                        pBox = pFinnish;
                        break;
                    case LanguageDef.lang.Swedish:
                        pBox = pSwedish;
                        break;
                    case LanguageDef.lang.Chinese:
                        pBox = pChinese;
                        break;
                    case LanguageDef.lang.Russian:
                        pBox = pRussian;
                        break;
                    case LanguageDef.lang.Danish:
                        pBox = pDanish;
                        break;
                    case LanguageDef.lang.French:
                        pBox = pFrensh;
                        break;
                    case LanguageDef.lang.German:
                        pBox = pGerman;
                        break;
                    case LanguageDef.lang.Spanish:
                        pBox = pSpanish;
                        break;
                    case LanguageDef.lang.Portuguese:
                        pBox = pPortuguese;
                        break;
                    case LanguageDef.lang.Estonian:
                        pBox = pEstonian;
                        break;
                    case LanguageDef.lang.Polish:
                        pBox = pPolish;
                        break;
                    case LanguageDef.lang.Hungarian:
                        pBox = pHungarian;
                        break;
                    case LanguageDef.lang.Bulgarian:
                        pBox = pBulgarian;
                        break;
                    case LanguageDef.lang.Japanese:
                        pBox = pJapanese;
                        break;
                    default:
                        break;
                }
                if (pBox != null)
                {
                    pBox.Location = new Point(i * 38, 0);
                    pBox.Visible = true;
                    if (conf[i] == LanguageConfig.Instance.ActiveLanguage)
                    {
                        pBox.BorderStyle = BorderStyle.FixedSingle;
                    }
                }
			}
        }

        private void selectLanguage(LanguageDef.lang lang, PictureBox pBox)
        {
            Language.Instance.ReadLanguageFromXml();
            Language.Instance.SetLanguage(lang);
            LanguageConfig.Instance.ActiveLanguage = lang;
            LanguageConfig.Instance.Write();
            if (LanguageSelected != null)
            {
                LanguageSelected(this, new SelectLanguageEventArgs(lang));
            }
            foreach (Control item in Controls)
            {
                if (item.GetType() == typeof(PictureBox))
                {
                    PictureBox box = (PictureBox)item;
                    box.BorderStyle = BorderStyle.None;
                }
            }
            pBox.BorderStyle = BorderStyle.FixedSingle;
        }

        private void pEnglish_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.English, pEnglish);
        }

        private void pGerman_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.German, pGerman);
        }

        private void pFinnish_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Finnish, pFinnish);
        }

        private void pFrensh_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.French, pFrensh);
        }

        private void pRussian_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Russian, pRussian);
        }

        private void pSwedish_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Swedish, pSwedish);
        }

        private void pDanish_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Danish, pDanish);
        }

        private void pChinese_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Chinese, pChinese);
        }

        private void pSpanish_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Spanish, pSpanish);
        }

        private void pPortuguese_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Portuguese, pPortuguese);
        }

        private void pEstonian_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Estonian, pEstonian);
        }

        private void pPolish_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Polish, pPolish);
        }

        private void pHungarian_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Hungarian, pHungarian);
        }

        private void pBulgarian_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Bulgarian, pBulgarian);
        }

        private void pJapanese_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.Japanese, pJapanese);
        }

        private void pEnglishUSA_Click(object sender, EventArgs e)
        {
            selectLanguage(LanguageDef.lang.English, pEnglishUSA);
        }
    }
    public class SelectLanguageEventArgs : EventArgs
    {
        public Language.lang Language { get; set; }

        public SelectLanguageEventArgs(Language.lang language) : base()
        {
            Language = language;
        }
    }
}
