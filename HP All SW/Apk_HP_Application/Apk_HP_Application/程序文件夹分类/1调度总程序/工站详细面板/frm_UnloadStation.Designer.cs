﻿namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    partial class frm_UnloadStation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UnloadStation));
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.启用禁用 = new DevExpress.XtraEditors.CheckButton();
            this.复位 = new DevExpress.XtraEditors.SimpleButton();
            this.停止 = new DevExpress.XtraEditors.SimpleButton();
            this.错误清除 = new DevExpress.XtraEditors.SimpleButton();
            this.启动 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LEDRobot = new HslCommunication.Controls.UserLantern();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.richTextCamera1 = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl6.AppearanceCaption.Options.UseFont = true;
            this.groupControl6.Controls.Add(this.启用禁用);
            this.groupControl6.Controls.Add(this.复位);
            this.groupControl6.Controls.Add(this.停止);
            this.groupControl6.Controls.Add(this.错误清除);
            this.groupControl6.Controls.Add(this.启动);
            this.groupControl6.Location = new System.Drawing.Point(198, 1);
            this.groupControl6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(316, 265);
            this.groupControl6.TabIndex = 125;
            this.groupControl6.Text = "操作(仅限于下料组盘电脑)";
            // 
            // 启用禁用
            // 
            this.启用禁用.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.启用禁用.Appearance.Options.UseFont = true;
            this.启用禁用.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearanceDisabled.Options.UseFont = true;
            this.启用禁用.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearanceHovered.Options.UseFont = true;
            this.启用禁用.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启用禁用.AppearancePressed.Options.UseFont = true;
            this.启用禁用.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.启用禁用.Checked = true;
            this.启用禁用.Enabled = false;
            this.启用禁用.Location = new System.Drawing.Point(7, 33);
            this.启用禁用.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.启用禁用.Name = "启用禁用";
            this.启用禁用.Size = new System.Drawing.Size(76, 51);
            this.启用禁用.TabIndex = 173;
            this.启用禁用.Text = "启用/禁用";
            this.启用禁用.CheckedChanged += new System.EventHandler(this.启用禁用_CheckedChanged);
            // 
            // 复位
            // 
            this.复位.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.复位.Appearance.Options.UseFont = true;
            this.复位.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.复位.AppearanceDisabled.Options.UseFont = true;
            this.复位.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.复位.AppearanceHovered.Options.UseFont = true;
            this.复位.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.复位.AppearancePressed.Options.UseFont = true;
            this.复位.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("复位.ImageOptions.Image")));
            this.复位.Location = new System.Drawing.Point(4, 208);
            this.复位.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.复位.Name = "复位";
            this.复位.Size = new System.Drawing.Size(167, 49);
            this.复位.TabIndex = 135;
            this.复位.Text = "复位";
            this.复位.Click += new System.EventHandler(this.复位_Click);
            // 
            // 停止
            // 
            this.停止.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.停止.Appearance.Options.UseFont = true;
            this.停止.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearanceDisabled.Options.UseFont = true;
            this.停止.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearanceHovered.Options.UseFont = true;
            this.停止.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.停止.AppearancePressed.Options.UseFont = true;
            this.停止.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("停止.ImageOptions.Image")));
            this.停止.Location = new System.Drawing.Point(89, 30);
            this.停止.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.停止.Name = "停止";
            this.停止.Size = new System.Drawing.Size(84, 55);
            this.停止.TabIndex = 132;
            this.停止.Text = "停止";
            this.停止.Click += new System.EventHandler(this.停止_Click);
            // 
            // 错误清除
            // 
            this.错误清除.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.错误清除.Appearance.Options.UseFont = true;
            this.错误清除.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearanceDisabled.Options.UseFont = true;
            this.错误清除.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearanceHovered.Options.UseFont = true;
            this.错误清除.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.错误清除.AppearancePressed.Options.UseFont = true;
            this.错误清除.Enabled = false;
            this.错误清除.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("错误清除.ImageOptions.Image")));
            this.错误清除.Location = new System.Drawing.Point(4, 146);
            this.错误清除.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.错误清除.Name = "错误清除";
            this.错误清除.Size = new System.Drawing.Size(79, 55);
            this.错误清除.TabIndex = 131;
            this.错误清除.Text = "错误清除";
            this.错误清除.Click += new System.EventHandler(this.错误清除_Click);
            // 
            // 启动
            // 
            this.启动.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.启动.Appearance.Options.UseFont = true;
            this.启动.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearanceDisabled.Options.UseFont = true;
            this.启动.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearanceHovered.Options.UseFont = true;
            this.启动.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.启动.AppearancePressed.Options.UseFont = true;
            this.启动.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("启动.ImageOptions.Image")));
            this.启动.Location = new System.Drawing.Point(7, 87);
            this.启动.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.启动.Name = "启动";
            this.启动.Size = new System.Drawing.Size(76, 55);
            this.启动.TabIndex = 130;
            this.启动.Text = "启动";
            this.启动.Click += new System.EventHandler(this.启动_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceDisabled.Options.UseFont = true;
            this.labelControl5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearanceHovered.Options.UseFont = true;
            this.labelControl5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl5.AppearancePressed.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(24, 67);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(120, 17);
            this.labelControl5.TabIndex = 188;
            this.labelControl5.Text = "机器人服务端连接状态";
            // 
            // LEDRobot
            // 
            this.LEDRobot.BackColor = System.Drawing.Color.Transparent;
            this.LEDRobot.LanternBackground = System.Drawing.Color.Gray;
            this.LEDRobot.Location = new System.Drawing.Point(61, 21);
            this.LEDRobot.Margin = new System.Windows.Forms.Padding(394, 9179, 394, 9179);
            this.LEDRobot.Name = "LEDRobot";
            this.LEDRobot.Size = new System.Drawing.Size(32, 39);
            this.LEDRobot.TabIndex = 189;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 250;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // richTextCamera1
            // 
            this.richTextCamera1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.richTextCamera1.Location = new System.Drawing.Point(12, 324);
            this.richTextCamera1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextCamera1.Name = "richTextCamera1";
            this.richTextCamera1.Size = new System.Drawing.Size(331, 387);
            this.richTextCamera1.TabIndex = 193;
            this.richTextCamera1.Text = "";
            this.richTextCamera1.WordWrap = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.LightSalmon;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(12, 301);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 19);
            this.label10.TabIndex = 194;
            this.label10.Text = "包装数据交互运行信息输出";
            // 
            // frm_UnloadStation
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 911);
            this.Controls.Add(this.richTextCamera1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.groupControl6);
            this.Controls.Add(this.LEDRobot);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_UnloadStation";
            this.Text = "frm_UnloadStation";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private HslCommunication.Controls.UserLantern LEDRobot;
        private DevExpress.XtraEditors.CheckButton 启用禁用;
        private DevExpress.XtraEditors.SimpleButton 复位;
        private DevExpress.XtraEditors.SimpleButton 停止;
        private DevExpress.XtraEditors.SimpleButton 错误清除;
        private DevExpress.XtraEditors.SimpleButton 启动;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.RichTextBox richTextCamera1;
        private System.Windows.Forms.Label label10;
    }
}