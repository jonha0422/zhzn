﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace LanguageLib
{
    public class ShowMessage
    {
        public static DialogResult Show(string msg, String caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            string strMsg = Language.Instance.GetTextTip(msg);
            string strCaption = Language.Instance.GetTextTip(caption);
            return MessageBox.Show(strMsg, strCaption, buttons, icon);
        }
    }
}
