using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

/*
 * The SchemaDef is a collection of serialization classes that also support design-time
 * editing of the collections and properties, primarily to be used with the property grid.
 */

namespace daitou
{
	/// <summary>
	/// Supported field types, used by the schema to define table field.
	/// </summary>
	public enum FieldType
	{
		String,
		Integer,
		Real,
		Money,
		Boolean,
		Date,
		Time,
		Guid,
		DateTime,
	}

	/// <summary>
	/// Supports the declarative instantiation of the table/view schema definition.
	/// </summary>
	[DefaultProperty("Name")]
	public class SchemaDef
	{
		protected List<TableDef> tables;
		protected string name;
        protected string pridata;
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public List<TableDef> Tables
		{
			get { return tables; }
		}

        public string Pridata
        {
            get { return pridata; }
            set { pridata = value; }
        }
		public SchemaDef()
		{
			tables = new List<TableDef>();
			name = "�ͺ�ѡ��";
		}

		public TableDef GetTable(string name)
		{
			TableDef ret=null;

			foreach (TableDef td in tables)
			{
				if (td.Name == name)
				{
					ret = td;
					break;
				}
			}
			return ret;
		}

		public string[] GetTableNames()
		{
			string[] strTables = new string[tables.Count];
			for (int i = 0; i < tables.Count; i++)
			{
				strTables[i] = tables[i].Name;
			}
			return strTables;
		}
		public string[] GetFieldNames(string tableName)
		{
			string[] ret = null;

			foreach (TableDef table in tables)
			{
				if (table.Name == tableName)
				{
					ret = new string[table.Fields.Count];

					for (int i = 0; i < table.Fields.Count; i++)
					{
						ret[i] = table.Fields[i].Name;	
					}

					break;
				}
			}
			return ret;
		}
	}

	public class TableDef
	{
		protected string name;
		protected string comments;
		protected List<TableDefTwo> fields;
		public List<TableDefTwo> Fields
		{
			get { return fields; }
		}
		public string Name
		{
			get { return name; }
			set { name = value; }
		}
		public string Comments
		{
			get { return comments; }
			set { comments = value; }
		}
        protected string pridata;
        public string Pridata
        {
            get { return pridata; }
            set { pridata = value; }
        }
		public TableDef()
		{
			fields = new List<TableDefTwo>();
			name = "Table";
			comments = String.Empty;
		}
	}

    public class TableDefTwo
    {
        protected string name;
        protected string pridata;
        protected List<TableDefThree> fields;
        public List<TableDefThree> Fields
        {
            get { return fields; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Pridata
        {
            get { return pridata; }
            set { pridata = value; }
        }
        public TableDefTwo()
        {
            fields = new List<TableDefThree>();
            name = "TableTwo";
        }
    }
    public class TableDefThree
    {
        protected string name;
        protected List<TableDefFour> fields;
        public List<TableDefFour> Fields
        {
            get { return fields; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        protected string pridata;
        public string Pridata
        {
            get { return pridata; }
            set { pridata = value; }
        }
        public TableDefThree()
        {
            fields = new List<TableDefFour>();
            name = "TableThree";
        }
    }
    public class TableDefFour
    {
        protected string name;
        protected List<TableDefFive> fields;
        public List<TableDefFive> Fields
        {
            get { return fields; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        protected string pridata;
        public string Pridata
        {
            get { return pridata; }
            set { pridata = value; }
        }
        public TableDefFour()
        {
            fields = new List<TableDefFive>();
            name = "TableFour";
        }
    }
    public class TableDefFive
    {
        protected string name;
        protected List<TableFieldDef> fields;
        public List<TableFieldDef> Fields
        {
            get { return fields; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        protected string pridata;
        public string Pridata
        {
            get { return pridata; }
            set { pridata = value; }
        }
        public TableDefFive()
        {
            fields = new List<TableFieldDef>();
            name = "TableFive";
        }
    }

    public class TableFieldDef
	{
		protected string name;
		protected FieldType dataType;
		public FieldType DataType
		{
			get { return dataType; }
			set { dataType = value; }
		}
		public string Name
		{
			get { return name; }
			set { name = value; }
		}
        protected string pridata;
        public string Pridata
        {
            get { return pridata; }
            set { pridata = value; }
        }
		public TableFieldDef()
		{
			dataType = FieldType.String;
			name = "Field";
		}
	}


}
