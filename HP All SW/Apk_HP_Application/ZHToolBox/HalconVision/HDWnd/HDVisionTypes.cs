﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using HalconDotNet;
using System.Windows;
using System.Windows.Forms;
using System.ComponentModel;
using System.Xml.Serialization;

namespace ZHToolBox.HalconVision
{
    public class HDVisionTypes
    {
        //=======================Vision Comm=======================
        //相机品牌
        public enum CameraBrand
        {
            CAMERA_BRAND_NO_DEFINE = -1,
            CAMERA_BRAND_BASLER = 0,
            CAMERA_BRAND_AVT = 1,
            CAMERA_BRAND_BAUMER = 2,
            CAMERA_BRAND_AVTGigE = 3,
        }

        //相机型号 
        public enum CameraType
        {
            CAMERA_TYPE_NO_DEFINE = -1,
            CAMERA_TYPE_Gige = 0,
            CAMERA_TYPE_Usb = 1,
        }

        //Trigger Mode 
        public enum TriggerMode
        {
            TRIGGER_MODEL_NOT_DEFINE = -1,
            TRIGGER_MODEL_OFF = 0,
            TRIGGER_MODEL_ON = 1,
        }

        //TringSource
        public enum TriggerSource
        {
            TRIGGER_SOURCE_NOT_DEFINE = -1,
            TRIGGER_SOURCE_SOFTWARE = 0,
            TRIGGER_SOURCE_EXTERNER_LINE1 = 1,
            TRIGGER_SOURCE_EXTERNER_LINE2 = 2,
            TRIGGER_SOURCE_FREERUN = 3,
        }

        public enum LineInput
        {
            LineInput_Line1,
            LineInput_Line2,
            LineInput_Line3,
            LineInput_Line4,

        }

        public enum LineOutput
        {
            LineOutput_Out1,
            LineOutput_Out2,
            LineOutput_Out3,
            LineOutput_Out4,
        }

        public const int CAMERA_TIEMOUT = 5;

        //         struct tg_Multi_Trigger_Image
        //         {
        //             void* pCamera;
        //             TriggerMode emTriggerMode;
        //             TriggerSource emTriggerSource;
        //             HBITMAP* parryBitmap;
        //             int nCountOfImage;
        //             DWORD nTimeout;
        //         }
        // 
        // typedef int ( CALLBACK* CAPTURE_FRAME_PROC)();

        //         public delegate int CAPTURE_FRAME_PROC(const unsigned char* pImageBuffer, Int32 nWidth, Int32 nHeight, Int32 nChannel);
        //         struct CameraTrigger
        //         {
        //             public static extern Int16 bTriggerMode;
        //             int nTriggerSource;
        //         }

        public enum CameraColorFormat
        {
            ColorFormatNotDefine = -1,
            ColorFormatMono8 = 0x01,
            ColorFormatRGB8 = 0x02,
            ColorFormatBGR8 = 0x03,
            ColorFormatBayerGR8 = 0x04,
            ColorFormatBayerRG8 = 0x05,
            ColorFormatBayerGB8 = 0x06,
            ColorFormatBayerBG8 = 0x07,
        }

        // 定义一个线段的结构体
        public class LINE_DES
        {
            public double fRowStart1;
            public double fColStart1;
            public double fRowEnd1;
            public double fColEnd1;
            public double fLength;
            public double fCenternRow;
            public double fCenternCol;
            public double fAngle;
        }

        // 定义RIO结构体
        public class RIO_DES
        {
            [Category("可变举行坐标"), DisplayName("Row")]
            public double fRow { get; set; }

            [Category("可变举行坐标"), DisplayName("Col")]
            public double fCol { get; set; }

            [Category("可变举行坐标"), DisplayName("Phi")]
            public double fPhi { get; set; }

            [Category("可变举行坐标"), DisplayName("Len1 (Row Len)")]
            public double fLen1 { get; set; }

            [Category("可变举行坐标"), DisplayName("Len2 (Col Len)")]
            public double fLen2 { get; set; }

            [Category("可变举行坐标"), DisplayName("备注")]
            public string szItemRemark { get; set; }

            [Category("圆"), DisplayName("Radix")]
            public double fRadix { get; set; }

            [Category("圆"), DisplayName("RowStart")]
            public double fRowStart { get; set; }

            [Category("圆"), DisplayName("ColStart")]
            public double fColStart { get; set; }

            [Category("圆/椭圆"), DisplayName("RowEnd")]
            public double fRowEnd { get; set; }

            [Category("圆/椭圆"), DisplayName("RowEnd")]
            public double fColEnd { get; set; }

            public RIO_DES()
            {
                fRow = -1.00; fCol = -1.00; fPhi = -1.00; fLen1 = -1.00; fLen2 = -1.00; szItemRemark = ""; fRadix = -1.00; fRowStart = -1.00; fColStart = -1.00; fRowEnd = -1.00; fColEnd = -1.00;
            }

            public void InitRIO() { fRow = -1.00; fCol = -1.00; fPhi = -1.00; fLen1 = -1.00; fLen2 = -1.00; szItemRemark = ""; fRadix = -1.00; fRowStart = -1.00; fColStart = -1.00; fRowEnd = -1.00; fColEnd = -1.00; }

        }

        public class CIRCLE_DES
        {
            public HObject CircleImage;
            public Int32 fArea;
            public double fRow;
            public double fCol;
            public double fRadius;
        }

        public class Rectangle_DES
        {
            public HObject RectangleImage;
            public Int32 fArea;
            public HTuple fRow;
            public HTuple fCol;
            public HTuple fPhi;
            public HTuple fLen1;
            public HTuple fLen2;
        }

        public enum ShapeModeMatchType
        {
            ShapeModeType_ShapeBase = 0, //基础外形匹配 (不可缩放比例）
            ShapeModeType_ShapeBase_XLD = 1,                //基础外形XLD匹配 (不可缩放比例）
            ShapeModeType_Scaled = 2,               //基础外形匹配 (可缩放比例）
            ShapeModeType_Scaled_XLD = 3,               //基础外形XLD匹配 (可缩放比例）
            ShapeModeType_AnisoScaled = 4,   //基础外形匹配 (X, Y 方向可缩放比例）
            ShapeModeType_AnisoScaled_XLD = 5,				//基础外形XLD匹配 (X, Y 方向可缩放比例）
        }

        public const int RIO_TYPE_RECTANGLE1 = 0;

        public const int RIO_TYPE_RECTANGLE2 = 1;

        //寻找模板输出的相应的信息,包括所含模板的个数, 每个对像所对应的坐标, 角度及匹配度
        public class MATCHINGRESULT_DES
        {
            [XmlIgnoreAttribute]
            [Category("匹配结果"), DefaultValue(3), DisplayName("TupleRow")]
            public HTuple TupleRow { get; set; }

            [XmlIgnoreAttribute]
            [Category("匹配结果"), DefaultValue(3), DisplayName("TupleCol")]
            public HTuple TupleCol { get; set; }

            [XmlIgnoreAttribute]
            [Category("匹配结果"), DefaultValue(3), DisplayName("TupleAngle")]
            public HTuple TupleAngle { get; set; }

            [XmlIgnoreAttribute]
            [Category("匹配结果"), DefaultValue(3), DisplayName("TupleScore")]
            public HTuple TupleScore { get; set; }

            [XmlIgnoreAttribute]
            [Category("匹配结果"), DefaultValue(3), DisplayName("TupleModel")]
            public HTuple TupleModel { get; set; }

            [XmlIgnoreAttribute]
            [Category("匹配结果"), DefaultValue(3), DisplayName("TupleRScaled")]
            public HTuple TupleRScaled { get; set; }

            [XmlIgnoreAttribute]
            [Category("匹配结果"), DefaultValue(3), DisplayName("TupleCScaled")]
            public HTuple TupleCScaled { get; set; }

            public MATCHINGRESULT_DES()
            {
                TupleRow = 0;
                TupleCol = 0;
                TupleAngle = 0;
                TupleScore = 0;
                TupleModel = 0;
                TupleRScaled = 0;
                TupleCScaled = 0;
            }

            public void Reset()
            {
                TupleRow = 0;
                TupleCol = 0;
                TupleAngle = 0;
                TupleScore = 0;
                TupleModel = 0;
                TupleRScaled = 0;
                TupleCScaled = 0;
            }
        }

        //定义模板匹配输入输出参数据结构
        public class SHAPE_MODE_DES
        {
            [Category("基本"), DefaultValue(3), DisplayName("名字")]
            public string szDescription { get; set; }

            // 输入参数
            [TypeConverter(typeof(ExpandableObjectConverter)), Category("建模参数"), DefaultValue(3), DisplayName("图片")]
            public HObject objImage { get; set; }

            [TypeConverter(typeof(ExpandableObjectConverter)), Category("建模参数"), DefaultValue(3), DisplayName("RIO 模版区域1")]
            public RIO_DES strRIO1 { get; set; } //一个图像包含两个RIO的联合

            [TypeConverter(typeof(ExpandableObjectConverter)), Category("建模参数"), DefaultValue(3), DisplayName("RIO 模版区域2")]
            public RIO_DES strRIO2 { get; set; }

            [Category("建模参数"), DefaultValue(3), DisplayName("二值化最小")]
            public double fThresholdMin { get; set; }

            [Category("建模参数"), DefaultValue(3), DisplayName("二值化最大")]
            public double fThresholdMax { get; set; }

            [Category("建模参数"), DefaultValue(3), DisplayName("匹配类型")]
            public ShapeModeMatchType MatchingType { get; set; }

            [Category("建模参数"), DefaultValue(-10), DisplayName("建模开始角度")]
            public double fAngleStart_T { get; set; } // 模板匹配寻找的初始角度, 最大角度及角度间隔

            [Category("建模参数"), DefaultValue(20), DisplayName("建模角度范转")]
            public double fAngleExtent_T { get; set; }

            [Category("建模参数"), DefaultValue(20), DisplayName("建模角度增量")]
            public double fAngleStep_T { get; set; }

            [Category("检测参数"), DefaultValue(0), DisplayName("金字塔级别")]
            public Int32 dwNumLevels { get; set; }

            [Category("检测参数"), DefaultValue(-10), DisplayName("开始角度")]
            public double fAngleStart { get; set; } // 模板匹配寻找的初始角度, 最大角度及角度间隔

            [Category("检测参数"), DefaultValue(20), DisplayName("角度范转")]
            public double fAngleExtent { get; set; }

            [Category("检测参数"), DefaultValue(20), DisplayName("寻找角度增量")]
            public double fAngleStep { get; set; }

            [Category("检测参数"), DefaultValue(0.90), DisplayName("最小匹配度")]
            public double fMinScore { get; set; }

            [Category("检测参数"), DefaultValue(0.90), DisplayName("优化程度")]
            public string szOptimization { get; set; }

            [Category("检测参数"), DefaultValue(0.90), DisplayName("szMetric")]
            public string szMetric { get; set; }

            [Category("检测参数"), DefaultValue(0.90), DisplayName("szContrast")]
            public string szContrast { get; set; }

            [Category("检测参数"), DefaultValue(0.90), DisplayName("szMinContrast")]
            public string szMinContrast { get; set; }

            [Category("检测参数"), DefaultValue(100), DisplayName("最大检验/匹配个数")]
            public Int32 dwNumMatches { get; set; }

            [Category("检测参数"), DefaultValue(0.70), DisplayName("最大被覆盖范围比例")]
            public double fMaxOverlap { get; set; }

            [Category("检测参数"), DefaultValue(0.90), DisplayName("优化程度")]
            public string szSubPixel { get; set; }

            [Category("检测参数"), DefaultValue(0.4), DisplayName("搜索启发式值")]
            public double fGreediness { get; set; }

            [Category("检测参数"), DefaultValue(1), DisplayName("最小比例")]
            public double fScaledMin { get; set; }

            [Category("检测参数"), DefaultValue(1), DisplayName("最大比例")]
            public double fScaledMax { get; set; }

            // 输出参数, 输出模板对像的原始信息,包括对象句柄, 对像坐标, 角度及匹配度
            [TypeConverter(typeof(ExpandableObjectConverter)), Category("检测结果"), DefaultValue(1), DisplayName("最大比例")]
            public CIRCLE_DES MarkCircleDes { get; set; }

            [Category("检测结果"), DefaultValue(1), DisplayName("最大比例")]
            public double fRadiuxMinForFind { get; set; }

            [Category("检测结果"), DisplayName("最大比例")]
            public double fRadiuxMaxForFind { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID")]
            public HTuple TupleModelID { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_1")]
            public HTuple TupleModelID_1 { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_1_Min")]
            public HTuple TupleModelID_1_Min { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_1_Max")]
            public HTuple TupleModelID_1_Max { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_2")]
            public HTuple TupleModelID_2 { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_2_Min")]
            public HTuple TupleModelID_2_Min { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_2_Max")]
            public HTuple TupleModelID_2_Max { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_3")]
            public HTuple TupleModelID_3 { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_3_Min")]
            public HTuple TupleModelID_3_Min { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_3_Max")]
            public HTuple TupleModelID_3_Max { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_4")]
            public HTuple TupleModelID_4 { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_4_Min")]
            public HTuple TupleModelID_4_Min { get; set; }

            [Category("检测结果"), DisplayName("TupleModelID_4_Max")]
            public HTuple TupleModelID_4_Max { get; set; }

            [TypeConverter(typeof(ExpandableObjectConverter)), Category("检测结果"), DisplayName("MarkCircleDes")]
            public MATCHINGRESULT_DES MatchingResultOrg { get; set; }

            public SHAPE_MODE_DES()
            {
                szDescription = "";
                objImage = null;

                CIRCLE_DES MarkCircleDes = new CIRCLE_DES();

                strRIO1 = new RIO_DES();
                strRIO2 = new RIO_DES();
                strRIO1.InitRIO();
                strRIO2.InitRIO();

                MatchingType = ShapeModeMatchType.ShapeModeType_ShapeBase;
                dwNumLevels = 0;
                fAngleStart_T = -10;
                fAngleExtent_T = 10;
                fAngleStep_T = 0.1;

                fThresholdMin = 0;
                fThresholdMax = 128;

                fAngleStart = 0;
                fAngleExtent = 180;
                fAngleStep = 0.05;
                fMinScore = 0.6;
                szOptimization = "auto";
                szMetric = "use_polarity";
                szContrast = "auto";
                szMinContrast = "auto";
                dwNumMatches = 1;
                fMaxOverlap = 0.6;
                szSubPixel = "least_squares";
                fGreediness = 0.9;

                fScaledMin = 0.9;
                fScaledMax = 1.1;

                // 输出参数, 输出模板对像的原始信息,包括对象句柄, 对像坐标, 角度及匹配度
                MatchingResultOrg = new MATCHINGRESULT_DES();
                TupleModelID = 0;
                MatchingResultOrg.TupleRow = 0;
                MatchingResultOrg.TupleCol = 0;
                MatchingResultOrg.TupleAngle = 0;
                MatchingResultOrg.TupleScore = 0;
                MatchingResultOrg.TupleModel = 0;
                MatchingResultOrg.TupleRScaled = 0;
                MatchingResultOrg.TupleCScaled = 0;
            }

            public void Dispose()
            {
                try
                {

                }
                catch(Exception ex)
                {

                }
            }

            public void InitShapemode() { MatchingType = ShapeModeMatchType.ShapeModeType_ShapeBase; }
            public void InitShapemode_Scaled() { MatchingType = ShapeModeMatchType.ShapeModeType_Scaled; fScaledMin = 0.9; fScaledMax = 1.1; }

        }
    }

}
