using System;

namespace Clifton.Windows.Forms.XmlTree
{
	public interface INode
	{
		string Name
		{
			get;
			set;
		}
	}
}
