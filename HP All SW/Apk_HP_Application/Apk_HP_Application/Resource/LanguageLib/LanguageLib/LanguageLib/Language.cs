﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Windows.Forms;

using GenericLib;
using System.Drawing;
using CCWin;
namespace LanguageLib
{
    public class ExportImportResult
    {
        public int RowsAdded;
        public int RowsUpdated;
        public int RowCount;
        public int RowsFailed;

        public ExportImportResult()
        {
            Reset();
        }

        public void Reset()
        {
            RowsAdded = 0;
            RowsUpdated = 0;
            RowCount = 0;
        }
    }

    ///<summary>
    /// Defines interface methods for language system:
    /// - Reading data from Excel
    /// - Reading data from the XML file
    /// - Writing data into the XML file
    /// - Updating Excel with data from the XML file
    /// - Setting and changing the active language
    /// - Changing the names of Excel file, Excel sheet and XML file
    /// - Getting text with the selected language
    /// - Adding new data during execution
    /// </summary>    
    public partial class Language : LanguageDef
    {
        String xmlFile = Application.StartupPath + "\\Config\\Language\\Language.xml";
        String excelFile = Application.StartupPath + "\\Config\\Language\\Language.xls";
        String excelSheet = "Sheet1";

        static Language instance = null;
        static readonly object padlock = new object();

        public Dictionary<String, LanguageSet> langDict = new Dictionary<String, LanguageSet>();
            
        lang selectedLang = lang.English;       // Default language is English
        bool readAllLanguagesFromExcel = true;  // All data is read from Excel as default

        ///<summary>
        /// Constructor of this class.
        ///</summary>
        ///<param name="language">the language to get texts</param>
        private Language(lang language)
        {
            selectedLang = language;
            if (!IsLanguageDefinitionOk() )
                throw new System.IndexOutOfRangeException("Language definition error");
        }

        ///<summary>
        /// Defines the instance to be returned.
        ///</summary>
        public static Language Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Language(lang.English);
                        instance.SetDefaultTexts();
                    }
                    return instance;
                }
            }
        }

        private void setText(Control control, string text)
        {
            string name = ControlFunctions.GetControlName(control, true);
            string cType = control.GetType().ToString();
            if (control.GetType() == typeof(TabControl))
            {
                TabControl tb = (TabControl)control;

                string[] st = text.Split(new char[] { ';' });
                if (st.Length == tb.TabPages.Count)
                {
                    for (int i = 0; i < tb.TabPages.Count; i++)
                    {
                        tb.TabPages[i].Text = st[i];
                    }
                }
            }
            else if (control.GetType() == typeof(DataGridView))
            {
                DataGridView DG = (DataGridView)control;

                string[] st = text.Split(new char[] { ';' });
                if (st.Length >= DG.Columns.Count)
                {
                    for (int i = 0; i < DG.ColumnCount; i++)
                    {
                        DG.Columns[i].HeaderText = st[i];
                    }
                }
            }
            
            else
            {
                control.Text = text;
            }
        }

        public void SetControlText(Control control)
        {
            try
            {
                string name = ControlFunctions.GetControlName(control, true);
                if (control.GetType() == typeof(CCWin.SkinControl.SkinToolStrip))
                {
                    CCWin.SkinControl.SkinToolStrip skTool = (CCWin.SkinControl.SkinToolStrip)control;

                    for (int i = 0; i < skTool.Items.Count; i++)
                    {
                        string mtext = GetText(control.Name+"."+skTool.Items[i].Name);
                        if (mtext != "")
                        {
                            skTool.Items[i].Text = mtext;
                        }
                        //ToolStripMenuItem
                       // ToolStripMenuItem
                        if (skTool.Items[i].GetType() == typeof(ToolStripDropDownButton))
                        {
                            ToolStripDropDownButton dB = (ToolStripDropDownButton)skTool.Items[i];
                            for (int k = 0; k < dB.DropDownItems.Count; k++)
                            {
                                mtext = GetText(control.Name + "." + dB.DropDownItems[k].Name);
                                if (mtext != "")
                                {
                                    dB.DropDownItems[k].Text = mtext;
                                }
                            }
                        }

                    }
                    return;
                }
                // Set english text from original control text
                try
                {
                    LanguageSet set;
                    if (langDict.TryGetValue(name, out set))
                    {
                        if (set.English == "")
                        {
                            if (control.GetType() == typeof(TabControl))
                            {
                                TabControl tb = (TabControl)control;
                                string eng = getTabControlText(tb);
                                set.English = eng;
                            }
                            else
                            {
                                set.English = control.Text;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }

                string text = GetText(name);
                if (text != "")
                {
                    setText(control, text);
                }
            }
            catch (Exception)
            {
            }
        }
        ///<summary>
        /// Sets selected language texts to active controls
        ///</summary> 
        ///<param name="control">the pointer to the control</param>    
        public void SetText(Control control)
        {
            //SetControlText(control);
            if (control.Controls.Count > 0)
            {
                foreach (Control item in control.Controls)
                {
                    SetControlText(item);
                    SetText(item);
                }
            }
        }

        private string getTabControlText(TabControl tb)
        {
            string text = "";
            for (int i = 0; i < tb.TabPages.Count; i++)
            {
                text += tb.TabPages[i].Text + ";";
            }
            if (text.EndsWith(";"))
            {
                text = text.Remove(text.Length - 1, 1);
            }
            return text;
        }

        public void GiveText(Control control, Point location)
        {
            Control c = ControlFunctions.GetControl(control, location);
            if (c == null)
            {
                return;
            }
            Type t = c.GetType();
            if ((t == typeof(Button) || t.IsSubclassOf(typeof(Button))) ||
                (t == typeof(Label)) ||
                (t == typeof(UserControl)) ||
                (t == typeof(Panel)) ||
                (t == typeof(GroupBox)) ||
                (t == typeof(CheckBox)) ||
                (t == typeof(RadioButton)) ||
                (t == typeof(GroupBox) || t.IsSubclassOf(typeof(GroupBox))) ||
                (t.Name == "ucRepeat") ||
                (t == typeof(TabControl)))
                //(t == typeof(UCLibrary.Switch))
            {
                string st = ControlFunctions.GetControlName(control, location, true);
                if (st == "")
                {
                    return;
                }

                GiveControlText gh = new GiveControlText();
                if (LanguageConfig.Instance.ActiveLanguage == lang.English)
                {
                    gh.EnableControlTextField = false;
                }
                string text = "";
                if (c.GetType() == typeof(TabControl))
                {
                    TabControl tb = (TabControl)c;
                    text = getTabControlText(tb);
                }
                else
                {
                    text = c.Text;
                }
                gh.SetControlname(text);
                string translation = Language.Instance.GetText(st);
                bool translationFound = false;
                if (translation == "")
                {
                    gh.ControlText = text;
                }
                else
                {
                    translationFound = true;
                    gh.ControlText = translation;
                    gh.Message = Language.Instance.GetHelpText(st);
                }
                Point p = location;
                p.X += 30;
                p.Y += 30;
                if (p.Y > 700)
                {
                    p.Y -= 400;
                }
                if (p.X > 880)
                {
                    p.X -= 400;
                }
                gh.Location = p;
                gh.ShowDialog();
                if (gh.Accepted)
                {
                    // Set english text from control
                    if (!translationFound)
                    {
                        LanguageDef.lang old = Language.Instance.GetLanguage();
                        Language.Instance.SetLanguage(LanguageDef.lang.English);
                        Language.Instance.SetText(st, text);
                        Language.Instance.SetLanguage(old);
                    }
                    Language.Instance.SetText(st, gh.ControlText);
                    Language.Instance.SetHelpText(st, gh.Message);
                    Language.Instance.WriteLanguageToXml();
                    //c.Text = gh.ControlText;
                    setText(c, gh.ControlText);
                }
            }
        }

        public void ShowHelp(Control control, Point location, ucHelpMessage helpMessage)
        {
            Control c = ControlFunctions.GetControl(control, location);
            if (c == null)
            {
                return;
            }
            Type t = c.GetType();
            string st = ControlFunctions.GetControlName(control, location);
            if (st == "")
            {
                return;
            }

            //string message = HelpMessage.Instance.GetHelpMessage(st);
            string message = Language.Instance.GetHelpText(st);
            if (message == "")
            {
                message = "No Help Available";
            }
            helpMessage.Message = message;
            Point p = location;
            p.X += 30;
            p.Y += 30;
            if (p.Y > 880)
            {
                p.Y -= 200;
            }
            if (p.X > 940)
            {
                p.X -= 400;
            }
            control.Controls.Add(helpMessage);
            helpMessage.Location = p;
            helpMessage.BringToFront();
        }

        ///<summary>
        /// Checks the languages definitions.
        ///</summary> 
        /// <return>bool: true - OK, false - language definition errors</return>    
        public bool IsLanguageDefinitionOk()
        {
            // Test a language set.
            LanguageSet ls = new LanguageSet();
            if (ls.GetDictionarySize() != -1)
                return true;
            else
                return false;
        }

        ///<summary>
        /// Sets the active language.
        /// </summary>
        /// <param name="language">the language to get texts</param>   
        public bool SetLanguage(lang language)
        {
            selectedLang = language;
            return true;
        }

        ///<summary>
        /// Returns the active language.
        ///</summary>
        ///<return>lang:the active language</return>  
        public lang GetLanguage()
        {
            return selectedLang;
        }

        ///<summary>
        /// Sets the name of the files and the sheet if a new name has been defined.
        ///</summary> 
        /// <param name="excel">the name of the excel file</param>      
        /// <param name="sheet">the name of the excel sheet</param>      
        /// <param name="xml">the name of the XML file</param>        
        public bool SetNames(String excel, String sheet, String xml)
        {
            bool changed = false;
            
            // If the parameter for the excel file has been set,
            // update the name of the excel file.
            if (excel != "")
            {
                excelFile = excel;
                changed = true;
            }
            // If the parameter for the excel sheet has been set,
            // update the name of the excel sheet.
            if (sheet != "")
            {
                excelSheet = sheet;
                changed = true;
            }
            // If the parameter for the XML file has been set,
            // update the name of the XML file.
            if (xml != "")
            {
                xmlFile = xml;
                changed = true;
            }
            return changed;
        }

        ///<summary>
        /// Sets the state to read data from the excel table.
        ///</summary> 
        /// <param name="readAll">the reading state (true = all, false = active language)</param>
        /// 
        /// <return>bool:current reading state</return>  
        public bool SetReadingState(bool readAll)
        {
            readAllLanguagesFromExcel = readAll;
            return readAllLanguagesFromExcel;
        }

        ///<summary>
        /// Returns the status of the dictionary.
        ///</summary> 
        ///<return>bool:true - data, false - no data</return>
        public bool IsDictionaryReady()
        {
            if (langDict.Count > 0 )
                return true;
            else
                return false;
        }

        ///<summary>
        /// Clears the dictionary.
        ///</summary> 
        /// <return>bool:the clearing status</return>   
        public bool ClearDictionary()
        {
            langDict.Clear();
            return true;
        }

        public int GetDictionaryLength()
        {
            return langDict.Count;
        }

        public bool SynchronizeExcelWithXml()
        {
            ExportImportResult res = new ExportImportResult();
            return SynchronizeExcelWithXml(res);
        }

        ///<summary>
        /// Synchronizes the excel table with the XML file. If the cell data has been updated
        /// in the XML file, the cell is also updated in the excel table. In case a new tag
        /// set has been added in the middle or in the end of the XML, a new row is added into
        /// the end of the excel table.
        ///</summary>
        ///<return>bool:status of synchronization</return>   
        public bool SynchronizeExcelWithXml(ExportImportResult res)
        {
            // Create the object to communicate with the excel table.
            ExcelCommunications excel = new ExcelCommunications();

            res.Reset();

            // Create the object to create SQL queries.
            LanguageSql sql = new LanguageSql();

            // Initialize rows counters for synchronization.

            // Open the connection to the excel table.
            if (!excel.OpenConnection(excelFile, excelSheet))
                return false;

            Dictionary<String, LanguageSet> localDict = new Dictionary<string,LanguageSet>();

            // Read language data from excel table.
            if (!ReadLanguageFromExcel(localDict))
                return false;

            // Save data in the the global dictionary to the local language list.
            List<LanguageSet> localXml = CreateLangList();

            // Compare the local language list (XML data) with the local dictionary (Excel data).
            for (int i = 0; i < localXml.Count; i++)
            {
                try
                {
                    // Get the language set object from the language list (XML data).
                    LanguageSet xmlSet = localXml[i];

                    // Set the updated flag down.
                    bool updated = false;

                    // If language set object is found in the excel table accordind to the key,
                    // check if update needed: try to find the key in the dictionary.
                    LanguageSet lset;
                    if (localDict.TryGetValue(xmlSet.Key, out lset))
                    {
                        // Update language cells in the excel table if required.
                        updated = UpdateTexts(xmlSet, lset, excel, sql);

                        // If the language cell has been updated, increase the update counter by one.
                        // Set also the update flag down.
                        if (updated)
                        {
                            res.RowsUpdated++;
                            updated = false;
                        }
                    }
                    // If key word is not found in the local dictionary, a new row has to be added
                    // into the excel table.
                    else
                    {
                        // Create the SQL query to insert a new row.            
                        String insert = sql.InsertString(excelSheet, xmlSet);

                        // If the row has been added, increase the related counter by one.
                        if (excel.InsertRowToExcelTable(insert) == 1)
                        {
                            res.RowsAdded++;
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendLine("Can not export language to Excel");
                            sb.AppendLine("Key = " + xmlSet.Key);
                            sb.Append("English = " + xmlSet.English);
                            LoggerDataEx.Instance.AddMessage(LoggerDataEx.MessageType.Warning, "Can not export language to Excel", sb.ToString(), "");
                            res.RowsFailed++;
                        }
                    }
                }
                // Exception has happened either when reading language set object from the language
                // list or when trying to find the language set object in the local dictionary.
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }

            // Read language data from excel table to get row count.
            localDict = new Dictionary<string, LanguageSet>();
            if (!ReadLanguageFromExcel(localDict))
                return false;

            res.RowCount = localDict.Count;

            // Close the excel connection.
            excel.CloseConnection();
            return true;
        }

        public bool ReadLanguageFromExcel(ExportImportResult res)
        {
            return ReadLanguageFromExcel(langDict, res);
        }

        public bool ReadLanguageFromExcel()
        {
            return ReadLanguageFromExcel(langDict);
        }

        private bool ReadLanguageFromExcel(Dictionary<String, LanguageSet> dict)
        {
            ExportImportResult res = new ExportImportResult();
            return ReadLanguageFromExcel(dict, res);
        }
 
        ///<summary>
        /// Reads languages from the excel table.
        ///</summary> 
        ///<return>bool:status of reading (true/false)</return>   
        private bool ReadLanguageFromExcel(Dictionary<String, LanguageSet> dict, ExportImportResult res)
        {
            res.Reset();

            // Create the object to communicate with the excel table.
            ExcelCommunications excel = new ExcelCommunications();

            // Create the object to create SQL queries.
            LanguageSql sql = new LanguageSql();

            // Open the connection to the excel table. If opening fails, return with false.
            if (!excel.OpenConnection(excelFile, excelSheet))
                return false;
     
            // Create the SQL query to read all language data or only given language 
            // data from the excel table.
            String sqlQuery;
            if ( readAllLanguagesFromExcel )
                sqlQuery = sql.ReadAllString(excelSheet);
            else 
                sqlQuery = sql.ReadLanguageString(excelSheet, selectedLang);

            // Read data from the excel table. If the query fails, return with false.
            if (excel.ReadExcelTable(sqlQuery) != ExcelCommunications.errorCode.none)
                return false;

            // Read the number of columns and rows read from the excel table.
            int cols = excel.GetColumns();
            int rows = excel.GetRows();

            res.RowCount = rows;

            // Read columns names.
            String[] columns = excel.GetColumnNames();
          
            // A loop to read rows from the "ExcelCommunications" object and to write
            // row data to the "LanguageSet" object. The "LanguageSet" objects are saved to the
            // to the dictionary to make it possible to search for texts.
            for (int row = 0; row < rows; row++)
            {
                // This object will include a row data read from the excel table.
                Object[] cell;

                // Read a row from the result of excel table reading.
                excel.GetObjectRow(row, out cell);

                // Create a new language set object.
                LanguageSet ls = new LanguageSet();
                
                // Set data to the created language set object.
                if (readAllLanguagesFromExcel && cols == cell.Length )
                {
                    // Write the key to the language object.
                    ls.Key = (String)cell[0];

                    // A loop to write texts to the language object.
                    for (int i = 1; i < cols; i++)
                    {
                        ls.SetText(columns[i], (String)cell[i]);
                    }
                }

                // If the selected language has been read from the excel table,
                // add only one text to the LangSet object. The number of cols
                // is five, because following columns have been read: 'Tag',
                // 'English'. 'EnglishHelp' and text and help text for selected
                // language.
                else if (cols == 5)
                {   
                    // Write the key, english text and help text to the language set object.
                    ls.Key = (String)cell[0];
                    ls.SetText(lang.English.ToString(), (String)cell[1]);
                    ls.SetText(lang.English.ToString()+LanguageSet.HELP, (String)cell[2]);

                    // Write the text and helpt text in selected language into the language set
                    // object.
                    ls.SetText(selectedLang.ToString(), (String)cell[3]);
                    ls.SetText(selectedLang.ToString() + LanguageSet.HELP, (String)cell[4]);
                }

                // Error has happened, return immediately with false.
                else
                {
                    return false;
                }

                if (dict.ContainsKey(ls.Key))
                {
                    ls.English = dict[ls.Key].English;
                    if (!LanguageSet.IsSame(ls, dict[ls.Key]))
                    {
                        res.RowsUpdated++;
                    }
                    dict[ls.Key] = ls;
                }
                else
                {
                    dict.Add(ls.Key, ls);
                    res.RowsAdded++;
                }
            }

            // Close the excel connection.
            excel.CloseConnection();
            return true;
        }

        ///<summary>
        /// Reads language data from the XML file.
        ///</summary>
        ///<return>bool:reading status</return>   
        public bool ReadLanguageFromXml()
        {
            // Create a local list to save language data.
            List<LanguageSet> langList = new List<LanguageSet>();

            // Define the reader object.
            TextReader reader = null;
      
            // Return, if the global dictionary does not exist.
            if (langDict == null)
                return false;
            
            // Create a new serializer.
            XmlSerializer serializer = new XmlSerializer(typeof(List<LanguageSet>));
 
            try
            {
                // Create the reader object and deserialize language data from the
                // Excel file. Finally, close the reader.
                reader = new StreamReader(xmlFile, Encoding.Default);
                langList = (List<LanguageSet>)serializer.Deserialize(reader);
                reader.Close();
            }
            // Reading error has happened, return false.
            catch (Exception ex)
            {
                // If the reader is still open, it has to be closed.
                ex.ToString();
                if (reader != null)
                    reader.Close();
                return false;
            }

            // Add read language items from the local list to the global dictionary.
            for (int i = 0; i < langList.Count; i++)
            {
                // Get the language set element from the list.
                LanguageSet set = langList.ElementAt(i);

                // Convert format "_" -> "[NL]"
                if (LanguageConfig.Instance.Version == 0)
                {
                    set.ConvertToVersion1();
                }

                if (langDict.ContainsKey(set.Key))
                {
                    langDict[set.Key] = set;
                }
                else
                {
                    langDict.Add(set.Key, set);
                }
            }
            if (LanguageConfig.Instance.Version == 0)
            {
                LanguageConfig.Instance.Version = 1;
                LanguageConfig.Instance.Write();
            }
            return true;
        }

        ///<summary>
        /// Creates the XML language file from the dictionary.
        ///</summary> 
        ///<return>bool:writing status</return>   
        public bool WriteLanguageToXml()
        {
            // Define the writer object.
            TextWriter writer = null;

            // Create the local language list from the global dictionary.
            List<LanguageSet> langList = CreateLangList();

            // Create a new serializer.
            XmlSerializer serializer = new XmlSerializer(typeof(List<LanguageSet>));

            try
            {
                // Create the writer object.
                writer = new StreamWriter(xmlFile);

                // If the local language list and writer exists, make serialization
                if (langList != null && writer != null)
                {
                    serializer.Serialize(writer, langList);
                }
                // Error: writer and language list not defined.
                else
                {
                    // If the writer is still open, it has to closed.
                    if ( writer != null )
                        writer.Close();
                    return false;
                }
                // Close the writer.
                writer.Close();
            }
            // Error happened during the writing process.
            catch (Exception ex)
            {
                // If the writer is still open, it has to closed.
                ex.ToString();
                if (writer != null)
                    writer.Close();
                return false;
            }
            return true;
        }

        ///<summary>
        /// Sets the help text to the active language according to the tag.
        ///</summary> 
        ///<param name="tag">the key for the dictionary</param>     
        ///<param name="helpText">the new help text to be added</param>    
        /// 
        ///<return>bool:status of adding the new text</return>   
        public bool SetText(String tag, String helpText)
        {
            helpText = helpText.Replace("\r\n", "[NL]");
            return SetAndModifyText(tag, helpText, selectedLang.ToString());
        }

        ///<summary>
        /// Returns the text read from the dictionary according to the key word.
        ///</summary> 
        ///<param name="key">the key used to search the text</param>   
        /// 
        ///<return>String:the found text</return>  
        public String GetText(String key)
        {
            String readText = "";
            if (GetText(key, out readText,selectedLang.ToString()))
            {
                return readText.Replace("[NL]", "\r\n");
            }
            return "";
        }
        public String GetTextTip(String key)
        {
            String readText = "";
            if (GetText(key, out readText, selectedLang.ToString()))
            {
                return readText.Replace("[NL]", "\r\n");
            }
            return key;
        }
        public String GetText(Texts id)
        {
            return GetText(id.ToString());
        }

        public String GetTextEnglish(String key)
        {
            String readText = "";
            if (GetText(key, out readText, lang.English.ToString()))
            {
                return readText.Replace("[NL]", "\r\n");
            }
            return "";
        }

        public String GetText(object o)
        {
            String readText = "";
            if (GetText(o.ToString(), out readText, selectedLang.ToString()))
            {
                return readText.Replace("[NL]", "\r\n");
            }
            return "";
        }

        ///<summary>
        /// Sets the help text to the active language according to the tag.
        ///</summary> 
        ///<param name="tag">the key for the dictionary</param>     
        ///<param name="helpText">the new help text to be added</param>    
        /// 
        ///<return>bool:status of adding the new text</return>   
        public bool SetHelpText(String tag, String helpText)
        {
            helpText = helpText.Replace("\r\n", "[NL]");
            return SetAndModifyText(tag, helpText, selectedLang.ToString() + LanguageDef.HELP);
        }

        ///<summary>
        /// Returns the help text related to the given tag and the actived language.
        ///</summary> 
        ///<param name="key">the key used to search the help text</param>   
        /// 
        ///<return>String:the found help text</return>  
        public String GetHelpText(String key)
        {
            String readText = "";
            if (GetText(key, out readText, selectedLang.ToString()+LanguageDef.HELP))
            {
                return readText.Replace("[NL]", "\r\n");
            }
            return "";
        }

        ///<summary>
        /// Sets a text to the active language.
        ///</summary> 
        ///<param name="tag">the key for the dictionary</param>     
        ///<param name="text">the new text to be added</param> 
        ///<param name="language">the active language</param>
        ///
        ///<return>bool:status of adding the new text</return>   
        private bool SetAndModifyText(String tag, String text, String language)
        {
            // Try to find the given tag from the global dictionary.
            LanguageSet lset;
            if (langDict.TryGetValue(tag, out lset))
            {
                // The tag is found. Update the found tag with the text for the
                // selected language.
                lset.UpdateText(language, text);
            }
            else
            {
                // The tag is not found. The tag and text for active language is added into
                // the dictionary.
                LanguageSet lsn = new LanguageSet();
                lsn.Key = tag;
                lsn.SetText(language, text);
                langDict.Add(tag, lsn);
            }
            return true;
        }

        ///<summary>
        /// Gets the text in selected language from the dictionary. 
        ///</summary>  
        ///<param name="key">the key to search text from the dictionary</param>       
        ///<param name="value">the found text</param>
        ///<param name="activeLanguage">the active language</param>
        /// 
        ///<return>bool:status of searhing the text</return>  
        private bool GetText(String key, out String value, String activeLanguage)
        {
            LanguageSet langSet;
            
            // If the dictionary does not exist or it is empty, return with false.
            if ( langDict == null || langDict.Count <= 0 )
            {
                value = "";
                return false;
            }

            // Try to find the language set in the dictionary according to the key.
            if (langDict.TryGetValue(key, out langSet))
            {
                // Read the text from the language set using the active language.
                value = langSet.GetText(activeLanguage);

                // If the returned value is not defined, check if english text is found.
                if ( value == null || value.Equals(""))
                {
                    // If the help text is tried to be found, return empty string.
                    if (langSet.IsHelpText(activeLanguage))
                    {
                        value = "";
                        return false;
                    }

                    // Get the english text from the language set.
                    value = langSet.GetEnglish();

                    // If the english text is not found, try to find some language.
                    if (value == null || value.Equals(""))
                    {
                        //// Find the first active language.
                        //foreach (String language in Enum.GetNames(typeof(lang)))
                        //{
                        //    value = langSet.GetText(language);
                        //    if (value != null)
                        //    {
                        //        if (!value.Equals(""))
                        //            return true;
                        //    }
                        //}
                        value = "";
                        return false;
                    }
                }
                return true;
            }

            // The text was not found, return false.
            value = "";
            return false;
        }

        ///<summary>
        /// Creates a language list from the dictionary.
        ///</summary> 
        ///<return>List'LanguageSet':the language list</return>   
        private List<LanguageSet> CreateLangList()
        {
            List<LanguageSet> list = new List<LanguageSet>();

            // A loop to pick up language sets from the global dictionary and to write
            // them to the language list.
            for (int i = 0; i < langDict.Count; i++)
            {
                KeyValuePair<String,LanguageSet> pair = langDict.ElementAt(i);
                list.Add(pair.Value);
            }

            return list;
        }

        ///<summary>
        /// Updates the language cells in the excel table, if the language data in the excel table
        /// and the XML file is not equal.
        ///</summary> 
        ///<param name="xml">the lang set object for XML file</param>        
        /// <param name="excel">the lang set object for the excel table</param>         
        /// <param name="excelComm">the excel communcations object</param>     
        /// <param name="sql">the SQL object to create SQL queries</param>           
        /// 
        ///<return>bool:updating status</return>  
        private bool UpdateTexts(LanguageSet xml, LanguageSet excel, ExcelCommunications excelComm, LanguageSql sql)
        {
            bool updated = false;

            // Create string table for text and help texts keys in the dictionaries.
            String[] langS = new String[xml.GetDictionarySize()+excel.GetDictionarySize()];
            int counter = 0;

            // Create a string table from dictionaries for text and help texts.
            foreach ( lang langT in Enum.GetValues(typeof(lang)))
            {
                langS[counter++] = langT.ToString();
                langS[counter++] = langT.ToString()+HELP;
            }

            // A loop to check all text cells in the language set for excel and XML.
            // The language set includes a dictionary, where key is a language and
            // value is the text.
            for (int i = 0; i < langS.Length; i++)
            {
                // Try to find text from the dictionary of the language set "xml".
                String textXml = xml.GetText(langS[i]);

                // Try to find text from the dictionary of the language set "excel".
                String textExcel = excel.GetText(langS[i]);

                // If the text has been found in the XML file, check if it is equal to text
                // in the excel table.
                if (textXml != null)
                {
                    // If the textXml and textExcel are not equal, the excel table has to be updated.
                    if (!textXml.Equals(textExcel))
                    {
                        // Create the SQL query to updated the current language cell in the excel table.
                        String update = sql.UpdateString(excelSheet, langS[i], textXml, xml.GetText(LanguageSet.KEY));

                        // If updating done successfully, set the update flag up.
                        if (excelComm.UpdateExcelTable(update) == 1)
                            updated = true;
                    }
                }
            }
            return updated;
        }

        public bool FindEnglishMatchingString(string origText,out string textToConvert)
        {
            textToConvert = "";
            if (LanguageConfig.Instance.ActiveLanguage != LanguageLib.LanguageDef.lang.English)
            {
                foreach (KeyValuePair<string, LanguageSet> kvp in LanguageLib.Language.Instance.langDict)
                {
                    if (kvp.Value.GetText(LanguageConfig.Instance.ActiveLanguage.ToString()) == origText)
                    {
                        textToConvert = kvp.Value.GetText(LanguageLib.LanguageDef.lang.English.ToString());
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        public void ResetEnglishText()
        {
            for (int i = 0; i < langDict.Count; i++)
            {
                KeyValuePair<String,LanguageSet> pair = langDict.ElementAt(i);
                pair.Value.English = "";
            }
        }
    }
}
