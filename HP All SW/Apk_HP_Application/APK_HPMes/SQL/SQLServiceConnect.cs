﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Drawing.Design;
using HpWebService;

namespace APK_HP.MES.HPSql
{
    public enum emDataType
    {
        emDataType_short = 0,
        emDataType_Int,
        emDataType_Int16,
        emDataType_Int32,
        emDataType_Int64,
        emDataType_Byte,
        emDataType_float,
        emDataType_double,
        emDataType_string,
        emDataType_point,
    }

    public enum emCompareType
    {
        Compare_NA = 0,
        Compare_Equal,
        Compare_GreaterMin,
        Compare_LessMax,
        Compare_MinMax,
        Compare_StringContain,
    }

    public class ResultDes
    {
        public string CriteriaName { get; set; }
        public string CriteriaStantard { get; set; }
        public string CriteriaMin { get; set; }
        public string CriteriaMax { get; set; }
        public emDataType DataType { get; set; }
        public emCompareType CompareType { get; set; }

        private string _Result = "";
        public string Result { get { return _Result; } }

        public bool CompareStatus { get; set; }

        public ResultDes()
        {
            CriteriaName = "";
            CriteriaStantard = "";
            CriteriaMin = "";
            CriteriaMax = "";
            DataType = emDataType.emDataType_string;
            CompareType = emCompareType.Compare_NA;
            CompareStatus = false;
        }

        override public string ToString()
        {
            string temp = "{" + $"{CriteriaName}|{CriteriaStantard}|{CriteriaMin}|{CriteriaMax}|{DataType.ToString()}|{CompareType.ToString()}|{Result}" + "}";
            return temp;
        }

        public void SetResult(string result, bool refrenshCompareStatus = true)
        {
            _Result = result;

            if(refrenshCompareStatus)
            {
                RefereceCompareStatus();
            }
        }

        public void RefereceCompareStatus()
        {
            CompareStatus = true;
        }

        static public emDataType GetDataTypeFromString(string str)
        {
            try
            {
                if (str == "emDataType_short")
                    return emDataType.emDataType_short;

                else if (str == "emDataType_Int")
                    return emDataType.emDataType_Int;

                else if (str == "emDataType_Int16")
                    return emDataType.emDataType_Int16;

                else if (str == "emDataType_Int32")
                    return emDataType.emDataType_Int32;

                else if (str == "emDataType_Int64")
                    return emDataType.emDataType_Int64;

                else if (str == "emDataType_Byte")
                    return emDataType.emDataType_Byte;

                else if (str == "emDataType_float")
                    return emDataType.emDataType_float;

                else if (str == "emDataType_double")
                    return emDataType.emDataType_double;

                else if (str == "emDataType_string")
                    return emDataType.emDataType_string;

                else if (str == "emDataType_point")
                    return emDataType.emDataType_point;
                else
                    throw new Exception("传入的字符串无法找对应的数据类型");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static public emCompareType GetCompareTypeFromString(string str)
        {
            try
            {
                if (str == "Compare_NA")
                    return emCompareType.Compare_NA;

                else if (str == "Compare_Equal")
                    return emCompareType.Compare_Equal;

                else if (str == "Compare_GreaterMin")
                    return emCompareType.Compare_GreaterMin;

                else if (str == "Compare_LessMax")
                    return emCompareType.Compare_LessMax;

                else if (str == "Compare_MinMax")
                    return emCompareType.Compare_MinMax;

                else if (str == "Compare_StringContain")
                    return emCompareType.Compare_StringContain;

                else
                    throw new Exception("传入的字符串无法找对应的数据类型");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }



    public class ResultInformation
    {
        [Category("主要信息"), DefaultValue(3), DisplayName("工位名称")]
        public string StationName { get; set; }

        [Category("主要信息"), DefaultValue(3), DisplayName("结果状态")]
        public bool ResultStatus { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter)), Category("主要信息"), DefaultValue(3), DisplayName("结果状态")]
        public ResultDes[] Results { get; set; }

        public ResultInformation(string stationName)
        {
            StationName = stationName;
            ResultStatus = false;

            Results = new ResultDes[1];
            Results[0] = new ResultDes();
        }
    }

    public enum HPSqlError
    {
        Error_NoError = 0, //传入参数错误
        Error_Exception, //发生异常
        Error_Parameter, //传入参数错误
        Error_TrayUnAvalible, //操作当前的托盘是无效的或当前的托盘正处于工作中，没有解绑
        Error_UnBindError, //解绑失败，可以没有找到对应的托盘
        Error_PCBNotExist, //当前的PCBID不存在
        Error_TrayNotExist, //当前的Tray不存在
        Error_DataError, //数据错误
    }

    /// <summary>
    /// 工站名称 （上料工站，烧录工站，打码工站，切割工站，顶部测试工站，底部测试工站，包装工站）
    /// </summary>
    public enum WorkStationNameEnum
    {
        None = -1,
        上料工站 = 0,
        烧录工站 = 1,
        打码工站 = 2,
        切割工站 = 3,
        顶部测试工站 = 4,
        底部测试工站 = 5,
        包装工站 = 6
    }

    public class HPSQLService
    {
        private SqlConnection _conection = new SqlConnection();
        public static HPSQLService _instance = null;

        private static string _ErrorMessageLast = "";
        public static string ErrorMessageLast { get { return _ErrorMessageLast; } }

        private static short _ErrorCodeLast = 0;
        public static short ErrorCodeLast { get { return _ErrorCodeLast; } }

        private object SqlLock = new object();
        private  SqlCommand m_dbCommand = new SqlCommand();
        public string IpAddress = "192.168.1.100";
        public int Port = 1433;
        public string User = "sa";
        public string Password = "123456";
        public string Database = "";

        public HPSQLService()
        {
            if (_instance == null)
            {
                _instance = this;
            }
        }

        private short SetLastError(HPSqlError code, string message)
        {
            lock(_ErrorMessageLast)
            {
                _ErrorCodeLast = (short)code;
                _ErrorMessageLast = $"{message}";
            }

            return (short)code;
        }

        public  string connSql = " Data Source=192.168.1.100;Database=HPDatabase;User ID =sa; Password=123456;Connect Timeout=5;MultipleActiveResultSets=true";

        /// <summary>
        ///   连接数据库
        /// </summary>
        /// <param name="Service">连接地址</param>
        /// <param name="Database">数据库名称</param>
        /// <param name="User">用户名</param>
        /// <param name="Pwd">用户密码</param>
        /// <returns></returns>

        public bool Connect(string ip, int port, string database, string user, string pwd)
        {
            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                IpAddress = ip;
                Database = database;
                Port = port;
                User = user;
                Password = pwd;

                return Connect();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public bool Connect()
        {
            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                _conection.ConnectionString = $"Data Source={IpAddress};Database={Database};User ID ={User}; Password={Password};Connect Timeout=5;MultipleActiveResultSets=true";
                _conection.Close();
                int timeOut = _conection.ConnectionTimeout;
                //获取或设置用于打开 SQL Server 数据库的字符串
                //conn.ConnectionString = " Data Source=192.168.1.100;Database=HPDatabase;User ID =sa; Password=123456;";
                try
                {
                    //打开数据库
                    _conection.Open();
                    if (_conection.State.ToString() != "Open")
                    {
                        return false;
                    }
                }
                catch (SqlException ex)
                {
                    SetLastError(HPSqlError.Error_Exception, ex.Message);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
        }

        public void DisConnect()
        {
            try
            {
                _conection.Close();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public bool SQL_IsTrayAvalible(string TrayQrCode)
        {
            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public string SQL_GetResultFiledFromID(WorkStationNameEnum emStation)
        {
            if (emStation == WorkStationNameEnum.上料工站)
                return "LoadStation_ResultString";

            else if (emStation == WorkStationNameEnum.烧录工站)
                return "FlashStation_ResultString";

            else if (emStation == WorkStationNameEnum.打码工站)
                return "LaserMarkStation_ResultString";

            else if (emStation == WorkStationNameEnum.切割工站)
                return "LaserCutStation_ResultString";

            else if (emStation == WorkStationNameEnum.顶部测试工站)
                return "VistionTest1Station_ResultString";

            else if (emStation == WorkStationNameEnum.底部测试工站)
                return "VistionTest2Station_ResultString";

            else if (emStation == WorkStationNameEnum.包装工站)
                return "PackingStation_Station_ResultString";

            else
                return "";

        }

        /// <summary>
        /// 通过查询PCBID是否存在，绑定到托盘并更新到PCB板表
        /// </summary>
        /// <param name="QrCode">载盘二维码</param>
        /// <param name="PCBID"></param>
        /// <returns></returns>
        public bool SQL_TrayBind(string TrayQrCode, string WorkOrderID, string PCBQrCode, string PCBID)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if(TrayQrCode == null || PCBID == null || WorkOrderID == null || PCBQrCode ==null ||
                    TrayQrCode.Length <= 0 || PCBID.Length <= 0 || WorkOrderID.Length <= 0 || PCBQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "传入参数错误");
                    return false;
                }

                if (!SQL_IsTrayAvalible(TrayQrCode))
                {
                    SetLastError(HPSqlError.Error_Parameter, "操作当前的托盘是无效的或当前的托盘正处于工作中，没有解绑");
                    return false;
                }

                #region>>>>>更新托盘绑定信息TrayTable
                string Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (res == null)
                    return false;

                if (!res.HasRows)
                {
                    res.Close();
                    //如果没有添加新的 QrCode 状态是没有绑定的
                    Sql = string.Format("Insert into TrayTable(TrayQrCode,WorkOrderID,PCBQrCode,PCBID,BindTime,UnBindTime,CurrentState) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", TrayQrCode, WorkOrderID, PCBQrCode, PCBID, strDataTime, "", "已绑定");//BindFlag绑定必须是1，UnBind必须是0
                    var EXres = RunSql(Sql);
                    if (EXres <= 0)
                        return false;
                }
                else
                {
                    res.Close();
                    //如果有查询是否绑定，无绑定则绑定，有则返回
                    //查询虚拟ID是否存在，不存在就更新，存在则报错
                    Sql = string.Format("UPDATE TrayTable Set WorkOrderID='{1}',PCBQrCode='{2}',PCBID='{3}' ,BindTime='{4}',UnBindTime='{5}',CurrentState='{6}' where TrayQrCode='{0}'", TrayQrCode, WorkOrderID, PCBQrCode, PCBID, strDataTime, "", "已绑定");
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                    //虚拟PCBID不存在，根据托盘二维码更新数据，并绑定,
                }
                #endregion

                #region>>>>>更新结果信息
                Sql = string.Format("SELECT * FROM ResultsInfoTable WHERE PCBID='{0}'", PCBID);
                res = ExecuteReader(Sql);
                if (res == null)
                    return false;

                if (!res.HasRows)
                {
                    res.Close();
                    Sql = string.Format("Insert into ResultsInfoTable(PCBID,WorkOrderID,TrayQrCode,PCBQrCode,BindTime,UnBindTime,ResultStatus)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", PCBID, WorkOrderID, TrayQrCode, PCBQrCode, strDataTime, "", "0");//BindFlag绑定必须是1，UnBind必须是0
                    var EXres = RunSql(Sql);
                    if (EXres <= 0)
                        return false;
                }
                else
                {
                    res.Close();
                    Sql = string.Format("UPDATE  ResultsInfoTable Set WorkOrderID='{1}',TrayQrCode='{2}',PCBQrCode='{3}',BindTime='{4}',UnBindTime='{5}',ResultStatus='{6}' where PCBID='{0}'", PCBID, WorkOrderID, TrayQrCode, PCBQrCode, strDataTime, "", "0");
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                if(res != null)
                    res.Close(); 

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        /// <summary>
        /// 通过查询PCBID是否存在，解绑托盘，删除PCB虚拟ID
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <returns></returns>
        /// 
        public bool SQL_TrayUnBind(string TrayQrCode)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                if (!SQL_IsTrayAvalible(TrayQrCode))
                {
                    SetLastError(HPSqlError.Error_Parameter, "操作当前的托盘是无效的或当前的托盘正处于工作中，没有解绑");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (res == null)
                    return false;

                if (res.HasRows)
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    Int64 PCBID = ((Int64)res["PCBID"]);
                    res.Close();

                    //如果存在，更新解绑信息
                    if(PCBID != 0 && PCBID > 20190101000000)
                    Sql = string.Format("UPDATE ResultsInfoTable SET UnBindTime='{1}' WHERE PCBID='{0}'", PCBID.ToString(), strDataTime);
                    var N = RunSql(Sql);

                    //如果存在，更新绑定，清空虚拟PCBID，托盘状态变更为可用状态
                    Sql = string.Format("UPDATE TrayTable SET WorkOrderID='{1}',PCBQrCode='{2}',PCBID='{3}',BindTime='{4}',UnBindTime='{5}' ,CurrentState='{6}' where TrayQrCode='{0}'", TrayQrCode, "", "", "", "", "", "空闲");
                    N = RunSql(Sql);
                    if (N != 1)
                        return false;
                }
                else
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_UnBindError, "解绑失败，可以没有找到对应的托盘");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetTrayInfor(string TrayQrCode, out string PCBID, out string WrokOrde, out string PCBQrCode)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            PCBID = "";
            WrokOrde = "";
            PCBQrCode = "";

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                if (!SQL_IsTrayAvalible(TrayQrCode))
                {
                    SetLastError(HPSqlError.Error_Parameter, "操作当前的托盘是无效的或当前的托盘正处于工作中，没有解绑");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (res == null)
                    return false;

                if (res.HasRows)
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    PCBID = ((Int64)res["PCBID"]).ToString();
                    WrokOrde = (string)res["WorkOrderID"];
                    PCBQrCode = (string)res["PCBQrCode"];

                    res.Close();
                    return true;
                }
                else
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_UnBindError, "读取失败，可以没有找到对应的托盘");
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_SetStationResultStatus(string PCBID, WorkStationNameEnum emStation, bool ResultStatus)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (PCBID == null || PCBID.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"PCBID 参数据错误，为空");
                    return false;
                }

                if (emStation == WorkStationNameEnum.None)
                {
                    SetLastError(HPSqlError.Error_Parameter, "上传数据时工位没有补指定");
                    return false;
                }

                #region>>>>得到需要更新结果的字段
                string resultFiled = SQL_GetResultFiledFromID(emStation);
                if (resultFiled == null || resultFiled.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "无法得到当前工位结果指定的字段");
                    return false;
                }
                #endregion

                var Sql = string.Format("SELECT * FROM ResultsInfoTable WHERE PCBID='{0}'", PCBID);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_PCBNotExist, "当前的PCBID不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    int nStatus = (int)res["ResultStatus"];

                    int AddResult = 0x0000;
                    if(ResultStatus)
                    {
                        AddResult = (((int)0x0001) <<( (short) emStation));
                        nStatus |= AddResult;
                    }
                    else
                    {
                        AddResult = ~((((int)0x0001) << ((short)emStation)));
                        nStatus &= AddResult;
                    }

                    res.Close();

                    Sql = string.Format("UPDATE ResultsInfoTable SET {1}='{2}' where PCBID='{0}'", PCBID, "ResultStatus", nStatus.ToString());

                    var intRes = RunSql(Sql);
                    if (intRes != 1)
                        return false;
                }

                return true;

            }
            catch(Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
        }

        public bool SQL_SetStationResultStatusFromTray(string TrayQrCode, WorkStationNameEnum emStation, bool ResultStatus)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null  || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{TrayQrCode}不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    Int64 PCBID = (Int64)res["PCBID"];
                    string strDateTime = (string)res["BindTime"];
                    string CurrentState = (string)res["CurrentState"];

                    res.Close();
                    if (strDateTime.Length > 0 && PCBID > 20190101000000)
                        return SQL_SetStationResultStatus(PCBID.ToString(), emStation, ResultStatus);
                    else
                        return false;
                }

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetStationResultStatus(string PCBID, WorkStationNameEnum emStation)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (PCBID == null || PCBID.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"PCBID 参数据错误，为空");
                    return false;
                }

                if (emStation == WorkStationNameEnum.None)
                {
                    SetLastError(HPSqlError.Error_Parameter, "上传数据时工位没有指定");
                    return false;
                }

                #region>>>>得到需要更新结果的字段
                string resultFiled = SQL_GetResultFiledFromID(emStation);
                if (resultFiled == null || resultFiled.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "无法得到当前工位结果指定的字段");
                    return false;
                }
                #endregion

                #region>>>>得到PCBID 对应字段 ResultStatus 的结果并判断
                var Sql = string.Format("SELECT * FROM ResultsInfoTable WHERE PCBID='{0}'", PCBID);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_PCBNotExist, "当前的PCBID不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    int nStatus = (int)res["ResultStatus"];

                    res.Close();
                    int AddResult = (((int)0x0001) << ((short)emStation));
                    if ((nStatus & AddResult) == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetStationResultStatusFromTray(string TrayQrCode, WorkStationNameEnum emStation)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{TrayQrCode}不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    Int64 PCBID = (Int64)res["PCBID"];
                    string strDateTime = (string)res["BindTime"];
                    string CurrentState = (string)res["CurrentState"];

                    res.Close();
                    if (strDateTime.Length > 0 && PCBID > 20190101000000)
                        return SQL_GetStationResultStatus(PCBID.ToString(), emStation);
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                if(res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        /// <summary>
        /// 所有工位上传或更新数据
        /// </summary>
        /// <param name="QrCode"></param>
        /// <param name="PCBID"></param>
        /// <param name="itemparam"></param>
        /// <returns></returns>
        /// 
        public bool SQL_SetLastModeFlagAllProcess(string TrayQrCode, Int16[] lastModeFlag = null)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0 ||
                    lastModeFlag == null || lastModeFlag.Length < 100)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"SQL_SetLastModeFlagAllProcess TrayQrCode/ lastModeFlag 参数据错误，为空 ");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{TrayQrCode}不存在");
                    return false;
                }

                bool bRecord = res.Read();
                if (bRecord == false)
                {
                    res.Close();
                    return false;
                }

                Int64 PCBID = (Int64)res["PCBID"];
                string strDateTime = (string)res["BindTime"];
                string CurrentState = (string)res["CurrentState"];

                res.Close();
                if (strDateTime.Length <= 0 || PCBID < 20190101000000)
                    return false;

                Sql = string.Format("SELECT * FROM ResultsInfoTable WHERE PCBID='{0}'", PCBID);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_PCBNotExist, "当前的PCBID不存在");
                    return false;
                }

                res.Close();

                #region>>>>写入打码标志
                string temp = "";
                for (int nIndex = 0; nIndex < lastModeFlag.Length; nIndex++)
                {
                    if (nIndex == lastModeFlag.Length - 1)
                        temp += $"{lastModeFlag[nIndex].ToString("X02")}";
                    else
                        temp += $"{lastModeFlag[nIndex].ToString("X02")};";
                }

                Sql = string.Format("UPDATE ResultsInfoTable SET {1}='{2}' where PCBID='{0}'", PCBID, "LastModeFlagAllProcess", temp);

                int intRes = RunSql(Sql);
                if (intRes != 1)
                    return false;

                #endregion

                return true;
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }
        public bool SQL_SetStationResult(string PCBID, WorkStationNameEnum emStation, ResultInformation resultList, Int16[] lastModeFlag = null)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            try
            {
                if (PCBID == null || PCBID.Length <= 0 || resultList == null)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"PCBID / resultList 参数据错误，为空");
                    return false;
                }

                if (emStation == WorkStationNameEnum.None)
                {
                    SetLastError(HPSqlError.Error_Parameter, "上传数据时工位没有补指定");
                    return false;
                }

                //先绑盘在有更新上料数据
                //查询PCBID QrCode是否同时存在，如果不存在则为没有绑盘
                var Sql = string.Format("SELECT * FROM ResultsInfoTable WHERE PCBID='{0}'", PCBID);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();

                    SetLastError(HPSqlError.Error_PCBNotExist, "当前的PCBID不存在");
                    return false;
                }
                else
                {
                    res.Close();

                    #region>>>>得到需要更新结果的字段
                    string resultFiled = SQL_GetResultFiledFromID(emStation);
                    if (resultFiled == null || resultFiled.Length <= 0)
                    {
                        SetLastError(HPSqlError.Error_Parameter, "无法得到当前工位结果指定的字段");
                        return false;
                    }
                    #endregion

                    #region>>>>把结果转换成String字符串
                    string resultString = "";
                    resultList.ResultStatus = true;
                    for (int nIndex = 0; nIndex < resultList.Results.Length; nIndex++)
                    {
                        resultString += resultList.Results[nIndex].ToString();
                        if (resultList.Results[nIndex].CompareStatus == false)
                            resultList.ResultStatus = false;
                    }

                    #endregion

                    #region>>>>把结果转换成String XML字符串
                    string resultStringXML = "";
                    #endregion

                    #region>>>>写入结果
                    SQL_SetStationResultStatus(PCBID, emStation, resultList.ResultStatus);
                    Sql = string.Format("UPDATE ResultsInfoTable SET {1}='{2}' where PCBID='{0}'", PCBID, resultFiled, resultString );

                    var intRes = RunSql(Sql);
                    if (intRes != 1)
                        return false;

                    #endregion

                    #region>>>>写入打码标志
                    if (lastModeFlag != null && lastModeFlag.Length > 0)
                    {
                        string temp = "";
                        for (int nIndex = 0; nIndex < lastModeFlag.Length; nIndex++)
                        {
                            if (nIndex == lastModeFlag.Length - 1)
                                temp += $"{lastModeFlag[nIndex].ToString("X02")}";
                            else
                                temp += $"{lastModeFlag[nIndex].ToString("X02")};";
                        }

                        Sql = string.Format("UPDATE ResultsInfoTable SET {1}='{2}' where PCBID='{0}'", PCBID, "LastModeFlagAllProcess", temp);

                        intRes = RunSql(Sql);
                        if (intRes != 1)
                            return false;
                    }
                    #endregion
                }

                return true;
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetStationResult(string PCBID, WorkStationNameEnum emStation, out ResultInformation resultList)
        {
            resultList = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (PCBID == null || PCBID.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"PCBID 参数据错误，为空");
                    return false;
                }

                if (emStation == WorkStationNameEnum.None)
                {
                    SetLastError(HPSqlError.Error_Parameter, "上传数据时工位没有指定");
                    return false;
                }

                #region>>>>得到需要更新结果的字段
                string resultFiled = SQL_GetResultFiledFromID(emStation);
                if (resultFiled == null || resultFiled.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "无法得到当前工位结果指定的字段");
                    return false;
                }
                #endregion

                var Sql = string.Format("SELECT * FROM ResultsInfoTable WHERE PCBID='{0}'", PCBID);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_PCBNotExist, "当前的PCBID不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    string strResult = (string)res[resultFiled];
                    res.Close();

                    if (strResult.Length <= 0 || strResult.Contains("{") == false || strResult.Contains("}") == false)
                        return false;

                    ResultInformation resultsInfor = new ResultInformation("");
                    string[] arryList = strResult.Split("}".ToCharArray());
                    for (int nIndex = 0; nIndex < arryList.Length; nIndex++)
                    {
                        if (arryList[nIndex].Length <= 0 || strResult.Contains("{") == false)
                            continue;

                        arryList[nIndex] = arryList[nIndex].Replace("{", "");
                        string[] elements = arryList[nIndex].Split("|".ToCharArray());
                        /*                        string temp = "{" + $"{CriteriaName}|{CriteriaStantard}|{CriteriaMin}|{CriteriaMax}|{DataType.ToString()}|{CompareType.ToString()}|{Result}" + "}";*/
                        if (nIndex == 0)
                        {
                            resultsInfor.Results[nIndex].CriteriaName = elements[0];
                            resultsInfor.Results[nIndex].CriteriaStantard = elements[1];
                            resultsInfor.Results[nIndex].CriteriaMin = elements[2];
                            resultsInfor.Results[nIndex].CriteriaMax = elements[3];
                            resultsInfor.Results[nIndex].DataType = ResultDes.GetDataTypeFromString(elements[4]);
                            resultsInfor.Results[nIndex].CompareType = ResultDes.GetCompareTypeFromString(elements[5]);
                            resultsInfor.Results[nIndex].SetResult(elements[6]);

                        }
                        else
                        {
                            ResultDes[] Results = new ResultDes[1];
                            Results[0] = new ResultDes();

                            Results[0].CriteriaName = elements[0];
                            Results[0].CriteriaStantard = elements[1];
                            Results[0].CriteriaMin = elements[2];
                            Results[0].CriteriaMax = elements[3];
                            Results[0].DataType = ResultDes.GetDataTypeFromString(elements[4]);
                            Results[0].CompareType = ResultDes.GetCompareTypeFromString(elements[5]);
                            Results[0].SetResult(elements[6]);

                            resultsInfor.Results = resultsInfor.Results.Concat(Results).ToArray();
                        }

                    }

                    resultList = resultsInfor;
                }

                return true;

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetStationResultFromTray(string TrayQrCode, WorkStationNameEnum emStation, out ResultInformation resultList)
        {
            resultList = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                if (emStation == WorkStationNameEnum.None)
                {
                    SetLastError(HPSqlError.Error_Parameter, "上传数据时工位没有指定");
                    return false;
                }

                #region>>>>得到需要更新结果的字段
                string resultFiled = SQL_GetResultFiledFromID(emStation);
                if (resultFiled == null || resultFiled.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "无法得到当前工位结果指定的字段");
                    return false;
                }
                #endregion

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{TrayQrCode}不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    Int64 PCBID = (Int64)res["PCBID"];
                    string strDateTime = (string)res["BindTime"];
                    string CurrentState = (string)res["CurrentState"];

                    res.Close();
                    if (strDateTime.Length > 0 && PCBID > 20190101000000)
                        return SQL_GetStationResult(PCBID.ToString(), emStation, out resultList);
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public  bool SQL_SetFlashResultStationResult(string PCBID, Int16[] Flashresult, Int16[] lastModeFlag)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (PCBID == null || PCBID.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"PCBID 参数据错误，为空");
                    return false;
                }

                if (Flashresult == null || Flashresult.Length < 100)
                {
                    return false;
                }

                string temp = "";
                bool bResultFlash = true;
                for (int nIndex = 0; nIndex < Flashresult.Length; nIndex++)
                {
                    if (nIndex == Flashresult.Length - 1)
                        temp += $"{Flashresult[nIndex].ToString("X02")}";
                    else
                        temp += $"{Flashresult[nIndex].ToString("X02")};";

                    if (Flashresult[nIndex] != 0x00)
                        bResultFlash = false;
                }

                ResultInformation resultList = new ResultInformation("FlashStation Result");
                resultList.ResultStatus = bResultFlash;
                resultList.Results[0].SetResult(temp);
                resultList.Results[0].CompareStatus = bResultFlash;
                return SQL_SetStationResult(PCBID, WorkStationNameEnum.烧录工站, resultList, lastModeFlag);

            }
            catch (Exception ex)
            {
                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
        }

        public bool SQL_SetFlashResultStationResultFromTray(string TrayQrCode, Int16[] FlashResult, Int16[] lastModeFlag = null)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                if (FlashResult == null || FlashResult.Length < 100)
                {
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{TrayQrCode}不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    Int64 PCBID = (Int64)res["PCBID"];
                    string strDateTime = (string)res["BindTime"];
                    string CurrentState = (string)res["CurrentState"];

                    res.Close();
                    if (strDateTime.Length > 0 && PCBID > 20190101000000)
                        return SQL_SetFlashResultStationResult(PCBID.ToString(), FlashResult, lastModeFlag);
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetFlashResult(string PCBID, out Int16[] FlashResult)
        {
            FlashResult = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (PCBID == null || PCBID.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"PCBID 参数据错误，为空");
                    return false;
                }

                ResultInformation resultList = null;
                bool result = SQL_GetStationResult(PCBID, WorkStationNameEnum.烧录工站, out resultList);
                if (result == false || resultList == null)
                    return false;

                string strResult = resultList.Results[0].Result;
                string[] arrResult = strResult.Split(";".ToCharArray());
                if (arrResult == null || arrResult.Length < 100)
                    return false;

                FlashResult = new Int16[arrResult.Length];
                for (int nIndex = 0; nIndex < FlashResult.Length; nIndex++)
                {
                    try
                    {
                        FlashResult[nIndex] = Int16.Parse(arrResult[nIndex], System.Globalization.NumberStyles.HexNumber);
                    }
                    catch(Exception ex)
                    {
                        SetLastError(HPSqlError.Error_DataError, $"数据错误");
                        FlashResult[nIndex] = 0xFF;
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetFlashResultFromTray(string TrayQrCode, out Int16[] FlashResult)
        {
            FlashResult = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{TrayQrCode}不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    Int64 PCBID = (Int64)res["PCBID"];
                    string strDateTime = (string)res["BindTime"];
                    string CurrentState = (string)res["CurrentState"];

                    res.Close();
                    if (strDateTime.Length > 0 && PCBID > 20190101000000)
                        return SQL_GetFlashResult(PCBID.ToString(), out FlashResult);
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetFlashResultFromTray(string TrayQrCode, out Int16[,] FlashResult)
        {
            FlashResult = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                Int16[] FlashResult1D = null;
                SQL_GetFlashResultFromTray(TrayQrCode, out FlashResult1D);
                if (FlashResult1D == null || FlashResult1D.Length < 100)
                    return false;

                FlashResult = new Int16[10,10];
                int Index = 0;
                for (int nRow = 0; nRow < 10; nRow++)
                {
                    for (int nCol = 0; nCol < 10; nCol++)
                    {
                        FlashResult[nRow, nCol] = FlashResult1D[Index++];
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_RecordePackingInfo(string workOrder, string barCode, string PCBIDs)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (workOrder == null || workOrder.Length <= 0 ||
                    barCode == null || barCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"参数据错误，为空");
                    return false;
                }

                string Sql = string.Format("Insert into PackingPrint(WorkOrderID,RecordTime,BarcodePrint,PCBIDs) values('{0}','{1}','{2}','{3}')", workOrder, strDataTime, barCode, PCBIDs);
                var EXres = RunSql(Sql);
                if (EXres <= 0)
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_WriteWrokSheetInfo(string workSheet, string InfoXMl, bool bActived = false)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (workSheet == null || workSheet.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "SQL_WriteWrokSheetInfo 传入参数错误");
                    return false;
                }

                #region>>>>>更新工单信息
                string Sql = string.Format("SELECT * FROM WorkSheetTable WHERE WorkSheet='{0}'", workSheet);
                res = ExecuteReader(Sql);
                if (res == null)
                    return false;

                if (!res.HasRows)
                {
                    res.Close();
                    Sql = string.Format("Insert into WorkSheetTable(WorkSheet,ActiveStatus,DetailContent,StartTime) values('{0}','{1}','{2}','{3}')", workSheet, "", InfoXMl??"", strDataTime);//BindFlag绑定必须是1，UnBind必须是0
                    var EXres = RunSql(Sql);
                    if (EXres <= 0)
                        return false;
                }
                else
                {
                    res.Close();
                    Sql = string.Format("UPDATE WorkSheetTable Set ActiveStatus='{1}',DetailContent='{2}' where WorkSheet='{0}'", workSheet, "", InfoXMl ?? "");
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;
                }
                #endregion

                #region>>>>>更新激活信息
                if (bActived)
                    return SQL_SetActiveWorkSheet(workSheet);

                #endregion

                return true;
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_SetActiveWorkSheet(string workSheet)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;
            try
            {
                var Sql = string.Format("SELECT * FROM WorkSheetTable where WorkSheet='{0}'", workSheet);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{workSheet}不存在");
                    return false;
                }
                else
                {
                    res.Close();
                    Sql = string.Format("SELECT * FROM WorkSheetTable");
                    res = ExecuteReader(Sql);

                    string[] ArryWorkSheet = new string[0];
                    while (res.Read())
                    {
                        string workSheetTemp = (string)res["WorkSheet"];
                        ArryWorkSheet = ArryWorkSheet.Concat(new string[1] { workSheetTemp }).ToArray();
                    }

                    res.Close();
                    foreach (string ordeName in ArryWorkSheet)
                    {
                        if (ordeName == workSheet)
                            Sql = string.Format("UPDATE WorkSheetTable Set ActiveStatus='{1}' where WorkSheet='{0}'", ordeName, "Active");
                        else
                            Sql = string.Format("UPDATE WorkSheetTable Set ActiveStatus='{1}' where WorkSheet='{0}'", ordeName, "");

                        var intres = RunSql(Sql);
                        if (intres != 1)
                            return false;
                    }

                    return true;
                }
            }
            catch(Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
        }

        public bool SQL_SetWorkSheetStartEndTime(string workSheet, bool updateStartOrEndTime = true)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;
            try
            {
                var Sql = string.Format("SELECT * FROM WorkSheetTable where WorkSheet='{0}'", workSheet);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{workSheet}不存在");
                    return false;
                }
                else
                {
                    if(res.Read())
                    {
                        string tagValue = (updateStartOrEndTime ? "StartTime" : "CompletedTime");
                        object obj = res[tagValue];
                        res.Close();
                        if (obj == System.DBNull.Value || ((string)obj).Length <= 0)
                        {
                            Sql = $"UPDATE WorkSheetTable Set {tagValue}='{strDataTime}' where WorkSheet='{workSheet}'" ;
                            var intres = RunSql(Sql);
                            if (intres != 1)
                                return false;
                        }
                    }
                    else
                    {
                        res.Read();
                        return false;
                    }
                
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }
        public bool SQL_GetActivedWorkSheetInfo(out string workSheet, out bool bActived, out HpMesParameter info)
        {
            workSheet = null;
            bActived = false;
            info = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            try
            {
                var Sql = string.Format("SELECT * FROM WorkSheetTable");
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close(); 
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{workSheet}不存在");
                    return false;
                }
                else
                {
//                     DataTable table = new DataTable();
//                     table.Load(res);
                    while (res.Read())
                    {
                        string strActived = (string)res["ActiveStatus"];
                        if(strActived != null && strActived.Contains("Active"))
                        {
                            bActived = true;
                            workSheet = (string)res["WorkSheet"];
                            res.Close();

                            return SQL_GetWorkSheetInfo(workSheet, out bActived, out info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
            return false;
        }
        public bool SQL_GetWorkSheetInfo(string workSheet, out bool bActived, out HpMesParameter info)
        {
            bActived = false;
            info = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null; 

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                var Sql = string.Format("SELECT * FROM WorkSheetTable WHERE WorkSheet='{0}'", workSheet);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{workSheet}不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                        return false;
                    string strActived = (string)res["ActiveStatus"];
                    string DetailContext = (string)res["DetailContent"];
                    res.Close();

                    if (DetailContext != null && DetailContext.Length > 0)
                    {
                        info = Newtonsoft.Json.JsonConvert.DeserializeObject<HpWebService.HpMesParameter>(DetailContext);
                        return true;
                    }
                }

                return false;

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetLastModeFlagAllProcess(string PCBID, out Int16[] FlagResult)
        {
            FlagResult = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (PCBID == null || PCBID.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"PCBID 参数据错误，为空");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM ResultsInfoTable WHERE PCBID='{0}'", PCBID);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();

                    SetLastError(HPSqlError.Error_PCBNotExist, "当前的PCBID不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                    {
                        res.Close();
                        return false;
                    }

                    string strResult = (string)res["LastModeFlagAllProcess"];
                    res.Close();

                    string[] arrResult = strResult.Split(";".ToCharArray());
                    if (arrResult == null || arrResult.Length < 100)
                        return false;

                    FlagResult = new Int16[arrResult.Length];
                    for (int nIndex = 0; nIndex < FlagResult.Length; nIndex++)
                    {
                        try
                        {
                            FlagResult[nIndex] = Int16.Parse(arrResult[nIndex], System.Globalization.NumberStyles.HexNumber);
                        }
                        catch (Exception ex)
                        {
                            if (res != null)
                                res.Close();

                            SetLastError(HPSqlError.Error_DataError, $"数据错误");
                            FlagResult[nIndex] = 0xFF;
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }
        public bool SQL_GetLastModeFlagAllProcessFromTray(string TrayQrCode, out Int16[] FlagResult)
        {
            FlagResult = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (TrayQrCode == null || TrayQrCode.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, $"TrayQrCode 参数据错误，为空");
                    return false;
                }

                var Sql = string.Format("SELECT * FROM TrayTable WHERE TrayQrCode='{0}'", TrayQrCode);
                res = ExecuteReader(Sql);
                if (!res.HasRows)
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_TrayNotExist, $"当前的Tray{TrayQrCode}不存在");
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                        return false;

                    Int64 PCBID = (Int64)res["PCBID"];
                    string strDateTime = (string)res["BindTime"];
                    string CurrentState = (string)res["CurrentState"];

                    res.Close();
                    if (strDateTime.Length > 0 && PCBID > 20190101000000)
                        return SQL_GetLastModeFlagAllProcess(PCBID.ToString(), out FlagResult);
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }
        public bool SQL_GetLastModeFlagAllProcessFromTray(string TrayQrCode, out Int16[,] FlagResult)
        {
            FlagResult = null;
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                Int16[] FlashResult1D = null;
                SQL_GetLastModeFlagAllProcessFromTray(TrayQrCode, out FlashResult1D);
                if (FlashResult1D == null || FlashResult1D.Length < 100)
                    return false;

                FlagResult = new Int16[10, 10];
                int Index = 0;
                for (int nRow = 0; nRow < 10; nRow++)
                {
                    for (int nCol = 0; nCol < 10; nCol++)
                    {
                        FlagResult[nRow, nCol] = FlashResult1D[Index++];
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public class UserInfo
        {
            public string JobNumber = "";
            public string UserName = "未定义";
            public string Title = "操作员";
            public string Gender = "未定义";
            public string Password = "";
            public string PreviligeGroup = "操作员"; //管理员  工程师   操作员
            public string RecordeTime;

            public UserInfo()
            {
                DateTime dataTimeNow = DateTime.Now;
                RecordeTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public bool SQL_AddUser(UserInfo Info, bool bUpdateIfExist = true)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;

            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (Info.JobNumber == null || Info.JobNumber.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "传入参数错误");
                    return false;
                }

                string Sql = string.Format("SELECT * FROM OperatorTable WHERE JobNumber='{0}'", Info.JobNumber);
                res = ExecuteReader(Sql);
                if (res == null)
                    return false;

                if (!res.HasRows)
                {
                    res.Close();

                    //如果没有添加新的 QrCode 状态是没有绑定的
                    Sql = string.Format("Insert into OperatorTable(JobNumber,UserName,Title,Gender,Password,PreviligeGroup,RecordeTime) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                                                     Info.JobNumber,
                                                     Info.UserName,
                                                     Info.Title,
                                                     Info.Gender,
                                                     Info.Password,
                                                     Info.PreviligeGroup,
                                                     Info.RecordeTime);
                    var EXres = RunSql(Sql);
                    if (EXres <= 0)
                        return false;
                }
                else if (bUpdateIfExist && res.HasRows)
                {
                    res.Close();
                    Sql = string.Format("UPDATE OperatorTable Set UserName='{1}',Title='{2}',Gender='{3}' ,Password='{4}',PreviligeGroup='{5}',RecordeTime='{6}' where JobNumber='{0}'",
                                                     Info.JobNumber,
                                                     Info.UserName,
                                                     Info.Title,
                                                     Info.Gender,
                                                     Info.Password,
                                                     Info.PreviligeGroup, 
                                                     Info.RecordeTime);
                    var intres = RunSql(Sql);
                    if (intres != 1)
                        return false;

                }
                else
                {
                    res.Close();
                    SetLastError(HPSqlError.Error_Parameter, "当前用户已存在，不允许更新");
                    return false;
                }
              
                return true;
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        public bool SQL_GetUserGroup(string userName, out string group)
        {
            DateTime dataTimeNow = DateTime.Now;
            string strDataTime = dataTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            SqlDataReader res = null;
            group = "操作员";
            SetLastError(HPSqlError.Error_NoError, $"");

            try
            {
                if (userName == null || userName.Length <= 0)
                {
                    SetLastError(HPSqlError.Error_Parameter, "传入参数错误");
                    return false;
                }

                string Sql = string.Format("SELECT * FROM OperatorTable WHERE JobNumber='{0}'", userName);
                res = ExecuteReader(Sql);
                if (res == null)
                    return false;

                if (!res.HasRows)
                {
                    res.Close();
                    return false;
                }
                else
                {
                    bool bRecord = res.Read();
                    if (bRecord == false)
                        return false;

                    if(res["PreviligeGroup"] != System.DBNull.Value)
                        group = (string)res["PreviligeGroup"];

                    res.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                if (res != null)
                    res.Close();

                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return false;
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        /// <summary>
        /// 执行查询语句，返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>SqlDataReader</returns>
        public SqlDataReader ExecuteReader(string strSQL)
        {
            lock(_conection)
            {
                if (_conection.State.ToString() != "Open")
                    _conection.Open();

                SqlDataReader myReader = null;
                SqlCommand cmd = new SqlCommand(strSQL, _conection);
                try
                {
                    myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    return myReader;
                }
                catch (SqlException ex)
                {
                    if (myReader != null)
                        myReader.Close();

                    SetLastError(HPSqlError.Error_Exception, ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public DataSet Query(string SQLString)
        {
            using (SqlConnection connection = new SqlConnection(connSql))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter command = new SqlDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (SqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }

        /// 要执行SQL语句，该方法返回一个DataTable
        /// <param name="sql">执行SQL语句</param>
        public DataTable DtQuery(string sql)//外部 统计调用了
        {
            if (_conection.State.ToString() != "Open")
                _conection.Open();

            using (SqlDataAdapter sqlda = new SqlDataAdapter(sql, _conection))
            {
                using (DataTable dt = new DataTable())
                {
                    sqlda.Fill(dt);
                    return dt;
                }
            }
        }

        /// 执行SQL语句，返回影响的记录行数
        /// <param name="sql">要执行的SQL语句</param>
        private int RunSql(string sql)//外部调用
        {
            int result = -1;
            try
            {
                lock(_conection)
                {
                    if (_conection.State.ToString() != "Open")
                        _conection.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, _conection))
                    {
                        result = cmd.ExecuteNonQuery();
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                SetLastError(HPSqlError.Error_Exception, ex.Message);
                return result;
            }
        }
    }
}
