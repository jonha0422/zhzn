﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Net.Sockets;
using System.Net;
using HslCommunication.Core.Net;
using Apk_HP_Application.程序文件夹分类._0全局全量;
using Apk_HP_Application.程序文件夹分类._1调度总程序;

namespace Apk_HP_Application.程序文件夹分类._1调度总程序
{
    public partial class frm_LoadStation : DevExpress.XtraEditors.XtraForm
    {
        TcpClient stationClient = null;
        Thread threadWR = null;
        public frm_LoadStation()
        {
            InitializeComponent();

        }

        ~frm_LoadStation()
        {
            
        }

        public static void ProcessTCP_Call(object state)
        {
            try
            {
                string[] Parameters = null;
                TcpClient tcpClient = (TcpClient)state;
                string requirement = HPglobal.Server_GetClient_Requirement(tcpClient, ref Parameters);
                if (requirement != null && requirement.Contains(""))
                {
                    
                }

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("ProcessTCP_Call() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");

            }
        }

        public void Client_ReadAndWrite(object wsarg)
        {
            string strIP = stationClient.Client.RemoteEndPoint.ToString();
            while (!HPglobal.m_bQuitApp)
            {
                try
                {
                    //重新开始新的数据读取
                    Thread.Sleep(1500);
                    if (stationClient.Client == null || stationClient.Client.Poll(1, SelectMode.SelectRead) && (stationClient.Client.Available == 0) ||
                        stationClient.Connected == false ||
                        stationClient.Client.Send(System.Text.Encoding.ASCII.GetBytes("Test\r\n")) <= 0)
                    {
                        LEDClient.LanternBackground = (LEDClient.LanternBackground == System.Drawing.Color.Red ? System.Drawing.Color.Yellow : System.Drawing.Color.Red);
                        string error = string.Format("请注意: 客户端网络已断开(计算机：{0})", stationClient.Client.RemoteEndPoint);
                        HPglobal.RecordeAllMessage(error, true, false, "请检查电脑或下位机是否开机或连机及网线是否连接好");
                        stationClient.Close();
                    }
                    else
                        LEDClient.LanternBackground = (LEDClient.LanternBackground == System.Drawing.Color.Green ? System.Drawing.Color.Yellow : System.Drawing.Color.Green);

                    //ProcessTCP_Call(stationClient);
                }
                catch (Exception ex)
                {
                    HPglobal.m_strLastException = "Client_ReadAndWrite异常:\r\n" + ex.Message;
                }
            }

            //断开连接
            try
            {
                if (stationClient.Client != null)
                    stationClient.Client.Disconnect(false);
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("Client_ReadAndWrite() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");

            }
        }

        public void SetClient(TcpClient client)
        {
            stationClient = client;

            //建立数据读写线程
            if(threadWR == null)
            {
                threadWR = new Thread(new ParameterizedThreadStart(Client_ReadAndWrite));
                threadWR.Start(stationClient);
                HPInvoke.m_listThreadClient.Add(threadWR);
            }

        }

        public bool GetSN(ref string PCBSN, ref string TrayID)
        {
            try
            {
                PCBSN = "";
                TrayID = "";
                if (stationClient == null || stationClient.Client == null ||
                    stationClient.Client.Send(System.Text.Encoding.ASCII.GetBytes($"GetSN;{HPInvoke.ObjAllParameters.APKWorkOrderInfo.PcbNumber}\r\n")) <= 0) //stationClient.Client.Send(System.Text.Encoding.ASCII.GetBytes("GetSN\r\n")
                {
                    HPglobal.m_strLastException = $"GetSN() 异常, 上料客户端没有连接或发送异常!";
                    HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
                    return false;
                }

                string strEndPoint = stationClient.Client.LocalEndPoint.ToString();
                string responed = "";
                DateTime time1, time2;
                time1 = time2 = DateTime.Now;
                while (!HPglobal.m_bQuitApp)
                {
                    Application.DoEvents();
                    time2 = DateTime.Now;
                    if ((time2 - time1).TotalSeconds > 60)
                    {
                        HPglobal.RecordeAllMessage($"客户端{strEndPoint}没有在5s以内发送工站信息过来，将自动断开连接", true, false, "数据需要以\r\n (CRLF) 作为结束符");
                        break;
                    }

                    string[] Parameters = null;
                    responed = HPglobal.Server_GetClient_Requirement(stationClient, ref Parameters);
                    if (responed != null && responed.Contains("GetSN:NG"))
                        return false;
                    else if (responed != null && responed.Contains("GetSN:OK"))
                    {
                        string[] arryStr = responed.Split(";".ToCharArray());
                        if (arryStr.Length < 3)
                            return false;

                        PCBSN = arryStr[1];
                        TrayID = arryStr[2];

                        this.Invoke(new Action(() =>
                        {
                            txtPCBSN.Text = arryStr[1];
                            txtTRAYID.Text = arryStr[2];
                        }));

                        if (HPInvoke.ObjAllParameters.PCBAName.Length > 0 && !PCBSN.Contains(HPInvoke.ObjAllParameters.PCBAName))
                        {
                            HPglobal.RecordeAllMessage($"失败错误: 得到上料工位包含的PCB板号不对，与定义的不符({PCBSN} 不包含 {HPInvoke.ObjAllParameters.PCBAName})", true, false, "");
                            return false;
                        }

                        return true;
                    }
                }

            }
            catch(Exception ex)
            {
                PCBSN = "";
                TrayID = "";
                HPglobal.m_strLastException = string.Format("GetSN() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");

            }
            return false;
        }

        private void frm_LoadStation_Load(object sender, EventArgs e)
        {
           
        }

        private void btmMQQT_Write_Click(object sender, EventArgs e)
        {
            try
            {
                string a = "";
                string b = "";
                bool result  = HPInvoke.panel_Load.GetSN(ref a, ref b);

                txtPCBSN.Text = a;
                txtTRAYID.Text = b;
            }
            catch(Exception ex)
            {

            }

        }

        private void btmMQQT_Read_Click(object sender, EventArgs e)
        {

        }
    }
}