using System;
using HalconDotNet;

namespace InteractiveROI
{
	/// <summary>
	/// This class demonstrates one of the possible implementations for a 
	/// circular ROI. ROICircle inherits from the base class ROI and 
	/// implements (besides other auxiliary methods) all virtual methods 
	/// defined in ROI.cs.
	/// </summary>
	public class ROICircle : ROI
	{

		private double radius;
		private double row1, col1;  // first handle
		private double midR, midC;  // second handle

        public double Radius
        {
            get
            {
                return radius;
            }

            set
            {
                radius = value;
            }
        }

        public double Row1
        {
            get
            {
                return row1;
            }

            set
            {
                row1 = value;
            }
        }

        public double Col1
        {
            get
            {
                return col1;
            }

            set
            {
                col1 = value;
            }
        }

        public double MidR
        {
            get
            {
                return midR;
            }

            set
            {
                midR = value;
            }
        }

        public double MidC
        {
            get
            {
                return midC;
            }

            set
            {
                midC = value;
            }
        }

        public ROICircle()
		{
			NumHandles = 2; // one at corner of circle + midpoint
			activeHandleIdx = 1;
		}



		/// <summary>Creates a new ROI instance at the mouse position</summary>
		public override void createROI(double midX, double midY)
		{
			MidR = midY;
			MidC = midX;

			Radius = 100;

			Row1 = MidR;
			Col1 = MidC + Radius;
		}

		/// <summary>Paints the ROI into the supplied window</summary>
		/// <param name="window">HALCON window</param>
		public override void draw(HalconDotNet.HWindow window)
		{
			window.DispCircle(MidR, MidC, Radius);
			window.DispRectangle2(Row1, Col1, 0, 5, 5);
			window.DispRectangle2(MidR, MidC, 0, 5, 5);
		}

		/// <summary> 
		/// Returns the distance of the ROI handle being
		/// closest to the image point(x,y)
		/// </summary>
		public override double distToClosestHandle(double x, double y)
		{
			double max = 10000;
			double [] val = new double[NumHandles];

			val[0] = HMisc.DistancePp(y, x, Row1, Col1); // border handle 
			val[1] = HMisc.DistancePp(y, x, MidR, MidC); // midpoint 

			for (int i=0; i < NumHandles; i++)
			{
				if (val[i] < max)
				{
					max = val[i];
					activeHandleIdx = i;
				}
			}// end of for 
			return val[activeHandleIdx];
		}

		/// <summary> 
		/// Paints the active handle of the ROI object into the supplied window 
		/// </summary>
		public override void displayActive(HalconDotNet.HWindow window)
		{

			switch (activeHandleIdx)
			{
				case 0:
					window.DispRectangle2(Row1, Col1, 0, 5, 5);
					break;
				case 1:
					window.DispRectangle2(MidR, MidC, 0, 5, 5);
					break;
			}
		}

		/// <summary>Gets the HALCON region described by the ROI</summary>
		public override HRegion getRegion()
		{
			HRegion region = new HRegion();
			region.GenCircle(MidR, MidC, Radius);
			return region;
		}

		public override double getDistanceFromStartPoint(double row, double col)
		{
			double sRow = MidR; // assumption: we have an angle starting at 0.0
			double sCol = MidC + 1 * Radius;

			double angle = HMisc.AngleLl(MidR, MidC, sRow, sCol, MidR, MidC, row, col);

			if (angle < 0)
				angle += 2 * Math.PI;

			return (Radius * angle);
		}

		/// <summary>
		/// Gets the model information described by 
		/// the  ROI
		/// </summary> 
		public override HTuple getModelData()
		{
			return new HTuple(new double[] { MidR, MidC, Radius });
		}

		/// <summary> 
		/// Recalculates the shape of the ROI. Translation is 
		/// performed at the active handle of the ROI object 
		/// for the image coordinate (x,y)
		/// </summary>
		public override void moveByHandle(double newX, double newY)
		{
			HTuple distance;
			double shiftX,shiftY;

			switch (activeHandleIdx)
			{
				case 0: // handle at circle border

					Row1 = newY;
					Col1 = newX;
					HOperatorSet.DistancePp(new HTuple(Row1), new HTuple(Col1),
											new HTuple(MidR), new HTuple(MidC),
											out distance);

					Radius = distance[0].D;
					break;
				case 1: // midpoint 

					shiftY = MidR - newY;
					shiftX = MidC - newX;

					MidR = newY;
					MidC = newX;

					Row1 -= shiftY;
					Col1 -= shiftX;
					break;
			}
		}
	}//end of class
}//end of namespace
