﻿using HalconDotNet;
using InteractiveROI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib.ImageProcess
{
    public class Result
    {
        public bool isOk { get; set; }
        public double cX { get; set; }
        public double cY { get; set; }
        public double Phi { get; set; }
        public double length1 { get; set; }
        public double length2 { get; set; }
        public string Str { get; set; }
    }
    public class ImageProcessBase
    {
        [JsonIgnore]
        public HWindowControl ViewPort{ get; set; }
        public string cName { get; set; }
        [JsonIgnore]
        public HObject TempImage;//模板图像
        [JsonIgnore]
        public HObject TempDesImage;//处理后的模板；主要用来显示
        [JsonIgnore]
        public HObject SrcObject;//输入处理源图像
        [JsonIgnore]
        public HObject DesObject;//处理后的图像
        //public int inPutType {get;set;}
       // public int outPutType {get;set;}
        [Category("结果")]
        [JsonIgnore]
        [ReadOnly(true)]
        public List<Result> mResult { get; set; }//反馈的结果
        [Category("结果")]
        [ReadOnly(true)]
        [JsonIgnore]
        public bool ResultSta { get; set; }

        public int SearchRoiCount { get; set; }//搜索范围
        public int ModelRoiCount { get; set; }//搜索范围
        public List<ROIRectangle2> SearchRoi { get; set; }//搜索范围
        public List<ROIRectangle2> ModelRoi { get; set; }//模板对象
        public Threshold PreThreshold;

        [JsonIgnore]
        public int WordSpaceID;//工位ID
        [JsonIgnore]
        public int CamID;//相机ID

        public bool isEpson { get; set; }//模板对象
        public ImageProcessBase()
        {
            isEpson = false;
            mResult = new  List<Result>();

        }
        public virtual int Init(HImage vTempImage)
        {
            return 0;
        }
        public virtual int ImageProcess()
        {
            return 0;
        }
        public virtual ImageProcessBase CreatClass(string ClassPara)
        {
            ImageProcessBase aFun = new ImageProcessBase();
            return aFun;
        }
        public virtual string GetClassPara()
        {
            return "";
        }
        public virtual int calibration()
        {
            return 0;
        }
        public virtual int MoveToStartPos()
        {
            return 0;
        }
    }
}
