﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using HalconDotNet;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using static ZHToolBox.HalconVision.HDVisionTypes;


namespace ZHToolBox.HalconVision
{
    public class DeformableInspector : MethodBase
    {
        #region>>>>>>>>> 输入
        bool TemplateIsCreated = false;
        #endregion

        [XmlIgnoreAttribute]
        public int WordSpaceID;

        [XmlIgnoreAttribute]
        object threadObj = new object();

        HObject objImage_Temp = null;
        HObject objImage_Check = null;

        HTuple hv_VariationModelID;
        HTuple hv_ModelID;

        #region>>>>>>>>> 配制参数  //Category("选项"), DefaultValue(3), DisplayName("测试")
        [Editor(typeof(PropertyGridTempleteUI), typeof(UITypeEditor)), Category("配置"), DefaultValue(""), DisplayName("模版图像文件")]
        public string tempFileName { get; set; }

        [Editor(typeof(PropertyGridTempleteUI), typeof(UITypeEditor)), Category("配置"), DefaultValue(""), DisplayName("测量图像文件")]
        public string ImageFileName { get; set; }
        #endregion

        #region>>>>>>>>> 建模参数   HTuple absThreshold, HTuple varThreshold
        [Category("建模参数"), DefaultValue(3), DisplayName("建模等级")]
        public int hv_Smoothness { get; set; }

        [Category("建模参数"), DefaultValue(3), DisplayName("Canny 平滑系数")]
        public Int32 Canny_Alpha { get; set; }

        [Category("建模参数"), DefaultValue(3), DisplayName("absThreshold")]
        public Int32 absThreshold { get; set; }

        [Category("建模参数"), DefaultValue(3), DisplayName("varThreshold")]
        public Int32 varThreshold { get; set; }

        [Category("建模参数"), DefaultValue(3), DisplayName("图像二值化最小值")]
        public double ThresholdMin { get; set; }

        [Category("建模参数"), DisplayName("图像二值化最大值")]
        public double ThresholdMax { get; set; }

        [Category("建模参数"), DefaultValue(-10), DisplayName("开始角度")]
        public double AngleStart_T { get; set; }  // angle.rad → (real)  Smallest rotation of the model. Default value: -0.39 Suggested values: -3.14, -1.57, -0.78, -0.39, -0.20, 0.0

        [Category("建模参数"), DefaultValue(20), DisplayName("角度范转")]
        public double AngleExtent_T { get; set; }  //angle.rad → (real) Extent of the rotation angles. Default value: 0.78 Suggested values: 6.29, 3.14, 1.57, 0.78, 0.39, 0.0 Restriction: AngleExtent >= 0

        [Category("建模参数"), DefaultValue(20), DisplayName("角度范转步进")]
        public double AngleStep_T { get; set; }  //Step length of the angles(resolution). Default value: 'auto'  Suggested values: 'auto', 0.0175, 0.0349, 0.0524, 0.0698, 0.0873 Restriction: AngleStep >= 0

        [Category("建模参数"), DefaultValue(1), DisplayName("Row 最小比例")]
        public double ScaleRMin_T { get; set; } //number → (real) Minimum scale of the model in row direction. Default value: 1.0 Suggested values: 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 Restriction: ScaleRMin > 0

        [Category("建模参数"), DefaultValue(1), DisplayName("Row 最大比例")]
        public double ScaleRMax_T { get; set; }  //number → (real) Maximum scale of the model in row direction. Default value: 1.0 Suggested values: 1.0, 1.1, 1.2, 1.3, 1.4, 1.5 Restriction: ScaleRMax >= ScaleRMin

        [Category("建模参数"), DefaultValue(1), DisplayName("Row 比例步进")]
        public double ScaleRStep_T { get; set; }  //Scale step length(resolution) in row direction. Default value: 'auto' Suggested values: 'auto', 0.01, 0.02, 0.05, 0.1, 0.15, 0.2 Restriction: ScaleRStep >= 0

        [Category("建模参数"), DefaultValue(1), DisplayName("Col 最小比例")]
        public double ScaleCMin_T { get; set; }  //number → (real) Minimum scale of the model in column direction. Default value: 1.0 Suggested values: 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 Restriction: ScaleCMin > 0

        [Category("建模参数"), DefaultValue(1), DisplayName("Col 最大比例")]
        public double ScaleCMax_T { get; set; }  //number → (real) Maximum scale of the model in column direction. Default value: 1.0 Suggested values: 1.0, 1.1, 1.2, 1.3, 1.4, 1.5 Restriction: ScaleCMax >= ScaleCMin

        [Category("建模参数"), DefaultValue(1), DisplayName("Clo 比例步进")]
        public double ScaleCStep_T { get; set; }  //Scale step length(resolution) in col direction. Default value: 'auto' Suggested values: 'auto', 0.01, 0.02, 0.05, 0.1, 0.15, 0.2 Restriction: ScaleCStep >= 0
        #endregion

        #region>>>>>>>>> 检测参数
        [Category("检测参数"), DefaultValue(-10), DisplayName("颗粒面积最小")]
        public double AreaMin { get; set; }  // angle.rad → (real)  Smallest rotation of the model. Default value: -0.39 Suggested values: -3.14, -1.57, -0.78, -0.39, -0.20, 0.0

        [Category("检测参数"), DefaultValue(-10), DisplayName("开始角度")]
        public double AngleStart { get; set; }  // angle.rad → (real)  Smallest rotation of the model. Default value: -0.39 Suggested values: -3.14, -1.57, -0.78, -0.39, -0.20, 0.0

        [Category("检测参数"), DefaultValue(20), DisplayName("角度范转")]
        public double AngleExtent { get; set; }  //angle.rad → (real) Extent of the rotation angles. Default value: 0.78 Suggested values: 6.29, 3.14, 1.57, 0.78, 0.39, 0.0 Restriction: AngleExtent >= 0

        [Category("检测参数"), DefaultValue(1), DisplayName("Y最小比例")]
        public double ScaleRMin { get; set; } //number → (real) Minimum scale of the model in row direction. Default value: 1.0 Suggested values: 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 Restriction: ScaleRMin > 0

        [Category("检测参数"), DefaultValue(1), DisplayName("Y最大比例")]
        public double ScaleRMax { get; set; }  //number → (real) Maximum scale of the model in row direction. Default value: 1.0 Suggested values: 1.0, 1.1, 1.2, 1.3, 1.4, 1.5 Restriction: ScaleRMax >= ScaleRMin

        [Category("检测参数"), DefaultValue(1), DisplayName("X最小比例")]
        public double ScaleCMin { get; set; }  //number → (real) Minimum scale of the model in column direction. Default value: 1.0 Suggested values: 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 Restriction: ScaleCMin > 0

        [Category("检测参数"), DefaultValue(1), DisplayName("X最大比例")]
        public double ScaleCMax { get; set; }  //number → (real) Maximum scale of the model in column direction. Default value: 1.0 Suggested values: 1.0, 1.1, 1.2, 1.3, 1.4, 1.5 Restriction: ScaleCMax >= ScaleCMin

        [Category("检测参数"), DefaultValue(0.90), DisplayName("最小匹配度")]
        public double MinScore  { get; set; }  //real → (real) Minumum score of the instances of the model to be found. Default value: 0.5 Suggested values: 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 Typical range of values: 0 ≤ MinScore ≤ 1 Minimum increment: 0.01 Recommended increment: 0.05

        [Category("检测参数"), DefaultValue(100), DisplayName("最大检验/匹配个数")]
        public int NumMatches  { get; set; }  //integer → (integer) Number of instances of the model to be found(or 0 for all matches). Default value: 1 Suggested values: 0, 1, 2, 3, 4, 5, 10, 20

        [Category("检测参数"), DefaultValue(0.70), DisplayName("最大被覆盖范围比例")]
        public double MaxOverlap  { get; set; }  //real → (real) Maximum overlap of the instances of the model to be found. Default value: 1.0 Suggested values: 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 Typical range of values: 0 ≤ MaxOverlap ≤ 1 Minimum increment: 0.01 Recommended increment: 0.05

        [Category("检测参数"), DefaultValue(0), DisplayName("金字塔级别")]
        public int NumLevels  { get; set; }  //integer(-array) → (integer) Number of pyramid levels used in the matching. Default value: 0 List of values: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

        [Category("检测参数"), DefaultValue(0.4), DisplayName("搜索启发式值")]
        public double Greediness  { get; set; }  //real → (real) “Greediness” of the search heuristic(0: safe but slow; 1: fast but matches may be missed). Default value: 0.9 Suggested values: 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 Typical range of values: 0 ≤ Greediness ≤ 1 Minimum increment: 0.01 Recommended increment: 0.05

        [Category("检测参数"), DefaultValue(new string[] { " image_rectified", "vector_field", "deformed_contours" }), DisplayName("检验结果类型")]
        public string[] ResultType { get; set; }  //string-array → (string) Switch for requested iconic result. Default value: []  List of values: [], 'deformed_contours', 'image_rectified', 'vector_field'

        public class Parameters
        {
            public string ParamName { get; set; }   //string-array → (string) The general parameter names. Default value: [] List of values: [], 'angle_step', 'deformation_smoothness', 'expand_border', 'expand_border_bottom', 'expand_border_left', 'expand_border_right', 'expand_border_top', 'scale_c_step', 'scale_r_step', 'subpixel'
            public int ParamValue { get; set; }  //integer-array → (integer / real / string) Values of the general parameters.Default value: []         List of values: [], 'least_squares', 'least_squares_high', 'least_squares_very_high', 'none'

            public Parameters()
            {
                ParamName = "deformation_smoothness";
                ParamValue = 25;
            }

            public Parameters(string name, int value) //public static implicit operator Parameters(string name, string value)
            {
                ParamName = name;
                ParamValue = value;
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter)), Category("检测参数"), DefaultValue(new string[] { " deformation_smoothness", "expand_border", "subpixel" }), DisplayName("参数名例表")]
        public Parameters[] ArryParameters { get; set; }

        #endregion

        #region>>>>>>>>> 输出
        [Category("结果"), DefaultValue(3), DisplayName("最终结果")]
        public bool FinallyResult { get; set; }

        [Category("结果"), DefaultValue(3), DisplayName("缺陷数量")]
        public int CountOfDefect { get; set; }

        [Editor(typeof(PropertyGridRIOCollectorUI), typeof(UITypeEditor)), Category("结果"), DefaultValue(3), DisplayName("检测区域及结果")]
        public CheckResultDef[] CheckResult { get; set; }

        #endregion

        #region>>>>>>>>>显示
        bool _bShowProcessWindow = false;
        [Category("性能/显示"), DefaultValue(false), DisplayName("显示图像处理窗口")]
        public bool bShowProcessWindow
        {
           get
            {
                return _bShowProcessWindow;
            }

            set
           {
                _bShowProcessWindow = value;
                if(_bShowProcessWindow)
                {
                    if (ImageTempWnd != null)
                        ImageTempWnd.Show();

                    if (ImageProcessWnd != null)
                        ImageProcessWnd.Show();
                }
                else
                {
                    if (ImageTempWnd != null)
                        ImageTempWnd.Hide();

                    if (ImageProcessWnd != null)
                        ImageProcessWnd.Hide();
                }
            }

        }

        [Category("性能/显示"), DefaultValue(false), DisplayName("启用多线程")]
        public bool bEnableMuitlThread { get; set; }
        #endregion

        HalconForm ImageTempWnd = new HalconForm("模版窗口");
        HalconForm ImageProcessWnd = new HalconForm("图像处理窗口");

        public DeformableInspector()
        {
            DateTime dateTime = DateTime.Now;
            Name = dateTime.ToString("yyyyMMddHHmmss") + $".{dateTime.Millisecond.ToString("D03")}缺陷检查方法";

            bShowProcessWindow = _bShowProcessWindow;
            bEnableMuitlThread = false;
            hv_Smoothness = 25;

            AreaMin = 30;
            Canny_Alpha = 20;
            absThreshold = 30;
            varThreshold = 8;
            ThresholdMin = -1;
            ThresholdMax = -1;

            AngleStart_T = -10;
            AngleExtent_T = 20;
            AngleStep_T = 0.01;
            ScaleRMin_T = 0.98;
            ScaleRMax_T = 1.02;
            ScaleRStep_T = 0.01;
            ScaleCMin_T = 0.98;
            ScaleCMax_T = 1.02;
            ScaleCStep_T = 0.01;

            AngleStart = -10;
            AngleExtent = 20;
            ScaleRMin = 1;
            ScaleRMax = 1;
            ScaleCMin = 1;
            ScaleCMax = 1;
            MinScore = 0.4;
            NumMatches = 1;
            MaxOverlap = 0.8;
            NumLevels = 0;
            Greediness = 0.75;

            FinallyResult = false;

            CheckResult = new CheckResultDef[1];
            CheckResult[0] = new CheckResultDef();

            ResultType = new string[3] { "image_rectified", "vector_field", "deformed_contours" };
            ArryParameters = new Parameters[3] { new Parameters("deformation_smoothness",  hv_Smoothness),
                                                                           new Parameters("expand_border", 0),
                                                                           new Parameters("subpixel", 1),
                                                                          };
        }

        public void Dispose()
        {
            try
            {
                if (hv_ModelID != null)
                    HOperatorSet.ClearDeformableModel(hv_ModelID);

                if (hv_VariationModelID != null)
                    HOperatorSet.ClearVariationModel(hv_VariationModelID);

                if (objImage_Temp != null)
                    objImage_Temp.Dispose();

                if (objImage_Check != null)
                    objImage_Check.Dispose();

                ImageTempWnd.Dispose();
                ImageProcessWnd.Dispose();
            }
            catch (Exception ex)
            {

            }
        }

        ~DeformableInspector()
        {

        }


        private void gen_warped_mesh_region(HObject ho_VectorField, out HObject ho_WarpedMesh,   HTuple hv_Step)
        {
            // Stack for temporary objects 
            HObject[] OTemp = new HObject[20];

            // Local iconic variables 

            HObject ho_ObjectSelected = null, ho_DRow = null;
            HObject ho_DCol = null, ho_Region = null;

            // Local control variables 

            HTuple hv_Number = null, hv_Index = null, hv_Width = new HTuple();
            HTuple hv_Height = new HTuple(), hv_ContR = new HTuple();
            HTuple hv_Col1 = new HTuple(), hv_Row1 = new HTuple();
            HTuple hv_GrayRow = new HTuple(), hv_GrayCol = new HTuple();
            HTuple hv_ContC = new HTuple();

            // Initialize local and output iconic variables 
            HOperatorSet.GenEmptyObj(out ho_WarpedMesh);
            HOperatorSet.GenEmptyObj(out ho_ObjectSelected);
            HOperatorSet.GenEmptyObj(out ho_DRow);
            HOperatorSet.GenEmptyObj(out ho_DCol);
            HOperatorSet.GenEmptyObj(out ho_Region);

            try
            {
                ho_WarpedMesh.Dispose();
                HOperatorSet.GenEmptyObj(out ho_WarpedMesh);
                HOperatorSet.CountObj(ho_VectorField, out hv_Number);
                HTuple end_val2 = hv_Number;
                HTuple step_val2 = 1;
                for (hv_Index = 1; hv_Index.Continue(end_val2, step_val2); hv_Index = hv_Index.TupleAdd(step_val2))
                {
                    //For visualization we use the returned vector field
                    //and generate a grid of the deformation in the search image.
                    ho_ObjectSelected.Dispose();
                    HOperatorSet.SelectObj(ho_VectorField, out ho_ObjectSelected, hv_Index);
                    ho_DRow.Dispose(); ho_DCol.Dispose();
                    HOperatorSet.VectorFieldToReal(ho_ObjectSelected, out ho_DRow, out ho_DCol);
                    HOperatorSet.GetImageSize(ho_VectorField, out hv_Width, out hv_Height);
                    HTuple end_val8 = (hv_Height.TupleSelect(0)) - 1;
                    HTuple step_val8 = hv_Step;
                    for (hv_ContR = 0.5; hv_ContR.Continue(end_val8, step_val8); hv_ContR = hv_ContR.TupleAdd(step_val8))
                    {
                        hv_Col1 = HTuple.TupleGenSequence(0.5, (hv_Width.TupleSelect(0)) - 1, 1);
                        HOperatorSet.TupleGenConst((hv_Width.TupleSelect(0)) - 1, hv_ContR, out hv_Row1);
                        HOperatorSet.GetGrayvalInterpolated(ho_DRow, hv_Row1, hv_Col1, "bilinear", out hv_GrayRow);
                        HOperatorSet.GetGrayvalInterpolated(ho_DCol, hv_Row1, hv_Col1, "bilinear",  out hv_GrayCol);
                        ho_Region.Dispose();
                        HOperatorSet.GenRegionPoints(out ho_Region, hv_GrayRow, hv_GrayCol);
                        {
                            HObject ExpTmpOutVar_0;
                            HOperatorSet.ConcatObj(ho_WarpedMesh, ho_Region, out ExpTmpOutVar_0);
                            ho_WarpedMesh.Dispose();
                            ho_WarpedMesh = ExpTmpOutVar_0;
                        }
                    }
                    HTuple end_val16 = (hv_Width.TupleSelect(0)) - 1;
                    HTuple step_val16 = hv_Step;
                    for (hv_ContC = 0.5; hv_ContC.Continue(end_val16, step_val16); hv_ContC = hv_ContC.TupleAdd(step_val16))
                    {
                        hv_Row1 = HTuple.TupleGenSequence(0.5, (hv_Height.TupleSelect(0)) - 1, 1);
                        HOperatorSet.TupleGenConst((hv_Height.TupleSelect(0)) - 1, hv_ContC, out hv_Col1);
                        HOperatorSet.GetGrayvalInterpolated(ho_DRow, hv_Row1, hv_Col1, "bilinear",  out hv_GrayRow);
                        HOperatorSet.GetGrayvalInterpolated(ho_DCol, hv_Row1, hv_Col1, "bilinear",    out hv_GrayCol);
                        ho_Region.Dispose();
                        HOperatorSet.GenRegionPoints(out ho_Region, hv_GrayRow, hv_GrayCol);
                        {
                            HObject ExpTmpOutVar_0;
                            HOperatorSet.ConcatObj(ho_WarpedMesh, ho_Region, out ExpTmpOutVar_0);
                            ho_WarpedMesh.Dispose();
                            ho_WarpedMesh = ExpTmpOutVar_0;
                        }
                    }
                }
                ho_ObjectSelected.Dispose();
                ho_DRow.Dispose();
                ho_DCol.Dispose();
                ho_Region.Dispose();

                return;
            }
            catch (HalconException HDevExpDefaultException)
            {
                ho_ObjectSelected.Dispose();
                ho_DRow.Dispose();
                ho_DCol.Dispose();
                ho_Region.Dispose();

                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Init(string tempImageFileName, bool reset = false)
        {
            try
            {
                HObject objImage;
                if (tempImageFileName == null || tempImageFileName.Length <= 0)
                {
                    if (!File.Exists(tempFileName))
                        throw new HalconException($"DeformableInspector ImageProcess()处理异常，当前文件不存在\r\n{tempFileName}");

                    HOperatorSet.ReadImage(out objImage, tempFileName);
                }
                else
                {
                    if (!File.Exists(tempImageFileName))
                        throw new HalconException($"DeformableInspector ImageProcess()处理异常，当前文件不存在\r\n{tempImageFileName}");

                    tempFileName = tempImageFileName;
                    HOperatorSet.ReadImage(out objImage, tempImageFileName);
                }

                return Init(new HImage(objImage), reset);

            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Init(HImage vTempImage, bool reset = false)
        {
            try
            {
                if (TemplateIsCreated && !reset)
                    return true;

                // Local iconic variables 

                HObject ho_ModelImage, ho_Image, ho_EdgeAmplitude;
                HObject ho_ModelContours, ho_ContoursAffinTrans, ho_ImageRectified = null;
                HObject ho_VectorField = null, ho_DeformedContours = null, ho_MeshRegion = null;
                HObject ho_EdgeRegion = null, ho_RegionDilation = null, ho_RegionIntersection = null;
                HObject ho_Region = null, ho_ConnectedRegions = null, ho_SelectedRegions = null;
                HObject ho_ContEllipse = null;

                // Local control variables 
                HTuple hv_Row = null, hv_Column = null, hv_Width = null;
                HTuple hv_Height = null;
                HTuple hv_Area = null, hv_HomMat2DIdentity = null;
                HTuple hv_HomMat2DTranslate = null, hv_NumImages = null;
                HTuple hv_Index = null, hv_S1 = new HTuple(), hv_Score = new HTuple();
                HTuple hv_S2 = new HTuple(), hv_Time = new HTuple();
                HTuple[] hv_Found = null; ;
                HTuple hv_Number = new HTuple(), hv_Row1 = new HTuple();
                HTuple hv_Column1 = new HTuple(), hv_Ra = new HTuple();
                HTuple hv_Rb = new HTuple(), hv_Phi = new HTuple(), hv_Ones = new HTuple();
                HTuple hv_PointOrder = new HTuple(), hv_Idx = new HTuple();
                // Initialize local and output iconic variables 
                HOperatorSet.GenEmptyObj(out ho_ModelImage);
                HOperatorSet.GenEmptyObj(out ho_Image);
                HOperatorSet.GenEmptyObj(out ho_EdgeAmplitude);
                HOperatorSet.GenEmptyObj(out ho_ModelContours);
                HOperatorSet.GenEmptyObj(out ho_ContoursAffinTrans);
                HOperatorSet.GenEmptyObj(out ho_ImageRectified);
                HOperatorSet.GenEmptyObj(out ho_VectorField);
                HOperatorSet.GenEmptyObj(out ho_DeformedContours);
                HOperatorSet.GenEmptyObj(out ho_MeshRegion);
                HOperatorSet.GenEmptyObj(out ho_EdgeRegion);
                HOperatorSet.GenEmptyObj(out ho_RegionDilation);
                HOperatorSet.GenEmptyObj(out ho_RegionIntersection);
                HOperatorSet.GenEmptyObj(out ho_Region);
                HOperatorSet.GenEmptyObj(out ho_ConnectedRegions);
                HOperatorSet.GenEmptyObj(out ho_SelectedRegions);
                HOperatorSet.GenEmptyObj(out ho_ContEllipse);

                HOperatorSet.CopyImage(vTempImage, out objImage_Temp);
                HOperatorSet.CopyImage(vTempImage, out ho_ModelImage);
                HTuple width, height;
                HOperatorSet.GetImageSize(ho_ModelImage, out width, out height);
                ImageTempWnd.ShowImage(ho_ModelImage, width, height);
                HOperatorSet.GetImageSize(objImage_Temp, out width, out height);

                //
                //Create variation model
                ho_EdgeAmplitude.Dispose();
                HObject ho_EdgeDir;
                /*                HOperatorSet.EdgesSubPix(ho_ModelImage, out ho_EdgeAmplitude, "canny", 0.5, 20, 40);*/
                HOperatorSet.EdgesImage(ho_ModelImage, out ho_EdgeAmplitude, out ho_EdgeDir, "canny", Canny_Alpha, "none", ThresholdMin, ThresholdMax);
                /*                HOperatorSet.SobelAmp(ho_ModelImage, out ho_EdgeAmplitude, "sum_abs", 1);*/
                HOperatorSet.GetImageSize(ho_ModelImage, out width, out height);
                HOperatorSet.CreateVariationModel(width, height, "byte", "direct", out hv_VariationModelID);
                HOperatorSet.PrepareDirectVariationModel(ho_ModelImage, ho_EdgeAmplitude, hv_VariationModelID, absThreshold, varThreshold);
                //
                //Create locally deformable model
                HOperatorSet.CreateLocalDeformableModel(ho_ModelImage, "auto",
                                                                                        (new HTuple(AngleStart_T)).TupleRad(), (new HTuple(AngleExtent_T)).TupleRad(), AngleStep_T,
                                                                                        ScaleRMin_T, ScaleRMax_T, ScaleRStep_T,
                                                                                        ScaleCMin_T, ScaleCMax_T, ScaleCStep_T,
                                                                                        "none", "use_polarity", "auto", "auto",
                                                                                        new HTuple(), new HTuple(), out hv_ModelID);
                ho_ModelContours.Dispose();
                HOperatorSet.GetDeformableModelContours(out ho_ModelContours, hv_ModelID, 1);
                HOperatorSet.AreaCenter(ho_ModelImage, out hv_Area, out hv_Row, out hv_Column);
                HOperatorSet.HomMat2dIdentity(out hv_HomMat2DIdentity);
                HOperatorSet.HomMat2dTranslate(hv_HomMat2DIdentity, hv_Row, hv_Column, out hv_HomMat2DTranslate);
                ho_ContoursAffinTrans.Dispose();
                HOperatorSet.AffineTransContourXld(ho_ModelContours, out ho_ContoursAffinTrans, hv_HomMat2DTranslate);

                HOperatorSet.SetLineWidth(ImageTempWnd.hWindow, 2);
                HOperatorSet.SetColor(ImageTempWnd.hWindow, "yellow");
                HOperatorSet.DispObj(ho_ModelImage, ImageTempWnd.hWindow);
                HOperatorSet.DispObj(ho_ContoursAffinTrans, ImageTempWnd.hWindow);

                HalconGeneral.HD_DisplayMessage(ImageTempWnd.hWindow, "Model image and contours", "image", 12, 12, "black", "true");
                TemplateIsCreated = true;
                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ImageProcess(string tempImageFileName)
        {
            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"DeformableInspector 没有建立模版，请调用Init");

                if (!File.Exists(tempImageFileName))
                    throw new HalconException($"DeformableInspector ImageProcess()处理异常，当前文件不存在\r\n{tempImageFileName}");

                HObject objImage;
                HOperatorSet.ReadImage(out objImage, tempImageFileName);
                bool result = ImageProcess(new HImage(objImage));
                return result;
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ImageProcess(HImage imageObj)
        {
            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"DeformableInspector 没有建立模版，请调用Init");

                if (!HalconGeneral.IsImageObj(imageObj))
                    throw new HalconException($"DeformableInspector ImageProcess 图像格式不对");

                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    CheckResult[nIndexAOI].Result = -999;
                    CheckResult[nIndexAOI].ResultStatus = false;
                }

                if (CheckResult.Length > 1 && bEnableMuitlThread)
                {
                    return ImageProcess_MuitlThred(imageObj);
                }
                else
                    return ImageProcess_SingleThread(imageObj);
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ImageProcess_SingleThread(HImage imageObj)
        {
            FinallyResult = true;
            // Local iconic variables 

            HObject ho_ModelImage = null;
            HObject ho_Image = null;
            HObject ho_EdgeAmplitude = null;
            HObject ho_ModelContours = null, ho_ContoursAffinTrans = null, ho_ImageRectified = null;
            HObject ho_VectorField = null, ho_DeformedContours = null, ho_MeshRegion = null;
            HObject ho_EdgeRegion = null, ho_RegionDilation = null, ho_RegionIntersection = null;
            HObject ho_Region = null, ho_ConnectedRegions = null, ho_SelectedRegions = null;
            HObject ho_ContEllipse = null;

            // Local control variables 

            HTuple hv_Row = null, hv_Column = null;
            HTuple hv_Area = null;
            HTuple hv_NumImages = null;
            HTuple hv_S1 = new HTuple(), hv_Score = new HTuple();
            HTuple hv_S2 = new HTuple(), hv_Time = new HTuple();
            HTuple[] hv_Found = null; ;
            HTuple hv_Number = new HTuple(), hv_Row1 = new HTuple();
            HTuple hv_Column1 = new HTuple(), hv_Ra = new HTuple();
            HTuple hv_Rb = new HTuple(), hv_Phi = new HTuple(), hv_Ones = new HTuple();
            HTuple hv_PointOrder = new HTuple(), hv_Idx = new HTuple();

            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"ShapeModelInspector 没有建立模版，请调用Init");

                if (!HalconGeneral.IsImageObj(imageObj))
                    throw new HalconException($"ShapeModelInspector ImageProcess 图像格式不对");

                HOperatorSet.CopyImage(imageObj, out objImage_Check);

                // Initialize local and output iconic variables 
                HOperatorSet.GenEmptyObj(out ho_ModelImage);
                HOperatorSet.GenEmptyObj(out ho_Image);
                HOperatorSet.GenEmptyObj(out ho_EdgeAmplitude);
                HOperatorSet.GenEmptyObj(out ho_ModelContours);
                HOperatorSet.GenEmptyObj(out ho_ContoursAffinTrans);
                HOperatorSet.GenEmptyObj(out ho_ImageRectified);
                HOperatorSet.GenEmptyObj(out ho_VectorField);
                HOperatorSet.GenEmptyObj(out ho_DeformedContours);
                HOperatorSet.GenEmptyObj(out ho_MeshRegion);
                HOperatorSet.GenEmptyObj(out ho_EdgeRegion);
                HOperatorSet.GenEmptyObj(out ho_RegionDilation);
                HOperatorSet.GenEmptyObj(out ho_RegionIntersection);
                HOperatorSet.GenEmptyObj(out ho_Region);
                HOperatorSet.GenEmptyObj(out ho_ConnectedRegions);
                HOperatorSet.GenEmptyObj(out ho_SelectedRegions);
                HOperatorSet.GenEmptyObj(out ho_ContEllipse);

                hv_NumImages = 7;
                HTuple end_val37 = hv_NumImages;
                HTuple step_val37 = 1;

                HOperatorSet.ClearWindow(ImageProcessWnd.hWindow);

                //开始处理图片
                CountOfDefect = 999;
                HTuple width, height;
                HOperatorSet.GetImageSize(imageObj, out width, out height);
                ImageProcessWnd.ShowImage(imageObj, width, height);

                HOperatorSet.CopyImage(imageObj, out ho_Image);
                HOperatorSet.DispObj(ho_Image, ImageProcessWnd.hWindow);

                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    HOperatorSet.CountSeconds(out hv_S1);

                    if (CheckResult[nIndexAOI].CheckedRIO.fRow <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fCol <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fLen1 <= 0.01 ||
                      CheckResult[nIndexAOI].CheckedRIO.fLen2 <= 0.01)
                    {
                        CheckResult[nIndexAOI].CheckedRIO.fRow = (double)height / 2;
                        CheckResult[nIndexAOI].CheckedRIO.fCol = (double)width / 2;
                        CheckResult[nIndexAOI].CheckedRIO.fPhi = 0;
                        CheckResult[nIndexAOI].CheckedRIO.fLen1 = (double)width / 2 - 2;
                        CheckResult[nIndexAOI].CheckedRIO.fLen2 = (double)height / 2 - 2;
                    }

                    HObject Rectangle1, TemplateBottleRegion, ImageReduce;
                    HOperatorSet.GenRectangle2(out Rectangle1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fRow, CheckResult[nIndexAOI].CheckedRIO.fCol,
                                                                    (new HTuple(CheckResult[nIndexAOI].CheckedRIO.fPhi)).TupleRad(), CheckResult[nIndexAOI].CheckedRIO.fLen1,
                                                                    CheckResult[nIndexAOI].CheckedRIO.fLen2);

                    HOperatorSet.Union1(Rectangle1, out TemplateBottleRegion);
                    HOperatorSet.ReduceDomain(ho_Image, TemplateBottleRegion, out ImageReduce);
                    HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                    ho_ImageRectified.Dispose(); ho_VectorField.Dispose(); ho_DeformedContours.Dispose();

                    HTuple tupleResultType = new HTuple[0];
                    HTuple ParamName = new HTuple[0];
                    HTuple ParamValue = new HTuple[0];

                    tupleResultType = new HTuple(ResultType);
                    for (int nIndex = 0; nIndex < ArryParameters.Length; nIndex++)
                    {
                        ParamName = ParamName.TupleConcat(new HTuple(ArryParameters[nIndex].ParamName));
                        ParamValue = ParamValue.TupleConcat(new HTuple(ArryParameters[nIndex].ParamValue));
                    }

                    NumMatches = 1;
                    HOperatorSet.FindLocalDeformableModel(ImageReduce, out ho_ImageRectified, out ho_VectorField, out ho_DeformedContours, hv_ModelID,
                                                                                        (new HTuple(AngleStart)).TupleRad(), (new HTuple(AngleExtent)).TupleRad(),
                                                                                        ScaleRMin, ScaleRMax,
                                                                                        ScaleCMin, ScaleCMax,
                                                                                        MinScore,
                                                                                        NumMatches,
                                                                                        MaxOverlap,
                                                                                        NumLevels,
                                                                                        Greediness,
                                                                                        tupleResultType,
                                                                                        ParamName,
                                                                                        ParamValue,
                                                                                        out hv_Score, out hv_Row, out hv_Column);
                    HOperatorSet.CountSeconds(out hv_S2);
                    hv_Time = hv_S2 - hv_S1;
                    hv_Found = new HTuple[hv_Score.TupleLength()];

                    if (hv_Score.TupleLength() <= 0)
                    {
                        FinallyResult = false;
                        CheckResult[nIndexAOI].Result = -998;
                        CheckResult[nIndexAOI].ResultStatus = false;
                        HOperatorSet.SetColor(ImageProcessWnd.hWindow, "red");
                        HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                        HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part NG(Nothing found)", "image",
                                                                                 CheckResult[nIndexAOI].CheckedRIO.fRow, // - CheckResult[nIndexAOI].CheckedRIO.fLen1 + 40,
                                                                                 CheckResult[nIndexAOI].CheckedRIO.fCol, // - CheckResult[nIndexAOI].CheckedRIO.fLen2 + 40,
                                                                                 "red", "true");
                        continue;
                    }

                    ho_MeshRegion.Dispose();
                    gen_warped_mesh_region(ho_VectorField, out ho_MeshRegion, hv_Smoothness);
                    ho_EdgeRegion.Dispose();
                    HOperatorSet.GenRegionContourXld(ho_DeformedContours, out ho_EdgeRegion, "margin");
                    ho_RegionDilation.Dispose();
                    HOperatorSet.DilationCircle(ho_EdgeRegion, out ho_RegionDilation, 2 * hv_Smoothness);
                    ho_RegionIntersection.Dispose();
                    HOperatorSet.Intersection(ho_RegionDilation, ho_MeshRegion, out ho_RegionIntersection);

                    HOperatorSet.SetLineWidth(ImageProcessWnd.hWindow, 1);
                    HOperatorSet.SetColor(ImageProcessWnd.hWindow, "yellow");
                    /*                    HOperatorSet.DispObj(ho_RegionIntersection, ImageProcessWnd.hWindow);*/

                    HOperatorSet.SetLineWidth(ImageProcessWnd.hWindow, 2);
                    HOperatorSet.SetColor(ImageProcessWnd.hWindow, "green"); //HDevWindowStack.GetActive()
                                                                             /*                    HOperatorSet.DispObj(ho_DeformedContours, ImageProcessWnd.hWindow);*/

                    HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Match found", "image", hv_Row, hv_Column, "black", "true");
                    HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "MTime: " + hv_Time.TupleString("1.2f") + "s", "image", hv_Row + 100, hv_Column, "black", "true");
                    HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Score: " + hv_Score.TupleString(".2f"), "image", hv_Row + 200, hv_Column, "black", "true");
                    /*                    HOperatorSet.DispObj(ho_ImageRectified, ImageTempWnd.hWindow);*/

                    ho_Region.Dispose();
                    HOperatorSet.CompareVariationModel(ho_ImageRectified, out ho_Region, hv_VariationModelID);
                    ho_ConnectedRegions.Dispose();
                    HOperatorSet.Connection(ho_Region, out ho_ConnectedRegions);
                    ho_SelectedRegions.Dispose();
                    HOperatorSet.SelectShape(ho_ConnectedRegions, out ho_SelectedRegions, "area", "and", AreaMin, 99999);
                    HOperatorSet.CountObj(ho_SelectedRegions, out hv_Number);

                    CountOfDefect = hv_Number;
                    if (hv_Number <= 0)
                    {
                        CheckResult[nIndexAOI].Result = 0;
                        CheckResult[nIndexAOI].ResultStatus = true;
                        if (CheckResult[nIndexAOI].MatchingResult == null)
                            CheckResult[nIndexAOI].MatchingResult = new MATCHINGRESULT_DES();

                        CheckResult[nIndexAOI].MatchingResult.TupleRow = -1;
                        CheckResult[nIndexAOI].MatchingResult.TupleCol = -1;
                        CheckResult[nIndexAOI].MatchingResult.TupleAngle = -1;

                        HOperatorSet.GetImageSize(objImage_Temp, out width, out height);
                        HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                        HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part OK", "image", hv_Row - height / 2, hv_Column - width / 2, "forest green", "true");
                        continue;
                    }

                    HOperatorSet.SetColor(ImageProcessWnd.hWindow, "red");
                    HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                    HOperatorSet.GetImageSize(objImage_Temp, out width, out height);
                    HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part NG", "image", hv_Row - height / 2, hv_Column - width / 2, "red", "true");
                    HOperatorSet.AreaCenter(ho_SelectedRegions, out hv_Area, out hv_Row1, out hv_Column1);

                    //============================
                    HOperatorSet.EllipticAxis(ho_SelectedRegions, out hv_Ra, out hv_Rb, out hv_Phi);

                    HOperatorSet.TupleGenConst(hv_Number, 1, out hv_Ones);
                    hv_PointOrder = new HTuple();
                    HTuple end_val73 = hv_Number - 1;
                    HTuple step_val73 = 1;
                    for (hv_Idx = 0; hv_Idx.Continue(end_val73, step_val73); hv_Idx = hv_Idx.TupleAdd(step_val73))
                    {
                        hv_PointOrder = hv_PointOrder.TupleConcat("positive");
                    }
                    ho_ContEllipse.Dispose();

                    FinallyResult = false;
                    CheckResult[nIndexAOI].Result = CountOfDefect;
                    CheckResult[nIndexAOI].ResultStatus = false;
                    if (CheckResult[nIndexAOI].MatchingResult == null)
                        CheckResult[nIndexAOI].MatchingResult = new MATCHINGRESULT_DES();

                    CheckResult[nIndexAOI].MatchingResult.TupleRow = hv_Row1;
                    CheckResult[nIndexAOI].MatchingResult.TupleCol = hv_Column1;
                    CheckResult[nIndexAOI].MatchingResult.TupleAngle = hv_Phi;

                    HOperatorSet.GenEllipseContourXld(out ho_ContEllipse, hv_Row1, hv_Column1, hv_Phi, hv_Ra + 10, hv_Rb + 10, 0 * hv_Ones, 6.28318 * hv_Ones, hv_PointOrder, 1.5);
                    HOperatorSet.SetColor(ImageTempWnd.hWindow, "red");

                    //重新定义缺陷坐标
                    width = 0;
                    height = 0;
                    HOperatorSet.GetImageSize(objImage_Temp, out width, out height);
                    HTuple HomMat2DIdentity, HomMat2DScale, HomMat2DRotate, HomMat2DTranslate;
                    HOperatorSet.HomMat2dIdentity(out HomMat2DIdentity);
                    HOperatorSet.HomMat2dScale(HomMat2DIdentity, 1, 1, 0, 0, out HomMat2DScale);
                    HOperatorSet.HomMat2dRotate(HomMat2DScale, new HTuple(0).TupleRad(), 0, 0, out HomMat2DRotate);
                    HOperatorSet.HomMat2dTranslate(HomMat2DRotate, hv_Row - height / 2, hv_Column - width / 2, out HomMat2DTranslate);
                    HOperatorSet.AffineTransContourXld(ho_ContEllipse, out ho_ContEllipse, HomMat2DTranslate);
                    HOperatorSet.SetColor(ImageProcessWnd.hWindow, "red");
                    HOperatorSet.DispObj(ho_ContEllipse, ImageProcessWnd.hWindow);

                    HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part not OK!", "image", 12, 12, "red", "true");

                    //
                    //Clean up
                    ENDCHECK:
                    ho_ModelImage.Dispose();

                    ho_EdgeAmplitude.Dispose();
                    ho_ModelContours.Dispose();
                    ho_ContoursAffinTrans.Dispose();
                    ho_ImageRectified.Dispose();
                    ho_VectorField.Dispose();
                    ho_DeformedContours.Dispose();
                    ho_MeshRegion.Dispose();
                    ho_EdgeRegion.Dispose();
                    ho_RegionDilation.Dispose();
                    ho_RegionIntersection.Dispose();
                    ho_Region.Dispose();
                    ho_ConnectedRegions.Dispose();
                    ho_SelectedRegions.Dispose();
                    ho_ContEllipse.Dispose();
                    GC.Collect();
                }

                ho_Image.Dispose();
                GC.Collect();

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                FinallyResult = false;
                ho_ModelImage.Dispose();
                ho_Image.Dispose();
                ho_EdgeAmplitude.Dispose();
                ho_ModelContours.Dispose();
                ho_ContoursAffinTrans.Dispose();
                ho_ImageRectified.Dispose();
                ho_VectorField.Dispose();
                ho_DeformedContours.Dispose();
                ho_MeshRegion.Dispose();
                ho_EdgeRegion.Dispose();
                ho_RegionDilation.Dispose();
                ho_RegionIntersection.Dispose();
                ho_Region.Dispose();
                ho_ConnectedRegions.Dispose();
                ho_SelectedRegions.Dispose();
                ho_ContEllipse.Dispose();

                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ImageProcess_RIO(ref CheckResultDef rio)
        {
            // Local iconic variables 

            HObject ho_ModelImage = null;
            HObject ho_Image = null;
            HObject ho_EdgeAmplitude = null;
            HObject ho_ModelContours = null, ho_ContoursAffinTrans = null, ho_ImageRectified = null;
            HObject ho_VectorField = null, ho_DeformedContours = null, ho_MeshRegion = null;
            HObject ho_EdgeRegion = null, ho_RegionDilation = null, ho_RegionIntersection = null;
            HObject ho_Region = null, ho_ConnectedRegions = null, ho_SelectedRegions = null;
            HObject ho_ContEllipse = null;

            // Local control variables 
            HTuple hv_Row = null, hv_Column = null;
            HTuple hv_Area = null;
            HTuple hv_NumImages = null;
            HTuple hv_S1 = new HTuple(), hv_Score = new HTuple();
            HTuple hv_S2 = new HTuple(), hv_Time = new HTuple();
            HTuple[] hv_Found = null; ;
            HTuple hv_Number = new HTuple(), hv_Row1 = new HTuple();
            HTuple hv_Column1 = new HTuple(), hv_Ra = new HTuple();
            HTuple hv_Rb = new HTuple(), hv_Phi = new HTuple(), hv_Ones = new HTuple();
            HTuple hv_PointOrder = new HTuple(), hv_Idx = new HTuple();

            try
            {
                if (!TemplateIsCreated)
                    throw new HalconException($"ShapeModelInspector 没有建立模版，请调用Init");

                // Initialize local and output iconic variables 
                HOperatorSet.GenEmptyObj(out ho_ModelImage);
                HOperatorSet.GenEmptyObj(out ho_Image);
                HOperatorSet.GenEmptyObj(out ho_EdgeAmplitude);
                HOperatorSet.GenEmptyObj(out ho_ModelContours);
                HOperatorSet.GenEmptyObj(out ho_ContoursAffinTrans);
                HOperatorSet.GenEmptyObj(out ho_ImageRectified);
                HOperatorSet.GenEmptyObj(out ho_VectorField);
                HOperatorSet.GenEmptyObj(out ho_DeformedContours);
                HOperatorSet.GenEmptyObj(out ho_MeshRegion);
                HOperatorSet.GenEmptyObj(out ho_EdgeRegion);
                HOperatorSet.GenEmptyObj(out ho_RegionDilation);
                HOperatorSet.GenEmptyObj(out ho_RegionIntersection);
                HOperatorSet.GenEmptyObj(out ho_Region);
                HOperatorSet.GenEmptyObj(out ho_ConnectedRegions);
                HOperatorSet.GenEmptyObj(out ho_SelectedRegions);
                HOperatorSet.GenEmptyObj(out ho_ContEllipse);

                hv_NumImages = 7;
                HTuple end_val37 = hv_NumImages;
                HTuple step_val37 = 1;

                //开始处理图片
                CountOfDefect = 999;
                HTuple width, height;
                HOperatorSet.GetImageSize(objImage_Check, out width, out height);

                HOperatorSet.CountSeconds(out hv_S1);
                if (rio.CheckedRIO.fRow <= 0.01 ||
                  rio.CheckedRIO.fCol <= 0.01 ||
                  rio.CheckedRIO.fLen1 <= 0.01 ||
                  rio.CheckedRIO.fLen2 <= 0.01)
                {
                    rio.CheckedRIO.fRow = (double)height / 2;
                    rio.CheckedRIO.fCol = (double)width / 2;
                    rio.CheckedRIO.fPhi = 0;
                    rio.CheckedRIO.fLen1 = (double)width / 2 - 2;
                    rio.CheckedRIO.fLen2 = (double)height / 2 - 2;
                }

                HObject Rectangle1, TemplateBottleRegion, ImageReduce;
                HOperatorSet.GenRectangle2(out Rectangle1,
                                                                rio.CheckedRIO.fRow, rio.CheckedRIO.fCol,
                                                                (new HTuple(rio.CheckedRIO.fPhi)).TupleRad(), rio.CheckedRIO.fLen1,
                                                                rio.CheckedRIO.fLen2);

                HOperatorSet.Union1(Rectangle1, out TemplateBottleRegion);
                HOperatorSet.ReduceDomain(objImage_Check, TemplateBottleRegion, out ImageReduce);
                HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                ho_ImageRectified.Dispose(); ho_VectorField.Dispose(); ho_DeformedContours.Dispose();

                HTuple tupleResultType = new HTuple[0];
                HTuple ParamName = new HTuple[0];
                HTuple ParamValue = new HTuple[0];

                tupleResultType = new HTuple(ResultType);
                for (int nIndex = 0; nIndex < ArryParameters.Length; nIndex++)
                {
                    ParamName = ParamName.TupleConcat(new HTuple(ArryParameters[nIndex].ParamName));
                    ParamValue = ParamValue.TupleConcat(new HTuple(ArryParameters[nIndex].ParamValue));
                }

                NumMatches = 1;
                HOperatorSet.FindLocalDeformableModel(ImageReduce, out ho_ImageRectified, out ho_VectorField, out ho_DeformedContours, hv_ModelID,
                                                                                    (new HTuple(AngleStart)).TupleRad(), (new HTuple(AngleExtent)).TupleRad(),
                                                                                    ScaleRMin, ScaleRMax,
                                                                                    ScaleCMin, ScaleCMax,
                                                                                    MinScore,
                                                                                    NumMatches,
                                                                                    MaxOverlap,
                                                                                    NumLevels,
                                                                                    Greediness,
                                                                                    tupleResultType,
                                                                                    ParamName,
                                                                                    ParamValue,
                                                                                    out hv_Score, out hv_Row, out hv_Column);
                HOperatorSet.CountSeconds(out hv_S2);
                hv_Time = hv_S2 - hv_S1;
                hv_Found = new HTuple[hv_Score.TupleLength()];

                if (hv_Score.TupleLength() <= 0)
                {
                    HOperatorSet.SetColor(ImageProcessWnd.hWindow, "red");
                    HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                    HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part NG(Nothing found)", "image",
                                                                             rio.CheckedRIO.fRow - rio.CheckedRIO.fLen1 + 40,
                                                                             rio.CheckedRIO.fCol - rio.CheckedRIO.fLen2 + 40,
                                                                             "red", "true");
                    return false;
                }

                ho_MeshRegion.Dispose();
                gen_warped_mesh_region(ho_VectorField, out ho_MeshRegion, hv_Smoothness);
                ho_EdgeRegion.Dispose();
                HOperatorSet.GenRegionContourXld(ho_DeformedContours, out ho_EdgeRegion, "margin");
                ho_RegionDilation.Dispose();
                HOperatorSet.DilationCircle(ho_EdgeRegion, out ho_RegionDilation, 2 * hv_Smoothness);
                ho_RegionIntersection.Dispose();
                HOperatorSet.Intersection(ho_RegionDilation, ho_MeshRegion, out ho_RegionIntersection);

                HOperatorSet.SetLineWidth(ImageProcessWnd.hWindow, 1);
                HOperatorSet.SetColor(ImageProcessWnd.hWindow, "yellow");
                /*                    HOperatorSet.DispObj(ho_RegionIntersection, ImageProcessWnd.hWindow);*/

                HOperatorSet.SetLineWidth(ImageProcessWnd.hWindow, 2);
                HOperatorSet.SetColor(ImageProcessWnd.hWindow, "green"); //HDevWindowStack.GetActive()
                                                                         /*                    HOperatorSet.DispObj(ho_DeformedContours, ImageProcessWnd.hWindow);*/

                HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Match found", "image", hv_Row, hv_Column, "black", "true");
                HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "MTime: " + hv_Time.TupleString("1.2f") + "s", "image", hv_Row + 100, hv_Column, "black", "true");
                HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Score: " + hv_Score.TupleString(".2f"), "image", hv_Row + 200, hv_Column, "black", "true");
                /*                    HOperatorSet.DispObj(ho_ImageRectified, ImageTempWnd.hWindow);*/

                ho_Region.Dispose();
                HOperatorSet.CompareVariationModel(ho_ImageRectified, out ho_Region, hv_VariationModelID);
                ho_ConnectedRegions.Dispose();
                HOperatorSet.Connection(ho_Region, out ho_ConnectedRegions);
                ho_SelectedRegions.Dispose();
                HOperatorSet.SelectShape(ho_ConnectedRegions, out ho_SelectedRegions, "area", "and", AreaMin, 99999);
                HOperatorSet.CountObj(ho_SelectedRegions, out hv_Number);

                CountOfDefect = hv_Number;
                if (hv_Number <= 0)
                {
                    //out hv_Score, out hv_Row, out hv_Column
                    HOperatorSet.GetImageSize(objImage_Temp, out width, out height);
                    HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                    HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part OK", "image", hv_Row - height / 2, hv_Column - width / 2, "forest green", "true");
                    return false;
                }

                HOperatorSet.SetColor(ImageProcessWnd.hWindow, "red");
                HOperatorSet.DispObj(Rectangle1, ImageProcessWnd.hWindow);
                HOperatorSet.GetImageSize(objImage_Temp, out width, out height);
                HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part NG", "image", hv_Row - height / 2, hv_Column - width / 2, "red", "true");
                HOperatorSet.AreaCenter(ho_SelectedRegions, out hv_Area, out hv_Row1, out hv_Column1);

                //============================
                HOperatorSet.EllipticAxis(ho_SelectedRegions, out hv_Ra, out hv_Rb, out hv_Phi);

                HOperatorSet.TupleGenConst(hv_Number, 1, out hv_Ones);
                hv_PointOrder = new HTuple();
                HTuple end_val73 = hv_Number - 1;
                HTuple step_val73 = 1;
                for (hv_Idx = 0; hv_Idx.Continue(end_val73, step_val73); hv_Idx = hv_Idx.TupleAdd(step_val73))
                {
                    hv_PointOrder = hv_PointOrder.TupleConcat("positive");
                }
                ho_ContEllipse.Dispose();

                if (rio.MatchingResult == null)
                    rio.MatchingResult = new MATCHINGRESULT_DES();

                rio.MatchingResult.TupleRow = hv_Row1;
                rio.MatchingResult.TupleCol = hv_Column1;
                rio.MatchingResult.TupleAngle = hv_Phi;

                HOperatorSet.GenEllipseContourXld(out ho_ContEllipse, hv_Row1, hv_Column1, hv_Phi, hv_Ra + 10, hv_Rb + 10, 0 * hv_Ones, 6.28318 * hv_Ones, hv_PointOrder, 1.5);
                HOperatorSet.SetColor(ImageTempWnd.hWindow, "red");

                //重新定义缺陷坐标
                width = 0;
                height = 0;
                HOperatorSet.GetImageSize(objImage_Temp, out width, out height);
                HTuple HomMat2DIdentity, HomMat2DScale, HomMat2DRotate, HomMat2DTranslate;
                HOperatorSet.HomMat2dIdentity(out HomMat2DIdentity);
                HOperatorSet.HomMat2dScale(HomMat2DIdentity, 1, 1, 0, 0, out HomMat2DScale);
                HOperatorSet.HomMat2dRotate(HomMat2DScale, new HTuple(0).TupleRad(), 0, 0, out HomMat2DRotate);
                HOperatorSet.HomMat2dTranslate(HomMat2DRotate, hv_Row - height / 2, hv_Column - width / 2, out HomMat2DTranslate);
                HOperatorSet.AffineTransContourXld(ho_ContEllipse, out ho_ContEllipse, HomMat2DTranslate);
                HOperatorSet.SetColor(ImageProcessWnd.hWindow, "red");
                HOperatorSet.DispObj(ho_ContEllipse, ImageProcessWnd.hWindow);

                HalconGeneral.HD_DisplayMessage(ImageProcessWnd.hWindow, "Part not OK!", "image", 12, 12, "red", "true");

                //
                //Clean up
                ENDCHECK:
                ho_ModelImage.Dispose();

                ho_EdgeAmplitude.Dispose();
                ho_ModelContours.Dispose();
                ho_ContoursAffinTrans.Dispose();
                ho_ImageRectified.Dispose();
                ho_VectorField.Dispose();
                ho_DeformedContours.Dispose();
                ho_MeshRegion.Dispose();
                ho_EdgeRegion.Dispose();
                ho_RegionDilation.Dispose();
                ho_RegionIntersection.Dispose();
                ho_Region.Dispose();
                ho_ConnectedRegions.Dispose();
                ho_SelectedRegions.Dispose();
                ho_ContEllipse.Dispose();
                ho_Image.Dispose();

                return true;
            }
            catch (HalconException HDevExpDefaultException)
            {
                ho_ModelImage.Dispose();
                ho_Image.Dispose();
                ho_EdgeAmplitude.Dispose();
                ho_ModelContours.Dispose();
                ho_ContoursAffinTrans.Dispose();
                ho_ImageRectified.Dispose();
                ho_VectorField.Dispose();
                ho_DeformedContours.Dispose();
                ho_MeshRegion.Dispose();
                ho_EdgeRegion.Dispose();
                ho_RegionDilation.Dispose();
                ho_RegionIntersection.Dispose();
                ho_Region.Dispose();
                ho_ConnectedRegions.Dispose();
                ho_SelectedRegions.Dispose();
                ho_ContEllipse.Dispose();

                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ImageProcess_MuitlThred(HImage imageObj)
        {
            lock(threadObj)
            {
                HObject ho_Image = null;
                try
                {
                    FinallyResult = true;
                    if (!TemplateIsCreated)
                        throw new HalconException($"ShapeModelInspector 没有建立模版，请调用Init");

                    if (!HalconGeneral.IsImageObj(imageObj))
                        throw new HalconException($"ShapeModelInspector ImageProcess 图像格式不对");

                    HOperatorSet.CopyImage(imageObj, out objImage_Check);

                    //开始处理图片
                    CountOfDefect = 999;
                    HTuple width, height;
                    HOperatorSet.GetImageSize(imageObj, out width, out height);
                    ImageProcessWnd.ShowImage(imageObj, width, height);

                    HOperatorSet.CopyImage(imageObj, out ho_Image);
                    HOperatorSet.DispObj(ho_Image, ImageProcessWnd.hWindow);

                    HObject[] ImageReduce = new HObject[CheckResult.Length];
                    Task<bool>[] taskArry = new Task<bool>[CheckResult.Length];
                    bool[] taskResult = new bool[CheckResult.Length];

                    int nIndexAOI = 0;
                    while(nIndexAOI < CheckResult.Length)
                    {
                        
                        taskArry[nIndexAOI] = Task.Factory.StartNew<bool>((object obj) =>
                        {
                            int index = (int)obj;
                            CheckResultDef rio = new CheckResultDef();
                            rio.CheckedRIO.fRow = CheckResult[index].CheckedRIO.fRow;
                            rio.CheckedRIO.fCol = CheckResult[index].CheckedRIO.fCol;
                            rio.CheckedRIO.fPhi = CheckResult[index].CheckedRIO.fPhi;
                            rio.CheckedRIO.fLen1 = CheckResult[index].CheckedRIO.fLen1;
                            rio.CheckedRIO.fLen2 = CheckResult[index].CheckedRIO.fLen2;
                            return ImageProcess_RIO(ref rio);
                        }, nIndexAOI);

                        nIndexAOI++;
                    }

                    Task.WaitAll(taskArry, 20000);
                    FinallyResult = true;
                    nIndexAOI = 0;
                    for (nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                    {
                        if (taskArry[nIndexAOI] == null || taskArry[nIndexAOI].Result == false)
                            FinallyResult = false;
                    }

                    return FinallyResult;
                }
                catch (HalconException HDevExpDefaultException)
                {

                    throw new Exception(HDevExpDefaultException.Message);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private void ResetCheckResult()
        {
            try
            {
                for (int nIndexAOI = 0; nIndexAOI < CheckResult.Length; nIndexAOI++)
                {
                    CheckResult[nIndexAOI].Result = -999;
                    CheckResult[nIndexAOI].ResultStatus = false;
                    if (CheckResult[nIndexAOI].MatchingResult == null)
                        CheckResult[nIndexAOI].MatchingResult = new MATCHINGRESULT_DES();

                    CheckResult[nIndexAOI].MatchingResult.TupleRow = -1;
                    CheckResult[nIndexAOI].MatchingResult.TupleCol = -1;
                    CheckResult[nIndexAOI].MatchingResult.TupleAngle = -1;
                }
            }
            catch (HalconException HDevExpDefaultException)
            {
                throw new Exception(HDevExpDefaultException.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
