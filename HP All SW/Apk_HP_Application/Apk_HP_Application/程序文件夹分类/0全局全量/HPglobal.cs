﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Net;
using System.Xml;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Data;
using HslCommunication.Profinet.Omron;
using HslCommunication;
using MySql.Data.MySqlClient;
using Apk_HP_Application.程序文件夹分类._1调度总程序;
using Apk_HP_Application.程序文件夹分类._3打码功割控制程序;
using Apk_HP_Application.程序文件夹分类._4装盘及机器人控制;

namespace Apk_HP_Application.程序文件夹分类._0全局全量
{
    public class HPglobal
    {
        public static frm_AutomaticSmartLine frm_MainPanel = null;
        public static frm_LaserStationMain frm_LaserPanel = null;
        public static frm_RobotControl frm_RobotPanel = null;

        public static frm_Main frmMainWindow;
        public static string m_strUserApp = "";
        public static string m_strRFID = "";
        public static string m_strAdminPSW = "123456";
        public static bool m_bQuitApp = false;

        public static int nAppFlage = 0;
        public static object objAddRevData = new object();
        public static object objAddSendData = new object();

        public static string strExeFolderName = "";
        public static string m_strLastException = "";
        public static string m_strXmlFilename = "";
        public static string m_strModelName = "";
        public static XmlDocument m_xmlSystemConfig = new XmlDocument();
        public static XmlNode m_MesInterfaceNode;

        public static bool m_bEnableRemoteDebug = false;

        public static Queue<string> m_queMessageOut = new Queue<string>();

        public static string PCBID1_Address = "D1130";
        public static string TrayID1_Address = "D1146";

        public static string PCBID2_Address = "D1162";
        public static string TrayID2_Address = "D1178";

        public enum emMachineWorkingState
        {
            emMachineWorkingState_Disable = 0,
            emMachineWorkingState_Idle,
            emMachineWorkingState_Reseting,
            emMachineWorkingState_Working,
            emMachineWorkingState_NotReset,
            emMachineWorkingState_Alarm,
            emMachineWorkingState_Suspend,
            emMachineWorkingState_Stopping,
            emMachineWorkingState_Starting,
        }

        public static emMachineWorkingState emMachineState = emMachineWorkingState.emMachineWorkingState_NotReset;

        public struct MessageOut
        {
            public string strMesg;
            public System.Drawing.Color nColor;
            public int nFontSize;
            string strSoluction;
        };

        #region>>>>>>>>>> 各设备的IP地址
        public static  int nPortOfAllDevice = 5000;
        public static string strIPAddress_调度PC = "192.168.1.100";
        public static string strIPAddress_调度MES = "192.168.1.100";
        public static string strIPAddress_调度条码枪 = "192.168.1.102";

        public static string strIPAddress_上料PC = "192.168.1.113";
        public static string strIPAddress_上料相机1 = "192.168.259.100";
        public static string strIPAddress_上料PLC = "192.168.1.110";
        public static string strIPAddress_上料条码枪 = "192.168.1.112";

        public static string strIPAddress_烧录1PLC = "192.168.1.120";
        public static string strIPAddress_烧录1条码枪 = "192.168.1.122";
        public static string strIPAddress_烧录1烧录机 = "192.168.1.123";
        public static string strComport_烧录1烧录机 = "com13";

        public static string strIPAddress_烧录2PLC  = "192.168.1.130";
        public static string strIPAddress_烧录2条码枪  = "192.168.1.132";
        public static string strIPAddress_烧录2烧录机  = "192.168.1.133";
        public static string strComport_烧录2烧录机 = "com4";

        public static string strIPAddress_打码切割PLC = "";
        public static string strIPAddress_打码切割主控PC = "192.168.1.140";
        public static string strIPAddress_打码切割打码PC= "192.168.1.145";
        public static string strIPAddress_打码切割切割PC= "192.168.1.146";
        public static string strIPAddress_打码切割相机1 = "192.168.259.101";
        public static string strIPAddress_打码切割ECI2406 = "192.168.1.148";
        public static string strIPAddress_打码切割ECI0064 = "192.168.1.149";
        public static string strIPAddress_打码切割条码枪1= "192.168.1.142";
        public static string strIPAddress_打码切割条码枪2= "192.168.1.143";

        public static string strIPAddress_下料PLC = "192.168.1.150";
        public static string strIPAddress_下料PC = "192.168.1.154";
        public static string strIPAddress_下料相机1 = "192.168.259.102";
        public static string strIPAddress_下料相机2 = "192.168.259.103";
        public static string strIPAddress_下料机器人 = "192.168.1.155";
        public static string strIPAddress_下料条码枪1 = "192.168.1.152";
        public static string strIPAddress_下料条码枪2 = "192.168.1.153";

        public static string strIPAddress_包装PLC = "192.168.1.160";
        public static string strIPAddress_包装条码枪 = "192.168.1.162";
        public static string strIPAddress_包装PC= "192.168.1.163";

        public static string strIPAddress_摄像头1	 = "192.168.1.171";
        public static string strIPAddress_摄像头2 = "192.168.1.172";
        public static string strIPAddress_摄像头3 = "192.168.1.173";
        public static string strIPAddress_摄像头4 = "192.168.1.174";
        public static string strIPAddress_摄像头5 = "192.168.1.175";
        public static string strIPAddress_摄像头6 = "192.168.1.176";

        #endregion

        public static bool RecordeAllMessage(string message, bool bRun, bool bDebug, string strSolution = "")
        {
            DateTime time = DateTime.Now;
            string strTime = string.Format("{0:d4}-{1}-{2}", time.Year, time.Month, time.Day);

            string writeFileNameError = strExeFolderName + "\\LoginFile\\Error\\" + strTime + "_ErrorLogFile.csv";
            string writeFileNameRun = strExeFolderName + "\\LoginFile\\Run\\" + strTime + "_RunLogFile.csv";
            string writeFileNameDebug = strExeFolderName + "\\LoginFile\\Debug\\" + strTime + "_DebugLogFile.csv";

            try
            {
                AddMessageOutput(message);

                if (message.Contains("NG") || message.Contains("失败") || message.Contains("异常") || message.Contains("错误"))
                    return Report_Write(message, writeFileNameError);

                if (bRun)
                    return Report_Write(message, writeFileNameRun);

                if (bDebug)
                    return Report_Write(message, writeFileNameDebug);
            }
            catch (Exception ex)
            {
                m_strLastException = string.Format("RecordeAllErrorMessage 失败\r\n{0}", ex.ToString());
            }

            return false;
        }

        private static bool Report_Write(string message, string strFileName)
        {
            try
            {

                DateTime time = DateTime.Now;
                string strTime = string.Format("{0:d4}-{1}-{2}", time.Year, time.Month, time.Day);
                string writeFileName = strFileName;

                byte[] byteUTF8 = { 0xEF, 0xBB, 0xBF };

                //确认文件是否存在
                FileStream mesLogFile = null;
                if (!File.Exists(writeFileName))
                {
                    //创建文件夹
                    string folderPath = System.IO.Path.GetDirectoryName(writeFileName);
                    if (!System.IO.Directory.Exists(folderPath))
                        System.IO.Directory.CreateDirectory(folderPath);

                    mesLogFile = new FileStream(writeFileName, FileMode.Create);
                    mesLogFile.Seek(0, SeekOrigin.End);

                    //写入UTF-8编码
                    mesLogFile.Write(byteUTF8, 0, byteUTF8.Length);
                }
                else
                {
                    mesLogFile = new FileStream(writeFileName, FileMode.Append);
                    mesLogFile.Seek(0, SeekOrigin.End);
                }

                //写入数据
                string strWriteFile = time.ToString("u").Replace("T", " ").Replace("Z", "") + ":  " + message;
                strWriteFile += "\r\n";

                byte[] bytesWrite = System.Text.Encoding.UTF8.GetBytes(strWriteFile);
                mesLogFile.Write(bytesWrite, 0, bytesWrite.Length);
                mesLogFile.Flush();
                mesLogFile.Close();

                return true;
            }
            catch (Exception ex)
            {
                m_strLastException = string.Format("RecordeAllErrorMessage 失败\r\n{0}", ex.ToString());
            }

            return false;
        }

        private static void AddMessageOutput(string strMessage, string strType = "", string strSolution = "", bool bRecordeError = true)
        {
            try
            {
                lock(HPglobal.m_queMessageOut)
                {
                    DateTime Time1 = DateTime.Now;
                    string strDateTime = Time1.ToString("u").Replace("T", " ").Replace("Z", "");

                    string strAll = (string)strDateTime + "#";
                    strAll += (string)strType + "#";
                    strAll += strMessage + "#";
                    strAll += strSolution;

                    HPglobal.m_queMessageOut.Enqueue(strAll);
                }
            }
            catch (Exception ex)
            {
                m_strLastException = string.Format("AddMessageOutput() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
            }
        }

        public static void WorkingMessageOutput(ref Queue<HPglobal.MessageOut> queWorkingMessage, string Message, System.Drawing.Color loColor, int fontSize = 9)
        {
            try
            {
                lock(queWorkingMessage)
                {
                    HPglobal.MessageOut message = new HPglobal.MessageOut();

                    if (Message.Contains("\r\n"))
                        message.strMesg = Message;
                    else
                        message.strMesg = Message + "\r\n";
                    message.nColor = loColor;
                    message.nFontSize = fontSize;

                    //============================================
                    queWorkingMessage.Enqueue(message);

                    if(Message.Contains("失败") || Message.Contains("错误") || Message.Contains("异常") || Message.Contains("有误") ||  Message.Contains("出错") || Message.Contains("有错"))
                    {
                        HPglobal.m_strLastException = message.strMesg;
                        HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                    }
                }
            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("HPGlobal WorkingMessageOutput() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
            }
        }

        public static void outMessage(RichTextBox objTextBox, Queue<HPglobal.MessageOut> queWorkingMessage)
        {
            lock(objTextBox)
            {
                if (queWorkingMessage.Count <= 0)
                    return;

                while (queWorkingMessage.Count > 0)
                {
                    lock(queWorkingMessage)
                    {
                        MessageOut message = queWorkingMessage.Dequeue();
                        Font newFont = objTextBox.SelectionFont;
                        objTextBox.SelectionFont = new Font(newFont.Name, message.nFontSize, newFont.Style);
                        objTextBox.SelectionColor = message.nColor;

                        if (message.strMesg.Contains("clear"))
                            objTextBox.Clear();
                        else
                        {
                            string strDisp = message.strMesg;
                            if (!message.strMesg.Contains("\r\n"))
                                strDisp += "\r\n";

                            objTextBox.AppendText(message.strMesg);
                            objTextBox.Select(objTextBox.TextLength, 0);//设置光标的位置到文本尾
                            objTextBox.ScrollToCaret();//滚动到控件光标处 
                        }
                    }
                }
            }
        }

        public static void GlobalWorkingMessageOutput(string Message, System.Drawing.Color loColor, int fontSize = 9, int nFlag = 1)
        {
            try
            {
                MessageOut message = new MessageOut();

                message.strMesg = Message;
                message.nColor = loColor;
                message.nFontSize = fontSize;
                HPglobal.RecordeAllMessage(Message, true, false, "");

            }
            catch(Exception ex)
            {
                m_strLastException = string.Format("WorkingMessageOutput() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
            }
        }

        public static emActionHandle HandlerShow(string strDispaly, bool bIsShowBox = false)
        {
            try
            {
                if (bIsShowBox)
                {
                    frm_ActionErrorHandle ErrorHandleDlg = new frm_ActionErrorHandle(null);
                    emActionHandle result = ErrorHandleDlg.MessageBoxShow(strDispaly);
                    ErrorHandleDlg.Dispose();
                    return result;
                }
                else
                {
                    frm_ActionErrorHandle ErrorHandleDlg = new frm_ActionErrorHandle(null);
                    emActionHandle result = ErrorHandleDlg.DisplayMessage(strDispaly);
                    ErrorHandleDlg.Dispose();
                    return result;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        public static void ShowColorStatus(DevExpress.XtraEditors.LabelControl obj, emMachineWorkingState state)
        {
            try
            {
                switch (state)
                {
                    default:
                        obj.Text = "未知";
                        obj.BackColor = Color.Red;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Disable:
                        obj.Text = "已禁用";
                        obj.BackColor = Color.Red;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Idle:
                        obj.Text = "空闲";
                        obj.BackColor = Color.Yellow;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Reseting:
                        obj.Text = "复位中.....";
                        obj.BackColor = Color.Yellow;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Working:
                        obj.Text = "工作中......";
                        obj.BackColor = Color.LightGreen;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_NotReset:
                        obj.Text = "未复位";
                        obj.BackColor = Color.Red;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Alarm:
                        obj.Text = "设备报警";
                        obj.BackColor = Color.Red;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Suspend:
                        obj.Text = "设备暂停";
                        obj.BackColor = Color.Yellow; 
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Stopping:
                        obj.Text = "设备停止中.....";
                        obj.BackColor = Color.Yellow;
                        break;

                    case HPglobal.emMachineWorkingState.emMachineWorkingState_Starting:
                        obj.Text = "设备启动中.....";
                        obj.BackColor = Color.Yellow;
                        break;

                }
            }
            catch (Exception ex)
            {

            }
        }

        public static string Server_GetClient_Requirement(TcpClient tcpClient, ref string[] Parameters)
        {
            try
            {
                String strCommand = "", strSend = "";
                int nLen = tcpClient.Client.Available;
                if (nLen <= 2)
                    return null;

                Thread.Sleep(200);
                nLen = tcpClient.Client.Available;
                byte[] bytesBuff = new byte[40000];
                Array.Clear(bytesBuff, 0, bytesBuff.Length);

                #region>>>>>>>先遍历一下数据，看数据里面有没有终止符
                tcpClient.Client.Receive(bytesBuff, nLen, SocketFlags.Peek);
                bool bSechr0xd0xA = false;
                for (int nIndex = 0; nIndex < nLen - 1; nIndex++)
                {
                    if (bytesBuff[nIndex] != '\r' || bytesBuff[nIndex + 1] != '\n')
                        continue;

                    bSechr0xd0xA = true;
                    Array.Clear(bytesBuff, 0, bytesBuff.Length);
                    nLen = tcpClient.Client.Receive(bytesBuff, nIndex + 2, SocketFlags.None);
                    if (nLen > 0)
                    {
                        string strTemp = "\r\n";
                        strCommand = System.Text.Encoding.ASCII.GetString(bytesBuff).Trim();
                        string strCommand2 = System.Text.Encoding.UTF32.GetString(bytesBuff).Trim();
                        strCommand = strCommand.Replace("\0", "");
                        strCommand = strCommand.Replace("\r\n", "");

                        tcpClient.Client.Send(System.Text.Encoding.ASCII.GetBytes("Command Wrong!\r\n"));

                        //显示收到的TCP数据
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>收到数据:\"{strCommand}\" 来自{((IPEndPoint)tcpClient.Client.RemoteEndPoint).ToString()}", Color.Blue, 9, 1);

                        //MES 处理收到的TCP数据
                        return strCommand;
                    }
                }
                #endregion

                #region >>>>>>>>>>>> 如果当前的数据没有\r\n, 读取数据并舍弃
                if (!bSechr0xd0xA && nLen > 0)
                {
                    tcpClient.Client.Receive(bytesBuff, nLen, SocketFlags.None);
                    strCommand = System.Text.Encoding.Default.GetString(bytesBuff).Trim();
                    strCommand = strCommand.Replace("\0", "");
                    strCommand = strCommand.Replace("\r\n", "");
                    if (strCommand.Length > 1)
                    {
                        string message = $"错误: 收到无法处理的命令: ({strCommand}), 数据需要以\r\n (CRLF) 作为结束符";
                        tcpClient.Client.Send(System.Text.Encoding.ASCII.GetBytes("Command Wrong!\r\n"));
                        HPglobal.RecordeAllMessage(message, true, false, "数据需要以\r\n (CRLF) 作为结束符");
                    }

                    return null;
                }

                #endregion

                return null;
            }
            catch (Exception e)
            {
                HPglobal.m_strLastException = "Client_ReadAndWrite() 有异常::\r\n" + e.Message;
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
                return null;
            }
        }

        public static bool SendTCPRespond(int bResult, string strMessage, TcpClient tcpClient)
        {
            string strOK = ";OK\r\n";
            string strNG = ";NG\r\n";

            string strSend = "";

            //写入TCP
            lock (objAddSendData)
            {
                string strIP = "N/A";
                if (tcpClient != null)
                {
                    strIP = tcpClient.Client.RemoteEndPoint.ToString();
                    if (tcpClient.Client.Send(System.Text.Encoding.ASCII.GetBytes(strMessage)) > 0)
                        HPglobal.GlobalWorkingMessageOutput($"{DateTime.Now.ToLongTimeString()}>>>发送命令:\"{strMessage}\" 来自{((IPEndPoint)tcpClient.Client.LocalEndPoint).ToString()}", Color.Blue, 9, 1);
                }
            }

            return true;
        }

        public static void Xml_UpdateNodeValue(XmlNode fater, string strNodeName, string strNodeValue)
        {
            try
            {
                if (fater[strNodeName] == null)
                {
                    XmlNode nodeNewItem = m_xmlSystemConfig.CreateNode(XmlNodeType.Element, strNodeName, "");
                    fater.AppendChild(nodeNewItem);
                }

                fater[strNodeName].InnerText = strNodeValue;
            }
            catch (Exception ex)
            {
                m_strLastException = string.Format("UpdateNodeValue() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                RecordeAllMessage(m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
            }
        }

        public static void Xml_UpdateNodeAttribute(XmlNode nodeNewItem, string strAttribute, string strAttributeValue)
        {
            try
            {
                if (nodeNewItem == null)
                    return;

                if (nodeNewItem.Attributes[strAttribute] == null)
                {
                    XmlAttribute nodeAttr = m_xmlSystemConfig.CreateAttribute(strAttribute);
                    nodeNewItem.Attributes.Append(nodeAttr);
                }

                nodeNewItem.Attributes[strAttribute].InnerText = strAttributeValue;
            }
            catch (Exception ex)
            {
                m_strLastException = string.Format("UpdateNodeValue() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                RecordeAllMessage(m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
            }
        }

        public static int GetIndexOf(byte[] src, byte[] dst)
        {
            try
            {
                if (src == null || dst == null || src.Length == 0 || dst.Length == 0)
                    return -1;

                int i, j;
                for (i = 0; i < src.Length; i++)
                {
                    if (src[i] == dst[0])
                    {
                        for (j = 1; j < dst.Length; j++)
                        {
                            if (src[i + j] != dst[j])
                                break;
                        }
                        if (j == dst.Length)
                            return i;
                    }
                }
                return -1;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static void QuitApplicationAndClear()
        {
            HPglobal.m_bQuitApp = true;
            Thread.Sleep(1000);

            #region>>>>>>>>>释放全局变量及对话框
            if (frm_MainPanel != null)
                HPInvoke.QuitApplicationAndClear();

            if (frm_LaserPanel != null)
                HPRobotCtrl.QuitApplicationAndClear();

            if (frm_RobotPanel != null)
                HPRobotCtrl.QuitApplicationAndClear();

            #endregion
        }

        public static void CopyDirectory(string srcPath, string desPath)
        {
            try
            {
                if (!Directory.Exists(srcPath))
                    return;

                if (!Directory.Exists(desPath))
                    Directory.CreateDirectory(desPath);

                string[] filenames = Directory.GetFileSystemEntries(srcPath);
                foreach (string file in filenames)
                {
                    if (Directory.Exists(file))
                    {
                        string destDirectory = desPath + "\\" + file.Substring(file.LastIndexOf("\\") + 1);
                        CopyDirectory(file, destDirectory);
                    }

                    else
                    {
                        string DestFilePath = file.Substring(file.LastIndexOf("\\") + 1);
                        DestFilePath = desPath + "\\" + DestFilePath;

                        File.Copy(file, DestFilePath);
                    }
                }
            }
            catch(Exception ex)
            {
                m_strLastException = string.Format("UpdateNodeValue() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                RecordeAllMessage(m_strLastException, true, false, "检查PLC电源是否开启或线路是否正常及IP配制");
            }
           
        }

        public static bool NewProductCopyExist(string currentModeName, string newModeName)
        {
            try
            {
                string folderName = $"ConfigFile\\{currentModeName}";
                string NewfolderName = $"ConfigFile\\Model_{newModeName}";

                if (Directory.Exists(NewfolderName))
                {
                    MessageBox.Show("当前的型号产品已经存在，请重新输入!");
                    return false;
                }

                if (!Directory.Exists(NewfolderName))
                {
                    Directory.CreateDirectory(NewfolderName);
                }

                CopyDirectory(folderName, NewfolderName);
                return true;

            }
            catch (Exception ex)
            {
                HPglobal.m_strLastException = string.Format("frm_RobotControl() 异常: {0}{0}{1}", Environment.NewLine, ex.ToString());
                HPglobal.RecordeAllMessage(HPglobal.m_strLastException, true, false, "");
            }

            return false;
        }
    }
}
