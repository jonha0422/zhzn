﻿using HalconDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionLib.ImageProcess
{
    public class ImageProcessTest : ImageProcessBase
    {
        public string TestStr{get;set;}

         [Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]

        public List<string> TestStrList { get; set; }
        public ImageProcessTest()
        {
            cName = "测试类";
            TestStr = "123";
            TestStrList = new List<string>();

        }
        public override int Init(HImage vTempImage)
        {
            if (vTempImage != null)
            {
                TempImage = vTempImage.Clone();
            }
            return 0;
        }
        public override int ImageProcess()
        {
            //MessageBox.
            return 0;
        }
        public override ImageProcessBase CreatClass(string ClassPara)
        {
            ImageProcessTest aFun = CoverLib.JsonCover.DeStrLSerialize<ImageProcessTest>(ClassPara);
            return aFun;
        }
        public override string GetClassPara()
        {
            string  aFunPara = CoverLib.JsonCover.StrSerialize<ImageProcessTest>(this);
            return aFunPara;
        }
    }
}
