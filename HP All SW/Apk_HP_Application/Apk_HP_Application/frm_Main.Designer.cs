﻿namespace Apk_HP_Application
{
    partial class frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Main));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButton1 = new DevExpress.XtraEditors.DropDownButton();
            this.dataMsgError = new System.Windows.Forms.DataGridView();
            this.nameDataTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameSolution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.日期时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.信息类型 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.信息描述 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.解决方案 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lableOperator = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.imageList32 = new System.Windows.Forms.ImageList(this.components);
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.imageList72 = new System.Windows.Forms.ImageList(this.components);
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labMachineStatus = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.timeExeStatus = new System.Windows.Forms.Timer(this.components);
            this.timeMonitorMessage = new System.Windows.Forms.Timer(this.components);
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.txtExceptionMes = new System.Windows.Forms.TextBox();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.dataMsgRun = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit29 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.lableRFID = new DevExpress.XtraEditors.LabelControl();
            this.btmNewProduct = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataMsgError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataMsgRun)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3});
            this.barManager1.MaxItemId = 3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1904, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 1031);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1904, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 1031);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1904, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 1031);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(436, 286);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(188, 56);
            this.simpleButton12.TabIndex = 57;
            this.simpleButton12.Text = "打开机器人控制界面";
            this.simpleButton12.Visible = false;
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(242, 286);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(188, 56);
            this.simpleButton11.TabIndex = 51;
            this.simpleButton11.Text = "打开打码切割界面";
            this.simpleButton11.Visible = false;
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // dropDownButton1
            // 
            this.dropDownButton1.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButton1.Location = new System.Drawing.Point(48, 280);
            this.dropDownButton1.MenuManager = this.barManager1;
            this.dropDownButton1.Name = "dropDownButton1";
            this.dropDownButton1.Size = new System.Drawing.Size(188, 69);
            this.dropDownButton1.TabIndex = 45;
            this.dropDownButton1.Text = "dropDownButton1";
            this.dropDownButton1.Visible = false;
            // 
            // dataMsgError
            // 
            this.dataMsgError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataMsgError.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataMsgError.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataMsgError.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataTime,
            this.nameType,
            this.nameDescription,
            this.nameSolution});
            this.dataMsgError.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataMsgError.Location = new System.Drawing.Point(783, 5);
            this.dataMsgError.Name = "dataMsgError";
            this.dataMsgError.RowHeadersVisible = false;
            this.dataMsgError.RowHeadersWidth = 80;
            this.dataMsgError.RowTemplate.Height = 23;
            this.dataMsgError.Size = new System.Drawing.Size(763, 193);
            this.dataMsgError.TabIndex = 47;
            // 
            // nameDataTime
            // 
            this.nameDataTime.HeaderText = "日期和时间";
            this.nameDataTime.Name = "nameDataTime";
            this.nameDataTime.Width = 150;
            // 
            // nameType
            // 
            this.nameType.HeaderText = "信息类型";
            this.nameType.Name = "nameType";
            this.nameType.Width = 80;
            // 
            // nameDescription
            // 
            this.nameDescription.HeaderText = "信息描述";
            this.nameDescription.Name = "nameDescription";
            this.nameDescription.Width = 400;
            // 
            // nameSolution
            // 
            this.nameSolution.HeaderText = "解决方案";
            this.nameSolution.Name = "nameSolution";
            this.nameSolution.Width = 400;
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl1.Location = new System.Drawing.Point(3, 6);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(928, 193);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gridView1.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gridView1.Appearance.DetailTip.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.DetailTip.Options.UseFont = true;
            this.gridView1.Appearance.Empty.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.Empty.Options.UseFont = true;
            this.gridView1.Appearance.EvenRow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.EvenRow.Options.UseFont = true;
            this.gridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FixedLine.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.FixedLine.Options.UseFont = true;
            this.gridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupButton.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.GroupButton.Options.UseFont = true;
            this.gridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HorzLine.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.HorzLine.Options.UseFont = true;
            this.gridView1.Appearance.OddRow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.OddRow.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.gridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.Appearance.TopNewRow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.TopNewRow.Options.UseFont = true;
            this.gridView1.Appearance.VertLine.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.gridView1.Appearance.VertLine.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.日期时间,
            this.信息类型,
            this.信息描述,
            this.解决方案});
            this.gridView1.FixedLineWidth = 12;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // 日期时间
            // 
            this.日期时间.Caption = "日期时间";
            this.日期时间.Name = "日期时间";
            this.日期时间.Visible = true;
            this.日期时间.VisibleIndex = 0;
            this.日期时间.Width = 120;
            // 
            // 信息类型
            // 
            this.信息类型.Caption = "信息类型";
            this.信息类型.Name = "信息类型";
            this.信息类型.Visible = true;
            this.信息类型.VisibleIndex = 1;
            this.信息类型.Width = 100;
            // 
            // 信息描述
            // 
            this.信息描述.Caption = "信息描述";
            this.信息描述.Name = "信息描述";
            this.信息描述.Visible = true;
            this.信息描述.VisibleIndex = 2;
            this.信息描述.Width = 415;
            // 
            // 解决方案
            // 
            this.解决方案.Caption = "解决方案";
            this.解决方案.Name = "解决方案";
            this.解决方案.Visible = true;
            this.解决方案.VisibleIndex = 3;
            this.解决方案.Width = 422;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl4.AppearanceDisabled.Options.UseFont = true;
            this.labelControl4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl4.AppearanceHovered.Options.UseFont = true;
            this.labelControl4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl4.AppearancePressed.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(951, 6);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(64, 21);
            this.labelControl4.TabIndex = 23;
            this.labelControl4.Text = "产品型号";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl5.AppearanceDisabled.Options.UseFont = true;
            this.labelControl5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl5.AppearanceHovered.Options.UseFont = true;
            this.labelControl5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl5.AppearancePressed.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(1061, 44);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(32, 21);
            this.labelControl5.TabIndex = 21;
            this.labelControl5.Text = "站位";
            this.labelControl5.Visible = false;
            // 
            // lableOperator
            // 
            this.lableOperator.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lableOperator.Appearance.Options.UseFont = true;
            this.lableOperator.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lableOperator.AppearanceDisabled.Options.UseFont = true;
            this.lableOperator.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lableOperator.AppearanceHovered.Options.UseFont = true;
            this.lableOperator.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lableOperator.AppearancePressed.Options.UseFont = true;
            this.lableOperator.Location = new System.Drawing.Point(1244, 32);
            this.lableOperator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lableOperator.Name = "lableOperator";
            this.lableOperator.Size = new System.Drawing.Size(48, 21);
            this.lableOperator.TabIndex = 19;
            this.lableOperator.Text = "操作员";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.ImageOptions.ImageIndex = 213;
            this.simpleButton1.ImageOptions.ImageList = this.imageList32;
            this.simpleButton1.Location = new System.Drawing.Point(1161, 5);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(77, 80);
            this.simpleButton1.TabIndex = 18;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // imageList32
            // 
            this.imageList32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32.ImageStream")));
            this.imageList32.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList32.Images.SetKeyName(0, "0_48px_1101130_easyicon.net.png");
            this.imageList32.Images.SetKeyName(1, "14.png");
            this.imageList32.Images.SetKeyName(2, "33.png");
            this.imageList32.Images.SetKeyName(3, "34.png");
            this.imageList32.Images.SetKeyName(4, "111.png");
            this.imageList32.Images.SetKeyName(5, "123.bmp");
            this.imageList32.Images.SetKeyName(6, "126.png");
            this.imageList32.Images.SetKeyName(7, "222.jpg");
            this.imageList32.Images.SetKeyName(8, "222.png");
            this.imageList32.Images.SetKeyName(9, "333.jpg");
            this.imageList32.Images.SetKeyName(10, "444.jpg");
            this.imageList32.Images.SetKeyName(11, "444.png");
            this.imageList32.Images.SetKeyName(12, "9282699c-1e78-4e39-86aa-b9edb6d539c2.gif");
            this.imageList32.Images.SetKeyName(13, "20170323092493089308.jpg");
            this.imageList32.Images.SetKeyName(14, "add_32px_27198_easyicon.net.png");
            this.imageList32.Images.SetKeyName(15, "Add_32px_1177019_easyicon.net.png");
            this.imageList32.Images.SetKeyName(16, "add_64px_27198_easyicon.net.png");
            this.imageList32.Images.SetKeyName(17, "add_document_32px_519934_easyicon.net.png");
            this.imageList32.Images.SetKeyName(18, "adobe_cs4_file_DXF_48px_563925_easyicon.net.png");
            this.imageList32.Images.SetKeyName(19, "advanced_advancedsettings_settings_setup_64px_1768_easyicon.net.png");
            this.imageList32.Images.SetKeyName(20, "Aji_Annotate_48px_549651_easyicon.net.png");
            this.imageList32.Images.SetKeyName(21, "analytics_64px_1207991_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(22, "application_exit_3_48px_539519_easyicon.net.png");
            this.imageList32.Images.SetKeyName(23, "arc.png");
            this.imageList32.Images.SetKeyName(24, "arc1.png");
            this.imageList32.Images.SetKeyName(25, "arc2.png");
            this.imageList32.Images.SetKeyName(26, "arrow_52.241860465116px_1163618_easyicon.net.png");
            this.imageList32.Images.SetKeyName(27, "arrow_163.06302521008px_1199178_easyicon.net.png");
            this.imageList32.Images.SetKeyName(28, "Arrow_Redo_32px_528913_easyicon.net.png");
            this.imageList32.Images.SetKeyName(29, "arrow_redo_64px_533291_easyicon.net.png");
            this.imageList32.Images.SetKeyName(30, "Arrow_Undo_32px_528922_easyicon.net.png");
            this.imageList32.Images.SetKeyName(31, "arrow_undo_64px_533292_easyicon.net.png");
            this.imageList32.Images.SetKeyName(32, "arrow-transfer.png");
            this.imageList32.Images.SetKeyName(33, "arrow-transfer_副本.jpg");
            this.imageList32.Images.SetKeyName(34, "arrow-transfer_副本.png");
            this.imageList32.Images.SetKeyName(35, "AutoMul.png");
            this.imageList32.Images.SetKeyName(36, "back_32px_1075832_easyicon.net.png");
            this.imageList32.Images.SetKeyName(37, "back_32px_1075832_easyicon.net1.png");
            this.imageList32.Images.SetKeyName(38, "back_32px_1075832_easyicon.net2.png");
            this.imageList32.Images.SetKeyName(39, "back_32px_1075834_easyicon.net.png");
            this.imageList32.Images.SetKeyName(40, "back_32px_1075834_easyicon.net1.png");
            this.imageList32.Images.SetKeyName(41, "back_32px_1075834_easyicon.net2.png");
            this.imageList32.Images.SetKeyName(42, "back_32px_1075834_easyicon.net555.png");
            this.imageList32.Images.SetKeyName(43, "BACK_32px_1115269_easyicon.net.png");
            this.imageList32.Images.SetKeyName(44, "back_forward_37.266832917706px_1190579_easyicon.net.png");
            this.imageList32.Images.SetKeyName(45, "Bar_Code_49.765586034913px_1157038_easyicon.net.png");
            this.imageList32.Images.SetKeyName(46, "Bar_Code_49.765586034913px_1157038_easyicon.net_副本.png");
            this.imageList32.Images.SetKeyName(47, "bin_recycle_48px_13902_easyicon.net.png");
            this.imageList32.Images.SetKeyName(48, "bitbug_favicon2.ico");
            this.imageList32.Images.SetKeyName(49, "box_green_square_48px_3851_easyicon.net.png");
            this.imageList32.Images.SetKeyName(50, "bullet.png");
            this.imageList32.Images.SetKeyName(51, "bullet-green.png");
            this.imageList32.Images.SetKeyName(52, "camera.png");
            this.imageList32.Images.SetKeyName(53, "camera_48px_1186746_easyicon.net.png");
            this.imageList32.Images.SetKeyName(54, "camera_64px_1186746_easyicon.net.png");
            this.imageList32.Images.SetKeyName(55, "Camera_Leica_Active_48px_1186714_easyicon.net.png");
            this.imageList32.Images.SetKeyName(56, "Camera_Leica_Active_64px_1186714_easyicon.net.png");
            this.imageList32.Images.SetKeyName(57, "cancel.png");
            this.imageList32.Images.SetKeyName(58, "cancel_close_48px_568794_easyicon.net.png");
            this.imageList32.Images.SetKeyName(59, "card_kcmpci_pcb_128px_5911_easyicon.net.png");
            this.imageList32.Images.SetKeyName(60, "caution.png");
            this.imageList32.Images.SetKeyName(61, "checklist_48px_1142185_easyicon.net.png");
            this.imageList32.Images.SetKeyName(62, "command_terminal_64px_12667_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(63, "command_terminal_64px_12667_easyicon.net.png");
            this.imageList32.Images.SetKeyName(64, "communication_55.636363636364px_1204797_easyicon.net.png");
            this.imageList32.Images.SetKeyName(65, "config_set_48px_548300_easyicon.net.png");
            this.imageList32.Images.SetKeyName(66, "config_set_64px_548300_easyicon.net.png");
            this.imageList32.Images.SetKeyName(67, "control_64px_1194867_easyicon.net.png");
            this.imageList32.Images.SetKeyName(68, "controlcenter_72px_1126177_easyicon.net.png");
            this.imageList32.Images.SetKeyName(69, "Conversion_54.89841986456px_1188571_easyicon.net.png");
            this.imageList32.Images.SetKeyName(70, "copy.png");
            this.imageList32.Images.SetKeyName(71, "copy_documents_duplicate_files_48px_1496_easyicon.net.png");
            this.imageList32.Images.SetKeyName(72, "delete_document_32px_519935_easyicon.net.png");
            this.imageList32.Images.SetKeyName(73, "disk_save_all_64px_4650_easyicon.net.png");
            this.imageList32.Images.SetKeyName(74, "document_edit_64px_18562_easyicon.net.png");
            this.imageList32.Images.SetKeyName(75, "document-edit (1).png");
            this.imageList32.Images.SetKeyName(76, "document-edit.png");
            this.imageList32.Images.SetKeyName(77, "document-save-as.png");
            this.imageList32.Images.SetKeyName(78, "Dots_48px_1184584_easyicon.net.png");
            this.imageList32.Images.SetKeyName(79, "double_arrow_copy_70.323353293413px_1190610_easyicon.net.png");
            this.imageList32.Images.SetKeyName(80, "down_24px_535792_easyicon.net.png");
            this.imageList32.Images.SetKeyName(81, "down_32px_535792_easyicon.net.png");
            this.imageList32.Images.SetKeyName(82, "Down_dwonload_32px_533868_easyicon.net.png");
            this.imageList32.Images.SetKeyName(83, "download_24.png");
            this.imageList32.Images.SetKeyName(84, "download38.png");
            this.imageList32.Images.SetKeyName(85, "draw-spiral.png");
            this.imageList32.Images.SetKeyName(86, "edit_32px_28938_easyicon.net.png");
            this.imageList32.Images.SetKeyName(87, "edit_47.702325581395px_1200630_easyicon.net.png");
            this.imageList32.Images.SetKeyName(88, "edit_48px_1208057_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(89, "f.png");
            this.imageList32.Images.SetKeyName(90, "F1.jpg");
            this.imageList32.Images.SetKeyName(91, "F2 - 副本.jpg");
            this.imageList32.Images.SetKeyName(92, "F2.jpg");
            this.imageList32.Images.SetKeyName(93, "file_48px_1208074_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(94, "file_527px_1208074_easyicon.net.png");
            this.imageList32.Images.SetKeyName(95, "File_Checked_22.5px_1073368_easyicon.net.png");
            this.imageList32.Images.SetKeyName(96, "file_complete_48px_1187329_easyicon.net.png");
            this.imageList32.Images.SetKeyName(97, "File_Delete_22.5px_1073371_easyicon.net.png");
            this.imageList32.Images.SetKeyName(98, "file_warning_48px_1187331_easyicon.net.png");
            this.imageList32.Images.SetKeyName(99, "focus.png");
            this.imageList32.Images.SetKeyName(100, "folder_84.564491654021px_1207496_easyicon.net.png");
            this.imageList32.Images.SetKeyName(101, "font-239.png");
            this.imageList32.Images.SetKeyName(102, "font-399.png");
            this.imageList32.Images.SetKeyName(103, "forward_media_skip_32px_7865_easyicon.net.png");
            this.imageList32.Images.SetKeyName(104, "free-60-icons-47.png");
            this.imageList32.Images.SetKeyName(105, "glue_55.6032px_1214075_easyicon.net.png");
            this.imageList32.Images.SetKeyName(106, "gps_48.444444444444px_1208084_easyicon.net.png");
            this.imageList32.Images.SetKeyName(107, "gps_48px_1208084_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(108, "Graph_64px_1184580_easyicon.net.png");
            this.imageList32.Images.SetKeyName(109, "grey-ball - 副本 (2).png");
            this.imageList32.Images.SetKeyName(110, "grey-ball - 副本.png");
            this.imageList32.Images.SetKeyName(111, "grey-ball.png");
            this.imageList32.Images.SetKeyName(112, "hand_64px_26742_easyicon.net.png");
            this.imageList32.Images.SetKeyName(113, "help_browser_48px_1172050_easyicon.net.png");
            this.imageList32.Images.SetKeyName(114, "home_48px_567874_easyicon.net.png");
            this.imageList32.Images.SetKeyName(115, "home_64px_26949_easyicon.net.png");
            this.imageList32.Images.SetKeyName(116, "home_accept_64px_535719_easyicon.net.png");
            this.imageList32.Images.SetKeyName(117, "home_next_64px_534766_easyicon.net.png");
            this.imageList32.Images.SetKeyName(118, "Laser_Black_64px_533588_easyicon.net.png");
            this.imageList32.Images.SetKeyName(119, "layer_directions.png");
            this.imageList32.Images.SetKeyName(120, "Left_32px_533860_easyicon.net.png");
            this.imageList32.Images.SetKeyName(121, "left46.png");
            this.imageList32.Images.SetKeyName(122, "Lights_64px_1184578_easyicon.net.png");
            this.imageList32.Images.SetKeyName(123, "line.png");
            this.imageList32.Images.SetKeyName(124, "line_graphic_64px_1205947_easyicon.net.png");
            this.imageList32.Images.SetKeyName(125, "line2.png");
            this.imageList32.Images.SetKeyName(126, "location_32px_1113431_easyicon.net.png");
            this.imageList32.Images.SetKeyName(127, "location_48px_1113431_easyicon.net.png");
            this.imageList32.Images.SetKeyName(128, "Location_48px_1178115_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(129, "location_58.860606060606px_1200654_easyicon.net.png");
            this.imageList32.Images.SetKeyName(130, "location_60.878048780488px_1215752_easyicon.net.png");
            this.imageList32.Images.SetKeyName(131, "location_64px_1187489_easyicon.net.png");
            this.imageList32.Images.SetKeyName(132, "log_64px_28444_easyicon.net.png");
            this.imageList32.Images.SetKeyName(133, "Login_64px_583562_easyicon.net.png");
            this.imageList32.Images.SetKeyName(134, "logview_72px_1174860_easyicon.net.png");
            this.imageList32.Images.SetKeyName(135, "map59.png");
            this.imageList32.Images.SetKeyName(136, "move_63.082437275986px_1208133_easyicon.net.png");
            this.imageList32.Images.SetKeyName(137, "move_64px_1208133_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(138, "move_128px_1205817_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(139, "move_256px_1146279_easyicon.net.png");
            this.imageList32.Images.SetKeyName(140, "MyDocuments_64px_1060328_easyicon.net.png");
            this.imageList32.Images.SetKeyName(141, "NEXT_32px_1115295_easyicon.net.png");
            this.imageList32.Images.SetKeyName(142, "next_32px_1130930_easyicon.net.png");
            this.imageList32.Images.SetKeyName(143, "Next_32px_1142771_easyicon.net.png");
            this.imageList32.Images.SetKeyName(144, "next_42.613466334165px_1190651_easyicon.net.png");
            this.imageList32.Images.SetKeyName(145, "next_45.176470588235px_1188115_easyicon.net.png");
            this.imageList32.Images.SetKeyName(146, "notepad_52.139449541284px_1208147_easyicon.net (1).png");
            this.imageList32.Images.SetKeyName(147, "ooopic_1521511232.png");
            this.imageList32.Images.SetKeyName(148, "ooopic_1521511331.png");
            this.imageList32.Images.SetKeyName(149, "open_file_64px_1187339_easyicon.net.png");
            this.imageList32.Images.SetKeyName(150, "order.png");
            this.imageList32.Images.SetKeyName(151, "Origin_48px_1175210_easyicon.net.png");
            this.imageList32.Images.SetKeyName(152, "pageBG.png");
            this.imageList32.Images.SetKeyName(153, "Parameter_Review_48px_534170_easyicon.net.png");
            this.imageList32.Images.SetKeyName(154, "Parameter_Review_64px_534170_easyicon.net.png");
            this.imageList32.Images.SetKeyName(155, "Parameters_64px_1129202_easyicon.net (1).png");
            this.imageList32.Images.SetKeyName(156, "Parameters_64px_1129202_easyicon.net.png");
            this.imageList32.Images.SetKeyName(157, "pause.png");
            this.imageList32.Images.SetKeyName(158, "pause_64px_1102252_easyicon.net.png");
            this.imageList32.Images.SetKeyName(159, "photograph_76.65306122449px_1206043_easyicon.net.png");
            this.imageList32.Images.SetKeyName(160, "photograph_543px_1193989_easyicon.net.png");
            this.imageList32.Images.SetKeyName(161, "photograph_941px_1199881_easyicon.net.png");
            this.imageList32.Images.SetKeyName(162, "playback_rec.png");
            this.imageList32.Images.SetKeyName(163, "power_exit_47.2px_1122609_easyicon.net_副本.png");
            this.imageList32.Images.SetKeyName(164, "rectangle_stroked.png");
            this.imageList32.Images.SetKeyName(165, "rectangle_stroked_副本.jpg");
            this.imageList32.Images.SetKeyName(166, "rectangle_stroked_副本2.jpg");
            this.imageList32.Images.SetKeyName(167, "redo_48px_1121336_easyicon.net.png");
            this.imageList32.Images.SetKeyName(168, "restart_32px_1201217_easyicon.net.png");
            this.imageList32.Images.SetKeyName(169, "Right_32px_533853_easyicon.net.png");
            this.imageList32.Images.SetKeyName(170, "Road_48px_1178527_easyicon.net.png");
            this.imageList32.Images.SetKeyName(171, "round_ok.png");
            this.imageList32.Images.SetKeyName(172, "RUN_64px_1068484_easyicon.net.png");
            this.imageList32.Images.SetKeyName(173, "Run_Control_64px_534174_easyicon.net.png");
            this.imageList32.Images.SetKeyName(174, "Run_Keeper_62.933333333333px_1118755_easyicon.net.png");
            this.imageList32.Images.SetKeyName(175, "save.png");
            this.imageList32.Images.SetKeyName(176, "scale_32px_1116588_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(177, "scale9.png");
            this.imageList32.Images.SetKeyName(178, "scale9_副本.png");
            this.imageList32.Images.SetKeyName(179, "Select_32px_1072882_easyicon.net.png");
            this.imageList32.Images.SetKeyName(180, "SELECTION_31.764705882353px_1162378_easyicon.net.png");
            this.imageList32.Images.SetKeyName(181, "Sense.png");
            this.imageList32.Images.SetKeyName(182, "set_system_48px_12353_easyicon.net.png");
            this.imageList32.Images.SetKeyName(183, "set_system_64px_12353_easyicon.net.png");
            this.imageList32.Images.SetKeyName(184, "setting_67.95219123506px_1160205_easyicon.net.png");
            this.imageList32.Images.SetKeyName(185, "Settings_64px_561261_easyicon.net.png");
            this.imageList32.Images.SetKeyName(186, "Settings_64px_1123332_easyicon.net.png");
            this.imageList32.Images.SetKeyName(187, "Settings_64px_1127072_easyicon.net.png");
            this.imageList32.Images.SetKeyName(188, "sign_out_45.12px_1189761_easyicon.net.png");
            this.imageList32.Images.SetKeyName(189, "sign_out_60.16px_1189761_easyicon.net.png");
            this.imageList32.Images.SetKeyName(190, "sign_out_exit_24px_515890_easyicon.net.png");
            this.imageList32.Images.SetKeyName(191, "spiral_64.143176733781px_1164210_easyicon.net.png");
            this.imageList32.Images.SetKeyName(192, "square_48.24px_1189785_easyicon.net.png");
            this.imageList32.Images.SetKeyName(193, "start_48px_568810_easyicon.net.png");
            this.imageList32.Images.SetKeyName(194, "Start_48px_1186321_easyicon.net.png");
            this.imageList32.Images.SetKeyName(195, "start_here_kde_48px_1174073_easyicon.net.png");
            this.imageList32.Images.SetKeyName(196, "Stop_32px_507733_easyicon.net.png");
            this.imageList32.Images.SetKeyName(197, "stop_48px_534893_easyicon.net.png");
            this.imageList32.Images.SetKeyName(198, "stop_48px_535068_easyicon.net.png");
            this.imageList32.Images.SetKeyName(199, "stop_48px_568811_easyicon.net.png");
            this.imageList32.Images.SetKeyName(200, "stop-red.png");
            this.imageList32.Images.SetKeyName(201, "System para.png");
            this.imageList32.Images.SetKeyName(202, "system_run_5_64px_540096_easyicon.net.png");
            this.imageList32.Images.SetKeyName(203, "target-download-icon.png");
            this.imageList32.Images.SetKeyName(204, "timg.jpg");
            this.imageList32.Images.SetKeyName(205, "timg22.jpg");
            this.imageList32.Images.SetKeyName(206, "Toolbar256.bmp");
            this.imageList32.Images.SetKeyName(207, "undo_48px_1121367_easyicon.net.png");
            this.imageList32.Images.SetKeyName(208, "Up_32px_533851_easyicon.net.png");
            this.imageList32.Images.SetKeyName(209, "up_32px_535793_easyicon.net.png");
            this.imageList32.Images.SetKeyName(210, "upload_24.png");
            this.imageList32.Images.SetKeyName(211, "upload28.png");
            this.imageList32.Images.SetKeyName(212, "USB_32px_555315_easyicon.net.ico");
            this.imageList32.Images.SetKeyName(213, "user_mapping_48px_1187361_easyicon.net.png");
            this.imageList32.Images.SetKeyName(214, "user_modify_32px_556807_easyicon.net.png");
            this.imageList32.Images.SetKeyName(215, "warning.ico");
            this.imageList32.Images.SetKeyName(216, "warning.png");
            this.imageList32.Images.SetKeyName(217, "World_64px_1184563_easyicon.net.png");
            this.imageList32.Images.SetKeyName(218, "zoom_48px_1121377_easyicon.net.png");
            this.imageList32.Images.SetKeyName(219, "zoom_auto.png");
            this.imageList32.Images.SetKeyName(220, "zoom_Full.png");
            this.imageList32.Images.SetKeyName(221, "zoom_in.png");
            this.imageList32.Images.SetKeyName(222, "zoom_out.png");
            this.imageList32.Images.SetKeyName(223, "ZoomIn.png");
            this.imageList32.Images.SetKeyName(224, "控制器.png");
            this.imageList32.Images.SetKeyName(225, "控制器_副本.jpg");
            this.imageList32.Images.SetKeyName(226, "三角形.png");
            this.imageList32.Images.SetKeyName(227, "四方形.png");
            this.imageList32.Images.SetKeyName(228, "微信截图_20180113225902.png");
            this.imageList32.Images.SetKeyName(229, "未命名_副本.png");
            this.imageList32.Images.SetKeyName(230, "无标题.png");
            this.imageList32.Images.SetKeyName(231, "无标题2.png");
            this.imageList32.Images.SetKeyName(232, "圆.png");
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl2.AppearanceDisabled.Options.UseFont = true;
            this.labelControl2.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl2.AppearanceHovered.Options.UseFont = true;
            this.labelControl2.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.labelControl2.AppearancePressed.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(1242, 6);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 21);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "工单信息";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 32.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 32.25F, System.Drawing.FontStyle.Italic);
            this.labelControl1.AppearanceDisabled.Options.UseFont = true;
            this.labelControl1.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 32.25F, System.Drawing.FontStyle.Italic);
            this.labelControl1.AppearanceHovered.Options.UseFont = true;
            this.labelControl1.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 32.25F, System.Drawing.FontStyle.Italic);
            this.labelControl1.AppearancePressed.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(197, 12);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(643, 57);
            this.labelControl1.TabIndex = 15;
            this.labelControl1.Text = "Apexmic HP带头智能自动生产线";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(0, 1);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(175, 85);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2010 Blue";
            // 
            // imageList72
            // 
            this.imageList72.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList72.ImageStream")));
            this.imageList72.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList72.Images.SetKeyName(0, "abiword.png");
            this.imageList72.Images.SetKeyName(1, "akonadi.png");
            this.imageList72.Images.SetKeyName(2, "ardour.png");
            this.imageList72.Images.SetKeyName(3, "bell.png");
            this.imageList72.Images.SetKeyName(4, "black-address-book-new.png");
            this.imageList72.Images.SetKeyName(5, "black-document-open.png");
            this.imageList72.Images.SetKeyName(6, "black-folder-new.png");
            this.imageList72.Images.SetKeyName(7, "black-go-down.png");
            this.imageList72.Images.SetKeyName(8, "black-go-next.png");
            this.imageList72.Images.SetKeyName(9, "black-go-previous.png");
            this.imageList72.Images.SetKeyName(10, "black-go-up.png");
            this.imageList72.Images.SetKeyName(11, "blue-address-book-new.png");
            this.imageList72.Images.SetKeyName(12, "blue-document-open.png");
            this.imageList72.Images.SetKeyName(13, "blue-folder-new.png");
            this.imageList72.Images.SetKeyName(14, "blue-go-down.png");
            this.imageList72.Images.SetKeyName(15, "blue-go-next.png");
            this.imageList72.Images.SetKeyName(16, "blue-go-previous.png");
            this.imageList72.Images.SetKeyName(17, "blue-go-up.png");
            this.imageList72.Images.SetKeyName(18, "blue-view-refresh.png");
            this.imageList72.Images.SetKeyName(19, "bookmark-new.png");
            this.imageList72.Images.SetKeyName(20, "bookmarks_list-add.png");
            this.imageList72.Images.SetKeyName(21, "camorama.png");
            this.imageList72.Images.SetKeyName(22, "clamtk.png");
            this.imageList72.Images.SetKeyName(23, "cog-icon-2-48x48.png");
            this.imageList72.Images.SetKeyName(24, "color-line.png");
            this.imageList72.Images.SetKeyName(25, "config-date.png");
            this.imageList72.Images.SetKeyName(26, "conky.png");
            this.imageList72.Images.SetKeyName(27, "contact-new.png");
            this.imageList72.Images.SetKeyName(28, "ddd.png");
            this.imageList72.Images.SetKeyName(29, "document-save.png");
            this.imageList72.Images.SetKeyName(30, "document-save-as.png");
            this.imageList72.Images.SetKeyName(31, "document-send.png");
            this.imageList72.Images.SetKeyName(32, "dvdstyler.png");
            this.imageList72.Images.SetKeyName(33, "EasyTAG_icon.png");
            this.imageList72.Images.SetKeyName(34, "eclipse.png");
            this.imageList72.Images.SetKeyName(35, "edit-delete.png");
            this.imageList72.Images.SetKeyName(36, "emesene.png");
            this.imageList72.Images.SetKeyName(37, "evolution01.png");
            this.imageList72.Images.SetKeyName(38, "evolution02.png");
            this.imageList72.Images.SetKeyName(39, "firefox11.png");
            this.imageList72.Images.SetKeyName(40, "gadu.png");
            this.imageList72.Images.SetKeyName(41, "galternatives.png");
            this.imageList72.Images.SetKeyName(42, "gnucash.png");
            this.imageList72.Images.SetKeyName(43, "gnumeric.png");
            this.imageList72.Images.SetKeyName(44, "gnutella.png");
            this.imageList72.Images.SetKeyName(45, "gtk-color-picker.png");
            this.imageList72.Images.SetKeyName(46, "gtk-preferences.png");
            this.imageList72.Images.SetKeyName(47, "gtk-print-report.png");
            this.imageList72.Images.SetKeyName(48, "gtk-print-setup.png");
            this.imageList72.Images.SetKeyName(49, "gtranslator.png");
            this.imageList72.Images.SetKeyName(50, "gwget.png");
            this.imageList72.Images.SetKeyName(51, "gworldclock.png");
            this.imageList72.Images.SetKeyName(52, "help-browser.png");
            this.imageList72.Images.SetKeyName(53, "help-contents.png");
            this.imageList72.Images.SetKeyName(54, "help-faq.png");
            this.imageList72.Images.SetKeyName(55, "hwbrowser.png");
            this.imageList72.Images.SetKeyName(56, "hwinfo.png");
            this.imageList72.Images.SetKeyName(57, "hydrogen.png");
            this.imageList72.Images.SetKeyName(58, "insert-link.png");
            this.imageList72.Images.SetKeyName(59, "insert-object.png");
            this.imageList72.Images.SetKeyName(60, "insert-text.png");
            this.imageList72.Images.SetKeyName(61, "internet-radio-new.png");
            this.imageList72.Images.SetKeyName(62, "licq.png");
            this.imageList72.Images.SetKeyName(63, "lincity-ng.png");
            this.imageList72.Images.SetKeyName(64, "linuxdcpp.png");
            this.imageList72.Images.SetKeyName(65, "list-remove.png");
            this.imageList72.Images.SetKeyName(66, "multimedia.png");
            this.imageList72.Images.SetKeyName(67, "nautilus-cd-burner.png");
            this.imageList72.Images.SetKeyName(68, "neat.png");
            this.imageList72.Images.SetKeyName(69, "neat-control.png");
            this.imageList72.Images.SetKeyName(70, "netscape.png");
            this.imageList72.Images.SetKeyName(71, "newstyle-edit-find.png");
            this.imageList72.Images.SetKeyName(72, "newstyle-edit-find-replace.png");
            this.imageList72.Images.SetKeyName(73, "object-rotate-left.png");
            this.imageList72.Images.SetKeyName(74, "object-rotate-right.png");
            this.imageList72.Images.SetKeyName(75, "system-config-boot.png");
            this.imageList72.Images.SetKeyName(76, "xmms.png");
            this.imageList72.Images.SetKeyName(77, "xvidcap.png");
            this.imageList72.Images.SetKeyName(78, "znes.png");
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.ImageOptions.ImageIndex = 213;
            this.simpleButton2.ImageOptions.ImageList = this.imageList32;
            this.simpleButton2.Location = new System.Drawing.Point(872, 5);
            this.simpleButton2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(77, 80);
            this.simpleButton2.TabIndex = 35;
            this.simpleButton2.Text = "[";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.FontStyleDelta = System.Drawing.FontStyle.Italic;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Italic);
            this.labelControl6.AppearanceDisabled.Options.UseFont = true;
            this.labelControl6.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Italic);
            this.labelControl6.AppearanceHovered.Options.UseFont = true;
            this.labelControl6.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Italic);
            this.labelControl6.AppearancePressed.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(360, 65);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(356, 21);
            this.labelControl6.TabIndex = 36;
            this.labelControl6.Text = "Created by 众华智能科技 Copyright 2019~2025";
            // 
            // labMachineStatus
            // 
            this.labMachineStatus.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labMachineStatus.Appearance.Font = new System.Drawing.Font("微软雅黑", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMachineStatus.Appearance.Options.UseBackColor = true;
            this.labMachineStatus.Appearance.Options.UseFont = true;
            this.labMachineStatus.Appearance.Options.UseTextOptions = true;
            this.labMachineStatus.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labMachineStatus.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labMachineStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labMachineStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.labMachineStatus.Location = new System.Drawing.Point(1493, 1);
            this.labMachineStatus.Name = "labMachineStatus";
            this.labMachineStatus.Size = new System.Drawing.Size(315, 81);
            this.labMachineStatus.TabIndex = 37;
            this.labMachineStatus.Text = "Ready.....";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.ImageOptions.Image")));
            this.simpleButton3.ImageOptions.ImageIndex = 213;
            this.simpleButton3.ImageOptions.ImageList = this.imageList32;
            this.simpleButton3.Location = new System.Drawing.Point(1827, 4);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(77, 78);
            this.simpleButton3.TabIndex = 39;
            // 
            // timeExeStatus
            // 
            this.timeExeStatus.Interval = 300;
            this.timeExeStatus.Tick += new System.EventHandler(this.timeExeStatus_Tick);
            // 
            // timeMonitorMessage
            // 
            this.timeMonitorMessage.Interval = 200;
            this.timeMonitorMessage.Tick += new System.EventHandler(this.timeMonitorMessage_Tick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(952, 252);
            // 
            // txtExceptionMes
            // 
            this.txtExceptionMes.Location = new System.Drawing.Point(937, 6);
            this.txtExceptionMes.Multiline = true;
            this.txtExceptionMes.Name = "txtExceptionMes";
            this.txtExceptionMes.Size = new System.Drawing.Size(609, 193);
            this.txtExceptionMes.TabIndex = 64;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.xtraTabControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtraTabControl1.Appearance.Options.UseBackColor = true;
            this.xtraTabControl1.Appearance.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderDisabled.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabControl1.AppearancePage.PageClient.Options.UseFont = true;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 792);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1557, 235);
            this.xtraTabControl1.TabIndex = 65;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.Header.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderActive.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderActive.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderDisabled.Options.UseFont = true;
            this.xtraTabPage1.Appearance.HeaderHotTracked.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.HeaderHotTracked.Options.UseFont = true;
            this.xtraTabPage1.Appearance.PageClient.BackColor = System.Drawing.Color.PapayaWhip;
            this.xtraTabPage1.Appearance.PageClient.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.xtraTabPage1.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage1.Appearance.PageClient.Options.UseFont = true;
            this.xtraTabPage1.Controls.Add(this.panelControl1);
            this.xtraTabPage1.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.Image")));
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1551, 203);
            this.xtraTabPage1.Text = "运行信息";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelControl1.Appearance.Options.UseFont = true;
            this.panelControl1.Controls.Add(this.dataMsgRun);
            this.panelControl1.Controls.Add(this.dataMsgError);
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1551, 215);
            this.panelControl1.TabIndex = 0;
            // 
            // dataMsgRun
            // 
            this.dataMsgRun.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataMsgRun.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataMsgRun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataMsgRun.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dataMsgRun.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataMsgRun.Location = new System.Drawing.Point(11, 5);
            this.dataMsgRun.Name = "dataMsgRun";
            this.dataMsgRun.RowHeadersVisible = false;
            this.dataMsgRun.RowHeadersWidth = 80;
            this.dataMsgRun.RowTemplate.Height = 23;
            this.dataMsgRun.Size = new System.Drawing.Size(763, 193);
            this.dataMsgRun.TabIndex = 48;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "日期和时间";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "信息类型";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 80;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "信息描述";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 400;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "解决方案";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 400;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.panelControl2);
            this.xtraTabPage2.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage2.Image")));
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1551, 203);
            this.xtraTabPage2.Text = "生产数据统计";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.groupControl1);
            this.panelControl2.Controls.Add(this.groupControl2);
            this.panelControl2.Controls.Add(this.groupControl3);
            this.panelControl2.Controls.Add(this.groupControl4);
            this.panelControl2.Controls.Add(this.groupControl5);
            this.panelControl2.Controls.Add(this.groupControl6);
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1556, 204);
            this.panelControl2.TabIndex = 1;
            // 
            // groupControl1
            // 
            this.groupControl1.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.Appearance.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.simpleButton4);
            this.groupControl1.Controls.Add(this.textEdit5);
            this.groupControl1.Controls.Add(this.textEdit4);
            this.groupControl1.Controls.Add(this.textEdit3);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.textEdit2);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.textEdit1);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Location = new System.Drawing.Point(9, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(230, 173);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "上料";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton4.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.AppearanceHovered.Image")));
            this.simpleButton4.AppearanceHovered.Options.UseFont = true;
            this.simpleButton4.AppearanceHovered.Options.UseImage = true;
            this.simpleButton4.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton4.AppearancePressed.Options.UseFont = true;
            this.simpleButton4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.ImageOptions.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(147, 130);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(75, 38);
            this.simpleButton4.TabIndex = 8;
            this.simpleButton4.Text = "清零";
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(164, 65);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit5.Size = new System.Drawing.Size(54, 24);
            this.textEdit5.TabIndex = 7;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(164, 35);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit4.Size = new System.Drawing.Size(54, 24);
            this.textEdit4.TabIndex = 6;
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(108, 95);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit3.Size = new System.Drawing.Size(54, 24);
            this.textEdit3.TabIndex = 5;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl10.Appearance.Options.UseBackColor = true;
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl10.AppearanceDisabled.Options.UseFont = true;
            this.labelControl10.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl10.AppearanceHovered.Options.UseFont = true;
            this.labelControl10.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl10.AppearancePressed.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(77, 99);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(25, 17);
            this.labelControl10.TabIndex = 4;
            this.labelControl10.Text = "UPH";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(108, 65);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit2.Size = new System.Drawing.Size(54, 24);
            this.textEdit2.TabIndex = 3;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl9.AppearanceDisabled.Options.UseFont = true;
            this.labelControl9.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl9.AppearanceHovered.Options.UseFont = true;
            this.labelControl9.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl9.AppearancePressed.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(15, 69);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(87, 17);
            this.labelControl9.TabIndex = 2;
            this.labelControl9.Text = "坏品(整个Panel)";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(108, 35);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit1.Size = new System.Drawing.Size(54, 24);
            this.textEdit1.TabIndex = 1;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceDisabled.Options.UseFont = true;
            this.labelControl8.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearanceHovered.Options.UseFont = true;
            this.labelControl8.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl8.AppearancePressed.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(15, 39);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(87, 17);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "好品(整个Panel)";
            // 
            // groupControl2
            // 
            this.groupControl2.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.Appearance.Options.UseFont = true;
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.simpleButton5);
            this.groupControl2.Controls.Add(this.textEdit6);
            this.groupControl2.Controls.Add(this.textEdit7);
            this.groupControl2.Controls.Add(this.textEdit8);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.textEdit9);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.textEdit10);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Location = new System.Drawing.Point(245, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(230, 173);
            this.groupControl2.TabIndex = 46;
            this.groupControl2.Text = "烧录1";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton5.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton5.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton5.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.AppearanceHovered.Image")));
            this.simpleButton5.AppearanceHovered.Options.UseFont = true;
            this.simpleButton5.AppearanceHovered.Options.UseImage = true;
            this.simpleButton5.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton5.AppearancePressed.Options.UseFont = true;
            this.simpleButton5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton5.ImageOptions.Image")));
            this.simpleButton5.Location = new System.Drawing.Point(147, 130);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(75, 38);
            this.simpleButton5.TabIndex = 8;
            this.simpleButton5.Text = "清零";
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(164, 65);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit6.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit6.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit6.Size = new System.Drawing.Size(54, 24);
            this.textEdit6.TabIndex = 7;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(164, 35);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit7.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit7.Size = new System.Drawing.Size(54, 24);
            this.textEdit7.TabIndex = 6;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(108, 95);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit8.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit8.Size = new System.Drawing.Size(54, 24);
            this.textEdit8.TabIndex = 5;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl11.AppearanceDisabled.Options.UseFont = true;
            this.labelControl11.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl11.AppearanceHovered.Options.UseFont = true;
            this.labelControl11.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl11.AppearancePressed.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(77, 99);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(25, 17);
            this.labelControl11.TabIndex = 4;
            this.labelControl11.Text = "UPH";
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(108, 65);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit9.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit9.Size = new System.Drawing.Size(54, 24);
            this.textEdit9.TabIndex = 3;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearanceDisabled.Options.UseFont = true;
            this.labelControl12.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearanceHovered.Options.UseFont = true;
            this.labelControl12.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl12.AppearancePressed.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(15, 69);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(87, 17);
            this.labelControl12.TabIndex = 2;
            this.labelControl12.Text = "坏品(整个Panel)";
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(108, 35);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit10.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit10.Size = new System.Drawing.Size(54, 24);
            this.textEdit10.TabIndex = 1;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearanceDisabled.Options.UseFont = true;
            this.labelControl13.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearanceHovered.Options.UseFont = true;
            this.labelControl13.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl13.AppearancePressed.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(15, 39);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(87, 17);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "好品(整个Panel)";
            // 
            // groupControl3
            // 
            this.groupControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.Appearance.Options.UseFont = true;
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.simpleButton6);
            this.groupControl3.Controls.Add(this.textEdit11);
            this.groupControl3.Controls.Add(this.textEdit12);
            this.groupControl3.Controls.Add(this.textEdit13);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.textEdit14);
            this.groupControl3.Controls.Add(this.labelControl15);
            this.groupControl3.Controls.Add(this.textEdit15);
            this.groupControl3.Controls.Add(this.labelControl16);
            this.groupControl3.Location = new System.Drawing.Point(485, 5);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(230, 173);
            this.groupControl3.TabIndex = 47;
            this.groupControl3.Text = "烧录2";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton6.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.AppearanceHovered.Image")));
            this.simpleButton6.AppearanceHovered.Options.UseFont = true;
            this.simpleButton6.AppearanceHovered.Options.UseImage = true;
            this.simpleButton6.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton6.AppearancePressed.Options.UseFont = true;
            this.simpleButton6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.ImageOptions.Image")));
            this.simpleButton6.Location = new System.Drawing.Point(147, 130);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(75, 38);
            this.simpleButton6.TabIndex = 8;
            this.simpleButton6.Text = "清零";
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(164, 65);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit11.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit11.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit11.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit11.Size = new System.Drawing.Size(54, 24);
            this.textEdit11.TabIndex = 7;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(164, 35);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit12.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit12.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit12.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit12.Size = new System.Drawing.Size(54, 24);
            this.textEdit12.TabIndex = 6;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(108, 95);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit13.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit13.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit13.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit13.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit13.Size = new System.Drawing.Size(54, 24);
            this.textEdit13.TabIndex = 5;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearanceDisabled.Options.UseFont = true;
            this.labelControl14.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearanceHovered.Options.UseFont = true;
            this.labelControl14.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl14.AppearancePressed.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(77, 99);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(25, 17);
            this.labelControl14.TabIndex = 4;
            this.labelControl14.Text = "UPH";
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(108, 65);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit14.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit14.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit14.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit14.Size = new System.Drawing.Size(54, 24);
            this.textEdit14.TabIndex = 3;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl15.AppearanceDisabled.Options.UseFont = true;
            this.labelControl15.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl15.AppearanceHovered.Options.UseFont = true;
            this.labelControl15.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl15.AppearancePressed.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(15, 69);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(87, 17);
            this.labelControl15.TabIndex = 2;
            this.labelControl15.Text = "坏品(整个Panel)";
            // 
            // textEdit15
            // 
            this.textEdit15.Location = new System.Drawing.Point(108, 35);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit15.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit15.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit15.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit15.Size = new System.Drawing.Size(54, 24);
            this.textEdit15.TabIndex = 1;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl16.AppearanceDisabled.Options.UseFont = true;
            this.labelControl16.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl16.AppearanceHovered.Options.UseFont = true;
            this.labelControl16.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl16.AppearancePressed.Options.UseFont = true;
            this.labelControl16.Location = new System.Drawing.Point(15, 39);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(87, 17);
            this.labelControl16.TabIndex = 0;
            this.labelControl16.Text = "好品(整个Panel)";
            // 
            // groupControl4
            // 
            this.groupControl4.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.Appearance.Options.UseFont = true;
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.simpleButton7);
            this.groupControl4.Controls.Add(this.textEdit16);
            this.groupControl4.Controls.Add(this.textEdit17);
            this.groupControl4.Controls.Add(this.textEdit18);
            this.groupControl4.Controls.Add(this.labelControl17);
            this.groupControl4.Controls.Add(this.textEdit19);
            this.groupControl4.Controls.Add(this.labelControl18);
            this.groupControl4.Controls.Add(this.textEdit20);
            this.groupControl4.Controls.Add(this.labelControl19);
            this.groupControl4.Location = new System.Drawing.Point(721, 5);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(230, 173);
            this.groupControl4.TabIndex = 47;
            this.groupControl4.Text = "打码/切割";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton7.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton7.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton7.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton7.AppearanceHovered.Image")));
            this.simpleButton7.AppearanceHovered.Options.UseFont = true;
            this.simpleButton7.AppearanceHovered.Options.UseImage = true;
            this.simpleButton7.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton7.AppearancePressed.Options.UseFont = true;
            this.simpleButton7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton7.ImageOptions.Image")));
            this.simpleButton7.Location = new System.Drawing.Point(147, 130);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(75, 38);
            this.simpleButton7.TabIndex = 8;
            this.simpleButton7.Text = "清零";
            // 
            // textEdit16
            // 
            this.textEdit16.Location = new System.Drawing.Point(164, 65);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit16.Properties.Appearance.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit16.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit16.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit16.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit16.Size = new System.Drawing.Size(54, 24);
            this.textEdit16.TabIndex = 7;
            // 
            // textEdit17
            // 
            this.textEdit17.Location = new System.Drawing.Point(164, 35);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit17.Properties.Appearance.Options.UseFont = true;
            this.textEdit17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit17.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit17.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit17.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit17.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit17.Size = new System.Drawing.Size(54, 24);
            this.textEdit17.TabIndex = 6;
            // 
            // textEdit18
            // 
            this.textEdit18.Location = new System.Drawing.Point(108, 95);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit18.Properties.Appearance.Options.UseFont = true;
            this.textEdit18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit18.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit18.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit18.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit18.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit18.Size = new System.Drawing.Size(54, 24);
            this.textEdit18.TabIndex = 5;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl17.Appearance.Options.UseBackColor = true;
            this.labelControl17.Appearance.Options.UseFont = true;
            this.labelControl17.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl17.AppearanceDisabled.Options.UseFont = true;
            this.labelControl17.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl17.AppearanceHovered.Options.UseFont = true;
            this.labelControl17.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl17.AppearancePressed.Options.UseFont = true;
            this.labelControl17.Location = new System.Drawing.Point(77, 99);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(25, 17);
            this.labelControl17.TabIndex = 4;
            this.labelControl17.Text = "UPH";
            // 
            // textEdit19
            // 
            this.textEdit19.Location = new System.Drawing.Point(108, 65);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit19.Properties.Appearance.Options.UseFont = true;
            this.textEdit19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit19.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit19.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit19.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit19.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit19.Size = new System.Drawing.Size(54, 24);
            this.textEdit19.TabIndex = 3;
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl18.Appearance.Options.UseBackColor = true;
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl18.AppearanceDisabled.Options.UseFont = true;
            this.labelControl18.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl18.AppearanceHovered.Options.UseFont = true;
            this.labelControl18.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl18.AppearancePressed.Options.UseFont = true;
            this.labelControl18.Location = new System.Drawing.Point(15, 69);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(87, 17);
            this.labelControl18.TabIndex = 2;
            this.labelControl18.Text = "坏品(整个Panel)";
            // 
            // textEdit20
            // 
            this.textEdit20.Location = new System.Drawing.Point(108, 35);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit20.Properties.Appearance.Options.UseFont = true;
            this.textEdit20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit20.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit20.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit20.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit20.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit20.Size = new System.Drawing.Size(54, 24);
            this.textEdit20.TabIndex = 1;
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl19.AppearanceDisabled.Options.UseFont = true;
            this.labelControl19.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl19.AppearanceHovered.Options.UseFont = true;
            this.labelControl19.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl19.AppearancePressed.Options.UseFont = true;
            this.labelControl19.Location = new System.Drawing.Point(15, 39);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(87, 17);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "好品(整个Panel)";
            // 
            // groupControl5
            // 
            this.groupControl5.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.Appearance.Options.UseFont = true;
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.Controls.Add(this.simpleButton8);
            this.groupControl5.Controls.Add(this.textEdit21);
            this.groupControl5.Controls.Add(this.textEdit22);
            this.groupControl5.Controls.Add(this.textEdit23);
            this.groupControl5.Controls.Add(this.labelControl20);
            this.groupControl5.Controls.Add(this.textEdit24);
            this.groupControl5.Controls.Add(this.labelControl21);
            this.groupControl5.Controls.Add(this.textEdit25);
            this.groupControl5.Controls.Add(this.labelControl22);
            this.groupControl5.Location = new System.Drawing.Point(955, 5);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(230, 173);
            this.groupControl5.TabIndex = 47;
            this.groupControl5.Text = "下料/组盘";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton8.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton8.AppearanceHovered.Image")));
            this.simpleButton8.AppearanceHovered.Options.UseFont = true;
            this.simpleButton8.AppearanceHovered.Options.UseImage = true;
            this.simpleButton8.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton8.AppearancePressed.Options.UseFont = true;
            this.simpleButton8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton8.ImageOptions.Image")));
            this.simpleButton8.Location = new System.Drawing.Point(147, 130);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(75, 38);
            this.simpleButton8.TabIndex = 8;
            this.simpleButton8.Text = "清零";
            // 
            // textEdit21
            // 
            this.textEdit21.Location = new System.Drawing.Point(164, 65);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit21.Properties.Appearance.Options.UseFont = true;
            this.textEdit21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit21.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit21.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit21.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit21.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit21.Size = new System.Drawing.Size(54, 24);
            this.textEdit21.TabIndex = 7;
            // 
            // textEdit22
            // 
            this.textEdit22.Location = new System.Drawing.Point(164, 35);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit22.Properties.Appearance.Options.UseFont = true;
            this.textEdit22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit22.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit22.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit22.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit22.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit22.Size = new System.Drawing.Size(54, 24);
            this.textEdit22.TabIndex = 6;
            // 
            // textEdit23
            // 
            this.textEdit23.Location = new System.Drawing.Point(108, 95);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit23.Properties.Appearance.Options.UseFont = true;
            this.textEdit23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit23.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit23.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit23.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit23.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit23.Size = new System.Drawing.Size(54, 24);
            this.textEdit23.TabIndex = 5;
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl20.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Appearance.Options.UseFont = true;
            this.labelControl20.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.AppearanceDisabled.Options.UseFont = true;
            this.labelControl20.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.AppearanceHovered.Options.UseFont = true;
            this.labelControl20.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl20.AppearancePressed.Options.UseFont = true;
            this.labelControl20.Location = new System.Drawing.Point(77, 99);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(25, 17);
            this.labelControl20.TabIndex = 4;
            this.labelControl20.Text = "UPH";
            // 
            // textEdit24
            // 
            this.textEdit24.Location = new System.Drawing.Point(108, 65);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit24.Properties.Appearance.Options.UseFont = true;
            this.textEdit24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit24.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit24.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit24.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit24.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit24.Size = new System.Drawing.Size(54, 24);
            this.textEdit24.TabIndex = 3;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl21.Appearance.Options.UseBackColor = true;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl21.AppearanceDisabled.Options.UseFont = true;
            this.labelControl21.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl21.AppearanceHovered.Options.UseFont = true;
            this.labelControl21.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl21.AppearancePressed.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(15, 69);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(87, 17);
            this.labelControl21.TabIndex = 2;
            this.labelControl21.Text = "坏品(整个Panel)";
            // 
            // textEdit25
            // 
            this.textEdit25.Location = new System.Drawing.Point(108, 35);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit25.Properties.Appearance.Options.UseFont = true;
            this.textEdit25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit25.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit25.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit25.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit25.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit25.Size = new System.Drawing.Size(54, 24);
            this.textEdit25.TabIndex = 1;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl22.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl22.Appearance.Options.UseBackColor = true;
            this.labelControl22.Appearance.Options.UseFont = true;
            this.labelControl22.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl22.AppearanceDisabled.Options.UseFont = true;
            this.labelControl22.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl22.AppearanceHovered.Options.UseFont = true;
            this.labelControl22.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl22.AppearancePressed.Options.UseFont = true;
            this.labelControl22.Location = new System.Drawing.Point(15, 39);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(87, 17);
            this.labelControl22.TabIndex = 0;
            this.labelControl22.Text = "好品(整个Panel)";
            // 
            // groupControl6
            // 
            this.groupControl6.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl6.Appearance.Options.UseFont = true;
            this.groupControl6.AppearanceCaption.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.groupControl6.AppearanceCaption.Options.UseFont = true;
            this.groupControl6.Controls.Add(this.simpleButton9);
            this.groupControl6.Controls.Add(this.textEdit26);
            this.groupControl6.Controls.Add(this.textEdit27);
            this.groupControl6.Controls.Add(this.textEdit28);
            this.groupControl6.Controls.Add(this.labelControl23);
            this.groupControl6.Controls.Add(this.textEdit29);
            this.groupControl6.Controls.Add(this.labelControl24);
            this.groupControl6.Controls.Add(this.textEdit30);
            this.groupControl6.Controls.Add(this.labelControl25);
            this.groupControl6.Location = new System.Drawing.Point(1191, 5);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(230, 173);
            this.groupControl6.TabIndex = 48;
            this.groupControl6.Text = "打包";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearanceDisabled.Options.UseFont = true;
            this.simpleButton9.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearanceHovered.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton9.AppearanceHovered.Image")));
            this.simpleButton9.AppearanceHovered.Options.UseFont = true;
            this.simpleButton9.AppearanceHovered.Options.UseImage = true;
            this.simpleButton9.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.simpleButton9.AppearancePressed.Options.UseFont = true;
            this.simpleButton9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton9.ImageOptions.Image")));
            this.simpleButton9.Location = new System.Drawing.Point(147, 130);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(75, 38);
            this.simpleButton9.TabIndex = 8;
            this.simpleButton9.Text = "清零";
            // 
            // textEdit26
            // 
            this.textEdit26.Location = new System.Drawing.Point(164, 65);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit26.Properties.Appearance.Options.UseFont = true;
            this.textEdit26.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit26.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit26.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit26.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit26.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit26.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit26.Size = new System.Drawing.Size(54, 24);
            this.textEdit26.TabIndex = 7;
            // 
            // textEdit27
            // 
            this.textEdit27.Location = new System.Drawing.Point(164, 35);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit27.Properties.Appearance.Options.UseFont = true;
            this.textEdit27.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit27.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit27.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit27.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit27.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit27.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit27.Size = new System.Drawing.Size(54, 24);
            this.textEdit27.TabIndex = 6;
            // 
            // textEdit28
            // 
            this.textEdit28.Location = new System.Drawing.Point(108, 95);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit28.Properties.Appearance.Options.UseFont = true;
            this.textEdit28.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit28.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit28.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit28.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit28.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit28.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit28.Size = new System.Drawing.Size(54, 24);
            this.textEdit28.TabIndex = 5;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.labelControl23.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.Appearance.Options.UseBackColor = true;
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearanceDisabled.Options.UseFont = true;
            this.labelControl23.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearanceHovered.Options.UseFont = true;
            this.labelControl23.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl23.AppearancePressed.Options.UseFont = true;
            this.labelControl23.Location = new System.Drawing.Point(77, 99);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(25, 17);
            this.labelControl23.TabIndex = 4;
            this.labelControl23.Text = "UPH";
            // 
            // textEdit29
            // 
            this.textEdit29.Location = new System.Drawing.Point(108, 65);
            this.textEdit29.Name = "textEdit29";
            this.textEdit29.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit29.Properties.Appearance.Options.UseFont = true;
            this.textEdit29.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit29.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit29.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit29.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit29.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit29.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit29.Size = new System.Drawing.Size(54, 24);
            this.textEdit29.TabIndex = 3;
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.BackColor = System.Drawing.Color.DeepPink;
            this.labelControl24.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.Appearance.Options.UseBackColor = true;
            this.labelControl24.Appearance.Options.UseFont = true;
            this.labelControl24.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearanceDisabled.Options.UseFont = true;
            this.labelControl24.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearanceHovered.Options.UseFont = true;
            this.labelControl24.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl24.AppearancePressed.Options.UseFont = true;
            this.labelControl24.Location = new System.Drawing.Point(15, 69);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(87, 17);
            this.labelControl24.TabIndex = 2;
            this.labelControl24.Text = "坏品(整个Panel)";
            // 
            // textEdit30
            // 
            this.textEdit30.Location = new System.Drawing.Point(108, 35);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Properties.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit30.Properties.Appearance.Options.UseFont = true;
            this.textEdit30.Properties.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit30.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEdit30.Properties.AppearanceFocused.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit30.Properties.AppearanceFocused.Options.UseFont = true;
            this.textEdit30.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.textEdit30.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.textEdit30.Size = new System.Drawing.Size(54, 24);
            this.textEdit30.TabIndex = 1;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.Lime;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.Appearance.Options.UseBackColor = true;
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.AppearanceDisabled.Options.UseFont = true;
            this.labelControl25.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.AppearanceHovered.Options.UseFont = true;
            this.labelControl25.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.labelControl25.AppearancePressed.Options.UseFont = true;
            this.labelControl25.Location = new System.Drawing.Point(15, 39);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(87, 17);
            this.labelControl25.TabIndex = 0;
            this.labelControl25.Text = "好品(整个Panel)";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.panelControl3);
            this.xtraTabPage3.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage3.Image")));
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1551, 203);
            this.xtraTabPage3.Text = "历史记录";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelControl3.Appearance.Options.UseFont = true;
            this.panelControl3.Controls.Add(this.txtExceptionMes);
            this.panelControl3.Controls.Add(this.gridControl1);
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1551, 204);
            this.panelControl3.TabIndex = 1;
            // 
            // panelControl4
            // 
            this.panelControl4.Location = new System.Drawing.Point(0, 86);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1904, 940);
            this.panelControl4.TabIndex = 70;
            // 
            // lableRFID
            // 
            this.lableRFID.Appearance.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lableRFID.Appearance.Options.UseFont = true;
            this.lableRFID.AppearanceDisabled.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lableRFID.AppearanceDisabled.Options.UseFont = true;
            this.lableRFID.AppearanceHovered.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lableRFID.AppearanceHovered.Options.UseFont = true;
            this.lableRFID.AppearancePressed.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lableRFID.AppearancePressed.Options.UseFont = true;
            this.lableRFID.Location = new System.Drawing.Point(1244, 58);
            this.lableRFID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lableRFID.Name = "lableRFID";
            this.lableRFID.Size = new System.Drawing.Size(36, 21);
            this.lableRFID.TabIndex = 75;
            this.lableRFID.Text = "RFID";
            // 
            // btmNewProduct
            // 
            this.btmNewProduct.Appearance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmNewProduct.Appearance.Options.UseFont = true;
            this.btmNewProduct.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.btmNewProduct.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton10.ImageOptions.Image")));
            this.btmNewProduct.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.btmNewProduct.Location = new System.Drawing.Point(941, 38);
            this.btmNewProduct.Name = "btmNewProduct";
            this.btmNewProduct.Size = new System.Drawing.Size(185, 38);
            this.btmNewProduct.TabIndex = 52;
            this.btmNewProduct.Text = "新建产品型号(当前复制)";
            this.btmNewProduct.Click += new System.EventHandler(this.btmNewProduct_Click);
            // 
            // frm_Main
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 1031);
            this.Controls.Add(this.btmNewProduct);
            this.Controls.Add(this.lableRFID);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.simpleButton12);
            this.Controls.Add(this.simpleButton11);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.dropDownButton1);
            this.Controls.Add(this.labMachineStatus);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.lableOperator);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panelControl4);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm_Main";
            this.Text = "Apexmic HP带头智能自动生产线 V1.0 (Created by ZhongHuaZhiNeng)";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Main_Closing);
            this.Load += new System.EventHandler(this.frm_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataMsgError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataMsgRun)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lableOperator;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn 日期时间;
        private DevExpress.XtraGrid.Columns.GridColumn 信息类型;
        private DevExpress.XtraGrid.Columns.GridColumn 信息描述;
        private DevExpress.XtraGrid.Columns.GridColumn 解决方案;
        private System.Windows.Forms.ImageList imageList32;
        private System.Windows.Forms.ImageList imageList72;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labMachineStatus;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.Timer timeExeStatus;
        private System.Windows.Forms.Timer timeMonitorMessage;
        private System.Windows.Forms.DataGridView dataMsgError;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameType;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameSolution;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.DropDownButton dropDownButton1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private System.Windows.Forms.TextBox txtExceptionMes;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private DevExpress.XtraEditors.TextEdit textEdit17;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit textEdit19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit textEdit20;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.TextEdit textEdit21;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private DevExpress.XtraEditors.TextEdit textEdit23;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit textEdit24;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit textEdit25;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.TextEdit textEdit26;
        private DevExpress.XtraEditors.TextEdit textEdit27;
        private DevExpress.XtraEditors.TextEdit textEdit28;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.TextEdit textEdit29;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit textEdit30;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.DataGridView dataMsgRun;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DevExpress.XtraEditors.LabelControl lableRFID;
        private DevExpress.XtraEditors.SimpleButton btmNewProduct;
    }
}

