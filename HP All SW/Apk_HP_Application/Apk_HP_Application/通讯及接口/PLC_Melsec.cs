﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HslCommunication.Profinet.Omron;
using HslCommunication;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace CommunicationZH
{
    public class MelsecPLC_TCP : HslCommunication.Profinet.Melsec.MelsecFxLinks
    {
        public bool isConnected = false;
        public MelsecPLC_TCP() : base()
        {
        }

        public override OperateResult<byte[]> Read(string address, ushort length)
        {
            try
            {
                return base.Read(address, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public override string ToString()
        {
            return base.ToString();
        }
        public override OperateResult Write(string address, byte[] value)
        {
            try
            {
                return base.Write(address, value);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}

